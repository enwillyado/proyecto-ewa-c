/*********************************************************************************************
 *	Name		: w_red_biproxy_tcp.cpp
 *	Description	: Implementación de la clase para implementar un proxy de red basado en TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_biproxy_tcp.h"

typedef TWThreadData<ServidorTCP*> TWThreadDataProxy;
void BiProxyTCP::funcionHilo (const WThread & hilo)
{
	const TWThreadDataProxy & c = hilo.getData<TWThreadDataProxy>();
	c.data->start();
}

BiProxyTCP::BiProxyTCP (const std::string & remoteHost, const int remotePort, const int biLocalPort)
	: ProxyTCP (remoteHost, remotePort), biSocketID (-1)
{
	if (true == this->biServidorTCP.open (biLocalPort))
	{
		this->biServidorTCP.setCallBack (&BiProxyTCP::biServidor, this);

		TWThreadDataProxy hiloData (&this->biServidorTCP);
		WThread hilo (&BiProxyTCP::funcionHilo, hiloData);
		hilo.start();
	}
}
void BiProxyTCP::onClienteGetData (const SocketID socketID, const std::string & mensaje)
{
	ProxyTCP::onClienteGetData (socketID, mensaje);

	// Y mandar o acumular el mensaje por la bi-conexión
	if (this->biSocketID != -1)
	{
		this->biServidorTCP.send (this->biSocketID, mensaje);
	}
	else
	{
		this->mensaje0 += mensaje;
	}
}

void BiProxyTCP::biServidor (ServidorTCP & servidor)
{
	const EventoServidorTCP & evento = servidor.getEvento();
	servidor.analizaEventoDef (evento);

	// Y procesar la nueva bi-conexión
	if (evento.tipo ==  EventoServidorTCP::CONEXION_ACEPTADA)
	{
		servidor.getCallBackObj<BiProxyTCP>().biSocketID = evento.socketID;
		std::string & mensaje0 = servidor.getCallBackObj<BiProxyTCP>().mensaje0;
		if (mensaje0 != "")
		{
			servidor.send (evento.socketID, mensaje0);
			mensaje0.clear();
		}
	}
}
