/*********************************************************************************************
 * Name			: ewadrones.cpp
 * Description	: Implementación de la clase para gestionar el AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones/ewadrones.h"

EWadrones::EWadrones (const std::string & iHost)
	: host (iHost),
	  controlador(), video(), xml()
{
	this->controlador.setPtrArDrone (this);
	this->video.setPtrArDrone (this);
	this->xml.setPtrArDrone (this);
}

void EWadrones::init()
{
	this->controlador.init();
	this->video.init();
}
void EWadrones::start()
{
	this->controlador.start();
	this->video.start();
}
void EWadrones::stop()
{
	this->controlador.stop();
	this->video.stop();
}

// ----------------------------------------------------------------------------
const std::string & EWadrones::getHost() const
{
	return this->host;
}

void EWadrones::setHost (const std::string & host)
{
	this->host = host;
}

// ----------------------------------------------------------------------------
ArDroneControlador & EWadrones::getControlador()
{
	return this->controlador;
}
const ArDroneControlador & EWadrones::getControlador() const
{
	return this->controlador;
}

ArDroneVideo & EWadrones::getVideo()
{
	return this->video;
}
const ArDroneVideo & EWadrones::getVideo() const
{
	return this->video;
}

ArDroneXML & EWadrones::getXML()
{
	return this->xml;
}
const ArDroneXML & EWadrones::getXML() const
{
	return this->xml;
}
