/*********************************************************************************************
 *	Name		: w_red_bypass_udp.cpp
 *	Description	: Implementaci�n de la clase para implementar un by-pass de red basado en TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_bypass_udp.h"

typedef TWThreadData<ServidorUDP*> TWThreadDataProxy;
void ByPassUDP::funcionHilo (const WThread & hilo)
{
	const TWThreadDataProxy & c = hilo.getData<TWThreadDataProxy>();
	c.data->start();
}

// ----------------------------------------------------------------------------
// Constructor
ByPassUDP::ByPassUDP (const int localPort)
	: eventoRemote(), eventoByPass()
{
	if (true == this->servidorToRemoteUDP.open (localPort))
	{
		this->servidorToRemoteUDP.setCallBack (&ByPassUDP::staticCallBackRemote, this);

		TWThreadDataProxy hiloData (&this->servidorToRemoteUDP);
		WThread hilo (&ByPassUDP::funcionHilo, hiloData);
		hilo.start();
	}
}


bool ByPassUDP::open (const int byPassPort)
{
	// Ajustar el servidor y abrirlo
	const bool retCallBack = servidorToByPassUDP.setCallBack (&ByPassUDP::staticCallBackByPass, this);
	const bool retOpen = retCallBack && this->servidorToByPassUDP.open (byPassPort);
	return retCallBack && retOpen;
}

bool ByPassUDP::start()
{
	const bool retStart = this->servidorToByPassUDP.start();
	return retStart;
}

// ----------------------------------------------------------------------------
//
void ByPassUDP::staticCallBackRemote (ServidorUDP & servidorRemote)
{
	ByPassUDP & byPassUDP = servidorRemote.getCallBackObj<ByPassUDP>();
	byPassUDP.callBackRemote();
}
void ByPassUDP::callBackRemote()
{
	const EventoServidorUDP & evento = this->servidorToRemoteUDP.getEvento();
	this->servidorToRemoteUDP.analizaEventoDef (evento);

	// Procesar el mensaje UDP transmitido desde el remoto
	{
		if (this->eventoRemote.socketID != evento.socketID)
		{
			// El primer mensaje, es de conexi�n en UDP
			this->eventoRemote = evento;
		}
		// Y siempre se env�an al by-pass remoto
		this->onRemoteGetData (evento, evento.mensaje);
	}
}

// ----------------------------------------------------------------------------
//
void ByPassUDP::staticCallBackByPass (ServidorUDP & servidorByPass)
{
	ByPassUDP & byPassUDP = servidorByPass.getCallBackObj<ByPassUDP>();
	byPassUDP.callBackByPass();
}
void ByPassUDP::callBackByPass()
{
	const EventoServidorUDP & evento = this->servidorToByPassUDP.getEvento();
	servidorToByPassUDP.analizaEventoDef (evento);

	// Procesar el mensaje UDP transmitido desde el by-pass
	{
		if (this->eventoByPass.socketID != evento.socketID ||
				this->eventoByPass.puerto != evento.puerto ||
				this->eventoByPass.ip != evento.ip)
		{
			// El primer mensaje, es de conexi�n en UDP
			this->eventoByPass = evento;
			this->servidorToByPassUDP.send (this->eventoByPass, evento.mensaje);
		}
		else
		{
			// Y el resto, se env�an al cliente remoto
			this->onByPassPutData (evento, evento.mensaje);
		}
	}
}

// ----------------------------------------------------------------------------
// M�todos de by-pas
void ByPassUDP::onRemoteGetData (const EventoServidorUDP & evento, const std::string & mensaje)
{
	// Cliente --send-> ByPass
	if (this->eventoByPass.socketID != SOCKET_ID_NULL)
	{
		this->servidorToByPassUDP.send (this->eventoByPass, mensaje);
	}
}
void ByPassUDP::onByPassPutData (const EventoServidorUDP & evento, const std::string & mensaje)
{
	// ByPass --send-> Remote
	if (this->eventoRemote.socketID != SOCKET_ID_NULL)
	{
		this->servidorToRemoteUDP.send (this->eventoRemote, mensaje);
	}
}
