/*********************************************************************************************
 * Name			: w_sistema_execs_unix.cpp
 * Description	: Class to call a command
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef WIN32
#include "w_sistema/w_sistema_execs_bi.h"

#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

enum Descriptores
{
	RD = 0,   // Read end of pipe
	WR = 1,   // Write end of pipe
};

bool SistemaExecsBi::putEntrada (const std::string & valor)
{
	char inbuf[SistemaExecsBase::BUFFER_CMD_SIZE];
	const int wr = write (this->filedes[WR], valor.c_str(), valor.size());
	if (wr > 0)
	{
		return true;
	}
#ifdef _DEBUG
	const int i = errno;
#endif
	this->lastError = SistemaExecsBase::FINALIZADO;
	return false;
}

std::string SistemaExecsBi::getPartialSalidaComando() const
{
	char inbuf[SistemaExecsBase::BUFFER_CMD_SIZE];
	const int rd = read (this->filedes[RD], inbuf, SistemaExecsBase::BUFFER_CMD_SIZE);
	if (rd > 0)
	{
		inbuf[rd] = '\0';
		return inbuf;
	}
#ifdef _DEBUG
	const int i = errno;
#endif
	this->lastError = SistemaExecsBase::FINALIZADO;
	return "";
}
SistemaExecsBase::Errores SistemaExecsBi::exeComando()
{
	// Crear las pipes
	int ptocpipe[2];    // Parent-to-child pipe
	int ctoppipe[2];    // Chile-to-parent pipe

	// Create pipe for writing to child
	if (pipe (ptocpipe) < 0)
	{
		fprintf (stderr, "pipe(ptocpipe) failed!\n");
		_exit (1);
	}

	// Create pipe for writing back to parent
	if (pipe (ctoppipe) < 0)
	{
		fprintf (stderr, "pipe(ctoppipe) failed!\n");
		_exit (2);
	}

	// Duplicar el proceso
	const pid_t pid = fork();
	if (pid == -1)
	{
		// Error al duplicar el proceso
		return this->lastError = SistemaExecsBase::ERROR_CREAR_PROCESO;
	}
	else if (pid == 0)
	{
		// --------------------------------------------------------------------------
		// Hijo
		// --------------------------------------------------------------------------

		// Cerrar los descriptores innecesarios
		close (ptocpipe[WR]);
		close (ctoppipe[RD]);

		// Preparar el "stdin"
		close (0);
		if (dup (ptocpipe[RD]) < 0)
		{
			_exit (3);
		}

		// Preparar el "stdout"
		close (1);
		if (dup (ctoppipe[WR]) < 0)
		{
			fprintf (stderr, "Failed dup(stdout)\n");
			_exit (4);
		}

		// Crear el proceso de esta forma
		// TODO: �mejorar la forma de crear el proceso?
		execl ("/bin/sh", "sh", "-c", (this->getComando()).c_str(), NULL);
		_exit (5);
	}

	// --------------------------------------------------------------------------
	// Proceso principal
	// --------------------------------------------------------------------------

	// ---------------------------------------------------------------------------
	// Cerrar los descriptores innecesarios
	close (ptocpipe[RD]);
	close (ctoppipe[WR]);

	// Ajustar los descriptores interesantes
	this->filedes[WR] = ptocpipe[WR];
	this->filedes[RD] = ctoppipe[RD];

	this->lastError = SistemaExecsBase::SIN_ERROR;

	return this->lastError;
}

#endif
