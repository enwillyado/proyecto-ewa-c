/*********************************************************************************************
 * Name			: w_web_catalogo.h
 * Description	: Almac�n para obtener un determinado servicio que ejecutar�
 *					el Servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_CATALOGO_H_
#define _W_RED_CATALOGO_H_

#include "w_web/w_web_services_base.h"			//< para tener "BasicSrvService"
#include "w_web/w_web_catalogo_extendidos.h"	//< para tener "MapaServiciosExtendidos"

#include <ctype.h>								//< para el "toupper"

// Clase para almacenar los servicios registrados
class BasicSrvServicesCatalogo
{
	~BasicSrvServicesCatalogo()
	{
		BasicSrvServicesCatalogo::delAllServices();
	}
public:
	typedef std::map<std::string, const BasicSrvService*> MapaServicios;

	// Gesti�n individual de los servicios
	static const BasicSrvService* getService (const std::string &);
	static bool addService (const BasicSrvService & service);
	static bool addService (const BasicSrvService* ptr);
	static bool delService (const BasicSrvService & service);
	static bool delService (const BasicSrvService* ptr);
	static bool delService (const std::string &);

	// Gesti�n del grupo de servicios
	static const MapaServicios & getAllServices();
	static bool delAllServices();
	static size_t size();

	// Static utils
	static std::string getIdAbs (const std::string & id);
	static std::string tolower (std::string str);

protected:
	static const BasicSrvService* getServiceAbs (const std::string &);
	// Gesti�n de los servicios extendidos
	static void insertarExtendido (const std::string & id);
	static void borrarExtendido (const std::string & id);

private:
	BasicSrvServicesCatalogo() {};
	// �rea de datos est�tica y privada:
	static MapaServicios mapaServicios;
	static IndiceServiciosExtendidos indiceServiciosExtendidos;
};

#endif	//_W_RED_CATALOGO_H_
