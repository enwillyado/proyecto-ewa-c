/*********************************************************************************************
 *	Name		: w_web_services_id.hpp
 *	Description	: Identificadores (como string) de los servicios que se ejecutar�n
 *					en el Servidor usando polimorfismo (al menos los p�blicos)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_SERVICES_ID_H_
#define _W_WEB_SERVICES_ID_H_

// Servicio para cuando se haga un ping
#define SRVSERVICE_PING_ID		"/ping"

// Servicio para cuando se descargue un recurso
#define SRVSERVICE_DOWNLOAD_ID	"/download"

#endif	//_W_WEB_SERVICES_ID_H_
