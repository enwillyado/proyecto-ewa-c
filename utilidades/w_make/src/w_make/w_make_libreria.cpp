/*********************************************************************************************
 *	Name		: w_make_libreria.cpp
 *	Description	: Implementanción de las funciones para preparar libreria
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_make/w_make.h"

#include "w_sistema/w_sistema_execs.h"
#include "w_sistema/w_sistema_files.h"

#include <iostream>

WMake::Estado WMake::prepararLib()
{
	if (this->parametros.srcOrigen.size() > 0)
	{
		// Pasar de src a obj
		this->prepararObj();
	}

	const std::string & dirOrigen = this->parametros.dirOrigen;
	const std::string & dirBinDestino = this->parametros.dirBinDestino;

	{
		// Crear el directorio de destino de la librería
		const std::string & dirBinDestinoFinal = dirOrigen + dirBinDestino;
		std::cout << "+" << dirBinDestinoFinal << std::endl;
		WFiles::createDirPath (dirBinDestinoFinal);
	}

	const strvector & srcOrigen = this->parametros.srcOrigen;
	{
		// Crear la librería empaquetando todos los ficheros objeto
		std::string comando;
		comando += "ar ruvs " + (dirOrigen + dirBinDestino + "lib" + this->parametros.nombre + ".a");

		const strvector & objOrigen = this->parametros.objOrigen;
		for (size_t i = 0; i < objOrigen.size(); i++)
		{
			const std::string & itemIn = objOrigen[i];
			comando += " " + (dirOrigen + itemIn);
		}

		SistemaExecsSimple exec;
		exec.setComando (comando + STDERR_REDIRECT);
		std::cout << "[]" << exec.getComando() << std::endl;
		exec.exeComando();

		const std::string & salida = exec.getSalidaComando();
		std::cout << "[]>" << salida << std::endl;
	}

	return WMake::CORRECTO;
}
