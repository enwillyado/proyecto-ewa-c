/*********************************************************************************************
 *	Name		: _w_red_srvevent.h
 *	Description	: Evento recibido por un Servidor TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRVEVENT_H_
#define _W_RED_SRVEVENT_H_

#include "w_red/w_red_event.h"			//< para tener "EventoTCP" y "EventoUPD"
#include <queue>
#include <map>

#include <string>

class EventoServidorTCP : public EventoTCP
{
public:
	EventoServidorTCP();
};

typedef std::queue<EventoServidorTCP> ColaEventosServidorTCP;
typedef std::map<SocketID, EventoServidorTCP> MapaEventosServidorTCP;

class EventoServidorUDP : public EventoUDP
{
public:
	EventoServidorUDP();

private:
	static void CONEXION_ACEPTADA() {}
};

typedef std::queue<EventoServidorUDP> ColaEventosServidorUDP;

#endif	//_W_RED_SRVEVENT_H_
