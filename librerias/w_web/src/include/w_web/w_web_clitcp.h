/*********************************************************************************************
 *	Name		: _w_web_cliudp.h
 *	Description	: Cliente web en TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_CLITCP_H_
#define _W_WEB_CLIUDP_H_

#include "w_web/w_web_mensaje.h"
#include "w_red/w_red_clitcp.h"

class ClienteWebTCP : public ClienteTCP
{
public:
	// ASK: �M�s constructores?
	ClienteWebTCP();

public:
	//---------------------------------------------------------------------------
	// Env�o a alto nivel de los mensajes
	bool enviar_recibir (const Mensaje & mensajeEnviar, Mensaje & respuesta, unsigned int segundosEsperados = 0);

public:
	//---------------------------------------------------------------------------
	// Env�o a bajo nivel de mensajes
	bool enviar (const Mensaje & mensaje);
	bool recibir (Mensaje & mensaje, unsigned int segundosEsperados = 0);
};

#endif	//_W_WEB_CLITCP_H_
