/*********************************************************************************************
 * Name			: w_shell_online_services.h
 * Description	: Servicios para el ShellOnline
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SHELL_ONLINE_SERVICES_H_
#define _W_SHELL_ONLINE_SERVICES_H_

#include "w_web/w_web_services.h"							//< para tener el "BasicSrvService"
#include "w_shell_online/w_shell_online_services_id.hpp"	//< para tener los ID de los ServiciosThread de prueba

#include "w_shell_online/w_shell_online.h"					//< para tener los ID de los ServiciosThread de prueba

// ---------------------------------------------------------------------------
// Servicios para el ShellOnline
class ServicioShellOnline : public BasicSrvService
{
public:
	ServicioShellOnline (ShellOnline* const shellOnline)
		: shellOnline (shellOnline)
	{
	}
	virtual BasicSrvService* newInstance() const
	{
		return new ServicioShellOnline (*this);
	}
	virtual std::string getServicioName() const
	{
		return SHELL_ONLINE_SERCICES_ID;
	}

public:
	// Funci�n para ejecutarse por un hilo
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;


private:
	ShellOnline* const shellOnline;
};

#endif	//_W_SHELL_ONLINE_SERVICES_H_
