/*********************************************************************************************
 *	Name		: _w_sistema.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_PRAGMALIB_H_
#define _W_SISTEMA_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_sistema/_w_sistema_thread.pragmalib.h"
#include "w_sistema/_w_sistema_execs.pragmalib.h"
#include "w_sistema/_w_sistema_files.pragmalib.h"
#include "w_sistema/_w_sistema_time.pragmalib.h"

#endif

#undef _W_SISTEMA_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_SISTEMA_PRAGMALIB_H_
