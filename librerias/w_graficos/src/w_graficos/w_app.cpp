/*********************************************************************************************
 * Name			: w_app.cpp
 * Description	: Implementación de la clase que gestiona una aplicación gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_app.h"

// ----------------------------------------------------------------------------
// Métodos propios
int WApp::init (const size_t ancho, const size_t alto)
{
	// Set dimensiones
	this->WControl::lienzo.reserve (ancho, alto);

	// Set parent and root
	this->setParent (*this);
	this->setRoot (*this);

	// Continuar inicialización
	return this->initInternal();
}

void WApp::destructor()
{
	WMultiControl::destructor();
}

const WLienzo & WApp::getFondo() const
{
	return this->fondo;
}
WLienzo & WApp::getFondo()
{
	return this->fondo;
}

bool WApp::setCursor (const WLienzo & cursor)
{
	this->mouse.setLienzo (cursor);
	return this->setCursorInternal();
}
bool WApp::setCursor (const WControl & mouse)
{
	this->mouse = mouse;
	return this->setCursorInternal();
}

bool WApp::setFondo (const WLienzo & fondo)
{
	this->fondo = fondo;
	return true;
}

void WApp::setSize (const size_t ancho, const size_t alto)
{
	this->fondo.reserve (ancho, alto);
	this->WControl::setSize (ancho, alto);
}

bool WApp::rePosAndDim()
{
	return this->rePosAndDimInternal();
}

WLienzo & WApp::recomponerLienzo()
{
	WLienzo & lienzoFinal = this->WControl::getLienzo();

	const bool recomponer = this->WMultiControl::solicitadoPintar || true;
	// TODO: optimizar para evitar esta asignación
	if (recomponer == true)
	{
		// Inicialmente se utiliza el fondo como lienzo de partida
		lienzoFinal = this->fondo;

		// Y recursivamente, se le unen los demás componentes
		this->WMultiControl::recomponerLienzo();
	}
	else
	{
		// TODO: eliminar el cursor anterior del lienzo final
	}

	// Finalmente colocar el cursor
	lienzoFinal.unir (this->mouse);

	// Y retornar
	return lienzoFinal;
}

// ----------------------------------------------------------------------------
// Redefinición de métodos jerárquicos
WControl & WApp::getSelf()
{
	return WMultiControl::getSelf();
}
const WControl & WApp::getSelf() const
{
	return WMultiControl::getSelf();
}
