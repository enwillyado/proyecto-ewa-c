/*********************************************************************************************
 * Name			: w_eventoMouseDrag.h
 * Description	: Evento para los desencadenadores de rat�n
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_EVENTO_MOUSE_DRAG_H_
#define _W_EVENTO_MOUSE_DRAG_H_

#include "w_graficos/w_eventoMouse.h"
#include <cstring>	// size_t

class WEventoMouseDrag : public WEventoMouse
{
public:
	typedef void (*CbEventoMouseDrag) (const WEventoMouseDrag &);

public:
	// Constructor
	WEventoMouseDrag (WControl & iControl, const size_t & iPrevPosX, const size_t & iPrevPosY);

	// Observadores r�pidos
	size_t getPosX() const;
	size_t getPosY() const;

	size_t getIncrementoPosX() const;
	size_t getIncrementoPosY() const;

	// �rea de datos
	const size_t & prevPosX;
	const size_t & prevPosY;
};

#endif // _W_EVENTO_MOUSE_WHEEL_H_
