/*********************************************************************************************
 * Name			: w_shell_online_services.cpp
 * Description	: Implementación de los servicios para el ShellOnline
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_shell_online/w_shell_online_services.h"

#include "w_shell_online/w_shell_online_services.h"
#include "w_web/w_web_catalogo.h"

ShellOnline::ShellOnline()
	: ServidorWebTCP()
{
	BasicSrvServicesCatalogo::addService (ServicioShellOnline (this));
}
