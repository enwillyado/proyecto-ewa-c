/*********************************************************************************************
 *	Name		: w_red_proxy_udp.cpp
 *	Description	: Implementaci�n de la clase para implementar un proxy de red basado en UDP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_proxy_udp.h"
#include <iostream>

static bool isgraph (const std::string & m)
{
	for (size_t i = 0; i < m.size(); i++)
	{
		const char c = m[i];
		if (false == ::isgraph (c) && c != ' ' && c != '\r' && c != '\n' && c != '\t')
		{
			return false;
		}
	}
	return true;
}

// ----------------------------------------------------------------------------
// Clase auxiliar para alojar los clientes remotos
class ProxyUDP::DatosCliente
{
public:
	// Funci�n manejadora para cada cliente
	static void recibirCliente (const WThread & hilo)
	{
		const ProxyUDP::DatosCliente::TWThreadDataClientes & c = hilo.getData<ProxyUDP::DatosCliente::TWThreadDataClientes>();
		while (c.data->clienteUDP.getLastError() == ClienteUDP::SOCKET_ACTIVO)
		{
			// Leer mensajes mientras el socket est� activo
			std::string nuevoMensaje;
			if (false == c.data->clienteUDP.recibir (nuevoMensaje))
			{
				break;
			}

			// Y mand�rselos al proxy
			c.data->proxyUDP->onClienteGetData (c.data->evento, nuevoMensaje);
		}
	}

public:
	typedef TWThreadData<DatosCliente*> TWThreadDataClientes;

	// �rea de datos p�blica
	WThread hilo;
	ClienteUDP clienteUDP;
	SocketID socketID;
	EventoServidorUDP evento;
	ProxyUDP* proxyUDP;		//< referencia al padre
};

// ----------------------------------------------------------------------------
// Constructor del proxy
ProxyUDP::ProxyUDP (const std::string & remoteHost, const int remotePort)
	: Proxy (remoteHost, remotePort), clientesUDP()
{
}

void ProxyUDP::staticCallBack (ServidorUDP & servidor)
{
	ProxyUDP & self = servidor.getCallBackObj<ProxyUDP>();
	self.callBack();
}
void ProxyUDP::callBack()
{
	const EventoServidorUDP & evento = this->servidorUDP.getEvento();
	const SocketID & socketID = evento.socketID;
	switch (evento.tipo)
	{
	case Evento::CONEXION_ACEPTADA:
	case Evento::MENSAJE_RECIBIDO:
	{
		// Enviar al cliente remoto el evento recibido
		this->aceptarConexion (evento);
		this->onProxyGetData (socketID, evento.mensaje);
	}
	break;

	case Evento::CONEXION_DESCONECTADA:
	case Evento::CONEXION_PERDIDA:
	{
		ProxyUDP::DatosClientesUDP::iterator itr = this->clientesUDP.find (socketID);
		if (itr != this->clientesUDP.end())
		{
			// Detener el cliente remoto y su hilo
			itr->second->clienteUDP.closeSocket();
			itr->second->hilo.finish();

			// Borrar cliente remoto
			delete itr->second;
			itr->second = NULL;

			this->clientesUDP.erase (itr);

			// Cerrar conexi�n con el padre
			// NOTA: no tiene mucho sentido en UDP la desconexi�n
		}
	}
	break;

	default:
	{
		int i = 0;
	}
	break;
	}
}

void ProxyUDP::aceptarConexion (const EventoServidorUDP & evento)
{
	// Obtener identificador del socket aceptado
	const SocketID & socketID = evento.socketID;

	DatosClientesUDP::iterator itr = this->clientesUDP.find (evento.socketID);
	if (itr == this->clientesUDP.end())
	{
		// Crear datos si no existen ya
		DatosCliente* cliente = new ProxyUDP::DatosCliente;

		// A�adir datos al almacen
		this->clientesUDP[socketID] = cliente;

		// Ajustar padre e identificador
		cliente->proxyUDP = this;
		cliente->socketID = socketID;
		cliente->evento = evento;

		// Crear cliente remoto
		std::cout << "UDP(" << servidorUDP.getPort() << "): open [" << this->Proxy::host << ":" << this->Proxy::port << "]" << std::endl;
		const bool retOpen = cliente->clienteUDP.openSocket (this->Proxy::host, this->Proxy::port);

		// Crear hilo lector remoto->local
		DatosCliente::TWThreadDataClientes data (cliente);
		cliente->hilo.setFunction (&ProxyUDP::DatosCliente::recibirCliente);
		cliente->hilo.setData (data);
		cliente->hilo.start();
	}
	else
	{
		// Reajustar identificador
		itr->second->evento = evento;
	}
}

void ProxyUDP::onProxyGetData (const SocketID socketID, const std::string & mensaje)
{
#ifdef _DEBUG
	std::cout << "UDP(" << servidorUDP.getPort() << "): send (" << socketID << ") [" << mensaje.size() << "] ";
	if (true == ::isgraph (mensaje))
	{
		std::cout << mensaje << std::endl;
	}
#endif

	// Enviar el dato al cliente adecuado si existe
	DatosClientesUDP::iterator itr = this->clientesUDP.find (socketID);
	if (itr != this->clientesUDP.end())
	{
		itr->second->clienteUDP.enviar (mensaje);
	}
}

void ProxyUDP::onClienteGetData (const EventoServidorUDP & evento, const std::string & mensaje)
{
#ifdef _DEBUG
	std::cout << "UDP(" << servidorUDP.getPort() << "): put [" << mensaje.size() << "] ";
	if (true == ::isgraph (mensaje))
	{
		std::cout << mensaje << std::endl;
	}
#endif

	// Enviar el dato al proxy
	this->servidorUDP.send (evento, mensaje);
}

bool ProxyUDP::open (const int localPort)
{
	// Ajustar los valores comunes
	Proxy::open (localPort);

	// Ajustar el servidor y abrirlo
	const bool retCallBack = servidorUDP.setCallBack (&ProxyUDP::staticCallBack, this);
	const bool retOpen = retCallBack && this->servidorUDP.open (localPort);
	return retCallBack && retOpen;
}

bool ProxyUDP::start()
{
	const bool retStart = this->servidorUDP.start();
	return retStart;
}
