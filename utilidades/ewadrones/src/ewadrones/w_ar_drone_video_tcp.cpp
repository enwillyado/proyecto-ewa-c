/*********************************************************************************************
 *	Name		: w_ar_drone_video_tcp.cpp
 *	Description	: Implementaci�n de la funci�n para obtener el v�deo
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones/w_ar_drone_video.h"

#include "ewadrones/ewadrones.h"

#include "w_base/toStr.hpp"
#include "w_base/fromFile.hpp"

// Cargar mapa de puertos necesarios para comunicarse con el AR.DRONE
#include "ewadrones/ar_drone_puertos.hpp"
#include "pave.h"

// Cliente TCP
//#define CONEXION_TCP
#include "w_red/w_red_clitcp.h"

// Funci�n principal
extern "C"
{
	int procesarBuffer (void* iPtr, const char* buffer, unsigned int size);

	void obtenerBuffer (void* iPtr)
	{
		ArDroneVideo* ptr = (ArDroneVideo*) (iPtr);
		ptr->obtenerBuffer();
	}
};

void ArDroneVideo::obtenerBuffer()
{
	void* iPtr = (ArDroneVideo*) this;
	std::string mensaje;

	// Obtener streaming de v�deo
#ifdef CONEXION_TCP
	ClienteTCP c;
	c.openSocket (this->arDrone->getHost(), PUERTO_VIDEO_EN_PROXY);

	// Obtener mensaje mientras dure la conexi�n
	while (c.isOpenSocket() == true)
	{
		// Recibir
		std::string m;
		c.recibir (m);

		// Concatenar
		mensaje += m;
#else
	// Obtener mensaje desde el fichero si est� definido
	std::string file = this->getVideoSrc();
	if (file != "")
	{
		mensaje = ::leerContenidoFile (file);
#endif
		while (true)
		{
			const char* const buffer = mensaje.c_str();
			const size_t tot = mensaje.size();
			if (0 < tot)
			{
				const std::string & cabecera = mensaje.substr (0, 4);
				if (cabecera == PAVE_CABECERA)
				{
					if (sizeof (pave) <= tot)
					{
						const pave* const paveHeader = (const pave * const)buffer;
						if (paveHeader->header_size <= tot)
						{
							unsigned int paveSize = paveHeader->payload_size + paveHeader->header_size;
							if (paveHeader->payload_size + paveHeader->header_size <= tot)
							{
								// Procesar
								const int ret = procesarBuffer (iPtr, buffer, paveSize);
								if (ret < 0)
								{
									// Reintentar
									return this->obtenerBuffer();
								}

								//Truncar
								mensaje = mensaje.substr (ret);

								// Y buscar otro frame m�s
								continue;
							}
						}
					}
				}
				else
				{
					// Reintentar
					return this->obtenerBuffer();
				}
			}

			// En el resto de casos, obtener m�s datos
			break;
		}
	}
}

// ---------------------------------------------------------------------------
// Gestor del v�deo
void ArDroneVideo::setVideoSrc (const std::string & videoSrc)
{
	this->videoSrc = videoSrc;
}
std::string ArDroneVideo::getVideoSrc() const
{
	if (this->videoSrc != "")
	{
		return this->videoSrc;
	}
	return "";
	/*
	const std::string & videoSrc = "tcp://" + this->arDrone->getHost() + ":" + ::toStr (PUERTO_VIDEO_EN_PROXY);
	return videoSrc;
	*/
}
