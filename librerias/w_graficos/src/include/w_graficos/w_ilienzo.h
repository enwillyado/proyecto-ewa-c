/*********************************************************************************************
 * Name			: w_ilienzo.h
 * Description	: Clase para manipular una representación gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ILIENZO_H_
#define _W_ILIENZO_H_

#include <cstring>	//< size_t

// FWD
class RGBA;

class WILienzo
{
public:
	// Accesores
	virtual size_t getAncho() const = 0;
	virtual size_t getAlto() const = 0;

	virtual RGBA getPixel (const size_t x, const size_t y) const = 0;
	virtual bool setPixel (const size_t x, const size_t y, const RGBA &) = 0;

	virtual void reserve (const size_t ancho, const size_t alto) = 0;

protected:
	virtual void reserveIntern() = 0;
	virtual RGBA getPixelFast (const size_t x, const size_t y) const = 0;
	virtual void setPixelFast (const size_t x, const size_t y, const RGBA &) = 0;
};

#endif // _W_ILIENZO_H_
