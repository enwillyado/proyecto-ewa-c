/*********************************************************************************************
*	Name		: w_red_bibypass_udp.h
*	Description	: Clase para implementar un by-pass de red basado en UDP
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_BI_BY_PASS_UDP_H_
#define _W_RED_BI_BY_PASS_UDP_H_

#include "w_red/w_red_cliudp.h"
#include "w_red/w_red_bypass_udp.h"

#include <string>

class BiByPassUDP : public ByPassUDP
{
public:
	BiByPassUDP (const std::string & remoteHost, const int remotePort, const int localPort);

protected:
	// Manejadores de datos del proxy
	virtual void onRemoteGetData (const EventoServidorUDP & evento, const std::string & mensaje);
	virtual void onByPassPutData (const EventoServidorUDP & evento, const std::string & mensaje);

private:
	/*
	std::string remoteHost;
	int remotePort;
	*/
	ClienteUDP clienteRemoteUDP;
};


#endif //_W_RED_BI_BY_PASS_UDP_H_
