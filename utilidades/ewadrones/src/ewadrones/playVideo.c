// tutorial01.c
// Code based on a tutorial by Martin Bohme (boehme@inb.uni-luebeckREMOVETHIS.de)
// Tested on Gentoo, CVS version 5/01/07 compiled with GCC 4.1.1
// With updates from https://github.com/chelyaev/ffmpeg-tutorial
// Updates tested on:
// LAVC 54.59.100, LAVF 54.29.104, LSWS 2.1.101
// on GCC 4.7.2 in Debian February 2015

// A small sample program that shows how to use libavformat and libavcodec to
// read video from a file.
//
// Use
//
// gcc -o tutorial01 tutorial01.c -lavformat -lavcodec -lswscale -lz
//
// to build (assuming libavformat and libavcodec are correctly installed
// your system).
//
// Run using
//
// tutorial01 myvideofile.mpg
//
// to write the first five frames from "myvideofile.mpg" to disk in PPM
// format.
#define inline __inline

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>

#include <libavutil/mathematics.h>

// compatibility with newer API
#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(55,28,1)
#define av_frame_alloc avcodec_alloc_frame
#define av_frame_free avcodec_free_frame
#endif

#ifdef POR_FICHERO
#define INBUF_SIZE			33013760
#define INBUF_SIZE_TOTAL	INBUF_SIZE + FF_INPUT_BUFFER_PADDING_SIZE
#endif

void init (void* iPtr, const int width, const int height);
void send (void* iPtr, const unsigned char* const pBase, const int width, const int y);
void end (void* iPtr);
void obtenerBuffer (void* iPtr);

void saveFrame (void* iPtr, AVFrame* pFrame, int width, int height, int iFrame)
{
	int  y;

	init (iPtr, width, height);

	// Write pixel data
	for (y = 0; y < height; y++)
	{
		//fwrite (pFrame->data[0] + y * pFrame->linesize[0], 1, width * 3, pFile);
		const unsigned char* const p = pFrame->data[0] + y * pFrame->linesize[0];
		send (iPtr, p, width, y);
	}

	end (iPtr);
}

#ifdef POR_FICHERO
uint8_t*	       inbuf = NULL;
#endif

AVCodecContext*    pCodecCtx = NULL;
AVCodec*           pCodec = NULL;
AVFrame*           pFrame = NULL;
AVFrame*           pFrameRGB = NULL;
int                iFrame;
AVPacket*          avPacket;
int                numBytes;
uint8_t*           buffer = NULL;
struct SwsContext* sws_ctx = NULL;

int mainVideoFile (void* iPtr)
{
	// Register all formats and codecs
	av_register_all();
	avformat_network_init();

	avPacket = (AVPacket*)av_malloc (sizeof (AVPacket));
	av_init_packet (avPacket);

#ifdef POR_FICHERO
	/* set end of buffer to 0 (this ensures that no overreading happens for damaged mpeg streams) */
	inbuf = (uint8_t*)av_malloc (INBUF_SIZE_TOTAL * sizeof (uint8_t));
	memset (inbuf + INBUF_SIZE, 0, FF_INPUT_BUFFER_PADDING_SIZE);
#endif

	/* find the mpeg1 video decoder */
	pCodec = avcodec_find_decoder (AV_CODEC_ID_H264);
	if (!pCodec)
	{
		//codec not found
		exit (1);
	}

	pCodecCtx = avcodec_alloc_context3 (pCodec);

	/* For some codecs, such as msmpeg4 and mpeg4, width and height
	   MUST be initialized there because this information is not
	   available in the bitstream. */

	/* open it */
	if (avcodec_open2 (pCodecCtx, pCodec, NULL) < 0)
	{
		//could not open codec
		exit (1);
	}

	// Allocate video frame
	pFrame = av_frame_alloc();

	if (pCodec->capabilities & CODEC_CAP_TRUNCATED)
	{
		pCodecCtx->flags |= CODEC_FLAG_TRUNCATED;    /* we do not send complete frames */
	}
	pCodecCtx->width = 640;					//< TODO: harcoded data init
	pCodecCtx->height = 360;				//< TODO: harcoded data init
	pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;//< TODO: harcoded data init

	// Allocate an AVFrame structure
	pFrameRGB = av_frame_alloc();
	if (pFrameRGB == NULL)
	{
		return -1;
	}

	// Determine required buffer size and allocate buffer
#ifdef WIN32
#define COLOR PIX_FMT_BGR24
#else
#define COLOR PIX_FMT_RGBA
#endif
	numBytes = avpicture_get_size (COLOR, pCodecCtx->width, pCodecCtx->height);
	buffer = (uint8_t*)av_malloc (numBytes * sizeof (uint8_t));

	// Assign appropriate parts of buffer to image planes in pFrameRGB
	// Note that pFrameRGB is an AVFrame, but AVFrame is a superset
	// of AVPicture
	avpicture_fill ((AVPicture*)pFrameRGB, buffer, COLOR, pCodecCtx->width, pCodecCtx->height);

	// initialize SWS context for software scaling
	sws_ctx = sws_getContext (pCodecCtx->width,
							  pCodecCtx->height,
							  pCodecCtx->pix_fmt,
							  pCodecCtx->width,
							  pCodecCtx->height,
							  COLOR,
							  SWS_BILINEAR,
							  NULL,
							  NULL,
							  NULL
							 );


	/* the codec gives us the frame size, in samples */
#ifdef POR_FICHERO
	f = fopen (fileName, "rb");
	if (!f)
	{
		// could not open file
		exit (1);
	}

	unsigned int tot = 0;
	for (;;)
	{
		tot = fread (inbuf, 1, INBUF_SIZE, f);
		if (tot == 0)
		{
			break;
		}

		procesarBuffer (iPtr, inbuf, tot);
	}

	fclose (f);

#else
	// Por socket

	iFrame = 0;
	avPacket->size = 0;
	for (;;)
	{
		obtenerBuffer (iPtr);
	}

#endif

	avcodec_close (pCodecCtx);
	av_free (pCodecCtx);
	av_free (pFrame);

	av_free (avPacket);

	return 0;
}

int procesarBuffer (void* iPtr, const uint8_t* inbuf, const unsigned int tot)
{
#ifdef MAS_DE_UN_FRAME
	int ret = 0;
#endif
	int frameFinished, len;

#ifdef CON_AJUSTE_DE_BUFFER
	// Ajustar buffer
	const unsigned int bufferSize = tot + FF_INPUT_BUFFER_PADDING_SIZE + 1;
	uint8_t* buffer = (uint8_t*)av_malloc (bufferSize * sizeof (uint8_t));
	memcpy (buffer, inbuf, tot);
	memset (buffer + tot, 0, FF_INPUT_BUFFER_PADDING_SIZE + 1);
#else
	uint8_t* buffer = (uint8_t*)inbuf;
#endif
	avPacket->data = buffer;
	avPacket->size = tot;

	// Recorrer buffer
#ifdef MAS_DE_UN_FRAME_EN_BUFFER
	while (avPacket->size > 0)
#endif
	{
		// Decode video frame
		len = avcodec_decode_video2 (pCodecCtx, pFrame, &frameFinished, avPacket);

		// Liberar memoria
		av_free_packet (avPacket);
#ifdef CON_AJUSTE_DE_BUFFER
		av_free (buffer);
#endif

		if (len >= 0)
		{
			// Did we get a video frame?
			if (frameFinished)
			{
				// Convert the image from its native format to RGB
				sws_scale (sws_ctx,
						   (uint8_t const * const*)pFrame->data,
						   pFrame->linesize, 0, pCodecCtx->height,
						   pFrameRGB->data, pFrameRGB->linesize);

				// Save the frame to disk
				saveFrame (iPtr, pFrameRGB, pCodecCtx->width, pCodecCtx->height, iFrame);
				iFrame++;
			}

#ifdef MAS_DE_UN_FRAME
			avPacket->size -= len;
			avPacket->data += len;
			ret += len;
#endif
			return len;
		}
		else
		{
			switch (len)
			{
			case AVERROR_INVALIDDATA:
				// TODO: only warnings?
				return tot;
			}
			return -1;
		}
	}

#ifdef MAS_DE_UN_FRAME
	return ret;
#endif
}
