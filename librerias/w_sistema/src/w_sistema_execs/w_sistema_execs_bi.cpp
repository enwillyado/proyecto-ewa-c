/*********************************************************************************************
 * Name			: w_sistema_execs_bi.cpp
 * Description	: Implementación de la clase para llamar a un comando de forma bi-direccional
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema_execs_bi.h"

SistemaExecsBi::SistemaExecsBi()
	: SistemaExecsBase()
{
}

const std::string & SistemaExecsBi::getSalidaComando() const
{
	do
	{
		this->salidaGenerada += this->getPartialSalidaComando();
	}
	while (this->SistemaExecsBase::lastError == SistemaExecsBase::SIN_ERROR);
	return this->salidaGenerada;
}
