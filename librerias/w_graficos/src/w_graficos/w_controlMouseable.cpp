/*********************************************************************************************
 * Name			: w_controlMouseable.cpp
 * Description	: Implementación de los desencadenadores de ratón para controles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_controlMouseable.h"

#include "w_graficos/w_eventoMouse.h"
#include "w_graficos/w_control.h"
#include "w_graficos/w_app.h"

#include <cstring>

WControlMouseable::WControlMouseable()
	: WControlBase(),
	  cbOnMoveMouseOver (NULL),		cbOnMoveMouseExt (NULL),
	  cbOnMoveMouseEnter (NULL),	cbOnMoveMouseOut (NULL),
	  cbOnLClicMouseOver (NULL),	cbOnMClicMouseOver (NULL),	cbOnRClicMouseOver (NULL),
	  cbOnLUpMouseOver (NULL),		cbOnMUpMouseOver (NULL),	cbOnRUpMouseOver (NULL),
	  cbOnLDownMouseOver (NULL),	cbOnMDownMouseOver (NULL),	cbOnRDownMouseOver (NULL),
	  cbOnLMouseDrag (NULL),		cbOnMMouseDrag (NULL),		cbOnRMouseDrag (NULL),
	  cbOnRUpMouseOut (NULL),		cbOnMUpMouseOut (NULL),		cbOnLUpMouseOut (NULL),
	  cbOnWheelMouseOver (NULL),
	  lastLDragPunteroPos(),		lastMDragPunteroPos(),		lastRDragPunteroPos(),
	  punteroPos()
{
}

void WControlMouseable::clear()
{
	this->cbOnMoveMouseOver = this->cbOnMoveMouseExt = NULL;
	this->cbOnMoveMouseEnter = this->cbOnMoveMouseOut = NULL;
	this->cbOnLClicMouseOver = this->cbOnMClicMouseOver = this->cbOnRClicMouseOver = NULL;
	this->cbOnLUpMouseOver = this->cbOnMUpMouseOver = this->cbOnRUpMouseOver = NULL;
	this->cbOnLDownMouseOver = this->cbOnMDownMouseOver = this->cbOnRDownMouseOver  = NULL;
	this->cbOnLMouseDrag = this->cbOnMMouseDrag = this->cbOnRMouseDrag = NULL;
	this->cbOnRUpMouseOut = this->cbOnMUpMouseOut = this->cbOnLUpMouseOut = NULL;
	this->cbOnWheelMouseOver = NULL;
}

// Observadores
const WCoordenada & WControlMouseable::getPunteroPos() const
{
	return this->punteroPos;
}

// Modificadores
void WControlMouseable::setPunteroPos (const WCoordenada & punteroPos)
{
	this->punteroPos = punteroPos;
}


// ---------------------------------------------------------------------------
// onMoveMouseOver
void WControlMouseable::onMoveMouseOver()
{
	if (this->cbOnMoveMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnMoveMouseOver (evento);
	}
}

void WControlMouseable::setOnMoveMouseOver (WControlMouseable::OnMoveMouseOver cbOnMoveMouseOver)
{
	this->cbOnMoveMouseOver = cbOnMoveMouseOver;
}

// onMoveMouseExt
void WControlMouseable::onMoveMouseExt()
{
	if (this->cbOnMoveMouseExt != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnMoveMouseExt (evento);
	}
}

void WControlMouseable::setOnMoveMouseExt (WControlMouseable::OnMoveMouseExt cbOnMoveMouseExt)
{
	this->cbOnMoveMouseExt = cbOnMoveMouseExt;
}

// onMoveMouseEnter
void WControlMouseable::onMoveMouseEnter()
{
	if (this->cbOnMoveMouseEnter != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnMoveMouseEnter (evento);
	}
}

void WControlMouseable::setOnMoveMouseEnter (WControlMouseable::OnMoveMouseEnter cbOnMoveMouseEnter)
{
	this->cbOnMoveMouseEnter = cbOnMoveMouseEnter;
}

// onMoveMouseOut
void WControlMouseable::onMoveMouseOut()
{
	if (this->cbOnMoveMouseOut != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnMoveMouseOut (evento);
	}
}

void WControlMouseable::setOnMoveMouseOut (WControlMouseable::OnMoveMouseOut cbOnMoveMouseOut)
{
	this->cbOnMoveMouseOut = cbOnMoveMouseOut;
}

// ---------------------------------------------------------------------------
// onLClicMouseOver
void WControlMouseable::onLClicMouseOver()
{
	if (this->cbOnLClicMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnLClicMouseOver (evento);
	}
}

void WControlMouseable::setOnLClicMouseOver (WControlMouseable::OnLClicMouseOver cbOnLClicMouseOver)
{
	this->cbOnLClicMouseOver = cbOnLClicMouseOver;
}

// onLUpMouseOver
void WControlMouseable::onLUpMouseOver()
{
	if (this->cbOnLUpMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnLUpMouseOver (evento);
	}
}

void WControlMouseable::setOnLUpMouseOver (WControlMouseable::OnLUpMouseOver cbOnLUpMouseOver)
{
	this->cbOnLUpMouseOver = cbOnLUpMouseOver;
}

// onLDownMouseOver
void WControlMouseable::onLDownMouseOver()
{
	if (this->cbOnLDownMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnLDownMouseOver (evento);
	}
	this->lastLDragPunteroPos.posX = this->getSelf().getRoot().getCursor().getPosX() + this->getSelf().getRoot().getPosX();
	this->lastLDragPunteroPos.posY = this->getSelf().getRoot().getCursor().getPosY() + this->getSelf().getRoot().getPosY();
}

void WControlMouseable::setOnLDownMouseOver (WControlMouseable::OnLDownMouseOver cbOnLDownMouseOver)
{
	this->cbOnLDownMouseOver = cbOnLDownMouseOver;
}

// ---------------------------------------------------------------------------
// onMClicMouseOver
void WControlMouseable::onMClicMouseOver()
{
	if (this->cbOnMClicMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnMClicMouseOver (evento);
	}
}

void WControlMouseable::setOnMClicMouseOver (WControlMouseable::OnMClicMouseOver cbOnMClicMouseOver)
{
	this->cbOnMClicMouseOver = cbOnMClicMouseOver;
}

// onMUpMouseOver
void WControlMouseable::onMUpMouseOver()
{
	if (this->cbOnMUpMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnMUpMouseOver (evento);
	}
}

void WControlMouseable::setOnMUpMouseOver (WControlMouseable::OnMUpMouseOver cbOnMUpMouseOver)
{
	this->cbOnMUpMouseOver = cbOnMUpMouseOver;
}

// onMDownMouseOver
void WControlMouseable::onMDownMouseOver()
{
	if (this->cbOnMDownMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnMDownMouseOver (evento);
	}
	this->lastMDragPunteroPos.posX = this->getSelf().getRoot().getCursor().getPosX() + this->getSelf().getRoot().getPosX();
	this->lastMDragPunteroPos.posY = this->getSelf().getRoot().getCursor().getPosY() + this->getSelf().getRoot().getPosY();
}

void WControlMouseable::setOnMDownMouseOver (WControlMouseable::OnMDownMouseOver cbOnMDownMouseOver)
{
	this->cbOnMDownMouseOver = cbOnMDownMouseOver;
}

// ---------------------------------------------------------------------------
// onXUpMouseOut
void WControlMouseable::onLUpMouseOut()
{
	if (this->cbOnLUpMouseOut != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnLUpMouseOut (evento);
	}
}

void WControlMouseable::setOnLUpMouseOut (WControlMouseable::OnLUpMouseOut cbOnLUpMouseOut)
{
	this->cbOnLUpMouseOut = cbOnLUpMouseOut;
}

// onMUpMouseOut
void WControlMouseable::onMUpMouseOut()
{
	if (this->cbOnMUpMouseOut != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnMUpMouseOut (evento);
	}
}

void WControlMouseable::setOnMUpMouseOut (WControlMouseable::OnMUpMouseOut cbOnMUpMouseOut)
{
	this->cbOnMUpMouseOut = cbOnMUpMouseOut;
}

// onRDownMouseOver
void WControlMouseable::onRUpMouseOut()
{
	if (this->cbOnRUpMouseOut != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnRUpMouseOut (evento);
	}
}

void WControlMouseable::setOnRUpMouseOut (WControlMouseable::OnRUpMouseOut cbOnRUpMouseOut)
{
	this->cbOnRUpMouseOut = cbOnRUpMouseOut;
}

// ---------------------------------------------------------------------------
// onRClicMouseOver
void WControlMouseable::onRClicMouseOver()
{
	if (this->cbOnRClicMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnRClicMouseOver (evento);
	}
}

void WControlMouseable::setOnRClicMouseOver (WControlMouseable::OnRClicMouseOver cbOnRClicMouseOver)
{
	this->cbOnRClicMouseOver = cbOnRClicMouseOver;
}

// onRUpMouseOver
void WControlMouseable::onRUpMouseOver()
{
	if (this->cbOnRUpMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnRUpMouseOver (evento);
	}
}

void WControlMouseable::setOnRUpMouseOver (WControlMouseable::OnRUpMouseOver cbOnRUpMouseOver)
{
	this->cbOnRUpMouseOver = cbOnRUpMouseOver;
}

// onRDownMouseOver
void WControlMouseable::onRDownMouseOver()
{
	if (this->cbOnRDownMouseOver != NULL)
	{
		WEventoMouse evento (this->getSelf());
		this->cbOnRDownMouseOver (evento);
	}
	this->lastRDragPunteroPos.posX = this->getSelf().getRoot().getCursor().getPosX() + this->getSelf().getRoot().getPosX();
	this->lastRDragPunteroPos.posY = this->getSelf().getRoot().getCursor().getPosY() + this->getSelf().getRoot().getPosY();
}

void WControlMouseable::setOnRDownMouseOver (WControlMouseable::OnRDownMouseOver cbOnRDownMouseOver)
{
	this->cbOnRDownMouseOver = cbOnRDownMouseOver;
}

// ---------------------------------------------------------------------------
// onWheelMouseOver
void WControlMouseable::onWheelMouseOver (const WEventoMouseWheel::Direccion direccion)
{
	if (this->cbOnWheelMouseOver != NULL)
	{
		WEventoMouseWheel evento (this->getSelf(), direccion);
		this->cbOnWheelMouseOver (evento);
	}
}

void WControlMouseable::setOnWheelMouseOver (WControlMouseable::OnWheelMouseOver cbOnWheelMouseOver)
{
	this->cbOnWheelMouseOver = cbOnWheelMouseOver;
}


// ---------------------------------------------------------------------------
// onLMouseDrag
void WControlMouseable::onLMouseDrag()
{
	const size_t newLMouseDragPosX = this->getSelf().getRoot().getCursor().getPosX() + this->getSelf().getRoot().getPosX();
	const size_t newLMouseDragPosY = this->getSelf().getRoot().getCursor().getPosY() + this->getSelf().getRoot().getPosY();
	if (this->cbOnLMouseDrag != NULL)
	{
		WEventoMouseDrag evento (this->getSelf(), this->lastLDragPunteroPos.posX, this->lastLDragPunteroPos.posY);
		this->cbOnLMouseDrag (evento);
	}
	this->lastLDragPunteroPos.posX = newLMouseDragPosX;
	this->lastLDragPunteroPos.posY = newLMouseDragPosY;
}

void WControlMouseable::setOnLMouseDrag (WControlMouseable::OnLMouseDrag cbOnLMouseDrag)
{
	this->cbOnLMouseDrag = cbOnLMouseDrag;
}

// onMMouseDrag
void WControlMouseable::onMMouseDrag()
{
	const size_t newMMouseDragPosX = this->getSelf().getRoot().getCursor().getPosX() + this->getSelf().getRoot().getPosX();
	const size_t newMMouseDragPosY = this->getSelf().getRoot().getCursor().getPosY() + this->getSelf().getRoot().getPosY();
	if (this->cbOnMMouseDrag != NULL)
	{
		WEventoMouseDrag evento (this->getSelf(), lastMDragPunteroPos.posX, this->lastMDragPunteroPos.posY);
		this->cbOnMMouseDrag (evento);
	}
	this->lastMDragPunteroPos.posX = newMMouseDragPosX;
	this->lastMDragPunteroPos.posY = newMMouseDragPosY;
}

void WControlMouseable::setOnMMouseDrag (WControlMouseable::OnMMouseDrag cbOnMMouseDrag)
{
	this->cbOnMMouseDrag = cbOnMMouseDrag;
}

// onRMouseDrag
void WControlMouseable::onRMouseDrag()
{
	const size_t newRMouseDragPosX = this->getSelf().getRoot().getCursor().getPosX() + this->getSelf().getRoot().getPosX();
	const size_t newRMouseDragPosY = this->getSelf().getRoot().getCursor().getPosY() + this->getSelf().getRoot().getPosY();
	if (this->cbOnRMouseDrag != NULL)
	{
		WEventoMouseDrag evento (this->getSelf(), this->lastRDragPunteroPos.posX, this->lastRDragPunteroPos.posY);
		this->cbOnRMouseDrag (evento);
	}
	this->lastRDragPunteroPos.posX = newRMouseDragPosX;
	this->lastRDragPunteroPos.posY = newRMouseDragPosY;
}

void WControlMouseable::setOnRMouseDrag (WControlMouseable::OnRMouseDrag cbOnRMouseDrag)
{
	this->cbOnRMouseDrag = cbOnRMouseDrag;
}
