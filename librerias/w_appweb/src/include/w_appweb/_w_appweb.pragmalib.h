/*********************************************************************************************
 * Name			: _w_appweb.pragmalib.h
 * Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_APPWEB_PRAGMALIB_H_
#define _W_APPWEB_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_base/_pragmalib_base.h"

// Propia librer�a: (para no autoengancharla si es una dll)
#pragma comment(lib, "w_appweb" FIN_LIB)

// Dependencias
#include "w_web/_w_web_server.pragmalib.h"
#include "w_web/_w_web_protocol.pragmalib.h"
#include "w_web/_w_web_services.pragmalib.h"
#include "w_arbol/_w_arbolxml.pragmalib.h"

#endif

#undef _W_APPWEB_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_APPWEB_PRAGMALIB_H_
