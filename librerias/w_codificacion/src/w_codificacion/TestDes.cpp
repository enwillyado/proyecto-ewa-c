/******************************************************************************
 *	dprotdll_secstr.c
 *		Funciones para cifrar/descifrar cadenas con longitud integrada
 *
 *	Author:
 * 		Ignacio Pomar Ballestero
 *
 *	Copyright:
 *		(C) 2006 DATISA (http://www.datisa.es)
 *****************************************************************************/
#include <stdlib.h>
#include <string.h>
//#include "dprotdll_des.h"
#include "w_codificacion/w_codificacion.h"


/******************************************************************************
 * \brief		Transforma un caracter a decimal
 * \author		Ignacio Pomar Ballestero
 * \return		Devuelve el valor en decimal de un caracter escrito en hexadecimal
 * \param	[in]	c	Caracter que se transformar�
 * \since		20061205
 * \version 	20061205
 *****************************************************************************/
int HexaToInt (char c)
{
	if ((c >= 'A') && (c <= 'F'))
	{
		return c - 'A' + 10;
	}
	else if ((c >= 'a') && (c <= 'f'))
	{
		return c - 'a' + 10;
	}
	else
	{
		return c - '0';
	}
}

/******************************************************************************
 * \brief		Descifra una cadena con triple DES que tiene al principio la longitud
 * \author		Ignacio Pomar Ballestero
 * \return		Devuelve 0 en caso de no haber podido realizar la operaci�n. Si ha sido exito devuelve lo ocupaba en memoria
 * \param	[out]	BufferDestino	Buffer en el que se almacenar� la cadena
 * \param	[in]	Capacidad		Tama�o que tiene el buffer para almacenar
 * \param	[in]	BufferCifrado	Buffer a descifrar
 * \param	[in]	Key1			Llave 1 del cifrado
 * \param	[in]	Key2			Llave 2 del cifrado
 * \since		20061205
 * \version 	20061205
 *****************************************************************************/
extern "C"
long Descifra (unsigned char* BufferDestino, unsigned long int Capacidad, unsigned char* BufferCifrado, unsigned char* Key1, unsigned char* Key2)
{
	long iLongitudBuffer = 0;
	long iValRetorno;
	int iLongitudRecorrida = 6; //M�xima cantidad soportada

	while ((BufferCifrado[0] != '@') && (iLongitudRecorrida)) //Avanzamos hasta que empieza la cadena cifrada
	{
		iLongitudBuffer *= 16;
		iLongitudBuffer += HexaToInt (BufferCifrado[0]);
		BufferCifrado++;
		iLongitudRecorrida--;
	}
	if ((iLongitudRecorrida) && (iLongitudRecorrida < 6)) //Comprobamos que la cuenta es valida
	{
		BufferCifrado++;

		Capacidad--; //Espacio para el caracter 0

		if (EncryptOk == DesDesencryptBuffer (BufferCifrado, iLongitudBuffer, BufferDestino, (size_t*)&Capacidad,
											  Key1, Key2, 1, DesMode3Des, DesConfigChainBlockCipher))
			//if (McbDoDES2(BufferCifrado,iLongitudBuffer,Key1,Key2,BufferDestino,&Capacidad,DES_DECRYPT,1,3,1))
		{
			iValRetorno = Capacidad;
		}
		else
		{
			iValRetorno = 0;
		}
		BufferDestino[Capacidad] = 0;
	}
	else
	{
		iValRetorno = 0;
	}

	return iValRetorno;
}



/******************************************************************************
 * \brief		Cifra una cadena con triple DES colocando al principio la longitud
 * \author		Ignacio Pomar Ballestero
 * \return		Devuelve 0 en caso de no haber podido realizar la operaci�n
 * \param	[out]	BufferDestino	Buffer en el que se almacenar� la cadena
 * \param	[in]	Capacidad		Tama�o que tiene el buffer para almacenar
 * \param	[in]	BufferCifrado	Buffer a cifrar
 * \param	[in]	Longitud		Cantidad de Bytes a cifrar
 * \param	[in]	Key1			Llave 1 del cifrado
 * \param	[in]	Key2			Llave 2 del cifrado
 * \since		20070124
 * \version 	20070124
 *****************************************************************************/
int CifraRaw (unsigned char* BufferDestino, unsigned long int Capacidad, unsigned char* Buffer, unsigned long int Longitud, unsigned char* Key1, unsigned char* Key2)
{
	char achBuff [10];
	int iDigito = 0;
	int iPosicion = 0;
	int iEsEspacioSuficiente;
	unsigned long int ulTemporal;
	unsigned long int ulEspacioUsado;
	unsigned char* pchBufferCifrado;

	//Ciframos en un nuevo buffer para obtener lo que ocupa
	ulEspacioUsado = Capacidad;
	pchBufferCifrado = (unsigned char*) malloc (sizeof (unsigned char) * Capacidad);

	//iEsEspacioSuficiente=McbDoDES2(Buffer,Longitud,Key1,Key2,pchBufferCifrado,&ulEspacioUsado,DES_ENCRYPT,1,3,1);
	if (EncryptOk == DesEncryptBuffer (Buffer, Longitud, pchBufferCifrado, (size_t*)&ulEspacioUsado, Key1, Key2, 1, DesMode3Des, DesConfigChainBlockCipher))
	{
		iEsEspacioSuficiente = 1;
	}
	else
	{
		iEsEspacioSuficiente = 0;
	}

	if (!iEsEspacioSuficiente) // No cupo en el buffer, nos saltamos el resto de la funcion
	{
		free (pchBufferCifrado);
		return 0;
	}

	//Colocamos en hexadecimal lo que ocupa la cadena cifrada
	achBuff[iPosicion++] = '@';
	ulTemporal = ulEspacioUsado;
	while (ulTemporal)
	{
		iDigito = ulTemporal % 16;
		ulTemporal = ulTemporal / 16;
		if (iDigito < 10)
		{
			achBuff[iPosicion++] = '0' + iDigito;
		}
		else
		{
			achBuff[iPosicion++] = 'A' - 10 + iDigito;
		}
	}

	//Rellenamos la cadena final, si cabe
	Capacidad -= iPosicion;
	if (Capacidad >= ulEspacioUsado)
	{
		while (iPosicion > 0)
		{
			BufferDestino[0] = achBuff[--iPosicion];
			BufferDestino++;
		}
		memcpy (BufferDestino, pchBufferCifrado, ulEspacioUsado);
	}
	else
	{
		iEsEspacioSuficiente = 0;    //no cabe la cadena
	}

	free (pchBufferCifrado);
	return iEsEspacioSuficiente;
}

/******************************************************************************
 * \brief		Cifra una cadena con triple DES colocando al principio la longitud
 * \author		Ignacio Pomar Ballestero
 * \return		Devuelve 0 en caso de no haber podido realizar la operaci�n
 * \param	[out]	BufferDestino	Buffer en el que se almacenar� la cadena
 * \param	[in]	Capacidad		Tama�o que tiene el buffer para almacenar
 * \param	[in]	BufferCifrado	Buffer a cifrar
 * \param	[in]	Key1			Llave 1 del cifrado
 * \param	[in]	Key2			Llave 2 del cifrado
 * \since		20061205
 * \version 	20070124
 *****************************************************************************/
extern "C"
int Cifra (unsigned char* BufferDestino, unsigned long int Capacidad, unsigned char* Buffer, unsigned char* Key1, unsigned char* Key2)
{
	return CifraRaw (BufferDestino, Capacidad, Buffer, (unsigned long)strlen ((char*)Buffer), Key1, Key2);
}

