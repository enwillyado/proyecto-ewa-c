/*********************************************************************************************
 * Name			: w_lienzo_jpeg.cpp
 * Description	: Implementación de la clase para cargar lienzos en JPEG
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_lienzo_jpeg.h"

#include "w_graficos/w_rgba.h"
#include "w_graficos/jpeg/jpeg.h"

#include "w_base/fromFile.hpp"

WLienzoJPEG::WLienzoJPEG (const std::string & filename)
{
	if (filename != "")
	{
		this->leerContenidoFile (filename);
	}
}
bool WLienzoJPEG::leerContenidoFile (const std::string & filename)
{
	const std::string & buffer = ::leerContenidoFile (filename);
	return this->leerContenidoStr (buffer);
}

bool WLienzoJPEG::leerContenidoStr (const std::string & buffer)
{
	// Apertura del archivo
	Picture pic;
	const bool retOpen = pic.processStr (buffer);

	// Comprobar si se pudo cargar el archivo
	if (false == retOpen)
	{
		return false;
	}

	// Volcar los pixeles al lienzo
	this->WLienzo::reserve (pic.getwidth(), pic.getheight());
	for (size_t y = 0; y < this->WLienzo::getAlto(); y++)
	{
		for (size_t x = 0; x < this->WLienzo::getAncho(); x++)
		{
			const RGBA & rgba = pic.getpixel (x, y);
			this->WLienzo::setPixel (x, y, rgba);
		}
	}

	// Cierro el archivo
	// NOTA: auto-destructor

	// Todo ha ido bien
	return true;
}

WLienzoJPEG WLienzoJPEG::getWLienzoJPEGFromFile (const std::string & filename)
{
	WLienzoJPEG ret (filename);
	return ret;
}

WLienzoJPEG WLienzoJPEG::getWLienzoJPEGFromStr (const std::string & buffer)
{
	WLienzoJPEG ret;
	ret.leerContenidoStr (buffer);
	return ret;
}
/*
void WLienzoJPEG::save (const std::string & filename)
{
}
*/
