/*********************************************************************************************
 *	Name		: w_make.h
 *	Description	: Funciones para preparar soluciones
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_MAKE_H_
#define _W_MAKE_H_

#include "w_make/w_make_parametros.h"

#include <string>

class WMake
{
public:
	// Constructores
	WMake();

	// Tipos internos
	enum TipoOrden
	{
		DESCONOCIDO = -1,
		// Externos
		SOLUCION,		//< .sln
		PROYECTO,		//< .vcxproj

		// Propios
		WMAKE,			//< .wmake
		ORDEN,			//< orden nativa
	};
	enum Estado
	{
		OMITIDO = -1,
		CORRECTO = 0,
		NO_EXISTE_FICHERO,
		NO_EXISTE_ORDEN,
		ORDEN_MAL_FORMADA,
	};

	// Modificadores
	void setFicheroOrden (const std::string & ficheroOrden);

	// Observadores
	TipoOrden getTipoOrden() const;

	// Ejecutores
	Estado preparar();		// prepara la orden (gen�rico)

	Estado prepararSln();	// prepara la soluci�n
	Estado prepararPro();	// prepara el proyecto
	Estado prepararWMK();	// prepara el wmake

	// Ejecutores
	Estado prepararObj();	// de fuente a obj
	Estado prepararLib();	// de objeto a lib
	Estado prepararDll();	// de objeto a dll
	Estado prepararExe();	// de objeto a exe

private:
	// �rea de datos privada
	TipoOrden tipoOrden;
	std::string ficheroOrden;

	// Par�metros
	WMakeParametros parametros;
};

#endif // _W_MAKE_H_
