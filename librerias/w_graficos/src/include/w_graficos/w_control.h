/*********************************************************************************************
 * Name			: w_control.h
 * Description	: Clase para gestionar un control con parte gr�fica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_CONTROL_H_
#define _W_CONTROL_H_

#include "w_graficos/w_lienzo.h"

#include "w_graficos/w_icontrol.h"
#include "w_graficos/w_controlBase.h"
#include "w_graficos/w_controlMouseable.h"
#include "w_graficos/w_controlJerarquizable.h"
#include "w_graficos/w_controlKeyable.h"

#include <string>
#include <vector>

class WControl :
	public IWControl,
	public WControlMouseable, public WControlJerarquizable, public WControlKeyable
{
public:
	// Constructores
	WControl (const std::string & iName = "");

	// Operadores
	void operator= (const WControl & org);

	// Accesores
	WLienzo & getLienzo();
	const WLienzo & getLienzo() const;
	void setLienzo (const WLienzo & lienzo);

	size_t getAlto() const;
	size_t getAncho() const;

	size_t getPosX() const;
	size_t getPosY() const;

	// Modificadores
	virtual void setPos (const size_t posX, const size_t posY);
	virtual void setPosX (const size_t posX);
	virtual void setPosY (const size_t posY);

	// Eliminar el control y eventos suscritos
	virtual void clear();

	// Manteniendo contenido (redimensionar)
	virtual void resize (const size_t ancho, const size_t alto);

	// Cambiar tama�o (recortar/alargar)
	virtual void setSize (const size_t ancho, const size_t alto);

	virtual void pintar();
	virtual WLienzo & recomponerLienzo();

	// Observadores
	virtual bool esApuntadoPor (const WCoordenada & control) const;
	virtual bool esApuntadoPor (const WControl & control) const;
	virtual bool leSobreVuela (const WControl & control) const;	//< ASK: �esCruzadoPor?
	virtual bool leInterseca (const WControl & control) const;	//< ASK: �esCubiertoPor?

public:
	// Identificador (propio)
	virtual WControl & getSelf();
	virtual const WControl & getSelf() const;

	virtual const std::string & getName() const;

protected:
	// �rea de datos privada
	const std::string name;
	size_t xPos;
	size_t yPos;
	WLienzo lienzo;
};

typedef std::vector<WControl> WControles;

struct WControlPair
{
	WControl* ptr;
	bool deletable;
};
typedef std::vector<WControlPair> WControlesPtr;

#endif // _W_XLIENZO_H_
