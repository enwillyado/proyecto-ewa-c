#ifndef _ABU_H_
#define _ABU_H_

class ABU
{
public:
	// constructor
	ABU();
	const bool accesoRegistros (const bool K, const bool Z, const bool U, const bool V, const bool W);
	void ejecutaABU();

protected:
	const bool abu_or (const bool, const bool);
	const bool abu_nor (const bool, const bool);
	const bool abu_nxor (const bool, const bool);
	const bool abu_nand (const bool, const bool);
	const bool abu_not (const bool);
	const bool abu_xor (const bool, const bool);
	const bool abu_and (const bool, const bool);

private:
	bool O;
	bool P;
	bool X;
	bool Y;
	bool N;
	bool M;
};

#endif	//_ABU_H_
