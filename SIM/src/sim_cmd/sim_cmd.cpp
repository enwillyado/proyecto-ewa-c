/*********************************************************************************************
 *	Name		: sim_cmd.cpp
 *	Description	: Ejecutable que pone en funcionamiento un SIM
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "sim/_sim_cmd.pragmalib.h"

#include "sim/sim.h"

// ----------------------------------------------------------------------------
/**
 *	Función principal
**/
int main (int argc, char* argv[])
{
	int ret = 0;

	// DECLARACIÓN DEL SIM
	Sim sim;

	// INICIALIZACIÓN DEL SIM
	sim.iniciaSistemaInformatico();
	sim.cargaGestorDeArranque ("../testdata/gestordearranque.txt");

	// EJECUCIONES DE LOS CUANTOS
	for (size_t i = 0; i < TMP_CUANTO; i++)
	{
		sim.cuanto();
	}

	return ret;
}

