/*********************************************************************************************
 *	Name		: ewadrones_server.cpp
 *	Description	: Ejecutable donde se implementa el servidor de control para JUMPING SUMO
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_ewadrones_server.pragmalib.h"

#include "ewadrones_control_globales.h"
#include "ewadrones_server.hpp"

// Programa principal
int main (int argc, char* argv[])
{
	// Ajustar los manejadores de diferentes se�ales
	::GetJumpingSumo().setOnNewImageJPEG (&ServidorWeb::onNewJPEG);
	//::GetJumpingSumo().setOnBateriaCambia (CosasAPP::MostrarBateria);

	// Iniciar el JUMPING SUMO
	::GetJumpingSumo().init();
	::GetJumpingSumo().start();

	// Iniciar servidor web
	ServidorWeb::mainServidorWeb();

	// Finalizar correctamente
	return 0;
}

