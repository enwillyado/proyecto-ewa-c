/*********************************************************************************************
 * Name			: w_vScrollControl.h
 * Description	: Clase para gestionar un control de scroll vertical (nativo)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_V_SCROLL_CONTROL_H_
#define _W_V_SCROLL_CONTROL_H_

#include "w_graficos/w_multicontrol.h"
#include "w_graficos/w_controlScrollable.h"

#include <string>

// ----------------------------------------------------------------------------
// Base para los controles internos
class WTimer;			//< FWD
class WVScrollControl;	//< FWD
class WBotonScrollBaseControl : public WControl
{
public:
	// Constructores
	WBotonScrollBaseControl (const std::string & iName = "");

	// Modificador
	void setVScrolControl (WVScrollControl* const iVScroll);

protected:
	WVScrollControl* vScroll;
};

// ----------------------------------------------------------------------------
// Controles internos
class WBotonScrollControl : public WBotonScrollBaseControl
{
public:
	// Constructores
	WBotonScrollControl (const std::string & iName = "");

	// Manejadores
	virtual void onLDownMouseOver();
	virtual void onMDownMouseOver();
	virtual void onRDownMouseOver();
	//
	virtual void onXDownMouseOver();

	virtual void onLUpMouseOver();
	virtual void onMUpMouseOver();
	virtual void onRUpMouseOver();
	//
	virtual void onLUpMouseOut();
	virtual void onMUpMouseOut();
	virtual void onRUpMouseOut();
	//
	virtual void onXUpMouse();

	virtual void onMoveMouseEnter();
	virtual void onMoveMouseOut();

protected:
	virtual void onScroll();

private:
	virtual void setTimerScroll (const bool encendido = true);
	WTimer* timer;
};

class WZonaScrollControl : public WBotonScrollBaseControl
{
public:
	// Constructores
	WZonaScrollControl (const std::string & iName = "");

	// Modificador
	virtual void resize (const size_t ancho, const size_t alto);

	// Manejadores
	virtual void onLClicMouseOver();
	virtual void onMClicMouseOver();
	virtual void onRClicMouseOver();

	virtual void onXClicMouseOver();
};

// ----------------------------------------------------------------------------
// Controles externo (final)
class WVScrollControl : public WMultiControl, public WControlScrollable
{
public:
	// Constructores
	WVScrollControl (const std::string & iName = "");
	WVScrollControl (const WVScrollControl & org);

	// Inicializador
	void init();

	// Modificadores
	virtual void resize (const size_t ancho, const size_t alto);

	// Observadores virtuales
	virtual WControl & getSelf();
	virtual const WControl & getSelf() const;

	// Manejadores
	virtual void onWheelMouseOver (const WEventoMouseWheel::Direccion);

private:
	WZonaScrollControl areaScroll;
	WBotonScrollControl botonArriba;
	WBotonScrollControl botonAbajo;
	float relativePos;		//< rango [0 - 1]
};

#endif // _W_V_SCROLL_CONTROL_H_
