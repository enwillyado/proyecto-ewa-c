/*********************************************************************************************
 * Name			: w_ar_drone_xml.h
 * Description	: Clase para el gestor de mensajes XML
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_AR_DRONE_XML_H_
#define _W_AR_DRONE_XML_H_

#include "w_red/w_red_cliudp.h"
#include "w_sistema/w_sistema_thread.h"

#include <cstring>				//< NULL
#include <string>

// FWD
class EWadrones;
class ArbolXML;

class ArDroneXML
{
public:
	ArDroneXML (EWadrones* = NULL);
	void setPtrArDrone (EWadrones*);

	// Procesamiento
	void procesar (const std::string & xml);
	void procesar (const ArbolXML &);

private:
	EWadrones* arDrone;
};

#endif // _W_AR_DRONE_XML_H_
