/*********************************************************************************************
 * Name			: w_app_functores.cpp
 * Description	: Implementaci�n de los m�todos para la colecci�n de funtores
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_app_functores.h"

// ----------------------------------------------------------------------------
// M�todos propios
#include "w_graficos/w_eventoKey.h"
#include "w_graficos/w_eventoMouse.h"
#include "w_graficos/w_eventoMouseDrag.h"

#include <string>
#include <map>

const AppFunctores & AppFunctores::GetNull()
{
	static const AppFunctores ret;
	return ret;
}


AppFunctores::FunctorOn AppFunctores::getFunctorOn (const std::string & id) const
{
	const AppFunctores::FunctoresWEvento::const_iterator & itr = this->functoresWEvento.find (id);
	if (itr != this->functoresWEvento.end())
	{
		return itr->second;
	}
	return NULL;
}
void AppFunctores::add (const std::string & id, const AppFunctores::FunctorOn & functor)
{
	this->functoresWEvento[id] = functor;
}


AppFunctores::FunctorOnKey AppFunctores::getFunctorOnKey (const std::string & id) const
{
	const AppFunctores::FunctoresWEventoKey::const_iterator & itr = this->functoresWEventoKey.find (id);
	if (itr != this->functoresWEventoKey.end())
	{
		return itr->second;
	}
	return NULL;
}
void AppFunctores::add (const std::string & id, const AppFunctores::FunctorOnKey & functor)
{
	this->functoresWEventoKey[id] = functor;
}


AppFunctores::FunctorWEventoMouse AppFunctores::getFunctorWEventoMouse (const std::string & id) const
{
	const AppFunctores::FunctoresWEventoMouse::const_iterator & itr = this->functoresWEventoMouse.find (id);
	if (itr != this->functoresWEventoMouse.end())
	{
		return itr->second;
	}
	return NULL;
}
void AppFunctores::add (const std::string & id, const AppFunctores::FunctorWEventoMouse & functor)
{
	this->functoresWEventoMouse[id] = functor;
}


AppFunctores::FunctorWEventoMouseDrag AppFunctores::getFunctorWEventoMouseDrag (const std::string & id) const
{
	const AppFunctores::FunctoresWEventoMouseDrag::const_iterator & itr = this->functoresWEventoMouseDrag.find (id);
	if (itr != this->functoresWEventoMouseDrag.end())
	{
		return itr->second;
	}
	return NULL;
}
void AppFunctores::add (const std::string & id, const AppFunctores::FunctorWEventoMouseDrag & functor)
{
	this->functoresWEventoMouseDrag[id] = functor;
}
