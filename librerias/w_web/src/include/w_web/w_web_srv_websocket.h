/*********************************************************************************************
 *	Name		: w_web_srv_websocket.h
 *	Description	: Servidor TCP para WebSockets
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_SRVTCP_WEBSOCKET_H_
#define _W_WEB_SRVTCP_WEBSOCKET_H_

#include "w_web/w_web_srvtcp.h"
#include <set>

class ServidorWebSockets : public ServidorWebTCP
{
public:
	// TODO: m�s constructores
	ServidorWebSockets();

protected:
	// Cola de eventos incompletos
	typedef std::set<SocketID> MapaSocketsAbiertos;
	MapaSocketsAbiertos socketsAbiertos;

	// Recepci�n
	virtual bool analizaEventoMsgRecv (const EventoServidorTCP &);
	virtual Mensaje responderMsgSokRecv (const EventoServidorTCP &, const Mensaje &);

	virtual std::string procesaMsgSokRecv (const EventoServidorTCP &);
	virtual void procesaMsgNoWebRecv (const EventoServidorTCP &);

	// Emisi�n
	virtual void procesaMsgSokEnv (const EventoServidorTCP &, const std::string & respuesta);
};

#endif	//_W_WEB_SRVTCP_WEBSOCKET_H_
