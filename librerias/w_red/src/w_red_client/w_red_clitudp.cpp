/*********************************************************************************************
 *	Name		: _w_red_cliudp.cpp
 *	Description	: Implementaci�n del cliente que se conecta por UDP con el Servidor
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/

#include "w_red/w_red_cliudp.h"

#include <stdlib.h>					//< para el atoi(...)

#include "w_base/toStr.hpp"			//< para tener el toStr

// Para los sockets 'est�ndares'
#ifdef WIN32
#include <ws2tcpip.h>				//< en WIN32, est�n aqu�
#pragma comment(lib, "ws2_32.lib")	//< y se enlazan con esta librer�a
#else
#include <errno.h>					//< para control de errores
#include <sys/time.h>
#include <sys/socket.h>				//< en UNIX est�n aqu�
#include <sys/types.h>				//< TODO: y enlazan con "-lsocket"
#include <unistd.h>					//< para "close(...)"
#include <netdb.h>					//< para "getaddrinfo(...)"
#include <string.h>					//< para "memset(...)"
#endif

// Definiciones para la gesti�n del socket
#define DESCRIPTOR_NULO 		-1		// valor que indica que un descriptor (socket, file...) es nulo
#define RECV_MENSAJE_MAX_SIZE	65507	// longitud del buffer que se usar� para recibir un mensaje por el socket

// Declaraci�n de la funci�n que obtiene mensajes con tiempo de espera:
static int recvtimeout (int s, char* buf, int len, int timeout);
#define SOCKET_ERROR_SIGNAL		-1		// valor que retornar� "recvtimeout(...)" en caso de error gen�rico
#define SOCKET_TIMEOUT_SIGNAL	-2		// valor que retornar� "recvtimeout(...)" en caso de que termine el tiempo de espera
#define SOCKET_UNKNOWN_SIGNAL	-3		// valor que retornar� "recvtimeout(...)" en caso de error desconocido

ClienteUDP::ClienteUDP()
{
	this->sockfd = DESCRIPTOR_NULO;
	this->lastError = ClienteUDP::SOCKET_DESCONECTADO;
}

ClienteUDP::~ClienteUDP()
{
	if (this->isOpenSocket())
	{
		this->closeSocket();
	}
}

//---------------------------------------------------------------------------
// Env�o a alto nivel de los mensajes:

/**
*	Env�a un mensaje y espera a recibir una respuesta (TODO: completa)
*	\param [in]	mensajeEnviar	mensaje como string que se enviar�
*	\param [in]	respuesta		mensaje como string que se ha recibido
*	\retval	TRUE si se ha podido enviar y recibir los mensajes
 *		Con "GetError()" se obtiene el error que se produjo
**/
bool ClienteUDP::enviar_recibir (const std::string & mensajeEnviar, std::string & mensajeRespuesta, unsigned int segundosEsperados)
{
	// Enviar el mensaje generado:
	if (this->enviar (mensajeEnviar) == false)
	{
		return false;	// error (ya deja puesto "ERROR_AL_CONNECT_SCKT", etc.)
	}
	// Recibir el mensaje esperado:
	if (this->recibir (mensajeRespuesta, segundosEsperados) == false)
	{
		return false;
	}
	return true;
}

/**
 *	Funci�n que obtiene el �ltimo error ocurrido.
 *	\retval	el �ltimo valor ocurrido, ya convertido a "error" b�sico
**/
ClienteUDP::Errores ClienteUDP::getLastError() const
{
	return this->lastError;
}
/**
 *	Funci�n que obtiene el �ltimo error ocurrido como string
 *	\retval	el �ltimo valor ocurrido, ya convertido a "error" b�sico
**/
std::string ClienteUDP::getLastErrorStr() const
{
	return ClienteUDP::toStr (this->lastError);
}
/**
 *	Funci�n que obtiene el �ltimo error ocurrido como string
 *	\retval	el �ltimo valor ocurrido, ya convertido a "error" b�sico
**/
std::string ClienteUDP::toStr (ClienteUDP::Errores error)
{
	switch (error)
	{
	// Errores del socket:
	case SOCKET_USADO:
		return "SOCKET_USADO";
	case SOCKET_DISPONIBLE:
		return "SOCKET_DISPONIBLE";
	case SOCKET_CONECTANDO:
		return "SOCKET_CONECTANDO";
	case SOCKET_DESCONECTANDO:
		return "SOCKET_DESCONECTANDO";
	case SOCKET_SIN_CAMBIOS:
		return "SOCKET_SIN_CAMBIOS";
	case SOCKET_ESPERANDO_RESPUESTA:
		return "SOCKET_ESPERANDO_RESPUESTA";
	case SOCKET_RESET_BY_PEER:
		return "SOCKET_RESET_BY_PEER";
	case SOCKET_ERROR_FATAL:
		return "SOCKET_ERROR_FATAL";
	// Errores al enviar por un socket:
	case SOCKET_ERROR_ENVIAR_DESC:
		return "SOCKET_ERROR_ENVIAR_DESC";
	// Errores al recibir por un socket:
	case SOCKET_RECV_TIMEOUT:
		return "SOCKET_RECV_TIMEOUT";
	case SOCKET_RECV_TIMEOUT_ERROR:
		return "SOCKET_RECV_TIMEOUT_ERROR";
	case MENSAJE_MAL_FORMADO:
		return "MENSAJE_MAL_FORMADO";
	// Errores al cerrar un socket:
	case ERROR_AL_LIBERAR_WINSOCK:
		return "ERROR_AL_LIBERAR_WINSOCK";
	// Errores al abrir un socket:
	case ERROR_AL_TRANSFORMAR_PUERTO:
		return "ERROR_AL_TRANSFORMAR_PUERTO";
	case ERROR_AL_TRADUCIR_DNS:
		return "ERROR_AL_TRADUCIR_DNS";
	case ERROR_AL_CREAR_SCKT:
		return "ERROR_AL_CREAR_SCKT";
	case ERROR_AL_CONNECT_SCKT:
		return "ERROR_AL_CONNECT_SCKT";
	case ERROR_HACKER:
		return "ERROR_HACKER";
	case ERROR_DESCONOCIDO:
		return "ERROR_DESCONOCIDO";

		// NOTA: No hay caso 'default' para que, revisando los warns de compilaci�n, se pueda
		//			detectar nuevos casos de error que no est�n puestos humanamente legibles
	}
	return "Error Desconocido";
}


/**
 *	Obtiene el puerto por el que est� o estaba conectado el socket cliente la �ltima vez
 *	\retval	el puerto por el que est� o estaba conectado; DESCRIPTOR_NULO si no se indic� nunca
**/
int ClienteUDP::getPort() const
{
	if (this->portActive == "")
	{
		return DESCRIPTOR_NULO;
	}
	return ::atoi (this->portActive.c_str());
}
/**
 *	Obtiene el puerto por el que est� o estaba conectado el socket cliente la �ltima vez
 *	\retval	el puerto por el que est� o estaba conectado como string; "" si no se indic� nunca
**/
std::string ClienteUDP::getPortStr() const
{
	return this->portActive;
}

/**
 *	Obtiene el host al que est� o estaba conectado el socket cliente la �ltima vez
 *	\retval	el host al que est� o estaba conectado la �ltima vez; "" si no se indic� nunca
**/
std::string ClienteUDP::getHost() const
{
	return this->hostActive;
}

//---------------------------------------------------------------------------
// Gesti�n del socket que mantiene la conexi�n UDP:
/**
 *	Env�a un mensaje cueste lo que cueste
 *	\param	[in]	mensaje		 mensaje a enviar
 *	\retval	TRUE si se consigui� enviar; y FALSE en case contrario
 *		Con "GetError()" se obtiene el error en concreto que se produjo
**/
bool ClienteUDP::enviar (const std::string & mensaje)
{
	// Abrir el socket (no har� nada y continuar� si ya est� abierto):
	if (this->openSocket (this->hostActive, this->portActive) == false)
	{
		// no se puedo abrir (no se contempla que sea diferente host/posrt al que deber�a)
		return false;
	}
	// Enviar el mensaje generado por el socket abierto:
	if (this->sendMsgSocket (mensaje) == false)
	{
		return false;
	}
	return true;
}

/**
 *	Obtiene un mensaje del socket activo (si lo est�, que deber�a estarlo siempre)
 *	\param	[out]	mensaje		 mensaje recibido
 *	\retval	TRUE si se recibi� algo; o FALSE en case contrario
 *		Con "GetError()" se obtiene el error en concreto que se produjo
**/
bool ClienteUDP::recibir (std::string & mensaje, unsigned int segundosEsperados)
{
	// Recibir un mensaje: (sin timeout)
	if (this->recvMsgSocket (mensaje, segundosEsperados) == false)
	{
		return false;
	}
	return true;
}

//---------------------------------------------------------------------------
// Gesti�n de eventos recibidos en la capa del socket
/**
 *	Obitene el �ltimo evento recibido, o el evento nulo si no hay
 *	\retval	el evento que se recibe y lo elimina de la cola de eventos pendientes
**/
EventoClienteUDP ClienteUDP::getEvento()
{
	if (this->colaEventos.size())
	{
		EventoClienteUDP evento = this->colaEventos.front();
		this->colaEventos.pop();
		return evento;
	}
	return EventoClienteUDP();
}

/**
 *	Obitene el n�mero de eventos que a�n est�n pendientes de procesar
 *	\retval	el n�mero esperado
**/
size_t ClienteUDP::getNumEventosPendintes()
{
	return this->colaEventos.size();
}

/**
 *	Procesa un evento nuevo. Por defecto, lo encola y luego llama
 *		al callBack si est� definido; si no, intenta procesarlo como buenamente pueda
 *	\param	evento	[in]	evento que se va a procesar
**/
void ClienteUDP::analizaEvento (const EventoClienteUDP & evento)
{
	if (eventCallBack != NULL)
	{
		this->colaEventos.push (evento);
		eventCallBack (*this);
	}
	else
	{
		switch (evento.tipo)
		{
		case EventoClienteUDP::MENSAJE_RECIBIDO:
		case EventoClienteUDP::MENSAJE_RECIBIDO_VACIO:
		default:
			return;
		}
	}
}


//---------------------------------------------------------------------------
// Gesti�n del socket que mantiene la conexi�n UDP:

// -- Funciones auxiliares para el control de sockets:
// get sockaddr, IPv4 or IPv6:
static void* get_in_addr (struct sockaddr* sa)
{
	if (sa->sa_family == AF_INET)
	{
		return & (((struct sockaddr_in*)sa)->sin_addr);
	}
	return & (((struct sockaddr_in6*)sa)->sin6_addr);
}

// get data with timeout

static int recvfromtimeout (int sock, char* data, int length, int timeoutinseconds, sockaddr_in* sockfrom, socklen_t* addr_len)
{
	fd_set socks;
	struct timeval t;
	FD_ZERO (&socks);
	FD_SET (sock, &socks);
	t.tv_sec = timeoutinseconds;
	t.tv_usec = 0;
	int n = select (-1, &socks, NULL, NULL, &t);
	if (n == 0)
	{
		return SOCKET_TIMEOUT_SIGNAL;  // timeout!
	}
	if (n == -1)
	{
		return SOCKET_ERROR_SIGNAL;  // error
	}
	return recvfrom (sock, data, length, 0, (sockaddr*)sockfrom, addr_len);
}

/**
 *	Intenta abrir un socket
 *	\param	[in]	host		nombre del dominio
 *	\param	[in]	port		puerto como entero sin signo
 *	\retval	TRUE si todo ha ido bien (se cre� el socket con los datos dados o ya exist�a con esos datos),
 *		o  FALSE en caso contrario: en "getLastError()" se puede encontrar el error que se produjo
 *	\see http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html#windows
 **/
bool ClienteUDP::openSocket (const std::string & host, unsigned int port)
{
	// convert port to string:
	const std::string & portString = ::toStr (port);
	if (portString == "")
	{
		lastError = ClienteUDP::ERROR_AL_TRANSFORMAR_PUERTO;
		return false; // problema al obtener el puerto
	}

	// Abrir el puerto
	return this->openSocket (host, portString);
}

/**
 *	Intenta abrir un socket
 *	\param	[in]	host		nombre del dominio
 *	\param	[in]	port		puerto como string
 *	\retval	TRUE si todo ha ido bien (se cre� el socket con los datos dados o ya exist�a con esos datos),
 *		o  FALSE en caso contrario: en "getLastError()" se puede encontrar el error que se produjo
 *	\see http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html#windows
**/
bool ClienteUDP::openSocket (const std::string & host, const std::string & port)
{
	if (this->isOpenSocket())
	{
		{
			// Si ya estaba abierto:
			if (host == this->hostActive && port == this->portActive)
			{
				lastError = ClienteUDP::SOCKET_DISPONIBLE;	// es un WARN que convertible en SOCKET_ACTIVO
				return true;
			}
			lastError = ClienteUDP::SOCKET_USADO;
			return false;
		}
		// Si est� abierto, supondremos que est� abierto al socket que queremos, para simplificar
		this->lastError = ClienteUDP::SOCKET_ACTIVO;
		return true;
	}
	// Abrir el socket, porque est� sin abrir:
#ifdef WIN32
	WSADATA wsaData;  // if this doesn't work
	//WSAData wsaData; // then try this instead
	// MAKEWORD(1,1) for Winsock 1.1, MAKEWORD(2,0) for Winsock 2.0:
	if (WSAStartup (MAKEWORD (2, 0), &wsaData) != 0)
	{
		return false;
	}
#endif
	struct addrinfo hints, *servinfo, *actServInfo;

	// Inicializar las estructuras
	memset (&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;			// IPv4
	hints.ai_socktype = SOCK_DGRAM;		// UDP

	// get info to next connection:
	if ((getaddrinfo (host.c_str(), port.c_str(), &hints, &servinfo)) != 0)
	{
		this->lastError = ClienteUDP::ERROR_AL_TRADUCIR_DNS;
		return false;
	}

	// loop through all the results and connect to the first we can
	// NOTA: se podr�a suponer que nunca se va a resolver el host en m�s de una m�quina
	for (actServInfo = servinfo; actServInfo != NULL; actServInfo = actServInfo->ai_next)
	{
		// create a socket
		if ((this->sockfd = socket (actServInfo->ai_family, actServInfo->ai_socktype, actServInfo->ai_protocol)) == -1)
		{
			this->lastError = ClienteUDP::ERROR_AL_CREAR_SCKT;
			continue;
		}
		// conecto to destination
		if (connect (this->sockfd, actServInfo->ai_addr, actServInfo->ai_addrlen) == -1)
		{
			this->sockfd = DESCRIPTOR_NULO;	// forzar el cierre
			this->lastError = ClienteUDP::ERROR_AL_CONNECT_SCKT;
			continue;
		}
		break;
	}
	if (actServInfo == NULL)
	{
		// si no se econtr� ning�n conector v�lido
		return false;
	}

	freeaddrinfo (servinfo); // all done with this structure

	// Y si llegamos aqu�, es que todo est� correcto (socket activo/conectado)
	this->hostActive = host;
	this->portActive = port;
	this->lastError = ClienteUDP::SOCKET_ACTIVO;
	return true;
}

/**
 *	Verifica que el socket est� abierto
 *	\retval	TRUE si est� abierto
 *		o  FALSE en caso contrario: en "getLastError()" se puede encontrar el estado actualizado
**/
bool ClienteUDP::isOpenSocket() const
{
	if (sockfd >= 0)
	{
		// si ese identificador es "v�lido" (mayor o igual que cero), est� abierto
		const_cast<ClienteUDP*> (this)->lastError = ClienteUDP::SOCKET_ACTIVO;
		return true; // HACK: se puede hilar m�s fino y garantizar de alguna manera que est� realmente abierto
	}
	// si no (menor que cero), est� desconectado
	const_cast<ClienteUDP*> (this)->lastError = ClienteUDP::SOCKET_DESCONECTADO;
	return false;
}

/**
 *	Abre un socket; previamente, si estaba abierto, lo cierra
 *	\retval	TRUE si finalmente queda abierto (TODO: o se puede considerar abierto)
 *		o  FALSE en caso contrario: en "getLastError()" se puede encontrar el estado actualizado
**/
bool ClienteUDP::reOpenSocket()
{
	if (this->isOpenSocket())
	{
		// Si ya estaba abierto:
		if (this->closeSocket() == false)	// lo intenta cerrar
		{
			// Si <NO> lo consigue, sale con error
			return false;
		}
	}
	// Se supone que estar� cerrado aqu�. TODO: �cerciorarse?
	return openSocket (this->hostActive, this->portActive);	// Y abrir con los datos anteriores (si los hubi�ramos guardado)
}

/**
 *	Env�a un mensaje dado por un socket PRESUMIBLEMENTE abierto
 *	\param	[in]	socketNumer	identificador del socket que se enviar�
 *	\param	[in]	mensaje		mensaje que se va a enviar
 *	\retval	TRUE si se envi� bien
 *		o  FALSE en caso contrario: en "getLastError()" se puede encontrar el error que se produjo
**/
bool ClienteUDP::sendMsgSocket (const std::string & mensaje)
{
	if (this->isOpenSocket())
	{
		int bytesSend = ::send (this->sockfd, mensaje.c_str(), mensaje.size(), 0);
		if (bytesSend == (int)mensaje.size())
		{
			// Fue bien:
			this->lastError = ClienteUDP::SOCKET_ACTIVO;
			return true;
		}
	}
	// El socket no estaba conectado:
	this->lastError = ClienteUDP::SOCKET_ERROR_ENVIAR_DESC;
	return false;
}

/**
 *	Intenta recibir un mensaje del socket (supuestamente abierto) en un tiempo dado
 *	\param	[out]				mensaje				Se deja el mensaje recibido; si se recibe "" es el que se cerr� el socket
 *	\param	[in]	opt. = 0	segundosEsperados	lapso de segundos que esperar�; con "0" indefinidos
 *	\retval	si se ha recibido correctamente un mensaje
**/
bool ClienteUDP::recvMsgSocket (std::string & mensaje, unsigned int segundosEsperados)
{
	if (this->isOpenSocket())
	{
		struct sockaddr_storage their_addr;
		socklen_t addr_len = sizeof their_addr;
		char msg [RECV_MENSAJE_MAX_SIZE];
		int bytes_rec;
		if (segundosEsperados > 0)
		{
			bytes_rec = recvfromtimeout (this->sockfd, msg, RECV_MENSAJE_MAX_SIZE, segundosEsperados, (struct sockaddr_in*)&their_addr, &addr_len);
		}
		else
		{
			bytes_rec = ::recvfrom (this->sockfd, msg, RECV_MENSAJE_MAX_SIZE, 0, (struct sockaddr*)&their_addr, &addr_len);
		}
		if (bytes_rec > 0)
		{
			// Se ha recibido un paquete...
			this->lastError = ClienteUDP::SOCKET_ACTIVO;
			mensaje = std::string (msg, bytes_rec);	// devolver el mensaje recibido a la longitud que tiene
			return true;
		}
		else if (bytes_rec == 0)
		{
			// it can be "0", and this can mean only one thing: remote has closed connection!
			// si no, es que se cerr� la conexi�n
			this->closeSocket();	// TODO: �dar� error cerrarlo as�?
			mensaje = "";
			return true;
		}
		else if (bytes_rec == SOCKET_TIMEOUT_SIGNAL)
		{
			if (segundosEsperados > 0)
			{
				// simplemente es un timeout!
				this->lastError = ClienteUDP::SOCKET_RECV_TIMEOUT;
				return false;	// TODO: �podr�a ser "true"?
			}
			// Si llegas aqu� eres un maldito HACKER; o un manazas ha tocado lo que devuelve 'recvtimeout(...)'.
			this->lastError = ClienteUDP::SOCKET_RECV_TIMEOUT_ERROR;
			return false;
		}
		else if (bytes_rec == SOCKET_ERROR_SIGNAL)
		{
			// it can be "-1", and this is a error
			this->lastError = ClienteUDP::SOCKET_ERROR_FATAL;
			// TODO: intentar obtener un error m�s preciso:
			this->obtainSysLastErrorSocket();
			const Errores tmp = this->lastError;	// guardar el estado actual del error que se produjo en "recvMsgSocket(...)"
			if (this->closeSocket() == false)	// cerrar el socket
			{
				// si no se pudo ni tan siquiera cerrar e socket:
				this->lastError = ClienteUDP::SOCKET_ERROR_FATAL;
				return false;	// este es un error completamente desastroso
			}
			this->lastError = tmp;		// restaurar el estado que produjo el error en "recvMsgSocket(...)"
			return false;
		}
		else	//< SOCKET_UNKNOWN_SIGNAL and others returns
		{
			// fatal error unknown
			this->lastError = ClienteUDP::ERROR_DESCONOCIDO;
			return false;
		}
	}
	this->lastError = SOCKET_DESCONECTADO;
	return false;
}
/**
 *	Intenta cerrar un socket ABIERTO
 *	\retval	TRUE si todo ha ido bien (se cerr� el socket o no estaba abierto; en ese caso avisa del "error")
 *				y "this->sockfd" pasar� a ser "DESCRIPTOR_NULO",
 *		o  FALSE en caso contrario: en "getLastError()" se puede encontrar el error que se produjo
 *				y "this->sockfd" mantendr� su valor
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms741549(v=vs.85).aspx (WSACleanup function (Windows))
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms737582(v=vs.85).aspx (closesocket function (Windows))
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740481(v=vs.85).aspx (shutdown  function (Windows))
**/
bool ClienteUDP::closeSocket()
{
	if (this->isOpenSocket())
	{
#ifdef WIN32
		if (shutdown (this->sockfd, SD_BOTH) == 0)
#else
		if (shutdown (this->sockfd, SHUT_RDWR) == 0)
#endif
		{
#ifdef WIN32
			if (closesocket (this->sockfd) == 0)
#else
			if (close (this->sockfd) == 0)
#endif
			{
				this->sockfd = DESCRIPTOR_NULO;
				this->lastError = SOCKET_DESCONECTADO;
#ifdef WIN32
				// The WSACleanup function terminates use of the Winsock 2 DLL (Ws2_32.dll).
				if (WSACleanup() != 0)
				{
					this->lastError = ERROR_AL_LIBERAR_WINSOCK;
					return false;
				}
#endif
				return true;
			}
		}
		// Si no se cerr� bien, mal vamos:
		this->lastError = SOCKET_ERROR_FATAL;
		// TODO: intentar obtener un error m�s preciso:
		this->obtainSysLastErrorSocket();
		return false;
	}
	this->lastError = SOCKET_SIN_CAMBIOS;
	return true;
}

/**
 *	Obtiene el �ltimo error generado por una operaci�n de comunicaci�n con la librer�a del socket
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740668(v=vs.85).aspx
**/
void ClienteUDP::obtainSysLastErrorSocket()
{
#ifdef WIN32
	int i = WSAGetLastError();
	switch (i)
	{
	case WSAECONNRESET:	// Connection reset by peer
		this->lastError = SOCKET_RESET_BY_PEER;	//< no hay peor error que no saber de qu� error se trata
		return;
	case WSAEADDRINUSE:	// Address already in use
	case WSAESHUTDOWN:	// Cannot send after socket shutdown
	case WSAENOTCONN:	// Socket is not connected

		return;
	}
#else
	switch (errno)	// \see http://www.ibm.com/developerworks/aix/library/au-errnovariable/
	{
	case EBADF:		// A function tried to use a bad file descriptor

		return;
	}
#endif
	this->lastError = SOCKET_ERROR_FATAL;	//< no hay peor error que no saber de qu� error se trata
}

/**
 *	Funci�n que traduce a su "error" b�sico el �ltimo error ocurrido.
 *		Esto sirve para que ciertos avisos que se producen cuando una acci�n sobre el socket
 *		se ha dado por buena (devolviendo 'true') se traduzcan a "SOCKET_ACTIVO".
 *	\retval	el �ltimo valor ocurrido, ya convertido a "error" b�sico
**/
ClienteUDP::Errores ClienteUDP::basicErrorSocket()
{
	switch (this->lastError)
	{
	//		case SOCKET_DISPONIBLE :	// este caso ya no se contempla
	case SOCKET_SIN_CAMBIOS :
		this->lastError = SOCKET_ACTIVO;
		break;
	default:
		break;
	}
	return this->lastError;
}
