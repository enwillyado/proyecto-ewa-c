/*********************************************************************************************
 * Name			: w_areaScrollControl.h
 * Description	: Clase para gestionar un multicontrol de �rea con scroll vertical
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_AREA_SCROLL_CONTROL_H_
#define _W_AREA_SCROLL_CONTROL_H_

#include "w_graficos/w_vScrollControl.h"
#include "w_graficos/w_multicontrol.h"

#include <string>

// ----------------------------------------------------------------------------
// Controles externo (final)
class WAreaScrollControl : public WMultiControl
{
public:
	// Constructores
	WAreaScrollControl (const std::string & iName = "");
	WAreaScrollControl (WAreaScrollControl & org);

	// Inicializador
	void init();

	// Modificadores
	virtual size_t insertarPtr (WControl* const control, const bool deletable = true);
	virtual void resize (const size_t ancho, const size_t alto);
	virtual WLienzo & recomponerLienzo();

	virtual void onMoveMouseOver()
	{
		WMultiControl::onMoveMouseOver();
		this->recomponerLienzo();
	}
	virtual void onMoveMouseOut()
	{
		WMultiControl::onMoveMouseOut();
		this->recomponerLienzo();
	}
private:
	WVScrollControl vScroll;
};

#endif // _W_AREA_SCROLL_CONTROL_H_
