/*********************************************************************************************
* Name			: w_red_tester_thread.cpp
* Description	: Pruebas unitarias que se realizar�n sobre hilos
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema.h"

#include <iostream>

// ----------------------------------------------------------------------------
// Execute Threads
int executeThreads()
{
	// Clase temporal que contendr� una funci�n que se ejecuta de forma indefinida
	class FuncionParaElHilo
	{
	public:
		static void functionDePrueba (const WThread & ptrData)
		{
			std::cout << "[HILO] Comienza la ejecucion del hilo 'functionDePrueba'..." << std::endl;
			while (ptrData.isStoped() == false)
			{
				std::cout << "[HILO] No hago nada mas que esto y esperar 300 milisegundos..." << std::endl;
				WTime::esperarMilisegundos (300);
			}
			std::cout << "[HILO] Finaliza la ejecucion del hilo 'functionDePrueba'..." << std::endl;
			std::cout.flush();
		}
	};

	// Vamos a probar un poco el tema de los hilos: ejecutar un hilo que recibe un dato templatizado y ejecuta una funci�n global
	WThread dsystemThread (FuncionParaElHilo::functionDePrueba);
	{
		// Lanzar el hilo
		const WThread::Estado result = dsystemThread.start();
		if (result != WThread::SIN_ERROR)
		{
			std::cerr << "start() [FALLO] (" << result << ")" << std::endl;
		}
		else
		{
			std::cout << "Hilo lanzado con exito." << std::endl;
		}
	}
	{
		// Ver que est� en ejecuci�n
		const bool result = dsystemThread.isStarted();
		if (result == false)
		{
			std::cerr << "isStarted(...) [FALLO] (" << result << ")" << std::endl;
		}
		else
		{
			std::cout << "Hilo sigue en ejecucion." << std::endl;
		}
	}
	WTime::esperarMilisegundos (3 * 1000); //< esperar 3 segundos
	{
		// Solicitar educadamente que se detenga
		const WThread::Estado result = dsystemThread.stop();
		if (result != WThread::SIN_ERROR)
		{
			std::cerr << "stop(...) [FALLO] (" << result << ")" << std::endl;
		}
		else
		{
			std::cout << "Solicitar al hilo que se detenga." << std::endl;
		}
	}
	while (true)	//< espera activa
	{
		// Ver que est� en ejecuci�n
		const bool result = dsystemThread.isStarted();
		if (result == false)
		{
			std::cout << "Hilo finalizo!" << std::endl;
			break; //salir de la espera activa
		}
		else
		{
			std::cout << "Hilo sigue en ejecucion; esperando..." << std::endl;
			WTime::esperarMilisegundos (1 * 1000); //< esperar 1 segundo
		}
	}
	return 0;
}
