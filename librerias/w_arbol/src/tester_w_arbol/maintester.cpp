/*********************************************************************************************
 *	Name		: maintester.cpp
 *	Description	: Este fichero sirve para configurar las pruebas unitarias que se realizarán
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_arbol_tester.pragmalib.h"

#include "w_arbol/w_arbolxml.h"
#include <assert.h>

int main()
{
	// Test incorrectos
	{
		const ArbolXML & ret = ArbolXML::getArbolFromSTR ("");
		assert (ret.getLastError() == ArbolXML::PARSER_ERROR);
		assert (ret.getClave() == "");
		assert (ret.hijosize() == 0);
	}
	{
		const ArbolXML & ret = ArbolXML::getArbolFromSTR ("pepito");
		assert (ret.getLastError() == ArbolXML::PARSER_ERROR);
		assert (ret.getClave() == "");
		assert (ret.hijosize() == 0);
	}

	// Test correctos
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<pep/>");
		assert (ret.getClave() == "pep");
		assert (ret.hijosize() == 0);
		assert (ret.atributosize() == 0);
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<pepito nombre='jose'/>");
		assert (ret.getClave() == "pepito");
		assert (ret.hijosize() == 0);
		assert (ret % "nombre" == "jose");
		assert (ret.atributosize() == 1);
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<pepito nombre='jos/e'/>");
		assert (ret.getClave() == "pepito");
		assert (ret.hijosize() == 0);
		assert (ret % "nombre" == "jos/e");
		assert (ret.atributosize() == 1);
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<pepito              nombre       =        'jose'       />");
		assert (ret.getClave() == "pepito");
		assert (ret.hijosize() == 0);
		assert (ret % "nombre" == "jose");
		assert (ret.atributosize() == 1);
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<pepito nombre='jose \"ramon\"'/>");
		assert (ret.getClave() == "pepito");
		assert (ret.hijosize() == 0);
		assert (ret % "nombre" == "jose \"ramon\"");
		assert (ret.atributosize() == 1);
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<pepito nombre='jo\\\\s\\\'e'/>");
		assert (ret.getClave() == "pepito");
		assert (ret.hijosize() == 0);
		assert (ret % "nombre" == "jo\\s'e");
		assert (ret.atributosize() == 1);
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<gente><pepito nombre='jose'/><maruja/><paco></paco></gente>");
		assert (ret.getClave() == "gente");

		assert (ret.hijosize() == 3);
		assert (ret.existeHijo ("pepito") == true);
		assert (ret.existeHijo ("maruja") == true);
		assert (ret.existeHijo ("paco") == true);

		assert (ret["pepito"] % "nombre" == "jose");
		assert (ret["pepito"].atributosize() == 1);
		assert (ret["pepito"].hijosize() == 0);
		assert (ret["maruja"].atributosize() == 0);
		assert (ret["maruja"].hijosize() == 0);
		assert (ret["paco"].atributosize() == 0);
		assert (ret["paco"].hijosize() == 0);
	}
	{
		ArbolXML ret =
			ArbolXML::getArbolFromSTR ("<mas><gente><pepito nombre='jose'/><maruja/><paco></paco></gente></mas>");
		assert (ret.getClave() == "mas");
		assert (ret.hijosize() == 1);

		assert (ret["gente"].hijosize() == 3);
		assert (ret["gente"].existeHijo ("pepito") == true);
		assert (ret["gente"].existeHijo ("maruja") == true);
		assert (ret["gente"].existeHijo ("paco") == true);

		assert (ret["gente"]["pepito"] % "nombre" == "jose");
		assert (ret["gente"]["pepito"].atributosize() == 1);
		assert (ret["gente"]["pepito"].hijosize() == 0);
		assert (ret["gente"]["maruja"].atributosize() == 0);
		assert (ret["gente"]["maruja"].hijosize() == 0);
		assert (ret["gente"]["paco"].atributosize() == 0);
		assert (ret["gente"]["paco"].hijosize() == 0);
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<mas>valor</mas>");
		assert ("valor" == ret.getValor());
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<mas>valor > con simbolos raros="" y cosas asi</mas>");
		assert ("valor > con simbolos raros="" y cosas asi" == ret.getValor());
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<mas>texto<gente></gente> y <gente></gente>mas</mas>");
		assert ("texto y mas" == ret.getValor());
	}
	{
		ArbolXML ret = ArbolXML::getArbolFromSTR ("<?cabecera?>residuo<xml>valor</xml>");
		assert (ret.getClave() == "xml");
		assert (ret.getValor() == "valor");
		assert (ret.hijosize() == 0);
		assert (ret.atributosize() == 0);
	}
	return 0;
}
