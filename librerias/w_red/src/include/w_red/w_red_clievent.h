/*********************************************************************************************
 *	Name		: dhttp_clievent.h
 *	Description	: Evento recibido por un Cliente TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _DHTTP_CLIEVENT_H_
#define _DHTTP_CLIEVENT_H_

#include "w_red/w_red_event.h"			//< para tener "EventoTCP"
#include <queue>

#include <string>

class EventoClienteTCP : public EventoTCP
{
public:
	EventoClienteTCP();
};

typedef std::queue<EventoClienteTCP> ColaEventosClienteTCP;

class EventoClienteUDP : public EventoUDP
{
public:
	EventoClienteUDP();
};

typedef std::queue<EventoClienteUDP> ColaEventosClienteUDP;

#endif	//_DHTTP_CLIEVENT_H_
