/*********************************************************************************************
*	Name		: strToMatriz.hpp
*	Description	: M�todo de conversi�n de string a matriz con delimitadores
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _STR_TO_MATRIZ_HPP_
#define _STR_TO_MATRIZ_HPP_

#include "w_base/strToVector.hpp"

static strmatriz strToMatriz (const std::string & s, const char delimitadorFila = '\n', const char delimitadorColumna = '\0')
{
	const strvector & filas = ::strToVector (s, delimitadorFila);
	strmatriz ret (filas.size());
	for (size_t i = 0; i < filas.size(); i ++)
	{
		ret[i] = ::strToVector (filas[i], delimitadorColumna);
	}
	return ret;
}

#endif	//_STR_TO_VECTOR_HPP_
