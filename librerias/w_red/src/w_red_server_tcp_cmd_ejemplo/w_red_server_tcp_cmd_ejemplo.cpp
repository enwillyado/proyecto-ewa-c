/*********************************************************************************************
 *	Name		: _w_red_server_tcp_cmd_ejemplo.cpp
 *	Description	: Prueba de ejemplo de un Servidor TCP por un puerto dado (o uno por defecto)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_red_server_tcp_cmd_ejemplo.pragmalib.h"

#define PUERTO_EN_SERVIDOR_DEFECTO "52013"

#include <iostream>							// std::ios, std::istream, std::cout

#include "w_red/w_red_srvtcp.h"				//< para tener el "ServidorTCP"

void callBack (ServidorTCP & servidor)
{
	const EventoServidorTCP & evento = servidor.getEvento();
	servidor.send (evento, "que tal?");
	return;
}

int mainServer (int argc, char* argv[])
{
	const std::string & port = (argc > 1) ? argv[1] : PUERTO_EN_SERVIDOR_DEFECTO;

	// Arrancar el cliente TCP
	ServidorTCP servidor;
#ifdef _DEBUG
	std::cout << "Generando un 'ServidorTCP'" << std::endl;
#endif
	if (false == servidor.open (port))
	{
		std::cerr << "[Error]: servidorTCP.run() " << servidor.getLastError() << " = " << servidor.getLastErrorStr() << std::endl;
		std::cerr << "[Error]: Error del socket: " << servidor.getLastErrorSocket() << " = " << servidor.getLastErrorSocketStr() << std::endl;
		return -1;
	}
	std::cout << "[AVISO] Escuchando por puerto TPC: " << PUERTO_EN_SERVIDOR_DEFECTO << std::endl;

	const strvector & ipsv4 = ServerSocket::getIPsIPv4();
	std::cout << "[AVISO] Se encontraron " << ipsv4.size() << " inteface" << (ipsv4.size() == 1 ? "" : "s") << " IPv4:" << std::endl;
	for (size_t i = 0; i < ipsv4.size(); i++)
	{
		std::cout << "[AVISO] Escuchando en la inteface IPv4: " << ipsv4[i] << std::endl;
	}

	servidor.setCallBack (callBack);

	if (false == servidor.start())
	{
		std::cerr << "[Error] servidorTCP.start() " << servidor.getLastError() << " = " << servidor.getLastErrorStr() << std::endl;
	}

	return 1;
}

int main (int argc, char* argv[])
{
	const int ret = mainServer (argc, argv);
	return ret;
}
