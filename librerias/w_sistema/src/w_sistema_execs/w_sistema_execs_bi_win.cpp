/*********************************************************************************************
 * Name			: w_sistema_execs_bi_win.cpp
 * Description	: Class to call a command
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifdef WIN32
#include "w_sistema/w_sistema_execs_bi.h"

#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <strsafe.h>

bool SistemaExecsBi::putEntrada (const std::string & valor)
{
	// ---------------------------------------------------------------------------
	// Write to the pipe for the child's STDIN.
	// ---------------------------------------------------------------------------
	BOOL bSuccess = FALSE;
	DWORD dwWritten;
	bSuccess = WriteFile (this->g_hChildStd_IN_Wr, valor.c_str(), valor.size(), &dwWritten, NULL);
	if (! bSuccess)
	{
		return false;
	}
	return true;
}

std::string SistemaExecsBi::getPartialSalidaComando() const
{
	// ---------------------------------------------------------------------------
	// Read output from the child process's pipe
	// ---------------------------------------------------------------------------
	{
		// Read output from the child process's pipe for STDOUT
		// and write to the parent process's pipe for STDOUT.
		// Stop when there is no more data.
		DWORD dwRead;
		CHAR chBuf[SistemaExecsBase::BUFFER_CMD_SIZE];
		const BOOL bSuccess = ReadFile (this->g_hChildStd_OUT_Rd, chBuf, SistemaExecsBase::BUFFER_CMD_SIZE,
										&dwRead, NULL);
		if (bSuccess && dwRead > 0)
		{
			return std::string (chBuf, dwRead);
		}

		this->lastError = SistemaExecsBase::FINALIZADO;

		HANDLE hParentStdOut = GetStdHandle (STD_OUTPUT_HANDLE);
		if (! CloseHandle (g_hChildStd_OUT_Rd))
		{
			this->lastError = SistemaExecsBase::ERROR_ENVIAR_AL_HIJO;
			return "";
		}
	}
	return "";
}

SistemaExecsBase::Errores SistemaExecsBi::exeComando()
{
	HANDLE g_hChildStd_IN_Rd = NULL, g_hChildStd_OUT_Wr = NULL;
	this->g_hChildStd_IN_Wr = NULL;
	this->g_hChildStd_OUT_Rd = NULL;

	// ---------------------------------------------------------------------------
	// Crear Pipe
	// ---------------------------------------------------------------------------
	SECURITY_ATTRIBUTES saAttr;
	saAttr.nLength = sizeof (SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	// ---------------------------------------------------------------------------
	// Create a pipe for the child process's STDOUT.
	if (! CreatePipe (&g_hChildStd_OUT_Rd, &g_hChildStd_OUT_Wr, &saAttr, 0))
	{
		return this->lastError = SistemaExecsBase::ERROR_PIPES;
	}

	// Ensure the read handle to the pipe for STDOUT is not inherited.
	if (! SetHandleInformation (g_hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0))
	{
		return this->lastError = SistemaExecsBase::ERROR_PIPES;
	}

	// ---------------------------------------------------------------------------
	// Create a pipe for the child process's STDIN.
	if (! CreatePipe (&g_hChildStd_IN_Rd, &g_hChildStd_IN_Wr, &saAttr, 0))
	{
		return this->lastError = SistemaExecsBase::ERROR_PIPES;
	}

	// Ensure the write handle to the pipe for STDIN is not inherited.
	if (! SetHandleInformation (g_hChildStd_IN_Wr, HANDLE_FLAG_INHERIT, 0))
	{
		return this->lastError = SistemaExecsBase::ERROR_PIPES;
	}

	// ---------------------------------------------------------------------------
	// Create the child process.
	// ---------------------------------------------------------------------------
	{
		// Create a child process that uses the previously created pipes for STDIN and STDOUT.
		const std::string & comando = "cmd /c " + this->getComando();
		TCHAR* szCmdline = new TCHAR[comando.size() + 1];
		szCmdline[comando.size()] = '\0';
		for (size_t i = 0; i < comando.size(); i++)
		{
			szCmdline[i] = comando[i];
		}
		PROCESS_INFORMATION piProcInfo;
		STARTUPINFO siStartInfo;
		BOOL bSuccess = FALSE;

		// Set up members of the PROCESS_INFORMATION structure.

		ZeroMemory (&piProcInfo, sizeof (PROCESS_INFORMATION));

		// Set up members of the STARTUPINFO structure.
		// This structure specifies the STDIN and STDOUT handles for redirection.

		ZeroMemory (&siStartInfo, sizeof (STARTUPINFO));
		siStartInfo.cb = sizeof (STARTUPINFO);
		siStartInfo.hStdError = g_hChildStd_OUT_Wr;
		siStartInfo.hStdOutput = g_hChildStd_OUT_Wr;
		siStartInfo.hStdInput = g_hChildStd_IN_Rd;
		siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

		// Create the child process.

		bSuccess = CreateProcess (NULL,
								  szCmdline,     // command line
								  NULL,          // process security attributes
								  NULL,          // primary thread security attributes
								  TRUE,          // handles are inherited
								  0,             // creation flags
								  NULL,          // use parent's environment
								  NULL,          // use parent's current directory
								  &siStartInfo,  // STARTUPINFO pointer
								  &piProcInfo);  // receives PROCESS_INFORMATION

		// If an error occurs, exit the application.
		if (! bSuccess)
		{
			return this->lastError = SistemaExecsBase::ERROR_CREAR_PROCESO;
		}
		else
		{
			// Close handles to the child process and its primary thread.
			// Some applications might keep these handles to monitor the status
			// of the child process, for example.

			if (! CloseHandle (piProcInfo.hProcess))
			{
				return this->lastError = SistemaExecsBase::ERROR_CREAR_PROCESO;
			}
			if (! CloseHandle (piProcInfo.hThread))
			{
				return this->lastError = SistemaExecsBase::ERROR_CREAR_PROCESO;
			}
			if (! CloseHandle (g_hChildStd_OUT_Wr))
			{
				return this->lastError = SistemaExecsBase::ERROR_CREAR_PROCESO;
			}
		}
	}

	this->lastError = SistemaExecsBase::SIN_ERROR;

	return this->lastError;
}

#endif
