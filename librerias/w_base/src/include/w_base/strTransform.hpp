/*********************************************************************************************
*	Name		: strTransform.hpp
*	Description	: M�todo de conversi�n para transformar un string
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _STR_TRANSFORM_HPP_
#define _STR_TRANSFORM_HPP_

#include <string>       // std::string
#include <ctype.h>		// ::tolower / ::toupper

static std::string strToLower (std::string org)
{
	for (size_t i = 0; i < org.size(); i++)
	{
		org[i] = ::tolower (org[i]);
	}
	return org;
}

static std::string strToUpper (std::string org)
{
	for (size_t i = 0; i < org.size(); i++)
	{
		org[i] = ::toupper (org[i]);
	}
	return org;
}

static std::string & remplazar (std::string & str, const std::string & que, const std::string & por)
{
	// Si no hay nada donde cambiar o nada que cambiar o si es lo mismo
	if (str.empty() || que.empty() || que == por)
	{
		return str;
	}

	// Si no, buscar y reemplazar
	size_t start_pos = 0;
	while ((start_pos = str.find (que, start_pos)) != std::string::npos)
	{
		str.replace (start_pos, que.length(), por);
		start_pos += por.length();
	}

	// retornar resultado modificado
	return str;
}

static std::string remplazar (const std::string & str, const std::string & que, const std::string & por)
{
	std::string str_copy = str;
	return remplazar (str_copy, que, por);
}

static std::string & remplazar (std::string & str, const char que, const char por)
{
	// Si no hay nada donde cambiar
	if (str.empty())
	{
		return str;
	}

	// Si no, buscar
	size_t start_pos = 0;
	while ((start_pos = str.find (que, start_pos)) != std::string::npos)
	{
		str[start_pos] = por;
		start_pos++;
	}

	// retornar resultado modificado
	return str;
}

static std::string remplazar (const std::string & str, const char que, const char por)
{
	std::string str_copy = str;
	return remplazar (str_copy, que, por);
}

#endif	//_STR_TRANSFORM_HPP_
