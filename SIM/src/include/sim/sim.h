#ifndef _SIM_H_
#define _SIM_H_

#include "sim/cpu.h"
#include "sim/mmu.h"

#include <string>

class Sim
{
public:
	// constructor
	Sim();
	~Sim();

	void cuanto();
	void iniciaSistemaInformatico();
	bool cargaGestorDeArranque (const std::string & fichero);

private:
	bool ejecutando;

	CPU* cpu;
	MMU* mmu;
};

#endif	//_SIM_H_
