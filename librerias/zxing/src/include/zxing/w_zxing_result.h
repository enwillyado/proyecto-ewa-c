/*********************************************************************************************
 *	Name		: w_zxing_result.h
 *	Description	: Clase que guarda el resultado de procesar un c�digo Zxing
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ZXING_RESULT_H_
#define _W_ZXING_RESULT_H_

#include "w_graficos/w_coordenada.h"

#include <vector>
#include <string>

class WZxingResult
{
public:
	WZxingResult();

	// Formato
	enum Formato
	{
		FORMATO_DESCONOCIDO = 0,
		FORMATO_QR_CODE,
		FORMATO_DATA_MATRIX,
		FORMATO_UPC_E,
		FORMATO_UPC_A,
		FORMATO_EAN_8,
		FORMATO_EAN_13,
		FORMATO_CODE_128,
		FORMATO_CODE_39,
		FORMATO_ITF,
		FORMATO_AZTEC
	};
	const Formato & getFormato() const;
	void setFormato (const Formato &);

	// Coordenadas
	typedef std::vector<WCoordenada> WCoordenadas;
	const WCoordenadas & getCoordenadas() const;
	void clearCoordeandas();
	void pushCoordenada (const WCoordenada &);

	// Texto
	const std::string & getTexto() const;
	void setTexto (const std::string &);

private:
	Formato formato;
	std::string texto;
	WCoordenadas coordenadas;
};

#endif // _W_ZXING_RESULT_H_
