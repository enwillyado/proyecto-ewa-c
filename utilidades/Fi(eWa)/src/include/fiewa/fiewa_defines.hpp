/*********************************************************************************************
 *	Name		: fiewa_defines.hpp
 *	Description	: Definiciones generales para STUN
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _FIEWA_DEFINES_HPP_
#define _FIEWA_DEFINES_HPP_

#ifdef _DEBUG
#define NOMBRE_DE_SERVIDOR_STUN_DEFECTO "localhost"
#else
#define NOMBRE_DE_SERVIDOR_STUN_DEFECTO "proyectoewa.com"
#endif
#define PUERTO_EN_SERVIDOR_STUN_DEFECTO	52013
#define MAXIMA_LONGITUD_MENSAJE_UDP		1024

#endif //_FIEWA_DEFINES_HPP_
