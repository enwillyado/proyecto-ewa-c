/*********************************************************************************************
 *	Name		: _w_red_tester.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_TESTER_PRAGMALIB_H_
#define _W_RED_TESTER_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

// Parte privada
#include "w_red/_w_red_server.pragmalib.h"
#include "w_red/_w_red_client.pragmalib.h"
#include "w_red/_w_red_proxy.pragmalib.h"

#endif

#undef _W_RED_TESTER_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_RED_TESTER_PRAGMALIB_H_
