/*********************************************************************************************
*	Name		: _zxing.pragmalib.h
*	Description	: Librer�as necesarias para enlazar con la libre�a ZXING
* Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _ZXING_PRAGMALIB_H_
#define _ZXING_PRAGMALIB_H_

#ifdef _MSC_VER

#include "w_base/_pragmalib_base.h"

#include "w_codificacion/_w_codificacion.pragmalib.h"
#include "w_web/_w_web_protocol.pragmalib.h"

// Parte privada
#pragma comment(lib, "whatsacppi" FIN_LIB)

#pragma comment(lib, "libcurl.lib")
#pragma comment(lib, "ws2_32.lib")

#endif

#undef _ZXING_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _ZXING_PRAGMALIB_H_
