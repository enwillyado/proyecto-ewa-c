/*********************************************************************************************
 *	Name		: w_base_tester_files.cpp
 *	Description	: Pruebas unitarias para probar los mecanismos de manejo de rutas de ficheros
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_base_tester.pragmalib.h"

#include "w_base/filesStr.hpp"

#include <assert.h>

int baseTesterFiles()
{
	{
		assert ("fichero.extension" == WFilesStr::getNombre ("fichero.extension"));
		assert ("fichero" == WFilesStr::getNombreOnly ("fichero.extension"));
		assert ("extension" == WFilesStr::getExt ("fichero.extension"));
		assert ("./" == WFilesStr::getDir ("fichero.extension"));
	}
	{
		assert ("fichero.extension" == WFilesStr::getNombre ("./fichero.extension"));
		assert ("fichero" == WFilesStr::getNombreOnly ("./fichero.extension"));
		assert ("extension" == WFilesStr::getExt ("./fichero.extension"));
		assert ("./" == WFilesStr::getDir ("./fichero.extension"));
	}
	{
		assert ("fichero.extension" == WFilesStr::getNombre ("/dir/fichero.extension"));
		assert ("fichero" == WFilesStr::getNombreOnly ("/dir/fichero.extension"));
		assert ("extension" == WFilesStr::getExt ("/dir/fichero.extension"));
		assert ("/dir/" == WFilesStr::getDir ("/dir/fichero.extension"));
	}
	{
		assert ("fichero.extension" == WFilesStr::getNombre ("/dir/subdir/fichero.extension"));
		assert ("fichero" == WFilesStr::getNombreOnly ("/dir/subdir/fichero.extension"));
		assert ("extension" == WFilesStr::getExt ("/dir/subdir/fichero.extension"));
		assert ("/dir/subdir/" == WFilesStr::getDir ("/dir/subdir/fichero.extension"));
	}
	return 0;
}
