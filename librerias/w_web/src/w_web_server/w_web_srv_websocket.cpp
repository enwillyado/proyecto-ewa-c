/*********************************************************************************************
 *	Name		: w_web_srv_websocket.cpp
 *	Description	: Implementaci�n del Servidor TCP para WebSockets
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_web/w_web_srv_websocket.h"

#include "w_codificacion/w_codificacion.h"

// --------------------------------------------------------------------------
// Implementaci�n del Servidor WebSockets

ServidorWebSockets::ServidorWebSockets()
	: ServidorWebTCP()
{
}

static std::string calcularDesafio (const std::string & secWebSocketKey)
{
	static const std::string & mag = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
	const std::string & cad = secWebSocketKey + mag;
	SHA1 out;
	sha1CreateFromString (cad.c_str(), out);
	std::string ret;
	base64Encode ((char*)out, sizeof (out), ret);
	return ret;
}

/**
 *	Analiza un nuevo mensaje recibido para procesarlo si se conoce el protocolo que tiene
 *	\param	evento	[in]	evento (de MENSAJE_RECIBIDO) que se va a analizar
 *	\retval	TRUE si el evento fue analizado; FALSE si no
**/
bool ServidorWebSockets::analizaEventoMsgRecv (const EventoServidorTCP & evento)
{
	if (this->socketsAbiertos.find (evento.socketID) == this->socketsAbiertos.end())
	{
		// Procesarlo como un mensaje web
		return ServidorWebTCP::analizaEventoMsgRecv (evento);
	}

	// Procesarlo como un mensaje de websocks
	this->procesaMsgSokRecv (evento);
	return true;
}

Mensaje ServidorWebSockets::responderMsgSokRecv (const EventoServidorTCP & evento, const Mensaje & peticion)
{
	Mensaje respuesta;
	respuesta.setVersionProtocolo (Mensaje::HTTP1_1);
	respuesta.setError (Mensaje::SWITCHING_PROTOCOLS);
	respuesta % "Connection" = "Upgrade";
	respuesta % "Sec-WebSocket-Accept" = calcularDesafio (peticion % "Sec-WebSocket-Key");
	respuesta % "Server" = "EWA";
	respuesta % "Upgrade" = "websocket";
	respuesta.setTipoMensaje (Mensaje::RESPONSE);

	this->socketsAbiertos.insert (evento.socketID);
	return respuesta;
}

void ServidorWebSockets::procesaMsgNoWebRecv (const EventoServidorTCP & evento)
{
	this->procesaMsgSokRecv (evento);
}

std::string ServidorWebSockets::procesaMsgSokRecv (const EventoServidorTCP & evento)
{
	const std::string & mensajeRecibido = evento.mensaje;
	const unsigned char* const bufferRecibido = (const unsigned char*)mensajeRecibido.c_str();
	size_t itr = 0;

	// 0. Leer formato
	if (bufferRecibido[itr] != 0x81)
	{
		// Desconectar el modo websockets
		this->socketsAbiertos.erase (evento.socketID);
		this->close (evento);
		return "";
	}

	// 1. Leer longitud
	size_t longitud = mensajeRecibido[++itr] & 0x7F;
	if (longitud == 0x7F)
	{
		// TODO: soportar longitudes > 65535
		return "";
	}
	else if (longitud == 0x7E)
	{
		size_t byteAlto = bufferRecibido[++itr] & 0xFF;
		longitud = byteAlto * 0x100 + bufferRecibido[++itr];
	}

	// 2. Leer m�scara
	const char mascaras[4] = {bufferRecibido[++itr], bufferRecibido[++itr],
							  bufferRecibido[++itr], bufferRecibido[++itr],
							 };
	itr++;

	// 3. Leer mensaje
	std::string ret = mensajeRecibido.substr (itr);
	for (size_t i = 0; i < longitud; i++)
	{
		ret[i] ^= mascaras[i % 4];
	}
	return ret;
}

void ServidorWebSockets::procesaMsgSokEnv (const EventoServidorTCP & evento, const std::string & respuesta)
{
	std::string mensajeCompuesto;
	mensajeCompuesto.push_back ((signed char)0x81);
	const size_t longitud = respuesta.size();
	if (longitud < 0x7E)
	{
		// tama�o < 127 bytes
		mensajeCompuesto.push_back ((unsigned char)longitud);
	}
	else if (longitud <= 0xFFFF)
	{
		// tama�o < 65.536 bytes
		mensajeCompuesto.push_back (0x7E);
		unsigned char byteAlto = (unsigned char) (longitud / 0x100);
		unsigned char byteBajo = (unsigned char) (longitud % 0x100);
		mensajeCompuesto.push_back (byteAlto);
		mensajeCompuesto.push_back (byteBajo);
	}
	else
	{
		// TODO: soportar 64 bits (por ahora solo 32 bits)
		// tama�o < 4.294.967.296 bytes
		mensajeCompuesto.push_back (0x7F);
		unsigned char byteMAlto = (unsigned char) ((longitud & 0xFF000000) / 0x1000000);
		unsigned char byteAlto = (unsigned char) ((longitud & 0x00FF0000) / 0x10000);
		unsigned char byteBajo = (unsigned char) ((longitud & 0x0000FF00) / 0x100);
		unsigned char byteMBajo = (unsigned char) ((longitud & 0x000000FF) / 0x1);
		mensajeCompuesto.push_back (0x00);
		mensajeCompuesto.push_back (0x00);
		mensajeCompuesto.push_back (0x00);
		mensajeCompuesto.push_back (0x00);
		mensajeCompuesto.push_back (byteMAlto);
		mensajeCompuesto.push_back (byteAlto);
		mensajeCompuesto.push_back (byteBajo);
		mensajeCompuesto.push_back (byteMBajo);
	}
	mensajeCompuesto += respuesta;
	this->ServidorTCP::send (evento, mensajeCompuesto);
}
