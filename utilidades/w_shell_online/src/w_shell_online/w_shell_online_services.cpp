/*********************************************************************************************
 * Name			: w_shell_online_services.cpp
 * Description	: Implementación de los servicios para el ShellOnline
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_shell_online/w_shell_online_services.h"

#include "w_sistema/w_sistema_execs.h"

#include <fstream>
#include <iostream>

inline static std::string leerContenidoFile (const std::string & filename)
{
	// Abrir el fichero
	std::ifstream infile;
	infile.open (filename.c_str(), std::ifstream::binary | std::ifstream::in);
	if (false == infile.good())
	{
		return "";
	}
	// Retornar el contenido
	return std::string (std::istreambuf_iterator<char> (infile), std::istreambuf_iterator<char>());
}

// ----------------------------------------------------------------------------
// Servicio para cuando se solicite un ping
Mensaje ServicioShellOnline::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setVersionProtocolo (Mensaje::HTTP1_0);

	if (true == peticion.existeRequestVar ("file"))
	{
		SistemaExecsBi executeCommand;
		executeCommand.setComando ("cmd");

		// Ejecutar el comando
		const SistemaExecsBi::Errores error = executeCommand.exeComando();
		if (error == SistemaExecsBi::SIN_ERROR)
		{
			// Ajustar la entrada
			const std::string & filename = peticion.getRequestVar ("file");
			const std::string & entrada = ::leerContenidoFile ("../testdata/" + filename + ".txt");
			executeCommand.putEntrada (entrada);

			// Obtener la salida
			const std::string & salida = executeCommand.getSalidaComando();
			respuesta.setBody ("<code><pre>" + salida + "</pre></code>");
		}
		else
		{
			respuesta.setBody ("<h2>ERROR</h2>");
		}

		respuesta.setError (Mensaje::FOUND);
	}
	else
	{
		respuesta.setError (Mensaje::NOT_FOUND);
	}
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	return respuesta;
}
