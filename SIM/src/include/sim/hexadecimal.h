#ifndef _HEXADECIMAL_H_
#define _HEXADECIMAL_H_

// ----------------------------------------------------------------------------
static bool esHexa (const char num);
static const bool hexaSubChar (const char que, const int num);
static char hexaSubString (const char que, const int num);

//static void pasaToChar (int len, long long ptr, char* reales);
//static void pasaToPuntero (int len, char* reales, char rea[PAGINAS][COMPONT]);

// ----------------------------------------------------------------------------
bool esHexa (const char que)
{
	return (que == '0'	 || que == '1' ||
			que == '2'	 || que == '3' ||
			que == '4'	 || que == '5' ||
			que == '6'	 || que == '7' ||
			que == '8'	 || que == '9' ||
			que == 'A'	 || que == 'B' ||
			que == 'C'	 || que == 'D' ||
			que == 'E' 	 || que == 'F');
}

const bool hexaSubChar (const char que, const int num)
{
	const char c = hexaSubString (que , num);
	if (c == '0')
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
char hexaSubString (const char que, const int num)
{
	if (!esHexa (que) || num >= 4 || num < 0)
	{
		return 0;
	}
	const char* r;
	switch (que)
	{
	case '0':
		r = "0000";
		return r[num];
		break;
	case '1':
		r = "0001";
		return r[num];
		break;
	case '2':
		r = "0010";
		return r[num];
		break;
	case '3':
		r = "0011";
		return r[num];
		break;
	case '4':
		r = "0100";
		return r[num];
		break;
	case '5':
		r = "0101";
		return r[num];
		break;
	case '6':
		r = "0110";
		return r[num];
		break;
	case '7':
		r = "0111";
		return r[num];
		break;
	case '8':
		r = "1000";
		return r[num];
		break;
	case '9':
		r = "1001";
		return r[num];
		break;
	case 'A':
		r = "1010";
		return r[num];
		break;
	case 'B':
		r = "1011";
		return r[num];
		break;
	case 'C':
		r = "1100";
		return r[num];
		break;
	case 'D':
		r = "1101";
		return r[num];
		break;
	case 'E':
		r = "1110";
		return r[num];
		break;
	case 'F':
		r = "1111";
		return r[num];
		break;
	}
	return 0;
}
/*
void pasaToPuntero (int len, char* reales, char rea[paginas][compont])
{
	for (int i = 0; i < len; i++)
	{
		int pag = i / compont;
		int com = i % compont;
		rea[pag][com] = reales[i];
	}
}

void pasaToChar (int len, long long ptr, char* reales)
{
	char buffer[len];
	for (int i = 0; i < len; i++)
	{
		buffer[i] = ' ';
	}
	itoa (ptr, buffer, 2);
	int j = 0;
	for (int i = len - 1; i >= 0 || j < len; i--)
	{
		if (i >= 0)
		{
			if (buffer[i] == '1' || buffer[i] == '0')
			{
				reales[j] = buffer[i] - 48;
				j++;
			}
		}
		else
		{
			reales[j] = 0;
			j++;
		}
	}
}
*/
#endif
