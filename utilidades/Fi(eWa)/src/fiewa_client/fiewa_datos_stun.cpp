/*********************************************************************************************
 *	Name		: fiewa_datos_stun.cpp
 *	Description	: Implementaci�n de la clase que almacena los datos enviados por el Servidor STUN
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "fiewa/fiewa_datos_stun.h"

#include "dencrypt/dencryptcpp.h"		//< para tener ::convertStringToBase64

// ----------------------------------------------------------------------------
// �raa de datos est�tica
FIEWA::DatosSTUN FIEWA::DatosSTUN::npos;

// ----------------------------------------------------------------------------
// Constructores
FIEWA::DatosSTUN::DatosSTUN()
{
}

FIEWA::DatosSTUN::DatosSTUN (const HTTPNS::Protocolo & mensajeRecibirDeSTUN)
{
	this->parseFromMsg (mensajeRecibirDeSTUN);
}

FIEWA::DatosSTUN::DatosSTUN (const HTTPNS::Evento & evento)
{
	this->publicID = evento.ip;
	this->publicPort = evento.puerto;
	this->publicID = ::convertStringToBase64 (evento.ip + HTTPNS::Protocolo::toStr (evento.puerto));
}

// Factor�a
FIEWA::DatosSTUN FIEWA::DatosSTUN::buscandoPeers (const HTTPNS::Protocolo & mensajeRecibirDeSTUN)
{
	FIEWA::DatosSTUN datos;
	if (datos.parseFromMsg (mensajeRecibirDeSTUN) == false)
	{
		return FIEWA::DatosSTUN::npos;
	}
	return datos;
}

// Asignador
bool FIEWA::DatosSTUN::parseFromMsg (const HTTPNS::Protocolo & mensajeRecibirDeSTUN)
{
	/*
	if (mensajeRecibirDeSTUN == null)
	{
		return false;
	}
	*/
	std::string body = mensajeRecibirDeSTUN.getBody();
	size_t pos = body.find (':');
	if (pos == -1)
	{
		return false;
	}
	size_t nextPos = body.find (':', pos);
	if (pos == -1)
	{
		return false;
	}
	this->publicIP = body.substr (0, pos);
	this->publicPort = HTTPNS::Protocolo::fromStr (body.substr (pos + 1, nextPos - pos - 1));
	this->publicID = HTTPNS::Protocolo::fromStr (body.substr (nextPos + 1));
	return true;
}

// M�todos auxiliares est�ticos
int FIEWA::DatosSTUN::buscandoNumPeers (const HTTPNS::Protocolo & mensajeRecibirDeSTUNbuscandoPeers)
{
	return HTTPNS::Protocolo::fromStr (mensajeRecibirDeSTUNbuscandoPeers.getBody());
}

// Operadores
bool FIEWA::DatosSTUN::operator== (const FIEWA::DatosSTUN & org) const
{
	return this->publicIP == org.publicIP && this->publicPort == org.publicPort &&
		   this->publicID == org.publicID;
}
bool FIEWA::DatosSTUN::operator!= (const FIEWA::DatosSTUN & org) const
{
	return this->operator== (org) == false;
}

// Conversores
std::string FIEWA::DatosSTUN::toString() const
{
	return this->publicID + ":" + HTTPNS::Protocolo::toStr (this->publicPort) + ":" + this->publicID;
}