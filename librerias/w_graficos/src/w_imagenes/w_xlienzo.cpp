/*********************************************************************************************
 * Name			: w_xlienzo.cpp
 * Description	: Implementaci� de la clase para gestionar un lienzo en XML
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_xlienzo.h"

#include "w_arbol/w_arbolxml.h"

#include "w_graficos/w_rgba.h"
#include "w_base/strToMatriz.hpp"
#include "w_base/strPurge.hpp"

#include <cstdlib>		//< atoi

// ----------------------------------------------------------------------------
// Constructores
WXLienzo::WXLienzo (const size_t ancho, const size_t alto)
	: WLienzo (ancho, alto)
{
}

// ----------------------------------------------------------------------------
// Cargadores

bool buscarMatrizLaXY (const strmatriz & matriz, size_t & x, size_t & y)
{
	for (size_t j = 0; j < matriz.size(); j++)			// eje y
	{
		for (size_t i = 0; i < matriz[j].size(); i++)	// eje x
		{
			if ("=" == matriz[j][i])
			{
				x = i;
				y = j;
				return true;
			}
		}
	}
	return false;
}

WXLienzo WXLienzo::getWXLienzoFromFile (const std::string & filename)
{
	WXLienzo ret;
	ret.leerContenidoFile (filename);
	return ret;
}
WXLienzo WXLienzo::getWXLienzoFromArbol (const Arbol & contenido)
{
	WXLienzo ret;
	ret.leerContenidoArbol (contenido);
	return ret;
}

bool WXLienzo::leerContenidoFile (const std::string & filename)
{
	const Arbol & contenido = ArbolXML::getArbolFromFile (filename);
	if (contenido.getLastError() != ArbolXML::SIN_ERROR)
	{
		return false;
	}
	return this->leerContenidoArbol (contenido);
}

bool WXLienzo::leerContenidoArbol (const Arbol & contenido)
{
	// Procesar dimensiones
	{
		bool cambio = false;
		if (contenido.existeAtributo ("ancho"))
		{
			this->ancho = ::atoi ((contenido % "ancho").c_str());
			cambio = true;
		}
		if (contenido.existeAtributo ("alto"))
		{
			this->alto = ::atoi ((contenido % "alto").c_str());
			cambio = true;
		}
		if (true == cambio)
		{
			this->reserveIntern();
		}
	}

	// Procesar contenido: paleta
	typedef std::map<char, RGBA> MapaRGBA;
	MapaRGBA mapaRGBA;
	for (size_t i = 0; i < contenido["paleta"].atributosize(); i++)
	{
		const ArbolXML::Atributo & atributo = contenido["paleta"].getAtributo (i);
		const std::string & alias = atributo.first;
		const std::string & valor = atributo.second;

		mapaRGBA[alias[0]] = RGBA (valor);
	}

	// Procesar contenido: matriz de puntos
	{
		const std::string & cuadro = ::strPurge (contenido.getValor(), "\r\t");
		const strmatriz & matriz = ::strToMatriz (cuadro);

		// Obtener coordenadas x=0, y=0
		size_t x, y;
		const bool existeOrigen = ::buscarMatrizLaXY (matriz, x, y);
		if (false == existeOrigen)
		{
			return false;
		}

		// Ver el m�ximo de columnas que hay
		size_t maxCols = 0;
		for (size_t j = 0; j < matriz.size(); j++)				// eje y
		{
			if (matriz[j].size() > maxCols)
			{
				maxCols = matriz[j].size();
			}
		}

		// Buscar las columnas que tienen '>' en el eje (=,i)
		std::vector<size_t> columnas;
		for (size_t i = x + 1; i < matriz[y].size(); i++)		// eje x
		{
			const char c = matriz[y][i][0];
			if (c == '>')
			{
				// Se ignora esta columna
				continue;
			}
			columnas.push_back (i);
		}
		for (size_t ii = matriz[y].size(); ii < maxCols; ii++)
		{
			columnas.push_back (ii);
		}

		// Buscar ahora por filas, ignornado las vac�as, comentadas
		// y teniendo en cuenta las columnas comentadas
		size_t m = 0;
		for (size_t j = y + 1; j < matriz.size(); j++)		// eje y
		{
			if (matriz[j].size() == 0)
			{
				// Se ignora esta fila si est� vac�a
				continue;
			}
			const char c = matriz[j][0][0];
			if (c == '>')
			{
				// Se ignora esta fila
				continue;
			}
			for (size_t n = 0; n < columnas.size(); n++)
			{
				const size_t i = columnas[n];
				if (matriz[j].size() <= i)
				{
					continue;
				}
				const char color = matriz[j][i][0];
				if (color != ' ')
				{
					this->setPixel (n, m, mapaRGBA[color]);
				}
			}
			m++;
		}

	}

	return true;
}
