/*********************************************************************************************
 *	Name		: w_make_parametros.h
 *	Description	: Par�metros para preparar soluciones
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_MAKE_PARAMETROS_H_
#define _W_MAKE_PARAMETROS_H_

#include <string>
#include <vector>

typedef std::vector<std::string> strvector;

class WMakeParametros
{
public:
	// Constructores
	WMakeParametros();

	enum Tipo
	{
		EJECUTABLE,		//< .exe
		LIBRERIA_DLL,	//< .dll .so
		LIBRERIA_LIB,	//< .lib .a
		OBJETO,			//< .obj .o
		FUENTE_C,		//< .c
		FUENTE_CPP,		//< .cpp .cc .C
	};

	// Modificadores
	void pushLibOrg (const std::string & origen);
	void pushSrcOrg (const std::string & origen);

public:
	// �rea de datos p�blica
	strvector carpetasInclusion;
	strvector carpetasInclusionLib;

	std::string nombre;
	Tipo tipo;

	strvector libOrigen;
	strvector srcOrigen;
	strvector objOrigen;
	std::string dirOrigen;

	std::string dirObjDestino;
	std::string dirBinDestino;
};

#endif // _W_MAKE_PARAMETROS_H_
