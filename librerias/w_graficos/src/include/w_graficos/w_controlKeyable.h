/*********************************************************************************************
 * Name			: w_controlKeyable.h
 * Description	: Desencadenadores de teclas para controles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_CONTROL_KEYABLE_H_
#define _W_CONTROL_KEYABLE_H_

#include "w_graficos/w_eventoKey.h"
#include "w_graficos/w_controlBase.h"

#include <vector>
#include <cstring>	//< size_t

class WControlKeyable : public WControlBase
{
public:
	// Constructor
	WControlKeyable();

public:
	// ---------------------------------------------------------------------------
	// onKeyDown
	virtual void onKeyDown (const unsigned int key);

	typedef WEventoKey::CbEventoKey OnKeyDown;
	size_t setOnKeyDown (OnKeyDown cbOnKeyDown);
	bool unsetOnKeyDown (const size_t index);

	// ---------------------------------------------------------------------------
	// onKeyUp
	virtual void onKeyUp (const unsigned int key);

	typedef WEventoKey::CbEventoKey OnKeyUp;
	size_t setOnKeyUp (OnKeyUp cbOnKeyUp);
	bool unsetOnKeyUp (const size_t index);

protected:
	virtual void clear();

private:
	// �rea de desencadenadores
	std::vector<OnKeyDown> cbsOnKeyDown;
	std::vector<OnKeyUp> cbsOnKeyUp;
};

#endif // _W_CONTROL_KEYABLE_H_
