/*********************************************************************************************
*	Name		: w_red_proxy_udp.h
*	Description	: Clase para implementar un proxy de red basado en UDP
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_PROXY_UDP_H_
#define _W_RED_PROXY_UDP_H_

#include "w_red/w_red_proxy.h"

#include "w_red/w_red_cliudp.h"
#include "w_red/w_red_srvudp.h"

#include "w_sistema/w_sistema_thread.h"

#include <string>
#include <map>

class ProxyUDP : public Proxy
{
public:
	ProxyUDP (const std::string & remoteHost, const int remotePort);
	virtual bool open (const int localPort);
	virtual bool start();

protected:
	// CallBack manejador
	static void staticCallBack (ServidorUDP & servidor);
	virtual void callBack();
	virtual void aceptarConexion (const EventoServidorUDP &);

	// Manejadores de datos del proxy
	virtual void onClienteGetData (const EventoServidorUDP &, const std::string & mensaje);
	virtual void onProxyGetData (const SocketID, const std::string & mensaje);

	class DatosCliente;
	typedef std::map<SocketID, DatosCliente*> DatosClientesUDP;
	DatosClientesUDP clientesUDP;
	ServidorUDP servidorUDP;
};


#endif //_W_RED_PROXY_UDP_H_
