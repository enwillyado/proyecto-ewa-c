/*********************************************************************************************
 *	Name		: w_red_proxy_tcp.cpp
 *	Description	: Implementaci�n de la clase para implementar un proxy de red basado en TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_proxy_tcp.h"
#include <iostream>

static bool isgraph (const std::string & m)
{
	for (size_t i = 0; i < m.size(); i++)
	{
		const char c = m[i];
		if (false == ::isgraph (c) && c != ' ' && c != '\r' && c != '\n' && c != '\t')
		{
			return false;
		}
	}
	return true;
}

// ----------------------------------------------------------------------------
// Clase auxiliar para alojar los clientes remotos
class ProxyTCP::DatosCliente
{
public:
	// Funci�n manejadora para cada cliente
	static void recibirCliente (const WThread & hilo)
	{
		const ProxyTCP::DatosCliente::TWThreadDataClientes & c = hilo.getData<ProxyTCP::DatosCliente::TWThreadDataClientes>();
		while (c.data->clienteTCP.getLastError() == ClienteTCP::SOCKET_ACTIVO)
		{
			// Leer mensajes mientras el socket est� activo
			std::string nuevoMensaje;
			if (false == c.data->clienteTCP.recibir (nuevoMensaje))
			{
				break;
			}

			// Y mand�rselos al proxy
			c.data->proxyTCP->onClienteGetData (c.data->socketID, nuevoMensaje);
		}
	}

public:
	typedef TWThreadData<DatosCliente*> TWThreadDataClientes;

	// �rea de datos p�blica
	WThread hilo;
	ClienteTCP clienteTCP;
	SocketID socketID;
	ProxyTCP* proxyTCP;		//< referencia al padre
};

// ----------------------------------------------------------------------------
// Constructor del proxy
ProxyTCP::ProxyTCP (const std::string & remoteHost, const int remotePort)
	: Proxy (remoteHost, remotePort), clientesTCP()
{
}

void ProxyTCP::staticCallBack (ServidorTCP & servidor)
{
	ProxyTCP & self = servidor.getCallBackObj<ProxyTCP>();
	self.callBack();
}
void ProxyTCP::callBack()
{
	const EventoServidorTCP & evento = this->servidorTCP.getEvento();
	const SocketID & socketID = evento.socketID;
	switch (evento.tipo)
	{
	case Evento::CONEXION_ACEPTADA:
	{
		this->aceptarConexion (evento);
	}
	break;

	case Evento::MENSAJE_RECIBIDO:
	{
		// Enviar al cliente remoto el evento recibido
		this->onProxyGetData (socketID, evento.mensaje);
	}
	break;

	case Evento::CONEXION_DESCONECTADA:
	case Evento::CONEXION_PERDIDA:
	{
		ProxyTCP::DatosClientesTCP::iterator itr = this->clientesTCP.find (socketID);
		if (itr != this->clientesTCP.end())
		{
			// Detener el cliente remoto y su hilo
			itr->second->clienteTCP.closeSocket();
			itr->second->hilo.finish();

			// Borrar cliente remoto
			delete itr->second;
			itr->second = NULL;

			this->clientesTCP.erase (itr);

			// Cerrar conexi�n con el padre
			this->servidorTCP.close (evento);
		}
	}
	break;

	default:
	{
		int i = 0;
	}
	break;
	}
}

void ProxyTCP::aceptarConexion (const EventoServidorTCP & evento)
{
	DatosClientesTCP::iterator itr = this->clientesTCP.find (evento.socketID);
	if (itr == this->clientesTCP.end())
	{
		// Obtener identificador del socket aceptado
		const SocketID & socketID = evento.socketID;

		// Crear datos
		DatosCliente* cliente = new ProxyTCP::DatosCliente;

		// A�adir datos al almacen
		this->clientesTCP[socketID] = cliente;

		// Ajustar padre e identificador
		cliente->proxyTCP = this;
		cliente->socketID = socketID;

		// Crear cliente remoto
		std::cout << "TCP(" << servidorTCP.getPort() << "): open [" << this->Proxy::host << ":" << this->Proxy::port << "]";
		const bool retOpen = cliente->clienteTCP.openSocket (this->Proxy::host, this->Proxy::port);

		// Crear hilo lector remoto->local
		DatosCliente::TWThreadDataClientes data (cliente);
		cliente->hilo.setFunction (&ProxyTCP::DatosCliente::recibirCliente);
		cliente->hilo.setData (data);
		cliente->hilo.start();
	}
}


void ProxyTCP::onProxyGetData (const SocketID socketID, const std::string & mensaje)
{
#ifdef _DEBUG
	std::cout << "TCP(" << servidorTCP.getPort() << "): send (" << socketID << ") [" << mensaje.size() << "] ";
	if (true == ::isgraph (mensaje))
	{
		std::cout << mensaje << std::endl;
	}
#endif

	// Enviar el dato al cliente adecuado si existe
	DatosClientesTCP::iterator itr = this->clientesTCP.find (socketID);
	if (itr != this->clientesTCP.end())
	{
		itr->second->clienteTCP.enviar (mensaje);
	}
}

void ProxyTCP::onClienteGetData (const SocketID socketID, const std::string & mensaje)
{
#ifdef _DEBUG
	std::cout << "TCP(" << servidorTCP.getPort() << "): put [" << mensaje.size() << "]" << std::endl;
	if (true == ::isgraph (mensaje))
	{
		std::cout << mensaje << std::endl;
	}
#endif

	// Enviar el dato al proxy
	this->servidorTCP.send (socketID, mensaje);
}

bool ProxyTCP::open (const int localPort)
{
	// Ajustar los valores comunes
	Proxy::open (localPort);

	// Ajustar el servidor y abrirlo
	const bool retCallBack = servidorTCP.setCallBack (&ProxyTCP::staticCallBack, this);
	const bool retOpen = retCallBack && this->servidorTCP.open (localPort);
	return retCallBack && retOpen;
}

bool ProxyTCP::start()
{
	const bool retStart = this->servidorTCP.start();
	return retStart;
}
