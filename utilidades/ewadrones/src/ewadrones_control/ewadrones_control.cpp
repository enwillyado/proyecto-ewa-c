/*********************************************************************************************
 *	Name		: ewadrones_control.cpp
 *	Description	: Ejecutable donde se implementa una interfaz de control para EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_ewadrones_control.pragmalib.h"

#include "ewadrones_control_globales.h"
#include "ewadrones_control_vision.h"
#include "ewadrones_control_app.hpp"

#include "w_graficos/w_app.h"
#include "w_graficos/w_textControl.h"
#include "w_graficos/w_areaScrollControl.h"

#define ANCHO_APP	800//(ANCHO_VIDEO * 2 + 40)
#define ALTO_APP	400//(ALTO_VIDEO + 50)

//#define MAQUINA		"willypcp"
//#define MAQUINA		"localhost"
#define MAQUINA		"192.168.1.2"

#define VIDEO_SRC	"..\\bin\\video.bin"
//#define VIDEO_SRC	"..\\bin\\video_20150309_191456.mp4"
//#define VIDEO_SRC	"..\\bin\\QR.mp4"
//#define VIDEO_SRC	"tcp://" MAQUINA ":80"		//< PUERTO_VIDEO_EN_PROXY
//#define VIDEO_SRC	"tcp://" MAQUINA ":5555"	//< puerto en AR.DRONE...

// Evitar que salga la consola
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

// Programa principal
int main (int argc, char* argv[])
{
	// Crear la instancia de la APP
	WApp app ("eWadronesControl", ANCHO_APP, ALTO_APP);
	::GetAPP() = &app;

	// Crear la instancia del AR.DRONE
	::GetArDrone().setHost (MAQUINA);
#ifdef VIDEO_SRC
	::GetArDrone().getVideo().setVideoSrc (VIDEO_SRC);
#endif
	::GetArDrone().getVideo().setOnNewLienzo (&Video::onNewLienzo);
	::GetArDrone().init();

	class MaquetacionAPP
	{
		class Cerrar
		{
		public:
			static void onXClicMouseOver (const WEventoMouse & evento)
			{
				evento.getRoot().stop();
			}
		};
		class Aterrizar
		{
		public:
			static void onXClicMouseOver (const WEventoMouse & evento)
			{
				::GetArDrone().getControlador().aterrizar();
			}
		};
		class Despegar
		{
		public:
			static void onXClicMouseOver (const WEventoMouse & evento)
			{
				::GetArDrone().getControlador().despegar();
			}
		};
		class Emergencia
		{
		public:
			static void onXClicMouseOver (const WEventoMouse & evento)
			{
				::GetArDrone().getControlador().emergencia();
			}
		};

	public:

		static AppFunctores crearVector()
		{
			AppFunctores ret;
			ret.add ("onFousOut", &CosasAPP::onFousOut);
			ret.add ("onKeyUp", &CosasAPP::onKeyUp);
			ret.add ("onKeyDown", &CosasAPP::onKeyDown);
			ret.add ("Cerrar::onXClicMouseOver", &MaquetacionAPP::Cerrar::onXClicMouseOver);
			ret.add ("Despegar::onXClicMouseOver", &MaquetacionAPP::Despegar::onXClicMouseOver);
			ret.add ("Aterrizar::onXClicMouseOver", &MaquetacionAPP::Aterrizar::onXClicMouseOver);
			ret.add ("Emergencia::onXClicMouseOver", &MaquetacionAPP::Emergencia::onXClicMouseOver);
			ret.add ("onXMouseDrag", &CosasAPP::onXMouseDrag);
			return ret;
		}
		static AppFunctores & GetFunctores()
		{
			static AppFunctores functores = MaquetacionAPP::crearVector();
			return functores;
		}
	};

	// Cargar maquetación
	app.loadXW ("../testdata/maquetacion.xw", MaquetacionAPP::GetFunctores());

	// Iniciar la APP
	::GetArDrone().start();
	app.start();

	// Finalizar correctamente
	return 0;
}
