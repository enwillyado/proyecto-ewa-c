/*********************************************************************************************
 * Name			: w_areaResizeControl.h
 * Description	: Clase para gestionar un control de �rea resizeable
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_AREA_RESIZE_CONTROL_H_
#define _W_AREA_RESIZE_CONTROL_H_

#include "w_graficos/w_vScrollControl.h"
#include "w_graficos/w_multicontrol.h"
#include "w_graficos/w_app.h"

#include <string>

// ----------------------------------------------------------------------------
// Base para los controles internos
class WAreaResizeControl;
class WAreaRedimensinado : public WControl
{
public:
	// Constructores
	WAreaRedimensinado (const std::string & iName = "");

	// Modificador
	void setAreaResizeControl (WAreaResizeControl* const iAreaResize);

	// Observadores
	virtual bool esApuntadoPor (const WCoordenada & control) const;

	// Manejadores
	virtual void onMoveMouseEnter()
	{
		// Actualizar
		WEventoMouse e (*this);
		this->actualizar (e);
	}
	virtual void onMoveMouseOut()
	{
		// Actualizar
		WEventoMouse e (*this);
		this->actualizar (e);
	}

protected:
	static void arrastrar (const WEventoMouseDrag & e);
	static void actualizar (const WEventoMouse & e);

	bool invertido;
	bool abajo;

protected:
	WAreaResizeControl* areaResize;
};

// ----------------------------------------------------------------------------
// Controles externo (final)
class WAreaResizeControl : public WMultiControl
{
public:
	// Constructores
	WAreaResizeControl (const std::string & iName = "");
	WAreaResizeControl (WAreaResizeControl & org);

	// Inicializador
	void init();

	// Modificadores
	virtual void resize (const size_t ancho, const size_t alto);
	virtual size_t insertarPtr (WControl* const control, const bool deletable = true);
	virtual WLienzo & recomponerLienzo();

	// Manejadores
	virtual void onRMouseDrag();
	virtual void onMMouseDrag();
	virtual void onLMouseDrag();

protected:
	// �rae protegida
	WAreaRedimensinado areaRedimiensionado;
};

#endif // _W_AREA_RESIZE_CONTROL_H_
