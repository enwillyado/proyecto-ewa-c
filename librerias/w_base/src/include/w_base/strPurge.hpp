/*********************************************************************************************
*	Name		: strPurge.hpp
*	Description	: M�todo para quitar caracteres a una cadena
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _STR_PURGE_HPP_
#define _STR_PURGE_HPP_

#include "w_base/std_types.hpp"

static std::string strPurge (const std::string & s, const std::string & eliminar)
{
	std::string ret;
	for (size_t i = 0; i < s.size(); i ++)
	{
		const char c = s[i];
		if (eliminar.find (c) == std::string::npos)
		{
			ret.push_back (c);
		}
	}
	return ret;
}

static std::string strPurge (const std::string & s, const char eliminar)
{
	std::string ret;
	for (size_t i = 0; i < s.size(); i ++)
	{
		const char c = s[i];
		if (c != eliminar)
		{
			ret.push_back (c);
		}
	}
	return ret;
}

#endif	//_STR_PURGE_HPP_
