/*********************************************************************************************
 * Name			: w_controlJerarquizable.h
 * Description	: Clase manejadora de la jerarqu�a de elementos
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_CONTROL_ROOTABLE_H_
#define _W_CONTROL_ROOTABLE_H_

#include "w_graficos/w_controlBase.h"

#ifdef _DEBUG
#include "w_base/asserts.hpp"
#define ASSERT_IN_DEBUG ::assertIfNull
#else
#define ASSERT_IN_DEBUG
#endif

#include <string>

// FWD
class WApp;
class WControl;

class WControlJerarquizable : public WControlBase
{
public:
	// Constructor
	WControlJerarquizable();

	// ---------------------------------------------------------------------------
	// Colocar la ra�z
	bool haveRootPtr();
	WApp* getRootPtr();
	WApp & getRoot();

	virtual void setRoot (WApp & iWapp);

	// ---------------------------------------------------------------------------
	// Colocar la ascendencia
	WControl* getParentPtr();
	WControl & getParent();

	void setParent (WControl & iWapp);

	// ---------------------------------------------------------------------------
	// Colocar la descendencia
	virtual WControl* obtenerPtr (const std::string & iName, const bool recursivo)
	{
		return NULL;
	}

private:
	// �rea de desencadenadores
	WApp* wappRoot;
	WControl* controlParent;
};

#endif // _W_CONTROL_ROOTABLE_H_
