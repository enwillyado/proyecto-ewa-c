/*********************************************************************************************
 *	Name		: _w_red_srvudp.h
 *	Description	: Servidor UDP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRVUDP_H_
#define _W_RED_SRVUDP_H_

#define NUMERO_MAXIMO_CLIENTES 10	// n�mero m�ximo de clientes CONECTADOS (que no procesando) en el Servidor

// Definiciones para la gesti�n del socket
#define DESCRIPTOR_NULO 	-1		// valor que indica que un descriptor (socket, file...) es nulo

#include <string>

#include "w_red/w_red_srvevent.h"
#include "w_red/w_red_srvsocket.h"

#ifdef _DEBUG
#include "w_base/asserts.hpp"
#define ASSERT_IN_DEBUG ::assertIfNull
#else
#define ASSERT_IN_DEBUG
#endif

class ServidorUDP
{
	friend class ServerSocket;		// El ServerSocket es amiga (para acceder a nuestra zona privada)
	void setConectado();
	void setEscuchando();
	void setProcesando();
	void setDesconectando();
	void setDesconectado();
public:
	enum Errores
	{
		CONECTADO = 0,
		CONECTANDO,
		ESCUCHANDO,
		PROCESANDO,
		DESCONECTADO,
		DESCONECTANDO,
		SOCKET_RECV_TIMEOUT,
		SOCKET_RECV_TIMEOUT_ERROR,
		ERRONEO,
	};

	// ASK: �M�s constructores?
	ServidorUDP();

	// Estos son los miembros p�blicos de un servidor
	template <class T>
	bool run (const T & puerto)
	{
		if (this->open (puerto) == false)
		{
			// la llamada a 'open' ya ajusta un error en caso de fallo
			return false;
		}
		return this->start(); // si se sale, es que algo malo pas�
	}

	// Observadores
	const std::string & getHost();
	unsigned int getPort();

	// Control de errores:
	Errores getLastError() const;
	std::string getLastErrorStr() const;
	ServerSocket::Errores getLastErrorSocket() const;
	std::string getLastErrorSocketStr() const;

	// FIN DE LA FACADE.
	// --------------------------------------------------------------------------

public:	// Para que se puedan probar en los test

	// L�gica del servidor:
	bool open (const std::string & port);
	bool open (unsigned int port);
	bool start();
	bool send (const EventoServidorUDP &, const std::string & mensaje);
	bool close();
	EventoServidorUDP getEvento();
	size_t getNumEventosPendintes();

public:
	// Sobrecarga de m�todos de procesamiento de eventos de red por CallBack
	bool setCallBack (void (*) (ServidorUDP &), void* callBackObj = NULL);
	void* getCallBackObjPtr() const;

	template<class T>
	T & getCallBackObj() const
	{
		return *ASSERT_IN_DEBUG (static_cast<T*> (getCallBackObjPtr()));
	}
	// Envia un mensaje a otro equipo
	bool enviar (const std::string & mensajeEnviarm, const std::string & host, const int port);
	bool enviar (const std::string & mensajeEnviarm, const std::string & host, const std::string & port);
	bool recibir (std::string & mensaje, unsigned int segundosEsperados = 0);

	// Static Utils
	static std::string toStr (Errores);

protected:
	// �rea de datos protegida
	ColaEventosServidorUDP colaEventos;
	std::string host;
	unsigned int port;

public:
	virtual void analizaEvento (const EventoServidorUDP &);
	virtual void analizaEventoDef (const EventoServidorUDP &);
	virtual bool analizaEventoMsgRecv (const EventoServidorUDP &);

protected:
	void (*eventCallBack) (ServidorUDP &);
	void* callBackObj;

	// Controlde errores:
	Errores lastError;

private:
	// �rea de datos privada (porque en ella se guardan los datos para comunicarse
	//					  con las capas inferiores de la implementaci�n del socket)
	int sockfd;
	ServerSocket serverSocket;
};

#endif	//_W_RED_SRVUDP_H_
