/*********************************************************************************************
 * Name			: w_app.cpp
 * Description	: Implementaci�n de la clase que gestiona una aplicaci�n gr�fica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_app.h"

#include "w_arbol/w_arbol.h"		//< Arbol
#include "w_arbol/w_arbolxml.h"		//< ArbolXML

#include "w_graficos/w_xlienzo.h"

#include "w_base/fromStr.hpp"		//< ::fromStr

bool WApp::loadXW (const std::string & filename, const AppFunctores & functores)
{
	const Arbol & arbol = ArbolXML::getArbolFromFile (filename);
	if (arbol.getLastError() != Arbol::SIN_ERROR)
	{
		return false;
	}
	return this->loadXW (arbol, functores);
}

bool WApp::loadXW (const Arbol & arbol, const AppFunctores & functores)
{
	const std::string & raiz = arbol.getClave();
	if (raiz != "xwin")
	{
		return false;
	}

	// Procesar atributos:
	{
		{
			// Dimensiones
			size_t anchoNum = this->getAncho();
			size_t altoNum = this->getAlto();
			bool cambiaValores = false;

			// Ver si cambia el ancho
			const std::string & anchoNuevo = arbol.getValorAtributo ("ancho");
			if (anchoNuevo != "")
			{
				const size_t anchoNumNuevo = ::fromStr<size_t> (anchoNuevo);
				if (anchoNum != anchoNumNuevo)
				{
					cambiaValores = true;
					anchoNum = anchoNumNuevo;
				}
			}

			// Ver si cambia el alto
			const std::string & altoNuevo = arbol.getValorAtributo ("alto");
			if (altoNuevo != "")
			{
				const size_t altoNumNuevo = ::fromStr<size_t> (altoNuevo);
				if (altoNum != altoNumNuevo)
				{
					cambiaValores = true;
					altoNum = altoNumNuevo;
				}
			}

			// Cambiarlos si al menos uno cambi�
			if (true == cambiaValores)
			{
				this->setSize (anchoNum, altoNum);
				this->rePosAndDim();
			}
		}
		{
			// Posiciones
			size_t xNum = this->getPosX();
			size_t yNum = this->getPosY();
			bool cambiaValores = false;

			// Ver si cambia el ancho
			const std::string & xNuevo = arbol.getValorAtributo ("x");
			if (xNuevo != "")
			{
				const size_t xNumNuevo = ::fromStr<size_t> (xNuevo);
				if (xNum != xNumNuevo)
				{
					cambiaValores = true;
					xNum = xNumNuevo;
				}
			}

			// Ver si cambia el alto
			const std::string & yNuevo = arbol.getValorAtributo ("y");
			if (yNuevo != "")
			{
				const size_t yNumNuevo = ::fromStr<size_t> (yNuevo);
				if (yNum != yNumNuevo)
				{
					cambiaValores = true;
					yNum = yNumNuevo;
				}
			}

			// Cambiarlos si al menos uno cambi�
			if (true == cambiaValores)
			{
				this->setPos (xNum, yNum);
				this->rePosAndDim();
			}
		}

		// Ajustar acciones
		{
			// onFousIn
			const std::string & onFousIn = arbol.getValorAtributo ("onFousIn");
			if (onFousIn != "")
			{
				AppFunctores::FunctorOn functor = functores.getFunctorOn (onFousIn);
				if (functor != NULL)
				{
					this->setOnFocusIn (functor);
				}
			}

			// onFousOut
			const std::string & onFousOut = arbol.getValorAtributo ("onFousOut");
			if (onFousOut != "")
			{
				AppFunctores::FunctorOn functor = functores.getFunctorOn (onFousOut);
				if (functor != NULL)
				{
					this->setOnFocusOut (functor);
				}
			}
		}
		{
			// onKeyUp
			const std::string & onKey = arbol.getValorAtributo ("onKeyUp");
			AppFunctores::FunctorOnKey functor = functores.getFunctorOnKey (onKey);
			if (functor != NULL)
			{
				this->setOnKeyUp (functor);
			}
		}

		// Ajustar cursor
		{
			const std::string & cursor = arbol.getValorAtributo ("cursor");
			if (cursor != "")
			{
				this->setCursor (WXLienzo::getWXLienzoFromFile (cursor));
			}
		}
	}

	bool ret = true;	//< asumimos que va a ir bien
	for (size_t i = 0; i < arbol.hijosize(); i++)
	{
		const Arbol & arbolHijo = arbol.getHijos()[i];
		ret &= this->WMultiControl::loadXW (arbolHijo, functores);
		if (false == ret)
		{
			// si fall�, salir
			break;
		}
	}
	return ret;
}
