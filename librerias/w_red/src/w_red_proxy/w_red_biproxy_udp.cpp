/*********************************************************************************************
 *	Name		: w_red_biproxy_udp.cpp
 *	Description	: Implementación de la clase para implementar un proxy de red basado en UDP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_biproxy_udp.h"

typedef TWThreadData<ServidorUDP*> TWThreadDataProxy;
void BiProxyUDP::funcionHilo (const WThread & hilo)
{
	const TWThreadDataProxy & c = hilo.getData<TWThreadDataProxy>();
	c.data->start();
}

BiProxyUDP::BiProxyUDP (const std::string & remoteHost, const int remotePort, const int biLocalPort)
	: ProxyUDP (remoteHost, remotePort), eventoBiClienteUDP()
{
	if (true == this->biServidorUDP.open (biLocalPort))
	{
		this->biServidorUDP.setCallBack (&BiProxyUDP::biServidor, this);

		TWThreadDataProxy hiloData (&this->biServidorUDP);
		WThread hilo (&BiProxyUDP::funcionHilo, hiloData);
		hilo.start();
	}
}
void BiProxyUDP::onClienteGetData (const EventoServidorUDP & evento, const std::string & mensaje)
{
	ProxyUDP::onClienteGetData (evento, mensaje);

	// Y mandar o acumular el mensaje por la bi-conexión
	if (this->eventoBiClienteUDP.socketID != -1)
	{
		this->biServidorUDP.send (this->eventoBiClienteUDP, mensaje);
	}
	else
	{
		this->mensaje0 += mensaje;
	}
}

void BiProxyUDP::biServidor (ServidorUDP & servidor)
{
	const EventoServidorUDP & evento = servidor.getEvento();
	servidor.analizaEventoDef (evento);

	// Y procesar la nueva bi-conexión
	/*
	if (evento.tipo == EventoServidorUDP::CONEXION_ACEPTADA)
	*/
	{
		servidor.getCallBackObj<BiProxyUDP>().eventoBiClienteUDP = evento;
		std::string & mensaje0 = servidor.getCallBackObj<BiProxyUDP>().mensaje0;
		if (mensaje0 != "")
		{
			servidor.send (evento, mensaje0);
			mensaje0.clear();
		}
	}
}
