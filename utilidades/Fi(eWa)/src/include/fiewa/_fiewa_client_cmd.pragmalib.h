/*********************************************************************************************
 *	Name		: _fiewa_client_cmd.pragmalib.h
 *	Description	: Header file with unified libraries to link
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _FIEWA_CLIENT_CMD_PRAGMALIB_H_
#define _FIEWA_CLIENT_CMD_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#pragma message("Enlazando con las librer�as del proyecto 'fiewa_client_cmd'...")

#include "fiewa/_fiewa_client.pragmalib.h"

#include "w_sistema/_w_sistema_time.pragmalib.h"

#endif

#endif // _FIEWA_CLIENT_CMD_PRAGMALIB_H_
