/*********************************************************************************************
 * Name			: w_lienzo.cpp
 * Description	: Implementación de la clase para manipular una representación gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_lienzo.h"

#include "w_graficos/w_control.h"
#include "w_graficos/w_rgba.h"

#include <string.h>		//< memcpy

#ifdef WIN32
#define BYTES_POR_COLOR	3
#else
#define BYTES_POR_COLOR	4
#endif

// ----------------------------------------------------------------------------
// Clase interna
class WLienzo::WLienzoIntern
{
public:
	// Constructores
	WLienzoIntern (const size_t ancho, const size_t alto)
		: datos (NULL)
#ifdef WIN32
		, alphas (NULL)
#endif
	{
		this->reservar (ancho,  alto);
	}
	~WLienzoIntern()
	{
		this->liberar();
	}

	// Inicializadores
	void reservar (const size_t ancho, const size_t alto)
	{
		if (ancho * alto > 0)
		{
			this->datos = new unsigned char[ancho * alto * BYTES_POR_COLOR]();
#ifdef WIN32
			this->alphas = new unsigned char[ancho * alto]();
#endif
		}
		else
		{
			this->datos = NULL;
#ifdef WIN32
			this->alphas = NULL;
#endif
		}
	}
	void liberar_y_reservar (const size_t ancho, const size_t alto)
	{
		if (this->datos != NULL)
		{
			this->liberar();
		}
		this->reservar (ancho,  alto);
	}
	void liberar()
	{
		if (this->datos != NULL)
		{
			delete[] datos;
			this->datos = NULL;
		}
#ifdef WIN32
		if (this->alphas != NULL)
		{
			delete[] alphas;
			this->alphas = NULL;
		}
#endif
	}

	// Modificadores totales
	inline void set (const WLienzoIntern & data, const size_t ancho, const size_t alto)
	{
		this->set (data.datos, ancho, alto);
#ifdef WIN32
		memcpy (this->alphas, data.alphas, ancho * alto);
#endif
	}
	inline void set (const WLienzo::Pixeles & datosIn, const size_t ancho, const size_t alto)
	{
		memcpy (this->datos, datosIn, ancho * alto * BYTES_POR_COLOR);
	}

	// Modificadores parciales (por fila)
	inline void set (const WLienzo::Pixeles & datosFila, const size_t ancho, const size_t alto, const size_t fila)
	{
		this->set (datosFila, ancho, alto, fila, ancho);
	}
	inline void set (const WLienzo::Pixeles & datosFila, const size_t ancho, const size_t alto, const size_t fila,
					 const size_t anchoFila)
	{
		this->set (datosFila, ancho, alto, fila, ancho, 0);
	}
	inline void set (const WLienzo::Pixeles & datosFila, const size_t ancho, const size_t alto, const size_t fila,
					 const size_t anchoFila, const size_t xPos)
	{
		memcpy (this->datos + (ancho * BYTES_POR_COLOR * (alto - fila - 1)) + (xPos * BYTES_POR_COLOR), datosFila,
				anchoFila * BYTES_POR_COLOR);
#ifdef WIN32
		// TODO: canal alpha ajustado al máximo ¿variable?
		memset (this->alphas + (ancho * (alto - fila - 1)) + xPos, 0xFF, anchoFila);
#endif
	}

public:
	WLienzo::PixelesInt datos;
#ifdef WIN32
	WLienzo::AlphasInt alphas;
#endif

private:
	WLienzoIntern (const WLienzoIntern &);
	void operator= (const WLienzoIntern &);
};

// ----------------------------------------------------------------------------
// Constructores
WLienzo::WLienzo (const size_t ancho, const size_t alto)
	: data (*new WLienzoIntern (ancho, alto))
{
	this->ancho = ancho;
	this->alto = alto;
	this->reserve (ancho, alto);
}
WLienzo::WLienzo (const WLienzoBase & lienzo)
	: data (*new WLienzoIntern (lienzo.getAncho(), lienzo.getAlto()))
{
	this->ancho = lienzo.getAncho();
	this->alto = lienzo.getAlto();
	this->reserve (lienzo.getAncho(), lienzo.getAlto());
	this->WLienzoBase::operator= (lienzo);
}
WLienzo::WLienzo (const WLienzo & lienzo)
	: data (*new WLienzoIntern (lienzo.getAncho(), lienzo.getAlto()))
{
	this->ancho = lienzo.getAncho();
	this->alto = lienzo.getAlto();
	this->operator= (lienzo);
}

WLienzo WLienzo::createWLienzo (const size_t ancho, const size_t alto, const RGBA & rgba)
{
	WLienzo ret;
	ret.reserve (ancho, alto);
	for (size_t y = 0; y < alto; y++)
	{
		for (size_t x = 0; x < ancho; x++)
		{
			ret.setPixel (x, y, rgba);
		}
	}
	return ret;
}

void WLienzo::operator= (const WLienzo & lienzo)
{
	if (this->ancho * this->alto != lienzo.ancho * lienzo.alto)
	{
		this->data.liberar_y_reservar (lienzo.ancho, lienzo.alto);
	}
	this->ancho = lienzo.ancho;
	this->alto = lienzo.alto;
	this->data.set (lienzo.data, this->ancho, this->alto);
}

WLienzo::~WLienzo()
{
	WLienzoIntern* ptr = &this->data;
	ptr->liberar();
	delete ptr;
}

// ----------------------------------------------------------------------------
// Accesores
RGBA WLienzo::getPixel (const size_t x, const size_t y) const
{
	if (this->ancho <= x || this->alto <= y)
	{
		return RGBA();
	}
#ifdef WIN32
	return this->getPixelFast (x, this->alto - y - 1);
#else
	return this->getPixelFast (x, y);
#endif
}
bool WLienzo::setPixel (const size_t x, const size_t y, const RGBA & rgb)
{
	if (this->ancho <= x || this->alto <= y)
	{
		return false;
	}
	if (rgb.a > 0)
	{
#ifdef WIN32
		this->setPixelFast (x, this->alto - y - 1, rgb);
#else
		this->setPixelFast (x, y, rgb);
#endif
	}
	return true;
}
void WLienzo::setPixelFast (const size_t x, const size_t y, const RGBA & rgb)
{
	const size_t desfase = (y * this->ancho + x) * BYTES_POR_COLOR;
	this->data.datos[desfase + 0] = rgb.b;
	this->data.datos[desfase + 1] = rgb.g;
	this->data.datos[desfase + 2] = rgb.r;
#ifdef WIN32
	this->data.alphas[y * this->ancho + x] = rgb.a;
#else
	this->data.datos[desfase + 3] = rgb.a;
#endif
}
RGBA WLienzo::getPixelFast (const size_t x, const size_t y) const
{
	RGBA ret;
	const size_t desfase = (y * this->ancho + x) * BYTES_POR_COLOR;
	ret.b = this->data.datos[desfase + 0];
	ret.g = this->data.datos[desfase + 1];
	ret.r = this->data.datos[desfase + 2];
#ifdef WIN32
	ret.a = this->data.alphas[y * this->ancho + x];
#else
	ret.a = this->data.datos[desfase + 3];
#endif
	return ret;
}

WLienzo::Pixeles WLienzo::getPixeles() const
{
	return this->data.datos;
}
WLienzo::Pixeles WLienzo::getPixeles (const int fila) const
{
	return this->data.datos + ((this->getAlto() - fila - 1) * (BYTES_POR_COLOR * this->getAncho()));
}

WLienzo::Pixeles WLienzo::getAlphas() const
{
#ifdef WIN32
	return this->data.alphas;
#else
	// TODO: permitir usar alphas en otras plataformas
	return NULL;
#endif
}
WLienzo::Pixeles WLienzo::getAlphas (const int fila) const
{
#ifdef WIN32
	return this->data.alphas + ((this->getAlto() - fila - 1) * this->getAncho());
#else
	// TODO: permitir usar alphas en otras plataformas
	return NULL;
#endif
}

// ----------------------------------------------------------------------------
// Modificadores
void WLienzo::setPixeles (const unsigned char* const & datos)
{
	this->data.set (datos, this->ancho, this->alto);
}
void WLienzo::setPixeles (const unsigned char* const & datosFila, const int fila)
{
	this->data.set (datosFila, this->ancho, this->alto, fila);
}

void WLienzo::unir (const WLienzo & lienzo, const size_t xPos, const size_t yPos)
{
#define UNION_OPTIMIZADA
#ifdef UNION_OPTIMIZADA
	const size_t anchoLienzoOrg = lienzo.getAncho();
	const size_t altoLienzoOrg = lienzo.getAlto();
	if (anchoLienzoOrg == 0 || altoLienzoOrg == 0 || ancho == 0 || alto == 0)
	{
		return;
	}
	//const size_t anchoOrg = std::min (anchoLienzoOrg, this->ancho - xPos);
	const size_t yOrg = (signed)yPos >= 0 ? 0 : - (signed)yPos;
	const size_t xOrg = (signed)xPos >= 0 ? xPos : 0;
	if ((signed)ancho < (signed)xPos || (signed)alto < (signed)yPos)
	{
		return;
	}
	const size_t desfaseAncho = (signed)xPos >= 0 ? 0 : - (signed)xPos;
	const size_t anchoOrg = std::min (anchoLienzoOrg, this->ancho - xPos);
	const size_t altoOrg = std::min (altoLienzoOrg, this->alto - yPos);
	for (size_t y = yOrg; y < altoOrg; y++)
	{
		Pixeles pixelesFilaOrg = lienzo.getPixeles (y);
		Alphas alphaFilaOrg = lienzo.getAlphas (y);
#define PIXEL_A_PIXEL
#ifdef PIXEL_A_PIXEL
		for (size_t x = 0; x < anchoOrg - desfaseAncho; x++)
		{
			if (alphaFilaOrg[x] != 0x00)
			{
				this->data.set (pixelesFilaOrg + ((desfaseAncho + x) * BYTES_POR_COLOR),
								this->ancho, this->alto, y + yPos, 1, xOrg + x);
			}
			else
			{
				// TODO: combinar con alpha
				//int a = 0;
				//this->data.set (pixelesFilaOrg + desfaseAncho + (x * BYTES_POR_COLOR),
				//this->ancho, this->alto, y + yPos, 1, xOrg + x);
			}
		}
#else
		this->data.set (pixelesFilaOrg + desfaseAncho, this->ancho,
						this->alto, y + yPos, anchoOrg - desfaseAncho, xOrg);
#endif
	}
#else
	this->WLienzoBase::unir (lienzo, xPos, yPos);
#endif
}
void WLienzo::unir (const WLienzoBase & lienzo, const size_t xPos, const size_t yPos)
{
	this->WLienzoBase::unir (lienzo, xPos, yPos);
}
void WLienzo::unir (const WControl & control)
{
	this->unir (control.getLienzo(), control.getPosX(), control.getPosY());
}

void WLienzo::reserve (const size_t ancho, const size_t alto)
{
	if (this->ancho != ancho || this->alto != alto)
	{
		this->ancho = ancho;
		this->alto = alto;
		this->reserveIntern();
	}
}
void WLienzo::reserveIntern()
{
	this->data.liberar_y_reservar (this->ancho, this->alto);
}

// ----------------------------------------------------------------------------
// Observadores
size_t WLienzo::getAncho() const
{
	return this->ancho;
}
size_t WLienzo::getAlto() const
{
	return this->alto;
}
