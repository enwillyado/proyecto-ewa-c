/*********************************************************************************************
 *	Name		: fiewa_client_cmd.cpp
 *	Description	: Prueba de ejemplo de un PeerUDP (1) que se conectar� a trav�s
 *					del protocolo STUN por NAT a otro PeerUDP (2) de pares.
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#include "fiewa/_fiewa_client_cmd.pragmalib.h"

#include "fiewa/fiewa_defines.hpp"		//< para tener las definiciones generales
#include "fiewa/fiewa_client.h"			// para tener el "FIEWA::PeerUDP"
#include "fiewa/fiewa_services_id.h"	// para tener los identificadores de los servicios de prueba

#include "w_web/w_web_mensaje.h"		// para tener el "Mensaje Web"
#include "w_sistema/w_sistema_time.h"	// para tener el "WTime::esperarMilisegundos"

#include <iostream>					 	// std::ios, std::istream, std::cout

// TODO: que el servidor tenga mensajes de depuraci�n
void DCOUT_MESSAGE (const std::string & mensaje)
{
#ifdef _DEBUG
	//std::cout << mensaje << std::endl;
#endif
}

int mainClient (int argc, char* argv[])
{
	// Iniciar el peer
	FIEWA::PeerUDP peer;

#ifdef CLIENTE_ORIGINAL
	// Bucle infinito
	FIEWA::DatosSTUN datosSTUN1, datosSTUN2;
	bool conectado = false, encontrado = false;
	while (true)
	{
		// Ver si hay que crear los clientes
		if (conectado == false)
		{
			// Generar el mensaje que se enviar� al STUN
			Mensaje mensajeEnviarAlSTUN;
			mensajeEnviarAlSTUN.setVersionProtocolo (Mensaje::EWA1_0);
			mensajeEnviarAlSTUN.setTipoMensaje (Mensaje::GET);
			mensajeEnviarAlSTUN.setURI (SRVSERVICE_IPANDPORT_ID);

			// Conectar el peer al STUN para obtener los datos del propio peer
			if (peer.connect (NOMBRE_DE_SERVIDOR_STUN_DEFECTO, PUERTO_EN_SERVIDOR_STUN_DEFECTO) == false)
			{
				::dsystemSleep (3 * 1000);
				continue; // < y regresar al principio
			}
			if (peer.send (mensajeEnviarAlSTUN) == false)
			{
				::dsystemSleep (3 * 1000);
				continue; // < y regresar al principio
			}
			Mensaje mensajeRecibirDeSTUN1 = peer.recibir();
			datosSTUN1 = FIEWA::DatosSTUN::buscandoPeers (mensajeRecibirDeSTUN1);

			// Si no se pudo obtener los datos del cliente
			if (datosSTUN1 == FIEWA::DatosSTUN::npos)
			{
				peer.close();
				::dsystemSleep (3 * 1000);
				continue; // < y regresar al principio
			}
			// se da por conectado
			conectado = true;
		}
		if (encontrado == false)
		{
			// Conectar el peer al STUN para obtener los datos de otro peer
			FIEWA::PeerUDP obtenerDatos;
			if (obtenerDatos.connect (NOMBRE_DE_SERVIDOR_STUN_DEFECTO,
									  PUERTO_EN_SERVIDOR_STUN_DEFECTO) == false)
			{
				::dsystemSleep (3 * 1000);
				continue; // < y regresar al principio
			}

			// Obtener los datos de otro PEER en el STUN
			const std::string & servicio = SRVSERVICE_IPOFPEER_ID "?id=" + datosSTUN1.publicID;

			// Generar el mensaje que se enviar� al STUN
			Mensaje mensajeEnviarAlSTUNbuscandoPeers;
			mensajeEnviarAlSTUNbuscandoPeers.setVersionProtocolo (Mensaje::EWA1_0);
			mensajeEnviarAlSTUNbuscandoPeers.setTipoMensaje (Mensaje::GET);
			mensajeEnviarAlSTUNbuscandoPeers.setURI (servicio);

			// Enviar el mensaje y esperar por una respuesta
			obtenerDatos.send (mensajeEnviarAlSTUNbuscandoPeers);
			const Mensaje & mensajeRecibirDeSTUNbuscandoPeers = obtenerDatos.recibir();
			datosSTUN2 = FIEWA::DatosSTUN::buscandoPeers (mensajeRecibirDeSTUNbuscandoPeers);

			// Si no se pudo obtener los datos del peer
			if (datosSTUN2 == FIEWA::DatosSTUN::npos)
			{
				::dsystemSleep (3 * 1000);
				continue; // < y regresar al principio
			}
			// se da por encontrado
			encontrado = true;
			peer.connect (datosSTUN2);
		}

		std::string intento = datosSTUN1.toString() + " -> ??? ";
		if (encontrado == true)
		{
			intento = datosSTUN1.toString() + " -> " + datosSTUN2.toString();

			// Cliente 1 saludar� al otro PEER
			if (peer.send ("hola don pepito: " + intento) == false)
			{
				intento += " KO";
			}
			else
			{
				intento += " OK";
			}
		}

		// Cliente 1 recibir� algo de PEER
		std::string recibido = peer.recibirStr();
		if (recibido == "")
		{
			// No se recibi� nada
			recibido = "NADA: " + intento;	// suplantar lo recibido al intento inicial (para depuraci�n)
		}
		else
		{
			// Se recibi� algo
			intento = ""; // destruye el intento (solo para depuraci�n)
		}

		// A�adir autor�a
		recibido = recibido + " (soy " + datosSTUN1.toString() + ")";

		// Comunicamos el mensaje a la interface
		std::cout << "Mensaje recibido: " << recibido << std::endl;
		::dsystemSleep (1 * 1000);
	}

#else

	char nombre[255];
	strset (nombre, '\0');
	//dsystemGetNameMachine (nombre, 255);
	const std::string & my = nombre;

	// Bucle infinito
	FIEWA::DatosSTUN datosSTUN1, datosSTUN2;
	bool conectado = false, encontrado = false;
	while (true)
	{
		// Ver si hay que crear los clientes
		if (conectado == false)
		{
			// Obtener los datos LOCALES del STUN
			const std::string & servicio = SRVSERVICE_LOGIN_ID "?my=" + my;

			// Generar el mensaje que se enviar� al STUN
			Mensaje mensajeEnviarAlSTUN;
			mensajeEnviarAlSTUN.setVersionProtocolo (Mensaje::EWA1_0);
			mensajeEnviarAlSTUN.setTipoMensaje (Mensaje::GET);
			mensajeEnviarAlSTUN.setURI (servicio);

			// Conectar el peer al STUN para obtener los datos del propio peer
			if (peer.connect (NOMBRE_DE_SERVIDOR_STUN_DEFECTO, PUERTO_EN_SERVIDOR_STUN_DEFECTO) == false)
			{
				WTime::esperarMilisegundos (3 * 1000);
				continue; // < y regresar al principio
			}
			if (peer.send (mensajeEnviarAlSTUN) == false)
			{
				WTime::esperarMilisegundos (3 * 1000);
				continue; // < y regresar al principio
			}
			Mensaje mensajeRecibirDeSTUN1 = peer.recibir();

			datosSTUN1 = FIEWA::DatosSTUN::buscandoPeers (mensajeRecibirDeSTUN1);

			// Si no se pudo obtener los datos del cliente
			if (datosSTUN1 == FIEWA::DatosSTUN::npos)
			{
				peer.close();
				WTime::esperarMilisegundos (3 * 1000);
				continue; // < y regresar al principio
			}
			// se da por conectado
			conectado = true;
		}
		if (encontrado == false)
		{
			// Conectar el peer al STUN para obtener los datos de otro peer
			if (peer.connect (NOMBRE_DE_SERVIDOR_STUN_DEFECTO,
							  PUERTO_EN_SERVIDOR_STUN_DEFECTO) == false)
			{
				WTime::esperarMilisegundos (3 * 1000);
				continue; // < y regresar al principio
			}

			// Obtener los datos de otro PEER en el STUN
			const std::string & servicio = SRVSERVICE_LOGIN_ID "?id=" + my;

			// Generar el mensaje que se enviar� al STUN
			Mensaje mensajeEnviarAlSTUNbuscandoPeers;
			mensajeEnviarAlSTUNbuscandoPeers.setVersionProtocolo (Mensaje::EWA1_0);
			mensajeEnviarAlSTUNbuscandoPeers.setTipoMensaje (Mensaje::GET);
			mensajeEnviarAlSTUNbuscandoPeers.setURI (servicio);

			// Enviar el mensaje y esperar por una respuesta
			peer.send (mensajeEnviarAlSTUNbuscandoPeers);
			const Mensaje & mensajeRecibirDeSTUNbuscandoPeers = peer.recibir();
			datosSTUN2 = FIEWA::DatosSTUN::buscandoPeers (mensajeRecibirDeSTUNbuscandoPeers);

			// Si no se pudo obtener los datos del peer
			if (datosSTUN2 == FIEWA::DatosSTUN::npos)
			{
				WTime::esperarMilisegundos (3 * 1000);
				continue; // < y regresar al principio
			}
			// se da por encontrado
			encontrado = true;
			peer.connect (datosSTUN2);
		}

		std::string intento = datosSTUN1.toString() + " -> ??? ";
		if (encontrado == true)
		{
			intento = datosSTUN1.toString() + " -> " + datosSTUN2.toString();

			// Cliente 1 saludar� al otro PEER
			if (peer.send ("hola don pepito: " + intento) == false)
			{
				intento += " KO";
			}
			else
			{
				intento += " OK";
			}
		}

		// Cliente 1 recibir� algo de PEER
		std::string recibido = peer.recibirStr();
		if (recibido == "")
		{
			// No se recibi� nada
			std::cout << "NADA: " << intento <<  " (soy " + datosSTUN1.toString() + ")" << std::endl;
		}
		else
		{
			// Se recibi� algo
			std::cout << "Mensaje recibido: " << recibido <<  " (soy " + datosSTUN1.toString() + ")" << std::endl;
		}

		// Esperar un poco interface
		WTime::esperarMilisegundos (1 * 1000);
	}
#endif

	return 0;
}

int main (int argc, char* argv[])
{
	int ret;
	while (true)
	{
		ret = mainClient (argc, argv);
		std::cout << "Presione una tecla para empezar de nuevo; CONTROL+C para salir" << std::endl;
		std::cin.get();
	}
	return ret;
}
