/*********************************************************************************************
 * Name			: ewadrones.h
 * Description	: Clase para gestionar EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_H_
#define _EWADRONES_H_

#include "ewadrones/w_ar_drone_controlador.h"
#include "ewadrones/w_ar_drone_video.h"
#include "ewadrones/w_ar_drone_xml.h"

#include <string>

class EWadrones
{
public:
	EWadrones (const std::string & host = "");
	void init();
	void start();
	void stop();

public:
	// Observadores
	const std::string & getHost() const;
	void setHost (const std::string &);

	// Accesores
	ArDroneControlador & getControlador();
	const ArDroneControlador & getControlador() const;

	ArDroneVideo & getVideo();
	const ArDroneVideo & getVideo() const;

	ArDroneXML & getXML();
	const ArDroneXML & getXML() const;

private:
	// �rea de datos (componentes)
	ArDroneControlador controlador;
	ArDroneVideo video;
	ArDroneXML xml;

	// �rea de datos interna
	std::string host;
};

#endif // _EWADRONES_H_
