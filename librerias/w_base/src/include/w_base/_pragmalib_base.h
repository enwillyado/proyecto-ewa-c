/*********************************************************************************************
*	Name		: _pragmalib_base.h
*	Description	: Definiciones b�sicas para los pragmalibs
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _PRAGMALIB_BASE_H_
#define _PRAGMALIB_BASE_H_

#define SIMPLE_END_LIB_WIN32	".lib"
#define SIMPLE_END_LIB_UNIX		".a"

#ifdef WIN32
#define SIMPLE_END_LIB	SIMPLE_END_LIB_WIN32
#else
#define SIMPLE_END_LIB	SIMPLE_END_LIB_UNIX
#endif

#ifdef _DEBUG
#define FIN_LIB "d" SIMPLE_END_LIB
#else
#define FIN_LIB "r" SIMPLE_END_LIB
#endif

#endif	//_PRAGMALIB_BASE_H_
