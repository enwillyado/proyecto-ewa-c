/*********************************************************************************************
 *	Name		: _w_graficos.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_GRAFICOS_PRAGMALIB_H_
#define _W_GRAFICOS_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_base/_pragmalib_base.h"

// Propia librer�a: (para no autoengancharla si es una dll)
#pragma comment(lib, "w_graficos" FIN_LIB)
#include "w_graficos/_w_imagenes.pragmalib.h"

// Dependencias
#include "w_sistema/_w_sistema_time.pragmalib.h"

#ifdef WIN32
#else
#pragma comment(lib, "X11")
#endif

#endif

#undef _W_GRAFICOS_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_GRAFICOS_PRAGMALIB_H_
