/*********************************************************************************************
 *	Name		: fiewa_client.cpp
 *	Description	: Implementación del cliente que se conecta por UDP con el Servidor
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "fiewa/fiewa_client.h"

// Constructores
FIEWA::PeerUDP::PeerUDP()
{
	// Crear el socket
	this->createServer();
}

FIEWA::PeerUDP::PeerUDP (const std::string & nombre, const size_t puerto)
{
	// Crear el socket
	this->createServer();

	// Conectarse al destino indicado
	this->connect (nombre, puerto);
}

// Conectarse a un destino conocido
bool FIEWA::PeerUDP::createServer()
{
	// Crear el socket
	this->setTimeout (5 * 1000);				//< 5 segundos de timeout
	this->server.open (0);
	//	this.server.setReuseAddress (true);		//< esto no es necesario en C++
	return true;
}

// Conectarse a un destino conocido
bool FIEWA::PeerUDP::connect (const FIEWA::DatosSTUN & datosSTUN)
{
	if (datosSTUN == FIEWA::DatosSTUN::npos)
	{
		return false;
	}
	return this->connect (datosSTUN.publicIP, datosSTUN.publicPort);
}

// Ajustar el timeout
void FIEWA::PeerUDP::setTimeout (size_t milisegundos)
{
	this->timeoutinseconds = milisegundos / 1000;
}

// Conectarse a un destino
bool FIEWA::PeerUDP::connect (const std::string & nombre, const size_t puerto)
{
	if (nombre == "")
	{
		return false;
	}
	// Obtener el valor del destino deseado
	this->remote_host = nombre;
	this->remote_port = puerto;

	// Se ha conectado correctamente
	this->connected = true;
	return true;
}

// Enviar mensaje del protocolo reconectándose (cosa que permite UDP)
bool FIEWA::PeerUDP::send (const Mensaje & mensajeEnviarAlSTUN,
						   const std::string & nombre,
						   const size_t puerto)
{
	// Enviar el mensaje indicado a ese destino
	if (this->send (mensajeEnviarAlSTUN.genMsg(), nombre, puerto) == false)
	{
		return false;
	}
	return true;
}

// Enviar mensaje del protocolo reconectándose (cosa que permite UDP) con datos empaquetados
bool FIEWA::PeerUDP::send (const Mensaje & mensajeEnviarAlSTUN,
						   const FIEWA::DatosSTUN & datosSTUN)
{
	// Enviar el mensaje indicado a ese destino
	if (this->send (mensajeEnviarAlSTUN.genMsg(), datosSTUN) == false)
	{
		return false;
	}
	return true;
}

// Enviar reconectándose (cosa que permite UDP)
bool FIEWA::PeerUDP::send (const std::string & messageStr, const std::string & nombre,
						   const size_t puerto)
{
	// Conectarse al nuevo destino indicado
	if (this->connect (nombre, puerto) == false)
	{
		return false;
	}
	// Enviar el mensaje indicado a ese destino
	if (this->send (messageStr) == false)
	{
		return false;
	}
	return true;
}

// Enviar reconectándose (cosa que permite UDP)
bool FIEWA::PeerUDP::send (const std::string & messageStr, const FIEWA::DatosSTUN & datosSTUN)
{
	// Conectarse al nuevo destino indicado
	if (this->connect (datosSTUN) == false)
	{
		return false;
	}
	// Enviar el mensaje indicado a ese destino
	if (this->send (messageStr) == false)
	{
		return false;
	}
	return true;
}

// Enviar mensaje del protocolo
bool FIEWA::PeerUDP::send (const Mensaje & mensajeEnviarAlSTUN)
{
	// Enviar el mensaje indicado a ese destino
	if (this->send (mensajeEnviarAlSTUN.genMsg()) == false)
	{
		return false;
	}
	return true;
}

// Enviar
bool FIEWA::PeerUDP::send (const std::string & messageStr)
{
	if (this->isConnected() == false)
	{
		return false;
	}
	if (messageStr == "")
	{
		return false;
	}
	// Proceder al envío del mensaje
	return this->server.enviar (messageStr, this->remote_host, this->remote_port);
}

// Recibir un mensaje
Mensaje FIEWA::PeerUDP::recibir()
{
	// Procesar el mensaje recibido como parte del protocolo
	return Mensaje (this->recibirStr());
}

// Recibir simplemente un string
std::string FIEWA::PeerUDP::recibirStr()
{
	std::string str;
	bool error = server.recibir (str, this->timeoutinseconds);
	if (error == false)
	{
		// Esta excepción se ignora completamente
		return "";
	}
	return str;
}

// Cerrar el socket
bool FIEWA::PeerUDP::close()
{
	this->server.close();
	this->connected = false;
	return true;
}

// Observador del estado
bool FIEWA::PeerUDP::isConnected() const
{
	return this->connected;
}
