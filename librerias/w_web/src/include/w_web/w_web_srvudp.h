/*********************************************************************************************
 *	Name		: _w_web_srvudp.h
 *	Description	: Servidor Web en UDP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_SRVUDP_H_
#define _W_WEB_SRVUDP_H_

#include "w_web/w_web_mensaje.h"
#include "w_red/w_red_srvudp.h"

class ServidorWebUDP : public ServidorUDP
{
public:
	// ASK: �M�s constructores?
	ServidorWebUDP();

public:
	// L�gica del servidor:
	bool send (const EventoServidorUDP &, const Mensaje & mensaje);

protected:
	// Recepci�n
	virtual bool analizaEventoMsgRecv (const EventoServidorUDP &);
	virtual void procesaMsgNoWebRecv (const EventoServidorUDP &);
	virtual void procesaMsgWebRecv (const EventoServidorUDP &, const Mensaje &);
	virtual Mensaje responderMsgWebRecv (const EventoServidorUDP &, const Mensaje &);

	// Emisi�n
	virtual void procesaMsgWebEnv (const EventoServidorUDP &, const Mensaje & peticion,
								   const Mensaje & respuesta);
};

#endif	//_W_WEB_SRVUDP_H_
