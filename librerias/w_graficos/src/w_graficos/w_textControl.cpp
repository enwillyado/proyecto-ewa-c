/*********************************************************************************************
 * Name			: w_textControl.cpp
 * Description	: Implementaci�n de la clase para gestionar un control de texto nativo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_textControl.h"

#include "w_graficos/w_lienzo_bmp.h"
#include "w_graficos/w_lienzo_auto.h"
#include "w_graficos/w_rgba.h"
#include "w_graficos/w_app.h"

#include "w_base/strToVector.hpp"

#include <map>

#define ALTO_CHAR  50
#define ANCHO_CHAR 20
#define FONT_SIZE	6

// ----------------------------------------------------------------------------
// Constructores
WTextControl::WTextControl (const std::string & iName)
	: WControl (iName),
	  shift_key (false), manejarEventos (true), textoEditable (false),
	  filaCursor (0), columnaCursor (0)
{
}

// ----------------------------------------------------------------------------
// Modificadores
void WTextControl::resize (const size_t ancho, const size_t alto)
{
	this->setLienzo (WLienzo::createWLienzo (ancho, alto, RGBA ("0xFFFFFF")));
	if (textoEditable == true)
	{
		this->getLienzo().setBorder (RGBA (0x00, 0x00, 0xFF));
	}
}

void WTextControl::setText (const std::string & str)
{
	const strvector & vect = ::strToVector (str, '\n');
#ifdef REUTILIZAR_FILAS_ANTERIORES
	for (size_t i = 0; i < vect.size() && i < this->filas.size(); i++)
	{
		// TODO: buscar filas iguales (o similares)
	}
#else
	this->filas = WTextControl::Filas (vect.size());
	for (size_t i = 0; i < vect.size(); i++)
	{
		this->filas[i].texto = vect[i];
		this->filas[i].lienzo = this->pintar (this->filas[i].texto);
	}
#endif
	// Colocar el cursor en la �ltima fila
	if (vect.size() > 0)
	{
		this->filaCursor = vect.size() - 1;
	}
}
void WTextControl::addChar (const char c)
{
	if (c == '\n')
	{
		this->filaCursor++;
	}

	// Actualizar el n�mero de filas
	if (this->filaCursor >= this->filas.size())
	{
		this->filas.resize (this->filaCursor + 1);
	}

	if (c == '\n')
	{
		// Este caracter especial no se a�ade
		return;
	}

	// El resto s�, calculando de nuevo el lienzo de esa l�nea
	this->filas[this->filaCursor].texto.push_back (c);
	this->filas[this->filaCursor].lienzo = this->pintar (this->filas[this->filaCursor].texto);
}
void WTextControl::remChar()
{
	if (this->filas[this->filaCursor].texto == "")
	{
		if (this->filas.size() == 0)
		{
			return;
		}
		this->filas.resize (this->filaCursor);
		if (this->filaCursor > 0)
		{
			this->filaCursor--;
		}
		return;
	}
	else
	{
		this->filas[this->filaCursor].texto.erase (this->filas[this->filaCursor].texto.size() - 1);
	}
	this->filas[this->filaCursor].lienzo = this->pintar (this->filas[this->filaCursor].texto);
}

// ----------------------------------------------------------------------------
// Manejadores
void WTextControl::onKeyDown (const unsigned int key)
{
	if (manejarEventos == false)
	{
		return;
	}
	if ((key >= 'A' && key <= 'Z') || (key >= '0' && key <= '9') || key == ' ')
	{
		if (key >= 'A' && key <= 'Z')
		{
			if (false == shift_key)
			{
				this->addChar (key - 'A' + 'a');
			}
			else
			{
				this->addChar (key);
			}
		}
		else if (key == '1')
		{
			if (false == shift_key)
			{
				this->addChar (key);
			}
			else
			{
				this->addChar ('!');
			}
		}
		else if (key == '2')
		{
			if (false == shift_key)
			{
				this->addChar (key);
			}
			else
			{
				this->addChar ('"');
			}
		}
		else if (key == '6')
		{
			if (false == shift_key)
			{
				this->addChar (key);
			}
			else
			{
				this->addChar ('&');
			}
		}
		else if (key == '8')
		{
			if (false == shift_key)
			{
				this->addChar (key);
			}
			else
			{
				this->addChar ('(');
			}
		}
		else if (key == '9')
		{
			if (false == shift_key)
			{
				this->addChar (key);
			}
			else
			{
				this->addChar (')');
			}
		}
		else if (key == '0')
		{
			if (false == shift_key)
			{
				this->addChar (key);
			}
			else
			{
				this->addChar ('=');
			}
		}
		else
		{
			this->addChar (key);
		}
		this->pintar();
	}
	else if (key == 16)
	{
		shift_key = true;
	}
	else
	{
		switch (key)
		{
		case 13:	// enter
		{
			this->addChar ('\n');
			this->pintar();
		}
		break;
		case 8:
		{
			if (this->filas.size() > 0)
			{
				this->remChar();
				this->pintar();
			}
		}
		break;
		case 219: // '''
		{
			if (false == shift_key)
			{
				this->addChar ('\'');
			}
			else
			{
				this->addChar ('?');
			}
			this->pintar();
		}
		break;
		case '{':
		case '(':
		case '[':
		case ';':
		case '"':
		case 188: // ','
		case 190: // '.'
		case ']':
		case ')':
		case '}':
		case '!':
		case '&':
		case '?':
		case 189: // '-'
		case 187: // '+'
		case '=':
		{
			this->addChar (key);
			this->pintar();
		}
		break;
		}
	}
	WControl::onKeyDown (key);
}

void WTextControl::onKeyUp (const unsigned int key)
{
	if (this->manejarEventos == false)
	{
		return;
	}
	switch (key)
	{
	case 16:
		shift_key = false;
		break;
	}
	WControl::onKeyUp (key);
}

void WTextControl::setManejarEventos (const bool manejarEventos)
{
	this->manejarEventos = manejarEventos;
}

// ----------------------------------------------------------------------------
// M�todos privados
WLienzo WTextControl::getLetra (const unsigned char c)
{
	static const WLienzoBMP & letras = WLienzoBMP::getWLienzoBMPFromFile ("../testdata/fuente.bmp");
	if (c >= 'A' && c <= 'Z')
	{
		const size_t desfase = c - 'A';
		return letras.sub (15 + ALTO_CHAR * desfase, 65 + 128 * 0, ANCHO_CHAR, ALTO_CHAR);
	}
	else if (c >= 'a' && c <= 'z')
	{
		const size_t desfase = c - 'a';
		return letras.sub (15 + ALTO_CHAR * desfase, 65 + 128 * 1, ANCHO_CHAR, ALTO_CHAR);
	}
	else if (c >= '0' && c <= '9')
	{
		const size_t desfase = c - '0';
		return letras.sub (15 + ALTO_CHAR * desfase, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
	}
	else
	{
		switch (c)
		{
		case '{':
			return letras.sub (15 + ALTO_CHAR * 10, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case '(':
			return letras.sub (15 + ALTO_CHAR * 11, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case '[':
			return letras.sub (15 + ALTO_CHAR * 12, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case ';':
			return letras.sub (15 + ALTO_CHAR * 13, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case '"':
			return letras.sub (15 + ALTO_CHAR * 14, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case 188: // ','
			return letras.sub (15 + ALTO_CHAR * 15, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case 190: // '.'
			return letras.sub (15 + ALTO_CHAR * 16, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case ']':
			return letras.sub (15 + ALTO_CHAR * 17, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case ')':
			return letras.sub (15 + ALTO_CHAR * 18, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case '}':
			return letras.sub (15 + ALTO_CHAR * 19, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case '!':
			return letras.sub (15 + ALTO_CHAR * 20, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case '&':
			return letras.sub (15 + ALTO_CHAR * 21, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case '?':
			return letras.sub (15 + ALTO_CHAR * 22, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case 189: // '-'
			return letras.sub (15 + ALTO_CHAR * 23, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case 187: // '+'
			return letras.sub (15 + ALTO_CHAR * 24, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		case '=':
			return letras.sub (15 + ALTO_CHAR * 25, 65 + 128 * 2, ANCHO_CHAR, ALTO_CHAR);
		}
	}
	return WLienzo::createWLienzo (ANCHO_CHAR, ALTO_CHAR, RGBA (0, 0, 0, 0));
}

WLienzo WTextControl::getLetra (const unsigned char c, const size_t fontSize)
{
	typedef std::map<size_t, WLienzo> WLienzosLetrasTam;
	typedef std::map<const unsigned char, WLienzosLetrasTam> WLienzosLetras;
	static WLienzosLetras letras;
	WLienzosLetras::iterator itr = letras.find (c);
	if (itr != letras.end())
	{
		WLienzosLetrasTam::iterator itr2 = itr->second.find (fontSize);
		if (itr2 != itr->second.end())
		{
			return itr2->second;
		}
	}
	const WLienzo & letra = WTextControl::getLetra (c);
	const WLienzo & ret = letra.resize (fontSize, (ALTO_CHAR * fontSize) / ANCHO_CHAR);
	letras[c][fontSize] = ret;
	return ret;
}

void WTextControl::pintar()
{
	WLienzoAuto mapa;
	size_t altoAcum = 0;
	for (size_t n = 0; n < this->filas.size(); n++)
	{
		const WLienzo & lienzo = this->filas[n].lienzo;
		mapa.unir (lienzo, 0, altoAcum);
		if (this->filas[n].texto != "")
		{
			altoAcum += lienzo.getAlto();
		}
		else
		{
			altoAcum += (ALTO_CHAR * FONT_SIZE) / ANCHO_CHAR;
		}
	}
	this->resize (mapa.getAncho() + 2, altoAcum + 2);

	// Colocar el lienzo correspondiente y repintar root
	this->getLienzo().unir (mapa, 1, 1);
	this->WControl::pintar();
}

WLienzo WTextControl::pintar (const std::string & str)
{
	WLienzoAuto mapa;
	for (size_t n = 0; n < str.size(); n++)
	{
		const unsigned char c = str[n];
		const WLienzo & letra = WTextControl::getLetra (c, FONT_SIZE);
		mapa.unir (letra, n * letra.getAncho(), 0);
	}

	// Devolver la fila correspondiente
	WLienzo ret (mapa.getAncho() + 2, mapa.getAlto() + 2);
	ret.unir (mapa, 1, 1);
	return ret;
}
