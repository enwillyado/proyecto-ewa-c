/*********************************************************************************************
 * Name			: w_multicontrol.cpp
 * Description	: Implementaci�n de la clase para gestionar un control m�ltiple
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_multicontrol.h"

// Constructores
WMultiControl::WMultiControl (const std::string & iName)
	: WControl (iName),
	  controles(), solicitadoPintar (true),
	  lastMouseOver (NULL), lastLMouseDown (NULL), lastMMouseDown (NULL), lastRMouseDown (NULL)
{
}

WMultiControl::WMultiControl (const WMultiControl & org)
	: WControl (org),
	  controles(), solicitadoPintar (true),
	  lastMouseOver (NULL), lastLMouseDown (NULL), lastMMouseDown (NULL), lastRMouseDown (NULL)
{
}

WMultiControl::~WMultiControl()
{
	this->destructor();
}

void WMultiControl::destructor()
{
	for (size_t i = 0; i < this->controles.size(); i++)
	{
		this->deletePtr (i);
	}
	this->controles.clear();
}
void WMultiControl::deletePtr (const size_t position)
{
	if (this->controles[position].ptr != NULL)
	{
		if (this->controles[position].deletable == true)
		{
			delete this->controles[position].ptr;
		}
		this->controles[position].ptr = NULL;
	}
}

// Operadores
void WMultiControl::operator= (const WMultiControl & org)
{
	WControl::operator= (org);
}

// Insertores
size_t WMultiControl::insertarPtr (WControl* const control, const bool deletable)
{
	WControlPair par = {control, deletable};
	this->controles.push_back (par);
	if (control != NULL)
	{
		// Inicializar el control interno
		this->initInsertPtr (this->controles.back().ptr);
	}

	// Devolver posici�n de insercci�n
	return this->controles.size() - 1;
}
size_t WMultiControl::insertarPtr (WControl* const control, const bool deletable, const size_t position)
{
	if (this->controles.size() <= position)
	{
		const WControlPair parInicial = {NULL, false};
		this->controles.resize (position + 1, parInicial);
	}
	this->deletePtr (position);

	// Insertarlo
	const WControlPair par = {control, deletable};
	this->controles[position] = par;

	// Inicializar
	if (control != NULL)
	{
		// Inicializar el control interno
		this->initInsertPtr (this->controles[position].ptr);
	}

	// Devolver posici�n de insercci�n
	return position;
}

void WMultiControl::initInsertPtr (WControl* internalControlPtr)
{
	WControl & internalControl = * (internalControlPtr);

	// Set parent
	internalControl.setParent (*this);

	// Set root (si tiene)
	if (this->haveRootPtr() == true)
	{
		internalControl.setRoot (this->getRoot());
	}

	// Pintado inicial
	internalControl.recomponerLienzo();
	internalControl.pintar();
}

WControl & WMultiControl::obtener (const size_t iPos)
{
	return *ASSERT_IN_DEBUG (this->obtenerPtr (iPos));
}
WControl & WMultiControl::obtener (const std::string & iName, const bool recursivo)
{
	return *ASSERT_IN_DEBUG (this->obtenerPtr (iName, recursivo));
}

WControl* WMultiControl::obtenerPtr (const size_t iPos)
{
	if (this->controles.size() > iPos)
	{
		return this->controles [iPos].ptr;
	}
	return NULL;
}
WControl* WMultiControl::obtenerPtr (const std::string & iName, const bool recursivo)
{
	for (size_t iPos = 0; iPos < this->controles.size(); iPos++)
	{
		WControl* ptr = (this->controles[iPos].ptr);
		if (ptr != NULL)
		{
			if (ptr->getName() == iName)
			{
				return ptr;
			}
		}
	}
	if (true == recursivo)
	{
		for (size_t iPos = 0; iPos < this->controles.size(); iPos++)
		{
			WControl* ptr = (this->controles[iPos].ptr->obtenerPtr (iName, recursivo));
			if (ptr != NULL)
			{
				if (ptr->getName() == iName)
				{
					return ptr;
				}
			}
		}
	}
	return NULL;
}

const WControlesPtr & WMultiControl::getControlesPtr() const
{
	return this->controles;
}
WControlesPtr & WMultiControl::getControlesPtr()
{
	return this->controles;
}

void WMultiControl::setRoot (WApp & iWapp)
{
	this->WControlJerarquizable::setRoot (iWapp);
	for (size_t i = 0; i < this->controles.size(); i++)
	{
		WControl* ptr = (this->controles[i].ptr);
		if (ptr != NULL)
		{
			ptr->setRoot (this->getRoot());
		}
	}
}

void WMultiControl::setControlesPtr (const WControlesPtr & controles)
{
	this->controles = controles;
}

void WMultiControl::pintar()
{
	this->solicitadoPintar = true;
	this->WControl::pintar();
}

WLienzo & WMultiControl::recomponerLienzo()
{
	WLienzo & lienzoFinal = this->WControl::getLienzo();

	// Verificar si hay que recomponer
	if (this->solicitadoPintar == true)
	{
		// Desactivar recomposici�n
		this->solicitadoPintar = false;

		// Unirle los componentes
		for (size_t i = 0; i < this->controles.size(); i++)
		{
			if (this->controles[i].ptr != NULL)
			{
				WControl & control = * (this->controles[i].ptr);
				const WLienzo & lienzoControl = control.recomponerLienzo();
				lienzoFinal.unir (lienzoControl, control.getPosX(), control.getPosY());
			}
		}
	}

	return lienzoFinal;
}

void WMultiControl::clear()
{
	this->destructor();
	this->lastMouseOver = NULL;
	this->lastLMouseDown = NULL;
	this->lastMMouseDown = NULL;
	this->lastRMouseDown = NULL;

	// Limpiar tambi�n el control b�sico
	this->WControl::clear();
}
