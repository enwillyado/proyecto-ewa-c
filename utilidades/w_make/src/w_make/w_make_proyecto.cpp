/*********************************************************************************************
 *	Name		: w_make_proyecto.cpp
 *	Description	: Implementanción de las funciones para preparar una orden PROYECTO
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_make/w_make.h"

#include "w_arbol/w_arbolxml.h"

#include "w_base/strTransform.hpp"
#include "w_base/strToVector.hpp"
#include "w_base/filesStr.hpp"

class VisualStudioParserStatus
{
public:
	std::string procesarCadena (const std::string & cadena) const
	{
		std::string ret = cadena;
		::remplazar (ret, '\\', '/');
		::remplazar (ret, "$(SolutionDir)", getSolutionDir());
		::remplazar (ret, "$(ProjectDir)", getProjectDir());
		::remplazar (ret, "$(ProjectName)", getProjectName());
		::remplazar (ret, "$(Configuration)", configuration);
		::remplazar (ret, "%(AdditionalIncludeDirectories)", getAdditionalIncludeDirectories());
		return ret;
	}

	std::string getSolutionDir() const
	{
		if ("" != solutionDir)
		{
			return solutionDir;
		}
		return getProjectDir();
	}
	std::string getProjectDir() const
	{
		if ("" != projectDir)
		{
			return projectDir;
		}
		return "./";
	}
	std::string getProjectName() const
	{
		return projectName;
	}
	std::string getProjectTarget() const
	{
		if ("" != projectTarget)
		{
			return projectTarget;
		}
		return getProjectName();
	}
	std::string getAdditionalIncludeDirectories() const
	{
		// TODO: por ahora no se implementa
		return "";
	}
	strvector getDirs (const std::string & strdir) const
	{
		strvector ret = ::strToVector (strdir, ';');
		for (size_t i = 0; i < ret.size(); i++)
		{
			ret[i] = this->procesarCadena (ret[i]);
		}
		return ret;
	}

	// Área de datos
	std::string solutionDir;
	std::string projectDir;
	std::string projectName;
	std::string projectTarget;
	std::string configuration;
};

WMake::Estado WMake::prepararPro()
{
	const Arbol & project = ArbolXML::getArbolFromFile (this->ficheroOrden);
	if (project.getLastError() != Arbol::SIN_ERROR)
	{
		return WMake::NO_EXISTE_FICHERO;
	}
	if (project.getClave() != "Project")
	{
		return WMake::ORDEN_MAL_FORMADA;
	}

	// Preparar la orden
	WMake makeOrden;
	makeOrden.tipoOrden = WMake::ORDEN;
	makeOrden.parametros = this->parametros;

	//Inicializar el acumulador del estado del parser
	VisualStudioParserStatus visualStudioParser;
	visualStudioParser.projectDir = "./";
	visualStudioParser.projectName = WFilesStr::getNombreOnly (this->ficheroOrden);
	visualStudioParser.configuration = "Release"; //< TODO: asignar la adecuada

	// Leer datos
	for (size_t i = 0; i < project.hijosize(); i++)
	{
		const Arbol & dato = project[i];
		const std::string & tipoDato = dato.getClave();
		if (tipoDato == "ItemGroup")
		{
			const std::string & etiqueta = dato % "Label";
			if (etiqueta == "ProjectConfigurations")
			{
				// TODO: sacar las configuraciones (por lo general DEBUG, RELEASE)
			}
			else
			{
				for (size_t iDato = 0; iDato < dato.hijosize(); iDato++)
				{
					const Arbol & elemento = dato[iDato];
					const std::string & tipoElemento = elemento.getClave();
					if (tipoElemento == "ClCompile")
					{
						const std::string & valorItem = elemento % "Include";
						// TODO: hacer absoluto si NO lo es
						makeOrden.parametros.pushSrcOrg (valorItem);
					}
				}
			}
		}
		else if (tipoDato == "PropertyGroup")
		{
			const std::string & condicion = dato % "Condition";
			// TODO: filtrar por condicion
			for (size_t iDato = 0; iDato < dato.hijosize(); iDato++)
			{
				const Arbol & elemento = dato[iDato];
				const std::string & tipoElemento = elemento.getClave();
				if (tipoElemento == "ConfigurationType")
				{
					const std::string & valorItem = elemento.getValor();
					if (valorItem == "StaticLibrary")
					{
						makeOrden.parametros.tipo = WMakeParametros::LIBRERIA_LIB;
					}
					else if (valorItem == "Application")
					{
						makeOrden.parametros.tipo = WMakeParametros::EJECUTABLE;
					}
					else
					{
						// TODO: soportar bibliotecas (.dll)
						return WMake::ORDEN_MAL_FORMADA;
					}
				}
				else if (tipoElemento == "OutDir")
				{
					// TODO: hacer absoluto si NO lo es
					const std::string & valorItem = elemento.getValor();
					const std::string & valorItemReal = visualStudioParser.procesarCadena (valorItem);
					makeOrden.parametros.dirBinDestino = valorItemReal;
				}
				else if (tipoElemento == "IntDir")
				{
					// TODO: hacer absoluto si NO lo es
					const std::string & valorItem = elemento.getValor();
					const std::string & valorItemReal = visualStudioParser.procesarCadena (valorItem);
					makeOrden.parametros.dirObjDestino = valorItemReal;
				}
				else
				{
					// ignorado
				}
			}
		}
		else if (tipoDato == "ItemDefinitionGroup")
		{
			const std::string & condicion = dato % "Condition";
			// TODO: filtrar por condicion
			for (size_t iDato = 0; iDato < dato.hijosize(); iDato++)
			{
				const Arbol & elemento = dato[iDato];
				const std::string & tipoElemento = elemento.getClave();
				if (tipoElemento == "ClCompile")
				{
					for (size_t iElemento = 0; iElemento < elemento.hijosize(); iElemento++)
					{
						const Arbol & subelemento = elemento[iElemento];
						const std::string & claveSubelemento = subelemento.getClave();
						if (claveSubelemento == "AdditionalIncludeDirectories")
						{
							// TODO: hacer absoluto si NO lo es
							const std::string & valorSubElemento = subelemento.getValor();
							const strvector & includeDirectories = visualStudioParser.getDirs (valorSubElemento);
							for (size_t i = 0; i < includeDirectories.size(); i++)
							{
								const std::string includeDirectory = includeDirectories[i];
								if ("" != includeDirectory)
								{
									makeOrden.parametros.carpetasInclusion.push_back (includeDirectory);
								}
							}
						}
						else
						{
							// ignorado
						}
					}
				}
				else if (tipoElemento == "Link")
				{
					// ignorado
				}
				else if (tipoElemento == "Lib")
				{
					for (size_t iElemento = 0; iElemento < elemento.hijosize(); iElemento++)
					{
						const Arbol & subelemento = elemento[iElemento];
						const std::string & claveSubelemento = subelemento.getClave();
						if (claveSubelemento == "AdditionalLibraryDirectories")
						{
							// TODO: hacer absoluto si NO lo es
							const std::string & valorSubElemento = subelemento.getValor();
							const strvector & includeLibDirectories =
								visualStudioParser.getDirs (valorSubElemento);
							for (size_t i = 0; i < includeLibDirectories.size(); i++)
							{
								const std::string includeLibDirectory = includeLibDirectories[i];
								if ("" != includeLibDirectory)
								{
									makeOrden.parametros.carpetasInclusionLib.push_back (includeLibDirectory);
								}
							}
						}
						else
						{
							// ignorado
						}
					}
				}
				else
				{
					// ignorado
				}
			}
		}
		else
		{
			// ignorado
		}
	}

	// Ajustar el nombre del target
	makeOrden.parametros.nombre = visualStudioParser.getProjectTarget();

	return makeOrden.preparar();
}
