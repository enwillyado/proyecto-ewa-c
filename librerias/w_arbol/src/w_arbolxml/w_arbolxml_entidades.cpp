/*********************************************************************************************
 *	Name		: w_arbolxml_entidades.cpp
 *	Description	: Trabajo con entidades y escritura en ficheros
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_arbol/w_arbolxml.h"

#include "w_base/files.hpp"

/**
 *	Codifica en entidades html la cadena dada
 *	\param	[in]	value	cadena a codificar
 *  \retval	la cadena dada codificada para ser usada por Arbol
 **/
std::string ArbolXML::htmlEntitiesDecode (const std::string & value)
{
	return value;
}

/**
 *	Codifica en entidades html la cadena dada
 *	\param	[in]	value	cadena a codificar
 *  \retval	la cadena dada codificada con las entidades html
 **/
std::string ArbolXML::htmlEntitiesEncode (const std::string & value)
{
	return value;
}

/**
 *	Recorta una cadena dada eliminando todos los caracteres "DELIMITADORES_TRIM" por delante y por detr�s
 *	\param	[in]	valor	cadena a recortar
 *  \retval	 la cadena recortada
 **/
std::string ArbolXML::trim (const std::string & valor)
{
	std::string ret = valor.substr (0, valor.find_last_not_of (DELIMITADORES_TRIM) + 1);
	if (ret != "")
	{
		ret = ret.substr (ret.find_first_not_of (DELIMITADORES_TRIM));
	}
	return ret;
}

/**
 *	Determina si un fichero existe y puede ser abierto
 *	\param	[in]	filename	ruta del fichero que se desea comprobar
 *  \retval	TRUE si existe y se puede abrir; FALSE en caso contrario
 **/
bool ArbolXML::file_exists_and_can_be_read_from (const std::string & filename)
{
	return ::file_exists_and_can_be_read_from (filename);
}

/**
 *	Obtiene el contenido del fichero dado si existe
 *	\param	[in]	filename	ruta del fichero que se desea obtener
 *  \retval	el contenido del fichero dado si existe
 **/
std::string ArbolXML::leerContenidoFile (const std::string & filename)
{
	return ::leerContenidoFile (filename);
}

/**
 *	Devuelve el valor del Arbol indicado en formato XML plano
 *	\param	[in]	conSaltosDeLinea	(opt.=TRUE)	si se desea que tenga saltos de l�nea (\n) cuando tenga hijos
 *	\param	[in]	nivelTab			(opt.=0)	n�mero de "\t" al comienzo de cada l�nea
 *  \retval	 TRUE si se ha guardado bien, o FALSE si no
 **/
bool ArbolXML::saveToFile (const std::string & file, bool conSaltosDeLinea)
{
	const std::string & contenido = this->genXML (conSaltosDeLinea);
	return ::ponerContenidoFile (file, contenido);
}
