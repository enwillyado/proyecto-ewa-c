/*********************************************************************************************
 *	Name		: _w_red_peer_udp_cmd_ejemplo.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_PEER_UDP_CMD_EJEMPLO_PRAGMALIB_H_
#define _W_RED_PEER_UDP_CMD_EJEMPLO_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_red/_w_red_client.pragmalib.h"
#include "w_red/_w_red_server.pragmalib.h"

#endif

#endif // _W_RED_PEER_UDP_CMD_EJEMPLO_PRAGMALIB_H_
