/*********************************************************************************************
 *	Name		: _w_web_clitcp.h
 *	Description	: Cliente web en UPD
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_CLIUDP_H_
#define _W_WEB_CLIUDP_H_

#include "w_web/w_web_mensaje.h"
#include "w_red/w_red_cliudp.h"

class ClienteWebUDP : public ClienteUDP
{
public:
	// ASK: �M�s constructores?
	ClienteWebUDP();

public:
	//---------------------------------------------------------------------------
	// Env�o a alto nivel de los mensajes
	bool enviar_recibir (const Mensaje & mensajeEnviar, Mensaje & respuesta, unsigned int segundosEsperados = 0);

public:
	//---------------------------------------------------------------------------
	// Env�o a bajo nivel de mensajes
	bool enviar (const Mensaje & mensaje);
	bool recibir (Mensaje & mensaje, unsigned int segundosEsperados = 0);
};

#endif	//_W_WEB_CLIUDP_H_
