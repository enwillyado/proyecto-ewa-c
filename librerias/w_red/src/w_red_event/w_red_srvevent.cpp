/*********************************************************************************************
 *	Name		: _w_red_srvevent.cpp
 *	Description	: Implementación de un evento recibido por un Servidor TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/

#include "w_red/w_red_srvevent.h"

EventoServidorTCP::EventoServidorTCP()
	: EventoTCP()
{
}

EventoServidorUDP::EventoServidorUDP()
	: EventoUDP()
{
}
