/*********************************************************************************************
 * Name			: w_lienzo.h
 * Description	: Clase para manipular una representación gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_LIENZO_H_
#define _W_LIENZO_H_

#include "w_graficos/w_lienzoBase.h"

#include <string>

// FWD
class WControl;

class WLienzo : public WLienzoBase
{
public:
	typedef unsigned char* PixelesInt;
	typedef const unsigned char* const Pixeles;

	typedef unsigned char* AlphasInt;
	typedef const unsigned char* const Alphas;

public:
	// Constructores
	WLienzo (const size_t ancho = 0, const size_t alto = 0);
	WLienzo (const WLienzo &);
	explicit WLienzo (const WLienzoBase &);

	static WLienzo createWLienzo (const size_t ancho, const size_t alto, const RGBA &);

	~WLienzo();

	// Operadores
	void operator= (const WLienzo &);

	// Accesores
	size_t getAncho() const;
	size_t getAlto() const;

	RGBA getPixel (const size_t x, const size_t y) const;
	Pixeles getPixeles() const;
	Pixeles getPixeles (const int fila) const;
	Alphas getAlphas() const;
	Alphas getAlphas (const int fila) const;

	// Modificadores
	void reserve (const size_t ancho, const size_t alto);

	bool setPixel (const size_t x, const size_t y, const RGBA &);
	void setPixeles (Pixeles &);
	void setPixeles (Pixeles &, const int fila);

	void unir (const WLienzo & lienzo, const size_t xPos, const size_t yPos);
	void unir (const WLienzoBase & lienzo, const size_t xPos, const size_t yPos);
	void unir (const WControl & control);

protected:
	void reserveIntern();
	size_t ancho;
	size_t alto;

private:
	virtual RGBA getPixelFast (const size_t x, const size_t y) const;
	virtual void setPixelFast (const size_t x, const size_t y, const RGBA &);

	class WLienzoIntern;
	WLienzoIntern & data;
};

#endif // _W_LIENZO_H_
