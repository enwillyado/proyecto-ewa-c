/*********************************************************************************************
 * Name			: w_web_services_thread.h
 * Description	: Servicios que son capaces de responder a las peticiones recibidas con hilos
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRV_SERVICES_THREAD_H_
#define _W_RED_SRV_SERVICES_THREAD_H_

#include "w_web/w_web_services_base.h"		//< para tener el "BasicSrvService"
#include "w_red/w_red_srvevent.h"			//< para tener el "EventoServidorTCP" y "Evento"

#include "dsystem/dsystem_thread.h"			//< para tener el manejador de hilos

class ServidorTCP; //< FWD: para evitar incluir "w_web/w_web_srvtcp.h"

// Esqueleto de un servicio con hilos
class ServicioConHilo : public BasicSrvService
{
public:
	ServicioConHilo (ServidorTCP* server);
	ServicioConHilo (const ServicioConHilo &);

public:
	struct DatosParaElHilo
	{
		const EventoServidorTCP evento;
		const Mensaje peticion;
		const ServicioConHilo* servicio;
	};

	// Servicio para ejecutarse desde el cat�logo
	virtual Mensaje ejecutarServicio (const Evento & evento, const Mensaje & peticion) const;

	// Funci�n para ejecutarse por un hilo (modo multi-hilo)
	virtual Mensaje ejecutarServicioMultiHilo (const Evento & evento, const Mensaje & peticion) const;

	// Funci�n de carga del hilo creado
	static void ejecutarServicioMonoHilo (const DsystemThreadData* const threadData);

	// Funci�n para ejecutarse por un hilo (modo mono-hilo)
	virtual Mensaje ejecutarServicioFinal (const Evento & evento, const Mensaje & peticion) const = 0;

protected:
	ServidorTCP* server;
};

#endif	//_W_RED_SRV_SERVICES_THREAD_H_
