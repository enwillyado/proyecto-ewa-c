/*********************************************************************************************
*	Name		: macros_variadic.hpp
*	Description	: Macros para trabajar con las macros con argumentos variables
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _MACROS_VARIADIC_HPP_
#define _MACROS_VARIADIC_HPP_

// Expand
#define PP_EXPAND(x) x

// Stringice
#define PP_STRINGIZE(text) PP_STRINGIZE_A((text))
#define PP_STRINGIZE_A(arg) PP_STRINGIZE_I arg
#define PP_STRINGIZE_I(text) #text

// Concatenate
#define PP_CONCATENATE(arg1, arg2)   PP_CONCATENATE_I(arg1, arg2)
#define PP_CONCATENATE_I(arg1, arg2) arg1##arg2

// Size
#define PP_TUPLE_SIZE(...) PP_CONCATENATE(\
		PP_TUPLE_SIZE_I(__VA_ARGS__,\
						33, 32, 31, 30, 29, 28, 27, 26, 25, 24,\
						23, 22, 21, 20, 19, 18, 17, 16, 15, 14,\
						13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1,),)
#define PP_TUPLE_SIZE_I(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9,\
						e10, e11, e12, e13, e14, e15, e16, e17, e18, e19,\
						e20, e21, e22, e23, e24, e25, e26, e27, e28, e29,\
						e30, e31, e32, size, ...) size

// For each
#define  PP_FOR_EACH_1(what, x, ...) what(x)
#define  PP_FOR_EACH_2(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define  PP_FOR_EACH_3(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define  PP_FOR_EACH_4(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define  PP_FOR_EACH_5(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define  PP_FOR_EACH_6(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define  PP_FOR_EACH_7(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define  PP_FOR_EACH_8(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define  PP_FOR_EACH_9(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_10(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_11(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_12(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_13(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_14(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_15(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_16(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_17(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_18(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_19(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_20(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_21(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_22(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_23(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_24(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_25(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_26(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_27(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_28(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_29(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_30(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_31(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))
#define PP_FOR_EACH_32(what, x, ...) what(x)\
	PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, PP_TUPLE_SIZE(__VA_ARGS__))(what, __VA_ARGS__))

#define PP_FOR_EACH_(N, what, ...) PP_EXPAND(PP_CONCATENATE(PP_FOR_EACH_, N)(what, __VA_ARGS__))
#define PP_FOR_EACH(what, ...) PP_FOR_EACH_(PP_TUPLE_SIZE(__VA_ARGS__), what, __VA_ARGS__)

// Iterate
#define  PP_ITERATE_1(what, N, x, ...) what(x, N)
#define  PP_ITERATE_2(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define  PP_ITERATE_3(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define  PP_ITERATE_4(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define  PP_ITERATE_5(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define  PP_ITERATE_6(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define  PP_ITERATE_7(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define  PP_ITERATE_8(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define  PP_ITERATE_9(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_10(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_11(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_12(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_13(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_14(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_15(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_16(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_17(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_18(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_19(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_20(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_21(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_22(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_23(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_24(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_25(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_26(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_27(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_28(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_29(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_30(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_31(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))
#define PP_ITERATE_32(what, N, x, ...) what(x, N)\
	PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, PP_TUPLE_SIZE(__VA_ARGS__))\
			  (what, PP_TUPLE_SIZE(__VA_ARGS__), __VA_ARGS__))

#define PP_ITERATE_(N, what, ...) PP_EXPAND(PP_CONCATENATE(PP_ITERATE_, N)(what, N, __VA_ARGS__))
#define PP_ITERATE(what, ...) PP_ITERATE_(PP_TUPLE_SIZE(__VA_ARGS__), what, __VA_ARGS__)

#endif	//_MACROS_VARIADIC_HPP_
