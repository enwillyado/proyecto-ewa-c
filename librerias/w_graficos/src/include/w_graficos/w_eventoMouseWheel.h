/*********************************************************************************************
 * Name			: w_eventoMouseWheel.h
 * Description	: Evento para los desencadenadores de rat�n
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_EVENTO_MOUSE_WHEEL_H_
#define _W_EVENTO_MOUSE_WHEEL_H_

#include "w_graficos/w_eventoMouse.h"

class WEventoMouseWheel : public WEventoMouse
{
public:
	typedef void (*CbEventoMouseWheel) (const WEventoMouseWheel &);

	enum Direccion
	{
		ARRIBA,
		ABAJO
	};

public:
	// Constructor
	WEventoMouseWheel (WControl & iControl, const Direccion & iDireccion)
		: WEventoMouse (iControl), direccion (iDireccion)
	{
	}

	const Direccion & direccion;
};

#endif // _W_EVENTO_MOUSE_WHEEL_H_
