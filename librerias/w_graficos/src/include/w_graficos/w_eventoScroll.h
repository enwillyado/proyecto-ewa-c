/*********************************************************************************************
 * Name			: w_eventoScroll.h
 * Description	: Evento para los desencadenadores de Scrolles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_EVENTO_SCROLL_H_
#define _W_EVENTO_SCROLL_H_

#include "w_graficos/w_evento.h"

class WEventoScroll : public WEvento
{
public:
	typedef void (*CbEventoScroll) (const WEventoScroll &);

public:
	// Constructor
	WEventoScroll (WControl & iControl)
		: WEvento (iControl)
	{
	}
};

#endif // _W_EVENTO_SCROLL_H_
