/*********************************************************************************************
 *	Name		: w_sistema_execs_bi.h
 *	Description	: Clase para llamar a un comando de forma bi-direccional
 *	Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _SISTEMA_EXECS_BI_H_
#define _SISTEMA_EXECS_BI_H_

#include "w_sistema/w_sistema_execs_base.h"

class SistemaExecsBi : public SistemaExecsBase
{
public:
	SistemaExecsBi();

	// Inicio
	Errores		exeComando();
	bool		putEntrada (const std::string & valor);

	// Postmortem
	virtual const std::string & getSalidaComando() const;
	std::string getPartialSalidaComando() const;

private:
#ifdef WIN32
	typedef void* HANDLE;
	HANDLE g_hChildStd_IN_Wr;
	HANDLE g_hChildStd_OUT_Rd;
#else
	int filedes[2];
#endif
};

#endif // _SISTEMA_EXECS_BI_H_
