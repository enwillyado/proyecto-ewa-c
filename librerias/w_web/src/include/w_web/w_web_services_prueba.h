/*********************************************************************************************
 * Name			: w_web_services_prueba.h
 * Description	: Mecanismo para responder a un determinado servicio que ejecutar�
 *					el servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_SERVICES_PRUEBA_H_
#define _W_WEB_SERVICES_PRUEBA_H_

#include "w_web/w_web_services_base.h"	//< para tener la base

#include "w_web/w_web_services_prueba_id.h"	//< para tener los id de los servicios
// NOTA: recordar a�adir en este otro archivo los nuevos servicios implementados

#include <string>
#include <map>

// ----------------------------------------------------------------------------
// Servicio para realizar pruebas con servicios extendidos
class BasicSrvService_PruebaA : public BasicSrvService
{
public:
	BasicSrvService_PruebaA() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_PruebaA();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PRUEBA_A_ID;
	}
};
class BasicSrvService_PruebaB : public BasicSrvService
{
public:
	BasicSrvService_PruebaB() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_PruebaB();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PRUEBA_B_ID;
	}
};
class BasicSrvService_PruebaC : public BasicSrvService
{
public:
	BasicSrvService_PruebaC() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_PruebaC();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PRUEBA_C_ID;
	}
};
class BasicSrvService_PruebaD : public BasicSrvService
{
public:
	BasicSrvService_PruebaD() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_PruebaD();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PRUEBA_D_ID;
	}
};
class BasicSrvService_PruebaE : public BasicSrvService
{
public:
	BasicSrvService_PruebaE() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_PruebaE();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PRUEBA_E_ID;
	}
};
class BasicSrvService_PruebaF : public BasicSrvService
{
public:
	BasicSrvService_PruebaF() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_PruebaF();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PRUEBA_F_ID;
	}
};
class BasicSrvService_PruebaG : public BasicSrvService
{
public:
	BasicSrvService_PruebaG() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_PruebaG();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PRUEBA_G_ID;
	}
};
class BasicSrvService_PruebaH : public BasicSrvService
{
public:
	BasicSrvService_PruebaH() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_PruebaH();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PRUEBA_H_ID;
	}
};
class BasicSrvService_IPandPORT : public BasicSrvService
{
public:
	BasicSrvService_IPandPORT() : BasicSrvService() {};
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_IPandPORT();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_IPANDPORT_ID;
	}
};

#endif	//_W_WEB_SERVICES_PRUEBA_H_
