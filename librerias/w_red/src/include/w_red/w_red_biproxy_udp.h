/*********************************************************************************************
*	Name		: w_red_biproxy_udp.h
*	Description	: Clase para implementar un biproxy de red basado en UDP
* Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_BIPROXY_UDP_H_
#define _W_RED_BIPROXY_UDP_H_

#include "w_red/w_red_proxy_udp.h"

// Clase bi-proxy
class BiProxyUDP : public ProxyUDP
{
public:
	BiProxyUDP (const std::string & remoteHost, const int remotePort, const int biLocalPort);
	virtual void onClienteGetData (const EventoServidorUDP & eventoClienteUDP, const std::string & mensaje);

public:
	static void funcionHilo (const WThread & hilo);

protected:
	static void biServidor (ServidorUDP & servidor);

private:
	EventoServidorUDP eventoBiClienteUDP;
	std::string mensaje0;
	ServidorUDP biServidorUDP;
};

#endif //_W_RED_BIPROXY_UDP_H_
