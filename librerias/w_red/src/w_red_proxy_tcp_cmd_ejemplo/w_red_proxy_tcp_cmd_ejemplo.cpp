/*********************************************************************************************
 *	Name		: _w_red_proxy_tcp_cmd_ejemplo.cpp
 *	Description	: Prueba de ejemplo de un Proxy TCP a una direcci�n por
 *					defecto y un puerto por defecto
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_red_proxy_tcp_cmd_ejemplo.pragmalib.h"

#include "w_red/w_red_biproxy_tcp.h"

#include <iostream>

#define NOMBRE_DE_SERVIDOR		"localhost"
#define PUERTO_EN_SERVIDOR		52013		// TCP
#define PUERTO_EN_PROXY			52010
#define PUERTO_BI_PROXY			80

// Funci�n manejadora para cada cliente
typedef TWThreadData<Proxy*> TWThreadDataProxy;
static void funcionHilo (const WThread & hilo)
{
	const TWThreadDataProxy & c = hilo.getData<TWThreadDataProxy>();
	c.data->start();
	delete c.data;
}

// Funci�n principal
int main()
{
	// Abrir un bi-proxy tcp
	{
		TWThreadDataProxy hiloData (new BiProxyTCP (NOMBRE_DE_SERVIDOR, PUERTO_EN_SERVIDOR, PUERTO_BI_PROXY));
		if (true == hiloData.data->open (PUERTO_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto local " << PUERTO_EN_PROXY << std::endl;
		}
	}

	// Avisar
	std::cout << "Enganchado al puerto " << PUERTO_EN_SERVIDOR << " en " << NOMBRE_DE_SERVIDOR << std::endl;
	std::cout << "Abierto el puerto local " << PUERTO_EN_PROXY << " en el proxy" << std::endl;
	std::cout << "Abierto el puerto local " << PUERTO_BI_PROXY << " en el biproxy" << std::endl;

	// Esperar
	std::cout << std::endl;
	std::cout << "Presiona una tecla para finalizar (abruptamente)" << std::endl;
	std::cin.get();

	return 0;
}
