/*********************************************************************************************
*	Name		: w_red_bypass_udp.h
*	Description	: Clase para implementar un by-pass de red basado en UDP
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_BY_PASS_UDP_H_
#define _W_RED_BY_PASS_UDP_H_

#include "w_red/w_red_srvudp.h"

#include "w_sistema/w_sistema_thread.h"

#include "w_base/interfaces/iStartable.h"

#include <string>

class ByPassUDP : public IStartable
{
public:
	ByPassUDP (const int localPort);
	virtual bool open (const int byPassPort);
	virtual bool start();

protected:
	// CallBack manejador
	static void funcionHilo (const WThread & hilo);
	static void staticCallBackRemote (ServidorUDP & servidor);
	virtual void callBackRemote();
	static void staticCallBackByPass (ServidorUDP & servidor);
	virtual void callBackByPass();

	// Manejadores de datos del proxy
	virtual void onRemoteGetData (const EventoServidorUDP & evento, const std::string & mensaje);
	virtual void onByPassPutData (const EventoServidorUDP & evento, const std::string & mensaje);

private:
	EventoServidorUDP eventoRemote;
	ServidorUDP servidorToRemoteUDP;

	EventoServidorUDP eventoByPass;
	ServidorUDP servidorToByPassUDP;

	class DatosCliente;
	typedef std::map<SocketID, DatosCliente*> DatosClientesUDP;
	DatosClientesUDP clientesUDP;
};


#endif //_W_RED_BY_PASS_UDP_H_
