/*********************************************************************************************
 *	Name		: w_sistema_timer.cpp
 *	Description	: Trabajo con temporizadores
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema_timer.h"

#include "w_sistema/w_sistema_time.h"
#include <cstring>	//< NULL

// ---------------------------------------------------------------------------
// Clase interna
#ifdef WIN32
#include <Windows.h>
#else
#include <pthread.h>
#include <errno.h>
#endif

class WTimer::Data
{
public:
	enum RespuestaEvento
	{
		TIME_OUT,
		RECEPCCION_EVENTO,
		ERRONEO,
	};

	Data()
	{
#ifdef WIN32
		InitializeCriticalSection (&this->mutex);
		InitializeConditionVariable (&this->condicion);
#else
		pthread_mutex_init (&this->mutex, NULL);
		pthread_mutex_init (&this->cond_mutex, NULL);
		pthread_cond_init (&this->condicion, NULL);
#endif
	}
	~Data()
	{
#ifdef WIN32
		DeleteCriticalSection (&this->mutex);
#else
		pthread_mutex_destroy (&this->mutex);
		pthread_mutex_destroy (&this->cond_mutex);
		pthread_cond_destroy (&this->condicion);
#endif
	}

	// ---------------------------------------------------------------------------
	// Emite evento
	void emiteEventoInterruptor()
	{
#ifdef WIN32
		WakeAllConditionVariable (&this->condicion);
#else
		pthread_cond_signal (&this->condicion);
#endif
	}

	// Esperas a eventos
	RespuestaEvento esperaEventoInterrumpible (const size_t milisegundos)
	{
#ifdef WIN32
		EnterCriticalSection (&this->mutex);
		const int estado = SleepConditionVariableCS (&this->condicion, &this->mutex, milisegundos);
		LeaveCriticalSection (&this->mutex);
		if (estado == 0)
		{
			const int error = GetLastError();
			if (error == ERROR_TIMEOUT)
			{
				return WTimer::Data::TIME_OUT;
			}
			return WTimer::Data::ERRONEO;
		}
		return WTimer::Data::RECEPCCION_EVENTO;
#else
		// TODO: implementar la espera condicional
		struct timespec timeToWait;
		clock_gettime (CLOCK_REALTIME, &timeToWait);
		timeToWait.tv_sec += (milisegundos / 1000);
		timeToWait.tv_nsec += (milisegundos % 1000) * 1000 * 1000;
		const int estado = pthread_cond_timedwait (&this->condicion, &this->cond_mutex, &timeToWait);
		pthread_mutex_unlock (&this->cond_mutex);
		if (estado == 0)
		{
			return WTimer::Data::RECEPCCION_EVENTO;
		}
		else if (estado == ETIMEDOUT)
		{
			return WTimer::Data::TIME_OUT;
		}
		else
		{
			return WTimer::Data::ERRONEO;
		}
#endif
	}
	RespuestaEvento esperaEvento()
	{
#ifdef WIN32
		const RespuestaEvento estado = this->esperaEventoInterrumpible (INFINITE);
		return estado;
#else
		// TODO: implementar gestor de errores
		pthread_mutex_lock (&this->cond_mutex);
		pthread_cond_wait (&this->condicion, &this->cond_mutex);
		pthread_mutex_unlock (&this->cond_mutex);
		return WTimer::Data::ERRONEO;
#endif
	}

	// ---------------------------------------------------------------------------
	// Secciones cr�ticas
	void entraSeccionCritica()
	{
#ifdef WIN32
		EnterCriticalSection (&this->mutex);
#else
		pthread_mutex_lock (&this->mutex);
#endif
	}
	void salSeccionCritica()
	{
#ifdef WIN32
		LeaveCriticalSection (&this->mutex);
#else
		pthread_mutex_unlock (&this->mutex);
#endif
	}

private:
	// ---------------------------------------------------------------------------
	// �rea de datos
#ifdef WIN32
	CONDITION_VARIABLE condicion;
	CRITICAL_SECTION mutex;
#else
	pthread_cond_t condicion;
	pthread_mutex_t cond_mutex;
	pthread_mutex_t mutex;
#endif

public:
	// ----------------------------------------------------------------------------
	// Funci�n est�tica peri�dica
	static void functionPeriodica (const WThread & ptrData)
	{
		WTimer* timer = ptrData.getData<WTimer::TWThreadDataTimer>().data;
		while (timer->isStoped() == false)
		{
			const WTimer::Data::RespuestaEvento respuesta = timer->data.esperaEventoInterrumpible (timer->getMilisengundos());
			switch (respuesta)
			{
			case WTimer::Data::TIME_OUT:
			{
				// Revisar parada en secci�n cr�tica
				timer->data.entraSeccionCritica();
				if (timer->isStoped() == false)
				{
					timer->executeFunction();
				}
				timer->data.salSeccionCritica();
			}
			break;

			case WTimer::Data::RECEPCCION_EVENTO:
			{
				// Se�al recibida, finalizaci�n
				return;
			}

			case WTimer::Data::ERRONEO:
			default:
			{
				// TODO: procesar otros posibles errores
				return;
			}
			}
		}
	}
};

// ----------------------------------------------------------------------------
// WTimer:
// Funci�n que no hace nada
void WTimer::functionTimerNull (WTimer & timer)
{
}

// Constructor
WTimer::WTimer (WTimer::FunctionTimer fun, const size_t milisegundos, void* dataPtr)
	: thread (&WTimer::Data::functionPeriodica), data (*new WTimer::Data)
{
	this->thread.setData (WTimer::TWThreadDataTimer (this));
	this->functionTimer = fun;
	this->milisegundos = milisegundos;
	this->dataPtr = dataPtr;
}

// Destructor
WTimer::~WTimer()
{
	if (this->isStarted() == true && this->isStoped() == false)
	{
		// Pedir que finalice
		this->stop();

#ifdef ESPERA_ACTIVA
		// Y esperar a que finalice el timer
		while (true)
		{
			// Ver que est� en ejecuci�n
			const bool result = this->isStarted();
			if (result == false)
			{
				break; //salir de la espera activa
			}
			else
			{
				// TODO: �mejorar el tipo de espera?
				WTime::esperarMilisegundos (this->getMilisengundos() + 100); //< esperar un poco
			}
		}
#endif
	}

	// Liberar �rea de datos
	delete &data;
}

// Observadores
bool WTimer::isStarted() const
{
	return this->thread.isStarted();
}
bool WTimer::isStoped() const
{
	return this->thread.isStoped();
}
size_t WTimer::getMilisengundos() const
{
	return this->milisegundos;
}

void* WTimer::getDataPtr()
{
	return this->dataPtr;
}
void WTimer::setDataPtr (void* dataPtr)
{
	this->dataPtr = dataPtr;
}

// Ejecutores
void WTimer::executeFunction()
{
	this->functionTimer (*this);
}

// Solicita el reiniciado de la cuenta
WThread::Estado WTimer::start()
{
	return this->thread.start();
}

// Solicita el reiniciado de la cuenta
WThread::Estado WTimer::restart()
{
	return WThread::ERRONEO; //this->thread.restart();
}

// Solicita la detenci�n programada al hilo
WThread::Estado WTimer::pause()
{
	return WThread::ERRONEO; //this->thread.pause();
}

// Solicita la continuaci�n al hilo
WThread::Estado WTimer::resume()
{
	return WThread::ERRONEO; //this->thread.resume();
}

// Solicita la finalizaci�n y destrucci�n programada al hilo
WThread::Estado WTimer::stop()
{
	// Ajustar parada en secci�n cr�tica
	this->data.entraSeccionCritica();
	const WThread::Estado ret = this->thread.stop();
	this->data.salSeccionCritica();

	// Revisar si fue bien...
	if (ret == WThread::SIN_ERROR)
	{
		// Avisar al hilo para que se detenga
		this->data.emiteEventoInterruptor();

		// Esperar que retorne
		return this->thread.finish();
	}
	return ret;
}

// Detiene de forma abrupta y destruye el hilo
WThread::Estado WTimer::end()
{
	// Finalizar hilo del timer en secci�n cr�tica
	this->data.entraSeccionCritica();
	const WThread::Estado ret = this->thread.end();
	this->data.salSeccionCritica();
	return ret;
}
