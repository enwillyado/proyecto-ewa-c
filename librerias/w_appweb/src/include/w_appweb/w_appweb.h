/*********************************************************************************************
 * Name			: w_appweb.h
 * Description	: Clase para gestionar una WAppWeb
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_APPWEB_H_
#define _W_APPWEB_H_

#include "w_web/w_web_srvtcp.h"

#include <string>

class WAppWeb
{
public:
	// Constructores
	WAppWeb();

	bool loadaWe (const std::string & fichero);
	bool start (const std::string & uri = "/", const std::string & port = "80");

	// Servicio de respuesta
	std::string getRespuesta();
	Mensaje procesaMsgWebRecv (const Mensaje & mensajeRecibido);

private:


private:
	// �rea de datos
	ServidorWebTCP server;
	std::string fichero;
	std::string respuesta;
};

#endif // _W_APPWEB_H_
