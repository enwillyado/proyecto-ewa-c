/*********************************************************************************************
 * Name			: w_controlKeyable.cpp
 * Description	: Implementación de los desencadenadores de teclas para controles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_controlKeyable.h"

#include "w_graficos/w_eventoKey.h"
#include "w_graficos/w_control.h"
#include "w_graficos/w_app.h"

#include <cstring>

WControlKeyable::WControlKeyable()
	: cbsOnKeyDown(), cbsOnKeyUp()
{
}

void WControlKeyable::clear()
{
	this->cbsOnKeyDown.clear();
	this->cbsOnKeyUp.clear();
}

// ---------------------------------------------------------------------------
// onKeyDown
void WControlKeyable::onKeyDown (const unsigned int key)
{
	if (this->cbsOnKeyDown.size() > 0)
	{
		WEventoKey evento (this->getSelf(), key);
		for (size_t i = 0; i < this->cbsOnKeyDown.size(); i++)
		{
			if (this->cbsOnKeyDown[i] != NULL)
			{
				this->cbsOnKeyDown[i] (evento);
			}
		}
	}
}

size_t WControlKeyable::setOnKeyDown (WControlKeyable::OnKeyDown cbOnKeyDown)
{
	if (cbOnKeyDown != NULL)
	{
		this->cbsOnKeyDown.push_back (cbOnKeyDown);
		return this->cbsOnKeyDown.size() - 1;
	}
	this->cbsOnKeyDown.clear();
	return -1;
}

bool WControlKeyable::unsetOnKeyDown (const size_t index)
{
	if (index >= this->cbsOnKeyDown.size())
	{
		return false;
	}
	this->cbsOnKeyDown[index] = NULL;
	return true;
}

// ---------------------------------------------------------------------------
// onKeyUp
void WControlKeyable::onKeyUp (const unsigned int key)
{
	if (this->cbsOnKeyUp.size() > 0)
	{
		WEventoKey evento (this->getSelf(), key);
		for (size_t i = 0; i < this->cbsOnKeyUp.size(); i++)
		{
			if (this->cbsOnKeyUp[i] != NULL)
			{
				this->cbsOnKeyUp[i] (evento);
			}
		}
	}
}

size_t WControlKeyable::setOnKeyUp (WControlKeyable::OnKeyUp cbOnKeyUp)
{
	if (cbOnKeyUp != NULL)
	{
		this->cbsOnKeyUp.push_back (cbOnKeyUp);
		return this->cbsOnKeyUp.size() - 1;
	}
	this->cbsOnKeyUp.clear();
	return -1;
}

bool WControlKeyable::unsetOnKeyUp (const size_t index)
{
	if (index >= this->cbsOnKeyUp.size())
	{
		return false;
	}
	this->cbsOnKeyUp[index] = NULL;
	return true;
}
