/*********************************************************************************************
 *	Name		: w_ar_drone_video.cpp
 *	Description	: Implementanci�n de la clase para el gestor de v�deo (streaming) del AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones/w_ar_drone_video.h"

#include "ewadrones/ewadrones.h"

#define ANCHO_VIDEO		400
#define ALTO_VIDEO		300

//#define LENTO

// ---------------------------------------------------------------------------
// Gestor del v�deo externa
extern "C"
{
	int mainVideoFile (void* iPtr);

	void init (void* iPtr, const int width, const int height)
	{
		ArDroneVideo* ptr = (ArDroneVideo*) (iPtr);
		ptr->init (width, height);
	}
	void send (void* iPtr, const unsigned char* const pBase, const int width, const int y)
	{
		ArDroneVideo* ptr = (ArDroneVideo*) (iPtr);
		ptr->send (pBase, width, y);
	}
	void end (void* iPtr)
	{
		ArDroneVideo* ptr = (ArDroneVideo*) (iPtr);
		ptr->end();
	}
}

void ArDroneVideo::init (const int width, const int height)
{
	// Ajustar el tama�o del lienzo
	this->lienzo.reserve (width, height);
}

void ArDroneVideo::send (const unsigned char* const pBase, const int width, const int y)
{
	// Ajustar los valores de los p�xiles recibidos (para la fila y-�sima del ancho dado)
#ifdef LENTO
	const unsigned char* p = pBase;
	for (int x = 0; x < width; x++)
	{
		const RGBA c (* (p + 2), * (p + 1), * (p + 0));
		p += 3;
		lienzo.setPixel (x, y, c);
	}
#else
	this->lienzo.setPixeles (pBase, y);
#endif
}
void ArDroneVideo::end()
{
	this->onNewLienzo();
}

// ---------------------------------------------------------------------------
// Constructor
ArDroneVideo::ArDroneVideo (EWadrones* ptr)
	: arDrone (ptr), videoSrc()
{
}

void ArDroneVideo::setPtrArDrone (EWadrones* ptrArDrone)
{
	this->arDrone = ptrArDrone;
}

// ---------------------------------------------------------------------------
// Arranque del sistema
void ArDroneVideo::initVideoFile (const WThread & hilo)
{
	ArDroneVideo* ptr = hilo.getData<ArDroneVideo::TWThreadDataArDroneVideo>().data;
	::mainVideoFile ((void*) (ptr));
}

void ArDroneVideo::init()
{
	this->hiloVideo.setData (TWThreadDataArDroneVideo (this));
	this->hiloVideo.setFunction (&ArDroneVideo::initVideoFile);
#ifdef INICIO_AUTOMATICO
	this->start();
#endif
}

void ArDroneVideo::start()
{
	this->hiloVideo.start();
}
void ArDroneVideo::stop()
{
	this->hiloVideo.stop();
}

// ---------------------------------------------------------------------------
// Manejador
void ArDroneVideo::setOnNewLienzo (ArDroneVideo::OnNewLienzo cbOnNewLienzo)
{
	this->cbOnNewLienzo = cbOnNewLienzo;
}

void ArDroneVideo::onNewLienzo()
{
	if (this->cbOnNewLienzo != NULL)
	{
		this->cbOnNewLienzo (this->lienzo);
	}
}
