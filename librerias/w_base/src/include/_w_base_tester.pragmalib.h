/*********************************************************************************************
 *	Name		: _w_base_tester.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar el tester
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_BASE_TESTER_PRAGMALIB_H_
#define _W_BASE_TESTER_PRAGMALIB_H_

#ifdef _MSC_VER

// Parte privada

#endif

#undef _W_BASE_TESTER_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_BASE_TESTER_PRAGMALIB_H_
