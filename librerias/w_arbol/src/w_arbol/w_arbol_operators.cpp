/*********************************************************************************************
 *	Name		: w_arbol_operators.cpp
 *	Description	: Arbol para trabajar m�s c�modamente con un xml en memoria, usando std
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_arbol/w_arbol.h"

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Operadores <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

/**
 *	Devuelve una referencia (constante o no) al elemento hijo en la posici�n dada
 *	\param	[in]	index	posici�n [0,n) que se desea obtener
 *  \retval	 la referencia esperada, o el propio arbol con un "lastError" con el fallo
 *				Ojo: hay que limpiar el "lastError" con un ".clearError()" si se detecta y
 *				se desea continuar trabajando con el arbol.
 **/
Arbol & Arbol::operator[] (IndexHijo index)
{
	if (index < this->hijosize())
	{
		return hijos[index];
	}
	// TODO: �tendr�a sentido rellenar de nuevos hijos hasta la posici�n
	//			indicada como hace JavaScript con los "Arrays"?
	//			De esta forma, se devolver�a la referencia al �ltimo
	//			elemento correspondiente al �ndice solicitado.
	//			El problema es que se crear�an hijos, que quiz�s no se
	//			deberian crear, malgasando espacio y consumiendo recursos.
	//			Adem�s, el formato "xml" en si no distingue elementos por
	//			su posici�n; �es eso un motivo suficiente como para dar error?
	this->lastError = NO_EXISTE_HIJO;
	return *this;	//< se devuelve una referencia al propio arbol
}
const Arbol & Arbol::operator[] (IndexHijo index) const
{
	if (index < this->hijosize())
	{
		return hijos[index];
	}
	this->lastError = NO_EXISTE_HIJO;
	return *this;	//< se devuelve una referencia al propio arbol
}

/**
 *	Devuelve una referencia (constante o no) al primer elemento hijo con la clave dada
 *	\param	[in]	indexstr	clave del hijo buscado
 *  \retval	 la referencia esperada, o el propio arbol con un "lastError" con el fallo
 *				Ojo: hay que limpiar el "lastError" con un ".clearError()" si se detecta y
 *				se desea continuar trabajando con el arbol.
 **/
Arbol & Arbol::operator[] (const std::string & indexstr)
{
	for (size_t index = 0; index < hijos.size(); index++)
	{
		if (hijos[index].clave == indexstr)
		{
			return hijos[index];
		}
	}
	// TODO: �tendr�a sentido a�adir un hijo con la calve buscada?
	//			De esta forma, se devolver�a la referencia a ese elemento.
	//			El problema es que se podr�an a�adir luego hijos que no
	//			cumplan con la condici�n de que su clave sea la buscada.
	//			Por lo tanto, se opta por no a�adir y dar error.
	//			Para poder a�adir hijos, se debe usar ".addHijo(...)".
	this->lastError = NO_EXISTE_HIJO;
	return *this;	//< se devuelve una referencia al propio arbol
}
const Arbol & Arbol::operator[] (const std::string & indexstr) const
{
	for (size_t index = 0; index < hijos.size(); index++)
	{
		if (hijos[index].clave == indexstr)
		{
			return hijos[index];
		}
	}
	this->lastError = NO_EXISTE_HIJO;
	return *this;	//< se devuelve una referencia al propio arbol
}

/**
 *	Devuelve una referencia (constante o no) al atributo solicitado
 *	\param	[in]	indexstr	clave del atributo buscado
 *  \retval	 la referencia esperada.
 *				Si no existe, da error si se pide constante;
 *					y crea el atributo, si se pide no-const
 *					devolviendo posteriormente la referencia esperada.
 **/
const std::string & Arbol::operator% (const std::string & indexstr) const
{
	Atributos::const_iterator itr = atributos.find (indexstr);
	if (itr != atributos.end())
	{
		return itr->second;
	}
	this->lastError = NO_EXISTE_ATRIBUTO;
	return Arbol::nocampo;
}
std::string & Arbol::operator% (const std::string & indexstr)
{
	Atributos::iterator itr = atributos.find (indexstr);
	if (itr == atributos.end())
	{
		//< crear un atributo nuevo
		std::pair<std::string, std::string> foo;
		foo.first = indexstr;
		foo.second = std::string ("");
		itr = atributos.insert (foo).first;
	}
	return itr->second;	//< devolver la referencia al atributo (existente o nuevo)
}

/**
 *	Concatena en en una copia del propio Arbol los atributos del otro dado (sin recursividad)
 *	\param	[in]	otroArbol	el otro arbol del que se incluir�n los atributos en el retornado
 *  \retval	 el nuevo arbol
 **/
Arbol Arbol::operator% (const Arbol & otroArbol) const
{
	Arbol ret = *this;
	for (Atributos::const_iterator itr = otroArbol.atributos.begin(); itr != otroArbol.atributos.end(); itr++)
	{
		ret % itr->first = itr->second;
	}
	return ret;
}

/**
 *	Concatena en el propio Arbol los atributos del (sin recursividad)
 *	\param	[in]	otroArbol	el otro arbol del que se incluir�n los atributos en el retornado
 *  \retval	 el propio arbol (para permitir concatenar)
 **/
Arbol & Arbol::operator%= (const Arbol & otroArbol)
{
	for (Atributos::const_iterator itr = otroArbol.atributos.begin(); itr != otroArbol.atributos.end(); itr++)
	{
		this->operator% (itr->first) = itr->second;
	}
	return *this;
}

/**
 *	Determina si dos arboles son iguales o no
 *	\param	[in]	otro	el otro arbol a comparar
 *  \retval	 TRUE si son iguales (completamente, incluyendo el encoding); o FALSE en caso contrario
 **/
bool Arbol::operator== (const Arbol & otro) const
{
	return this->equals (otro);
}

/**
 *	Determina si dos arboles son distintios o no
 *	\param	[in]	otro	el otro arbol a comparar
 *  \retval	 TRUE si son distintos (completamente, incluyendo el encoding); o FALSE en caso contrario
 **/
bool Arbol::operator!= (const Arbol & otro) const
{
	return false == this->operator== (otro);
}

/**
 *	Determina si dos arboles son iguales o no
 *	\param	[in]	otro	el otro arbol a comparar
 *  \retval	 TRUE si son iguales (completamente, incluyendo el encoding); o FALSE en caso contrario
 **/
bool Arbol::equals (const Arbol & otro) const
{
	return this->clave == otro.clave && this->atributos == otro.atributos && this->hijos == otro.hijos;
}
