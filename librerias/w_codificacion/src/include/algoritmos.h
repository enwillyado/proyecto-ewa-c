/*********************************************************************************************
 *	Name		: algoritmos.c
 *	Description	: Algoritmos comunes a todas las encriptaciones / calculos de hash
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _ALGORITMOS_H_
#define _ALGORITMOS_H_

// Transforma un buffer binario a una cadena de caractareces (en hexadecimal) may�scula
// Incluye el '\0' al final de la cadena
void binToChar (const void* bufferIn, char* bufferOut, unsigned int sizeBufferIn);

#endif // _ALGORITMOS_H_
