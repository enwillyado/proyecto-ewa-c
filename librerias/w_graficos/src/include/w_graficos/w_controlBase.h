/*********************************************************************************************
 * Name			: w_controlBase.h
 * Description	: Interface para gestionar un control con parte gr�fica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_CONTROL_BASE_H_
#define _W_CONTROL_BASE_H_

// FWD
class WControl;

class WControlBase
{
public:
	virtual ~WControlBase() {}
	virtual WControl & getSelf() = 0;
	virtual const WControl & getSelf() const = 0;
};

#endif // _W_CONTROL_BASE_H_
