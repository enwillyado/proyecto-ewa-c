/*********************************************************************************************
 * Name			: w_graficos_tester.cpp
 * Description	: Este fichero sirve para configurar las pruebas unitarias que se realizarán
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_appweb_tester.pragmalib.h"

#include "w_appweb/w_appweb.h"

#include <assert.h>

int main()
{
	WAppWeb appWeb;

	// Finalización correcta
	return 0;
}
