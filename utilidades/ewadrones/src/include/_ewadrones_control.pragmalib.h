/*********************************************************************************************
 *	Name		: _ewadrones_control.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar la aplicaci�n de control para EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_CONTROL_PRAGMALIB_H_
#define _EWADRONES_CONTROL_PRAGMALIB_H_

#ifdef _MSC_VER

// Parte privada
#include "ewadrones/_ewadrones.pragmalib.h"

#include "w_graficos/_w_graficos.pragmalib.h"

#include "zxing/_w_zxing.pragmalib.h"

#endif

#undef _EWADRONES_CONTROL_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _EWADRONES_CONTROL_PRAGMALIB_H_
