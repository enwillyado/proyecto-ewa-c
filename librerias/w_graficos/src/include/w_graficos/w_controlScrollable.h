/*********************************************************************************************
 * Name			: w_controlScrollable.h
 * Description	: Desencadenadores de eventos para Scrolles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_CONTROL_SCROLLABLE_H_
#define _W_CONTROL_SCROLLABLE_H_

#include "w_graficos/w_eventoScroll.h"

#include "w_graficos/w_controlBase.h"

class WControlScrollable : public WControlBase
{
public:
	// Constructor
	WControlScrollable();

public:
	// ---------------------------------------------------------------------------
	// onScrollUp
	virtual void onScrollUp();

	typedef WEventoScroll::CbEventoScroll OnScrollUp;
	void setOnScrollUp (OnScrollUp cbOnRMouseDrag);

	// onScrollDown
	virtual void onScrollDown();

	typedef WEventoScroll::CbEventoScroll OnScrollDown;
	void setOnScrollDown (OnScrollDown cbOnMMouseDrag);

private:
	// �rea de desencadenadores
	OnScrollUp cbOnScrollUp;
	OnScrollDown cbOnScrollDown;

private:
	// �rea privada de datos
};

#endif // _W_CONTROL_SCROLLABLE_H_
