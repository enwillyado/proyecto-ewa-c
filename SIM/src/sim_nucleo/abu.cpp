#include "sim/abu.h"
#include <cstring>

ABU::ABU()
{
}

const bool ABU::accesoRegistros (const bool K, const bool Z, const bool U, const bool V, const bool W)
{
	if (U == 0)
	{
		if (V == 0)
		{
			if (W == 0)
			{
				return 0;				// 0  0  0 => Siempre es 0 (solo lectura)
			}
			else
			{
				this->O = K;		// 0  0  1 => O = K (solo escritura)
				if (Z == 1)
				{
					this->ejecutaABU();
				}
				return K;
			}
		}
		else
		{
			if (W == 0)
			{
				this->P = K;		// 0  1  0 => P = K (solo escritura)
				if (Z == 1)
				{
					this->ejecutaABU();
				}
				return K;
			}
			else
			{
				if (Z == 1)
				{
					this->ejecutaABU();
				}
				return this->X;			// 0  1  1 => X (solo lectura)
			}
		}
	}
	else
	{
		if (V == 0)
		{
			if (W == 0)
			{
				if (Z == 1)
				{
					this->ejecutaABU();
				}
				return this->Y;			// 1  0  0 => Y (solo lectura)
			}
			else
			{
				this->N = K;		// 1  1  0 => N = K (solo escritura)
				if (Z == 1)
				{
					this->ejecutaABU();
				}
				return K;
			}
		}
		else
		{
			if (W == 0)
			{
				this->M = K;		// 1  1  0 => M = K (solo escritura)
				if (Z == 1)
				{
					this->ejecutaABU();
				}
				return K;
			}
			else
			{
				return 1;				// 1  1  1 => Siempre es 1 (solo lectura)
			}
		}
	}
}
void ABU::ejecutaABU()
{
	if (this->O == 0 && this->P == 0)
	{
		this->X = ABU::abu_or (this->M, this->N);
		this->Y = ABU::abu_nor (this->M, this->N);
	}
	else if (this->O == 0 && this->P == 1)
	{
		this->X = ABU::abu_nxor (this->M, this->N);
		this->Y = ABU::abu_nand (this->M, this->N);
	}
	else if (this->O == 1 && this->P == 0)
	{
		this->X = ABU::abu_not (this->M);
		this->Y = ABU::abu_not (this->N);
	}
	else
	{
		this->X = ABU::abu_xor (this->M, this->N);
		this->Y = ABU::abu_and (this->M, this->N);
	}
}

const bool ABU::abu_or (const bool a, const bool b)
{
	if (a == 0 && b == 0)
	{
		return 0;
	}
	else if (a == 0 && b == 1)
	{
		return 1;
	}
	else if (a == 1 && b == 0)
	{
		return 1;
	}
	else if (a == 1 && b == 1)
	{
		return 1;
	}
	return NULL;
}
const bool ABU::abu_nor (const bool a, const bool b)
{
	if (a == 0 && b == 0)
	{
		return 1;
	}
	else if (a == 0 && b == 1)
	{
		return 0;
	}
	else if (a == 1 && b == 0)
	{
		return 0;
	}
	else if (a == 1 && b == 1)
	{
		return 0;
	}
	return NULL;
}
const bool ABU::abu_nxor (const bool a, const bool b)
{
	if (a == 0 && b == 0)
	{
		return 1;
	}
	else if (a == 0 && b == 1)
	{
		return 0;
	}
	else if (a == 1 && b == 0)
	{
		return 0;
	}
	else if (a == 1 && b == 1)
	{
		return 1;
	}
	return NULL;
}
const bool ABU::abu_nand (const bool a, const bool b)
{
	if (a == 0 && b == 0)
	{
		return 1;
	}
	else if (a == 0 && b == 1)
	{
		return 1;
	}
	else if (a == 1 && b == 0)
	{
		return 1;
	}
	else if (a == 1 && b == 1)
	{
		return 0;
	}
	return NULL;
}
const bool ABU::abu_not (const bool a)
{
	if (a == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
	return NULL;
}
const bool ABU::abu_xor (const bool a, const bool b)
{
	if (a == 0 && b == 0)
	{
		return 0;
	}
	else if (a == 0 && b == 1)
	{
		return 1;
	}
	else if (a == 1 && b == 0)
	{
		return 1;
	}
	else if (a == 1 && b == 1)
	{
		return 0;
	}
	return NULL;
}
const bool ABU::abu_and (const bool a, const bool b)
{
	if (a == 0 && b == 0)
	{
		return 0;
	}
	else if (a == 0 && b == 1)
	{
		return 0;
	}
	else if (a == 1 && b == 0)
	{
		return 0;
	}
	else if (a == 1 && b == 1)
	{
		return 1;
	}
	return NULL;
}

