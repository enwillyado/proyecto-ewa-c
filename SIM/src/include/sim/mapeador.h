#ifndef _MAPEADOR_H_
#define _MAPEADOR_H_

#include "sim/ids.h"

typedef unsigned int Tamanio;
typedef Tamanio Direccion;

class MAP
{
public:
	MAP();
	~MAP();

	bool & dameRegistro (const bool puntero[PAGINAS][COMPONT]);
	bool & dameRegistro (const Direccion direccion);

	static const Direccion dameDireccion (const bool puntero[PAGINAS][COMPONT]);
private:
	bool* c;
	bool o;
	Tamanio t;
};

#endif	//_MAPEADOR_H_
