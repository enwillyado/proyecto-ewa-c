/*********************************************************************************************
 * Name			: w_app.h
 * Description	: Clase que gestiona una aplicaci�n gr�fica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_APP_H_
#define _W_APP_H_

#include "w_graficos/w_app_functores.h"

#include "w_graficos/w_lienzo.h"
#include "w_graficos/w_multicontrol.h"

#include "w_graficos/w_controlFocusable.h"

class Arbol;
class WApp : public WMultiControl, public WControlFocusable
{
public:
	class WAppIntern;

public:
	WApp (const std::string & iName = "", size_t ancho = 0, size_t alto = 0);
	~WApp();

public:
	bool start();
	bool stop();

	// Observadores
	const WControl & getCursor() const;
	WControl & getCursor();

	const WLienzo & getFondo() const;
	WLienzo & getFondo();

	// Modificadores
	bool setFondo (const WLienzo &);
	bool setCursor (const WLienzo &);
	bool setCursor (const WControl &);

	bool setMouse (const int posX, const int posY);

	virtual bool loadXW (const std::string & filename, const AppFunctores & = AppFunctores::GetNull());
	virtual bool loadXW (const Arbol & filename, const AppFunctores & = AppFunctores::GetNull());

	void setSize (const size_t ancho, const size_t alto);
	virtual WLienzo & recomponerLienzo();
	bool rePosAndDim();

	virtual void pintar();
	void solicitarPintar();
	bool pintarInternal();

	virtual WControl & getSelf();
	virtual const WControl & getSelf() const;

protected:
	// �rea de datos protegida
	WLienzo fondo;
	WControl mouse;

private:
	int init (const size_t ancho, const size_t alto);
	int initInternal();
	void destructor();

	bool setCursorInternal();
	bool rePosAndDimInternal();

protected:
	// �rea de datos privada
	WAppIntern & data;
};

#endif // _W_APP_H_
