/*********************************************************************************************
 *	Name		: _w_jpeg.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_JPEG_PRAGMALIB_H_
#define _W_JPEG_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_base/_pragmalib_base.h"

// Propia librer�a: (para no autoengancharla si es una dll)
#pragma comment(lib, "w_jpeg" FIN_LIB)

// Dependencias
#include "w_graficos/_w_imagenes_base.pragmalib.h"

#endif

#undef _W_JPEG_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_JPEG_PRAGMALIB_H_
