/*********************************************************************************************
 * Name			: w_multicontrol.h
 * Description	: Clase para gestionar un control con parte m�ltiple
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_MULTICONTROL_H_
#define _W_MULTICONTROL_H_

#include "w_graficos/w_app_functores.h"

#include "w_graficos/w_control.h"

#ifdef _DEBUG
#include "w_base/asserts.hpp"
#define ASSERT_IN_DEBUG ::assertIfNull
#else
#define ASSERT_IN_DEBUG
#endif

class Arbol;
class WMultiControl : public WControl
{
public:
	// Constructores
	WMultiControl (const std::string & iName = "");
	WMultiControl (const WMultiControl & org);

	// Destructores
	~WMultiControl();
	void destructor();

	// Operadores
	void operator= (const WMultiControl & org);

	// Observadores
	template<class T>
	T & obtenerAs (const size_t iPos)
	{
		WControl* control = this->obtenerPtr (iPos);
		return * ASSERT_IN_DEBUG (static_cast<T*> (control));
	}
	template<class T>
	T & obtenerAs (const std::string & iName, const bool recursivo = false)
	{
		WControl* control = this->obtenerPtr (iName, recursivo);
		return * ASSERT_IN_DEBUG (static_cast<T*> (control));
	}

	WControl* obtenerPtr (const size_t iPos);
	WControl* obtenerPtr (const std::string & iName, const bool recursivo = false);

	WControl & obtener (const size_t iPos);
	WControl & obtener (const std::string & iName, const bool recursivo = false);

	const WControlesPtr & getControlesPtr() const;
	WControlesPtr & getControlesPtr();

	// Modificadores
	template<class T>
	T & insertarCopia (const T & org)
	{
		const size_t pos = this->insertarPtr (new T (org), true);
		return this->obtenerAs<T> (pos);
	}
	template<class T>
	size_t insertar (const T & org)
	{
		return this->insertarPtr (new T (org), true);
	}
	template<class T>
	size_t insertar (T & org)
	{
		// TODO: �permitir el swap de T?
		return this->insertarPtr (new T (org), true);
	}
	virtual size_t insertarPtr (WControl* const, const bool deletable = true);
	virtual size_t insertarPtr (WControl* const, const bool deletable, const size_t position);

	// Cargar una maquetaci�n
	virtual bool loadXW (const Arbol & filename, const AppFunctores & = AppFunctores::GetNull());

	void deletePtr (const size_t position);

	void setControlesPtr (const WControlesPtr & controles);
	virtual void setRoot (WApp & iWapp);

	virtual void pintar();
	virtual WLienzo & recomponerLienzo();

	// Eliminar todos los controles de este multicontrol
	virtual void clear();

	// Eventos
	virtual void onMoveMouseOver();
	virtual void onMoveMouseExt();
	virtual void onMoveMouseOut();
	virtual void onLUpMouseOver();
	virtual void onLDownMouseOver();
	virtual void onMUpMouseOver();
	virtual void onMDownMouseOver();
	virtual void onRUpMouseOver();
	virtual void onRDownMouseOver();
	virtual void onWheelMouseOver (const WEventoMouseWheel::Direccion);

	virtual void onLUpMouseOut();
	virtual void onMUpMouseOut();
	virtual void onRUpMouseOut();

	virtual void onKeyDown (const unsigned int key);
	virtual void onKeyUp (const unsigned int key);

protected:
	// �rea de datos protegida
	void initInsertPtr (WControl*);
	WControlesPtr controles;

	bool solicitadoPintar;

private:
	// �rea de datos privada
	WControl* lastMouseOver;
	WControl* lastLMouseDown;
	WControl* lastMMouseDown;
	WControl* lastRMouseDown;
};


#endif // _W_MULTICONTROL_H_
