/*********************************************************************************************
 * Name			: w_lienzoBase.h
 * Description	: Clase para manipular una representación gráfica genérica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_LIENZO_BASE_H_
#define _W_LIENZO_BASE_H_

#include "w_graficos/w_ilienzo.h"

// FWD
class IWControl;
class WLienzo;

class WLienzoBase : public WILienzo
{
public:
	// Operadores
	void operator= (const WLienzoBase &);
	void operator+= (const WLienzoBase &);

	WLienzo resize (const size_t ancho, const size_t alto) const;
	WLienzo sub (const size_t xPos, const size_t yPos, const size_t ancho, const size_t alto) const;

	// Modificadores
	void unir (const WLienzoBase & lienzo, const size_t xPos = 0, const size_t yPos = 0);
	void unir (const IWControl &);

	// Transformaciones
	typedef void (RGBA::*ForEachPixelOnlyColor)();
	void forEachPixel (ForEachPixelOnlyColor action);
	typedef bool (*ForEachPixelColor) (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y);
	void forEachPixel (ForEachPixelColor action);
	typedef bool (*ForEachPixel) (WLienzoBase & lienzo, const size_t x, const size_t y);
	void forEachPixel (ForEachPixel action);

	void setBorder (const RGBA &);
	void invertColor();
	void brightness (const short u);	//< rango de 'u' = [(-0xFF), (0xFF)]
	void fromRGBtoGRAY();
	void negative();
	void redChannel();
	void greenChannel();
	void blueChannel();
};

#endif // _W_LIENZO_BASE_H_
