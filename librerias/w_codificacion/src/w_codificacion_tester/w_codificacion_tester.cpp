/*********************************************************************************************
 *	Name		: maintester.cpp
 *	Description	: Este fichero sirve para configurar las pruebas unitarias que se realizar�n
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_codificacion_tester.pragmalib.h"

#include "w_codificacion/w_codificacion.h"

#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <string>

typedef enum Resultado
{
	PruebaOk,
	PruebaError,
}
EResult;

EResult TestDes();
EResult TestBase64();
EResult TestAes();
EResult TestMd5();
EResult TestSha1();

// Bater�a de pruebas
int main (int argc, char* argv[])
{
	// Diferentes testers...
	printf ("Prueba DES   : %s\n", TestDes() == PruebaOk ? "Ok" : "Error");
	printf ("Prueba Base64: %s\n", TestBase64() == PruebaOk ? "Ok" : "Error");
	printf ("Prueba AES   : %s\n", TestAes() == PruebaOk ? "Ok" : "Error");
	printf ("Prueba MD5   : %s\n", TestMd5() == PruebaOk ? "Ok" : "Error");
	printf ("Prueba SHA1  : %s\n", TestSha1() == PruebaOk ? "Ok" : "Error");

	// Despedida
	printf ("\n");
	printf ("Pruebas finalizadas\n");
	printf ("Presiona una tecla para finalizar...");
	getchar();
	return 0;
}

// Testea la funcionalidad de la libreria para DES
EResult TestDes()
{
#define CADENA_EJEMPLO_DES "Prueba DES"
	const unsigned char key1[8] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07 };
	const unsigned char key2[8] = { 0x70, 0x60, 0x50, 0x40, 0x30, 0x20, 0x10 };
	const char bufferIn[] = { 0x50, 0x72, 0x75, 0x65, 0x62, 0x61, 0x20, 0x44, 0x65, 0x73, 0x00 };
	const std::string stringIn = CADENA_EJEMPLO_DES;
	char bufferOutEncrypt[100];
	size_t sizeOutEncrypt = sizeof (bufferOutEncrypt);
	char bufferOutDesencrypt[100];
	size_t sizeOutDesencrypt = sizeof (bufferOutDesencrypt);
	std::string stringDesencrypt;

	// Des precalculado de la cadena
	const unsigned char resultadoEsperado[] = {0xD4, 0xE2, 0x44, 0xED, 0xB6, 0x0E, 0xFE, 0x5E,
											   0xe9, 0x27, 0x40, 0xde, 0xd7, 0x9f, 0x27, 0x16,
											   0x0b, 0x50, 0xa5, 0x62, 0xcf, 0x6e, 0x7f, 0xab
											  };

	if (desEncrypt (bufferIn, sizeof (bufferIn), bufferOutEncrypt, &sizeOutEncrypt, key1, key2) != EncryptOk)
	{
		return PruebaError;
	}
	if (desDesencrypt (bufferOutEncrypt, sizeOutEncrypt, bufferOutDesencrypt, &sizeOutDesencrypt, key1, key2) != EncryptOk)
	{
		return PruebaError;
	}
	if (memcmp (bufferIn, bufferOutDesencrypt, sizeof (bufferIn)) != 0)
	{
		return PruebaError;
	}

	return PruebaOk;
}

// Testea la funcionalidad de la libreria para Base64
EResult TestBase64()
{
	const unsigned char bufferorg[] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };
	char buffer[sizeof (bufferorg)];
	char bufferString [sizeof (bufferorg) + 1];
	char bufferBase64 [Base64CalculateSizeEncodedBuffer (sizeof (buffer))];
	size_t sizeBuffer;

	memset (bufferBase64, -1, sizeof (bufferBase64));

	// Pasamos buffer a base 64
	if (base64EncodeFromBuffer (bufferorg, sizeof (bufferorg), bufferBase64, sizeof (bufferBase64)) != EncryptOk)
	{
		return PruebaError;
	}

	// Nos aseguramos que metemos basura en el buffer
	memset (buffer, -1, sizeof (buffer));

	// Pasamos de base64 al buffer
	sizeBuffer = sizeof (buffer) + 4;
	if (base64DecodeToBuffer (bufferBase64, buffer, &sizeBuffer) != EncryptOk)
	{
		return PruebaError;
	}
	if (sizeBuffer != sizeof (buffer))
	{
		return PruebaError;
	}

	// Lo pasamos interpretandolo como un string
	sizeBuffer = sizeof (bufferString);
	if (base64DecodeToString (bufferBase64, bufferString, &sizeBuffer) != EncryptOk)
	{
		return PruebaError;
	}
	if (sizeBuffer != strlen (bufferString))
	{
		return PruebaError;
	}

	// Verificamos que tiene que ser lo mismo que al inicio
	if (memcmp (bufferorg, buffer, sizeof (buffer)) != 0)
	{
		return PruebaError;
	}

	return PruebaOk;
}

// Testea la funcionalidad de la libreria para AES
EResult TestAes()
{
	AES_KEY key = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };
	unsigned char bufferIn[] = { 0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00 };
	unsigned char bufferAes[sizeof (bufferIn)];
	unsigned char bufferOut[sizeof (bufferIn)];

	if (aesEncryptBuffer (bufferIn, sizeof (bufferIn), bufferAes, sizeof (bufferAes), key) != EncryptOk)
	{
		return PruebaError;
	}
	if (aesDesencryptBuffer (bufferAes, sizeof (bufferAes), bufferOut, sizeof (bufferOut), key) != EncryptOk)
	{
		return PruebaError;
	}
	{
		// Usando como buffer AES un string
		const std::string & bufferOrg = "hol45644235644634464r��Ѵ�d-�*++er43?=()?$&(/?37482334'5�!���\\\'23a";
		std::string bufferAES;
		std::string bufferFin;
		if (aesEncryptBuffer (bufferOrg, bufferAES, key) != EncryptOk)
		{
			return PruebaError;
		}
		if (aesDesencryptBuffer (bufferAES, bufferFin, key) != EncryptOk)
		{
			return PruebaError;
		}

		if (bufferOrg != bufferFin)
		{
			return PruebaError;
		}
	}

	return PruebaOk;
}

// Testea la funcionalidad de la libreria para Sha1
EResult TestSha1()
{
#define CADENA_EJEMPLO_SHA1 "Prueba Sha1"
	char bufferIn[] = { 0x50, 0x72, 0x75, 0x65, 0x62, 0x61, 0x20, 0x53, 0x68, 0x61, 0x31 };

	// Sha1 precalculado de la cadena
	SHA1 resultadoEsperado = { 0xda, 0xcb, 0x63, 0x8f, 0xf7, 0xc3, 0xc7, 0xe0, 0x39, 0xe7, 0x88, 0x16, 0x3e, 0xd0, 0x24, 0x7e, 0xd5, 0x46, 0xac, 0x9c };
	SHA1 sha1;

	sha1CreateFromString (CADENA_EJEMPLO_SHA1, sha1);
	if (memcmp (sha1, resultadoEsperado, sizeof (SHA1)) != 0)
	{
		return PruebaError;
	}
	if (sha1CompareString (CADENA_EJEMPLO_SHA1, resultadoEsperado) != EncryptEquals)
	{
		return PruebaError;
	}

	Sha1Context sha1Context;
	sha1CreateFromTrunkBufferInit (&sha1Context);
	sha1CreateFromTrunkBufferAdd (&sha1Context, bufferIn + 0, 2);
	sha1CreateFromTrunkBufferAdd (&sha1Context, bufferIn + 2, 2);
	sha1CreateFromTrunkBufferAdd (&sha1Context, bufferIn + 4, 2);
	sha1CreateFromTrunkBufferAdd (&sha1Context, bufferIn + 6, 2);
	sha1CreateFromTrunkBufferAdd (&sha1Context, bufferIn + 8, 2);
	sha1CreateFromTrunkBufferAdd (&sha1Context, bufferIn + 10, 1);
	sha1CreateFromTrunkBufferFinish (&sha1Context, sha1);
	if (memcmp (sha1, resultadoEsperado, sizeof (SHA1)) != 0)
	{
		return PruebaError;
	}

	if (sha1Str (CADENA_EJEMPLO_SHA1) != "DACB638FF7C3C7E039E788163ED0247ED546AC9C")
	{
		return PruebaError;
	}

	return PruebaOk;
}

// Prueba la funcionalidad de la libreria para MD5
EResult TestMd5()
{
#define CADENA_EJEMPLO_MD5 "Prueba Md5"
	const char bufferIn[] = { 0x50, 0x72, 0x75, 0x65, 0x62, 0x61, 0x20, 0x4d, 0x64, 0x35 };
	const std::string cadena = CADENA_EJEMPLO_MD5;

	// Md5 precalculado de la cadena
	const MD5 resultadoEsperado = { 0x0b, 0x82, 0x06, 0xb4, 0x4f, 0x81, 0x56, 0x4f, 0x2b, 0x40, 0x4a, 0x62, 0x92, 0x61, 0x19, 0x58 };
	MD5 md5;

	md5Create (bufferIn, sizeof (bufferIn), md5);
	if (memcmp (md5, resultadoEsperado, sizeof (MD5)) != 0)
	{
		return PruebaError;
	}

	md5Create (CADENA_EJEMPLO_MD5, sizeof (bufferIn), md5);
	if (memcmp (md5, resultadoEsperado, sizeof (MD5)) != 0)
	{
		return PruebaError;
	}

	md5Create (cadena, md5);
	if (memcmp (md5, resultadoEsperado, sizeof (MD5)) != 0)
	{
		return PruebaError;
	}

	if (md5Str (CADENA_EJEMPLO_MD5) != "0B8206B44F81564F2B404A6292611958")
	{
		return PruebaError;
	}

	return PruebaOk;
}
