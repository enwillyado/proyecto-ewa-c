/*********************************************************************************************
 *	Name		: srvevent.h
 *	Description	: Evento recibido por un Servidor TCP/UDP
 *
 * Copyright	(C) 2013 DATISA (http://www.datisa.es)
 ********************************************************************************************/

#ifndef _DPROTECTIONDSAAS_EVENT_H_
#define _DPROTECTIONDSAAS_EVENT_H_

#include <string>
#include <time.h>

#define SOCKET_ID_NULL 0

typedef unsigned int SocketID;
class Evento
{
public:
	Evento()
		: socketID (SOCKET_ID_NULL)
	{
	}
	enum TipoEvento
	{
		EVENTO_NULO = 0,
		MENSAJE_RECIBIDO = 1,		// se recibi� un mensaje correcto
		MENSAJE_RECIBIDO_VACIO = 2,	// se recibi� un mensaje correcto, pero vac�o
		ERROR_AL_SELECCIONAR,		// error al seleccionar una conexi�n
		ERROR_AL_ACEPTAR,			// error al aceptar una nueva conexi�n
		CONEXION_ACEPTADA,			// se acept� una nueva conexi�n
		SERVIDOR_DESCONECTADO,		// se cierra la conexi�n en el Servidor
		CONEXION_DESCONECTADA,		// se cierra la conexi�n con el Cliente
		CONEXION_PERDIDA,			// se cierra la conexi�n INSEPERADAMENTE
		ERROR_AL_RECIBIR,			// error al recibir un mensaje
	};
	// �rea de datos p�blica
	TipoEvento tipo;
	std::string ip;
	int puerto;
	std::string mensaje;
	time_t fechallegada;
	SocketID socketID;
};
class EventoTCP : public Evento
{
public:
	EventoTCP()
	{
		tipo = EventoTCP::EVENTO_NULO;
		mensaje = "";
		ip = "";
		puerto = 0;
		fechallegada = 0;
	}
};

// Clase m�s b�sica que la una
class IdentificadorUDP;

// Clase evento UDP
class EventoUDP : public Evento
{
public:
	EventoUDP();
	~EventoUDP();
	EventoUDP (const EventoUDP &);

	void operator= (const EventoUDP &);

	void destructor();

	// �rea de datos p�blica
	IdentificadorUDP* idUDP;
};

#endif	//_DPROTECTIONDSAAS_EVENT_H_
