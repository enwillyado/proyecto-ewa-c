#ifndef _CPU_H_
#define _CPU_H_

#include "sim/mmu.h"
#include "sim/abu.h"

class CPU
{
public:
	// constructor
	CPU();
	~CPU();

	void instalaMMU (MMU*);
	MMU* getMMU();
	void ciclo();

protected:
	void leeInstruccion();
	void ejecutaInstruccion();

	bool existeMMU();
	bool existeABU();

	void promociona();
	void ejecutaABU();
	void ejecutaMMU();

	void leeRegistro();
	void escRegistro();

	void dameMMU();
	void tomaMMU();

	const bool dameSemiInstruccion();

	// acceso a los registros
	const bool dameK();
	const bool dameZ();
	const bool dameU();
	const bool dameV();
	const bool dameW();
	const bool dameG();
	const bool dameH();
	const bool dameI();
	const bool dameJ();

	const bool dame0();
	const bool dame1();
	const bool dame2();
	const bool dame3();
	const bool dame4();
	const bool dame5();
	const bool dame6();
	const bool dame7();
	const bool dame8();
	const bool dame9();
	const bool dameA();
	const bool dameB();
	const bool dameC();
	const bool dameD();
	const bool dameE();
	const bool dameF();

	// escritura en los registros
	void tomaK (const bool);
	void tomaZ (const bool);
	void tomaU (const bool);
	void tomaV (const bool);
	void tomaW (const bool);
	void tomaG (const bool);
	void tomaH (const bool);
	void tomaI (const bool);
	void tomaJ (const bool);

	void toma0 (const bool);
	void toma1 (const bool);
	void toma2 (const bool);
	void toma3 (const bool);
	void toma4 (const bool);
	void toma5 (const bool);
	void toma6 (const bool);
	void toma7 (const bool);
	void toma8 (const bool);
	void toma9 (const bool);
	void tomaA (const bool);
	void tomaB (const bool);
	void tomaC (const bool);
	void tomaD (const bool);
	void tomaE (const bool);
	void tomaF (const bool);

private:
	MMU* laMmu;
	ABU* laAbu;
	/***************************
	 * Registros
	 *
	 **************************/
	bool K;

	bool Z;
	bool U;
	bool V;
	bool W;

	bool G;
	bool H;
	bool I;
	bool J;

	bool r8;
	bool r9;
	bool rA;
	bool rB;
	bool rC;
	bool rD;
	bool rE;
	bool rF;
};

#endif	//_CPU_H_
