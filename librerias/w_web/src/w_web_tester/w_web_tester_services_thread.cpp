/*********************************************************************************************
 * Name			: w_web_services_thread_prueba.cpp
 * Description	: Implementación del mecanismo para responder a un determinado servicio
 *					que ejecutará el servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_web/w_web_services_thread_prueba.h"

#include "dsystem/dsystem_base.h"					//< para tener el "dsystemSleep(...)"

// Servicio de Prueba
ServicioPruebaConHilos::ServicioPruebaConHilos (ServidorTCP* server)
	: ServicioConHilo (server) {}
BasicSrvService* ServicioPruebaConHilos::newInstance() const
{
	return new ServicioPruebaConHilos (this->server);
}
std::string ServicioPruebaConHilos::getServicioName() const
{
	return SRVSERVICE_PRUEBA_CON_HILOS_ID;
}

// Función para ejecutarse por un hilo
Mensaje ServicioPruebaConHilos::ejecutarServicioFinal (const Evento & evento, const Mensaje & peticion) const
{
	// Esperar un buen rato (5 segundos)
	::dsystemSleep (5 * 1000);

	// Montar la respuesta
	Mensaje respuesta;
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setVersionProtocolo (Mensaje::HTTP1_0);
	respuesta.setBody ("(" + Mensaje::toStr (evento.socketID) + ") --> " + evento.ip + ":" + Mensaje::toStr (
						   evento.puerto) + " (" + Mensaje::toStr ((size_t)evento.fechallegada) + ")");

	// Devolver la respuesta montada
	return respuesta;
}


// Servicio de Prueba
ServicioPruebaInteractivoConHilos::ServicioPruebaInteractivoConHilos (ServidorTCP* server)
	: ServicioConHilo (server) {}
BasicSrvService* ServicioPruebaInteractivoConHilos::newInstance() const
{
	return new ServicioPruebaInteractivoConHilos (this->server);
}
std::string ServicioPruebaInteractivoConHilos::getServicioName() const
{
	return SRVSERVICE_PRUEBA_INTERACTIVA_CON_HILOS_ID;
}

// Servicio para ejecutarse desde el catálogo
Mensaje ServicioPruebaInteractivoConHilos::ejecutarServicio (const Evento & evento,
		const Mensaje & peticion) const
{
	if (peticion.getRequestVar ("modo") == "mono")
	{
		// Montar la respuesta en modo mono-hilo (retornará cuando termine)
		return this->ejecutarServicioFinal (evento, peticion);
	}
	else if (peticion.getRequestVar ("modo") == "multi")
	{
		// Montar la respuesta en modo multi-hilo (retornará inmediatamente)
		return this->ejecutarServicioMultiHilo (evento, peticion);
	}
	else
	{
		// Montar la respuesta de la index
		Mensaje respuesta;
		respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
		respuesta.setVersionProtocolo (Mensaje::HTTP1_0);
		respuesta.setBody ("<h1><a href='?modo=mono'>monohilo</a></h1><h1><a href='?modo=multi'>multihilo</a></h1><h1><a href=''>reload</a></h1><h1><a href='"
						   SRVSERVICE_PRUEBA_CON_HILOS_ID "'>" SRVSERVICE_PRUEBA_CON_HILOS_ID "</a></h1>");

		// Devolver la respuesta
		return respuesta;
	}
}

// Función para ejecutarse por un hilo o en el programa principal (depende del parámetro "modo")
Mensaje ServicioPruebaInteractivoConHilos::ejecutarServicioFinal (const Evento & evento,
		const Mensaje & peticion) const
{
	// Esperar un buen rato (5 segundos)
	::dsystemSleep (5 * 1000);

	// Montar la respuesta
	Mensaje respuesta;
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setVersionProtocolo (Mensaje::HTTP1_0);
	respuesta.setBody ("(" + Mensaje::toStr (evento.socketID) + ") --> " + evento.ip + ":" + Mensaje::toStr (
						   evento.puerto) + " (" + Mensaje::toStr ((size_t)evento.fechallegada) + ")");

	// Devolver la respuesta montada
	return respuesta;
}
