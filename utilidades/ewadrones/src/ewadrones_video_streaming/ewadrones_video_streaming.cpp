/*********************************************************************************************
 *	Name		: ewadrones_video_streaming.cpp
 *	Description	: Ejecutable que implementa el v�deo streaming de EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_ewadrones_video_streaming.pragmalib.h"

#include "w_red/w_red_clitcp.h"
#include "w_red/w_red_srvtcp.h"

#include "w_base/fromFile.hpp"

// Cargar mapa de puertos necesarios para comunicarse con el AR.DRONE
#include "ewadrones/ar_drone_puertos.hpp"

#include <stdint.h>
struct pave
{
	/*00*/ uint8_t  signature[4];
	/*04*/ uint8_t  version;
	/*05*/ uint8_t  video_codec;
	/*06*/ uint16_t header_size;
	/*08*/ uint32_t payload_size;             /* Amount of data following this PaVE */
	/*12*/ uint16_t encoded_stream_width;     /* ex: 640 */
	/*14*/ uint16_t encoded_stream_height;    /* ex: 368 */
	/*16*/ uint16_t display_width;            /* ex: 640 */
	/*18*/ uint16_t display_height;           /* ex: 360 */
	/*20*/ uint32_t frame_number;             /* frame position inside the current stream */
	/*24*/ uint32_t timestamp;                /* in milliseconds */
	/*28*/ uint8_t  total_chuncks;            /* number of UDP packets containing the current decodable payload */
	/*29*/ uint8_t  chunck_index ;            /* position of the packet - first chunk is #0 */
	/*30*/ uint8_t  frame_type;               /* I-frame, P-frame */
	/*31*/ uint8_t  control;                  /* Special commands like end-of-stream or advertised frames */
	/*32*/ uint32_t stream_byte_position_lw;  /* Byte position of the current payload in the encoded stream  - lower 32-bit word */
	/*36*/ uint32_t stream_byte_position_uw;  /* Byte position of the current payload in the encoded stream  - upper 32-bit word */
	/*40*/ uint16_t stream_id;                /* This ID indentifies packets that should be recorded together */
	/*42*/ uint8_t  total_slices;             /* number of slices composing the current frame */
	/*43*/ uint8_t  slice_index ;             /* position of the current slice in the frame */
	/*44*/ uint8_t  header1_size;             /* H.264 only : size of SPS inside payload - no SPS present if value is zero */
	/*45*/ uint8_t  header2_size;             /* H.264 only : size of PPS inside payload - no PPS present if value is zero */
	/*46*/ uint8_t  reserved2[2];             /* Padding to align on 48 bytes */
	/*48*/ uint32_t advertised_size;          /* Size of frames announced as advertised frames */
	/*52*/ uint8_t  reserved3[12];            /* Padding to align on 64 bytes */
};


// Funci�n principal
int main()
{
#if 1
	ClienteTCP c;
	c.openSocket (NOMBRE_DE_SERVIDOR, PUERTO_VIDEO_EN_PROXY);

	std::ofstream f ("bin", std::ios_base::binary);
	while (c.isOpenSocket() == true)
	{
		std::string m;
		c.recibir (m, 10);
		f << m;
		f.flush();
	}
	f.close();
#elif 0
	std::ifstream s ("bin", std::ios_base::binary);
	std::ofstream f ("bin.mp4", std::ios_base::binary);
	unsigned int i = 0;
	char m[64];

	do
	{
		const char c = s.get();
		if (i >= 64)
		{
			pave* p = (pave*)m;
			if (i >= p->header_size)
			{
				if (i < p->payload_size + p->header_size - 1)
				{
					f.put (c);
				}
				else
				{
					i = 0;
					continue;
				}
			}
		}
		else
		{
			if (i == 0 && c != 'P')
			{
				f.put (c);
				continue;
			}
			if (i == 1 && c != 'a')
			{
				f.put (c);
				i = 0;
				continue;
			}
			if (i == 2 && c != 'V')
			{
				f.put (c);
				i = 0;
				continue;
			}
			if (i == 3 && c != 'E')
			{
				f.put (c);
				i = 0;
				continue;
			}
			m[i] = c;
		}
		i++;
	}
	while (s.eof() == false);

	f.close();
#else
	class ServidorTCP2 : public ServidorTCP
	{
		virtual bool analizaEventoConexion (const EventoServidorTCP & e)
		{
			const std::string & m = leerContenidoFile ("bin");
			return this->send (e, m);
		}

	};
	ServidorTCP2 c;
	c.open (PUERTO_2_VIDEO_EN_PROXY);
	c.start();
	c.close();

#endif
	return 0;
}
