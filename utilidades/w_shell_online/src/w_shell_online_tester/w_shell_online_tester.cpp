/*********************************************************************************************
*	Name		: w_web_tester.cpp
 *	Description	: Este fichero sirve para configurar las pruebas unitarias que se realizarán
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_shell_online_tester.pragmalib.h"

#include <assert.h>
#include "w_web/w_web_srvtcp.h"
#include "w_web/w_web_clitcp.h"

#define NOMBRE_SERVIDOR_PRUEBA	"localhost"
#define PUERTO_SERVIDOR_PRUEBA	5005

void main()
{
	ServidorWebTCP servidor;
	assert (true == servidor.open (PUERTO_SERVIDOR_PRUEBA));
	servidor.start();
	return;
}
