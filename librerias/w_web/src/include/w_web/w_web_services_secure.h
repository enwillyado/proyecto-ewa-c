/*********************************************************************************************
 * Name			: w_web_services_secure.h
 * Description	: Servicios que son capaces de responder a las peticiones recibidas con hilos
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRV_SERVICES_SECURE_H_
#define _W_RED_SRV_SERVICES_SECURE_H_

#include "w_web/w_web_services_base.h"		//< para tener el "BasicSrvService"
#include "w_red/w_red_srvevent.h"			//< para tener el "EventoServidorTCP" y "Evento"

#include <string>
#include <map>

// Esqueleto de un servicio con hilos
class ServicioSecure : public BasicSrvService
{
public:
	typedef std::string UsuNom;
	typedef std::string UsuPass;
	typedef std::map<UsuNom, UsuPass> Credenciales;

	ServicioSecure (const Credenciales & = Credenciales(), const bool ignorarIncorrectos = true);
	ServicioSecure (const ServicioSecure &);

public:

	// Estructura que indicará el resultado de la autenticación
	enum EstadoAutenticacion
	{
		SIN_ERROR,			//< todo correcto
		NO_AUTH,			//< no se envió la autenticación
		BAD_AUTORIZACION,	//< la autenticación indicada no es correcta
		NO_USUARIO,			//< no existe el usuario indicado en la autenticación
		MAL_PASSWORD,		//< la constraseña indicada en la autenticación no es correcta
	};
	struct Authorization
	{
		const std::string type;
		const UsuNom usuNom;
		const UsuPass usuPass;

	public:
		static Authorization fromString (const std::string &);
	};

	struct ResultadoAutenticacion
	{
		const EstadoAutenticacion estadoAutenticacion;
		const Authorization authorization;
	};

	// Servicio a implementar
	virtual Mensaje ejecutarServicio (const Evento & evento, const Mensaje & peticion,
									  const ResultadoAutenticacion & resultadoAutenticacion) const = 0;

protected:
	// Servicio para ejecutarse desde el catálogo
	virtual Mensaje ejecutarServicio (const Evento & evento, const Mensaje & peticion) const;

protected:
	bool ignorarIncorrectos;
	Credenciales credenciales;
};

#endif	//_W_RED_SRV_SERVICES_SECURE_H_
