/*********************************************************************************************
 * Name			: w_coordenada.h
 * Description	: Clase para manipular una coordenada gr�fica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_COORDENADA_H_
#define _W_COORDENADA_H_

#include <string>		//< size_t

class WCoordenada
{
public:
	// Constructores
	WCoordenada (const size_t posX = 0, const size_t posY = 0);

public:
	// �rea de datos
	size_t posX;
	size_t posY;
};

#endif // _W_COORDENADA_H_
