/*********************************************************************************************
 * Name			: w_web_services_thread_prueba_id.h
 * Description	: Identificador de los ServiciosThread de prueba
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRV_SERVICES_THREAD_PRUEBA_ID_H_
#define _W_RED_SRV_SERVICES_THREAD_PRUEBA_ID_H_

// ----------------------------------------------------------------------------
// Identificador del Servicio de Prueba
#define SRVSERVICE_PRUEBA_CON_HILOS_ID	"/pruebaHilos"

// ----------------------------------------------------------------------------
// Identificador del Servicio de Prueba Interactiva
#define SRVSERVICE_PRUEBA_INTERACTIVA_CON_HILOS_ID	"/"

#endif	//_W_RED_SRV_SERVICES_THREAD_PRUEBA_ID_H_
