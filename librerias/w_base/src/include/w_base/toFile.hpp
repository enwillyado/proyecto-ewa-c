/*********************************************************************************************
*	Name		: toFile.hpp
*	Description	: M�todo de escritura de ficheros
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _TO_FILE_HPP_
#define _TO_FILE_HPP_

#include <string>       //< std::string
#include <fstream>		//< std::ofstream

static bool ponerContenidoFile (const std::string & file, const std::string & data)
{
	std::ofstream fileout (file.c_str(), std::ios::out | std::ios::binary);
	if (fileout.good())
	{
		fileout << data;
		fileout.close();
		return true;
	}
	return false;
}


#endif	//_TO_FILE_HPP_
