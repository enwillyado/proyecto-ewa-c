/*********************************************************************************************
 *	Name		: _w_graficos_cmd.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_GRAFICOS_CMD_PRAGMALIB_H_
#define _W_GRAFICOS_CMD_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

// Parte privada
#include "w_graficos/_w_graficos.pragmalib.h"

#endif

#undef _W_GRAFICOS_CMD_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_GRAFICOS_CMD_PRAGMALIB_H_
