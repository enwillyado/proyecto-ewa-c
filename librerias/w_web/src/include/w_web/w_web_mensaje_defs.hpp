/*********************************************************************************************
 * Name			: w_web_mensaje_defs.hpp
 * Description	: Definiciones usadas en los Mensajes Web
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_MENSAJE_DEFS_H_
#define _W_RED_MENSAJE_DEFS_H_

#define CONJUNTO_SEPARADORES							"\r\n\t "	// conjunto de los caracteres que sirven para serpar partes de un mensaje
#define FIN_DE_LINEA									"\r\n"

#define	W_RED_TIPOMENSAJE_GET							"GET"
#define	W_RED_TIPOMENSAJE_HEAD							"HEAD"
#define	W_RED_TIPOMENSAJE_POST							"POST"
#define	W_RED_TIPOMENSAJE_PUT							"PUT"
#define	W_RED_TIPOMENSAJE_DELETE						"DELETE"
#define	W_RED_TIPOMENSAJE_TRACE							"TRACE"
#define	W_RED_TIPOMENSAJE_OPTIONS						"OPTIONS"
#define	W_RED_TIPOMENSAJE_CONNECT						"CONNECT"
#define	W_RED_TIPOMENSAJE_UNKNOWN						"UNKNOWN"	// NOTA: antes era "", pero es preferible que tenga longitud mayor que cero

#define	W_RED_VERSIONPROTOCOLO_EWA						"EWA/1.0"
#define	W_RED_VERSIONPROTOCOLO_10						"HTTP/1.0"
#define	W_RED_VERSIONPROTOCOLO_11						"HTTP/1.1"

#define	W_RED_URI_SUBSEPARATORS							'/'		// char!!!
#define	W_RED_URI_GET_INICIATOR							"?"		// char*
#define	W_RED_URI_GETPOST_ASIGNATOR						"="		// char*
#define	W_RED_URI_GETPOST_SEPARATOR						"&"		// char*

#define	ENCABEZADO_SEPARATOR							": "
#define	ENCABEZADOID_DATE 								"Date"
#define	ENCABEZADOID_COOKIE								"Cookie"
#define	ENCABEZADOID_CONTENT_ENCODING					"Content-Encoding"
#define	ENCABEZADOID_CONTENT_LENGTH						"Content-Length"
#define	ENCABEZADOID_WWW_AUTHENTICATE 					"WWW-Authenticate"
#define	ENCABEZADOID_WWW_AUTHORIZATION					"Authorization"
#define	ENCABEZADOID_X_XSS_PROTECTION 					"X-XSS-Protection"
#define	ENCABEZADOID_SERVER								"Server"
#define	ENCABEZADOID_CONTENT_TYPE 						"Content-Type"
#define ENCABEZADOID_CONTENT_TYPE_TEXT						"text/plain"
#define ENCABEZADOID_CONTENT_TYPE_XML						"text/xml"
#define ENCABEZADOID_CONTENT_TYPE_HTML_UTF8					"text/html; charset=utf-8"
#define ENCABEZADOID_CONTENT_TYPE_BINARY					"application/octet-stream"
#define ENCABEZADOID_CONTENT_DISPOSITION				"Content-Disposition"
#define ENCABEZADOID_CONTENT_DISPOSITION_FILENAME(s)		"attachment; filename=\"" + ((const std::string &)(s)) + "\""
#define	ENCABEZADOID_CACHE_CONTROL 						"Cache-Control"
#define	ENCABEZADOID_EXPIRES 							"Expires"

#endif	//_W_RED_MENSAJE_DEFS_H_
