/*********************************************************************************************
 *	Name		: _w_red_clievent.cpp
 *	Description	: Implementación de un evento recibido por un Cliente TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/

#include "w_red/w_red_clievent.h"
#include "w_red/w_red_srvudp_private.h"

EventoClienteTCP::EventoClienteTCP()
	: EventoTCP()
{
}

EventoClienteUDP::EventoClienteUDP()
	: EventoUDP()
{
}
