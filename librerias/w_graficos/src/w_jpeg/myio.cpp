
/*             _      | |        |  _      | |      |  | |
 *            / \     | |          / \     | |  /|  |  | |             \
 *      Q___|____\  __| | | . |__Q____\  __| | (_|__|__| |  Q_|__|__|___)
 *  ___/    :      /      |___|         /               ___/          .
 *               _/                   _/
 *
 */

//  Written by Hamed Ahmadi Nejad
//    ahmadinejad@ce.sharif.edu
//    comphamed@yahoo.com

#include "w_graficos/jpeg/myio.h"
#include "w_graficos/jpeg/datastruct.h"
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <cstring>		//< memcpy

using namespace std;

int nError;

const int chunksize = 32768;
unsigned char* entire_file = NULL;
int filesize = 0;
int fp;  //file pointer

void MyIO::openfile (const char* s)
{
	FILE* f;
	int filesize_buffer = 0;
	f = fopen (s, "rb");
	if (!f)
	{
		Error ("Unable to open specified file.");
		return;
	}
	fseek (f, 0, SEEK_END);
	filesize_buffer = ftell (f);
	rewind (f); //for us, does the same as fseek(f,0,SEEK_SET);
	unsigned char* entire_file_buffer = new unsigned char[filesize_buffer];
	int d = filesize_buffer / ::chunksize, i;
	for (i = 0; i < d; i++)
	{
		fread (entire_file_buffer + i * ::chunksize, 1, ::chunksize, f);
	}
	fread (entire_file_buffer + i * ::chunksize, 1, filesize_buffer % ::chunksize, f);
	MyIO::copyBuffer (entire_file_buffer, filesize_buffer);
}

void MyIO::copyBuffer (const unsigned char* s, const size_t size)
{
	assert (::entire_file == NULL);
	::entire_file = new unsigned char[size];
	memcpy (::entire_file, s, size);
	::filesize = size;
}

void MyIO::closefile()
{
	::filesize = 0;
	::fp = 0;
	delete [] ::entire_file;
	::entire_file = NULL;
}

int MyIO::read2bytes()
{
	if (::fp + 1 >= ::filesize)
	{
		Error ("Unexpected end of file");
		return -1;
	}
	int x = ::entire_file[::fp] * 0x100 + ::entire_file[::fp + 1];
	::fp += 2;
	return x;
}
int MyIO::readbyte()
{
	if (::fp >= ::filesize)
	{
		Error ("Unexpected end of file");
		return -1;
	}
	return ::entire_file[::fp++];
}

//  -------------------

struct Chunk
{
	unsigned char data[::chunksize];
	int cnt;
	void add (int x)
	{
		data[cnt++] = x;
	}
	Chunk()
	{
		cnt = 0;
	}
};

LList <Chunk*> buffer;

void MyIO::resetwritebuffer()
{
	buffer.clear();
	buffer.append (new Chunk);
}
void MyIO::writebyte (int x)
{
	if (buffer.last()->x->cnt == ::chunksize - 1)
	{
		buffer.append (new Chunk);
	}
	buffer.last()->x->add (x);
}
void MyIO::write2bytes (int x)
{
	if (buffer.last()->x->cnt >= ::chunksize - 2) //Leave one byte hanging sometimes :)
	{
		buffer.append (new Chunk);
	}
	Chunk* & bl = buffer.last()->x;
	bl->add (x >> 8);
	bl->add (x & 0xFF);
}
void MyIO::writestring (const char* s)
{
	for (; *s; s++)
	{
		writebyte (*s);
	}
}
void MyIO::writetofile (const char* const filename)
{
	FILE* f;
	f = fopen (filename, "wb");
	if (!f)
	{
		Error ("Unable to open specified file for writing");
		return;
	}
	LList <Chunk*> :: entry* e;
	for (e = buffer.first(); e->next; e = e->next)
	{
		fwrite (e->x->data, 1, e->x->cnt, f);
	}
	fclose (f);
}

//  -------------------

void MyIO::Error (const char* const s)
{
	cerr << "Error : " << s << endl;
	nError++;
	//exit(1);
}

