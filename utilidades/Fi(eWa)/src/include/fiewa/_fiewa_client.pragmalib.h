/*********************************************************************************************
 *	Name		: _fiewa_client.pragmalib.h
 *	Description	: Header file with unified libraries to link
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _FIEWA_CLIENT_PRAGMALIB_H_
#define _FIEWA_CLIENT_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#ifndef END_LIB_EWA
#ifdef _DEBUG
#define END_LIB_EWA "d.lib"
#else
#define END_LIB_EWA ".lib"
#endif
#endif

#pragma message("Enlazando con las librer�as del proyecto 'fiewa_client'...")

#pragma comment(lib, "fiewa_client" END_LIB_EWA)

#include "fiewa/_fiewa_comun.pragmalib.h"

#include "w_web/_w_web_server.pragmalib.h"
//#pragma comment(lib, "dencrypt" END_LIB_EWA)

#endif

#endif // _FIEWA_CLIENT_PRAGMALIB_H_
