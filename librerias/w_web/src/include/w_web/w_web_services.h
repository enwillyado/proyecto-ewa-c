/*********************************************************************************************
 * Name			: w_web_services.h
 * Description	: Mecanismo para responder a un determinado servicio que ejecutar�
 *					el servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SERVICES_H_
#define _W_RED_SERVICES_H_

#include "w_web/w_web_services_base.h"	//< para tener la base

#include "w_web/w_web_services_id.hpp"	//< para tener los id de los servicios
// NOTA: recordar a�adir en este otro archivo los nuevos servicios implementados

// Ahora, a�adir los servicios que NO se deseen exportar:
#define SRVSERVICE_OPENINTERN_ID	"@Open"		//< Servicio para cuando se realiza la conexi�n de un puerto
#define SRVSERVICE_SENDINTERN_ID	"@Send"		//< Servicio para cuando se env�a un mensaje que no se ha podido procesar
#define SRVSERVICE_CLOSEINTERN_ID	"@Close"	//< Servicio para cuando se pierde la conexi�n de un puerto

// TODO: hacer que esta lista est� en un archivo "srvservicesid_intern.h"?

#include <string>
#include <map>

// ----------------------------------------------------------------------------
// Servicio para cuando se haga un ping
class BasicSrvService_Ping : public BasicSrvService
{
public:
	BasicSrvService_Ping() : BasicSrvService() {};
	virtual ~BasicSrvService_Ping() {}
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_Ping();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_PING_ID;
	}
};

// ----------------------------------------------------------------------------
// Servicio para cuando se haga un download
class BasicSrvService_Download : public BasicSrvService
{
public:
	BasicSrvService_Download() : BasicSrvService() {};
	virtual ~BasicSrvService_Download() {}
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
	virtual BasicSrvService* newInstance() const
	{
		return new BasicSrvService_Download();
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_DOWNLOAD_ID;
	}
};

#endif	//_W_RED_SERVICES_H_
