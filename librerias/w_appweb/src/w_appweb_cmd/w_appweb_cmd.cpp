/*********************************************************************************************
 * Name			: w_appweb_cmd.cpp
 * Description	: Este fichero sirve para ver en funcionamiento de una WAppWeb
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_appweb_cmd.pragmalib.h"

#include "w_appweb/w_appweb.h"

#define FICHERO_AWE "../testdata/bienvenida.awe"

int main()
{
	WAppWeb appWeb;
	appWeb.loadaWe (FICHERO_AWE);

	appWeb.start();

	// Finalización correcta
	return 0;
}
