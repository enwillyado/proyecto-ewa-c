/*********************************************************************************************
 *	Name		: w_codificacion.h
 *	Description	: Funciones para codificar y decodificar secuencias de bytes (incluidas cadenas)
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _ENCRYPTCPP_H_
#define _ENCRYPTCPP_H_

#include "w_codificacion/w_codificacion_c.h"

#include <string>
#include <vector>

//____________________________________________________________________________________________
//
// MD5
//____________________________________________________________________________________________

// Calcula el MD5 de un buffer binario o un string que puede estar con distintas representaciones

// Formatos para el buffer entrada:
// buffer binario de C (void * , size_t)
// string de C (char * terminado en \0)
// std::vector

// Formato que deseamos que se deje el buffer de salida

// Formatos posibles para el buffer de entrada
void md5Create (const void* bufferIn, size_t szBufferIn, MD5 md5Out);
void md5Create (const char* stringIn, MD5 md5Out);
void md5Create (const std::string & stringIn, MD5 md5Out);

std::string md5Str (const std::string & stringIn);

//____________________________________________________________________________________________
//
// SHA1
//____________________________________________________________________________________________

// Calcula el SHA1 de un string

std::string sha1Str (const std::string & stringIn);

//____________________________________________________________________________________________
//
// DES
//____________________________________________________________________________________________

// Encripta / desencripta con el algoritmo DES un buffer binario o string

// Formatos posibles para el buffer entrada:
// buffer binario de C (void * , size_t)
// std::string
// std::vector

// Formato que deseamos que se deje el buffer de salida
// buffer binario de C (void * , size_t)
// std::vector
//

// Valores por defecto que toman los algoritmos para simplificar la larga lista de parametros
// que tienen. Pensado para que una aplicacion pueda hacerse sus definiciones en un solo punto
// de la aplicacion y no arrastre tantos parametros en todas las llamadas. Para cambiar las
// opciones solo hay que crear las definiciones antes de la inclusion del fichero

#ifndef DES_DEFAULT_PADDING
#define DES_DEFAULT_PADDING 1
#endif
#ifndef DES_DEFAULT_DESMODE
#define DES_DEFAULT_DESMODE DesMode3Des
#endif
#ifndef DES_DEFAULT_CONFIG
#define DES_DEFAULT_CONFIG DesConfigChainBlockCipher
#endif

// ENCRIPTA

EEncryptResult desEncrypt (const void* bufferIn, size_t szBufferIn, void* pBufferEncryptOut, size_t* szBufferEncryptOut,
						   const Des3Key key1, const Des3Key key2,
						   int nPadding = DES_DEFAULT_PADDING, EDesMode mode = DES_DEFAULT_DESMODE, EDesConfig config = DES_DEFAULT_CONFIG);

EEncryptResult desEncrypt (const void* bufferIn, size_t szBufferIn, std::vector<unsigned char> & pBufferEncryptOut,
						   const Des3Key key1, const Des3Key key2,
						   int nPadding = DES_DEFAULT_PADDING, EDesMode mode = DES_DEFAULT_DESMODE, EDesConfig config = DES_DEFAULT_CONFIG);

EEncryptResult desEncrypt (const std::string & stringIn, void* pBufferEncryptOut, size_t* szBufferEncryptOut,
						   const Des3Key key1, const Des3Key key2,
						   int nPadding = DES_DEFAULT_PADDING, EDesMode mode = DES_DEFAULT_DESMODE, EDesConfig config = DES_DEFAULT_CONFIG);

EEncryptResult desEncrypt (const std::string & stringIn, std::vector<unsigned char> & pBufferEncryptOut,
						   const Des3Key key1, const Des3Key key2,
						   int nPadding = DES_DEFAULT_PADDING, EDesMode mode = DES_DEFAULT_DESMODE, EDesConfig config = DES_DEFAULT_CONFIG);

EEncryptResult desEncrypt (const std::vector<unsigned char> & buffeIn, void* pBufferEncryptOut, size_t* szBufferEncryptOut,
						   const Des3Key key1, const Des3Key key2,
						   int nPadding = DES_DEFAULT_PADDING, EDesMode mode = DES_DEFAULT_DESMODE, EDesConfig config = DES_DEFAULT_CONFIG);

EEncryptResult desEncrypt (const std::vector<unsigned char> & buffeIn, std::vector<unsigned char> & pBufferEncryptOut,
						   const Des3Key key1, const Des3Key key2,
						   int nPadding = DES_DEFAULT_PADDING, EDesMode mode = DES_DEFAULT_DESMODE, EDesConfig config = DES_DEFAULT_CONFIG);

// DESENCRIPTA

EEncryptResult desDesencrypt (const void* bufferEncryptIn, size_t szBufferEncryptIn, void* bufferOut, size_t* szBufferOut,
							  const Des3Key key1, const Des3Key key2,
							  int nPadding = DES_DEFAULT_PADDING, EDesMode mode = DES_DEFAULT_DESMODE, EDesConfig config = DES_DEFAULT_CONFIG);

EEncryptResult desDesencrypt (const void* bufferEncryptIn, size_t szBufferEncryptIn, std::vector<unsigned char> & bufferOut,
							  const Des3Key key1, const Des3Key key2,
							  int nPadding = DES_DEFAULT_PADDING, EDesMode mode = DES_DEFAULT_DESMODE, EDesConfig config = DES_DEFAULT_CONFIG);

//____________________________________________________________________________________________
//
// AES
//____________________________________________________________________________________________

// Encripta / desencripta con el algoritmo AES un buffer binario o string

EEncryptResult contrToAESKeyEncrypt (AES_KEY & key, const std::string & contr);

// ENCRIPTA

EEncryptResult aesEncryptBuffer (const std::string & buffeIn, std::string & pBufferEncryptOut, const std::string & contr);
EEncryptResult aesEncryptBuffer (const std::string & buffeIn, std::string & pBufferEncryptOut, const AES_KEY key);

// DESENCRIPTA

EEncryptResult aesDesencryptBuffer (const std::string & pBufferEncryptIn, std::string & buffeOut, const std::string & contr);
EEncryptResult aesDesencryptBuffer (const std::string & pBufferEncryptIn, std::string & buffeOut, const AES_KEY key);

//____________________________________________________________________________________________
//
// Base64
//____________________________________________________________________________________________

// Codifica / decodifica un buffer binario o string a codificación Base 64

// Formatos posibles para el buffer entrada
// buffer binario de C (void * , size_t)
// string de C (char * terminado en \0)
// std::string
// std::vector

// Formato que deseamos que se deje el string de salida codificado
// string de C (char * terminado en \0)
// std::string
//

// Formatos posibles para el buffer de entrada Formato que deseamos que se deje el buffer de salida
EEncryptResult base64Encode (const void* bufferIn, size_t szBufferIn, std::string & stringB64Out);
EEncryptResult base64Encode (const void* bufferIn, size_t szBufferIn, char* bufferB64Out, size_t szBufferB64Out);
EEncryptResult base64Encode (const std::vector<unsigned char> & bufferIn, std::string & stringB64Out);
EEncryptResult base64Encode (const std::vector<unsigned char> & bufferIn, char* bufferB64Out, size_t szBufferB64Out);
EEncryptResult base64Encode (const char* stringIn, std::string & stringB64Out);
EEncryptResult base64Encode (const char* stringIn, char* bufferB64Out, size_t szBufferB64Out);
EEncryptResult base64Encode (const std::string & stringIn, std::string & stringB64Out);
EEncryptResult base64Encode (const std::string & stringIn, char* bufferB64Out, size_t szBufferB64Out);


// Formatos posibles para el buffer de entrada Formato que deseamos que se deje el buffer de salida
EEncryptResult base64Decode (const std::string & stringB64In, std::string & stringOut);
EEncryptResult base64Decode (const std::string & stringB64In, void* bufferOut, size_t* szBufferOut);
EEncryptResult base64Decode (const std::string & stringB64In, std::vector<unsigned char> & bufferOut);
EEncryptResult base64Decode (const char* stringB64In, std::string & stringOut);
EEncryptResult base64Decode (const char* stringB64In, void* bufferOut, size_t* szBufferOut);

/// Funcion auxiliar para decodificar una cadena en base 64 a su representacion original
std::string convertBase64ToString (const std::string & srcStrB64);

/// Convierte una cadena a su codificación en base 64
std::string convertStringToBase64 (const std::string & srcStr);

#endif // _ENCRYPTCPP_H_
