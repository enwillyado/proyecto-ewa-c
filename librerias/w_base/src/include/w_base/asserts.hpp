/*********************************************************************************************
*	Name		: asserts.hpp
*	Description	: M�todos para comprobar "asertos"
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _ASERTS_HPP_
#define _ASERTS_HPP_

#include <string.h>		// NULL
#include <assert.h>		// assert

template<class T>
static inline T* assertIfNull (T* t)
{
	assert (t != NULL);
	return t;
}

template<class T>
static inline const T* assertIfNull (const T* t)
{
	assert (t != NULL);
	return t;
}

template<class T>
static inline T* const assertIfNullConst (T* const t)
{
	assert (t != NULL);
	return t;
}

template<class T>
static inline const T* const assertIfNullConst (const T* const t)
{
	assert (t != NULL);
	return t;
}

#endif	//_ASERTS_HPP_
