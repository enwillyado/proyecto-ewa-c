/*********************************************************************************************
 *	Name		: w_arbol.cpp
 *	Description	: Arbol para trabajar m�s c�modamente con un xml en memoria, usando std
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_arbol/w_arbol.h"

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Miembros est�ticos <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

const std::string Arbol::nocampo = "";
const Arbol Arbol::npos (Arbol::PARSER_ERROR);

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Constructores <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

Arbol::Arbol (const std::string & nuevaClave, const std::string & nuevoValor)
{
	this->clearError();
	this->setClave (nuevaClave);
	this->setValor (nuevoValor);
}
Arbol::Arbol (const Arbol::Errores error)
{
	this->setError (error);
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> M�todos P�blicos <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

/**
 *	Devuelve el n�mero de hijos que tiene el elemento actual
 *  \retval	 n�mero de hijos (subelementos) que tiene el elemento actual
 **/
size_t Arbol::hijosize() const
{
	return this->hijos.size();
}

/**
 *	Devuelve el n�mero de atributos que tiene el elemento actual
 *  \retval	 n�mero de atributos que tiene el elemento actual
 **/
size_t Arbol::atributosize() const
{
	return this->atributos.size();
}

/**
*	Ajusta una nueva clave para el elemento actual
*	\param	[in]	nuevoValor	nuevo valor para el campo "clave" del elemento actual
**/
void Arbol::setClave (const std::string & nuevoValor)
{
	this->clave = nuevoValor;
}

/**
*	Ajusta un nuevo valor para el elemento actual
*	\param	[in]	nuevoValor	nuevo valor para el campo "valor" del elemento actual
**/
void Arbol::setValor (const std::string & nuevoValor)
{
	this->valor = nuevoValor;
}

/**
 *	Devuelve la clave del elemento actual
 *  \retval	 contenido del campo "clave" del elemento actual
 **/
const std::string & Arbol::getClave() const
{
	return this->clave;
}

/**
 *	Devuelve el valor del elemento actual
 *  \retval	 contenido del campo "valor" del elemento actual
 **/
const std::string & Arbol::getValor() const
{
	return this->valor;
}

/**
*	Devuelve los hijos del elemento actual
*  \retval	 conjunto de hijos del elemento actual
 **/
const Arbol::Arboles & Arbol::getHijos() const
{
	return this->hijos;
}

/**
 *	Devuelve los atributos del elemento actual
 *  \retval	 conjunto de atributos del elemento actual
 **/
const Arbol::Atributos & Arbol::getAtributos() const
{
	return this->atributos;
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Operaciones <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

void Arbol::addHijo (const Arbol & arbol)
{
	this->hijos.push_back (arbol);
}

void Arbol::addHijo (const std::string & indexstr)
{
	Arbol arbol;
	arbol.clave = indexstr;
	this->hijos.push_back (arbol);
}

/**
 *	Elimina el primer hijo (si existe) con la clave dada
 *	\param	[in]	indexstr	clave del elemento candidato a ser eliminado
 *  \retval	 TRUE si se ha borrado, y FALSE si no
 *				Si realmente se borra un elemento, hay que tener cuidado
 *					porque el �ndice que se pueda tener apuntando a un
 *					hijo (si es siguiente al elemento borrado) apuntar�
 *					incorrectamente.
 **/
bool Arbol::eraseHijo (const std::string & indexstr)
{
	IndexHijo index = this->getNextIndexHijo (indexstr, 0);	//< el "0" indica el indice a partir del que se empieza a buscar
	if (index == NO_INDEX)
	{
		// si no existe ya, no se hace nada
		return false;
	}
	this->hijos.erase (this->hijos.begin() + index);
	return true;
}

/**
 *	Elimina el atributo (si existe) con la clave dada
 *	\param	[in]	indexstr	clave del atributo candidato a ser eliminado
 *  \retval	 TRUE si se ha borrado, y FALSE si no.
 **/
bool Arbol::eraseAtributo (const std::string & indexstr)
{
	const Atributos::iterator & itr = this->atributos.find (indexstr);
	if (itr != this->atributos.end())
	{
		//< borrar atributo existente
		this->atributos.erase (itr);
		return true;
	}
	return false;
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Pseudo-iteradores <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

/**
 *	Indica si existe un hijo con la clave dada
 *	\param	[in]	indexstr	clave a buscar
 *  \retval	 TRUE si existe al menos un hijo con la clave dada; FALSE si no
 **/
bool Arbol::existeHijo (const std::string & indexstr) const
{
	for (size_t index = 0; index < this->hijos.size(); index++)
	{
		if (this->hijos[index].clave == indexstr)
		{
			return true;
		}
	}
	return false;
}

/**
 *	Devuelve el �ndice donde se encuentra el primer hijo con la clave dada desde la posici�n indicada
 *	\param	[in]	indexstr			clave a buscar
 *	\param	[in]	initIndex (opt.=0)	posici�n [0,n) a partir de la que se va a buscar (inclusive)
 *  \retval	 el �ndice esperado, si existe a partir de la posici�n dada, o NO_INDEX (-1) si no hay o ya no hay
 **/
Arbol::IndexHijo Arbol::getNextIndexHijo (const std::string & indexstr, IndexHijo initIndex) const
{
	for (size_t index = initIndex; index < this->hijos.size(); index++)
	{
		if (this->hijos[index].clave == indexstr)
		{
			return index;
		}
	}
	return Arbol::NO_INDEX;
}

/**
 *	Devuelve el �ndice donde se encuentra el iposicion-hijo con la clave dada
 *	\param	[in]	indexstr			clave a buscar
 *	\param	[in]	iposicion (opt.=0)	ordinal deseado
 *  \retval	 el �ndice esperado, si existe el iposicion-hijo buscado, o NO_INDEX (-1) si no hay un iposicion-hijo
 **/
Arbol::IndexHijo Arbol::getIndexHijoAt (const std::string & indexstr, size_t iposicion) const
{
	for (size_t index = 0; index < this->hijos.size(); index++)
	{
		if (this->hijos[index].clave == indexstr)
		{
			if (iposicion == 0)
			{
				return index;
			}
			iposicion--;
		}
	}
	return Arbol::NO_INDEX;
}
/**
 *	Devuelve el vector de �ndices donde se encuentran los hijos con la clave dada
 *	\param	[in]	indexstr			clave a buscar
 *  \retval	 el vector de �ndices esperado; pudiendo ser vac�o si no hay
 **/
Arbol::IndexHijos Arbol::getIndexHijos (const std::string & indexstr) const
{
	IndexHijos indexHijos;
	for (size_t index = 0; index < this->hijos.size(); index++)
	{
		if (this->hijos[index].clave == indexstr)
		{
			indexHijos.push_back (index);
		}
	}
	return indexHijos;
}

/**
 *	Indica si existe un atributo del elemento actual con la clave dada
 *	\param	[in]	indexstr	clave a buscar
 *  \retval	 TRUE si existe el atributo esperado; FALSE si no
 **/
bool Arbol::existeAtributo (const std::string & indexstr) const
{
	Atributos::const_iterator itr = this->atributos.find (indexstr);
	if (itr == atributos.end())
	{
		return false;
	}
	return true;
}

Arbol::Atributo Arbol::getAtributo (const Arbol::IndexHijo initIndex) const
{
	Atributos::const_iterator itr = this->atributos.begin();
	for (size_t index = 0; index < initIndex; index++)
	{
		if (itr == atributos.end())
		{
			return Arbol::Atributo();
		}
		itr++;
	}
	return *itr;
}

std::string Arbol::getValorAtributo (const std::string & indexstr) const
{
	Atributos::const_iterator itr = this->atributos.find (indexstr);
	if (itr != this->atributos.end())
	{
		return itr->second;
	}
	return "";
}

void Arbol::addAtributo (const std::string & indexstr, const std::string & valor)
{
	this->atributos[indexstr] = valor;
}

bool Arbol::setValorAtributo (const std::string & indexstr, const std::string & valor)
{
	Atributos::iterator itr = this->atributos.find (indexstr);
	if (itr == atributos.end())
	{
		return false;
	}
	itr->second = valor;
	return true;
}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Errores <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

/**
 *	Ajusta en el ".lastError" el valor de "::Errores" inicial "NO_ERRRO".
 **/
void Arbol::clearError()
{
	this->setError (SIN_ERROR);
	this->setError ("");
}

/**
 *	Ajusta en el ".lastError" el valor de "::Errores" dado
 *	\param	[in]	error	el error, como "::Errores", a ajustar
 **/
void Arbol::setError (const Arbol::Errores error)
{
	this->lastError = error;
}

/**
 *	Obtiene el �ltimo error generado
 *  \retval	 el �ltimo error generado
 **/
const Arbol::Errores & Arbol::getLastError() const
{
	return this->lastError;
}

/**
 *	Ajusta en el ".lastErrorStr" el valor de error como string dado
 *	\param	[in]	error	el error, como string, a ajustar
 **/
void Arbol::setError (const std::string & error)
{
	this->lastErrorStr = error;
}

/**
 *	Obtiene el �ltimo error generado como string
 *  \retval	 el �ltimo error generado como string
 **/
const std::string & Arbol::getLastErrorStr() const
{
	return this->lastErrorStr;
}
