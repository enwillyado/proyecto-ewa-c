/*********************************************************************************************
*	Name		: fromFile.hpp
*	Description	: M�todo de lectura de ficheros
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _FROM_FILE_HPP_
#define _FROM_FILE_HPP_

#include <string>       //< std::string
#include <fstream>		//< std::ifstream

static std::string leerContenidoFile (const std::string & filename)
{
	// Abrir el fichero
	std::ifstream infile;
	infile.open (filename.c_str(), std::ifstream::binary | std::ifstream::in);
	if (false == infile.good())
	{
		return "";
	}
	return std::string (std::istreambuf_iterator<char> (infile), std::istreambuf_iterator<char>());
}

static bool file_exists_and_can_be_read_from (const std::string & filename)
{
	std::ifstream f;
	f.open (filename.c_str(), std::ios_base::in | std::ios_base::binary);
	bool ret = f.good();
	if (f.is_open())
	{
		f.close();
	}
	return ret;
}

#endif	//_FROM_FILE_HPP_
