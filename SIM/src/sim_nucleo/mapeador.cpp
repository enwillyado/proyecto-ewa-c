#include "sim/mapeador.h"
#include <cstring>

MAP::MAP()
{
	this->t = TAMANIO_RAM;
	this->c = new bool[this->t];
	this->o = 0;
}
MAP::~MAP()
{
	if (this->c != NULL)
	{
		delete[] this->c;
		this->c = NULL;
	}
}

const Direccion MAP::dameDireccion (const bool puntero[PAGINAS][COMPONT])
{
	unsigned int potencia = 1;
	Direccion direccion = 0;
	for (int i = 0; i < PAGINAS; i++)
	{
		for (int j = 0; j < COMPONT; j++)
		{
			direccion += puntero[i][j] * potencia;
			potencia *= 2;
		}
	}
	return direccion;
}
bool & MAP::dameRegistro (const bool puntero[PAGINAS][COMPONT])
{
	const Direccion & direccion = MAP::dameDireccion (puntero);
	return this->dameRegistro (direccion);
}

bool & MAP::dameRegistro (const Direccion direccion)
{
	if (direccion < this->t)
	{
		return this->c[direccion];
	}
	return this->o;
}
