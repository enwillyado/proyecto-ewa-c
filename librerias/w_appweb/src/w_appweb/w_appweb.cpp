/*********************************************************************************************
 * Name			: w_appweb.cpp
 * Description	: Implementación de la clase para gestionar una WAppWeb
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_appweb/w_appweb.h"

#include "w_arbol/w_arbolxml.h"

#define COMILLAS_DOBLES "\""

// ----------------------------------------------------------------------------
// Constructores
WAppWeb::WAppWeb()
{
}

void procesaraWe (const Arbol & awe, std::string & ret)
{
	for (size_t i = 0; i < awe.hijosize(); i++)
	{
		const Arbol & hijoaWe = awe[i];
		if (hijoaWe.getClave() == "div")
		{
			ret += "<div";
			if (hijoaWe.existeAtributo ("nombre"))
			{
				ret += " name=" COMILLAS_DOBLES + (hijoaWe % "nombre") + COMILLAS_DOBLES;
			}
			ret += " style=" COMILLAS_DOBLES;
			if (hijoaWe.existeAtributo ("y"))
			{
				ret += "top:" + (hijoaWe % "y") + "px;";
			}
			if (hijoaWe.existeAtributo ("x"))
			{
				ret += "left:" + (hijoaWe % "x") + "px;";
			}
			if (hijoaWe.existeAtributo ("ancho"))
			{
				ret += "width:" + (hijoaWe % "ancho") + "px;";
			}
			if (hijoaWe.existeAtributo ("alto"))
			{
				ret += "heigth:" + (hijoaWe % "alto") + "px;";
			}
			ret += COMILLAS_DOBLES ">";
			procesaraWe (hijoaWe, ret);
			ret += "</div>";
		}
		else if (hijoaWe.getClave() == "texto")
		{
			ret += "<span name=" + (hijoaWe % "nombre") + ">";
			ret += hijoaWe.getValor();
			ret += "</span>";
		}
		else if (hijoaWe.getClave() == "?")
		{
			// TODO
		}
	}
}
std::string procesaraWe (const Arbol & awe)
{
	std::string ret;

	procesaraWe (awe, ret);

	return ret;
}

bool WAppWeb::loadaWe (const std::string & fichero)
{
	const Arbol & awe = ArbolXML::getArbolFromFile (fichero);
	if (awe.getClave() != "awe")
	{
		return false;
	}

	this->fichero = fichero;
	this->respuesta = procesaraWe (awe);

	return true;
}

std::string WAppWeb::getRespuesta()
{
	loadaWe (this->fichero);
	return this->respuesta;
}


Mensaje WAppWeb::procesaMsgWebRecv (const Mensaje & mensajeRecibido)
{
	Mensaje ret;
	ret.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	ret.setVersionProtocolo (Mensaje::HTTP1_0);
	ret.setError (Mensaje::FOUND);
	ret.setBody (this->getRespuesta());
	return ret;
}

Mensaje procesaMsgWebRecv (ServidorWebTCP & servidor, const EventoServidorTCP & evento,
						   const Mensaje & mensajeRecibido)
{
	WAppWeb & appWeb = servidor.getWebCallBackObj<WAppWeb>();
	return appWeb.procesaMsgWebRecv (mensajeRecibido);
}

bool WAppWeb::start (const std::string & uri, const std::string & port)
{
	server.setWebCallBack (&::procesaMsgWebRecv, this);

	server.open (port);
	return server.start();
}