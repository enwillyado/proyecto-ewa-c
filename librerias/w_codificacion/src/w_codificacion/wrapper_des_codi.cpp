/*********************************************************************************************
 *	Name		: wrapper_des_cpp.cpp
 *	Description	: Wrapper de C++ para algoritmo de DES soportando los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion.h"

//____________________________________________________________________________________________
//
// Encriptar
//____________________________________________________________________________________________

/**
 * Encripta un buffer mediante el m�todo DES
 *
 * Por defecto los valores tomados por nPadding, mode y config vienen determinados por las
 * constantes DES_DEFAULT_PADDING, DES_DEFAULT_DESMODE y DES_DEFAULT_CONFIG respectivamente.
 * Estas constantes pueden ser declaradas antes de incluir el .h, o bien pueden usarse las que
 * vienen por defecto
 *
 * \param [in] bufferIn			Buffer a encriptar
 * \param [in] szBufferIn		Tama�o del buffer a encriptar
 * \param [out] bufferOut		Buffer destino para el resultado
 * \param [in,out] szBufferOut	Tama�o del buffer destino para el resultado
 * \param [in] key1				Contrase�a 1 del algoritmo
 * \param [in] key2				Contrase�a 2 del algoritmo
 * \param [in] nPadding
 * \param [in] mode				DesMode1Des - Algoritmo Des simple
 *								DesMode3Des - Algoritmo 3 Des
 * \param [in] config			DesConfigChainBlockCipher - Se emplea Chain Block Cipher (CBC)
 *								DesConfigElectronicCodeBook - Se emplea Electronic Code Book (ECB)
 * \return	EncryptOk		Encryptacion ha sido correcta
 *			EncryptError	El buffer entregado para la salida es demasiado peque�o
 **/
EEncryptResult desEncrypt (const void* bufferIn, size_t szBufferIn, void* bufferOut, size_t* szBufferOut,
						   const Des3Key key1, const Des3Key key2, int nPadding, EDesMode mode, EDesConfig config)
{
	return desEncryptBuffer (bufferIn, szBufferIn, bufferOut, szBufferOut, key1, key2, nPadding, mode, config);
}

/**
 * Encripta un buffer mediante el m�todo DES
 *
 * Por defecto los valores tomados por nPadding, mode y config vienen determinados por las
 * constantes DES_DEFAULT_PADDING, DES_DEFAULT_DESMODE y DES_DEFAULT_CONFIG respectivamente.
 * Estas constantes pueden ser declaradas antes de incluir el .h, o bien pueden usarse las que
 * vienen por defecto
 *
 * \param [in] bufferIn			Buffer a encriptar
 * \param [in] szBufferIn		Tama�o del buffer a encriptar
 * \param [out] bufferOut		Buffer destino para el resultado
 * \param [in] key1				Contrase�a 1 del algoritmo
 * \param [in] key2				Contrase�a 2 del algoritmo
 * \param [in] nPadding
 * \param [in] mode				DesMode1Des - Algoritmo Des simple
 *								DesMode3Des - Algoritmo 3 Des
 * \param [in] config			DesConfigChainBlockCipher - Se emplea Chain Block Cipher (CBC)
 *								DesConfigElectronicCodeBook - Se emplea Electronic Code Book (ECB)
 * \return	EncryptOk		Encryptacion ha sido correcta
 *			EncryptError	El buffer entregado para la salida es demasiado peque�o
 **/
EEncryptResult desEncrypt (const void* bufferIn, size_t szBufferIn, std::vector<unsigned char> & bufferOut,
						   const Des3Key key1, const Des3Key key2, int nPadding, EDesMode mode, EDesConfig config)
{
	size_t szBufferOut;

	bufferOut.resize (DesCalculateSizeEncryptBuffer (nPadding, config, szBufferIn));
	szBufferOut = bufferOut.size();
	return desEncryptBuffer (bufferIn, szBufferIn, &bufferOut[0], &szBufferOut, key1, key2, nPadding, mode, config);
}

/**
 * Encripta un buffer mediante el m�todo DES
 *
 * Por defecto los valores tomados por nPadding, mode y config vienen determinados por las
 * constantes DES_DEFAULT_PADDING, DES_DEFAULT_DESMODE y DES_DEFAULT_CONFIG respectivamente.
 * Estas constantes pueden ser declaradas antes de incluir el .h, o bien pueden usarse las que
 * vienen por defecto
 *
 * \param [in] stringIn				Cadena de caracteres a encriptar
 * \param [out] bufferOut			Buffer destino para el resultado
 * \param [in,out] szBufferOut		Tama�o del buffer destino para el resultado
 * \param [in] key1					Contrase�a 1 del algoritmo
 * \param [in] key2					Contrase�a 2 del algoritmo
 * \param [in] nPadding
 * \param [in] mode					DesMode1Des - Algoritmo Des simple
 *									DesMode3Des - Algoritmo 3 Des
 * \param [in] config				DesConfigChainBlockCipher - Se emplea Chain Block Cipher (CBC)
 *									DesConfigElectronicCodeBook - Se emplea Electronic Code Book (ECB)
 * \return	EncryptOk		Encryptacion ha sido correcta
 *			EncryptError	El buffer entregado para la salida es demasiado peque�o
 **/
EEncryptResult desEncrypt (const std::string & stringIn, void* bufferOut, size_t* szBufferOut,
						   const Des3Key key1, const Des3Key key2, int nPadding, EDesMode mode, EDesConfig config)
{
	return desEncrypt (stringIn.c_str(), stringIn.size(), bufferOut, szBufferOut, key1, key2, nPadding, mode, config);
}

/**
 * Encripta un buffer mediante el m�todo DES
 *
 * Por defecto los valores tomados por nPadding, mode y config vienen determinados por las
 * constantes DES_DEFAULT_PADDING, DES_DEFAULT_DESMODE y DES_DEFAULT_CONFIG respectivamente.
 * Estas constantes pueden ser declaradas antes de incluir el .h, o bien pueden usarse las que
 * vienen por defecto
 *
 * \param [in] stringIn			Cadena de caracteres a encriptar
 * \param [out] bufferOut		Buffer destino para el resultado
 * \param [in] key1				Contrase�a 1 del algoritmo
 * \param [in] key2				Contrase�a 2 del algoritmo
 * \param [in] nPadding
 * \param [in] mode				DesMode1Des - Algoritmo Des simple
 *								DesMode3Des - Algoritmo 3 Des
 * \param [in] config			DesConfigChainBlockCipher - Se emplea Chain Block Cipher (CBC)
 *								DesConfigElectronicCodeBook - Se emplea Electronic Code Book (ECB)
 * \return	EncryptOk		Encryptacion ha sido correcta
 *			EncryptError	El buffer entregado para la salida es demasiado peque�o
 **/
EEncryptResult desEncrypt (const std::string & stringIn, std::vector<unsigned char> & bufferOut,
						   const Des3Key key1, const Des3Key key2, int nPadding, EDesMode mode, EDesConfig config)
{
	return desEncrypt (stringIn.c_str(), stringIn.size(), bufferOut, key1, key2, nPadding, mode, config);
}

/**
 * Encripta un buffer mediante el m�todo DES
 *
 * Por defecto los valores tomados por nPadding, mode y config vienen determinados por las
 * constantes DES_DEFAULT_PADDING, DES_DEFAULT_DESMODE y DES_DEFAULT_CONFIG respectivamente.
 * Estas constantes pueden ser declaradas antes de incluir el .h, o bien pueden usarse las que
 * vienen por defecto
 *
 * \param [in] bufferIn				Buffer a encriptar
 * \param [out] bufferOut			Buffer destino para el resultado
 * \param [in,out] szBufferOut		Tama�o del buffer destino para el resultado
 * \param [in] key1					Contrase�a 1 del algoritmo
 * \param [in] key2					Contrase�a 2 del algoritmo
 * \param [in] nPadding
 * \param [in] mode					DesMode1Des - Algoritmo Des simple
 *									DesMode3Des - Algoritmo 3 Des
 * \param [in] config				DesConfigChainBlockCipher - Se emplea Chain Block Cipher (CBC)
 *									DesConfigElectronicCodeBook - Se emplea Electronic Code Book (ECB)
 * \return	EncryptOk		Encryptacion ha sido correcta
 *			EncryptError	El buffer entregado para la salida es demasiado peque�o
 **/
EEncryptResult desEncrypt (const std::vector<unsigned char> & buffeIn, void* bufferOut, size_t* szBufferOut,
						   const Des3Key key1, const Des3Key key2, int nPadding, EDesMode mode, EDesConfig config)
{
	return desEncrypt ((void*) (&buffeIn[0]), buffeIn.size(), bufferOut, szBufferOut, key1, key2, nPadding, mode, config);
}

EEncryptResult desEncrypt (const std::vector<unsigned char> & buffeIn, std::vector<unsigned char> & bufferOut,
						   const Des3Key key1, const Des3Key key2, int nPadding, EDesMode mode, EDesConfig config)
{
	return desEncrypt ((void*) (&buffeIn[0]), buffeIn.size(), bufferOut, key1, key2, nPadding, mode, config);
}
