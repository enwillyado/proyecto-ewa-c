/*********************************************************************************************
 *	Name		: _w_web_srvtcp.cpp
 *	Description	: Implementaci�n del Servidor Web TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_web/w_web_srvtcp.h"
#include "w_web/w_web_services.h"
#include "w_web/w_web_catalogo.h"

// --------------------------------------------------------------------------
// Implementaci�n del Servidor TCP

ServidorWebTCP::ServidorWebTCP()
	: ServidorTCP()
{
	this->webCallBack = NULL;
	this->webCallBackObj = NULL;
}

// --------------------------------------------------------------------------
// L�gica del Servidor:
/**
 *	Env�a un mensaje de respuesta al evento dado
 *	\param	evento	[in]	evento que se va a procesar
 *	\param	mensaje	[in]	mensaje que se va a (intentar) enviar
 *	\retval	TRUE si se envi� el mensaje correctamente
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorWebTCP::send (const EventoServidorTCP & evento, const Mensaje & mensaje)
{
	return this->ServidorTCP::send (evento, mensaje.genMsg());
}

/**
 *	Analiza un nuevo mensaje recibido para procesarlo si se conoce el protocolo que tiene
 *	\param	evento	[in]	evento (de MENSAJE_RECIBIDO) que se va a analizar
 *	\retval	TRUE si el evento fue analizado; FALSE si no
**/
bool ServidorWebTCP::analizaEventoMsgRecv (const EventoServidorTCP & evento)
{
	if (this->mapaEventosServidorTCP.find (evento.socketID) != this->mapaEventosServidorTCP.end())
	{
		// Si ya estaba en el mapa de eventos, actualizar el evento recibido con lo que hab�a
		const EventoServidorTCP & eventoPrevio = this->mapaEventosServidorTCP[evento.socketID];
		const_cast<EventoServidorTCP &> (evento).mensaje = eventoPrevio.mensaje + evento.mensaje;
		this->mapaEventosServidorTCP.erase (eventoPrevio.socketID); //< y borrar el evento anterior
	}

	// Si no, procesar el mensaje
	if (evento.tipo == EventoServidorTCP::MENSAJE_RECIBIDO)	//< para cerciorarse
	{
		const Mensaje mensajeRecibido (evento.mensaje);
		// Revisar que el mensaje no est� a medias
		if (mensajeRecibido.getLastError() == Mensaje::MENSAJE_INCOMPLETO)
		{
			this->mapaEventosServidorTCP [evento.socketID] = evento;	//< incluirlo dentro del mapa de eventos
			return false;
		}

		// Una vez completo, enviar al callback si est� configurado
		if (this->webCallBack != NULL)
		{
			// Obtener la respuesta del callback
			const Mensaje & respuesta = this->webCallBack (*this, evento, mensajeRecibido);

			// Procesar la respuesta
			this->procesaMsgWebEnv (evento, mensajeRecibido, respuesta);
		}
		else
		{
			// Una vez completo, revisar el tipo del mensaje y si no es <UNKNOWN> procesarlo
			const Mensaje::TiposMensaje tipo = mensajeRecibido.getTipoMensaje();
			if (tipo != Mensaje::UNKNOWN)
			{
				// se ha recibido conocido un Mensaje Web
				// por lo que se va a procesar
				this->procesaMsgWebRecv (evento, mensajeRecibido);
			}
			else
			{
				// si no es un protocolo conocido, procesarlo a parte
				this->procesaMsgNoWebRecv (evento);
			}
		}

		return true;
	}
	return false;
}

/**
 *	Procesa un evento nuevo. Es un evento no web (mensaje sin formato conocido)
 *	\param	evento		[in]	evento que se va a procesar
**/
void ServidorWebTCP::procesaMsgNoWebRecv (const EventoServidorTCP & evento)
{
	// si no es un protocolo conocido, se ignorar�
	// TODO: �dar alg�n mensaje de despedida?
	this->close (evento);	// cerrar la conexi�n
}

/**
 *	Procesa un evento nuevo. Se supone que ser� del Protocolo DHTTP (y "tipoMensaje" ser�a DPROT/GET/POST..)
 *	\param	evento			[in]	evento que se va a procesar
 *	\param	mensajeRecibido	[in]	peticion que se va a procesar
**/
void ServidorWebTCP::procesaMsgWebRecv (const EventoServidorTCP & evento, const Mensaje & mensajeRecibido)
{
	// Obtener la respuesta
	const Mensaje & respuesta = this->responderMsgWebRecv (evento, mensajeRecibido);

	// Procesar la respuesta
	this->procesaMsgWebEnv (evento, mensajeRecibido, respuesta);
}

/**
 *	Procesa un evento nuevo. Se supone que ser� del Protocolo DHTTP (y "tipoMensaje" ser�a DPROT/GET/POST..)
 *	\param	evento			[in]	evento que se va a procesar
 *	\param	mensajeRecibido	[in]	peticion que se va a procesar
 *	\return	respuesta al evento recibido
**/
Mensaje ServidorWebTCP::responderMsgWebRecv (const EventoServidorTCP & evento, const Mensaje & mensajeRecibido)
{
	Mensaje respuesta;	//< objeto que almacenar� la respuesta

	// Buscar el servicio
	const std::string & servicioSolicitado = mensajeRecibido.getURI();
	const BasicSrvService* servicio = BasicSrvServicesCatalogo::getService (servicioSolicitado);
	if (servicio == NULL)
	{
		// si no hay el servicio solicitado...
		const BasicSrvService* servicio = BasicSrvServicesCatalogo::getService (SRVSERVICE_SENDINTERN_ID);
		if (servicio == NULL)
		{
			// Si no hay nada, se da una respuesta hardcodeada
			const std::string & mensajeRespondido = "<hr/><pre><code>" + evento.mensaje + "</code></pre><hr/>";
			// TODO: �dar alg�n mensaje de despedida 'personalizado' alegando la falta del servicio solicitado?
			respuesta.setRespuestaWebPorDefecto (mensajeRespondido, Mensaje::NOT_FOUND);
			respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);	// por si se desea terminar en otro sitio
		}
		else
		{
			// si lo hay, solicitarle una respuesta
			respuesta = servicio->ejecutarServicio (evento, mensajeRecibido);
		}
	}
	else
	{
		// si lo hay, solicitarle una respuesta
		respuesta = servicio->ejecutarServicio (evento, mensajeRecibido);
	}
	return respuesta;
}

/**
 *	Procesa la respuesta dada sobre el evento indicado.
 *	\param	evento			[in]	evento que se va a procesar
 *	\param	mensajeRecibido	[in]	peticion que se va a procesar
 *	\param	respuesta		[in]	respuesta que se va a procesar
**/
void ServidorWebTCP::procesaMsgWebEnv (const EventoServidorTCP & evento, const Mensaje & mensajeRecibido,
									   const Mensaje & respuesta)
{
	// Procesar la respuesta dada salvo que sea <RESPONSE_THREADED>
	if (respuesta.getTipoMensaje() != Mensaje::RESPONSE_THREADED)
	{
		// Se env�a la respuesta generada
		this->send (evento, respuesta);

		// Y si hay qeu cerrar el socket, se cierra
		if (respuesta.getTipoMensaje() == Mensaje::RESPONSE_AND_EXIT)
		{
			// cerrar bajo demanda (si "respuesta" lo solicita)
			this->ServidorTCP::close (evento);	// se cierra la conexi�n por demanda
		}
	}
}

/**
 *	Ajusta la funci�n a la que se llamar� cuando se reciba un mensaje correcto y completo en el servidor
 *	\param	ptr				[in]	puntero a la funci�n que se llamar� cuando se reciba el mensaje
 *	\param	webCallBackObj	[in]	puntero al objeto que se cargar�
 *	\retval	TRUE si se ajust� el callback, FALSE si no
**/
bool ServidorWebTCP::setWebCallBack (WebCallBack ptr, void* webCallBackObj)
{
	this->webCallBack = ptr;
	this->webCallBackObj = webCallBackObj;
	return (ptr != NULL);
}

void* ServidorWebTCP::getWebCallBackObjPtr() const
{
	return this->webCallBackObj;
}
