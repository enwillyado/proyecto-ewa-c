/*********************************************************************************************
 *	Name		: w_base_tester_smart.cpp
 *	Description	: Pruebas unitarias para probar los mecanismos de inteligencia
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_base/smartAction.hpp"

#include <assert.h>

class Letra
{
public:
	const char getChar() const
	{
		return this->caracter;
	}
	void setChar (const char & value)
	{
		this->caracter = value;
	}

private:
	char caracter;
};

void f (Letra & letra)
{
	assert ('f' == letra.getChar());
}

class SmartActionLetra : public SmartActionObj<Letra>
{
public:
	SmartActionLetra (Letra & obj, const char & tmpValue)
		: SmartActionObj (obj, &Letra::getChar, &Letra::setChar, tmpValue)
	{
	}
};

int baseTesterSmart()
{
	// ---------------------------------------------------------------------------
	// SmartAction
	{
		Letra letra;
		letra.setChar ('a');
		assert ('a' == letra.getChar());
		{
			SmartAction tmp (letra, &Letra::getChar, &Letra::setChar, 'c');
			assert ('c' == letra.getChar());
		}
		{
			::f (SmartActionObj<Letra> (letra, &Letra::getChar, &Letra::setChar, 'f'));
			assert ('a' == letra.getChar());
		}
		{
			::f (SmartActionLetra (letra, 'f'));
			assert ('a' == letra.getChar());
		}
		assert ('a' == letra.getChar());
	}
	return 0;
}
