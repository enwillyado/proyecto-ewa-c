/*********************************************************************************************
 *	Name		: w_sistema_execs.h
 *	Description	: Clases para llamar a comandos
 *	Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_EXECS_H_
#define _W_SISTEMA_EXECS_H_

#include "w_sistema/w_sistema_execs_simple.h"		//< SistemaExecsSimple
#include "w_sistema/w_sistema_execs_bi.h"			//< SistemaExecsBi

#endif // _W_SISTEMA_EXECS_H_
