/*********************************************************************************************
 *	Name		: _sim.pragmalib.h
 *	Description	: Header file with unified libraries to link
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _SIM_PRAGMALIB_H_
#define _SIM_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#ifndef END_LIB_EWA
#ifdef _DEBUG
#define END_LIB_EWA "d.lib"
#else
#define END_LIB_EWA ".lib"
#endif
#endif

#pragma message("Enlazando con las librer�as del proyecto 'sim'...")

#pragma comment(lib, "sim" END_LIB_EWA)

#include "sim/_sim_nucleo.pragmalib.h"

#endif

#endif // _SIM_PRAGMALIB_H_
