/*********************************************************************************************
 *	Name		: w_sistema_execs_simple.h
 *	Description	: Clase para llamar a un comando simple
 *	Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _SISTEMA_EXECS_SIMPLE_H_
#define _SISTEMA_EXECS_SIMPLE_H_

#include "w_sistema/w_sistema_execs_base.h"

class SistemaExecsSimple : public SistemaExecsBase
{
public:
	SistemaExecsSimple();

	// Inicio
	virtual SistemaExecsSimple::Errores exeComando();
};

#endif // _SISTEMA_EXECS_SIMPLE_H_
