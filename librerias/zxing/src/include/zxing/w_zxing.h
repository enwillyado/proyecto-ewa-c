/*********************************************************************************************
 *	Name		: w_zxing.h
 *	Description	: Clase para obtener
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ZXING_H_
#define _W_ZXING_H_

#include "w_graficos/w_lienzoBase.h"
#include "zxing/w_zxing_result.h"

class WZxing
{
public:
	static WZxingResult procesar (const WLienzoBase &);
};

#endif // _W_ZXING_H_
