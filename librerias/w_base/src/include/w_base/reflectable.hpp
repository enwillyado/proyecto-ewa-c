/*********************************************************************************************
*	Name		: reflectable.hpp
*	Description	: Macro para permitir la reflacción en Clases de C++
*  Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _REFLECTABLE_HPP_
#define _REFLECTABLE_HPP_

#include "w_base/macros_variadic.hpp"

#include <string>

//-----------------------------------------------------------------------------
#define REMOVE_BRACKETS(...) __VA_ARGS__
#define REMOVE_NEXT(...)
#define STRIP_TYPE(x) REMOVE_NEXT x
#define DECLARE_DATA_MEMBER(x) REMOVE_BRACKETS x
#define TYPE_ONLY(x) x REMOVE_NEXT(
#define STRIP_NAME_(x) PP_EXPAND(TYPE_ONLY x))
#ifdef WIN32
#define STRIP_NAME(x) PP_EXPAND(STRIP_NAME_ x)
#else
#define STRIP_NAME(x) ""
#endif
//-----------------------------------------------------------------------------
#define REFLECTABLE(...) \
	friend struct Reflector;\
	typedef int CountType;\
	static const CountType number_of_fields = PP_TUPLE_SIZE(__VA_ARGS__);\
	\
	template<int N, class Parent = void>\
	struct FieldDataBase {};\
	\
	template<int N, class Parent = void>\
	struct FieldData {};\
	\
	PP_ITERATE(REFLECT_ITEARE_EACH, __VA_ARGS__)

//-----------------------------------------------------------------------------
#define REFLECT_ITEARE_EACH(x, i) \
	DECLARE_DATA_MEMBER(x);\
	\
	/*Declarate FielData<i>*/\
	template<class Self>\
	struct FieldDataBase<i, Self> \
	{\
		typedef TYPE_ONLY x) SelfType;\
		\
		const char* const name() const\
		{\
			return PP_STRINGIZE (STRIP_TYPE (x));\
		}\
		\
		const char* const type() const\
		{\
			return PP_STRINGIZE(STRIP_NAME((x)));\
		}\
	};\
	template<class Self>\
	struct FieldData<i, Self> : FieldDataBase<i, Self>\
	{\
		Self & self;\
		FieldData (Self & s) : self(s), FieldDataBase<i, Self>() {} \
		typename FieldDataBase<i, Self>::SelfType & get()\
		{\
			return self.STRIP_TYPE (x);\
		}\
	};\
	template<class Self>\
	struct FieldData<i, const Self> : FieldDataBase<i, Self>\
	{\
		const Self & self;\
		FieldData (const Self & s) : self(s), FieldDataBase<i, Self>() {} \
		const typename FieldDataBase<i, Self>::SelfType & get() const\
		{\
			return self.STRIP_TYPE (x);\
		}\
	};\
	\
	 
//-----------------------------------------------------------------------------

struct Reflector
{
	// Get number of fields
	template<class T>
	struct FieldCounter
	{
		// Reflector is a friend of T, so has access to the private member T::number_of_fields
		typedef typename T::CountType CountType;
		static const CountType number_of_fields = T::number_of_fields;
	};

	// Get a FieldData instance for the N'th field in type T
	template<int N, class T>
	static typename T::template FieldData<N, T> getFieldData (T & x)
	{
		return typename T::template FieldData<N, T> (x);
	}
	template<int N, class T>
	static typename T::template FieldData<N, const T> getFieldData (const T & x)
	{
		return typename T::template FieldData<N, const T> (x);
	}

	// Get a FieldData instance for the N'th field in type T
	template<int N, class T>
	static typename T::template FieldDataBase<N, T>::SelfType & getFieldDataValue (T & x)
	{
		return getFieldData<N, T> (x).get();
	}

	template<int N, class T>
	static const typename T::template FieldDataBase<N, const T>::SelfType & getFieldDataValue (const T & x)
	{
		return getFieldData<N, const T> (x).get();
	}
};

#endif	//_REFLECTABLE_HPP_