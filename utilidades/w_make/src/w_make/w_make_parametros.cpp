/*********************************************************************************************
 *	Name		: w_make_parametros.cpp
 *	Description	: Implementanción del constructor por defecto con los parámetros predefinidos
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_make/w_make_parametros.h"

WMakeParametros::WMakeParametros()
{
}

void WMakeParametros::pushLibOrg (const std::string & ficheroOrigen)
{
	this->libOrigen.push_back (ficheroOrigen);
}

void WMakeParametros::pushSrcOrg (const std::string & ficheroOrigen)
{
	this->srcOrigen.push_back (ficheroOrigen);
}
