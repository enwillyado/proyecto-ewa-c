/*********************************************************************************************
 * Name			: w_web_services.cpp
 * Description	: Implementaci�n del mecanismo para responder a un determinado
 *					servicio que ejecutar� el servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/

#include "w_web/w_web_services.h"

/*************************************************************************/
/* LOS SERVICIOS SE IMPLEMENTAN A PARTIR DE AQU�,                        */
/* 		IMPLEMENTANDO LA FUNCI�N MIEMBRO "ejecutarServicio()"            */
/* A d�a de hoy, la forma segura de a�adir al cat�logo es con la funci�n */
/* del cat�logo "addService(...)" que devuelve bool si se registr� bien. */
/*                                                                       */
/* ATENCI�N: por la forma en la que se inicializa el mapa de servicios,  */
/*				se deben realizar los "addService(...)" en los ficheros  */
/*				".cpp" junto al main, aunque la implementaci�n de las    */
/*				funciones miembro s� se pueden implementar aqu� o en     */
/*				otros ficheros ".cpp". Igualmente, se puede a�adir el    */
/*				servicio en cualquier otro lugar a partir del main; es   */
/*				decir, siempre y cuando sea dentro de una funci�n no     */
/*				global. Por supuesto, se puede eliminar del cat�logo     */
/*				cuando venga en gana.                                    */
/**************************************************************************/

#include <fstream>
#include <iostream>
inline static std::string leerContenidoFile (const std::string & filename)
{
	// Abrir el fichero
	std::ifstream infile;
	infile.open (filename.c_str(), std::ifstream::binary | std::ifstream::in);
	if (false == infile.good())
	{
		return "";
	}
#ifdef LEER_CARACTER_A_CARACTER
	// loop while extraction from file is possible
	std::string buffer;
	while (infile.good())
	{
		const char c = infile.get();	// HACK: "wchar_t" | get character from file
		if (infile.good())
		{
			buffer.push_back (c);
		}
	}
	infile.close();
	return buffer;
#else
	return std::string (std::istreambuf_iterator<char> (infile), std::istreambuf_iterator<char>());
#endif
}

// ----------------------------------------------------------------------------
// Servicio para cuando se solicite un ping
Mensaje BasicSrvService_Ping::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setError (Mensaje::FOUND);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_Ping('pong');");
	return respuesta;
}


// ----------------------------------------------------------------------------
// Servicio para cuando se solicite una descarga
Mensaje BasicSrvService_Download::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	// Leer el contenido de fichero deseado
#ifdef _DEBUG
	const std::string & fichero = peticion.existeRequestVar ("file") == false ? "w_red_testerDebug.exe" : peticion.getRequestVar ("file");
#else
	const std::string & fichero = peticion.existeRequestVar ("file") == false ? "w_red_testerRelease.exe" : peticion.getRequestVar ("file");
#endif
	const std::string & contenido = leerContenidoFile (fichero);

	// Generar la respuesta
	Mensaje respuesta;
	respuesta.setVersionProtocolo (Mensaje::HTTP1_1);
	respuesta.setError (Mensaje::FOUND);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody (contenido);
	respuesta % ENCABEZADOID_CONTENT_TYPE = ENCABEZADOID_CONTENT_TYPE_BINARY;
	respuesta % ENCABEZADOID_CONTENT_DISPOSITION = ENCABEZADOID_CONTENT_DISPOSITION_FILENAME (fichero);
	return respuesta;
}
