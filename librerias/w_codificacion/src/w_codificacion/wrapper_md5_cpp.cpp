/*********************************************************************************************
 *	Name		: wrapper_md5_cpp.cpp
 *	Description	: Wrapper de C++ para algoritmo de MD5 soportando los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion.h"

void md5Create (const void* bufferIn, size_t szBufferIn, MD5 md5Out)
{
	md5CreateFromBuffer (bufferIn, (unsigned int) szBufferIn, md5Out);
}

void md5Create (const char* stringIn, MD5 md5Out)
{
	md5CreateFromString (stringIn, md5Out);
}


void md5Create (const std::string & stringIn, MD5 md5Out)
{
	md5CreateFromBuffer (stringIn.c_str(), (unsigned int)stringIn.size(), md5Out);
}

/**
 *	Codifica en MD5 la cadena dada
 *	\param [in] value 	una cadena a codificar
 *	\retval		la cadena codificada en MD5
**/
std::string md5Str (const std::string & value)
{
	MD5 md5Hash;
	::md5Create (value, md5Hash);
	MD5STR md5str;
	::md5ToStr (md5Hash, md5str);
	std::string retstr;
	retstr.resize (sizeof (MD5STR) - 1);
	for (size_t i = 0; i < retstr.size(); i++)
	{
		retstr[i] = md5str[i];
	}
	return retstr;
}
