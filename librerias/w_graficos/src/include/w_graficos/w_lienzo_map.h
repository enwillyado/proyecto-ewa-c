/*********************************************************************************************
 * Name			: w_lienzo_map.h
 * Description	: Clase para manipular una representación gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_LIENZO_MAP_H_
#define _W_LIENZO_MAP_H_

#include "w_graficos/w_lienzoBase.h"

#include <string>

// FWD
class WControl;

class WLienzoMap : public WLienzoBase
{
public:
	// Constructores
	WLienzoMap();
	WLienzoMap (const WLienzoBase &);
	~WLienzoMap();

	// Operadores
	void operator= (const WLienzoBase &);

	// Accesores
	size_t getAncho() const;
	size_t getAlto() const;
	RGBA getPixel (const size_t x, const size_t y) const;

	// Modificadores
	void resize (const size_t ancho, const size_t alto);

	bool setPixel (const size_t x, const size_t y, const RGBA &);

protected:
	void reserveIntern();
	size_t ancho;
	size_t alto;

private:
	virtual RGBA getPixelFast (const size_t x, const size_t y) const;
	virtual void setPixelFast (const size_t x, const size_t y, const RGBA &);

	class WLienzoIntern;
	WLienzoIntern & data;
};

#endif // _W_LIENZO_MAP_H_
