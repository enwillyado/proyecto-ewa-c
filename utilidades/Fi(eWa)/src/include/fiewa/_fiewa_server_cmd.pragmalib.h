/*********************************************************************************************
 *	Name		: _fiewa_server_cmd.pragmalib.h
 *	Description	: Header file with unified libraries to link
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _FIEWA_SERVER_CMD_PRAGMALIB_H_
#define _FIEWA_SERVER_CMD_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#pragma message("Enlazando con las librer�as del proyecto 'fiewa_server_cmd'...")

#include "fiewa/_fiewa_services.pragmalib.h"

#include "w_web/_w_web_server.pragmalib.h"
#include "w_web/_w_web_services.pragmalib.h"
//#include "dlog/_dlog_pragmalib.h"

#endif

#endif // _FIEWA_SERVER_CMD_PRAGMALIB_H_
