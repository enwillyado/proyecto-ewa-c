/*********************************************************************************************
 *	Name		: fiewa_server_cmd.cpp
 *	Description	: Ejecutable que pone en funcionamiento un Servidor UDP de Servicios para Pares
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "fiewa/_fiewa_server_cmd.pragmalib.h"

#include "fiewa/fiewa_defines.hpp"	//< para tener las definiciones generales
#include "fiewa/fiewa_services.h"	//< para poder tener los Servicios para Pares

#define  NO_USE_BOOST
#include "w_web/w_web_srvudp.h"		//< para tener el "ServidorWebUDP"
#include "w_web/w_web_catalogo.h"	//< para poder tener el cat�logo de servicios del servidor
#include "w_web/w_web_services.h"	//< para poder tener otros servicios del servidor

#include "w_base/fromStr.hpp"		//< para poder tener ::fromStr

//#include "dlog/dlog.h"				//< para tener el logger

// Para la gesti�n de interrupciones que puedan venir
#include <signal.h>

// Instancia est�tica del logger
//static DLog logger (DLogSalidas::POR_PANTALLA, DLogNiveles::All);

// Instancia est�tica del servidor
static ServidorWebUDP servidor;

/**
 *	Funci�n que inicia el servidor
 *	\param	[in]	port	puerto por el que se pone a la escucha
**/
void DCOUT_MESSAGE (const std::string & mensaje)
{
#ifdef _DEBUG
	//::logger.trace << mensaje << DLog::endl;
#endif
}

/**
 *	Funci�n que inicia el servidor
 *	\param	[in]	port	puerto por el que se pone a la escucha
**/
bool initServer (const int port)
{
	// Arrancar el servidor Web UDP
	::servidor = ServidorWebUDP();
	//::logger.info << "Generando un 'ServidorUDP'" << DLog::endl;
	if (false == servidor.open (port))
	{
		//::logger.error << "[Error]:servidorUDP.run() " << servidor.getLastError() << " = " <<
		//			   servidor.getLastErrorStr() << DLog::endl;
		//::logger.error << "[Error]:Error del socket: " << servidor.getLastErrorSocket() << "= " <<
		//			   servidor.getLastErrorSocketStr() << DLog::endl;
		return false;
	}
	else
	{
		const strvector & ipsv4 = ServerSocket::getIPsIPv4();
		//::logger.info << "[AVISO] Se encontraron " + HTTPNS::Protocolo::toStr (ipsv4.size()) + " inteface" +
		//			  (ipsv4.size() == 1 ? "" : "s") + " IPv4:" << DLog::endl;
		for (size_t i = 0; i < ipsv4.size(); i++)
		{
			//	::logger.debug << "[AVISO] Escuchando en la inteface IPv4: " << ipsv4[i] << DLog::endl;
		}

		if (false == servidor.start())
		{
			/*
			::logger.error << "[Error] servidorTCP.start() " << servidor.getLastError() << " = " <<
				servidor.getLastErrorStr() << DLog::endl;
			*/
		}
	}
	return true;
}

/**
 *	Funci�n que se invoca al recibir una excepcion
 *	\param	[in]	sig		id de la se�al que se recibion
**/
void intHandler (int sig = 0)
{
	::servidor.close();
}

/**
 *	Funci�n que se invoca al recibir una excepcion
 *	\param	[in]	sig		id de la se�al que se recibion
**/
int mainServer (int argc, char* argv[])
{
	const int port = (argc > 1) ? ::fromStr<int> (argv[1]) : PUERTO_EN_SERVIDOR_STUN_DEFECTO;

	// Meter en el cat�logo los servicios deseados
	BasicSrvServicesCatalogo::addService (BasicSrvService_Ping());
	BasicSrvServicesCatalogo::addService (FIEWA::BasicSrvService_IPandPORT());
	BasicSrvServicesCatalogo::addService (FIEWA::BasicSrvService_IPofPEER());
	BasicSrvServicesCatalogo::addService (FIEWA::BasicSrvService_LoginPEER());

	// Iniciar el servidor
	const int ret = (::initServer (port) == true) ? 0 : 1;

	// Vaciar el cat�logo antes de terminar para no tener memoryleaks
	BasicSrvServicesCatalogo::delAllServices();

	return ret;	// si se llega aqu�, en monohilo, malo malo
}

/**
 *	Funci�n principal
**/
int main (int argc, char* argv[])
{
	signal (SIGINT, ::intHandler);		// cuando se hace control+c
	signal (SIGTERM, ::intHandler);		// cuando se lanza un kill
#ifdef WIN32
	signal (SIGBREAK, ::intHandler);	// cuando se cierra el cmd
#endif

	int ret = mainServer (argc, argv);
	return ret;
}
