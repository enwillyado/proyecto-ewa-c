#include "whatsacppi/phone.h"
#include "whatsacppi/register.h"
#include "whatsacppi/util/log.h"
#include "whatsacppi/util/util.h"
#include "whatsacppi/util/sha1.h"

#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

using WhatsAcppi::Phone;
using WhatsAcppi::Register;
using WhatsAcppi::Util::Log;
using WhatsAcppi::Util::sha1Str;

int mainRegister (int argc, char* argv[])
{
	if (argc < 4)
	{
		cout << "Ussage:" << endl;
		cout << "\t" << argv[0] << " PHONE_NUMBER USERNAME CODE" << endl;
		return 0;
	}

	WhatsAcppi::Util::Init init;
	Log::setLogLevel (Log::DebugMsg);

	Phone phone (argv[1]);

	phone.guessPhoneInformation();

#define PRINT(X) cout << #X << ": " << phone.get##X() << endl
	PRINT (PhoneNumber);
	PRINT (Phone);
	PRINT (Country);
	PRINT (Cc);
	PRINT (Iso3166);
	PRINT (Iso639);
#undef PRINT

	string identity = sha1Str (argv[2]);
	Register reg (phone, identity);
	cout << "identity" << identity <<  endl;

	cout << "** Registering code" << endl;

	int ret = reg.codeRegister (argv[3]);
	if (ret < 0)
	{
		cout << "Error in http request! ret:" << ret << endl;
		return -1;
	}
	else if (ret)
	{
		cout << "Whats app error! ret: " << ret << endl;
		return 1;
	}

	cout << "Success! ret: " << ret << endl;
	cout << "Pw: " << reg.getPw() << endl;
	cout << "Login: " << reg.getLogin() << endl;

	return 0;
}
