#include "sim/cdPantalla.h"
#include "sim/cpu.h"
#include "sim/mmu.h"

void actualizaPlantilla (UINT xx, UINT yy, UINT zz)
{
	CBitBlt (NULL, NULL, NULL, FALSE, xx, yy, zz);
}

void CBitBlt (HDC hDC, HWND hWnd, HBITMAP hBitmap, BOOL reset, UINT xx, UINT yy, UINT zz)
{
	static HDC memDC;
	static BOOL paso;
	static UINT pasos;
	BITMAP bm;
	RECT re;

	if (xx != 0 || yy != 0 || zz != 0)
	{
		SetPixel (memDC, xx, yy, zz % 0x01000000);
	}
	else
	{
		GetClientRect (hWnd, &re);
		if (paso && reset)
		{
			DeleteDC (memDC);
		}
		if (!paso || reset)
		{
			memDC = CreateCompatibleDC (hDC);
			SelectObject (memDC, hBitmap);
			GetObject (hBitmap, sizeof (BITMAP), (LPSTR)&bm);
			paso = TRUE;
		}
		BitBlt (hDC, 0, 0, re.right, re.bottom, memDC, 0, 0, SRCCOPY);
	}
}

typedef struct MyData
{
	MMU* mmu;
	CPU* cpu;
} MYDATA, *PMYDATA;

DWORD WINAPI hiloPantalla (LPVOID lpParam)
{
	PMYDATA  pDataArray = (PMYDATA)lpParam;
	MMU* mmu = pDataArray->mmu;

	static int cuantosHubo = 0;
	static int ix = 0;
	static int jy = 0;

	while (true)
	{
		cuantosHubo++;
		char c = mmu->leer_de_Memoria (0x100);
		if (c == 1)
		{
			Beep (440, 50);
		}
		ix++;
		for (jy = 0; jy < anxo; jy++)
		{
			actualizaPlantilla (ix, jy, ix + jy);
		}
		Sleep (10);
	}
	return 0;
}
