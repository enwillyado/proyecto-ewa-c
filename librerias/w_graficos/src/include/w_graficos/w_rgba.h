/*********************************************************************************************
 * Name			: w_rgba.h
 * Description	: Clase para gestionar color RGBA
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RGBA_H_
#define _W_RGBA_H_

#include <string>

class RGBA
{
public:
	typedef unsigned char byte;

	RGBA (const RGBA::byte r = 0x00,
		  const RGBA::byte g = 0x00,
		  const RGBA::byte b = 0x00,
		  const RGBA::byte a = 0xFF);
	RGBA (const std::string & str);
	RGBA (const char* const str);

	void loadStr (const std::string & str);
	void invertColor();
	void brightness (const short u);

public:
	RGBA::byte r;
	RGBA::byte g;
	RGBA::byte b;
	RGBA::byte a;
};

#endif // _W_RGBA_H_
