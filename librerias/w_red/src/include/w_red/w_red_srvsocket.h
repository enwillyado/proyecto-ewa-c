/*********************************************************************************************
 *	Name		: _w_red_srvsocket.h
 *	Description	: Socket de Servidor
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRVSOCKET_H_
#define _W_RED_SRVSOCKET_H_

#include <stddef.h>	// para el NULL

#include <string>
#include <vector>

#define NOMBRE_EN_LOCAL			"localhost"

#define RECV_MENSAJE_MAX_SIZE	65507
#define TCP_BLOCK_SIZE			32 * 1024 * 1024	// 32 MB

typedef std::vector<std::string> strvector;

class ServidorTCP;		//< para evitar la redundancia c�clica
class ServidorUDP;		//< para evitar la redundancia c�clica
class Evento;			//< FWD: para eveitar incluir los eventos
class ServerSocket
{
public:
	enum Errores
	{
		SIN_ERROR = 0,
		ERROR_DESCONOCIDO,				//< no se pudo determinar el error
		ERROR_AL_TRANSFORMAR_PUERTO,	//< no se puede obtener el char* con el puerto dado
		ERROR_AL_OBTENER_INFO,			//< no se puede obtener la informaci�n del servidor local
		ERROR_AL_CREAR_SCKT,			//< no se puede crear un socket con la informaci�n solicitada
		ERROR_AL_RESERVAR_SCKTR,		//< no se puede bloquar el puerto solicitado en el socket creado
		ERROR_AL_ESCUCHAR_SCKT,			//< no se puede poner a la escucha en puerto reservado el socket creado
		ERROR_AL_CERRAR_SCKT,			//< no se puede cerrar el socket servidor creado
		ERROR_AL_LIBERAR_WINSOCK,		//< no se puede liberar las librer�as WINSOCK (solo en WINDOWS)

		// Errores en los clientes:
		ERROR_CLIENTE_DESCONECTADO,		//< el cliente se desconect� sin previo aviso
		ERROR_CLIENTE_DESCONOCIDO,		//< el cliente no identifica a un socket v�lido

		// Errores comunes:
		ERROR_AL_ENVIAR_MENSAJE,
		ERROR_AL_RECIBIR_MENSAJE,

	};
	ServerSocket();
	ServerSocket (ServidorTCP* ptr);
	ServerSocket (ServidorUDP* ptr);

	// M�todos p�blicos
	void setDefaults (ServidorTCP* ptr = NULL);
	void setDefaults (ServidorUDP* ptr = NULL);

	// M�todos del server socket
	bool openSocket (unsigned int port);
	bool openSocket (const std::string & port);
	bool procesarPeticiones();
	int recibirDelCliente (const int socketID, Evento* evento);
	int sendMessageTCP (const int socketID, const char* mensaje, size_t leng);
	int sendMessageUDP (const int socketID, void* idUDP, const char* mensaje, size_t leng);
	bool closeSocket (const int socketID);
	bool closeSrvSocket();

	// Observadores
	int getSockFD() const;
	unsigned int getRealPort() const;

	// Control de errores
	Errores getLastError() const;
	std::string getLastErrorStr() const;
	Errores obtainSysLastErrorSocket() const;

	// Utilidades est�ticas
	static strvector getIPsIPv4();

private:
	//Area de datos
	ServidorTCP* ptrServidorTCP;
	ServidorUDP* ptrServidorUDP;
	bool iniciado;
	Errores lastError;
	// �rea de datos para el multiserver:
	void* masterptr;  // master file descriptor list ptr
};

#endif	//_W_RED_SRVSOCKET_H_
