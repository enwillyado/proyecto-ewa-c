/*********************************************************************************************
 *	Name		: _w_arbolxml.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar la librer�a de lectura de XML
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ARBOLXML_PRAGMALIB_H_
#define _W_ARBOLXML_PRAGMALIB_H_

#ifdef _MSC_VER

#include "w_base/_pragmalib_base.h"

// Parte p�blica
#pragma comment(lib, "w_arbolxml" FIN_LIB)

// Parte privada
#include "w_arbol/_w_arbol.pragmalib.h"

#endif

#undef _W_ARBOLXML_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_ARBOLXML_PRAGMALIB_H_
