/*********************************************************************************************
 *	Name		: wrapper_base64_deco.cpp
 *	Description	: Wrapper de C++ para algoritmo de Base64 que soporta los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include <string.h>
#include "w_codificacion/w_codificacion.h"

//____________________________________________________________________________________________
//
// Decodifica
//____________________________________________________________________________________________

/**
 * Decodifica una cadena codificada en base64 y lo deja como un string de las STL
 * \param [in] stringB64In	Cadena en base 64 a decodificar
 * \param [out] stringOut	Cadena de caracteres destino
 * \return	EncryptOk		Codificación Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult base64Decode (const std::string & stringB64In, std::string & stringOut)
{
	return base64Decode (stringB64In.c_str(), stringOut);
}


/**
 * Decodifica una cadena codificada en base64 y lo deja como un buffer binario
 * \param [in] stringB64In			Cadena en base 64 a decodificar
 * \param [out] bufferB64Out		Buffer destino para la codificación en base64
 * \param [in,out] szBufferB64Out	Indica el tamaño del buffer destino para la decodificacion y
 *										recibe el tamaño ocupado del mismo
 * \return	EncryptOk		Codificación Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult base64Decode (const std::string & stringB64In, void* bufferOut, size_t* szBufferOut)
{
	return base64DecodeToBuffer (stringB64In.c_str(), bufferOut, szBufferOut);
}

/**
 * Decodifica una cadena codificada en base64 y lo deja como un vector de las STL
 * \param [in] stringB64In			Cadena en base 64 a decodificar
 * \param [out] bufferB64Out		Buffer destino para la codificación en base64
 * \param [in,out] szBufferB64Out	Indica el tamaño del buffer destino para la decodificacion y
 *										recibe el tamaño ocupado del mismo
 * \return	EncryptOk		Codificación Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult base64Decode (const std::string & stringB64In, std::vector<unsigned char> & bufferOut)
{
	EEncryptResult result;
	size_t szDecode = Base64CalculateSizeDecodedBuffer (stringB64In.size());

	if (szDecode == 0)
	{
		bufferOut.resize (1);
		//bufferOut.clear ();
		return EncryptOk;
	}

	bufferOut.clear();
	bufferOut.resize (szDecode);

	// espera que en ese tamaño se lo indiquemos
	result = base64Decode (stringB64In, (char*)&bufferOut[0], &szDecode);
	if (result == EncryptOk)
	{
		bufferOut.resize (szDecode);
	}

	return result;
}

/**
 * Decodifica una cadena codificada en base64 y lo deja como un string
 * \param [in] stringB64In	Cadena a decodificar
 * \param [out] stringOut	Cadena destino
 * \return	EncryptOk		Codificación Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult base64Decode (const char* stringB64In, std::string & stringOut)
{
	EEncryptResult result;
	size_t szDecode = Base64CalculateSizeDecodedBuffer (strlen (stringB64In));

	stringOut.clear();
	stringOut.resize (szDecode);
	szDecode++; // Le sumamos uno porque tenemos hueco para el \0, y el la funcion
	// espera que en ese tamaño se lo indiquemos
	result = base64DecodeToString (stringB64In, (char*)stringOut.c_str(), &szDecode);
	if (result == EncryptOk)
	{
		stringOut.resize (szDecode);
	}

	return result;
}

/**
 * Decodifica una cadena codificada en base64 y lo deja en un buffer binario
 * \param [in] stringB64In	Cadena a decodificar
 * \param [out] stringOut	Cadena destino
 * \return	EncryptOk		Codificación Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult base64Decode (const char* stringB64In, void* bufferOut, size_t* szBufferOut)
{
	return base64DecodeToBuffer (stringB64In, bufferOut, szBufferOut);
}

/**
 * Dada una cadena en base 64, la decodifica a su representacion original.
 * \param [in] srcStrB64	Cadena a decodificar
 * \return	Cadena resultante o vacía si falla
 **/
std::string convertBase64ToString (const std::string & srcStrB64)
{
	std::string ret;
	if (EncryptOk == base64Decode (srcStrB64, ret))
	{
		return ret.c_str();
	}
	return "";
}
