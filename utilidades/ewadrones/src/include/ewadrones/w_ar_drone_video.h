/*********************************************************************************************
 * Name			: w_ar_drone_video.h
 * Description	: Clase para el gestor de v�deo (streaming) del AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_AR_DRONE_VIDEO_H_
#define _W_AR_DRONE_VIDEO_H_

#include "w_sistema/w_sistema_thread.h"
#include "w_graficos/w_lienzo.h"

#include <cstring>				//< NULL

// FWD
class EWadrones;

class ArDroneVideo
{
public:
	ArDroneVideo (EWadrones* = NULL);
	void setPtrArDrone (EWadrones*);

	void init();

	void start();
	void stop();

	// Observadores
	void setVideoSrc (const std::string &);
	std::string getVideoSrc() const;

	// Manejadores (suscribirse o reimplementar)
	virtual void onNewLienzo();

	typedef void (*OnNewLienzo) (const WLienzo &);
	void setOnNewLienzo (OnNewLienzo cbOnNewLienzo);

public:
	// M�todos para procesar cada fotograma de v�deo recibido
	void init (const int width, const int height);
	void send (const unsigned char* const pBase, const int width, const int y);
	void end();
	void obtenerBuffer();

private:
	EWadrones* arDrone;

	// Origen del v�deo
	std::string videoSrc;

	// Lienzo donde va a dejar los fotogramas
	WLienzo lienzo;
	OnNewLienzo cbOnNewLienzo;

	// Hilo que ir� procesando el streaming
	static void initVideoFile (const WThread &);
	typedef TWThreadData<ArDroneVideo*> TWThreadDataArDroneVideo;
	WThread hiloVideo;
};

#endif // _W_AR_DRONE_VIDEO_H_
