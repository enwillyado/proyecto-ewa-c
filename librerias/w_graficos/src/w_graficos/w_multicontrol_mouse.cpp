/*********************************************************************************************
 * Name			: w_multicontrol_mouse.cpp
 * Description	: Implementaci�n de los m�todos relativos al MOUSE en MultiControles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_multicontrol.h"

#include "w_graficos/w_app.h"

// ----------------------------------------------------------------------------
// Tipos privados
typedef void (WControl::*WControlOn)();

//
// TODO: implementar correctamente la obtenci�n del "mouse"
//

// ----------------------------------------------------------------------------
// onMoveMouseOver
void WMultiControl::onMoveMouseOver()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento OnMoveMouseOver
	bool lanzado = false;
	for (size_t i = 0; i < this->WMultiControl::controles.size(); i++)
	{
		const size_t ii = this->WMultiControl::controles.size() - i - 1;
		if (this->controles[ii].ptr == NULL)
		{
			continue;
		}
		WControl & control = * (this->controles[ii].ptr);
		const bool sobre = control.esApuntadoPor (mouse);
		if (true == sobre && false == lanzado)
		{
			const WCoordenada newCoor (mouse.posX - control.getPosX(), mouse.posY - control.getPosY());
			control.setPunteroPos (newCoor);
			if (this->lastMouseOver != &control)
			{
				if (this->lastMouseOver != NULL)
				{
					this->lastMouseOver->onMoveMouseOut();
				}
				this->lastMouseOver = &control;
				control.onMoveMouseEnter();
			}
			control.onMoveMouseOver();
			lanzado = true;
		}
		else
		{
			control.onMoveMouseExt();
		}
	}

	if (lanzado == false)
	{
		// Si no se lanz�, lanzar la salida al �ltimo pulsado
		if (this->lastMouseOver != NULL)
		{
			const WCoordenada newCoor (mouse.posX - this->lastMouseOver->getPosX(), mouse.posY - this->lastMouseOver->getPosY());
			this->lastMouseOver->setPunteroPos (newCoor);
			this->lastMouseOver->onMoveMouseOut();
			this->lastMouseOver = NULL;
		}
	}

	// Control del arrastre (drag)
	if (NULL != this->lastLMouseDown)
	{
		const WCoordenada newCoor (mouse.posX - this->lastLMouseDown->getPosX(), mouse.posY - this->lastLMouseDown->getPosY());
		this->lastLMouseDown->setPunteroPos (newCoor);
		this->lastLMouseDown->onLMouseDrag();
	}
	if (NULL != this->lastMMouseDown)
	{
		const WCoordenada newCoor (mouse.posX - this->lastMMouseDown->getPosX(), mouse.posY - this->lastMMouseDown->getPosY());
		this->lastMMouseDown->setPunteroPos (newCoor);
		this->lastMMouseDown->onMMouseDrag();
	}
	if (NULL != this->lastRMouseDown)
	{
		const WCoordenada newCoor (mouse.posX - this->lastRMouseDown->getPosX(), mouse.posY - this->lastRMouseDown->getPosY());
		this->lastRMouseDown->setPunteroPos (newCoor);
		this->lastRMouseDown->onRMouseDrag();
	}

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onMoveMouseOver();
}

// ----------------------------------------------------------------------------
// onMoveMouseExt
void WMultiControl::onMoveMouseExt()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento OnMoveMouseExt
	bool lanzado = false;
	for (size_t i = 0; i < this->WMultiControl::controles.size(); i++)
	{
		const size_t ii = this->WMultiControl::controles.size() - i - 1;
		if (this->controles[ii].ptr == NULL)
		{
			continue;
		}
		WControl & control = * (this->controles[ii].ptr);
		control.onMoveMouseExt();
	}

	// Control del arrastre (drag)
	// TODO: �mantener el DRAG en movimientos EXTERNOS sin distinci�n frente a movimientos INTERNOS?
	if (NULL != this->lastLMouseDown)
	{
		const WCoordenada newCoor (mouse.posX - this->lastLMouseDown->getPosX(), mouse.posY - this->lastLMouseDown->getPosY());
		this->lastLMouseDown->setPunteroPos (newCoor);
		this->lastLMouseDown->onLMouseDrag();
	}
	if (NULL != this->lastMMouseDown)
	{
		const WCoordenada newCoor (mouse.posX - this->lastMMouseDown->getPosX(), mouse.posY - this->lastMMouseDown->getPosY());
		this->lastMMouseDown->setPunteroPos (newCoor);
		this->lastMMouseDown->onMMouseDrag();
	}
	if (NULL != this->lastRMouseDown)
	{
		const WCoordenada newCoor (mouse.posX - this->lastRMouseDown->getPosX(), mouse.posY - this->lastRMouseDown->getPosY());
		this->lastRMouseDown->setPunteroPos (newCoor);
		this->lastRMouseDown->onRMouseDrag();
	}

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onMoveMouseExt();
}

void WMultiControl::onMoveMouseOut()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento OnMoveMouseOver
	if (this->lastMouseOver != NULL)
	{
		this->lastMouseOver->onMoveMouseOut();
		this->lastMouseOver = NULL;
	}

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onMoveMouseOut();
}
// ----------------------------------------------------------------------------
// onXDownMouseOver
static bool onXDownMouseOver (WControlesPtr & controles, const WCoordenada & mouse, WControl* & last, WControlOn action)
{
	// Lanzar evento onXDownMouseOver
	for (size_t i = 0; i < controles.size(); i++)
	{
		const size_t ii = controles.size() - i - 1;
		if (controles[ii].ptr == NULL)
		{
			continue;
		}
		WControl & control = * (controles[ii].ptr);
		const bool sobre = control.esApuntadoPor (mouse);
		if (true == sobre)
		{
			// Primero la acci�n simple (XUp)
			(control.*action)();

			// Segundo guardar el control presionado
			last = &control;
			return true;
		}
	}
	last = NULL;
	return false;
}

void WMultiControl::onLDownMouseOver()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento onLDownMouseOver
	const bool lanzado = ::onXDownMouseOver (this->WMultiControl::controles, mouse, this->lastLMouseDown, &WControl::onLDownMouseOver);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onLDownMouseOver();
}

void WMultiControl::onMDownMouseOver()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento onRDownMouseOver
	const bool lanzado = ::onXDownMouseOver (this->WMultiControl::controles, mouse, this->lastMMouseDown, &WControl::onMDownMouseOver);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onMDownMouseOver();
}
void WMultiControl::onRDownMouseOver()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento onRDownMouseOver
	const bool lanzado = ::onXDownMouseOver (this->WMultiControl::controles, mouse, this->lastRMouseDown, &WControl::onRDownMouseOver);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onRDownMouseOver();
}

// ----------------------------------------------------------------------------
// onXUpMouseOver
static bool onXUpMouseOver (WControlesPtr & controles, const WCoordenada & mouse, WControl* & last, WControlOn action, WControlOn actionIf, WControlOn actionElse)
{
	bool ret = false;
	bool actionIfDone = false;
	// Lanzar evento onXUpMouseOver
	for (size_t i = 0; i < controles.size(); i++)
	{
		const size_t ii = controles.size() - i - 1;
		if (controles[ii].ptr == NULL)
		{
			continue;
		}
		WControl & control = * (controles[ii].ptr);
		const bool sobre = control.esApuntadoPor (mouse);
		if (true == sobre)
		{
			// Primero la acci�n simple (XUp)
			(control.*action)();

			// Despu�s la acci�n condicional (XClic)
			if (last == &control)
			{
				(control.*actionIf)();
				actionIfDone = true;
			}
			ret = true;
			break;
		}
	}

	// Ver si se ha levantado fuera
	if (actionIfDone == false && last != NULL)
	{
		(last->*actionElse)();
	}

	// Y quitar el �ltimo presionado
	last = NULL;
	return ret;
}

void WMultiControl::onLUpMouseOver()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento onLUpMouseOver
	const bool lanzado = ::onXUpMouseOver (this->WMultiControl::controles, mouse, this->lastLMouseDown, &WControl::onLUpMouseOver,
										   &WControl::onLClicMouseOver, &WControl::onLUpMouseOut);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onLUpMouseOver();
}

void WMultiControl::onMUpMouseOver()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento onMUpMouseOver
	const bool lanzado = ::onXUpMouseOver (this->WMultiControl::controles, mouse, this->lastMMouseDown, &WControl::onMUpMouseOver,
										   &WControl::onMClicMouseOver, &WControl::onMUpMouseOut);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onMUpMouseOver();
}

void WMultiControl::onRUpMouseOver()
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento onRUpMouseOver
	const bool lanzado = ::onXUpMouseOver (this->WMultiControl::controles, mouse, this->lastRMouseDown, &WControl::onRUpMouseOver,
										   &WControl::onRClicMouseOver, &WControl::onRUpMouseOut);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onRUpMouseOver();
}

// ----------------------------------------------------------------------------
// onWheelMouseOver
void WMultiControl::onWheelMouseOver (const WEventoMouseWheel::Direccion direccion)
{
	const WCoordenada & mouse = this->getPunteroPos();

	// Lanzar evento onWheelMouseOver
	for (size_t i = 0; i < this->controles.size(); i++)
	{
		const size_t ii = this->controles.size() - i - 1;
		if (controles[ii].ptr == NULL)
		{
			continue;
		}
		WControl & control = * (this->controles[ii].ptr);
		const bool sobre = control.esApuntadoPor (mouse);
		if (true == sobre)
		{
			// Realizar la acci�n
			control.onWheelMouseOver (direccion);
			return;
		}
	}

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onWheelMouseOver (direccion);
}

// ----------------------------------------------------------------------------
// onXUpMouseOut
static bool onXUpMouseOut (WControl* & last, WControlOn actionElse)
{
	bool ret = false;

	// Ver si se ha levantado fuera
	if (last != NULL)
	{
		(last->*actionElse)();

		// Y quitar el �ltimo presionado
		last = NULL;
		ret = true;
	}

	// Indicar si se lanz�
	return ret;
}

void WMultiControl::onLUpMouseOut()
{
	// Lanzar evento onLUpMouseOut
	const bool lanzado = ::onXUpMouseOut (this->lastLMouseDown, &WControl::onLUpMouseOut);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onLUpMouseOut();
}
void WMultiControl::onMUpMouseOut()
{
	// Lanzar evento onMUpMouseOut
	const bool lanzado = ::onXUpMouseOut (this->lastMMouseDown, &WControl::onMUpMouseOut);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onMUpMouseOut();
}
void WMultiControl::onRUpMouseOut()
{
	// Lanzar evento onRUpMouseOut
	const bool lanzado = ::onXUpMouseOut (this->lastRMouseDown, &WControl::onRUpMouseOut);

	// Lanzarlo a los hijos
	WMultiControl::WControlMouseable::onRUpMouseOut();
}
