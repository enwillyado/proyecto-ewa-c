/*********************************************************************************************
 * Name			: w_xlienzo.h
 * Description	: Clase para gestionar un lienzo en XML
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_XLIENZO_H_
#define _W_XLIENZO_H_

#include "w_graficos/w_lienzo.h"

#include <string>

// FWD
class Arbol;

class WXLienzo : public WLienzo
{
public:
	// Constructores
	WXLienzo (const size_t ancho = 0, const size_t alto = 0);
	static WXLienzo getWXLienzoFromFile (const std::string & filename);
	static WXLienzo getWXLienzoFromArbol (const Arbol & contenido);

	// Modificadores
	bool leerContenidoFile (const std::string & filename);
	bool leerContenidoArbol (const Arbol & contenido);
};

#endif // _W_XLIENZO_H_
