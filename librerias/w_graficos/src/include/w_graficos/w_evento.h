/*********************************************************************************************
 * Name			: w_evento.h
 * Description	: Evento base para los desencadenadores
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_EVENTO_H_
#define _W_EVENTO_H_

class WControl;
class WApp;

class WEvento
{
public:
	typedef void (*CbEvent) (const WEvento &);

public:
	// Constructor
	WEvento (WControl & iControl);

	WControl & getSelf() const;
	WApp & getRoot() const;

public:
	WControl & control;
};

#endif // _W_EVENTO_H_
