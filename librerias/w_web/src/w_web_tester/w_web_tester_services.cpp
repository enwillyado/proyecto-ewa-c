/*********************************************************************************************
 * Name			: w_web_tester_services.cpp
 * Description	: Implementaci�n del servicios que responder�n a un determinado servicio
 *					que ejecutar� el servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#if 0
#include "w_web/w_web_services_prueba.h"
#include "w_base/toStr.hpp"

/*************************************************************************/
/* LOS SERVICIOS SE IMPLEMENTAN A PARTIR DE AQU�,                        */
/* 		IMPLEMENTANDO LA FUNCI�N MIEMBRO "ejecutarServicio()"            */
/* A d�a de hoy, la forma segura de a�adir al cat�logo es con la funci�n */
/* del cat�logo "addService(...)" que devuelve bool si se registr� bien. */
/*                                                                       */
/* ATENCI�N: por la forma en la que se inicializa el mapa de servicios,  */
/*				se deben realizar los "addService(...)" en los ficheros  */
/*				".cpp" junto al main, aunque la implementaci�n de las    */
/*				funciones miembro s� se pueden implementar aqu� o en     */
/*				otros ficheros ".cpp". Igualmente, se puede a�adir el    */
/*				servicio en cualquier otro lugar a partir del main; es   */
/*				decir, siempre y cuando sea dentro de una funci�n no     */
/*				global. Por supuesto, se puede eliminar del cat�logo     */
/*				cuando venga en gana.                                    */
/**************************************************************************/

// ----------------------------------------------------------------------------
// Servicio para realizar pruebas con servicios extendidos
std::string getMenu()
{
	std::string ret;
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href=''>RELOAD</a></li>";
	ret += "<li><a href='/'>/ ERROR_404</a></li>";
	ret += "<li><a href='/no_existe'>/no_existe</a> ERROR_404</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/ping'>/ping</a> SRVSERVICE_PING_ID</li>";
	ret += "<li><a href='/ping/'>/ping/</a> SRVSERVICE_PING_ID</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/prueba'>/prueba</a> SRVSERVICE_PRUEBA_A_ID</li>";
	ret += "<li><a href='/prueba/'>/prueba/</a> SRVSERVICE_PRUEBA_A_ID</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/prueba/loquesea'>/prueba/loquesea</a> SRVSERVICE_PRUEBA_B_ID</li>";
	ret += "<li><a href='/prueba/loquesea/'>/prueba/loquesea/</a> SRVSERVICE_PRUEBA_B_ID</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/prueba/loquesea/uno'>/prueba/loquesea/uno</a> SRVSERVICE_PRUEBA_C_ID</li>";
	ret += "<li><a href='/prueba/loquesea/uno/'>/prueba/loquesea/uno/</a> SRVSERVICE_PRUEBA_C_ID</li>";
	ret += "<li><a href='/prueba/loquesea/uno/mas'>/prueba/loquesea/uno/mas</a> ERROR_404</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/prueba/loquesea/dos'>/prueba/loquesea/dos</a> SRVSERVICE_PRUEBA_D_ID</li>";
	ret += "<li><a href='/prueba/loquesea/dos/'>/prueba/loquesea/dos/</a> SRVSERVICE_PRUEBA_D_ID</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/prueba/loquesea/dos/algo'>/prueba/loquesea/dos/algo</a> SRVSERVICE_PRUEBA_E_ID</li>";
	ret += "<li><a href='/prueba/loquesea/dos/ALGO/'>/prueba/loquesea/dos/ALGO/</a> SRVSERVICE_PRUEBA_E_ID</li>";
	ret += "<li><a href='/prueba/loquesea/dos/algo/mas'>/prueba/loquesea/dos/algo/mas</a> ERROR_404</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/prueba/loquesea/tres'>/prueba/loquesea/tres</a> ERROR_404</li>";
	ret += "<li><a href='/prueba/loquesea/tres/'>/prueba/loquesea/tres/</a> ERROR_404</li>";
	ret += "<li><a href='/prueba/loquesea/tres/algo'>/prueba/loquesea/tres/algo</a> ERROR_404</li>";
	ret += "<li><a href='/prueba/loquesea/tres/algo/'>/prueba/loquesea/tres/algo/</a> ERROR_404</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/prueba/loquesea/tres/algo/rojo'>/prueba/loquesea/tres/algo/rojo</a> SRVSERVICE_PRUEBA_F_ID</li>";
	ret += "<li><a href='/prueba/loquesea/tres/algo/rojo/'>/prueba/loquesea/tres/algo/rojo/</a> SRVSERVICE_PRUEBA_F_ID</li>";
	ret += "<li><a href='/prueba/loquesea/TRES/algo/azul'>/prueba/loquesea/TRES/algo/azul</a> SRVSERVICE_PRUEBA_G_ID</li>";
	ret += "<li><a href='/prueba/loquesea/tres/algo/azul/'>/prueba/loquesea/tres/algo/azul/</a> SRVSERVICE_PRUEBA_F_ID</li>";
	ret += "<li><a href='/prueba/loquesea/tres/algo/otracosa'>/prueba/loquesea/tres/algo/otracosa</a> ERROR_404</li>";
	ret += "<li><a href='/prueba/loquesea/tres/algo/otracosa/'>/prueba/loquesea/tres/algo/otracosa/</a> ERROR_404</li>";
	ret += "<li><a href='/prueba/loquesea/tres/algo/otracosa/mas'>/prueba/loquesea/tres/algo/otracosa/mas</a> ERROR_404</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/prueba/loquesea/cuatro'>/prueba/loquesea/cuatro</a> ERROR_404</li>";
	ret += "<li><a href='/prueba/loquesea/cuatro/'>/prueba/loquesea/cuatro/</a> ERROR_404</li>";
	ret += "<li><a href='/prueba/loquesea/cuatro/algo'>/prueba/loquesea/cuatro/algo</a> SRVSERVICE_PRUEBA_H_ID</li>";
	ret += "<li><a href='/prueba/loquesea/cuatro/algo/'>/prueba/loquesea/cuatro/algo/</a> SRVSERVICE_PRUEBA_H_ID</li>";
	ret += "</ul>";
	ret += "<ul style='border:1px solid gray; padding:5px 30px;'>";
	ret += "<li><a href='/PruebA/*/cuatro/algo'>/PruebA/*/cuatro/algo</a> SRVSERVICE_PRUEBA_H_ID</li>";
	ret += "<li><a href='/prueba/*/cuatro/algo/'>/prueba/*/cuatro/algo/</a> SRVSERVICE_PRUEBA_H_ID</li>";
	ret += "</ul>";
	return ret;
}
Mensaje BasicSrvService_PruebaA::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_PruebaA('" + peticion.getURI() + "');" + getMenu());
	return respuesta;
}

Mensaje BasicSrvService_PruebaB::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_PruebaB('" + peticion.getURI() + "');" + getMenu());
	return respuesta;
}

Mensaje BasicSrvService_PruebaC::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_PruebaC('" + peticion.getURI() + "');" + getMenu());
	return respuesta;
}

Mensaje BasicSrvService_PruebaD::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_PruebaD('" + peticion.getURI() + "');" + getMenu());
	return respuesta;
}

Mensaje BasicSrvService_PruebaE::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_PruebaE('" + peticion.getURI() + "');" + getMenu());
	return respuesta;
}

Mensaje BasicSrvService_PruebaF::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_PruebaF('" + peticion.getURI() + "');" + getMenu());
	return respuesta;
}

Mensaje BasicSrvService_PruebaG::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_PruebaG('" + peticion.getURI() + "');" + getMenu());
	return respuesta;
}

Mensaje BasicSrvService_PruebaH::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("BasicSrvService_PruebaH('" + peticion.getURI() + "');" + getMenu());
	return respuesta;
}

Mensaje BasicSrvService_IPandPORT::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody (evento.ip + ":" + ::toStr (evento.puerto));
	return respuesta;
}
#endif
