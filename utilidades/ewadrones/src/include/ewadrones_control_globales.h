/*********************************************************************************************
 * Name			: ewadrones_control_globales.h
 * Description	: Variables globales para el control del EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_CONTROL_GLOBALES_H_
#define _EWADRONES_CONTROL_GLOBALES_H_

#include "ewadrones/w_jumping_sumo_controlador.h"
#include "ewadrones/ewadrones.h"
#include "w_graficos/w_app.h"

JumpingSumoControlador & GetJumpingSumo();
EWadrones & GetArDrone();
WApp* & GetAPP();

#endif // _EWADRONES_CONTROL_GLOBALES_H_
