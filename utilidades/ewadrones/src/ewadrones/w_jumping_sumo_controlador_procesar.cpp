/*********************************************************************************************
 * Name			: w_jumping_sumo_controlador_procesar.cpp
 * Description	: Implementaci�n de la liber�a de controlador del AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones/w_jumping_sumo_controlador.h"

#include "ewadrones/ewadrones.h"
#include "ewadrones/ar_drone_puertos.hpp"

#include "ewadrones/w_jumping_sumo_structs.hpp"

#include "w_sistema/w_sistema_time.h"
#include "w_red/w_red_clitcp.h"

#include "w_graficos/w_lienzo_jpeg.h"

#include "w_base/toFile.hpp"

#define DESAFIO "HELLO"

JumpingSumoControlador::JumpingSumoControlador (EWadrones* ptrArDrone)
	: clienteComandos(), arDrone (ptrArDrone), numeroSecuenciaSyn (0), numeroSecuenciaAck (0),
	  cbOnNewImageJPEG (NULL), cbOnNewLienzo (NULL), cbOnBateriaCambia (NULL)
{
}

void JumpingSumoControlador::setPtrArDrone (EWadrones* ptrArDrone)
{
	this->arDrone = ptrArDrone;
}

void JumpingSumoControlador::init()
{
	//if (this->arDrone != NULL)
	{
		// TODO: utilizar el host
		//const std::string & host = arDrone->getHost();
		{
			// Solicitar el alta en la conexi�n en el by-pass
			const bool conectado = this->clienteComandos.openSocket ("willypcp", 55555);
			const bool enviado = this->clienteComandos.enviar (DESAFIO);

			// Obtener mensaje de bienvenida
			std::string mensaje;
			const bool recibir = this->clienteComandos.recibir (mensaje, 5);	//< TODO: procesar TIME-OUT

			// Procesar mensaje de bienvenida
			const bool correcto = (mensaje == DESAFIO);

			// Ponerse a la escucha de nuevos mensajes
			if (true == correcto)
			{
#ifdef INICIO_AUTOMATICO
				this->start();
#endif
			}
		}
		{
			// Solicitar al conector el inicio de la transmisi�n
			ClienteTCP conector;
			const bool conectado = conector.openSocket ("willypcp", 44444, 5);	//< TODO: procesar TIME-OUT
			const bool enviado = conector.enviar ("{\"controller_type\":\"PC\", \"controller_name\":\"eWa\", \"d2c_port\":\"54321\"}");

			// Obtener mensaje de bienvenida
			std::string mensaje;
			const bool recibir = conector.recibir (mensaje, 5);	//< TODO: procesar TIME-OUT

			// Procesar mensaje de bienvenida
			// TODO: conectar indicarlo al by-pass
			int j = 0;
		}
	}
}

// ----------------------------------------------------------------------------
void JumpingSumoControlador::start()
{
	this->hiloComandos.setData (TWThreadData<JumpingSumoControlador*> (this));
	this->hiloComandos.setFunction (&JumpingSumoControlador::initControlUDP);
	this->hiloComandos.start();

	{
		// Activar streaming de v�deo
		struct ioctl_packet videoSend (1, sizeof (ioctl_packet), 4, 0, 0);
		const char* const accion = (const char* const)&videoSend;
		std::string mensaje;
		mensaje.assign (accion, sizeof (videoSend));
		this->enviarMensaje (mensaje);
	}

	this->hiloEmisor.setData (TWThreadData<JumpingSumoControlador*> (this));
	this->hiloEmisor.setFunction (&JumpingSumoControlador::initEmisor);
	this->hiloEmisor.start();
}
void JumpingSumoControlador::stop()
{
	this->hiloComandos.stop();
}

void JumpingSumoControlador::initEmisor (const WThread & hilo)
{
	JumpingSumoControlador* data = hilo.getData<TWThreadData<JumpingSumoControlador*>>().data;
	while (true)
	{
		const struct move accion (++data->numeroSecuenciaSyn,
								  (data->acelerador == 0 && data->volante == 0) ? 0 : 1,
								  (uint8_t)data->acelerador, (uint8_t)data->volante);
		const char* const accionChars = (const char* const)&accion;
		std::string mensaje;
		mensaje.assign (accionChars, sizeof (accion));
		data->enviarMensaje (mensaje);

		WTime::esperarMilisegundos (33);
	}
}
void JumpingSumoControlador::initControlUDP (const WThread & hilo)
{
	JumpingSumoControlador* data = hilo.getData<TWThreadData<JumpingSumoControlador*>>().data;
	ClienteUDP & control = data->clienteComandos;
	while (control.isOpenSocket())
	{
		std::string mensaje;
		const bool recibir = control.recibir (mensaje);
		data->procesarMensaje (mensaje);
	}
}
void JumpingSumoControlador::procesarMensaje (const std::string & mensaje)
{
	const char* const mensajeChar = mensaje.c_str();
	const header* const mensajeHeader = (const header * const)mensajeChar;
#ifdef _DEBUG
	printf ("IN: head %d: size: %5d: ext: %3d, seqno: %3d, unk: %3d\n", mensajeHeader->type, mensajeHeader->size, mensajeHeader->ext, mensajeHeader->seqno,
			mensajeHeader->unk);
#endif
	switch (mensajeHeader->type)
	{
	case SYNC:
	{
		// Procesar sincronizaci�n
		const sync* const mensajeSync = (const sync * const)mensajeChar;
		switch (mensajeHeader->ext)
		{
		case 0:
		{
			// PING... PONG!
			struct sync headerSend = *mensajeSync;
			headerSend.head.ext = 1;
			const char* const accion = (const char* const)&headerSend;
			std::string mensajeEnviar;
			mensajeEnviar.assign (accion, sizeof (headerSend));
			this->enviarMensaje (mensajeEnviar);
			int i = 0;
			break;
		}
		case 1:
		case 127:
		{
			// Ignorar estos mensajes
			// TODO: �el 127 tambi�n?
			break;
		}
		}
		break;
	}

	case ACK:
	{
		// TODO: procesar asentimientos
#ifdef _DEBUG
		printf ("+ ACK for IN-IOCTL seqno: %d\n", * (mensajeChar + sizeof (*mensajeHeader)));
#endif
		int j = 0;
		break;
	}

	case IMAGE:
		// Procesar im�genes
	{
		const struct image* io = (const struct image*) (mensajeChar);
#ifdef _DEBUG
		printf ("!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		printf ("!!!!!!!!!! IMAGE !!!!!!!!!!");
		printf ("!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		printf ("IMAGE | size: %d, fno: %d\n", io->head.size, io->frame_number);
#endif
		const std::string & jpeg = io->getJpegAsString();
#ifdef GUARDAR_EN_FICHERO
		::ponerContenidoFile ("hola.jpeg", jpeg);
#else
		// Actualizar el fotograma recibido y enviar eventos
		this->onNewImageJPEG (jpeg);
		this->lienzo = WLienzoJPEG::getWLienzoJPEGFromStr (jpeg);
		this->onNewLienzo();
#endif
		break;
	}

	case IOCTL:
	{
		// TODO: Procesar telemetr�a
		const struct ioctl_packet* io = (const struct ioctl_packet*) (mensajeChar);
		const char* payload = mensajeChar + sizeof (*io);
#ifdef _DEBUG
		printf ("+ ioctl: flags %02x, type: %d, func: %d; unk: %d\n", io->flags, io->type, io->func, io->unk);
#endif
		switch (io->type)
		{
		case 5: /* confirm */
			switch (io->func)
			{
			case 1:
			{
				const unsigned char nivelBateria = *payload;
#ifdef _DEBUG
				printf ("->+ battery level: %d\n", nivelBateria);
#endif
				this->onBateriaCambia (nivelBateria);
			}
			break;
			}
		}
		// Y posteriormente... mandar el asentimiento
		{
			// ACK
			struct ack ackSend (io->head.ext | 0x80, ++this->numeroSecuenciaAck, io->head.seqno);
			const char* const accion = (const char* const)&ackSend;
			std::string mensajeEnviar;
			mensajeEnviar.assign (accion, sizeof (ackSend));
			this->enviarMensaje (mensajeEnviar);
		}
		break;
	}

	default:
		// TODO: �hay m�s tipos?
		int i = 0;
		break;
	}
}

// ----------------------------------------------------------------------------
void JumpingSumoControlador::enviarMensaje (const std::string & mensaje)
{
	this->clienteComandos.enviar (mensaje);
}
