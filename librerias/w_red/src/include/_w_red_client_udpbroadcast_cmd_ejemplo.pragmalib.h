/*********************************************************************************************
 *	Name		: _w_red_client_udpbroadcast_cmd_ejemplo.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SERVER_UDPBROADCAST_CMD_EJEMPLO_PRAGMALIB_H_
#define _W_RED_SERVER_UDPBROADCAST_CMD_EJEMPLO_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_red/_w_red_client.pragmalib.h"

#endif

#endif // _W_RED_SERVER_UDPBROADCAST_CMD_EJEMPLO_PRAGMALIB_H_
