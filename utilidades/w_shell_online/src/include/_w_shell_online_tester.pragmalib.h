/*********************************************************************************************
 *	Name		: _w_shell_online_tester.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SHELL_ONLINE_TESTER_PRAGMALIB_H_
#define _W_SHELL_ONLINE_TESTER_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

// Parte privada
#include "w_shell_online/_w_shell_online.pragmalib.h"

#endif

#undef _W_SHELL_ONLINE_TESTER_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_SHELL_ONLINE_TESTER_PRAGMALIB_H_
