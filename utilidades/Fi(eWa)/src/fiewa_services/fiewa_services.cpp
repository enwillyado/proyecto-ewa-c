/*********************************************************************************************
 *	Name		: fiewa_services.cpp
 *	Description	: Implementación de los diferentes servicios del Servidor de Pares
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "fiewa/fiewa_services.h"

#include <algorithm>
#include <iostream>

Mensaje FIEWA::BasicSrvService_IPandPORT::ejecutarServicio (const Evento & evento,
		const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);

	// Crear la estructura con los Datos que ha obtenido el STUN
	const FIEWA::DatosSTUN datosSTUN (evento);

	// Preparar la respuesta
	const std::string & cuerpo = datosSTUN.toString();
	respuesta.setBody (cuerpo);

	// Incluir el nuevo cliente en el listado
	FIEWA::BasicSrvService_IPofPEER::peers[datosSTUN.publicID] = datosSTUN;

	// Logger
	std::cout << "+  NUEVO: " << cuerpo << std::endl;
	std::cout << "+  SIZE: " << FIEWA::BasicSrvService_IPofPEER::peers.size() << std::endl;

	// Devolver la respuesta
	return respuesta;
}

FIEWA::BasicSrvService_IPofPEER::Peers FIEWA::BasicSrvService_IPofPEER::peers;
Mensaje FIEWA::BasicSrvService_IPofPEER::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);

	// Crear la estructura con los Datos que ha obtenido el STUN
	const FIEWA::DatosSTUN datosSTUN (evento);

	// Obtener la información
	if (peticion.existeRequestVar ("to") == true)
	{
		// Si se indica "to", se intenta dar el solicitado
		const std::string & to = peticion.getRequestVar ("to");
		FIEWA::BasicSrvService_IPofPEER::Peers::const_iterator itr =
			FIEWA::BasicSrvService_IPofPEER::peers.find (to);
		if (itr != FIEWA::BasicSrvService_IPofPEER::peers.end())
		{
			// Se da el encontrado
			respuesta.setBody (itr->second.toString());
		}
		else
		{
			// No se encontró
			respuesta.setBody ("");
		}
	}
	else
	{
		// Si no...
		std::string id = "";
		if (peticion.existeRequestVar ("id") == true)
		{
			// Si se indica el "id", se da uno que no sea el indicado
			id = peticion.getRequestVar ("id");
		}
		else
		{
			// Si no se indica el "id", se da uno que no sea uno mismo
			id = datosSTUN.publicID;
		}

		// Buscar uno que NO sea el indicado
		bool encontrado = false;
		for (FIEWA::BasicSrvService_IPofPEER::Peers::const_iterator itr =
					FIEWA::BasicSrvService_IPofPEER::peers.begin(); itr != FIEWA::BasicSrvService_IPofPEER::peers.end();
				itr++)
		{
			if (itr->first != id)
			{
				respuesta.setBody (itr->second.toString());
				encontrado = true;
				break;
			}
		}
		if (encontrado == false)
		{
			respuesta.setBody ("");
		}
	}

	// Devolver la respuesta
	return respuesta;
}


FIEWA::BasicSrvService_LoginPEER::Cuentas FIEWA::BasicSrvService_LoginPEER::cuentas;
Mensaje FIEWA::BasicSrvService_LoginPEER::ejecutarServicio (const Evento & evento, const Mensaje & peticion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::EWA1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);

	// Crear la estructura con los Datos que ha obtenido el STUN
	FIEWA::DatosSTUN datosSTUN (evento);

	// Obtener la información
	if (peticion.existeRequestVar ("my") == true)
	{
		// Si se indica "my", se actualiza el actual
		const std::string & my = peticion.getRequestVar ("my");
		FIEWA::BasicSrvService_LoginPEER::cuentas[my] = datosSTUN;
	}
	else if (peticion.existeRequestVar ("id") == true)
	{
		// Si se indica "id", se intenta dar el solicitado
		const std::string & id = peticion.getRequestVar ("id");
		FIEWA::BasicSrvService_LoginPEER::Cuentas::const_iterator itr =
			FIEWA::BasicSrvService_LoginPEER::cuentas.find (id);
		if (itr != FIEWA::BasicSrvService_LoginPEER::cuentas.end())
		{
			datosSTUN = itr->second;
		}
	}

	// Preparar la respuesta
	const std::string & cuerpo = datosSTUN.toString();
	respuesta.setBody (cuerpo);

	// Devolver la respuesta
	return respuesta;
}
