/*********************************************************************************************
 *	Name		: _w_sistema_time.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_TIME_PRAGMALIB_H_
#define _W_SISTEMA_TIME_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_base/_pragmalib_base.h"

// Propia librer�a: (para no autoengancharla si es una dll)
#pragma comment(lib, "w_sistema_time" FIN_LIB)

// Dependencias
#include "w_sistema/_w_sistema_thread.pragmalib.h"

// Librer�as externas
#ifdef WIN32
#else
#pragma comment(lib, "rt")		//< REAL TIME library
#endif


#endif

#undef _W_SISTEMA_TIME_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_SISTEMA_TIME_PRAGMALIB_H_
