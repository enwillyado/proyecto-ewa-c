/*********************************************************************************************
 *	Name		: w_web_clitcp.cpp
 *	Description	: Implementaci�n del Cliente Web TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_web/w_web_clitcp.h"

// --------------------------------------------------------------------------
// Implementaci�n del Servidor TCP

ClienteWebTCP::ClienteWebTCP()
	: ClienteTCP()
{
}

//---------------------------------------------------------------------------
// Env�o a alto nivel de los mensajes:

/**
 *	Env�a un Mensaje Web y espera a recibir una respuesta como Mensaje Web completa
 *	\param [in]	peticion		Mensaje Web que se enviar�
 *	\param [in]	respuesta		Mensaje Web que se ha recibido
 *	\retval	TRUE si se ha podido enviar y recibir los mensajes
 *		Con "GetError()" se obtiene el error que se produjo
**/
bool ClienteWebTCP::enviar_recibir (const Mensaje & peticion, Mensaje & respuesta, unsigned int segundosEsperados)
{
	// Enviar el mensaje generado:
	if (this->enviar (peticion) == false)
	{
		return false;	// error (ya deja puesto "ERROR_AL_CONNECT_SCKT", etc.)
	}
	// Recibir el mensaje esperado:
	if (this->recibir (respuesta, segundosEsperados) == false)
	{
		return false;
	}
	return true;
}

//---------------------------------------------------------------------------
// Env�o a bajo nivel de mensajes

/**
 *	Env�a un Mensaje Web
 *	\param [in]	peticion		Mensaje Web que se enviar�
 *	\retval	TRUE si se ha podido enviar el Mensaje Web
 *		Con "GetError()" se obtiene el error que se produjo
**/
bool ClienteWebTCP::enviar (const Mensaje & mensajeEnviar)
{
	// Enviar el mensaje generado:
	if (this->ClienteTCP::enviar (mensajeEnviar.genMsg()) == false)
	{
		return false;	// error (ya deja puesto "ERROR_AL_CONNECT_SCKT", etc.)
	}
	return true;
}

bool ClienteWebTCP::recibir (Mensaje & respuesta, unsigned int segundosEsperados)
{
	// Recibir una respuesta:
	do
	{
		std::string mensajeRecibido;
		if (this->ClienteTCP::recibir (mensajeRecibido, segundosEsperados) == false)
		{
			return false; // error (ya deja puesto "SOCKET_ERROR_FATAL", etc.)
		}

		// Procesar el mensaje recibido
		respuesta.clearParser();					// NOTA: es preferible hacerlo as� porque no se puede soportar parsear parcialmente un mensaje si �ste no indica el ENCABEZADOID_CONTENT_LENGTH,
		respuesta.parseFromMsg (
			mensajeRecibido);	//			aunque si se ha indicado en la anterior vuelta, se podr�a hacer un "if" evitando el parseo constante desde el inicio del mensaje recibido
		if (respuesta.getLastError() != Mensaje::MENSAJE_CORRECTO)
		{
			if (respuesta.getLastError() == Mensaje::MENSAJE_MAL_FORMADO)
			{
				this->ClienteTCP::lastError = ClienteTCP::MENSAJE_MAL_FORMADO;
				return false;
			}
			// continuar en el do-while
		}
		else
		{
			// la respuesta est� completa!
			break;	// salir del do-while
		}
	}
	while (true);

	// La respuesta en este punto est� completa y se procesar�
	return true;
}
