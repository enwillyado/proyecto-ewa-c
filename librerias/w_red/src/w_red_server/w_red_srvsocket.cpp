/*********************************************************************************************
 *	Name		: _w_red_srvsocket.cpp
 *	Description	: Implementaci�n de un socket de Servidor
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/

#include "w_red/w_red_srvsocket.h"
#include "w_red/w_red_srvevent.h"		//< para tener la declaraci�n de un "EventoServidorTCP" y "EventoServidorUDP"
#include "w_red/w_red_srvtcp.h"			//< para tener la declaraci�n de un "ServidorTCP"
#include "w_red/w_red_srvudp.h"			//< para tener la declaraci�n de un "ServidorUPD"
#include "w_red/w_red_srvudp_private.h"	//< para tener la declaraci�n de un "IdentificadorUDP"

#include "w_base/toStr.hpp"				//< para tener el toStr

#include <time.h>						//< para el time(...)

#ifdef WIN32
#include <ws2tcpip.h>
#pragma comment (lib, "ws2_32.lib")
#else
#include <errno.h>						//< para el "errno"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>						//< para "close(...)"
#include <netdb.h>						//< para "getaddrinfo(...)"
#include <string.h>						//< para "memset(...)"
#endif

ServerSocket::ServerSocket()
{
	this->ptrServidorTCP = NULL;
	this->ptrServidorUDP = NULL;
	iniciado = false;
	this->masterptr = NULL;
}
ServerSocket::ServerSocket (ServidorTCP* ptr)
{
	this->setDefaults (ptr);
}
ServerSocket::ServerSocket (ServidorUDP* ptr)
{
	this->setDefaults (ptr);
}

/**
 *	Inicializa los valores del "aceptador" dados
 *	\param	[in]	ptr		Puntero al ServidorTCP que aceptar� peticiones
**/
void ServerSocket::setDefaults (ServidorTCP* ptr)
{
	if (NULL != this->ptrServidorTCP)
	{
		// TODO: cerrar todos los sockets que tengan ISSET en el master?
	}
	this->ptrServidorTCP = ptr;
	this->ptrServidorUDP = NULL;
	iniciado = (ptr != NULL);
	this->masterptr = NULL;
}

/**
 *	Inicializa los valores del "aceptador" dados
 *	\param	[in]	ptr		Puntero al ServidorUDP que aceptar� peticiones
**/
void ServerSocket::setDefaults (ServidorUDP* ptr)
{
	if (NULL != this->ptrServidorTCP)
	{
		// TODO: cerrar todos los sockets que tengan ISSET en el master?
	}
	this->ptrServidorTCP = NULL;
	this->ptrServidorUDP = ptr;
	iniciado = (ptr != NULL);
	this->masterptr = NULL;
}

// ----------------------------------------------------------------------------
// Funciones auxiliares para el control de sockets:
static void* get_in_addr (struct sockaddr* sa)
{
	// get sockaddr, IPv4 or IPv6:
	if (sa->sa_family == AF_INET)
	{
		return & (((struct sockaddr_in*)sa)->sin_addr);
	}
	return & (((struct sockaddr_in6*)sa)->sin6_addr);
}

static std::string getIPstr (struct sockaddr* their_addr)
{
	std::string ipv4;
	if (NULL != their_addr)
	{
		ipv4 += ::toStr ((unsigned short) (unsigned char) (their_addr->sa_data[2])) + ".";
		ipv4 += ::toStr ((unsigned short) (unsigned char) (their_addr->sa_data[3])) + ".";
		ipv4 += ::toStr ((unsigned short) (unsigned char) (their_addr->sa_data[4])) + ".";
		ipv4 += ::toStr ((unsigned short) (unsigned char) (their_addr->sa_data[5]));
	}
	return ipv4;
}

/**
 *	M�todo que procesa las peticiones, acept�ndolas, y avisando al servidor asociado para que las
 *		procese; y volviendo a empezar hasta que se cierra la conexi�n, o se desvincula este socket
 *		con el servidor asociado
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740668(v=vs.85).aspx
 **/
bool ServerSocket::procesarPeticiones()
{
	fd_set master;
	masterptr = &master;
	fd_set read_fds;					// temp file descriptor list for select()
	int fdmax;							// maximum file descriptor number
	int sockfd = this->getSockFD();		// socket descriptor
	if (sockfd == DESCRIPTOR_NULO)
	{
		return false;
	}

	FD_ZERO (&master);					// clear the master and temp sets
	FD_ZERO (&read_fds);
	FD_SET (sockfd, &master);			// insert in master the sockfd

	// keep track of the biggest file descriptor
	fdmax = sockfd;						// so far, it's this one

	// Crear la instancia 'evento' que se usar� recurrentemente
	Evento* evento;
	if (this->ptrServidorTCP != NULL)
	{
		evento = new EventoTCP();
	}
	else if (this->ptrServidorUDP != NULL)
	{
		evento = new EventoUDP();
	}

	// main loop
	//DCOUT_MESSAGE (LANG_ESPERAR_PETICIONES);
	while ((ptrServidorTCP != NULL || ptrServidorUDP != NULL) && iniciado)
	{
		read_fds = master; // copy it
		//DCOUT_MESSAGE (LANG_ESPERANDO_PETICIONES);
		if (this->ptrServidorTCP != NULL)
		{
			ptrServidorTCP->setEscuchando();
		}
		else if (this->ptrServidorUDP != NULL)
		{
			ptrServidorUDP->setEscuchando();
		}
		if (::select (fdmax + 1, &read_fds, NULL, NULL, NULL) == -1)		// OJO: est� sin timeout!!!
		{
			// si falla el "select"
#ifdef WIN32
			int i = WSAGetLastError();	// error: ver SEE para conocer los errores posibles
			switch (i)
			{
			case WSAEINVAL:			// 10022: Invalid argument
			case WSAENOTSOCK:		// 10038: Socket operation on nonsocket: "fd_set was not valid"
				break;
			}
#endif
			// Crear un evento correspondiente al error
			if (sockfd == DESCRIPTOR_NULO)
			{
				evento->tipo = Evento::SERVIDOR_DESCONECTADO;
				//DCOUT_MESSAGE (LANG_ERROR_SELECT);
				this->iniciado = false;
			}
			else
			{
				evento->tipo = Evento::ERROR_AL_SELECCIONAR;
				//DCOUT_MESSAGE (LANG_ERROR_SELECT);
			}
			time (&evento->fechallegada);
			// Y mand�rselo al servidor para que lo procese
			if (this->ptrServidorTCP != NULL)
			{
				ptrServidorTCP->analizaEvento (* (const EventoServidorTCP*)evento);
			}
			else if (this->ptrServidorUDP != NULL)
			{
				ptrServidorUDP->analizaEvento (* (const EventoServidorUDP*)evento);
			}
			break; // salir del for, e ir al "main loop"
		}
		if (this->ptrServidorTCP != NULL)
		{
			ptrServidorTCP->setProcesando();
		}
		else if (this->ptrServidorUDP != NULL)
		{
			ptrServidorUDP->setProcesando();
		}
		//DCOUT_MESSAGE (LANG_SE_RECIBIO_PETICION);
		// run through the existing connections looking for data to read
#ifdef WIN32
		// En WINDOWS, se puede optimizar esta b�squeda haci�ndola lineal
		for (u_int i = 0; i < read_fds.fd_count; i++)
		{
			int itr_fd = read_fds.fd_array[i];
#else
		// En UNIX, no =(
		for (int itr_fd = 0; itr_fd <= fdmax; itr_fd++)		// HACK: hacer que "itr_fd" sea un iterador lineal, y no natural
		{
#endif
			if (FD_ISSET (itr_fd, &read_fds)) // we got one!!
			{
				if (itr_fd == sockfd && NULL != this->ptrServidorTCP)
				{
					// handle new TCP connection
					struct sockaddr_storage their_addr;
					socklen_t addr_size = sizeof their_addr;
					int new_fd = ::accept (sockfd, (struct sockaddr*)&their_addr, &addr_size);
					if (new_fd == -1)
					{
						// Crear un evento correspondiente al error
						//DCOUT_MESSAGE (LANG_ERROR_ACEPTAR);
						evento->tipo = Evento::ERROR_AL_ACEPTAR;
					}
					else
					{
						//DCOUT_MESSAGE (LANG_SE_ACEPTO_CONEXION);
						evento->tipo = Evento::CONEXION_ACEPTADA;
						FD_SET (new_fd, &master); // add to master set
						if (new_fd > fdmax)  // keep track of the max
						{
							fdmax = new_fd;
						}
					}
					// completar el evento
					time (&evento->fechallegada);
					{
						evento->ip = getIPstr ((struct sockaddr*)&their_addr);
						evento->puerto = ntohs (((struct sockaddr_in*)&their_addr)->sin_port);
						evento->socketID = new_fd;
					}
					// Y mand�rselo al servidor para que lo procese
					if (this->ptrServidorTCP != NULL)
					{
						ptrServidorTCP->analizaEvento (* (const EventoServidorTCP*)evento);
					}
				} // __if (i == ptrServidorTCP->sockfd)
				else
				{
					// handle data from a client
					const int bytes_rec = this->recibirDelCliente (itr_fd, evento);

					// Procesar la respuesta recibida
					if (bytes_rec > 0)
					{
						//DCOUT_MESSAGE (LANG_SE_RECIBIO_MENSAJE);
						// Crear un evento correspondiente al mensaje recibido
						evento->tipo = Evento::MENSAJE_RECIBIDO;
						// Mostrar algunos mensajes
						//DCOUT_MESSAGE (" + IP ORIGEN   : " + evento->ip);
						//DCOUT_MESSAGE (" + PUERTO ORIGEN : " + Protocolo::toStr (evento->puerto));
						//DCOUT_MESSAGE (" + MENSAJE: ");
						//DCOUT_MESSAGE ("------------------------<[");
						//DCOUT_MESSAGE (evento->mensaje);
						//DCOUT_MESSAGE ("]>------------------------");
						// Y mand�rselo al servidor para que lo procese
						if (this->ptrServidorTCP != NULL)
						{
							ptrServidorTCP->analizaEvento (* (const EventoServidorTCP*)evento);
						}
						else if (this->ptrServidorUDP != NULL)
						{
							ptrServidorUDP->analizaEvento (* (const EventoServidorUDP*)evento);
						}
						continue;
					}
					if (this->iniciado == false)
					{
						//DCOUT_MESSAGE (LANG_ERROR_RECIBIR_DESC);
						// Crear un evento correspondiente a lo ocurrido
						evento->tipo = Evento::SERVIDOR_DESCONECTADO;
						// Y mand�rselo al servidor para que lo procese
						if (this->ptrServidorTCP != NULL)
						{
							ptrServidorTCP->analizaEvento (* (const EventoServidorTCP*)evento);
						}
						else if (this->ptrServidorUDP != NULL)
						{
							ptrServidorUDP->analizaEvento (* (const EventoServidorUDP*)evento);
						}
						this->iniciado = false;

						break; // salir del for, e ir al "main loop"
					}
					else
					{
						if (bytes_rec == 0)
						{
							//DCOUT_MESSAGE (LANG_SE_CIERRA_CONEXION);
							// Crear un evento correspondiente a lo ocurrido
							evento->tipo = Evento::CONEXION_DESCONECTADA;
						}
						else /*if(bytes_rec < 0)*/
						{
							//DCOUT_MESSAGE (LANG_ERROR_RECIBIR);
							const ServerSocket::Errores error = this->obtainSysLastErrorSocket();
							// Crear un evento correspondiente a lo ocurrido
							switch (error)
							{
							case ServerSocket::ERROR_DESCONOCIDO :
								evento->tipo = Evento::CONEXION_PERDIDA;
								break;
							case ServerSocket::ERROR_CLIENTE_DESCONECTADO :
								evento->tipo = Evento::CONEXION_PERDIDA;
								break;
							case ServerSocket::ERROR_AL_RECIBIR_MENSAJE :
								evento->tipo = Evento::ERROR_AL_RECIBIR;
								break;
							default :
								// TODO: habr�a que afinar m�s este caso si se da
								evento->tipo = Evento::CONEXION_PERDIDA;	//< NOTA: pero mientras tanto, siempre se finaliza la conexi�n
								break;
							}
						}
						if (itr_fd != sockfd)
						{
							// Si es un socket hijo, lo indicamos en el evento
							evento->socketID = itr_fd;

							// Y mand�rselo al servidor para que lo procese
							if (this->ptrServidorTCP != NULL)
							{
								ptrServidorTCP->analizaEvento (* (const EventoServidorTCP*)evento);
							}
							else if (this->ptrServidorUDP != NULL)
							{
								ptrServidorUDP->analizaEvento (* (const EventoServidorUDP*)evento);
							}
						}
					}
				} // __if__else (i == ptrServidorTCP->sockfd)
			}
			else
			{
				// este descriptor de socket no est� abierto
			}
		}
	}
	//DCOUT_MESSAGE (LANG_SE_DEJAN_ESPERAR_PETICIONES);
	if (this->ptrServidorTCP != NULL)
	{
		ptrServidorTCP->setDesconectado();
	}
	else if (this->ptrServidorUDP != NULL)
	{
		ptrServidorUDP->setDesconectado();
	}

	// Destruir los eventos
	if (this->ptrServidorTCP != NULL)
	{
		delete (EventoTCP*)evento;
	}
	else if (this->ptrServidorUDP != NULL)
	{
		delete (EventoUDP*)evento;
	}

	this->masterptr = NULL;
	return true; // todo bien llegados a este punto
}

int ServerSocket::recibirDelCliente (const int socketID, Evento* evento)
{
	// handle data from a client
	char msg [RECV_MENSAJE_MAX_SIZE];
	int len = RECV_MENSAJE_MAX_SIZE, bytes_rec;
	if (NULL != this->ptrServidorTCP)
	{
		// data of a client
		struct sockaddr_storage their_addr;
		socklen_t addr_size = sizeof their_addr;

		// Recibir
		bytes_rec = ::recv (socketID, msg, len, 0);

		// Obtener los datos de la conexi�n
		const int retName = ::getpeername (socketID, (struct sockaddr*)&their_addr, &addr_size);
		if (retName == 0)
		{
			// Rellenar el evento
			evento->ip = getIPstr ((struct sockaddr*)&their_addr);
			evento->puerto = ntohs (((struct sockaddr_in*)&their_addr)->sin_port);
		}
	}
	else if (NULL != this->ptrServidorUDP)
	{
		// data of a client
		struct sockaddr_storage their_addr;
		socklen_t addr_size = sizeof their_addr;

		// Recibir y obtener los datos de la conexi�n
		bytes_rec = ::recvfrom (socketID, msg, len, 0, (struct sockaddr*)&their_addr, &addr_size);

		// Rellenar el evento
		evento->ip = getIPstr ((struct sockaddr*)&their_addr);
		evento->puerto = ntohs (((struct sockaddr_in*)&their_addr)->sin_port);

		// NOTA: �esto hasta qu� punto es necesario una vez obtenida la IP y el puerto?
		IdentificadorUDP* idUDP = new IdentificadorUDP();
		idUDP->their_addr = their_addr;
		idUDP->addr_size = addr_size;
		((EventoUDP*)evento)->idUDP = idUDP;
	}

	// Si se ha recibido algo, actualizar el cuerpo del mensaje recibido
	if (bytes_rec > 0)
	{
		evento->mensaje = std::string (msg, bytes_rec);	// acortar el mensaje recibido a la longitud que tiene
	}

	// Indicar en el evento:
	evento->socketID = socketID;	//< el descriptor del socket
	time (&evento->fechallegada);	//< la hora actual

	// Devolver el n�mero de bytes recibidos (o c�digo de error)
	return bytes_rec;
}

/**
 *	Abre un socket por el puerto dado
 *	\param	[in]	port	puerto (como n�mero) donde se va a poner a escuchar
 *	\retval	TRUE si se abri� correctamente y FALSE si falla
 *		En "lastError" est� el error que se produjo
 **/
bool ServerSocket::openSocket (unsigned int port)
{
	// Convertir el PUERTO a un string:
	const std::string & portString = ::toStr (port);
	if (portString == "")
	{
		this->lastError = ServerSocket::ERROR_AL_TRANSFORMAR_PUERTO;
		return false; // problema al obtener el puerto
	}

	// Abrir el puerto
	return this->openSocket (portString);
}
/**
 *	Abre un socket por el puerto dado
 *	\param	[in]	port	puerto (como string) donde se va a poner a escuchar
 *	\retval	TRUE si se abri� correctamente y FALSE si falla
 *		En "lastError" est� el error que se produjo
 *	\see http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html#windows
 **/
bool ServerSocket::openSocket (const std::string & port)
{
	const char* portString = port.c_str();
#ifdef WIN32
	WSADATA wsaData;  // if this doesn't work
	//WSAData wsaData; // then try this instead
	// MAKEWORD(1,1) for Winsock 1.1, MAKEWORD(2,0) for Winsock 2.0:
	if (::WSAStartup (MAKEWORD (2, 0), &wsaData) != 0)
	{
		return false;
	}
#endif
	struct addrinfo hints;
	struct addrinfo* res;
	::memset (&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;			// use only IPv4
	if (this->ptrServidorTCP != NULL)
	{
		hints.ai_socktype = SOCK_STREAM;	// TCP socket
	}
	else if (this->ptrServidorUDP != NULL)
	{
		hints.ai_socktype = SOCK_DGRAM;	// UDP socket
	}
	hints.ai_flags = AI_PASSIVE;		// fill in my IP for me

	// get info:
	if (::getaddrinfo (NULL, portString, &hints, &res) != 0)
	{
		this->lastError = ServerSocket::ERROR_AL_OBTENER_INFO;
		return false; // problema al obtener la informaci�n local
	}

	// make a socket:
	int sockfd;
	sockfd = ::socket (res->ai_family, res->ai_socktype, res->ai_protocol);
	if (sockfd < 0)
	{
		this->lastError = ServerSocket::ERROR_AL_CREAR_SCKT;
		return false; // problema al crear el socket
	}

	// bind it to the port we passed in to getaddrinfo():
	if (::bind (sockfd, res->ai_addr, res->ai_addrlen) < 0)
	{
		this->lastError = ServerSocket::ERROR_AL_RESERVAR_SCKTR;
		return false; // problema al bloquear el puerto
	}

	if (this->ptrServidorTCP != NULL)
	{
		// set up to be a server (listening) socket: (only TCP)
		if (::listen (sockfd, NUMERO_MAXIMO_CLIENTES) == -1)
		{
			this->lastError = ServerSocket::ERROR_AL_ESCUCHAR_SCKT;
			return false;	// problema al abrir el socket
		}
	}

	// Actualizar el descriptor correspondiente
	if (this->ptrServidorTCP != NULL)
	{
		this->ptrServidorTCP->sockfd = sockfd;
		this->ptrServidorTCP->setConectado();	// decir al manejador que est� conectado
	}
	else if (this->ptrServidorUDP != NULL)
	{
		this->ptrServidorUDP->sockfd = sockfd;
		this->ptrServidorUDP->setConectado();	// decir al manejador que est� conectado
	}

	// Mostrar aviso a navegantes
	//DCOUT_MESSAGE (LANG_ESCUCHAR_PUERTO (portString, Protocolo::toStr (this->getRealPort())));
	this->lastError = ServerSocket::SIN_ERROR;

	return true;	// se abri� correctamente
}

/**
 *	Obtiene el descriptor del socket que est� asignado al servidor
 *	\retval	el descriptor del socket que est� asignado al servidor
 **/
int ServerSocket::getSockFD() const
{
	if (this->ptrServidorTCP != NULL)
	{
		return this->ptrServidorTCP->sockfd;
	}
	else if (this->ptrServidorUDP != NULL)
	{
		return this->ptrServidorUDP->sockfd;
	}
	return 0;
}

/**
 *	Obtiene el puerto por el que est� escuchado el servidor
 *	\retval	el puerto por el que est� escuchado el servidor
 **/
unsigned int ServerSocket::getRealPort() const
{
	struct sockaddr_in name;
	socklen_t namelen = sizeof (name);
	int ret = ::getsockname (this->getSockFD(), (struct sockaddr*)&name, &namelen);
	if (ret >= 0)
	{
		return ntohs (name.sin_port);
	}
	return 0;
}

/**
 *	Obtiene un listado de las direcciones IPv4 de este equipo
 *	\retval	el listado de las direcciones IPv4 (como string) de este equipo
 **/
strvector ServerSocket::getIPsIPv4()
{
	strvector ret;
#ifdef WIN32
	WSADATA wsaData;  // if this doesn't work
	//WSAData wsaData; // then try this instead
	// MAKEWORD(1,1) for Winsock 1.1, MAKEWORD(2,0) for Winsock 2.0:
	if (WSAStartup (MAKEWORD (2, 0), &wsaData) != 0)
	{
		return ret;
	}
#endif
	struct addrinfo hints;
	struct addrinfo* servinfo;
	memset (&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;			// use IPv4
	hints.ai_socktype = SOCK_STREAM;	// TCP socket, indiferente => podr�a ser "SOCK_DGRAM" tambi�n
	hints.ai_flags = AI_PASSIVE;		// fill in my IP for me

	// Obtener la informaci�n de la configuraci�n IP:
	// NOTA: si se pone NULL en el primer par�metro, se obtiene la ip gen�rica 0.0.0.0
	//		 si se pone "localhost", se obtiene la gen�rica y la 127.0.0.0
	//		 si se pone "", se obtienen todas las ip de las interfaces de red disponibles
	if (::getaddrinfo ("", NULL, &hints, &servinfo) != 0)
	{
		return ret; // problema al obtener la informaci�n local
	}

	for (struct addrinfo* actServInfo = servinfo; actServInfo != NULL; actServInfo = actServInfo->ai_next)
	{
		{
			// Obtener la direcci�n IPv4 que se han encontrado:
#ifdef INET_NTOP_DISPONIBLE
			char ipv4 [INET_ADDRSTRLEN]; // space to hold the IPv4 string
			inet_ntop (AF_INET, get_in_addr ((struct sockaddr*)actServInfo->ai_addr), ipv4, INET_ADDRSTRLEN);
#else
			std::string ipv4 = getIPstr ((struct sockaddr*)actServInfo->ai_addr);
#endif
			ret.push_back (ipv4);
		}
	}
	freeaddrinfo (servinfo); // all done with this structure
	return ret;
}

/**
 *	Env�a un mensaje dado por un socket PRESUMIBLEMENTE abierto en TCP
 *	\param	[in]	socketID	identificador del socket por donde se enviar�
 *	\param	[in]	mensaje		mensaje que se va a enviar
 *	\param	[in]	leng		longitud del mensaje a enviar
 *	\retval	n�mero de bytes enviados
 **/
int ServerSocket::sendMessageTCP (const int socketID, const char* mensaje, size_t leng)
{
	// Mostrar algunos mensajes
	//DCOUT_MESSAGE (LANG_SE_ENVIO_MENSAJE_TCP);
	//DCOUT_MESSAGE ("------------------------<[");
	//DCOUT_MESSAGE (mensaje);
	//DCOUT_MESSAGE ("]>------------------------");
	int n = 0;
	for (size_t i = 0; i < leng; i += TCP_BLOCK_SIZE)
	{
		const size_t finalLeng = (i + TCP_BLOCK_SIZE < leng) ? TCP_BLOCK_SIZE : (leng - i);
		const int partial = ::send (socketID, mensaje + i, finalLeng, 0);
		if (partial < 0)
		{
#ifdef WIN32
			int ei = WSAGetLastError();
#endif
			this->lastError = ServerSocket::ERROR_AL_ENVIAR_MENSAJE;
		}
		n += partial;
	}
	return n;

}

/**
 *	Env�a un mensaje dado por un socket PRESUMIBLEMENTE abierto en UDP
 *	\param	[in]	idUDP		identificador del socket UDP por donde se enviar�
 *	\param	[in]	socketID	identificador del socket por donde se enviar�
 *	\param	[in]	mensaje		mensaje que se va a enviar
 *	\param	[in]	leng		longitud del mensaje a enviar
 *	\retval	n�mero de bytes enviados
 **/
int ServerSocket::sendMessageUDP (const int socketID, void* idUDP, const char* mensaje, size_t leng)
{
	// Mostrar algunos mensajes
	//DCOUT_MESSAGE (LANG_SE_ENVIO_MENSAJE_UDP);
	//DCOUT_MESSAGE ("------------------------<[");
	//DCOUT_MESSAGE (mensaje);
	//DCOUT_MESSAGE ("]>------------------------");
	IdentificadorUDP* ptr = (IdentificadorUDP*)idUDP;
	const int n = ::sendto (socketID, mensaje, leng, 0, (struct sockaddr*) & (ptr->their_addr), ptr->addr_size);
	if (n < 0)
	{
#ifdef WIN32
		int ei = WSAGetLastError();
#endif
		this->lastError = ServerSocket::ERROR_AL_ENVIAR_MENSAJE;
	}
	return n;
}


/**
 *	Cierra un socket PRESUMIBLEMENTE abierto
 *	\param	[in]	socketNumer	identificador del socket que se enviar�
 *	\retval	TRUE si se cerr� correctamente, FLASE si no
 **/
bool ServerSocket::closeSocket (const int socketID)
{
	//DCOUT_MESSAGE (LANG_SE_CIERRA_CONEXION_CON_CLIENTE);
	if (this->masterptr != NULL)
	{
		FD_CLR (socketID, (fd_set*) (this->masterptr)); // remove from master set
	}
#ifdef WIN32
	return ::closesocket (socketID) == 0;
#else
	return ::close (socketID) == 0;
#endif
}

/**
 *	Cierra el socket servidor y todos los hijos abiertos
 *	\retval	TRUE si se cerr� correctamente, FLASE si no
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms741549(v=vs.85).aspx (WSACleanup function (Windows))
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms737582(v=vs.85).aspx (closesocket function (Windows))
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740481(v=vs.85).aspx (shutdown  function (Windows))
**/
bool ServerSocket::closeSrvSocket()
{
	if (this->ptrServidorTCP != NULL)
	{
		this->ptrServidorTCP->setDesconectando();	// decir al manejador que se est� desconectado
	}
	else if (this->ptrServidorUDP != NULL)
	{
		this->ptrServidorUDP->setDesconectando();	// decir al manejador que se est� desconectado
	}

	// Proceder al cierre
	int sockfd = this->getSockFD();
	bool cierreCorrecto = true;
	// TODO: Cerrar todos los hijos
	// Y ahora se cierra el socket servidor
#ifdef WIN32
	/**
	 * TODO: realizar un shutdown para luego hacer un closesocket es algo irrelevante,
	 *			por lo tanto, se comenta para que no d� problemas raros
	if (shutdown (ptrServidorTCP->sockfd, SD_BOTH) != 0)
	{
		this->lastError = obtainSysLastErrorSocket();
		if (this->lastError == ServerSocket::ERROR_DESCONOCIDO)
		{
			this->lastError = ServerSocket::ERROR_AL_CERRAR_SCKT;
		}
		return false;
	}
	*/
	cierreCorrecto = (::closesocket (sockfd) == 0);
#else
	cierreCorrecto = (::close (sockfd) == 0);
#endif
	if (cierreCorrecto)
	{
#ifdef WIN32
		// The WSACleanup function terminates use of the Winsock 2 DLL (Ws2_32.dll).
		if (WSACleanup() != 0)
		{
			this->lastError = ServerSocket::ERROR_AL_LIBERAR_WINSOCK;
			return false;
		}
#endif
		this->lastError = ServerSocket::SIN_ERROR;
	}
	else
	{
		this->lastError = ServerSocket::ERROR_AL_CERRAR_SCKT;
		return false;
	}
	this->iniciado = false;
	return true;
}

// ----------------------------------------------------------------------------
// Control de errores
/**
 *	Obtiene el �ltimo error ocurrido
 *	\retval	el �ltimo error ocurrido
**/
ServerSocket::Errores ServerSocket::getLastError() const
{
	return this->lastError;
}
/**
 *	Obtiene el �ltimo error ocurrido como string
 *	\retval	el �ltimo error ocurrido como string
**/
std::string ServerSocket::getLastErrorStr() const
{
	switch (this->getLastError())
	{
	case SIN_ERROR:
		return "SIN_ERROR";
	case ERROR_DESCONOCIDO:
		return "ERROR_DESCONOCIDO";
	case ERROR_AL_TRANSFORMAR_PUERTO:
		return "ERROR_AL_TRANSFORMAR_PUERTO";
	case ERROR_AL_OBTENER_INFO:
		return "ERROR_AL_OBTENER_INFO";
	case ERROR_AL_CREAR_SCKT:
		return "ERROR_AL_CREAR_SCKT";
	case ERROR_AL_RESERVAR_SCKTR:
		return "ERROR_AL_RESERVAR_SCKTR";
	case ERROR_AL_ESCUCHAR_SCKT:
		return "ERROR_AL_ESCUCHAR_SCKT";
	case ERROR_AL_CERRAR_SCKT:
		return "ERROR_AL_CERRAR_SCKT";
	case ERROR_AL_LIBERAR_WINSOCK:
		return "ERROR_AL_LIBERAR_WINSOCK";
	case ERROR_CLIENTE_DESCONECTADO:
		return "ERROR_CLIENTE_DESCONECTADO";
	case ERROR_CLIENTE_DESCONOCIDO:
		return "ERROR_CLIENTE_DESCONOCIDO";

		// NOTA: No hay caso 'default' para que, revisando los warns de compilaci�n, se pueda
		//			detectar nuevos casos de error que no est�n puestos humanamente legibles
	}
	return "Error Desconocido";
}

/**
 *	Obtiene el error a nivel de socket que se ha producido
 *	\retval	el error que se ha producido
 *	\see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740668(v=vs.85).aspx
**/
ServerSocket::Errores ServerSocket::obtainSysLastErrorSocket() const
{
#ifdef WIN32
	const int i = WSAGetLastError();
	switch (i)
	{
	case WSAECONNRESET:		// 10054: Connection reset by peer
		return ServerSocket::ERROR_CLIENTE_DESCONECTADO;
	case WSAENOTSOCK:		// 10038: Socket operation on nonsocket
		return ServerSocket::ERROR_CLIENTE_DESCONOCIDO;
	case WSAEADDRINUSE:		// 10048: Address already in use
	case WSAESHUTDOWN:		// 10058: Cannot send after socket shutdown
	case WSAENOTCONN :		// 10057: Socket is not connected
	case WSANOTINITIALISED:	// 10093: Successful WSAStartup not yet performed
		break;
	}
#else
	switch (errno)	// \see http://www.ibm.com/developerworks/aix/library/au-errnovariable/
	{
	case EBADF:		// A function tried to use a bad file descriptor
		break;
	}
#endif
	return ServerSocket::ERROR_DESCONOCIDO;
}
