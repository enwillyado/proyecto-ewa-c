/*********************************************************************************************
 *	Name		: w_codificacion_c.h
 *	Description	: Funciones para codificar y decodificar secuencias de bytes (incluidas cadenas)
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _ENCRYPTC_H_
#define _ENCRYPTC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

/// Posibles resultados de las funciones de la libreria
typedef enum _EEncryptResult
{
	EncryptOk,				///< La operacion se realizo correctamente
	EncryptError,			///< Error no especificado
	EncryptErrorBadParam,	///< Error de parametros
	EncryptEquals,			///< Dos elementos comparados son iguales
	EncryptDifferents,		///< Dos elementos comparados son distintos
	EncryptFailFile,		///< Error al leer/guardar fichero
}
EEncryptResult;

//____________________________________________________________________________________________
//
// SHA1
//____________________________________________________________________________________________

// Tipo de dato que representa un valor de tipo SHA1 como valor binario
typedef unsigned char SHA1[20]; ///< Tipo de dato que representa un valor de tipo SHA1 como valor binario
typedef char SHA1STR[41]; ///< Tipo de dato que representa un valor de tipo SHA1 como cadena de caracteres

// Calcular un sha1 a partir de un buffer
void sha1CreateFromBuffer (const void* pBuffer, unsigned int szBuffer, SHA1 shaOut);

// Calcula el sha1 de un buffer que se le entrega de varias veces
typedef struct _Sha1Context
{
	unsigned long state [5];
	unsigned long count [2];
	unsigned char buffer[64];
} Sha1Context; ///< Contexto que mantiene el estado del calculo durante el tiempo que se van
//////////////////< realizando los calculos en las distintas partes del buffer

void sha1CreateFromTrunkBufferInit (Sha1Context* context);
void sha1CreateFromTrunkBufferAdd (Sha1Context* context, const void* pBuffer, size_t szBuffer);
void sha1CreateFromTrunkBufferFinish (Sha1Context* context, SHA1 sha1Out);

// Calcular un sha1 de un string
void sha1CreateFromString (const char* pStr, SHA1 sha1Out);

// Calcula el SHA1 de un buffer y lo compara con un SHA1 previamente calculada. 0 = iguales. 1 = distintos
EEncryptResult sha1CompareBuffer (const void* pBuffer, size_t szBuffer, const SHA1 shaToCompare);

// Calcula el SHA1 de un string y lo compara con un SHA1 previamente calculada. 0 = iguales. 1 = distintos
EEncryptResult sha1CompareString (const char* pString, SHA1 sha1ToCompare);

// Transforma un sha1 binario a cadena de caracteres
void sha1ToStr (const SHA1 sha1, SHA1STR sha1strOut);

//____________________________________________________________________________________________
//
// MD5
//____________________________________________________________________________________________

// Variables de tipo MD5
typedef unsigned char MD5[16];
typedef unsigned char MD5STR[33];

// Calcular un md5 a partir de un buffer
void md5CreateFromBuffer (const void* pBuffer, unsigned int szBuffer, MD5 md5Out);

// Calcula un md5 a partir de un buffer que se le entrega de varias veces
typedef struct _Md5Context
{
	unsigned int state[5];
	unsigned int count[2];
	unsigned char buffer[64];
} Md5Context;

///< Contexto que mantiene el estado del calculo durante el tiempo que se van
///< realizando los calculos en las distintas partes del buffer
void md5CreateFromTrunkBufferInit (Md5Context* context);
void Md5CreateFromTrunkBufferAdd (Md5Context* context, const void* pBuffer, unsigned int szBuffer);
void md5CreateFromTrunkBufferFinish (Md5Context* context, MD5 sha1Out);

// Calcular un md5 de un string
void md5CreateFromString (const char* pStr, MD5 md5Out);

// Calcula el SHA1 de un buffer y lo compara con un SHA1 previamente calculada. 0 = iguales. 1 = distintos
EEncryptResult md5CompareBuffer (const void* pBuffer, unsigned int szBuffer, const MD5 md5ToCompare);

// Calcula el SHA1 de un string y lo compara con un SHA1 previamente calculada. 0 = iguales. 1 = distintos
EEncryptResult md5CompareString (const char* pStr, MD5 md5ToCompare);

// Transforma un md5 binario a cadena de caracteres
void md5ToStr (const MD5 sha1, MD5STR sha1strOut);

//____________________________________________________________________________________________
//
// DES
//____________________________________________________________________________________________

// Representa una contraseña empleada por el algoritmo DES
typedef unsigned char Des3Key[8];

// Modos en que podemos emplear el algoritmo
typedef enum _EDesMode
{
	DesMode1Des = 1,
	DesMode3Des = 3,
}
EDesMode;

// Configuracion de como funciona el algoritmo
typedef enum _EDesConfig
{
	DesConfigElectronicCodeBook,
	DesConfigChainBlockCipher,
}
EDesConfig;

// Encriptar un buffer
EEncryptResult desEncryptBuffer (const void* pBufferIn, size_t szBufferIn, void* pBufferOut,
								 size_t* szBufferOut, const Des3Key key1, const Des3Key key2,
								 int nPadding, EDesMode mode, EDesConfig config);

// Desencriptar un buffer
EEncryptResult desDesencryptBuffer (const void* pBufferIn, size_t szBufferIn, void* pBufferOut,
									size_t* szBufferOut, const Des3Key key1, const Des3Key key2,
									int nPadding, EDesMode mode, EDesConfig config);

// Calcular el tamaño maximo de un buffer tras ser encriptado
#define DesCalculateSizeEncryptBuffer(bPadding, desConfig, szDesencrypt) \
	((((szDesencrypt)/8) + (((szDesencrypt) % 8 > 0) ? 1 : ((bPadding) ? 1 : 0)) + \
	  ((desConfig) ? 1 : 0)) * 8)

// Calcular el tamaño maximo de un buffer tras ser desencriptado
#define DesCalculateSizeDesencryptBuffer(bPadding, desConfig, szEncrypt) \
	((((szEncrypt)/8) - ((desConfig) ? 1 : 0)) * 8)

//____________________________________________________________________________________________
//
// Base64
//____________________________________________________________________________________________

// Codifica en base64 un buffer binario
EEncryptResult base64EncodeFromBuffer (const void* bufferIn, size_t szBufferIn,
									   char* bufferB64Out, size_t szBuffer64Out);

// Codifica en base64 una cadena de caracteres
EEncryptResult base64EncodeFromString (const char* stringIn, char* stringB64Out, size_t szBufferB64Out);

// Decodifica un buffer en base 64 dejando el resultado como buffer binario
EEncryptResult base64DecodeToBuffer (const char* stringB64In, void* bufferOut, size_t* szBufferOut);

// Decodifica un buffer en base 64 dejando el resultado como string
EEncryptResult base64DecodeToString (const void* stringB64In, char* stringOut, size_t* szStringOut);


// Calcular el tamaño minimo que debe tener un buffer destino. Se deja como string. Este
// tamaño ya incluye el almacenaimento del \0
#define Base64CalculateSizeEncodedBuffer(bytesInDecodeMessage) ((bytesInDecodeMessage) / 3 * 4 + 4 + 1)

// Calcular el numero maximo de caracteres que debe tener un buffer destino. Se deja como string.
// Calcula numero de caracteres, por tanto no incluye el sitio para el \0
#define Base64CalculateNumCharsEncoded(bytesInDecodeMessage) ((bytesInDecodeMessage) / 3 * 4 + 4)

// Calcula el tamaño minimo que debe tener un buffer destino en la decodificacion de un buffer binario
#define Base64CalculateSizeDecodedBuffer(bytesInEncodeMessage) ((bytesInEncodeMessage) / 4 * 3)

// Calcula el tamaño minimo que debe tener un buffer destino en la decodificacion de un string
#define Base64CalculateSizeDecodedString(bytesInEncodeMessage) ((bytesInEncodeMessage) / 4 * 3 + 1)

//____________________________________________________________________________________________
//
// AES o Rijndael
//____________________________________________________________________________________________

// Representa una contraseña empleada por el algoritmo AES
typedef unsigned char AES_KEY[16];

// Encriptar un buffer
EEncryptResult aesEncryptBuffer (const void* bufferIn, size_t szBufferIn, void* bufferOut, size_t szBufferOut, const AES_KEY key);
// Desencriptar un buffer
EEncryptResult aesDesencryptBuffer (const void* bufferIn, size_t szBufferIn, void* bufferOut, size_t szBufferOut, const AES_KEY key);

#define AesCalculateSizeEncryptBuffer(bytesBufferIn) ( ((bytesBufferIn)&0x0f) ? (((bytesBufferIn)&0xfffffff0) + 0x10) : (((bytesBufferIn)&0xfffffff0)) )

#ifdef __cplusplus
}
#endif



#endif // _ENCRYPTC_H_