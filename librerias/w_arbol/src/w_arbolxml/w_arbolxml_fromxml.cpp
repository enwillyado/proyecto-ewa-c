/*********************************************************************************************
 *	Name		: w_arbolxml_fromxml.cpp
 *	Description	: Lectura de un �rbol a partir de un XML
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_arbol/w_arbolxml.h"

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> M�todos Internos <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

/**
 *	Introduce (recursivamente) la informaci�n en un arbol a partir del desplazamiento indicado hasta el cierre
 *	\param	[in/out]	arbol	referencia al arbol donde se introduce la informaci�n
 *	\param	[in]		cadena	cadena original recibida
 *	\param	[in/out]	node	desplazamiento del cursor sobre la cadena
 *  \retval	 si se ha evaluado correctamente
 **/
static bool parseDocumentNode (Arbol & arbol, const std::string & cadena, size_t & desplazamiento)
{
	bool hayElemento = false;
	std::string valor = "";
	for (size_t i = desplazamiento; i < cadena.size(); i++)
	{
		const char c = cadena[i];
		switch (c)
		{
		case '<':
		{
			// A�adir el valor acumulado
			hayElemento = true;
			if (valor != "")
			{
				arbol.setValor (valor);
			}

			// Procesar subelemento
			i++;
			Arbol arbolHijo;
			std::string tmp = "";
			std::string tmpNombre = "";
			bool inicioElemento = true;
			bool finElemento = false;
			while (i < cadena.size() && finElemento == false)
			{
				const char c = cadena[i];
				switch (c)
				{
				case '/':
					if (i == desplazamiento + 1)
					{
						desplazamiento = i;
						return true;
					}
					i++;
					if (tmp != "")
					{
						tmpNombre = tmp;
						tmp = "";
						arbolHijo.setClave (tmpNombre);
						arbol.addHijo (arbolHijo);
					}
					else
					{
						desplazamiento = i - 1;
						return true;
					}

					while (i < cadena.size() && finElemento == false)
					{
						const char c = cadena[i];
						switch (c)
						{
						case '>':
							finElemento = true;
							break;

						case '<':
							finElemento = true;
							break;

						default:
							tmp.push_back (c);
							i++;
							break;
						}
					}
					break;

				case '>':
				{
					i++;
					if (finElemento == false)
					{
						const bool ret = ::parseDocumentNode (arbolHijo, cadena, i);
						if (ret == false)
						{
							return false;
						}
					}
				}
				break;

				case ' ':
				{
					enum EstadoDentro
					{
						NO_DENTRO,
						DENTRO_SIMPLES,		//< iniciado con bloque de comillas simples
						DENTRO_DOBLES,		//< iniciado con bloque de comillas dobles
					};
					bool escape = false;
					EstadoDentro dentro = NO_DENTRO;
					bool finAtributos = false;
					std::string tmpClave = "", tmpValor = "", tmp = "";
					while (i < cadena.size() && finAtributos == false)
					{
						i++;
						const char c = cadena[i];
						switch (c)
						{
						case '=':
							if (dentro == NO_DENTRO)
							{
								tmpClave = tmp;
								tmp = "";
							}
							else
							{
								tmp.push_back (c);
							}
							break;

						case '\\':
						{
							if (i + 1 < cadena.size())
							{
								i++;
								const char c = cadena[i];
								if (c == '\\')
								{
									tmp.push_back (c);
								}
								else
								{
									// TODO: �qu� pasa con los escapes reales?
									tmp.push_back (c);
								}
								break;
							}
							escape = true;
						}
						break;

						case '"':
						case '\'':
							if (dentro == NO_DENTRO)
							{
								dentro = (c == '"' ? DENTRO_DOBLES : DENTRO_SIMPLES);
							}
							else if (dentro != (c == '"' ? DENTRO_DOBLES : DENTRO_SIMPLES))
							{
								tmp.push_back (c);
							}
							else
							{
								tmpValor = tmp;
								arbolHijo.addAtributo (tmpClave, tmpValor);
								tmpValor = tmpClave = tmp = "";
								dentro = NO_DENTRO;
							}
							break;

						case ' ':
							if (dentro != NO_DENTRO)
							{
								tmp.push_back (c);
							}
							break;

						case '>':
						case '/':
							if (dentro == NO_DENTRO)
							{
								if (tmpClave != "")
								{
									arbolHijo.addAtributo (tmpClave, tmpValor);
								}
								finAtributos = true;
							}
							else
							{
								tmp.push_back (c);
							}
							break;

						default:
							tmp.push_back (c);
							break;
						}
					}
				}
				break;

				case '?':
					if (true == inicioElemento)
					{
						std::string cabecera;

						enum EstadoDentro
						{
							DENTRO_CABECERA,
							PRE_FUERA,			//< tras leer un "?"
							FUERA,				//< tras leer un "?>"
							CUERPO,				//< tras leer la cabecera y el "<" del cuerpo
						};
						EstadoDentro dentro = DENTRO_CABECERA;
						while (i < cadena.size() && dentro != CUERPO)
						{
							i++;
							const char c = cadena[i];
							switch (c)
							{
							case '?':
								dentro = PRE_FUERA;
								break;
							case '>':
								if (dentro == PRE_FUERA)
								{
									// TODO: a�adir cabecera al arbol
									dentro = FUERA;

									// Buscar el primer '<' que inicia el cuerpo
									while (i < cadena.size() && dentro != CUERPO)
									{
										i++;
										const char c = cadena[i];
										switch (c)
										{
										case '<':
											dentro = CUERPO;
											i++;
											break;
										}
									}
								}
								else
								{
									cabecera.push_back ('?');
									cabecera.push_back ('>');
									dentro = DENTRO_CABECERA;
								}
								break;

							default:
								if (dentro == PRE_FUERA)
								{
									cabecera.push_back ('?');
									dentro = DENTRO_CABECERA;
								}
								cabecera.push_back (c);
							}
						}
						break;
					}
				// si no, es un caso por defecto

				default:
					i++;
					tmp.push_back (c);
				}
				inicioElemento = false;
			}
		}
		break;
		default:
		{
			valor.push_back (c);
			break;
		}
		}
	}

	// Llegados a este punto, es que ha ido mal si no encontramos ning�n elemento
	return hayElemento;
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> M�todos P�blicos <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

/**
 *	Devuelve una instancia ArbolXML tras intentar evaluar un fichero
 *	\param	[in]	filename	ruta del fichero que se desea abrir (supuestamente es un ".xml")
 *  \retval	 la instancia del ArbolXML correspondiente a evaluar el contenido del fichero (incluye un lastError con el resultado)
 **/
ArbolXML ArbolXML::getArbolFromFile (const std::string & filename)
{
	if (true == ArbolXML::file_exists_and_can_be_read_from (filename))
	{
		const std::string & contenido = ArbolXML::leerContenidoFile (filename);
		return ArbolXML::getArbolFromSTR (contenido);
	}
	ArbolXML arbol;
	arbol.setError (ArbolXML::NO_EXISTE_FICHERO);
	return arbol;
}

/**
 *	Devuelve una instancia ArbolXML tras intentar evaluar una cadena
 *	\param	[in]	str					referencia constante a la cadena a evaluar (se supone que es un xml)
 *  \retval	 la instancia del ArbolXML correspondiente a evaluar la cadena (incluye un lastError con el resultado)
 **/
ArbolXML ArbolXML::getArbolFromSTR (const std::string & str)
{
	Arbol arbol;
	size_t desplazamiento = 0;
	const bool correcto = ::parseDocumentNode (arbol, str, desplazamiento);
	if (correcto == false)
	{
		return Arbol::npos;
	}

	// Todo ha ido bien
	ArbolXML ret (arbol[0]);
	ret.setError (ArbolXML::SIN_ERROR);
	return ret;
}
