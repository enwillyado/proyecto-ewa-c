#include "sim/mmu.h"
#include <cstring>

MMU::MMU()
{
	this->laMap = new MAP();
}
MMU::~MMU()
{
	if (this->laMap != NULL)
	{
		delete this->laMap;
		this->laMap = NULL;
	}
}

void MMU::iniciar()
{
	// Ponemos el puntero PC a todo 0s apuntando al GAM
	for (int i = 0; i < PAGINAS; i++)
	{
		for (int j = 0; j < COMPONT; j++)
		{
			this->PC[i][j] = 0;
		}
	}
	this->pagina_actual = 0;
	this->puntro_actual = 0;
}

// acceso a los registros: p�gina actual de puntero actual
const bool MMU::dame0()
{
	return this->punteros[this->puntro_actual][this->pagina_actual][0];
}
const bool MMU::dame1()
{
	return this->punteros[this->puntro_actual][this->pagina_actual][1];
}
const bool MMU::dame2()
{
	return this->punteros[this->puntro_actual][this->pagina_actual][2];
}
const bool MMU::dame3()
{
	return this->punteros[this->puntro_actual][this->pagina_actual][3];
}
const bool MMU::dame4()
{
	return this->punteros[this->puntro_actual][this->pagina_actual][4];
}
const bool MMU::dame5()
{
	return this->punteros[this->puntro_actual][this->pagina_actual][5];
}
const bool MMU::dame6()
{
	return this->punteros[this->puntro_actual][this->pagina_actual][6];
}
const bool MMU::dame7()
{
	return this->punteros[this->puntro_actual][this->pagina_actual][7];
}

// escritura en los registros: p�gina actual de puntero actual
void MMU::toma0 (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][0] = que;
}
void MMU::toma1 (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][1] = que;
}
void MMU::toma2 (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][2] = que;
}
void MMU::toma3 (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][3] = que;
}
void MMU::toma4 (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][4] = que;
}
void MMU::toma5 (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][5] = que;
}
void MMU::toma6 (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][6] = que;
}
void MMU::toma7 (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][7] = que;
}

void MMU::actualPuntreroA (const bool que)
{
	for (int i = 0; i < PAGINAS; i++)
	{
		this->punteros[this->puntro_actual][i][0] = que;
		this->punteros[this->puntro_actual][i][1] = que;
		this->punteros[this->puntro_actual][i][2] = que;
		this->punteros[this->puntro_actual][i][3] = que;
		this->punteros[this->puntro_actual][i][4] = que;
		this->punteros[this->puntro_actual][i][5] = que;
		this->punteros[this->puntro_actual][i][6] = que;
		this->punteros[this->puntro_actual][i][7] = que;
	}
}
void MMU::actualPaginaA (const bool que)
{
	this->punteros[this->puntro_actual][this->pagina_actual][0] = que;
	this->punteros[this->puntro_actual][this->pagina_actual][1] = que;
	this->punteros[this->puntro_actual][this->pagina_actual][2] = que;
	this->punteros[this->puntro_actual][this->pagina_actual][3] = que;
	this->punteros[this->puntro_actual][this->pagina_actual][4] = que;
	this->punteros[this->puntro_actual][this->pagina_actual][5] = que;
	this->punteros[this->puntro_actual][this->pagina_actual][6] = que;
	this->punteros[this->puntro_actual][this->pagina_actual][7] = que;
}

void MMU::actualToPC()
{
#ifdef _DEBUG
	const Direccion & prePC = MAP::dameDireccion (this->PC);
	const Direccion & preAC = MAP::dameDireccion (this->punteros[this->puntro_actual]);
#endif
	for (int i = 0; i < PAGINAS; i++)
	{
		this->PC[i][0] = this->punteros[this->puntro_actual][i][0];
		this->PC[i][1] = this->punteros[this->puntro_actual][i][1];
		this->PC[i][2] = this->punteros[this->puntro_actual][i][2];
		this->PC[i][3] = this->punteros[this->puntro_actual][i][3];
		this->PC[i][4] = this->punteros[this->puntro_actual][i][4];
		this->PC[i][5] = this->punteros[this->puntro_actual][i][5];
		this->PC[i][6] = this->punteros[this->puntro_actual][i][6];
		this->PC[i][7] = this->punteros[this->puntro_actual][i][7];
	}
#ifdef _DEBUG
	const Direccion & posPC = MAP::dameDireccion (this->PC);
	const Direccion & posAC = MAP::dameDireccion (this->punteros[this->puntro_actual]);
#endif
}
void MMU::PCToActual()
{
#ifdef _DEBUG
	const Direccion & prePC = MAP::dameDireccion (this->PC);
	const Direccion & preAC = MAP::dameDireccion (this->punteros[this->puntro_actual]);
#endif
	for (int i = 0; i < PAGINAS; i++)
	{
		this->punteros[this->puntro_actual][i][0] = this->PC[i][0];
		this->punteros[this->puntro_actual][i][1] = this->PC[i][1];
		this->punteros[this->puntro_actual][i][2] = this->PC[i][2];
		this->punteros[this->puntro_actual][i][3] = this->PC[i][3];
		this->punteros[this->puntro_actual][i][4] = this->PC[i][4];
		this->punteros[this->puntro_actual][i][5] = this->PC[i][5];
		this->punteros[this->puntro_actual][i][6] = this->PC[i][6];
		this->punteros[this->puntro_actual][i][7] = this->PC[i][7];
	}
#ifdef _DEBUG
	const Direccion & posPC = MAP::dameDireccion (this->PC);
	const Direccion & posAC = MAP::dameDireccion (this->punteros[this->puntro_actual]);
#endif
}

void MMU::seleccionaPagina (int cual)
{
	this->pagina_actual = cual;
}
void MMU::seleccionaPuntro (int cual)
{
	this->puntro_actual = cual;
}

void MMU::retrocedePagina()
{
	this->pagina_actual--;
	if (this->pagina_actual < 0)
	{
		this->pagina_actual = PAGINAS - 1;
	}
}
void MMU::avanza_laPagina()
{
	this->pagina_actual++;
	if (this->pagina_actual == PAGINAS)
	{
		this->pagina_actual = 0;
	}
}

void MMU::retrocedePuntro()
{
	this->puntro_actual--;
	if (this->puntro_actual <= PUNTROS_ORG)
	{
		this->puntro_actual = PUNTROS - 1;
	}
}
void MMU::avanza_elPuntro()
{
	this->puntro_actual++;
	if (this->puntro_actual == PUNTROS)
	{
		this->puntro_actual = 5;
	}
}

void MMU::funcionDeEjecucion (const bool Z, const bool U, const bool V, const bool W)
{
	if (Z == 0)
	{
		if (U == 0)
		{
			if (V == 0)
			{
				if (W == 0)
				{
					this->actualPuntreroA (0);  // 0  0  0  0  Pone el Puntero Actual a todo ceros
				}
				else
				{
					this->actualPaginaA (1);  // 0  0  0  1  Pone la P�gina Actual a todo unos
				}
			}
			else
			{
				if (W == 0)
				{
					this->retrocedePagina(); // 0  0  1  0  Retrocede la P�gina Actual (*)
				}
				else
				{
					this->avanza_laPagina(); // 0  0  1  1  Avanza la P�gina Actual (*)
				}
			}
		}
		else
		{
			if (V == 0)
			{
				if (W == 0)
				{
					this->seleccionaPuntro (1);  // 0  1  0  0  Selecciona el Puntero Actual P1(Q)
				}
				else
				{
					this->seleccionaPuntro (2);  // 0  1  0  1  Selecciona el Puntero Actual P2(R)
				}
			}
			else
			{
				if (W == 0)
				{
					this->seleccionaPuntro (3);  // 0  1  1  0  Selecciona el Puntero Actual P3(S)
				}
				else
				{
					this->seleccionaPuntro (4);  // 0  1  1  1  Selecciona el Puntero Actual P4(T)
				}
			}
		}
	}
	else
	{
		if (U == 0)
		{
			if (V == 0)
			{
				if (W == 0)
				{
					this->seleccionaPagina (0);  // 1  0  0  0  Pone la P�gina Actual a la primera
				}
				else
				{
					this->seleccionaPuntro (5);  // 1  0  0  1  Selecciona el Puntero Actual L0(L)
				}
			}
			else
			{
				if (W == 0)
				{
					this->retrocedePuntro(); // 1  0  1  0  Selecciona el anterior Puntero Actual (*) (en L)
				}
				else
				{
					this->avanza_elPuntro(); // 1  0  1  1  Selecciona el siguiente Puntero Actual (*) (en L)
				}
			}
		}
		else
		{
			if (V == 0)
			{
				if (W == 0)
				{
					this->decrementaPuntero(); // 1  1  0  0  Decrementa el valor del Puntero Actual
				}
				else
				{
					this->incrementaPuntero(); // 1  1  0  1  Incrementa el valor del Puntero Actual
				}
			}
			else
			{
				if (W == 0)
				{
					this->PCToActual(); // 1  1  1  0  Pone en PC el valor del Puntero Actual
				}
				else
				{
					this->actualToPC(); // 1  1  1  0  Pone en el Puntero Actual el valor del PC
				}
			}
		}
	}
}
const bool MMU::dameSemiInstruccion()
{
	bool & SI = this->laMap->dameRegistro (this->PC);
	this->incrementaPunteroPC();
	return SI;
}
const bool MMU::dame()
{
	return this->laMap->dameRegistro (this->punteros[this->puntro_actual]);
}
void MMU::toma (const bool que)
{
	bool & c = this->laMap->dameRegistro (this->punteros[this->puntro_actual]);
	c = que;
}
void MMU::escribirMemoria (const Direccion ptr, const bool que)
{
	bool & c = this->laMap->dameRegistro (ptr);
	c = que;
}
const bool MMU::leer_de_Memoria (const Direccion ptr)
{
	return this->laMap->dameRegistro (ptr);
}

void MMU::decrementaPuntero()
{
	int acarreo = 1;
	for (int i = 0; i < PAGINAS && acarreo != 0; i++)
	{
		for (int j = 0; j < COMPONT && acarreo != 0; j++)
		{
			if (this->punteros[this->puntro_actual][i][j] == 1)
			{
				this->punteros[this->puntro_actual][i][j] = 0;
				acarreo = 0;
			}
			else
			{
				this->punteros[this->puntro_actual][i][j] = 1;
			}
		}
	}
}
void MMU::incrementaPuntero()
{
	int acarreo = 1;
	for (int i = 0; i < PAGINAS && acarreo != 0; i++)
	{
		for (int j = 0; j < COMPONT && acarreo != 0; j++)
		{
			if (this->punteros[this->puntro_actual][i][j] == 0)
			{
				this->punteros[this->puntro_actual][i][j] = 1;
				acarreo = 0;
			}
			else
			{
				this->punteros[this->puntro_actual][i][j] = 0;
			}
		}
	}
}

void MMU::incrementaPunteroPC()
{
	int acarreo = 1;
	for (int i = 0; i < PAGINAS && acarreo != 0; i++)
	{
		for (int j = 0; j < COMPONT && acarreo != 0; j++)
		{
			if (this->PC[i][j] == 0)
			{
				this->PC[i][j] = 1;
				acarreo = 0;
			}
			else
			{
				this->PC[i][j] = 0;
			}
		}
	}
}
