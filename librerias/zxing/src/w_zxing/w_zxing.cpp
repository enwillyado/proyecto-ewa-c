/*********************************************************************************************
 *	Name		: zxing_cmd.cpp
 *	Description	: Ejecutable donde se implementa un ejemplo para leer c�digos en BMP
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "zxing/w_zxing.h"

#include <zxing/qrcode/QRCodeReader.h>
#include <zxing/Exception.h>
#include <zxing/common/GlobalHistogramBinarizer.h>
#include <zxing/DecodeHints.h>

#include <traductor.h>

#include "w_graficos/w_lienzo_bmp.h"
#include "w_graficos/w_rgba.h"

#include <stdint.h>

using namespace std;
using namespace zxing;
using namespace zxing::qrcode;
using namespace qrviddec;

class BufferLienzo
{
public:
	BufferLienzo (const WLienzoBase & lienzo, const size_t width, const size_t height)
		: buffer (NULL)
	{
		this->buffer = new uint8_t[width * height];
		int i = 0;
		for (size_t y = 0; y < height; y++)
		{
			for (size_t x = 0; x < width; x++)
			{
				const RGBA & color = lienzo.getPixel (x, y);
				this->buffer[i] = (color.r + color.g + color.b) / 3;
				i++;
			}
		}
	}
	~BufferLienzo()
	{
		delete[] this->buffer;
	}
	operator uint8_t* & ()
	{
		return this->buffer;
	}

private:
	uint8_t* buffer;
};

WZxingResult WZxing::procesar (const WLienzoBase & lienzo)
{
	WZxingResult ret;

	// Intentar sacar un c�digo QR
	try
	{
		// A buffer containing an image. In your code, this would be an image from your camera. In this
		// example, it's just an array containing the code for "Hello!".
		const size_t width = lienzo.getAncho();
		const size_t height = lienzo.getAlto();
		BufferLienzo buffer (lienzo, width, height);

		// Convert the buffer to something that the library understands.
		Ref<LuminanceSource> source (new BufferBitmapSource (width, height, buffer));

		// Turn it into a binary image.
		Ref<Binarizer> binarizer (new GlobalHistogramBinarizer (source));
		Ref<BinaryBitmap> image (new BinaryBitmap (binarizer));

		// Tell the decoder to try as hard as possible.
		DecodeHints hints (DecodeHints::DEFAULT_HINT);
		hints.setTryHarder (true);

		// Perform the decoding.
		QRCodeReader reader;
		Ref<Result> result (reader.decode (image, hints));

		// Output the result.
		ret.setFormato ((WZxingResult::Formato)result->getBarcodeFormat());

		// Coordenadas
		cout << "Coordenadas:" << endl;
		const std::vector<Ref<ResultPoint>> & v = result->getResultPoints();
		for (std::vector<Ref<ResultPoint>>::const_iterator itr = v.begin(); itr != v.end(); itr++)
		{
			const WCoordenada coordenada ((size_t) (*itr)->getX(), (size_t) (*itr)->getY());
			ret.pushCoordenada (coordenada);
		}

		// Texto
		ret.setTexto (result->getText()->getText());
	}
	catch (zxing::Exception & e)
	{
		// Texto con el error
		ret.setTexto (e.what());
	}

	return ret;
}
