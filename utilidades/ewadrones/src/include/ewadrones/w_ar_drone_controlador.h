/*********************************************************************************************
 * Name			: w_ar_drone_controlador.h
 * Description	: Clase para el controloador del AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_AR_DRONE_CONTROLADOR_H_
#define _W_AR_DRONE_CONTROLADOR_H_

#include "w_red/w_red_cliudp.h"
#include "w_sistema/w_sistema_thread.h"

#include <cstring>				//< NULL
#include <string>

// FWD
class EWadrones;

class ArDroneControlador
{
public:
	ArDroneControlador (EWadrones* = NULL);
	void setPtrArDrone (EWadrones*);
	void init();

	void start();
	void stop();

	void aterrizar();
	void despegar();
	void emergencia();

	void estabilizar();

	void arriba();
	void abajo();
	void defrente();
	void atras();
	void derecha();
	void izquierda();
	void rotarDerecha();
	void rotarIzquierda();

	void ajustarHorizontal();

public:
	void habilitarSalidaComandos (const bool);
	const std::string & getSalidaComandos() const;
	void clearSalidaComandos();

protected:
	void enviarMensaje (const std::string &);
	static void initControlUDP (const WThread &);

private:

	ClienteUDP clienteComandos;
	EWadrones* arDrone;
	WThread hiloComandos;

	bool salidaComandosHabilitada;
	std::string salidaComandos;
};



#endif // _W_AR_DRONE_CONTROLADOR_H_
