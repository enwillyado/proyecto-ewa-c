/*********************************************************************************************
 * Name			: w_multicontrol_keyboard.cpp
 * Description	: Implementación de los métodos relativos al MOUSE
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_multicontrol.h"

// ----------------------------------------------------------------------------
// onKeyDown
void WMultiControl::onKeyDown (const unsigned int key)
{
	WMultiControl::WControlKeyable::onKeyDown (key);

	// Lanzarlo a los hijos
	for (size_t i = 0; i < this->controles.size(); i++)
	{
		const size_t ii = this->controles.size() - i - 1;
		if (this->controles[ii].ptr == NULL)
		{
			continue;
		}
		WControl & control = * (this->controles[ii].ptr);
		control.onKeyDown (key);
	}
}

// ----------------------------------------------------------------------------
// onKeyUp
void WMultiControl::onKeyUp (const unsigned int key)
{
	WMultiControl::WControlKeyable::onKeyUp (key);

	// Lanzarlo a los hijos
	for (size_t i = 0; i < this->controles.size(); i++)
	{
		const size_t ii = this->controles.size() - i - 1;
		if (this->controles[ii].ptr == NULL)
		{
			continue;
		}
		WControl & control = * (this->controles[ii].ptr);
		control.onKeyUp (key);
	}
}
