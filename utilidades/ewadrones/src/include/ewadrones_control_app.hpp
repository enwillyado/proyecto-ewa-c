/*********************************************************************************************
 *	Name		: ewadrones_control_app.hpp
 *	Description	: Eventos para la aplicaci�n de control para EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_ewadrones_control.pragmalib.h"

#include "ewadrones_control_globales.h"
#include "w_graficos/w_eventoKey.h"
#include "w_graficos/w_textControl.h"

#include "w_base/toStr.hpp"

class CosasAPP
{
public:
	static void MostrarTexto (const std::string & texto)
	{
		// Mostrar en el control de texto la tecla pulsada
		static WTextControl & controlTexto = ::GetAPP()->obtenerAs<WTextControl> ("texto", true);
		controlTexto.setText (texto);
		controlTexto.pintar();
	}
	static void MostrarBateria (const unsigned char nuevoValor)
	{
		// Mostrar en el control de texto la tecla pulsada
		static WTextControl & controlTexto = ::GetAPP()->obtenerAs<WTextControl> ("bateria", true);
		controlTexto.setText (::toStr ((short)nuevoValor));
		controlTexto.pintar();
	}
	static unsigned int & GetIsDown()
	{
		static unsigned int numDown = 0;
		return numDown;
	}
	static void onFousOut (const WEvento & evento)
	{
		unsigned int & numDown = CosasAPP::GetIsDown();
		numDown = 0;
		CosasAPP::accionSiSoltarMandos (numDown);
	}
	static void onKeyUp (const WEventoKey & evento)
	{
		// Decrementar n�mero de teclas pulsadas
		unsigned int & numDown = CosasAPP::GetIsDown();
		if (numDown > 0)
		{
			// Decrementar
			numDown--;
		}
		CosasAPP::accionSiSoltarMandos (numDown);
	}
	static void accionSiSoltarMandos (const unsigned int numDown)
	{
		if (numDown == 0)
		{
			// Si era la �ltima, estabilizar
#ifdef _W_JUMPING_SUMO_CONTROLADOR_H_
			::GetJumpingSumo().estabilizar();
#else
			::GetArDrone().getControlador().estabilizar();
#endif

			// Mostrar texto
			CosasAPP::MostrarTexto ("=");
		}
	}
	static void onKeyDown (const WEventoKey & evento)
	{
		// Mostrar en el control de texto la tecla pulsada
		CosasAPP::MostrarTexto (::toStr (evento.key));

		// Incrementar n�mero de teclas pulsadas
		CosasAPP::GetIsDown()++;

		// Realizar la acci�n correspondiente
		CosasAPP::RealizarAccion (evento.key);
	}
	static void RealizarAccion (const unsigned int tecla)
	{
		static bool shift = false;
		switch (tecla)
		{
		case 27:
			// ESC(27)
			::GetArDrone().getControlador().emergencia();
			::GetAPP()->stop();
			//exit (0);
			break;

#ifdef _W_JUMPING_SUMO_CONTROLADOR_H_
#else
		case 13:
			// ENTER(13)
			::GetArDrone().getControlador().despegar();
			break;

		case ' ':
			// SPACE(32)
			::GetArDrone().getControlador().aterrizar();
			break;

		case 8:
			// RETROCESO(8)
			::GetArDrone().getControlador().ajustarHorizontal();
			break;
#endif

		// --------------------------------------------------------------------
		case 37:
			// <-(37)
#ifdef _W_JUMPING_SUMO_CONTROLADOR_H_
			::GetJumpingSumo().izquierda();
#else
			::GetArDrone().getControlador().izquierda();
#endif
			break;
		case 38:
			// /|\(38)
#ifdef _W_JUMPING_SUMO_CONTROLADOR_H_
			::GetJumpingSumo().defrente();
#else
			::GetArDrone().getControlador().defrente();
#endif
			break;
		case 39:
			// ->(37)
#ifdef _W_JUMPING_SUMO_CONTROLADOR_H_
			::GetJumpingSumo().derecha();
#else
			::GetArDrone().getControlador().derecha();
#endif
			break;
		case 40:
			// \|/(40)
#ifdef _W_JUMPING_SUMO_CONTROLADOR_H_
			::GetJumpingSumo().atras();
#else
			::GetArDrone().getControlador().atras();
#endif
			break;

			// --------------------------------------------------------------------
#ifdef _W_JUMPING_SUMO_CONTROLADOR_H_
#else
		case 189:
			// -(189)
			::GetArDrone().getControlador().rotarDerecha();
			break;
		case 190:
			// .(190)
			::GetArDrone().getControlador().rotarIzquierda();
			break;

		case 16:
			// SHIFT(16)
			::GetArDrone().getControlador().arriba();
			break;
		case 17:
			// CTRL(17)
			::GetArDrone().getControlador().abajo();
			break;

		// --------------------------------------------------------------------
		case 188:
			// ,(188)
			::GetArDrone().getControlador().estabilizar();
			break;
#endif

		default:
			// En cualquier otro caso, ignorara
			break;
		}
	}

	static void onXMouseDrag (const WEventoMouseDrag & evento)
	{
		evento.getRoot().setPosX (evento.getRoot().getPosX() + evento.getIncrementoPosX());
		evento.getRoot().setPosY (evento.getRoot().getPosY() + evento.getIncrementoPosY());
		evento.getRoot().rePosAndDim();
	}

	static void invertCursorRoot (const WEventoMouse & evento)
	{
		WApp & app = evento.getRoot();
		WControl & cursor = app.getCursor();
		cursor.getLienzo().invertColor();
		app.setCursor (cursor);
	}
};
