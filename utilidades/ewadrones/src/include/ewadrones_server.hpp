/*********************************************************************************************
 * Name			: ewadrones_server.hpp
 * Description	: Servidor Web para el control remoto del EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_SERVER_HPP_
#define _EWADRONES_SERVER_HPP_

// ----------------------------------------------------------------------------
// Servidor Web
#include "w_web/w_web_srv_websocket.h"
#include "w_codificacion/w_codificacion.h"
#include "w_base/fromFile.hpp"
#include "w_base/fromStr.hpp"
#include "w_arbol/w_arbolxml.h"
#include "w_sistema/w_sistema_time.h"

#define PUERTO_SERVIDOR_PRUEBA	80

class ServidorWeb : public ServidorWebSockets
{
	virtual Mensaje responderMsgWebRecv (const EventoServidorTCP & evento, const Mensaje & peticion)
	{
		if (peticion.getURI() == "/")
		{
			Mensaje respuesta;
			const std::string & mensajeRespondido = ::leerContenidoFile ("../testdata/visor_online.htm");
			respuesta.setVersionProtocolo (Mensaje::HTTP1_0);
			respuesta.setBody (mensajeRespondido);
			respuesta.setError (Mensaje::FOUND);	// por si se desea terminar en otro sitio
			respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);	// por si se desea terminar en otro sitio
			return respuesta;
		}
		else if (peticion.getURI() == "/websocket")
		{
			return this->ServidorWebSockets::responderMsgSokRecv (evento, peticion);
		}

		return this->ServidorWebSockets::responderMsgWebRecv (evento, peticion);
	}
	virtual std::string procesaMsgSokRecv (const EventoServidorTCP & evento)
	{
		const std::string & mensaje = this->ServidorWebSockets::procesaMsgSokRecv (evento);
		procesaMsgSokRecv (evento, mensaje);
		return mensaje;
	}
	void procesaMsgSokRecv (const EventoServidorTCP & evento, const std::string & peticion)
	{
		if (peticion == "")
		{
			// Desconexión
		}
		else
		{
			// Control
			const Arbol & a = ArbolXML::getArbolFromSTR (peticion);
			if ((a % "y") != "")
			{
				::GetJumpingSumo().setAcelerador (::fromStr<int> (a % "y"));
			}
			if ((a % "x") != "")
			{
				::GetJumpingSumo().setVolante (::fromStr<int> (a % "x"));
			}
		}
	}
public:
	static void onNewJPEG (const std::string & jpegData)
	{
		ServidorWeb & servidor = GetMainServidorWeb();
		const MapaSocketsAbiertos s = servidor.socketsAbiertos;
		for (MapaSocketsAbiertos::const_iterator itr = s.begin();
				itr != s.end(); itr++)
		{
			EventoServidorTCP evento;
			evento.socketID = *itr;

			const std::string & base64 = convertStringToBase64 (jpegData);
			servidor.ServidorWebSockets::procesaMsgSokEnv (evento, base64);
		}
	}
	static ServidorWeb & GetMainServidorWeb()
	{
		static ServidorWeb servidor;
		return servidor;
	}
	static void mainServidorWeb (const WThread &)
	{
		mainServidorWeb();
	}
	static void mainServidorWeb()
	{
		ServidorWeb & servidor = GetMainServidorWeb();
		if (false == servidor.open (PUERTO_SERVIDOR_PRUEBA))
		{
			// std::cerr << "[ERROR] No se pudo abrir el puerto TCP " << PUERTO_SERVIDOR_PRUEBA << ": " << std::endl;
			// std::cerr << "[ERROR] ::: " << servidor.getLastErrorStr() << " (" << servidor.getLastError() << ")" << std::endl;
			// std::cerr << "[ERROR] ::: " << servidor.getLastErrorSocketStr() << " (" << servidor.getLastErrorSocket() << ")" << std::endl;
			return;
		}
		servidor.start();
	}
};

#endif // _EWADRONES_SERVER_HPP_
