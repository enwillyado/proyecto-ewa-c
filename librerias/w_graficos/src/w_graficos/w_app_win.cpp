/*********************************************************************************************
 * Name			: w_app_win.cpp
 * Description	: Implementaci�n de los m�todos dependientes de la plataforma WINDOWS
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifdef WIN32
#include "w_graficos/w_app.h"

#include <windows.h>
#include <windowsx.h>
#include <map>

#include <iostream>

// ----------------------------------------------------------------------------
// Clase interna
class WApp::WAppIntern
{
public:
	WAppIntern()
		: hWnd (NULL), eventosMouse (EventoMouse::END_VALUE)
	{
		this->init();
	};
	static WApp* loadWApp (const HWND & hWnd)
	{
		return WApp::WAppIntern::mapa[hWnd];
	};
	static WApp* loadWApp (const WAppIntern & data)
	{
		return WApp::WAppIntern::mapa[data.hWnd];
	}

	void init()
	{
		this->bi.biSize = sizeof (BITMAPINFOHEADER);
		this->bi.biPlanes = 1;
		this->bi.biBitCount = 24;
		this->bi.biCompression = BI_RGB;
	}
	void registar (WApp* const wApp)
	{
		WApp::WAppIntern::mapa[wApp->data.hWnd] = wApp;
	}

	WApp* operator->()
	{
		return WAppIntern::loadWApp (this->hWnd);
	}

public:
	struct Timers
	{
		enum Origenes
		{
			PANTALLA = 1,
			MOUSE = 2,
		};
	};
	struct EventoMouse
	{
		enum Evento
		{
			BEGIN_VALUE = 0,
			MOUSE_MOVE = BEGIN_VALUE,
			MOUSE_WHEEL,
			L_BUTTON_UP,
			L_BUTTON_DOWN,
			M_BUTTON_UP,
			M_BUTTON_DOWN,
			R_BUTTON_UP,
			R_BUTTON_DOWN,
			END_VALUE,
		};
		/*const */Evento evento;
		/*const */int xPos;
		/*const */int yPos;
		/*const */int zDelta;
	};
	typedef std::vector<EventoMouse> EventosMouse;
	EventosMouse eventosMouse;

public:

	BITMAPINFOHEADER bi;

	HWND hWnd;
	typedef std::map<HWND, WApp*> Mapa;
	static Mapa mapa;

public:
	static LRESULT CALLBACK WndProc (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
};
WApp::WAppIntern::Mapa WApp::WAppIntern::mapa;

// ----------------------------------------------------------------------------
// Constructores
WApp::WApp (const std::string & iName, const size_t ancho, const size_t alto)
	: WMultiControl (iName),
	  data (*new WApp::WAppIntern), fondo (ancho, alto), mouse ("Mouse")
{
	this->init (ancho, alto);
}
WApp::~WApp()
{
	this->destructor();
	delete &data;
}

// ----------------------------------------------------------------------------
// M�todos propios
int WApp::initInternal()
{
	// Iniciar ventana
	HINSTANCE hInstance = (HINSTANCE)GetModuleHandle (NULL);

	WNDCLASSEX wc;
	wc.cbSize = sizeof (wc);
	wc.style = 0;
	wc.lpfnWndProc = WApp::WAppIntern::WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = NULL;
	wc.hIconSm = wc.hIcon;
	wc.hCursor = NULL;
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = ("Main Class");

	if (RegisterClassEx (&wc) == 0)
	{
		MessageBox (NULL, "No se pudo registrar la ventana", "Error", MB_OK);
		return -1;
	}
	this->data.hWnd = CreateWindow (
						  wc.lpszClassName,
						  "Sample Program",
						  WS_POPUP /*WS_OVERLAPPEDWINDOW*/,
						  0,				0,
						  this->getAncho(),	this->getAlto(),
						  NULL,				NULL,
						  hInstance,		NULL
					  );

	if (this->data.hWnd == NULL)
	{
		MessageBox (NULL, "No se pudo crear la ventana", "Error", MB_OK);
		return -2;
	}

	// Register
	this->data.registar (this);

	// Show
	ShowWindow (this->data.hWnd, SW_SHOW);
	UpdateWindow (this->data.hWnd);

	return 0;
}

bool WApp::start()
{
	// Forzar repintado
	this->pintar();

	MSG msg;
	while (1)
	{
		BOOL ret = GetMessage (&msg, NULL, 0, 0);
		if (ret == 0 || ret == -1)
		{
			break;
		}
		else
		{
			DispatchMessage (&msg);
		}
	}
	return true;
}

LRESULT CALLBACK WApp::WAppIntern::WndProc (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	WApp* wApp = WApp::WAppIntern::loadWApp (hWnd);
	switch (msg)
	{
	case WM_DROPFILES:
	{
		return 0;
	}
	case WM_CREATE:
	{
		DragAcceptFiles (hWnd, true);
		SetTimer (hWnd,					// handle to main window
				  Timers::PANTALLA,		// timer identifier
				  (1000 * 1) / 30,		// 1/30-second interval
				  (TIMERPROC) NULL);	// no timer callback

		SetTimer (hWnd,					// handle to main window
				  Timers::MOUSE,		// timer identifier
				  (1000 * 1) / 50,		// 1/50-second interval
				  (TIMERPROC) NULL);	// no timer callback

		return 0;
	}
	case WM_ACTIVATE:
	{
		if (WA_ACTIVE == LOWORD (wParam) || WA_CLICKACTIVE == LOWORD (wParam))
		{
			SetCapture (hWnd);
			wApp->onFocusIn();
		}
		else
		{
			ReleaseCapture();
			wApp->onFocusOut();
		}
		return 0;
	}

	case WM_PAINT:
	{
		wApp->pintarInternal();
		return 0;
	}

	case WM_TIMER:
	{
		// Procesar eventos pendientes
		switch (wParam)
		{
		case Timers::MOUSE:
		{
			bool alguno = false;
			{
				EventoMouse & ev = wApp->data.eventosMouse[EventoMouse::MOUSE_MOVE];
				if (ev.evento != EventoMouse::END_VALUE)
				{
					wApp->setMouse (ev.xPos, ev.yPos);
					ev.evento = EventoMouse::END_VALUE;
					alguno = true;
				}
			}
			{
				EventoMouse & ev = wApp->data.eventosMouse[EventoMouse::MOUSE_WHEEL];
				if (ev.evento != EventoMouse::END_VALUE)
				{
					wApp->onWheelMouseOver (ev.zDelta >= 0 ? WEventoMouseWheel::ARRIBA : WEventoMouseWheel::ABAJO);
					ev.evento = EventoMouse::END_VALUE;
					alguno = true;
				}
			}
			if (alguno == true)
			{
				wApp->solicitarPintar();
			}
			break;
		}
		case Timers::PANTALLA:
		{
			wApp->solicitarPintar();
			break;
		}
		}
		return 0;
	}

	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
	{
		// Ajustarlas
		const bool teclaYaPresionada = (0 != (lParam & (1 << 30)));
		if (false == teclaYaPresionada)
		{
			// Solo presionar la primera vez
			wApp->onKeyDown (wParam);
		}
		return 0;
	}
	case WM_KEYUP:
	case WM_SYSKEYUP:
	{
		wApp->onKeyUp (wParam);
		return 0;
	}

	// Eventos de rat�n
	case WM_MOUSEMOVE:
	case WM_MOUSEWHEEL:
	case WM_LBUTTONUP:
	case WM_LBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_MBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_RBUTTONDOWN:
	{
		// Y, lanzar los eventos de rat�n....
		switch (msg)
		{
		case WM_MOUSEMOVE:
		{
			// Obtener las coordenadas
			const int xPos = GET_X_LPARAM (lParam);
			const int yPos = GET_Y_LPARAM (lParam);

			// Ajustarlas
			EventoMouse evento = {EventoMouse::MOUSE_MOVE, xPos, yPos, 0};
			wApp->data.eventosMouse[EventoMouse::MOUSE_MOVE] = evento;
		}
		break;

		case WM_MOUSEWHEEL:
		{
			int zDelta = GET_WHEEL_DELTA_WPARAM (wParam);

			// Ajustarlas
			EventoMouse evento = {EventoMouse::MOUSE_WHEEL, 0, 0, zDelta};
			wApp->data.eventosMouse[evento.evento] = evento;
		}
		break;

		case WM_RBUTTONUP:
		{
			wApp->onRUpMouseOver();
		}
		break;

		case WM_RBUTTONDOWN:
		{
			wApp->onRDownMouseOver();
		}
		break;

		case WM_MBUTTONUP:
		{
			wApp->onMUpMouseOver();
		}
		break;

		case WM_MBUTTONDOWN:
		{
			wApp->onMDownMouseOver();
		}
		break;

		case WM_LBUTTONUP:
		{
			wApp->onLUpMouseOver();
		}
		break;

		case WM_LBUTTONDOWN:
		{
			wApp->onLDownMouseOver();
		}
		break;
		}

		wApp->pintar();
		return 0;
	}

	case WM_DESTROY:
	{
		wApp->stop();
		return 0;
	}
	case WM_APPCOMMAND:
	{
		int cmd  = GET_APPCOMMAND_LPARAM (lParam);
		int uDevice = GET_DEVICE_LPARAM (lParam);
		int dwKeys = GET_KEYSTATE_LPARAM (lParam);
		return DefWindowProc (hWnd, msg, wParam, lParam);
	}
	}

	return DefWindowProc (hWnd, msg, wParam, lParam);
}

bool WApp::stop()
{
	PostQuitMessage (0);
	return true;
}

bool WApp::setCursorInternal()
{
	ShowCursor (FALSE);	//< ocultar el cursor del S.O.
	return true;
}

bool WApp::rePosAndDimInternal()
{
	HWND & hWnd = this->data.hWnd;
	::MoveWindow (hWnd, this->getPosX(), this->getPosY(), this->getAncho(), this->getAlto(), true);
	return true;
}

void WApp::pintar()
{
	this->WMultiControl::solicitadoPintar = true;
}
void WApp::solicitarPintar()
{
	if (this->WMultiControl::solicitadoPintar == true)
	{
		//this->WMultiControl::solicitadoPintar = false;
		// TODO: lo que hay que hacer es invalidar
		InvalidateRect (this->data.hWnd, NULL, false);
	}
}

bool WApp::pintarInternal()
{
	const WLienzo & lienzoFinal = this->recomponerLienzo();
	const byte* const pBits = lienzoFinal.getPixeles();

	// Empezar a pintar
	HWND & hWnd = this->data.hWnd;
	PAINTSTRUCT paintStruct;
	HDC hdc = BeginPaint (hWnd, &paintStruct);
#ifdef PINTAR_LENTO
	{
		size_t p = 0;
		for (size_t i = 0; i < lienzoFinal.getAncho(); i++)
		{
			for (size_t j = 0; j < lienzoFinal.getAlto(); j++)
			{
				BYTE B = pBits[p++];
				BYTE G = pBits[p++];
				BYTE R = pBits[p++];
				SetPixelV (hdc, i, j, RGB (R, G, B));
			}
		}
	}
#else
	HBITMAP hbitmap = CreateCompatibleBitmap (hdc, lienzoFinal.getAncho(), lienzoFinal.getAlto());
	if (hbitmap == NULL)
	{
		MessageBox (NULL, "Bitmap not created", "Error", MB_OK);
		return 0;
	}

	// Fill the BITMAPINFOHEADER structure
	this->data.bi.biWidth = lienzoFinal.getAncho();
	this->data.bi.biHeight = lienzoFinal.getAlto();
	SetDIBits (hdc, hbitmap, 0, lienzoFinal.getAlto(), pBits, (BITMAPINFO*)&this->data.bi, DIB_RGB_COLORS);

	// Enviar lienzo
	HDC hmemdc = CreateCompatibleDC (hdc);
	SelectObject (hmemdc, hbitmap);
	BitBlt (hdc, 0, 0, lienzoFinal.getAncho(), lienzoFinal.getAlto(), hmemdc, 0, 0, SRCCOPY);

	// Liberar recursos
	DeleteDC (hmemdc);
	DeleteObject (hbitmap);
#endif

	EndPaint (hWnd, &paintStruct);

	return 0;
}

#endif
