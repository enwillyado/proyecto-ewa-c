/*********************************************************************************************
*	Name		: strToVector.hpp
*	Description	: M�todo de conversi�n de string a vector con delimitadores
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _STR_TO_VECTOR_HPP_
#define _STR_TO_VECTOR_HPP_

#include "w_base/std_types.hpp"

static strvector strToVector (const std::string & s, const char delimitador = '\0')
{
	bool hayTmp = true;
	strvector ret;
	std::string tmp;
	for (size_t i = 0; i < s.size(); i++)
	{
		const char c = s[i];
		if (c == delimitador)
		{
			ret.push_back (tmp);
			tmp = "";
		}
		else if (delimitador == '\0')
		{
			tmp.push_back (c);
			ret.push_back (tmp);
			tmp = "";
			hayTmp = false;
		}
		else
		{
			tmp.push_back (c);
		}
	}
	if (true == hayTmp)
	{
		ret.push_back (tmp);
	}
	return ret;
}

#endif	//_STR_TO_VECTOR_HPP_
