/*********************************************************************************************
 *	Name		: algoritms.c
 *	Description	: Algoritmos comunes a todas las encriptaciones / calculos de hash
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "algoritmos.h"

/**
 *	Transforma un buffer binario a char en hexadecimal
 *	\param [in] bufferIn		Buffer binario a transformar
 *	\param [out] bufferOut		Cadena de caracteres de salida. Incluye el \0 al final
 *	\param [in] sizeBufferIn	Tamaño del buffer de entrada
**/
void binToChar (const void* bufferVoidIn, char* bufferOut, unsigned int sizeBufferIn)
{
	static const char hexValue[] = "0123456789ABCDEF";
	const unsigned char* bufferIn = (const unsigned char*)bufferVoidIn;
	unsigned int i = 0;
	for (i = 0; i < sizeBufferIn; i++)
	{
		* bufferOut = hexValue[ bufferIn[i] >> 4 ];
		bufferOut++;
		* bufferOut = hexValue[ bufferIn[i] & 0x0F ];
		bufferOut++;
	}
	* bufferOut = 0;
}
