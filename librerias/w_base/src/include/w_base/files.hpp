/*********************************************************************************************
*	Name		: files.hpp
*	Description	: M�todos para ficheros
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _FILES_HPP_
#define _FILES_HPP_

#include "w_base/fromFile.hpp"
#include "w_base/toFile.hpp"

#endif	//_FILES_HPP_
