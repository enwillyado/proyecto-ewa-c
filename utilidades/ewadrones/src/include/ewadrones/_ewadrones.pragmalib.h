/*********************************************************************************************
 *	Name		: _ewadrones.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar con la librer�a de EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_PRAGMALIB_H_
#define _EWADRONES_PRAGMALIB_H_

#ifdef _MSC_VER

#include "w_base/_pragmalib_base.h"

// Parte privada
#pragma comment(lib, "ewadrones" FIN_LIB)

#pragma comment(lib, "avcodec" SIMPLE_END_LIB)
#pragma comment(lib, "avformat" SIMPLE_END_LIB)
#pragma comment(lib, "avutil" SIMPLE_END_LIB)
#pragma comment(lib, "swscale" SIMPLE_END_LIB)
//#pragma comment(lib, "swresample" SIMPLE_END_LIB)

//#pragma comment(lib, "avdevice" SIMPLE_END_LIB)
//#pragma comment(lib, "avfilter" SIMPLE_END_LIB)
//#pragma comment(lib, "postproc" SIMPLE_END_LIB)

// Dependencias
#include "w_sistema/_w_sistema_thread.pragmalib.h"
#include "w_sistema/_w_sistema_time.pragmalib.h"
#include "w_graficos/_w_imagenes.pragmalib.h"
#include "w_red/_w_red_client.pragmalib.h"
#include "w_arbol/_w_arbolxml.pragmalib.h"

#endif

#undef _EWADRONES_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _EWADRONES_PRAGMALIB_H_
