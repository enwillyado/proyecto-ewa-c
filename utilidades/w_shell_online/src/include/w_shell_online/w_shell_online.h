/*********************************************************************************************
 * Name			: w_shell_online.h
 * Description	: ShellOnline
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SHELL_ONLINE_H_
#define _W_SHELL_ONLINE_H_

#include "w_web/w_web_srvtcp.h"

// ---------------------------------------------------------------------------
// Servicios para el ShellOnline
class ShellOnline : public ServidorWebTCP
{
public:
	ShellOnline();
private:
};

#endif	//_W_SHELL_ONLINE_H_
