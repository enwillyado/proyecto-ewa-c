/*********************************************************************************************
 * Name			: w_controlScrollable.cpp
 * Description	: Implementación de los desencadenadores de ratón para controles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_controlScrollable.h"

#include "w_graficos/w_eventoScroll.h"
#include "w_graficos/w_control.h"

#include <cstring>		//< NULL

WControlScrollable::WControlScrollable()
	: cbOnScrollUp (NULL),	cbOnScrollDown (NULL)
{
}

// ---------------------------------------------------------------------------
// onScrollUp
void WControlScrollable::onScrollUp()
{
	if (this->cbOnScrollUp != NULL)
	{
		WEventoScroll evento (this->getSelf());
		this->cbOnScrollUp (evento);
	}
}

void WControlScrollable::setOnScrollUp (WControlScrollable::OnScrollUp cbOnScrollUp)
{
	this->cbOnScrollUp = cbOnScrollUp;
}

// onScrollDown
void WControlScrollable::onScrollDown()
{
	if (this->cbOnScrollDown != NULL)
	{
		WEventoScroll evento (this->getSelf());
		this->cbOnScrollDown (evento);
	}
}

void WControlScrollable::setOnScrollDown (WControlScrollable::OnScrollDown cbOnScrollDown)
{
	this->cbOnScrollDown = cbOnScrollDown;
}
