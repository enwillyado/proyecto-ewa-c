/*********************************************************************************************
 * Name			: w_web_catalogo_extendidos.h
 * Description	: �ndice que permite traducir Servicios Extendidos
 *					en Servicios supuestamente disponibles en un Mapa de Servicios
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_CATALOGO_EXTENDIDOS_H_
#define _W_RED_CATALOGO_EXTENDIDOS_H_

#include <string>

//#include <unordered_map>
#include <map>

// Clase para almacenar servicios extendidos (los que llevan "*")
class IndiceServiciosExtendidos
{
public:
	typedef std::map<std::string, IndiceServiciosExtendidos> MapaIndices;
	IndiceServiciosExtendidos();

	// Gesti�n de identificadores
	void insert (const std::string & id);
	std::string resolve (const std::string & id, const std::string & ret = "") const;
	void erase (const std::string & id);

	// Static utils
	static bool esExtendido (const std::string & id);

private:
	MapaIndices serviciosExtendidos;
};


#endif	//_W_RED_CATALOGO_EXTENDIDOS_H_
