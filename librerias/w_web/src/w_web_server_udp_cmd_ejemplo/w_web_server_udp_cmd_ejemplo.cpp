/*********************************************************************************************
*	Name		: w_web_server_udp_cmd_ejemplo.cpp
 *	Description	: Este fichero sirve para lanzar un WebServerUDP de ejemplo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_web_server_udp_cmd_ejemplo.pragmalib.h"

#include "w_web/w_web_srvudp.h"
#include <iostream>

#define PUERTO_SERVIDOR_PRUEBA	5005

int main()
{
	ServidorWebUDP servidor;
	if (false == servidor.open (PUERTO_SERVIDOR_PRUEBA))
	{
		std::cerr << "[ERROR] No se pudo abrir el puerto TCP " << PUERTO_SERVIDOR_PRUEBA << ":" << std::endl;
		std::cerr << "[ERROR] ::: " << servidor.getLastErrorStr() <<
				  " (" << servidor.getLastError() << ")" << std::endl;
		std::cerr << "[ERROR] ::: " << servidor.getLastErrorSocketStr() <<
				  " (" << servidor.getLastErrorSocket() << ")" << std::endl;
		return 1;
	}
	else
	{
		std::cout << "Escuchando por el puerto TCP " << PUERTO_SERVIDOR_PRUEBA << "..." << std::endl;
	}
	servidor.start();
	return 0;
}
