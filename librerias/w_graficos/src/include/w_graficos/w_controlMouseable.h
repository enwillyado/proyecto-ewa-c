/*********************************************************************************************
 * Name			: w_controlMouseable.h
 * Description	: Desencadenadores de rat�n para controles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_CONTROL_MOUSEABLE_H_
#define _W_CONTROL_MOUSEABLE_H_

#include "w_graficos/w_eventoMouse.h"
#include "w_graficos/w_eventoMouseDrag.h"
#include "w_graficos/w_eventoMouseWheel.h"

#include "w_graficos/w_controlBase.h"
#include "w_graficos/w_coordenada.h"

class WControlMouseable : public WControlBase
{
public:
	// Constructor
	WControlMouseable();

	// Observadores
	const WCoordenada & getPunteroPos() const;
	void setPunteroPos (const WCoordenada &);

public:
	// ---------------------------------------------------------------------------
	// onMoveMouseOver
	virtual void onMoveMouseOver();

	typedef WEventoMouse::CbEventoMouse OnMoveMouseOver;
	void setOnMoveMouseOver (OnMoveMouseOver cbOnMoveMouseOver);

	// onMoveMouseOver
	virtual void onMoveMouseExt();

	typedef WEventoMouse::CbEventoMouse OnMoveMouseExt;
	void setOnMoveMouseExt (OnMoveMouseExt cbOnMoveMouseExt);

	// onMoveMouseEnter
	virtual void onMoveMouseEnter();

	typedef WEventoMouse::CbEventoMouse OnMoveMouseEnter;
	void setOnMoveMouseEnter (OnMoveMouseEnter cbOnMoveMouseEnter);

	// onMoveMouseOut
	virtual void onMoveMouseOut();

	typedef WEventoMouse::CbEventoMouse OnMoveMouseOut;
	void setOnMoveMouseOut (OnMoveMouseOut cbOnMoveMouseOut);

	// ---------------------------------------------------------------------------
	// onLClicMouseOver
	virtual void onLClicMouseOver();

	typedef WEventoMouse::CbEventoMouse OnLClicMouseOver;
	void setOnLClicMouseOver (OnLClicMouseOver cbOnLClicMouseOver);

	// onLUpMouseOver
	virtual void onLUpMouseOver();

	typedef WEventoMouse::CbEventoMouse OnLUpMouseOver;
	void setOnLUpMouseOver (OnLUpMouseOver cbOnLUpMouseOver);

	// onLDownMouseOver
	virtual void onLDownMouseOver();

	typedef WEventoMouse::CbEventoMouse OnLDownMouseOver;
	void setOnLDownMouseOver (OnLDownMouseOver cbOnLDownMouseOver);

	// ---------------------------------------------------------------------------
	// onRClicMouseOver
	virtual void onMClicMouseOver();

	typedef WEventoMouse::CbEventoMouse OnMClicMouseOver;
	void setOnMClicMouseOver (OnMClicMouseOver cbOnMClicMouseOver);

	// onMUpMouseOver
	virtual void onMUpMouseOver();

	typedef WEventoMouse::CbEventoMouse OnMUpMouseOver;
	void setOnMUpMouseOver (OnMUpMouseOver cbOnMUpMouseOver);

	// onMDownMouseOver
	virtual void onMDownMouseOver();

	typedef WEventoMouse::CbEventoMouse OnMDownMouseOver;
	void setOnMDownMouseOver (OnMDownMouseOver cbOnMDownMouseOver);

	// ---------------------------------------------------------------------------
	// onLUpMouseOut
	virtual void onLUpMouseOut();

	typedef WEventoMouse::CbEventoMouse OnLUpMouseOut;
	void setOnLUpMouseOut (OnLUpMouseOut cbOnLUpMouseOut);

	// onMUpMouseOut
	virtual void onMUpMouseOut();

	typedef WEventoMouse::CbEventoMouse OnMUpMouseOut;
	void setOnMUpMouseOut (OnMUpMouseOut cbOnMUpMouseOut);

	// onRDownMouseOver
	virtual void onRUpMouseOut();

	typedef WEventoMouse::CbEventoMouse OnRUpMouseOut;
	void setOnRUpMouseOut (OnRUpMouseOut cbOnRUpMouseOut);

	// ---------------------------------------------------------------------------
	// onRClicMouseOver
	virtual void onRClicMouseOver();

	typedef WEventoMouse::CbEventoMouse OnRClicMouseOver;
	void setOnRClicMouseOver (OnRClicMouseOver cbOnRClicMouseOver);

	// onRUpMouseOver
	virtual void onRUpMouseOver();

	typedef WEventoMouse::CbEventoMouse OnRUpMouseOver;
	void setOnRUpMouseOver (OnRUpMouseOver cbOnRUpMouseOver);

	// onRDownMouseOver
	virtual void onRDownMouseOver();

	typedef WEventoMouse::CbEventoMouse OnRDownMouseOver;
	void setOnRDownMouseOver (OnRDownMouseOver cbOnRDownMouseOver);

	// ---------------------------------------------------------------------------
	// onWheelMouseOver
	virtual void onWheelMouseOver (const WEventoMouseWheel::Direccion);

	typedef WEventoMouseWheel::CbEventoMouseWheel OnWheelMouseOver;
	void setOnWheelMouseOver (OnWheelMouseOver cbOnWheelMouseOver);

	// ---------------------------------------------------------------------------
	// onRMouseDrag
	virtual void onRMouseDrag();

	typedef WEventoMouseDrag::CbEventoMouseDrag OnRMouseDrag;
	void setOnRMouseDrag (OnRMouseDrag cbOnRMouseDrag);

	// onMMouseDrag
	virtual void onMMouseDrag();

	typedef WEventoMouseDrag::CbEventoMouseDrag OnMMouseDrag;
	void setOnMMouseDrag (OnMMouseDrag cbOnMMouseDrag);

	// onLMouseDrag
	virtual void onLMouseDrag();

	typedef WEventoMouseDrag::CbEventoMouseDrag OnLMouseDrag;
	void setOnLMouseDrag (OnLMouseDrag cbOnLMouseDrag);

protected:
	virtual void clear();

private:
	// �rea de desencadenadores
	OnMoveMouseOver cbOnMoveMouseOver;
	OnMoveMouseExt cbOnMoveMouseExt;
	OnMoveMouseOut cbOnMoveMouseEnter;
	OnMoveMouseOut cbOnMoveMouseOut;

	OnLClicMouseOver cbOnLClicMouseOver;
	OnLUpMouseOver cbOnLUpMouseOver;
	OnLDownMouseOver cbOnLDownMouseOver;

	OnMClicMouseOver cbOnMClicMouseOver;
	OnMUpMouseOver cbOnMUpMouseOver;
	OnMDownMouseOver cbOnMDownMouseOver;

	OnRClicMouseOver cbOnRClicMouseOver;
	OnRUpMouseOver cbOnRUpMouseOver;
	OnRDownMouseOver cbOnRDownMouseOver;

	OnWheelMouseOver cbOnWheelMouseOver;

	OnRMouseDrag cbOnRMouseDrag;
	OnMMouseDrag cbOnMMouseDrag;
	OnLMouseDrag cbOnLMouseDrag;

	OnRUpMouseOut cbOnRUpMouseOut;
	OnMUpMouseOut cbOnMUpMouseOut;
	OnLUpMouseOut cbOnLUpMouseOut;

private:
	// �rea privada de datos
	WCoordenada punteroPos;

	WCoordenada lastLDragPunteroPos;
	WCoordenada lastMDragPunteroPos;
	WCoordenada lastRDragPunteroPos;
};

#endif // _W_CONTROL_MOUSEABLE_H_
