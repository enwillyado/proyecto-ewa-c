/*********************************************************************************************
 *	Name		: w_base_tester_reflectable.cpp
 *	Description	: Pruebas unitarias para probar los mecanismos de reflacción
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_base_tester.pragmalib.h"

#include "w_base/reflectable.hpp"

#include <string>

#include <assert.h>

class ClaseReflejada
{
public:
	ClaseReflejada (const char* first_name, int age, const char* street, const std::string & town)
		: first_name (first_name),
		  age (age),
		  street_name (street),
		  town (town)
	{
	}

	typedef std::string Cadena;
private:
	REFLECTABLE
	(
		(const char*) first_name,
		(int) age,
		(std::string) street_name,
		(const std::string) town,
		(Cadena) cadena
	)
};

int baseTesterReflectable()
{
	bool status = true;
	{
		ClaseReflejada p ("John", 31, "Electric Avenue", "Harrogate");
		assert (5 == Reflector::FieldCounter<ClaseReflejada>::number_of_fields);
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<5> (p);
			assert ("John" == a.get());
			assert ("first_name" == a.name());
			assert ("const char*" == a.type());
#endif
			const char* & b = Reflector::getFieldDataValue<5> (p);
			assert ("John" == b);
		}
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<4> (p);
			assert (31 == a.get());
			assert ("age" == a.name());
			assert ("int" == a.type());
#endif
			int & b = Reflector::getFieldDataValue<4> (p);
			assert (31 == b);
		}
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<3> (p);
			assert ("Electric Avenue" == a.get());
			assert ("street_name" == a.name());
			assert ("std::string" == a.type());
#endif
			std::string & b = Reflector::getFieldDataValue<3> (p);
			assert ("Electric Avenue" == b);
		}
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<2> (p);
			assert ("Harrogate" == a.get());
			assert ("town" == a.name());
			assert ("const std::string" == a.type());
#endif
			const std::string & b = Reflector::getFieldDataValue<2> (p);
			assert ("Harrogate" == b);
		}
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<1> (p);
			assert ("" == a.get());
			assert ("cadena" == a.name());
			assert ("Cadena" == a.type());
#endif
			ClaseReflejada::Cadena & b = Reflector::getFieldDataValue<1> (p);
			assert ("" == b);
		}

		//for_each_field (p, PrintNameValueVisitor());
		//assert ("John" == get_field<const char*> (p, "first_name"));
		/*
		assert (31 == get_field<int> (p, "age"));
		assert ((31 == get_field<int> (p, "age", status), true == status));
		assert (0 != get_field<int> (p, "age"));
		assert (("" == get_field<std::string> (p, "age", status), false == status));
		assert ("Electric Avenue" == get_field<std::string> (p, "street_name"));
		assert ("Harrogate" == get_field<const std::string> (p, "town"));
		*/
	}
	{
		const ClaseReflejada p ("John", 31, "Electric Avenue", "Harrogate");
		assert (5 == Reflector::FieldCounter<ClaseReflejada>::number_of_fields);
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<5> (p);
			assert ("John" == a.get());
			assert ("first_name" == a.name());
			assert ("const char*" == a.type());
#endif
			const char* const & b = Reflector::getFieldDataValue<5> (p);
			assert ("John" == b);
		}
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<4> (p);
			assert (31 == a.get());
			assert ("age" == a.name());
			assert ("int" == a.type());
#endif
			const int & b = Reflector::getFieldDataValue<4> (p);
			assert (31 == b);
		}
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<3> (p);
			assert ("Electric Avenue" == a.get());
			assert ("street_name" == a.name());
			assert ("std::string" == a.type());
#endif
			const std::string & b = Reflector::getFieldDataValue<3> (p);
			assert ("Electric Avenue" == b);
		}
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<2> (p);
			assert ("Harrogate" == a.get());
			assert ("town" == a.name());
			assert ("const std::string" == a.type());
#endif
			const std::string & b = Reflector::getFieldDataValue<2> (p);
			assert ("Harrogate" == b);
		}
		{
#ifdef WIN32
			auto a = Reflector::getFieldData<1> (p);
			assert ("" == a.get());
			assert ("cadena" == a.name());
			assert ("Cadena" == a.type());
#endif
			const ClaseReflejada::Cadena & b = Reflector::getFieldDataValue<1> (p);
			assert ("" == b);
		}

		//for_each_field (p, PrintNameValueVisitor());
		/*
		assert ("John" == get_field_const<const char* const> (p, "first_name"));
		assert (31 == get_field_const<const int> (p, "age"));
		assert ((31 == get_field_const<const int> (p, "age", status), true == status));
		assert (0 != get_field_const<const int> (p, "age"));
		assert (("" == get_field_const<const std::string> (p, "age", status), false == status));
		assert ("Electric Avenue" == get_field_const<const std::string> (p, "street_name"));
		assert ("Harrogate" == get_field_const<const std::string> (p, "town"));
		*/
	}
	return 0;
}
