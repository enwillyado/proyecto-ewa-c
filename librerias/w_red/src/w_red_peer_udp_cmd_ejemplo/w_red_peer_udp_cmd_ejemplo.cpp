/*********************************************************************************************
*	Name		: _w_red_client_cmd_ejemplo.cpp
*	Description	: Prueba de ejemplo de un Servidor UDP (1) de pares que se conectar� a trav�s
*					del protocolo STUN por NAT a otro Servidor UDP (2) de pares.
* Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#include "_w_red_peer_udp_cmd_ejemplo.pragmalib.h"

#define NOMBRE_DE_SERVIDOR_STUN_DEFECTO	"localhost"		//< donde est� el STUN server
#define PUERTO_EN_SERVIDOR_STUN_DEFECTO	"52013"
#define PUERTO_EN_LOCAL						"0"			//< aleatorio

#include <iostream>   // std::ios, std::istream, std::cout

#include "w_red/w_red_cliudp.h"				// para tener el "ClienteUDP"
#include "w_red/w_red_srvudp.h"				// para tener el "ServidorUDP"

//#define SEGUNDOS_ESPERA_CONEXION_PEERS 2	// 2 segundos suelen servir
#ifdef SEGUNDOS_ESPERA_CONEXION_PEERS
#ifdef WIN32
#include <windows.h>			//< para tener el Sleep
#define SLEEP(t)	Sleep (t * 1000);			//< se le dan mili-segundos
#else
#include <unistd.h>				//< para tener el usleep
#define SLEEP(t)	usleep (t * 1000 * 1000);	//< se le dan micro-segundos
#endif
#endif

int mainClient (int argc, char* argv[])
{
	// Arrancar el servidor TCP
	ServidorUDP servidor1;
	std::cout << "Generando un 'ServidorUDP' que actuar� como peer 1" << std::endl;
	bool opened = servidor1.open (PUERTO_EN_LOCAL);
	std::cout << "Puerto 'ServidorUDP' donde escucha 1: " << servidor1.getPort() << std::endl;
	if (opened)
	{
		const std::string & hostSTUN = (argc > 1) ? argv[1] : NOMBRE_DE_SERVIDOR_STUN_DEFECTO;
		const std::string & portSTUN = (argc > 2) ? argv[2] : PUERTO_EN_SERVIDOR_STUN_DEFECTO;
		// Enviar un mensaje
		std::string mensajeEnviarAlSTUN = "hola";
		bool correctoEnviar = servidor1.enviar (mensajeEnviarAlSTUN, hostSTUN, portSTUN);
		if (correctoEnviar)
		{
			std::cout << "OK: enviar1() al STUN" << std::endl;
#ifdef _DEBUG
			std::cout << "-----------------------------" << std::endl;
			std::cout << mensajeEnviarAlSTUN << std::endl;
			std::cout << "-----------------------------" << std::endl;
#endif
			std::string respuesta1;
			bool correctoRecibir1 = servidor1.recibir (respuesta1);
			if (correctoRecibir1)
			{
				std::cout << "OK: Mensaje recibido en 1 desde el STUN" << std::endl;
#ifdef _DEBUG
				std::cout << "-----------------------------" << std::endl;
				std::cout << respuesta1 << std::endl;
				std::cout << "-----------------------------" << std::endl;
#endif

				// ------------------------------------------------------------------------
				// Opci�n uno, a trav�s del STUN:
				do
				{
					std::string respuesta2;

					ServidorUDP servidor2;
					std::cout << "Generando un 'ServidorUDP' que actuar� como peer 2" << std::endl;
					bool opened2 = servidor2.open (0);
					{
						bool sent2 = servidor2.enviar (mensajeEnviarAlSTUN, hostSTUN, portSTUN);
						if (sent2 == false)
						{
							std::cerr << "Error: enviar2() desde el STUN " << servidor2.getLastError() << " = " << servidor2.getLastErrorStr() << std::endl;
							break;
						}
						else
						{
							std::cout << "OK: enviar2() al STUN" << std::endl;
#ifdef _DEBUG
							std::cout << "-----------------------------" << std::endl;
							std::cout << mensajeEnviarAlSTUN << std::endl;
							std::cout << "-----------------------------" << std::endl;
#endif
						}
						bool recv2 = servidor2.recibir (respuesta2);
						if (recv2 == false)
						{
							std::cerr << "Error: recibir2() desde el STUN " << servidor2.getLastError() << " = " << servidor2.getLastErrorStr() << std::endl;
							break;
						}
						else
						{

							std::cout << "OK: Mensaje recibido en 2 desde el STUN" << std::endl;
#ifdef _DEBUG
							std::cout << "-----------------------------" << std::endl;
							std::cout << respuesta2 << std::endl;
							std::cout << "-----------------------------" << std::endl;
#endif
						}
					}
					{
						// Abrir la conexi�n desde 1 a 2
						const std::string & mensaje1a2 = "hola don pepito";
						const std::string & res2 = respuesta2;
						const std::string & host2 = res2.substr (0, res2.find_first_of (":"));
						const std::string & port2 = res2.substr (res2.find_first_of (":") + 1);
						bool sent1a2 = servidor1.enviar (mensaje1a2, host2, port2);
						if (sent1a2 == false)
						{
							std::cerr << "Error: enviar1a2() " << servidor1.getLastError() << " = " << servidor1.getLastErrorStr() << std::endl;
							break;
						}
						else
						{
							std::cout << "Mensaje enviado desde 1 a 2 (a " << host2 << ":" << port2 << ")" << std::endl;
#ifdef _DEBUG
							std::cout << "-----------------------------" << std::endl;
							std::cout << mensaje1a2 << std::endl;
							std::cout << "-----------------------------" << std::endl;
#endif
						}
					}
#ifdef SEGUNDOS_ESPERA_CONEXION_PEERS
					{
						// Esperar un poco a que lleguen los mensajes
						std::cout << "Esperando un poco..." << std::endl;
						SLEEP (SEGUNDOS_ESPERA_CONEXION_PEERS);
#ifdef _DEBUG
						std::cout << "-----------------------------" << std::endl;
						std::cout << "---> " << i << std::endl;
						std::cout << "-----------------------------" << std::endl;
#endif
					}
#endif
					{
						// Confirmar la conexi�n desde 2 a 1 enviando un mensaje
						const std::string & mensaje2a1 = "hola don jose";
						const std::string & res1 = respuesta1;
						const std::string & host1 = res1.substr (0, res1.find_first_of (":"));
						const std::string & port1 = res1.substr (res1.find_first_of (":") + 1);
						bool sent2a1 = servidor2.enviar (mensaje2a1, host1, port1);
						if (sent2a1 == false)
						{
							std::cerr << "Error: enviar2a1() " << servidor2.getLastError() << " = " << servidor2.getLastErrorStr() << std::endl;
							break;
						}
						else
						{
							std::cout << "Mensaje enviado desde 2 a 1 (a " << host1 << ":" << port1 << ")" << std::endl;
#ifdef _DEBUG
							std::cout << "-----------------------------" << std::endl;
							std::cout << mensaje2a1 << std::endl;
							std::cout << "-----------------------------" << std::endl;
#endif
						}
					}

					// Y ahora, recibir el mensaje desde 2 en 1
					std::string resDe2;
					bool correctoRecibir2 = servidor1.recibir (resDe2);
					if (correctoRecibir2 == true)
					{
						std::cout << "Mensaje recibido desde 2 en 1" << std::endl;
						std::cout << "-----------------------------" << std::endl;
						std::cout << resDe2 << std::endl;
						std::cout << "-----------------------------" << std::endl;
					}
					else
					{
						std::cerr << "Error: recibir1-2() " << servidor1.getLastError() << " = " << servidor1.getLastErrorStr() << std::endl;
					}

					const bool closeCorrecto2 = servidor2.close();
					if (true == closeCorrecto2)
					{
						std::cout << "OK: closeSocket2()" << std::endl;
					}
					else
					{
						std::cerr << "Error: closeSocket2() " << servidor2.getLastError() << " = " << servidor2.getLastErrorStr() << std::endl;
						break;
					}
				}
				while (false);
			}
			else
			{
				std::cerr << "Error: recibir desde el STUN " << servidor1.getLastError() << " = " << servidor1.getLastErrorStr() << std::endl;
			}

			// ------------------------------------------------------------------------
			// Opci�n dos:
			{
				// Mensajes enviados en loopback
				const std::string & mensajeLo = "hola loopback";
				bool enviar1 = servidor1.enviar (mensajeLo, servidor1.getHost(), servidor1.getPort());
				if (enviar1 == true)
				{
					std::cout << "OK: enviar1() por loopback" << std::endl;
#ifdef _DEBUG
					std::cout << "-----------------------------" << std::endl;
					std::cout << mensajeLo << std::endl;
					std::cout << "-----------------------------" << std::endl;
#endif
					std::string res;
					bool recibir1 = servidor1.recibir (res);
					if (recibir1 == true)
					{
						std::cout << "Mensaje recibido por loopback: " << servidor1.getHost() << ":" << servidor1.getPort() << std::endl;
						std::cout << "-----------------------------" << std::endl;
						std::cout << res << std::endl;
						std::cout << "-----------------------------" << std::endl;
					}
					else
					{
						std::cerr << "Error: recibir1() " << servidor1.getLastError() << " = " << servidor1.getLastErrorStr() << std::endl;
					}
				}
				else
				{
					std::cerr << "Error: enviar1) " << servidor1.getLastError() << " = " << servidor1.getLastErrorStr() << std::endl;
				}
			}
		}
		else
		{
			std::cerr << "Error: enviar() " << servidor1.getLastError() << " = " << servidor1.getLastErrorStr() << std::endl;
		}

		// Cerrar el peer 1
		const bool closeCorrecto1 = servidor1.close();
		if (true == closeCorrecto1)
		{
			std::cout << "OK: closeSocket1()" << std::endl;
		}
		else
		{
			std::cerr << "Error: closeSocket1() " << servidor1.getLastError() << " = " << servidor1.getLastErrorStr() << std::endl;
			return 2;
		}
	}
	else
	{
		std::cerr << "Error: isOpenSocket1() " << servidor1.getLastError() << " = " << servidor1.getLastErrorStr() << std::endl;
		return 1;
	}
	return 0;
}

int main (int argc, char* argv[])
{
	int ret;
	while (true)
	{
		ret = mainClient (argc, argv);
		std::cout << "Presione una tecla para continuar; CONTROL+C para salir" << std::endl;
		std::cin.get();
	}
	return ret;
}
