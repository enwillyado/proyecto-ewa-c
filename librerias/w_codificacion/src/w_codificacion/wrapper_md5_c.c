/*********************************************************************************************
 *	Name		: wrapper_md5_c.c
 *	Description	: Wrapper de C para algoritmo MD5
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion_c.h"

#include "md5.h"
#include "algoritmos.h"

#include <memory.h>
#include <string.h>

/**
 * Calculo de un Md5 desde un buffer de memoria
 * \param [in] pBuffer		Buffer a calcular el Md5
 * \param [in] sizeBuffer	Tamaño del buffer pBuffer
 * \param [out] md5Out		Valor Sha1 del buffer
 **/
void md5CreateFromBuffer (const void* pBuffer, unsigned int sizeBuffer, MD5 md5Out)
{
	md5_state_t context;
	md5_init (&context);
	md5_append (&context, (md5_byte_t*)pBuffer, sizeBuffer);
	md5_finish (&context, md5Out);
}

/**
 * Inicia el calculo de un MD5 de un buffer que entregaremos de varias veces
 * \param [in] pContext		Objeto que mantiene informacion interna del calculo
 **/
void md5CreateFromTrunkBufferInit (Md5Context* pContext)
{
	md5_init ((md5_state_t*) pContext);
}

/**
 * Añade un trozo nuevo de buffer al calculo de un MD5 de un otro buffer mayor que entregamos
 * de varias veces
 * \param [in] pContext		Objeto que mantiene informacion del calculo
 * \param [in] pBuffer		Nuevo buffer a añadir al calculo total
 * \param [in] sizeBuffer	Tamaño del buffer
 **/
void md5CreateFromTrunkBufferUpdate (Md5Context* pContext, const void* pBuffer, unsigned int sizeBuffer)
{
	md5_append ((md5_state_t*)pContext, (md5_byte_t*)pBuffer, sizeBuffer);
}

/**
 * Finaliza y obtiene el calculo de un MD5 desde un buffer que hemos entregado de varias veces
 * \param [in] pContext		Objeto que mantiene informacion del calculo
 * \param [out] MD5Out		Resultado del calculo realizado
 **/
void md5CreateFromTrunkBufferFinish (Md5Context* pContext, MD5 md5Out)
{
	md5_finish ((md5_state_t*)pContext, md5Out);
}

/**
 * Calculo de SHA desde un string
 * \param [in] pStr		Cadena de caracteres a calcular
 * \param [in] Md5Out	Valor Md5 de la cadena
 **/
void md5CreateFromString (const char* pStr, MD5 md5Out)
{
	md5CreateFromBuffer (pStr, (unsigned int)strlen (pStr), md5Out);
}

/**
 * Calcula el MD5 de un buffer y se compara con un MD5 calculado previamente
 * \param [in] pBuffer			Buffer a calcular MD5
 * \param [in] sizeBuffer		Tamaño del buffer pBuffer
 * \param [in] md5ToCompare		Objeto MD5 con el que se compara el resultado
 * \return	EncryptEquals		Al buffer le corresponde el mismo SHA que el comparado
 *			EncryptDifferents	Al buffer no le corresponde el mismo SHA que el comparado
 **/
EEncryptResult md5CompareBuffer (const void* pBuffer, unsigned int sizeBuffer, const MD5 md5ToCompare)
{
	MD5 md5;
	md5CreateFromBuffer (pBuffer, sizeBuffer, md5);
	if (memcmp (md5, md5ToCompare, sizeof (MD5)) == 0)
	{
		return EncryptEquals;
	}
	else
	{
		return EncryptDifferents;
	}
}

/**
 * Calcula el MD5 de una cadena y se compara con un MD5 calculado previamente
 * \param [in] pBuffer			Buffer a calcular MD5
 * \param [in] sizeBuffer		Tamaño del buffer pBuffer
 * \param [in] md5ToCompare		Objeto MD5 con el que se compara el resultado
 * \return	EncryptEquals		Al buffer le corresponde el mismo SHA que el comparado
 *			EncryptDistinct		Al buffer no le corresponde el mismo SHA que el comparado
 **/
EEncryptResult md5CompareString (const char* pStr, MD5 md5ToCompare)
{
	MD5 md5;
	md5CreateFromString (pStr, md5);
	if (memcmp (md5, md5ToCompare, sizeof (MD5) == 0))
	{
		return EncryptEquals;
	}
	else
	{
		return EncryptDifferents;
	}
}

/**
 * Obtiene un MD5 y calcula su cadena de caracteres hexadecimales
 * \param [in] md5			Estructura MD5 a convertir en cadena hexadecimal
 * \param [in] md5strOut	Buffer donde sera dejada la cadena hexadecimal
 **/
void md5ToStr (const MD5 md5, MD5STR md5strOut)
{
	binToChar (md5, (char*)md5strOut, sizeof (MD5));
}
