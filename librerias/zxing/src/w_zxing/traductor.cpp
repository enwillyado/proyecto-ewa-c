/*
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "_zxing_cmd.pragmalib.h"

#include "traductor.h"
#include <iostream>

namespace qrviddec
{
	BufferBitmapSource::BufferBitmapSource (int inWidth, int inHeight, unsigned char* inBuffer)
	{
		width = inWidth;
		height = inHeight;
		buffer = inBuffer;
	}

	BufferBitmapSource::~BufferBitmapSource()
	{
	}

	int BufferBitmapSource::getWidth() const
	{
		return width;
	}

	int BufferBitmapSource::getHeight() const
	{
		return height;
	}

	unsigned char* BufferBitmapSource::getRow (int y, unsigned char* row)
	{
		if (y < 0 || y >= height)
		{
			fprintf (stderr, "ERROR, attempted to read row %d of a %d height image.\n", y, height);
			return NULL;
		}
		// WARNING: NO ERROR CHECKING! You will want to add some in your code.
		if (row == NULL)
		{
			row = new unsigned char[width];
		}
		for (int x = 0; x < width; x ++)
		{
			row[x] = buffer[y * width + x];
		}
		return row;
	}

	unsigned char* BufferBitmapSource::getMatrix()
	{
		return buffer;
	}
}
