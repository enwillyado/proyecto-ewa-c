/*********************************************************************************************
 * Name			: w_areaResizeControl.cpp
 * Description	: Implementaci�n de la clase para gestionar un control de redimensionado (nativo)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_areaResizeControl.h"

#include "w_graficos/w_rgba.h"
#include "w_graficos/w_app.h"

#define ANCHO_AREA_REDIMENSIONADO	15

// Constructores
WAreaRedimensinado::WAreaRedimensinado (const std::string & iName)
	: WControl (iName),
	  areaResize (NULL),
	  invertido (false), abajo (false)
{
	this->setOnLMouseDrag (&WAreaRedimensinado::arrastrar);
	this->setOnMMouseDrag (&WAreaRedimensinado::arrastrar);
	this->setOnRMouseDrag (&WAreaRedimensinado::arrastrar);
}

// Modificador
void WAreaRedimensinado::setAreaResizeControl (WAreaResizeControl* const iAreaResize)
{
	this->areaResize = iAreaResize;
}

bool WAreaRedimensinado::esApuntadoPor (const WCoordenada & coordenada) const
{
	if (
		(coordenada.posY < ANCHO_AREA_REDIMENSIONADO || this->getAlto() - coordenada.posY < ANCHO_AREA_REDIMENSIONADO)
		||
		(coordenada.posX < ANCHO_AREA_REDIMENSIONADO || this->getAncho() - coordenada.posX < ANCHO_AREA_REDIMENSIONADO)
	)
	{
		return true;
	}
	return false;
}

void WAreaRedimensinado::actualizar (const WEventoMouse & e)
{
	e.getRoot().getCursor().getLienzo().invertColor();
	e.getRoot().pintar();
}

void WAreaRedimensinado::arrastrar (const WEventoMouseDrag & e)
{
	WControl & c = e.getSelf();

	// Calcular la zona de arrastre
	const WCoordenada & punteroPos = c.getPunteroPos();
	enum Posiciones
	{
		ARRIBAIZQUIERDA,	ARRIBA,		ARRIBADERECHA,
		IZQUIERDA,			NINGUNA,	DERECHA,
		ABAJOIZQUIERDA,		ABAJO,		ABAJODERECHA,
	} posicion = NINGUNA;
	if (punteroPos.posY < ANCHO_AREA_REDIMENSIONADO)
	{
		if (punteroPos.posX < ANCHO_AREA_REDIMENSIONADO)
		{
			posicion = ARRIBAIZQUIERDA;
		}
		else if (c.getAncho() - punteroPos.posX < ANCHO_AREA_REDIMENSIONADO)
		{
			posicion = ARRIBADERECHA;
		}
		else
		{
			posicion = ARRIBA;
		}
	}
	else if (c.getAlto() - punteroPos.posY < ANCHO_AREA_REDIMENSIONADO)
	{
		if (punteroPos.posX < ANCHO_AREA_REDIMENSIONADO)
		{
			posicion = ABAJOIZQUIERDA;
		}
		else if (c.getAncho() - punteroPos.posX < ANCHO_AREA_REDIMENSIONADO)
		{
			posicion = ABAJODERECHA;
		}
		else
		{
			posicion = ABAJO;
		}
	}
	else
	{
		if (punteroPos.posX < ANCHO_AREA_REDIMENSIONADO)
		{
			posicion = IZQUIERDA;
		}
		else if (c.getAncho() - punteroPos.posX < ANCHO_AREA_REDIMENSIONADO)
		{
			posicion = DERECHA;
		}
		else
		{
			posicion = NINGUNA;
		}
	}

	// Proceder al redimensionado
	const size_t incrementoX = e.getIncrementoPosX();
	const size_t incrementoY = e.getIncrementoPosY();
	const size_t nuevoAnchoMenos = c.getAncho() - (signed)incrementoX;
	const size_t nuevoAltoMenos = c.getAlto() - (signed)incrementoY;
	const size_t nuevoAnchoMas = c.getAncho() + (signed)incrementoX;
	const size_t nuevoAltoMas = c.getAlto() + (signed)incrementoY;
	if ((signed)nuevoAnchoMas > ANCHO_AREA_REDIMENSIONADO * 2 && (signed)nuevoAltoMas > ANCHO_AREA_REDIMENSIONADO * 2)
	{
		switch (posicion)
		{
		case ARRIBAIZQUIERDA:
			c.getParent().resize (nuevoAnchoMenos, nuevoAltoMenos);
			c.getParent().setPosX (c.getParent().getPosX() + (signed)incrementoX);
			c.getParent().setPosY (c.getParent().getPosY() + (signed)incrementoY);
			break;
		case ARRIBADERECHA:
			c.getParent().resize (nuevoAnchoMas, nuevoAltoMenos);
			c.getParent().setPosY (c.getParent().getPosY() + (signed)incrementoY);
			break;
		case ARRIBA:
			c.getParent().resize (c.getAncho(), nuevoAltoMenos);
			c.getParent().setPosY (c.getParent().getPosY() + (signed)incrementoY);
			break;

		case IZQUIERDA:
			c.getParent().resize (nuevoAnchoMenos, c.getAlto());
			c.getParent().setPosX (c.getParent().getPosX() + (signed)incrementoX);
			break;
		case DERECHA:
			c.getParent().resize (nuevoAnchoMas, c.getAlto());
			break;

		case ABAJO:
			c.getParent().resize (c.getAncho(), nuevoAltoMas);
			break;
		case ABAJOIZQUIERDA:
			c.getParent().resize (nuevoAnchoMenos, nuevoAltoMas);
			c.getParent().setPosX (c.getParent().getPosX() + (signed)incrementoX);
			c.getParent().setPosY (c.getParent().getPosY() + (signed)incrementoY);
			break;
		case ABAJODERECHA:
		default:
			c.getParent().resize (nuevoAnchoMas, nuevoAltoMas);
		}
	}
	else
	{
		c.getParent().resize (ANCHO_AREA_REDIMENSIONADO * 2, ANCHO_AREA_REDIMENSIONADO * 2);
	}
	e.getRoot().pintar();
}

// ----------------------------------------------------------------------------
// Constructores
WAreaResizeControl::WAreaResizeControl (const std::string & iName)
	: WMultiControl (iName),
	  areaRedimiensionado ("AreaRedimiensionado")
{
	this->init();
}
WAreaResizeControl::WAreaResizeControl (WAreaResizeControl & org)
	: WMultiControl (org),
	  areaRedimiensionado (org.areaRedimiensionado)
{
	// Copiar el elementos si hay
	WControlesPtr & orgControles = org.getControlesPtr();
	if (orgControles.size() > 1)
	{
		this->WMultiControl::insertarPtr (orgControles[0].ptr, orgControles[0].deletable);
		if (orgControles[0].deletable == true)
		{
			orgControles[0].ptr = NULL;
		}
	}

	// E inicializar
	this->init();
}

// Inicializador
void WAreaResizeControl::init()
{
	this->areaRedimiensionado.setAreaResizeControl (this);
	// A�adir el �rea de redimensionado en la posici�n "1"
	this->WMultiControl::insertarPtr (&this->areaRedimiensionado, false, 1);
}

// Modificadores
size_t WAreaResizeControl::insertarPtr (WControl* const control, const bool deletable)
{
	// Ajustar el tama�o al del control a�adido
	this->resize (control->getAncho(), control->getAlto());

	// Y finalmente, a�adir dicho en la posici�n "0"
	return this->WMultiControl::insertarPtr (control, deletable, 0);
}
void WAreaResizeControl::resize (const size_t ancho, const size_t alto)
{
	this->areaRedimiensionado.getLienzo().reserve (ancho, alto);
	this->WMultiControl::resize (ancho, alto);
}

WLienzo & WAreaResizeControl::recomponerLienzo()
{
	// Obtener lienzo b�sico
	WLienzo & lienzoFinal = this->WControl::getLienzo();

	// Obtener la uni�n del control redimensionados (posici�n 0)
	if (this->WMultiControl::controles.size() > 0)
	{
		WControl* const controlPtr = this->controles[0].ptr;
		if (controlPtr != NULL)
		{
			// Actualizar el lienzo al redimensionado
			WControl & control = *controlPtr;
			const WLienzo & lienzoControl = control.recomponerLienzo();
			lienzoFinal = lienzoControl.resize (lienzoFinal.getAncho(), lienzoFinal.getAlto());
		}
	}
	return lienzoFinal;
}

// Manejadores
void WAreaResizeControl::onRMouseDrag()
{
	const WCoordenada & mouse = this->getPunteroPos();
	const bool sobre = areaRedimiensionado.esApuntadoPor (mouse);
	if (true == sobre)
	{
		areaRedimiensionado.onRMouseDrag();
	}
	else
	{
		this->WMultiControl::onRMouseDrag();
	}
}
void WAreaResizeControl::onMMouseDrag()
{
	const WCoordenada & mouse = this->getPunteroPos();
	const bool sobre = areaRedimiensionado.esApuntadoPor (mouse);
	if (true == sobre)
	{
		areaRedimiensionado.onMMouseDrag();
	}
	else
	{
		this->WMultiControl::onMMouseDrag();
	}
}
void WAreaResizeControl::onLMouseDrag()
{
	const WCoordenada & mouse = this->getPunteroPos();
	const bool sobre = areaRedimiensionado.esApuntadoPor (mouse);
	if (true == sobre)
	{
		areaRedimiensionado.onLMouseDrag();
	}
	else
	{
		this->WMultiControl::onLMouseDrag();
	}
}