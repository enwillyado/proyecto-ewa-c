/*********************************************************************************************
 *	Name		: wrapper_sha1.c
 *	Description	: Wrapper de C para algoritmo SHA1
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion_c.h"

#include "sha1.h"
#include "algoritmos.h"

#include <memory.h>

/**
 * Calculo de un SHA1 desde un buffer de memoria
 * \param [in] pBuffer		Buffer a calcular el SHA1
 * \param [in] sizeBuffer	Tamaño del buffer pBuffer
 * \param [out] sha1Out		Valor Sha1 del buffer
 **/
void sha1CreateFromBuffer (const void* pBuffer, unsigned int sizeBuffer, SHA1 sha1Out)
{
	sha1_ctx_t context;
	sha1_init (&context);
	sha1_update (&context, (char*)pBuffer, sizeBuffer);
	sha1_final (sha1Out, &context);
}

/**
 * Inicia el calculo de un SHA1 de un buffer que entregaremos de varias veces
 * \param [in] pContext		Objeto que mantiene informacion interna del calculo
 **/
void sha1CreateFromTrunkBufferInit (Sha1Context* pContext)
{
	sha1_init ((sha1_ctx_t*) pContext);
}

/**
 * Añade un trozo nuevo de buffer al calculo de un SHA1 de un otro buffer mayor que entregamos
 * de varias veces
 * \param [in] pContext		Objeto que mantiene informacion del calculo
 * \param [in] pBuffer		Nuevo buffer a añadir al calculo total
 * \param [in] sizeBuffer	Tamaño del buffer
 **/
void sha1CreateFromTrunkBufferAdd (Sha1Context* pContext, const void* pBuffer, size_t sizeBuffer)
{
	sha1_update ((sha1_ctx_t*)pContext, (char*)pBuffer, (unsigned int)sizeBuffer);
}

/**
 * Finaliza y obtiene el calculo de un SHA1 desde un buffer que hemos entregado de varias veces
 * \param [in] pContext		Objeto que mantiene informacion del calculo
 * \param [out] sha1Out		Resultado del calculo realizado
 **/
void sha1CreateFromTrunkBufferFinish (Sha1Context* pContext, SHA1 sha1Out)
{
	sha1_final (sha1Out, (sha1_ctx_t*)pContext);
}

/**
 * Calculo de Sha desde un string
 * \param [in] pStr			Cadena de caracteres a calcular
 * \param [in] sha1Out		Valor Sha1 de la cadena
 **/
void sha1CreateFromString (const char* pStr, SHA1 sha1Out)
{
	sha1_string (sha1Out, pStr);
}

/**
 * Calcula el Sha1 de un buffer y se compara con un SHA1 calculado previamente
 * \param [in] pBuffer			Buffer a calcular SHA1
 * \param [in] sizeBuffer		Tamaño del buffer pBuffer
 * \param [in] sha1ToCompare	Objeto Sha1 con el que se compara el resultado
 * \return	EncryptEqual		Al buffer le corresponde el mismo SHA que el comparado
 *			EncryptDifferents	Al buffer no le corresponde el mismo SHA que el comparado
 **/
EEncryptResult sha1CompareBuffer (const void* pBuffer, size_t sizeBuffer, const SHA1 sha1ToCompare)
{
	SHA1 sha1;
	sha1CreateFromBuffer (pBuffer, (unsigned int)sizeBuffer, sha1);
	if (memcmp (sha1, sha1ToCompare, sizeof (SHA1) == 0))
	{
		return EncryptEquals;
	}
	else
	{
		return EncryptDifferents;
	}
}

/**
 * Calcula el Sha1 de una cadena y se compara con un SHA1 calculado previamente
 * \param [in] pBuffer			Buffer a calcular SHA1
 * \param [in] sizeBuffer		Tamaño del buffer pBuffer
 * \param [in] sha1ToCompare	Objeto Sha1 con el que se compara el resultado
 * \return	EncryptEqual		Al buffer le corresponde el mismo SHA que el comparado
 *			EncryptDifferents	Al buffer no le corresponde el mismo SHA que el comparado
 **/
EEncryptResult sha1CompareString (const char* pStr, SHA1 shaToCompare)
{
	if (sha1_strcmp (shaToCompare, pStr) == 0)
	{
		return EncryptEquals;
	}
	else
	{
		return EncryptDifferents;
	}
}

/**
 * Obtiene un SHA1 y calcula su cadena de caracteres
 * \param [in] sha1	 Sh1 en binario a transformar
 * \param [in] sha1strOut	 Buffer donde sera dejada la cadena
 **/
void sha1ToStr (const SHA1 sha1, SHA1STR sha1strOut)
{
	binToChar ((unsigned char*)sha1, sha1strOut, sizeof (SHA1));
}
