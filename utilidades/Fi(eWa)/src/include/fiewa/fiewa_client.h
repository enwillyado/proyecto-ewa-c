/*********************************************************************************************
 *	Name		: fiewa_client.h
 *	Description	: Cliente para conectarse con pares a trav�s de STUN
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _FIEWA_CLIENT_H_
#define _FIEWA_CLIENT_H_

#include "fiewa/fiewa_defines.hpp"	//< para tener las definiciones generales

#include "fiewa/fiewa_datos_stun.h"	//< para tener el FIEWA::DatosSTUN

#include "w_red/w_red_srvudp.h"		//< para tener el ServidorUDP
#include "w_web/w_web_mensaje.h"	//< para tener el Mensaje

#include <string>

namespace FIEWA
{
	class PeerUDP
	{
	public:
		// Constructores
		PeerUDP();
		PeerUDP (const std::string & nombre, const size_t puerto);

		// Ajustar el timeout (milisegundos)
		void setTimeout (size_t milisegundos);

		// Conectarse a un destino dado
		bool connect (const FIEWA::DatosSTUN &);
		bool connect (const std::string & nombre, const size_t puerto);

		// Enviar mensaje del protocolo reconect�ndose (cosa que permite UDP)
		bool send (const Mensaje & mensajeEnviarAlSTUN, const std::string & nombre,
				   const size_t puerto);
		bool send (const Mensaje & mensajeEnviarAlSTUN, const DatosSTUN &);
		bool send (const std::string & messageStr, const std::string & nombre, const size_t puerto);
		bool send (const std::string & messageStr, const FIEWA::DatosSTUN &);

		// Enviar mensaje del protocolo al nodo conectado
		bool send (const Mensaje & mensajeEnviarAlSTUN);
		bool send (const std::string & messageStr);

		// Recibir un mensaje
		Mensaje recibir();

		// Recibir simplemente un string
		std::string recibirStr();

		// Cerrar el socket
		bool close();

		// Observador del estado
		bool isConnected() const;

	private:
		// Conectarse a un destino conocido
		bool createServer();

	private:
		// �rea de datos
		ServidorUDP server;
		std::string remote_host;
		size_t remote_port;
		size_t timeoutinseconds;
		bool connected;
	};
}

#endif //_FIEWA_CLIENT_H_
