/*********************************************************************************************
 * Name			: w_lienzo_map.cpp
 * Description	: Implementación de la clase para manipular una representación gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_lienzo_map.h"

#include "w_graficos/w_control.h"
#include "w_graficos/w_rgba.h"

#include <map>

class WLienzoMap::WLienzoIntern
{
public:
	typedef std::map<size_t, RGBA> ColumnaRGBA;
	typedef std::map<size_t, ColumnaRGBA> MatrizRGBA;
public:
	WLienzoIntern()
		: datos()
	{
	}
	MatrizRGBA datos;
};

// ----------------------------------------------------------------------------
// Constructores
WLienzoMap::WLienzoMap()
	: data (*new WLienzoIntern)
{
	this->ancho = 0;
	this->alto = 0;
}
WLienzoMap::WLienzoMap (const WLienzoBase & lienzo)
	: data (*new WLienzoIntern)
{
	this->operator= (lienzo);
}

void WLienzoMap::operator= (const WLienzoBase & lienzo)
{
	if (this->ancho * this->alto != lienzo.getAncho() * lienzo.getAlto())
	{
		this->data.datos.clear();
	}
	this->ancho = lienzo.getAncho();
	this->alto = lienzo.getAlto();
	WLienzoBase::operator+= (lienzo);
}

WLienzoMap::~WLienzoMap()
{
	delete &this->data;
}

// ----------------------------------------------------------------------------
// Accesores
RGBA WLienzoMap::getPixel (const size_t x, const size_t y) const
{
	if (this->ancho <= x || this->alto <= y)
	{
		return RGBA();
	}
	return this->getPixelFast (x, y);
}
bool WLienzoMap::setPixel (const size_t x, const size_t y, const RGBA & rgba)
{
	if (this->ancho <= x)
	{
		this->ancho = x + 1;
	}
	if (this->alto <= y)
	{
		this->alto = y + 1;
	}
	if (rgba.a > 0)
	{
		this->setPixelFast (x, y, rgba);
	}
	return true;
}
void WLienzoMap::setPixelFast (const size_t x, const size_t y, const RGBA & rgba)
{
	this->data.datos[x][y] = rgba;
}
RGBA WLienzoMap::getPixelFast (const size_t x, const size_t y) const
{
	return this->data.datos[x][y];
}

// ----------------------------------------------------------------------------
// Modificadores
void WLienzoMap::resize (const size_t ancho, const size_t alto)
{
	this->ancho = ancho;
	this->alto = alto;
	this->reserveIntern();
}
void WLienzoMap::reserveIntern()
{
	this->data.datos.clear();
}

// ----------------------------------------------------------------------------
// Observadores
size_t WLienzoMap::getAncho() const
{
	return this->ancho;
}
size_t WLienzoMap::getAlto() const
{
	return this->alto;
}
