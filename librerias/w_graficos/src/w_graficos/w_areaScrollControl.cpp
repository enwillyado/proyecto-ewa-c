/*********************************************************************************************
 * Name			: w_areaScrollControl.cpp
 * Description	: Implementación de la clase para gestionar un control de scroll vertical (nativo)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_areaScrollControl.h"

#include "w_graficos/w_app.h"

// Constructores
WAreaScrollControl::WAreaScrollControl (const std::string & iName)
	: WMultiControl (iName),
	  vScroll ("vScroll")
{
	this->init();

	// Suscribirse eventos
	class Auxiliar
	{
	public:
		static void onScrollUp (const WEventoScroll & e)
		{
			WAreaScrollControl* ptr = static_cast<WAreaScrollControl*> (e.getSelf().getParentPtr());
			WControl* contenido = ptr->obtenerPtr (0);
			if (contenido != NULL)
			{
				signed posY = contenido->getPosY();
				if (posY < -10)
				{
					contenido->setPosY (posY + 10);
				}
				else if (posY < 0)
				{
					contenido->setPosY (0);
				}
			}
			ptr->recomponerLienzo();
			e.getRoot().pintar();
		}
		static void onScrollDown (const WEventoScroll & e)
		{
			WAreaScrollControl* ptr = static_cast<WAreaScrollControl*> (e.getSelf().getParentPtr());
			WControl* contenido = ptr->obtenerPtr (0);
			if (contenido != NULL)
			{
				signed maxY = contenido->getAlto() - ptr->getAlto();
				signed posY = contenido->getPosY();
				if (posY - 10 > -maxY)
				{
					contenido->setPosY (posY - 10);
				}
				else if (posY > -maxY)
				{
					contenido->setPosY (-maxY);
				}
			}
			ptr->recomponerLienzo();
			e.getRoot().pintar();
		}
	};
	this->vScroll.setOnScrollDown (&Auxiliar::onScrollDown);
	this->vScroll.setOnScrollUp (&Auxiliar::onScrollUp);
}
WAreaScrollControl::WAreaScrollControl (WAreaScrollControl & org)
	: WMultiControl (org),
	  vScroll (org.vScroll)
{
	WControlesPtr & orgControles = org.getControlesPtr();
	if (orgControles.size() > 0)
	{
		this->WMultiControl::insertarPtr (orgControles[0].ptr, orgControles[0].deletable);
		if (orgControles[0].deletable == true)
		{
			orgControles[0].ptr = NULL;
		}
	}
	this->init();
}

// Inicializador
void WAreaScrollControl::init()
{
	if (this->WMultiControl::controles.size() == 0)
	{
		this->WMultiControl::insertarPtr (NULL, false);
	}
	this->WMultiControl::insertarPtr (&this->vScroll, false);
	this->vScroll.resize (10, this->getAlto());
}

// Modificadores
void WAreaScrollControl::resize (const size_t ancho, const size_t alto)
{
	// Reposicionar
	this->WMultiControl::resize (ancho, alto);
	this->vScroll.resize (this->vScroll.getAncho(), alto);
	this->vScroll.setPosX (this->getAncho() - this->vScroll.getAncho());
	this->recomponerLienzo();
}

size_t WAreaScrollControl::insertarPtr (WControl* const control, const bool deletable)
{
	return this->WMultiControl::insertarPtr (control, deletable, 0);
}

WLienzo & WAreaScrollControl::recomponerLienzo()
{
	WLienzo & lienzoFinal = WMultiControl::recomponerLienzo();

	// Unirle los componentes
	lienzoFinal.unir (this->vScroll);

	return lienzoFinal;
}
