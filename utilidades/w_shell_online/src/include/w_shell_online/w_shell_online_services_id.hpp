/*********************************************************************************************
 * Name			: w_shell_online_services_id.hpp
 * Description	: Identificador de los Servicios para el ShellOnline
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SHELL_ONLINE_SERCICES_ID_H_
#define _W_SHELL_ONLINE_SERCICES_ID_H_

// ----------------------------------------------------------------------------
// Identificador de los Servicios para el ShellOnline
#define SHELL_ONLINE_SERCICES_ID		"/shell_online"

// ----------------------------------------------------------------------------
// Identificador del Servicio de Prueba Interactiva
#define SRVSERVICE_PRUEBA_INTERACTIVA_CON_HILOS_ID	"/"

#endif	//_W_SHELL_ONLINE_SERCICES_ID_H_
