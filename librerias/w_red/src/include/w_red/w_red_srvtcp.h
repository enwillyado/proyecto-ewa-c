/*********************************************************************************************
 *	Name		: w_red_srvtcp.h
 *	Description	: Servidor TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRVTCP_H_
#define _W_RED_SRVTCP_H_

#define NUMERO_MAXIMO_CLIENTES 10	// n�mero m�ximo de clientes CONECTADOS (que no procesando) en el Servidor

// Definiciones para la gesti�n del socket
#define DESCRIPTOR_NULO 	-1		// valor que indica que un descriptor (socket, file...) es nulo

#include <string>

#include "w_red/w_red_srvevent.h"
#include "w_red/w_red_srvsocket.h"

#ifdef _DEBUG
#include "w_base/asserts.hpp"
#define ASSERT_IN_DEBUG ::assertIfNull
#else
#define ASSERT_IN_DEBUG
#endif

class ServidorTCP
{
	friend class ServerSocket;		// El ServerSocket es amiga (para acceder a nuestra zona privada)
	void setConectado();
	void setEscuchando();
	void setProcesando();
	void setDesconectando();
	void setDesconectado();
public:
	enum Errores
	{
		CONECTADO = 0,
		CONECTANDO,
		ESCUCHANDO,
		PROCESANDO,
		DESCONECTADO,
		DESCONECTANDO,
		ERRONEO,
	};

	// ASK: �M�s constructores?
	ServidorTCP();

	// Estos son los miembros p�blicos de un servidor
	template <class T>
	bool run (const T & puerto)
	{
		if (this->open (puerto) == false)
		{
			// la llamada a 'open' ya ajusta un error en caso de fallo
			return false;
		}
		return this->start(); // si se sale, es que algo malo pas�
	}

	// Observadores
	const std::string & getHost();
	unsigned int getPort();

	// Control de errores:
	Errores getLastError() const;
	std::string getLastErrorStr() const;
	ServerSocket::Errores getLastErrorSocket() const;
	std::string getLastErrorSocketStr() const;

	// FIN DE LA FACADE.
	// --------------------------------------------------------------------------

public:	// Para que se puedan probar en los test

	// L�gica del servidor:
	bool open (const std::string & port);
	bool open (unsigned int port);
	bool start();
	bool send (const EventoServidorTCP &, const std::string & mensaje);
	bool send (const SocketID, const std::string & mensaje);
	bool close (const EventoServidorTCP &);
	bool close();

public:
	bool setCallBack (void (*) (ServidorTCP &), void* callBackObj = NULL);
	void* getCallBackObjPtr() const;

	template<class T>
	T & getCallBackObj() const
	{
		return *ASSERT_IN_DEBUG (static_cast<T*> (getCallBackObjPtr()));
	}

	EventoServidorTCP getEvento();
	size_t getNumEventosPendintes();

	// Static Utils
	static std::string toStr (Errores);

protected:
	ColaEventosServidorTCP colaEventos;
	void (*eventCallBack) (ServidorTCP &);
	void* callBackObj;

public:
	virtual void analizaEvento (const EventoServidorTCP &);
	virtual void analizaEventoDef (const EventoServidorTCP &);
	virtual bool analizaEventoConexion (const EventoServidorTCP &);
	virtual bool analizaEventoDesconexion (const EventoServidorTCP &);
	virtual bool analizaEventoMsgRecv (const EventoServidorTCP &);

protected:
	// Control de errores:
	Errores lastError;

protected:
	// �rea de datos protegida
	std::string host;
	unsigned int port;

private:
	// �rea de datos privada (porque en ella se guardan los datos para comunicarse
	//					  con las capas inferiores de la implementaci�n del socket)
	int sockfd;
	ServerSocket serverSocket;
};

#endif	//_W_RED_SRVTCP_H_
