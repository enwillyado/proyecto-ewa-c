/*********************************************************************************************
 *	Name		: w_base_tester.cpp
 *	Description	: Este fichero sirve para configurar las pruebas unitarias que se realizarán
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_base_tester.pragmalib.h"

int baseTesterSmart();
int baseTesterStrings();
int baseTesterFiles();
int baseTesterMacros();
int baseTesterReflectable();

// ----------------------------------------------------------------------------
// Maintester
int main()
{
	baseTesterSmart();
	baseTesterStrings();
	baseTesterFiles();
	baseTesterMacros();
	return baseTesterReflectable();
}
