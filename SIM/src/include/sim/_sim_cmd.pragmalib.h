/*********************************************************************************************
 *	Name		: _sim_cmd.pragmalib.h
 *	Description	: Header file with unified libraries to link
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _SIM_CMD_PRAGMALIB_H_
#define _SIM_CMD_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#pragma message("Enlazando con las librer�as del proyecto 'sim_cmd'...")

#include "sim/_sim.pragmalib.h"

#endif

#endif // _SIM_CMD_PRAGMALIB_H_
