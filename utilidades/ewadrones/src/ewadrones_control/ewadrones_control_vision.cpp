/*********************************************************************************************
 *	Name		: ewadrones_control_vision.cpp
 *	Description	: Implementaci�n la clase auxiliar para ejecutar el reconocimiento de im�genes
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones_control_globales.h"
#include "ewadrones_control_vision.h"

#include "w_graficos/w_lienzo.h"
#include "w_graficos/w_xlienzo.h"
#include "w_graficos/w_rgba.h"

#include "w_graficos/w_app.h"

#include "w_sistema/w_sistema_time.h"

#include "zxing/w_zxing.h"		//< c�digos QR

#ifdef _DEBUG
#include <iostream>
#endif

// M�todos para tratar los fotogramas recibidos
bool Video::pixelRojo (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
{
	const bool rMg = (color.r - 0x40 > color.g);
	const bool rMb = (color.r - 0x40 > color.b);
	const bool gLb = (color.g < 0x60 && color.b < 0x60);
	if (rMg && rMb && gLb)
	{
		color.r = 0xFF;
		color.g = color.b = 0x00;
	}
	else
	{
		color.g = color.b = color.r = 0x00;
	}
	return true;
}
bool Video::pixelMascara (WLienzoBase &, RGBA & color, const size_t x, const size_t y)
{
	/*
	RGBA colorFondo = lienzo.getPixel (x, y);
	const RGBA & colorLienzo = lienzoMascara.getPixel (x, y);
	if (colorLienzo.r == 0xFF && colorLienzo.g == colorLienzo.b && colorLienzo.g == 0x00)
	{
		lienzoMascara.setPixel (x, y, colorFondo);
	}
	else
	{
		colorFondo.brightness (0x80);
		lienzoMascara.setPixel (x, y, colorFondo);
	}
	*/
	return false;
}
bool Video::calcularQR (WLienzoBase & lienzo)
{
	const WZxingResult result = WZxing::procesar (lienzo);
	if (result.getFormato() != WZxingResult::FORMATO_DESCONOCIDO)
	{
		// Coordenadas
#ifdef _DEBUG
		std::cout << "Coordenadas:" << std::endl;
#endif
		const WZxingResult::WCoordenadas & coordenadas = result.getCoordenadas();
		for (WZxingResult::WCoordenadas::const_iterator itr = coordenadas.begin();
				itr != coordenadas.end(); itr++)
		{
			const WCoordenada & coordenada = *itr;
#ifdef _DEBUG
			std::cout << " " << coordenada.posX << " x " << coordenada.posY << std::endl;
#endif
			lienzo.unir (WXLienzo::getWXLienzoFromFile ("../testdata/cursor.xlz"),
						 coordenada.posX, coordenada.posY);
		}

#ifdef _DEBUG
		// Texto
		std::cout << "TEXTO:" << std::endl;
		std::cout << " " << result.getTexto() << std::endl;
		std::cout.flush();
#endif

		const std::string & texto = result.getTexto();
		::GetArDrone().getXML().procesar (texto);
	}

	return true;
}

template<const int N>
WControl* & ObtenerAppControl (const std::string & nombre)
{
	static WControl* control = NULL;
	control = ::GetAPP()->obtenerPtr (nombre, true);
	return control;
}

void Video::onNewLienzo (const WLienzo & newLienzo)
{
	static WControl* & controlVideoOriginal = ::ObtenerAppControl<0> ("videoOrg");
	static WControl* & controlVideoTratado = ::ObtenerAppControl<1> ("videoTratado");

#ifdef DIRECTO
	controlVideoOriginal->setLienzo (newLienzo);
	controlVideoOriginal->pintar();
#else
	const WLienzo & lienzo = newLienzo.resize (controlVideoOriginal->getAncho(), controlVideoOriginal->getAlto());
	WLienzo lienzoMascara = lienzo;
	//lienzoMascara.forEachPixel (Video::pixelRojo);		//< sacar pixeles rojos
	//lienzoMascara.forEachPixel (Video::pixelMascara);		//< resaltar sobre el fondo

	Video::calcularQR (lienzoMascara);						//< sacar c�digo QR

	controlVideoOriginal->setLienzo (lienzo);
	controlVideoOriginal->pintar();
	controlVideoTratado->setLienzo (lienzoMascara);
	controlVideoTratado->pintar();
#endif

	//WTime::esperarMilisegundos (1000 * 1 / 30);
}
