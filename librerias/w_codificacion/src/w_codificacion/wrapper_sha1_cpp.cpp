/*********************************************************************************************
 *	Name		: wrapper_sha1_cpp.cpp
 *	Description	: Wrapper de C++ para algoritmo de SHA1 soportando los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion.h"

/**
 *	Codifica en SHA1 la cadena dada
 *	\param [in] value 	una cadena a codificar
 *	\retval		la cadena codificada en md5
**/
std::string sha1Str (const std::string & value)
{
	SHA1 sha1Hash;
	::sha1CreateFromBuffer (value.c_str(), value.size(), sha1Hash);
	SHA1STR sha1Str;
	::sha1ToStr (sha1Hash, sha1Str);
	std::string retstr;
	retstr.resize (sizeof (SHA1STR) - 1);
	for (size_t i = 0; i < retstr.size(); i++)
	{
		retstr[i] = sha1Str[i];
	}
	return retstr;
}
