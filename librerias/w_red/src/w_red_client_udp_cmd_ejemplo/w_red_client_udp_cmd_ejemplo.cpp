/*********************************************************************************************
 *	Name		: _w_red_client_cmd_ejemplo.cpp
 *	Description	: Prueba de ejemplo de un Cliente UDP por a una direcci�n dada (o una por
 *					defecto) y un puerto dado (o uno por defecto)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_red_client_udp_cmd_ejemplo.pragmalib.h"

#define NOMBRE_DE_SERVIDOR_DEFECTO "localhost"
#define PUERTO_EN_SERVIDOR_DEFECTO "52013"

#include <iostream>   // std::ios, std::istream, std::cout

#include "w_red/w_red_cliudp.h"			// para tener el "ClienteUDP"

int mainClient (int argc, char* argv[])
{
	// Arrancar el servidor TCP
	ClienteUDP cliente;
#ifdef _DEBUG
	std::cout << "Generando un 'ClienteUDP'" << std::endl;
#endif
	const std::string & host = (argc > 1) ? argv[1] : NOMBRE_DE_SERVIDOR_DEFECTO;
	const std::string & port = (argc > 2) ? argv[2] : PUERTO_EN_SERVIDOR_DEFECTO;
	bool opened = cliente.openSocket (host, port);
	if (opened && true == cliente.isOpenSocket())
	{
		const std::string & mensajeEnviar = "hola";
		std::string respuesta;
		bool correctoEnviar = cliente.enviar_recibir (mensajeEnviar, respuesta);
		if (correctoEnviar)
		{
#ifdef _DEBUG
			std::cout << "OK: enviar_recibir()" << std::endl;
#endif
			std::cout << "Mensaje recibido" << std::endl;
#ifdef _DEBUG
			std::cout << "-----------------------------" << std::endl;
			std::cout << respuesta << std::endl;
			std::cout << "-----------------------------" << std::endl;
#endif
		}
		else
		{
			std::cerr << "Error: enviar_recibir() " << cliente.getLastError() << " = " << cliente.getLastErrorStr() << std::endl;
		}

		bool closeCorrecto = cliente.closeSocket();
		if (true == closeCorrecto)
		{
#ifdef _DEBUG
			std::cout << "OK: closeSocket()" << std::endl;
#endif
		}
		else
		{
			std::cerr << "Error: closeSocket() " << cliente.getLastError() << " = " << cliente.getLastErrorStr() << std::endl;
			return 2;
		}
	}
	else
	{
		std::cerr << "Error: isOpenSocket() " << cliente.getLastError() << " = " << cliente.getLastErrorStr() << std::endl;
		return 1;
	}
	return 0;
}

int main (int argc, char* argv[])
{
	int ret;
	while (true)
	{
		ret = ::mainClient (argc, argv);
		std::cout << "Presione una tecla para continuar; CONTROL+C para salir" << std::endl;
		std::cin.get();
	}
	return ret;
}
