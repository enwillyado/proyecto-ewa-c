/*********************************************************************************************
 *	Name		: ewadrones_jumping_sumo_control_globales.cpp
 *	Description	: Implementación de las variables globales para el control del JUMPING SUMO
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones_control_globales.h"

EWadrones & GetArDrone()
{
	static EWadrones arDrone;
	return arDrone;
}
JumpingSumoControlador & GetJumpingSumo()
{
	static JumpingSumoControlador jumpingSumoControlador;
	return jumpingSumoControlador;
}
WApp* & GetAPP()
{
	static WApp* wApp;
	return wApp;
}
