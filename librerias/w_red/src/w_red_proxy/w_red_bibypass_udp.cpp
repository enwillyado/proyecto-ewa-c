/*********************************************************************************************
 *	Name		: w_red_bypass_udp.cpp
 *	Description	: Implementación de la clase para implementar un by-pass de red basado en TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_bibypass_udp.h"

// ----------------------------------------------------------------------------
// Constructor
BiByPassUDP::BiByPassUDP (const std::string & iRemoteHost, const int iRemotePort, const int localPort)
	: ByPassUDP (localPort)
{
	this->clienteRemoteUDP.openSocket (iRemoteHost, iRemotePort);
}

// ----------------------------------------------------------------------------
// Métodos de by-pas
void BiByPassUDP::onRemoteGetData (const EventoServidorUDP & evento, const std::string & mensaje)
{
	// Este queda igual
	ByPassUDP::onRemoteGetData (evento, mensaje);
}
void BiByPassUDP::onByPassPutData (const EventoServidorUDP & evento, const std::string & mensaje)
{
	// ByPass --send-> Cliente
	if (this->clienteRemoteUDP.isOpenSocket() == true)
	{
		this->clienteRemoteUDP.enviar (mensaje);
	}
}
