/*********************************************************************************************
 * Name			: w_controlFocusable.cpp
 * Description	: Implementación de los desencadenadores de ratón para controles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_controlKeyable.h"

#include "w_graficos/w_eventoKey.h"
#include "w_graficos/w_control.h"
#include "w_graficos/w_app.h"

#include <cstring>

WControlFocusable::WControlFocusable()
	: cbsOnFocusIn(), cbsOnFocusOut()
{
}

// ---------------------------------------------------------------------------
// OnFocusIn
void WControlFocusable::onFocusIn()
{
	if (this->cbsOnFocusIn.size() > 0)
	{
		WEvento evento (this->getSelf());
		for (size_t i = 0; i < this->cbsOnFocusIn.size(); i++)
		{
			if (this->cbsOnFocusIn[i] != NULL)
			{
				this->cbsOnFocusIn[i] (evento);
			}
		}
	}
}

size_t WControlFocusable::setOnFocusIn (WControlFocusable::OnFocusIn cbOnFocusIn)
{
	if (cbOnFocusIn != NULL)
	{
		this->cbsOnFocusIn.push_back (cbOnFocusIn);
		return this->cbsOnFocusIn.size() - 1;
	}
	this->cbsOnFocusIn.clear();
	return -1;
}

bool WControlFocusable::unsetOnFocusIn (const size_t index)
{
	if (index >= this->cbsOnFocusIn.size())
	{
		return false;
	}
	this->cbsOnFocusIn[index] = NULL;
	return true;
}

// ---------------------------------------------------------------------------
// onFocusOut
void WControlFocusable::onFocusOut()
{
	if (this->cbsOnFocusOut.size() > 0)
	{
		WEvento evento (this->getSelf());
		for (size_t i = 0; i < this->cbsOnFocusOut.size(); i++)
		{
			if (this->cbsOnFocusOut[i] != NULL)
			{
				this->cbsOnFocusOut[i] (evento);
			}
		}
	}
}

size_t WControlFocusable::setOnFocusOut (WControlFocusable::OnFocusOut cbOnFocusOut)
{
	if (cbOnFocusOut != NULL)
	{
		this->cbsOnFocusOut.push_back (cbOnFocusOut);
		return this->cbsOnFocusOut.size() - 1;
	}
	this->cbsOnFocusOut.clear();
	return -1;
}

bool WControlFocusable::unsetOnFocusOut (const size_t index)
{
	if (index >= this->cbsOnFocusOut.size())
	{
		return false;
	}
	this->cbsOnFocusOut[index] = NULL;
	return true;
}
