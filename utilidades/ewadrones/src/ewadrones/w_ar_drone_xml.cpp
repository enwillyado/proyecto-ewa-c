/*********************************************************************************************
 *	Name		: w_ar_drone_xml.cpp
 *	Description	: Implementanción de la clase para el procesar XML del AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones/w_ar_drone_xml.h"

#include "ewadrones/ewadrones.h"

#include "w_arbol/w_arbolxml.h"
#include "w_base/strTransform.hpp"

ArDroneXML::ArDroneXML (EWadrones* ptrArDrone)
	: arDrone (ptrArDrone)
{
}

void ArDroneXML::setPtrArDrone (EWadrones* ptrArDrone)
{
	this->arDrone = ptrArDrone;
}

void ArDroneXML::procesar (const std::string & xml)
{
	// Procesar la cadena a arbol
	const ArbolXML & arbol = ArbolXML::getArbolFromSTR (xml);
	if (arbol.getLastError() != ArbolXML::SIN_ERROR)
	{
		return;
	}

	// Procesar la cadena ya como arbol
	return this->procesar (arbol);
}

void ArDroneXML::procesar (const ArbolXML & arbol)
{
	// Sacar el elemento principal
	const std::string & elementoOrg = arbol.getClave();
	const std::string & elemento = ::strToUpper (elementoOrg);

	// Procesar el XML directamente
	if (elemento == "ATERRIZAR")
	{
		this->arDrone->getControlador().aterrizar();
	}
	else if (elemento == "DESPEGAR")
	{
		this->arDrone->getControlador().despegar();
	}
}
