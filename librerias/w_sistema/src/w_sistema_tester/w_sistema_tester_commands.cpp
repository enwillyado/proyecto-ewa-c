/*********************************************************************************************
 * Name			: w_sistema_tester_commands.cpp
 * Description	: Pruebas unitarias que se realizarán sobre commands
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema_execs.h"

#include <assert.h>

#include <iostream>

// ----------------------------------------------------------------------------
// Execute Commands
void f (const std::string & s)
{
	if (s != "")
	{
		std::cout << "+";
		bool salto = false;
		for (size_t i = 0; i < s.size(); i++)
		{
			const char c = s[i];
			if (c >= 0x20 && c <= 0x7E)
			{
				std::cout << c;
				salto = false;
			}
			else if (c == '\n')
			{
				if (salto == false)
				{
					salto = true;
					std::cout << std::endl;
				}
			}
		}
		if (salto == false)
		{
			std::cout << std::endl;
		}
	}
}

int executeCommandSimple()
{
	std::cout << "Probando 'SistemaExecsSimple'..." << std::endl;
	SistemaExecsSimple sistemaExecsSimple;
#ifdef WIN32
	sistemaExecsSimple.setComando ("echo|set /p=hola");
#else
	sistemaExecsSimple.setComando ("echo -n hola");
#endif
	sistemaExecsSimple.exeComando();

	const std::string & salida = sistemaExecsSimple.getSalidaComando();
	assert (salida == "hola"); // cadena mostrada por el 'echo'

	const int codigoSalida = sistemaExecsSimple.getLastExecStatus();
#ifdef WIN32
	assert (codigoSalida == 1); // en WIN32, devuelve '1'
#else
	assert (codigoSalida == 0); // en LINUX, devuelve '0'
#endif

	std::cout << "Fin prueba 'SistemaExecsSimple'." << std::endl;
	return 0;
}
int executeCommands()
{
	SistemaExecsBi sistemaExecsBi;
#ifdef WIN32
	sistemaExecsBi.setComando ("cmd");
#else
	sistemaExecsBi.setComando ("ssh -T willy@localhost");
#endif
	sistemaExecsBi.funcionChild = f;
	sistemaExecsBi.exeComando();

	int i = 0;
	while (sistemaExecsBi.getLastError() == SistemaExecsBase::SIN_ERROR)
	{
		std::cout << "-----------------" << std::endl;
		std::cout << "(" << ++i << "):" << std::endl;
		const std::string & salida = sistemaExecsBi.getPartialSalidaComando();
		if (salida != "")
		{
			f (salida);
		}

		std::cout << ":::::::::::::::::" << std::endl;
		switch (i)
		{
		case 1:
#ifdef WIN32
			sistemaExecsBi.putEntrada ("exit\n");
			break;
#else
			sistemaExecsBi.putEntrada ("pwd\n");
			break;

		case 2:
			sistemaExecsBi.putEntrada ("ls\n");
			break;

		case 3:
			sistemaExecsBi.putEntrada ("gdb\n");
			break;

		case 4:
			sistemaExecsBi.putEntrada ("help\n");
			break;

		case 5:
			sistemaExecsBi.putEntrada ("q\n");
			sistemaExecsBi.putEntrada ("exit\n");
			break;
#endif
		default:
			break;
		}
	}

	return 0;
}
