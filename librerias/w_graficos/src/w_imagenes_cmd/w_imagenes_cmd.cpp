/*********************************************************************************************
 *	Name		: w_imagenes_cmd.cpp
 *	Description	: Este fichero sirve para ver en funcionamiento
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_imagenes_cmd.pragmalib.h"

#include "w_graficos/w_app.h"
#include "w_graficos/w_xlienzo.h"
#include "w_graficos/w_lienzo_bmp.h"
#include "w_graficos/w_rgba.h"

#define ANCHO_APP	800
#define ALTO_APP	600

// Evitar que salga la consola
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

WLienzo fon;
int main()
{
	// ---------------------------------------------------------------------------
	// Iniciar lienzo del fondo
	WLienzoBMP fondo;
	{
		const bool cargado = fondo.leerContenidoFile ("../testdata/imagen_prueba.bmp");
		if (false == cargado)
		{
			(WLienzo &)fondo = WLienzo::createWLienzo (ANCHO_APP, ALTO_APP, RGBA (0, 0, 0xFF));
		}

		// Y ponerle borde
		fondo.setBorder (RGBA (0xFF, 0xFF, 0xFF));
	}


	{
		// Tratar imagen de fondo
		class Lienzo
		{
		public:
			static bool pixelVecino (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
			{
				if (x > 1 && y > 1)
				{
					RGBA color3 = lienzo.getPixel (x, y);
					const RGBA colores[6] = {lienzo.getPixel (x - 1, y), lienzo.getPixel (x, y),
											 lienzo.getPixel (x - 1, y - 1), lienzo.getPixel (x, y - 1),
											 lienzo.getPixel (x, y), lienzo.getPixel (x - 1, y)
											};
					color3.r = (colores[0].r + colores[1].r + colores[2].r + colores[3].r +
								colores[4].r + colores[5].r) / 6;
					lienzo.setPixel (x, y, color3);
				}
				return false;
			}
			static bool pixelRojo (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
			{
				const bool rMg = (color.r - 0x40 > color.g);
				const bool rMb = (color.r - 0x40 > color.b);
				const bool gLb = (color.g < 0x60 && color.b < 0x60);
				if (rMg && rMb && gLb)
				{
					color.r = 0xFF;
					color.g = color.b = 0x00;
				}
				else
				{
					color.g = color.b = color.r = 0x00;
				}
				return true;
			}
			static bool pixelMascara (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
			{
				const RGBA & colorLienzo = lienzo.getPixel (x, y);
				if (colorLienzo.r == 0xFF && colorLienzo.g == colorLienzo.b && colorLienzo.g == 0x00)
				{
					const RGBA & colorFondo = fon.getPixel (x, y);
					lienzo.setPixel (x, y, colorFondo);
					return true;
				}
				return false;
			}
		};

		fon = fondo;
		for (size_t i = 0; i < 5; i++)
		{
			fondo.forEachPixel (Lienzo::pixelRojo);
			fondo.forEachPixel (Lienzo::pixelVecino);
		}
		fondo.forEachPixel (Lienzo::pixelRojo);

		fondo.forEachPixel (Lienzo::pixelMascara);
	}


	// Crear la aplicación
	WApp wApp ("app", ANCHO_APP, ALTO_APP);
	wApp.setFondo (fondo);
	{
		// --------------------------------------------------------------------------
		// Eventos propios de la aplicación
		class CosasAPP
		{
		public:
			static void onKeyUp (const WEventoKey & evento)
			{
				if (evento.key == 27)
				{
					evento.getRoot().stop();
				}
			}
		};
		// Añadirlos a la app
		wApp.setOnKeyUp (CosasAPP::onKeyUp);
	}

	// Iniciar la aplicación
	wApp.start();

	// Finalización correcta
	return 0;
}
