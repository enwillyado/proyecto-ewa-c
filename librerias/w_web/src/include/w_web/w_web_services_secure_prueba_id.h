/*********************************************************************************************
 * Name			: w_web_services_secure_prueba_id.h
 * Description	: Identificador de los ServiciosThread de prueba
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRV_SERVICES_SECURE_PRUEBA_ID_H_
#define _W_RED_SRV_SERVICES_SECURE_PRUEBA_ID_H_

// ----------------------------------------------------------------------------
// Identificador del Servicio de Prueba
#define SRVSERVICE_SECURE_PRUEBA_ID					"/pruebaSecure"

// ----------------------------------------------------------------------------
// Identificador del Servicio de Prueba Interactiva
#define SRVSERVICE_SECURE_PRUEBA_ID_ALTERNATIVA		"/pruebaSecureAlt"

#endif	//_W_RED_SRV_SERVICES_SECURE_PRUEBA_ID_H_
