/*********************************************************************************************
 *	Name		: wrapper_base64_codi.cpp
 *	Description	: Wrapper de C++ para algoritmo de Base64 que soporta los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion.h"

#include <string.h>		// strlen

//____________________________________________________________________________________________
//
// Codifica
//____________________________________________________________________________________________

/**
 * Codifica en base64 un buffer binario y lo deja en un string
 * \param [in] bufferIn			Buffer binario a codificar
 * \param [in] szBufferIn		Tamaño del buffer a codificar
 * \param [out] stringB64Out	Cadena de caracteres destino para la codificación en base64
 * \return	EncryptOk			Codificación Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64Encode (const void* bufferIn, size_t szBufferIn, std::string & stringB64Out)
{
	stringB64Out.clear();
	stringB64Out.resize (Base64CalculateNumCharsEncoded (szBufferIn));
	// Hacemos un resize del numero de letras esperado, pero internamente ya tiene el +1 para el \0
	return base64EncodeFromBuffer (bufferIn, szBufferIn, (char*)stringB64Out.c_str(), stringB64Out.size() + 1);
}

/**
 * Codifica en base64 un buffer binario y lo deja en un buffer binario. No incluye el \0
 * \param [in] bufferIn			Buffer binario a codificar
 * \param [in] szBufferIn		Tamaño del buffer a codificar
 * \param [out] bufferB64Out	Buffer destino para la codificación en base64
 * \param [in] szBufferB64Out	Tamaño del buffer destino para la codificación en base64
 * \return	EncryptOk			Codificación Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64Encode (const void* bufferIn, size_t szBufferIn, char* bufferB64Out, size_t szBufferB64Out)
{
	return base64EncodeFromBuffer (bufferIn, szBufferIn, bufferB64Out, szBufferB64Out);
}

/**
 * Codifica en base64 un buffer binario que esta almacenado en un vector de las STL y lo deja
 * en un string
 * \param [in] bufferIn			Buffer binario a codificar
 * \param [out] stringB64Out	Cadena de caracteres destino para la codificación en base64
 * \return	EncryptOk			Codificación Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64Encode (const std::vector<unsigned char> & bufferIn, std::string & stringB64Out)
{
	return base64Encode ((const void*) (&bufferIn[0]), bufferIn.size(), stringB64Out);
}

/**
 * Codifica en base64 un buffer binario que esta almacenado en un vector de las STL y lo deja
 * en un buffer binario. No incluye el \0
 * \param [in] bufferIn			Buffer binario a codificar
 * \param [out] bufferB64Out	Buffer destino para la codificación en base64
 * \param [in] szBufferB64Out	Tamaño del buffer destino para la codificación en base64
 * \return	EncryptOk			Codificación Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64Encode (const std::vector<unsigned char> & bufferIn, char* bufferB64Out, size_t szBufferB64)
{
	return base64EncodeFromBuffer ((const void*) (&bufferIn[0]), bufferIn.size(), bufferB64Out, szBufferB64);
}

/**
 * Codifica en base64 una cadena de caracteres almacenada en un string de las STL y lo deja
 * en otro string
 * \param [in]	stringIn		Cadena a codificar
 * \param [out] stringB64Out	Cadena de caracteres destino para la codificación en base64
 * \return	EncryptOk			Codificación Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64Encode (const std::string & stringIn, std::string & stringB64Out)
{
	return base64Encode (stringIn.c_str(), stringIn.size(), stringB64Out);
}

/**
 * Codifica en base64 una cadena de caracteres almacenada en un string de las STL y lo deja en
 * un buffer binario. No incluye el \0
 * \param [in] stringIn			Cadena a codificar
 * \param [out] bufferB64Out	Buffer destino para la codificación en base64
 * \param [in] szBufferB64Out	Tamaño del buffer destino para la codificación en base64
 * \return	EncryptOk			Codificación Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64Encode (const std::string & stringIn, char* bufferB64Out, size_t szBufferB64)
{
	return base64EncodeFromBuffer (stringIn.c_str(), stringIn.size(), bufferB64Out, szBufferB64);
}

/**
 * Codifica en base64 una cadena de caracteres y lo deja en un string de las STL
 * \param [in] stringIn			Cadena a codificar
 * \param [out] stringB64Out	Cadena de caracteres destino para la codificación en base64
 * \return	EncryptOk			Codificación Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64Encode (const char* pStringIn, std::string & stringB64Out)
{
	stringB64Out.clear();
	stringB64Out.resize (Base64CalculateNumCharsEncoded (strlen (pStringIn)));
	return base64EncodeFromString (pStringIn, (char*)stringB64Out.c_str(), stringB64Out.size() + 1);
}

/**
 * Codifica en base64 una cadena de caracteres y lo deja en un buffer binario. No incluye el \0
 * \param [in] stringIn			Cadena a codificar
 * \param [out] bufferB64Out	Buffer destino para la codificación en base64
 * \param [in] szBufferB64Out	Tamaño del buffer destino para la codificación en base64
 * \return	EncryptOk			Codificación Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64Encode (const char* pStringIn, char* bufferB64Out, size_t szBufferB64Out)
{
	return base64EncodeFromString (pStringIn, bufferB64Out, szBufferB64Out);
}

/**
 * Devuelve la representacion en base 64 de la cadena dada
 * \param [in] srcStrB64	Cadena a codificar
 * \return	Cadena resultante o vacía si falla
 **/
std::string convertStringToBase64 (const std::string & srcStr)
{
	std::string ret;
	if (EncryptOk == base64Encode (srcStr, ret))
	{
		// A veces, un 5% de los calculos de longitud pueden fallar...
		return ret.c_str();
	}
	return "";
}
