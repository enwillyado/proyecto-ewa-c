/*********************************************************************************************
 *	Name		: _w_sistema_execs.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_EXECS_PRAGMALIB_H_
#define _W_SISTEMA_EXECS_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_base/_pragmalib_base.h"

// Propia librer�a: (para no autoengancharla si es una dll)
#pragma comment(lib, "w_sistema_execs" FIN_LIB)

#endif

#undef _W_SISTEMA_EXECS_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_SISTEMA_EXECS_PRAGMALIB_H_
