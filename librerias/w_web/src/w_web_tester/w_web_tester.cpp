/*********************************************************************************************
*	Name		: w_web_tester.cpp
 *	Description	: Este fichero sirve para configurar las pruebas unitarias que se realizarán
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_web_tester.pragmalib.h"

#include <assert.h>
#include "w_web/w_web_srvtcp.h"
#include "w_web/w_web_clitcp.h"

#define NOMBRE_SERVIDOR_PRUEBA	"localhost"
#define PUERTO_SERVIDOR_PRUEBA	5005

int main()
{
#ifdef PROBAR_SERVER
	ServidorWebTCP servidor;
	assert (true == servidor.open (PUERTO_SERVIDOR_PRUEBA));
	servidor.start();
#else
	ClienteTCP cliente;
	assert (true == cliente.openSocket ("echo.websocket.org", "80"));

	{
		std::string mensaje;
		mensaje += "GET ws://echo.websocket.org/?encoding=text HTTP/1.1\r\n\
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Maxthon/4.4.3.4000 Chrome/30.0.1599.101 Safari/537.36\r\n\
Pragma: no-cache\r\n\
Cache-Control: no-cache\r\n\
Origin: http://www.websocket.org\r\n\
Connection: Upgrade\r\n\
Sec-WebSocket-Version: 13\r\n\
Host: echo.websocket.org\r\n\
Sec-WebSocket-Key: 6uE6zFi4Na1FR6BpGH6jdA==\r\n\
Sec-WebSocket-Extensions: x-webkit-deflate-frame\r\n\
Upgrade: websocket\r\n\
\r\n";
		assert (true == cliente.enviar (mensaje));

		std::string respuesta;
		cliente.recibir (respuesta, 10);

		std::string s;
		s.push_back (0x81);
		s.push_back (0x82);
		s.push_back (0x00);
		s.push_back (0x00);
		s.push_back (0x00);
		s.push_back (0x00);
		s.push_back ('A');
		s.push_back ('Z');
		cliente.ClienteTCP::enviar (s);

		std::string r;
		cliente.ClienteTCP::recibir (r, 10);
		int i = 0;
	}

#endif
	return 0;
}
