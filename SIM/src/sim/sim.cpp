/*********************************************************************************************
 *	Name		: sim_cmd.cpp
 *	Description	: Clase que implementa un SIM
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "sim/sim.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

#include "sim/hexadecimal.h"
#include "sim/ids.h"

Sim::Sim()
	: cpu (NULL), mmu (NULL)
{
	this->ejecutando = true;
	this->ejecutando = true;
}
Sim::~Sim()
{
	if (cpu != NULL)
	{
		delete this->cpu;
		this->cpu = NULL;
	}
	if (mmu != NULL)
	{
		delete this->mmu;
		this->mmu = NULL;
	}
}

void Sim::cuanto()
{
	static int cuantosHubo = 0;
	static int ciclos_Hubo = 0;
	while (this->ejecutando == true)
	{
		for (int i = 0; i < TMP_CICLOS; i++)
		{
			this->cpu->ciclo();
			ciclos_Hubo++;
		}
		cuantosHubo++;

		// --------------------------------------
		// Revisi�n de Dispositivos/Controladores
		{
			// Gestor del Altavoz del sistema
			if (this->cpu->getMMU()->leer_de_Memoria (0xFE) == 1)
			{
				std::cout << '\a';
				//Beep (440, 333);
			}
		}
		{
			// Gestor del Sistema El�ctrico
			if (this->cpu->getMMU()->leer_de_Memoria (0xFD) == 1)
			{
				// Apagado
				this->ejecutando = false;
			}
			if (this->cpu->getMMU()->leer_de_Memoria (0xFC) == 1)
			{
				// Reiniciado
				this->cpu->getMMU()->escribirMemoria (0xFC, 0);
				this->mmu->iniciar();
			}
		}
	}
}

void Sim::iniciaSistemaInformatico()
{
	if (this->cpu == NULL)
	{
		this->cpu = new CPU();

		if (this->mmu == NULL)
		{
			this->mmu = new MMU();
		}
		this->mmu->iniciar();
		this->cpu->instalaMMU (this->mmu);
	}
}

bool Sim::cargaGestorDeArranque (const std::string & fichero)
{
	int i;
	Direccion PTR = 0;
	bool cmntr = false, marcador = false;
	int cmntBloque = 0, tempBloque = 0;
	std::ifstream file;
	file.open (fichero.c_str(), std::ifstream::in | std::ifstream::binary);
	if (file.is_open() == false)
	{
		return false;
	}

	// LECTURA ITERATIVA DEL FICHERO
	char c;
	for (c = file.get(); c != EOF && file.good() == true; c = file.get())
	{
#ifdef _DEBUG
		if (c == '$')
		{
			continue;
		}
#endif
		if (esHexa (c) && !cmntr && cmntBloque == 0)
		{
			for (i = 0; i < 4; i++)
			{
				this->mmu->escribirMemoria (PTR, ::hexaSubChar (c, i));
				PTR++;
			}
		}
		else
		{
			switch (c)
			{
			case '/' :
				if (cmntr)
				{
				}
				else if (marcador)
				{
					marcador = false;
					cmntr = true;
				}
				else if (cmntBloque > 0 && tempBloque > 0)
				{
					cmntBloque = (cmntBloque <= tempBloque ? 0 : cmntBloque - tempBloque);
					marcador = false;
				}
				else
				{
					marcador = true;
				}
				tempBloque = 0;
				break;
			case '*' :
				if (cmntr)
				{
				}
				else if (marcador)
				{
					cmntr = true;
					cmntBloque += 1;
				}
				else if (cmntr && cmntBloque > 0)
				{
					cmntBloque += 1;
				}
				else
				{
					tempBloque += 1;
					cmntr = false;
				}
				break;
			case '\n' :
			case '\r' :
				cmntr = false;
				marcador = false;
				tempBloque = 0;
				break;
			default :
				if (cmntr)
				{
				}
				else
				{
					marcador = false;
					tempBloque = 0;
				}
				break;
			}
		}
	}
	// FINAL DE LA LECTURA ITERATIVA DEL FICHERO
	file.close();
	return true;
}
