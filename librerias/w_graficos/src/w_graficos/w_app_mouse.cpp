/*********************************************************************************************
 * Name			: w_app_mouse.cpp
 * Description	: Implementación de los métodos relativos al MOUSE en WApp
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_app.h"

// ----------------------------------------------------------------------------
// Métodos propios
const WControl & WApp::getCursor() const
{
	return this->mouse;
}
WControl & WApp::getCursor()
{
	return this->mouse;
}


bool WApp::setMouse (const int posX, const int posY)
{
	if (this->mouse.getPosX() == posX && this->mouse.getPosY() == posY)
	{
		return false;
	}
	this->mouse.setPosX (posX);
	this->mouse.setPosY (posY);

	// Ajustar puntero
	const WCoordenada newCoordenada (posX, posY);
	this->setPunteroPos (newCoordenada);

	// Provocar evento
	this->onMoveMouseOver();
	return true;
}
