/*********************************************************************************************
 * Name			: w_lienzo_auto.cpp
 * Description	: Implementación de la clase para manipular una representación gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_lienzo_auto.h"

#include "w_graficos/w_control.h"
#include "w_graficos/w_rgba.h"

#include <vector>

class WLienzoAuto::WLienzoIntern
{
public:
	class RGB : public RGBA
	{
	public:
		RGB()
			: RGBA (0, 0, 0, 0)
		{
		}
		RGB (const RGBA & org)
			: RGBA (org)
		{
		}
	};
	typedef std::vector<RGB> ColumnaRGBA;
	typedef std::vector<ColumnaRGBA> MatrizRGBA;
public:
	WLienzoIntern()
		: datos()
	{
	}
	MatrizRGBA datos;
};

// ----------------------------------------------------------------------------
// Constructores
WLienzoAuto::WLienzoAuto()
	: data (*new WLienzoIntern)
{
	this->ancho = 0;
	this->alto = 0;
}
WLienzoAuto::WLienzoAuto (const WLienzoBase & lienzo)
	: data (*new WLienzoIntern)
{
	this->operator= (lienzo);
}

void WLienzoAuto::operator= (const WLienzoBase & lienzo)
{
	if (this->ancho * this->alto != lienzo.getAncho() * lienzo.getAlto())
	{
		this->data.datos.clear();
	}
	this->resize (lienzo.getAncho(), lienzo.getAlto());
	WLienzoBase::operator+= (lienzo);
}

WLienzoAuto::~WLienzoAuto()
{
	delete &this->data;
}

// ----------------------------------------------------------------------------
// Accesores
RGBA WLienzoAuto::getPixel (const size_t x, const size_t y) const
{
	if (this->ancho <= x || this->alto <= y)
	{
		return RGBA();
	}
	return this->getPixelFast (x, y);
}
bool WLienzoAuto::setPixel (const size_t x, const size_t y, const RGBA & rgba)
{
	bool cambio = false;
	if (this->ancho <= x)
	{
		this->ancho = x + 1;
		cambio = true;
	}
	if (this->alto <= y)
	{
		this->alto = y + 1;
		cambio = true;
	}
	if (cambio == true)
	{
		this->reserveIntern();
	}
	if (rgba.a > 0)
	{
		this->setPixelFast (x, y, rgba);
	}
	return true;
}
void WLienzoAuto::setPixelFast (const size_t x, const size_t y, const RGBA & rgba)
{
	this->data.datos[x][y] = rgba;
}
RGBA WLienzoAuto::getPixelFast (const size_t x, const size_t y) const
{
	return this->data.datos[x][y];
}

// ----------------------------------------------------------------------------
// Modificadores
void WLienzoAuto::reserve (const size_t ancho, const size_t alto)
{
	this->ancho = ancho;
	this->alto = alto;
	this->reserveIntern();
}
void WLienzoAuto::reserveIntern()
{
	this->data.datos.resize (this->ancho);
	for (size_t i = 0; i < this->ancho; i++)
	{
		this->data.datos[i].resize (this->alto);
	}
}

// ----------------------------------------------------------------------------
// Observadores
size_t WLienzoAuto::getAncho() const
{
	return this->ancho;
}
size_t WLienzoAuto::getAlto() const
{
	return this->alto;
}
