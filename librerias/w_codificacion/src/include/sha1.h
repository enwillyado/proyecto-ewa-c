/*
 * Fetched from ftp://ftp.funet.fi/pub/crypt/hash/sha/sha1.c on May 4, 1999
 * Modified by EPG May 6, 1999
 *
 * SHA-1 in C
 * By Steve Reid <steve@edmweb.com>
 * 100% Public Domain
 *
 * Headers and minor additions by Nate Nielsen <nielsen@memberwebs.com>
 */

#ifndef __SHA1_H__
#define __SHA1_H__

#ifndef LITTLE_ENDIAN
#define LITTLE_ENDIAN
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define SHA1_LEN 20

typedef struct sha1_ctx
{
	unsigned long state[5];
	unsigned long count[2];
	unsigned char buffer[64];
}
sha1_ctx_t;

void sha1_init (sha1_ctx_t* context);	//SHA1Init - Initialize new context
void sha1_update (sha1_ctx_t* context, const void* data, unsigned int len);	// Run your data through this.
void sha1_final (unsigned char digest[SHA1_LEN], sha1_ctx_t* context);		// Add padding and return the message digest.
void sha1_string (unsigned char digest[SHA1_LEN], const char* str);	// Create a digest form a null terminated string
int sha1_strcmp (unsigned char digest[SHA1_LEN], const char* str);	// Compare a digest with a new digest form a null terminated string

#ifdef __cplusplus
}
#endif

#endif // __SHA1_H__


