/*********************************************************************************************
*	Name		: iStartable.h
*	Description	: Interfaz para clases que contentan el m�todo "start" (pensado para hilos)
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _I_STARTABLE_H_
#define _I_STARTABLE_H_

class IStartable
{
public:
	virtual bool start() = 0;
};

#endif	//_I_STARTABLE_H_
