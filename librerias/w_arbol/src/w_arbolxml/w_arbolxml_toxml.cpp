/*********************************************************************************************
 *	Name		: w_arbolxml_toxml.cpp
 *	Description	: Escritura de un �rbol a un XML
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_arbol/w_arbolxml.h"

/**
 *	Devuelve el valor del ArbolXML actual en formato string-xml plano
 *	\param	[in]	conSaltosDeLinea	(opt.=TRUE)	si se desea que tenga saltos de l�nea (\n) cuando tenga hijos
 *	\param	[in]	conEncabezado		(opt.=TRUE)	si se desea que el xml incluya el encabezado est�ndar
 *  \retval	 la cadena con el xml plano del elemento actual;
 **/
std::string ArbolXML::genXML (bool conSaltosDeLinea, bool conEncabezado) const
{
	return ArbolXML::genXML (*this, conSaltosDeLinea, conEncabezado);
}

/**
 *	Devuelve el valor del Arbol indicado en formato string-xml plano
 *	\param	[in]	arbol				instancia de un arbol que se va a volcar en el xml
 *	\param	[in]	conSaltosDeLinea	(opt.=TRUE)			si se desea que tenga saltos de l�nea (\n) cuando tenga hijos
 *	\param	[in]	conEncabezado		(opt.=TRUE)			si se desea que el xml incluya el encabezado est�ndar
 *	\param	[in]	finalEncoding		(opt.=DCP_UNKNOWN)	codificaci�n que tendr� al final el string
 *	\param	[in]	nivelTab			(opt.=0)			nivel de tabulaci�n actual (variable de control recursiva)
 *  \retval	 la cadena con el xml plano del elemento actual;
 **/
std::string ArbolXML::genXML (const Arbol & arbol, bool conSaltosDeLinea, bool conEncabezado)
{
	return ArbolXML::genXML (arbol, conSaltosDeLinea, conEncabezado, 0);
}

std::string ArbolXML::genXML (const Arbol & arbol, bool conSaltosDeLinea, bool conEncabezado, size_t nivelTab)
{
	std::string ret = "";
	if (nivelTab == 0 && conEncabezado)
	{
		const std::string & cabecera = ArbolXML::genCabeceraXMLStd (arbol);
		ret += cabecera;
		if (conSaltosDeLinea == true && cabecera != "")
		{
			ret.push_back ('\n');
		}
	}
	ret += "<";
	ret += arbol.getClave();
	const Arbol::Atributos & atributos = arbol.getAtributos();
	for (Arbol::Atributos::const_iterator itr = atributos.begin(); itr != atributos.end(); itr++)
	{
		ret.push_back (' ');
		ret += itr->first;
		ret.push_back ('=');
		ret.push_back ('"');
		ret += ArbolXML::htmlEntitiesEncode (itr->second);	// los atributos van escapados siempre
		ret.push_back ('"');
	}
	if (arbol.hijosize() > 0)
	{
		ret += ">";
		const Arbol::Arboles & hijos = arbol.getHijos();
		for (size_t i = 0; i < arbol.hijosize(); i++)
		{
			if (conSaltosDeLinea)
			{
				ret.push_back ('\n');
				ret += std::string (nivelTab + 1, '\t');
			}
			// Llamada recursiva para crear el resto de hijos
			ret += ArbolXML::genXML (hijos[i], conSaltosDeLinea, false, nivelTab + 1);
		}
		if (conSaltosDeLinea)
		{
			ret.push_back ('\n');
			ret += std::string (nivelTab, '\t');
		}
		// TODO: Si hay valor e hijos, no se pone el valor!!!
		ret.push_back ('<');
		ret.push_back ('/');
		ret += arbol.getClave();
		ret.push_back ('>');
	}
	else if (arbol.getValor() != "")
	{
		ret += ">";
		ret += arbol.getValor();
		ret.push_back ('<');
		ret.push_back ('/');
		ret += arbol.getClave();
		ret.push_back ('>');
	}
	else
	{
		ret += " />";
	}
	return ret;
}

/**
 *	Devuelve la cabecera est�ndar correspondiente al encoding dado; y si no, al que tenga el arbol dado
 *	\param	[in]	encoding	encoding deseado
 *	\param	[in]	arbol		arbol que desempatar� en caso de duda
 *  \retval	 la cadena con el xml plano del elemento actual;
 **/
std::string ArbolXML::genCabeceraXMLStd (const Arbol & arbol)
{
	// Si no, el encoding es el indicado
	return ENCABEZADO_XML ("UTF-8");
}
