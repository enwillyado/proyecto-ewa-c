/*********************************************************************************************
 *	Name		: wrapper_des_deco.cpp
 *	Description	: Wrapper de C++ para algoritmo de DES soportando los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion.h"

//____________________________________________________________________________________________
//
// Desencriptar
//____________________________________________________________________________________________

EEncryptResult desDesencrypt (const void* bufferEncryptIn, size_t szBufferEncryptIn, void* bufferOut, size_t* szBufferOut,
							  const Des3Key key1, const Des3Key key2, int nPadding, EDesMode mode, EDesConfig config)
{
	return desDesencryptBuffer (bufferEncryptIn, szBufferEncryptIn, bufferOut, szBufferOut, key1, key2, nPadding, mode, config);
}

EEncryptResult desDesencrypt (const void* bufferEncryptIn, size_t szBufferEncryptIn, std::vector<unsigned char> & bufferOut,
							  const Des3Key key1, const Des3Key key2, int nPadding, EDesMode mode, EDesConfig config)
{
	size_t szBufferOut;

	bufferOut.resize (DesCalculateSizeDesencryptBuffer (nPadding, config, szBufferEncryptIn));
	szBufferOut = bufferOut.size();
	if (desDesencrypt (bufferEncryptIn, szBufferEncryptIn, &bufferOut[0], &szBufferOut, key1, key2, nPadding, mode, config) != EncryptOk)
	{
		return EncryptError;
	}
	return EncryptOk;
}
