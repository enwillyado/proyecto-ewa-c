/*********************************************************************************************
 * Name			: w_sistema_files.cpp
 * Description	: Implementaci�n de los m�todos comunes para el trabajo con ficheros
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema_files.h"

#include "w_base/strTransform.hpp"
#include "w_base/filesStr.hpp"

#include <sys/stat.h>
#include <sys/types.h>

#ifdef WIN32
#include <direct.h>
#else
int _mkdir (const char* dir)
{
	return mkdir (dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}
#endif

std::string WFiles::normalicePath (const std::string & path)
{
	return ::remplazar (path, '\\', '/');
}
std::string WFiles::getDirOfPath (const std::string & path)
{
	const std::string & normalicedPath = WFiles::normalicePath (path);
	if (normalicedPath.find_last_of ('/') != path.npos)
	{
		return normalicedPath.substr (0, path.find_last_of ('/'));
	}
	return "";
}

bool WFiles::createDirPath (const std::string & path)
{
	std::string dir = WFiles::getDirOfPath (path);
	if (path == "")
	{
		return true;
	}
	if (false == WFiles::fileExists (dir))
	{
		if (false == WFiles::createDirPath (dir))
		{
			// Error al crear el subdirectorio
			return false;
		}

		// Crear el directorio
		::_mkdir (dir.c_str());

		// Comprobar que se cre�
		if (false == WFiles::fileExists (dir))
		{
			// Error al crearlo
			return false;
		}
	}
	return true;
}

bool WFiles::fileExists (const std::string & filename)
{
	return WFiles::fileExists (filename.c_str());
}
bool WFiles::fileExists (const char* filename)
{
	struct stat st;
	return stat (filename, &st) == 0;
}
