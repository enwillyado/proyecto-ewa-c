/*********************************************************************************************
 * Name			: w_lienzo_bmp.cpp
 * Description	: Implementación de la clase para cargar lienzos en BMP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_lienzo_bmp.h"

#include "w_graficos/w_rgba.h"
#include <cstdio>	// fopen

RGBA getPixelRaw (const WLienzoBMP::BYTE* const pImageData, const WLienzoBMP::BMPHeader & header,
				  const size_t x, const size_t y)
{
	const size_t coordenada = ((header.height - y - 1) * ((header.bitsPerPixel * header.width + 31) / 32) * 4) + (x * 3);
	RGBA rgba (pImageData[ coordenada + 2],
			   pImageData[ coordenada + 1],
			   pImageData[ coordenada + 0]);
	return rgba;
}

void setPixelRaw (WLienzoBMP::BYTE* const pImageData, const WLienzoBMP::BMPHeader & header,
				  const size_t x, const size_t y, const RGBA & rgba)
{
	const size_t coordenada = ((header.height - y - 1) * ((header.bitsPerPixel * header.width + 31) / 32) * 4) + (x * 3);
	pImageData[ coordenada + 0] = rgba.b;
	pImageData[ coordenada + 1] = rgba.g;
	pImageData[ coordenada + 2] = rgba.r;
}

WLienzoBMP::WLienzoBMP (const std::string & filename)
{
	if (filename != "")
	{
		this->leerContenidoFile (filename);
	}
}
bool WLienzoBMP::leerContenidoFile (const std::string & filename)
{
	// Apertura del archivo
	FILE* pFile = fopen (filename.c_str(), "rb");

	// Si se pudo cargar el archivo
	if (pFile == NULL)
	{
		return false;
	}
	// Cargo la cabecera
	fread (&header.identifier,       1, sizeof (WORD),  pFile);
	fread (&header.size,             1, sizeof (DWORD), pFile);
	fread (&header.reserved,         1, sizeof (DWORD), pFile);
	fread (&header.bitoffset,        1, sizeof (DWORD), pFile);
	fread (&header.headerSize,       1, sizeof (DWORD), pFile);
	fread (&header.width,            1, sizeof (DWORD), pFile);
	fread (&header.height,           1, sizeof (DWORD), pFile);
	fread (&header.planes,           1, sizeof (WORD),  pFile);
	fread (&header.bitsPerPixel,     1, sizeof (WORD),  pFile);
	fread (&header.compression,      1, sizeof (DWORD), pFile);
	fread (&header.imageSize,        1, sizeof (DWORD), pFile);
	fread (&header.hresolution,      1, sizeof (DWORD), pFile);
	fread (&header.vresolution,      1, sizeof (DWORD), pFile);
	fread (&header.numberOfColours,  1, sizeof (DWORD), pFile);
	fread (&header.importantColours, 1, sizeof (DWORD), pFile);

	// Si el archivo no es de 8bpp, termino la ejecución
	if (header.bitsPerPixel != 24)
	{
		fclose (pFile);
		return false;
	}

	// Cargo los datos de la imagen
	BYTE* pImageData = new BYTE[this->header.imageSize];
	fseek (pFile, header.bitoffset, SEEK_SET);
	fread (pImageData, 1, this->header.imageSize, pFile);

	// Volcar los pixeles al lienzo
	this->WLienzo::reserve (this->header.width, this->header.height);
	for (size_t y = 0; y < this->header.height; y++)
	{
		for (size_t x = 0; x < this->header.width; x++)
		{
			const RGBA & rgba = ::getPixelRaw (pImageData, this->header, x, y);
			this->WLienzo::setPixel (x, y, rgba);
		}
	}

	// Cierro el archivo
	fclose (pFile);
	delete[] pImageData;

	// Todo ha ido bien
	return true;
}

WLienzoBMP WLienzoBMP::getWLienzoBMPFromFile (const std::string & filename)
{
	WLienzoBMP ret (filename);
	return ret;
}

/*
void WLienzoBMP::save (const std::string & filename)
{
	FILE* pFile = fopen (filename.c_str(), "wb");

	fwrite (&header.identifier,       sizeof (header.identifier), 1, pFile);
	fwrite (&header.size,             sizeof (header.size), 1, pFile);
	fwrite (&header.reserved,         sizeof (header.reserved), 1, pFile);
	fwrite (&header.bitoffset,        sizeof (header.bitoffset), 1, pFile);
	fwrite (&header.headerSize,       sizeof (header.headerSize), 1, pFile);
	fwrite (&header.width,            sizeof (header.width), 1, pFile);
	fwrite (&header.height,           sizeof (header.height), 1, pFile);
	fwrite (&header.planes,           sizeof (header.planes), 1, pFile);
	fwrite (&header.bitsPerPixel,     sizeof (header.bitsPerPixel), 1, pFile);
	fwrite (&header.compression,      sizeof (header.compression), 1, pFile);
	fwrite (&header.imageSize,        sizeof (header.imageSize), 1, pFile);
	fwrite (&header.hresolution,      sizeof (header.hresolution), 1, pFile);
	fwrite (&header.vresolution,      sizeof (header.vresolution), 1, pFile);
	fwrite (&header.numberOfColours,  sizeof (header.numberOfColours), 1, pFile);
	fwrite (&header.importantColours, sizeof (header.importantColours), 1, pFile);

	fseek (pFile, header.bitoffset, SEEK_SET);
	BYTE* pImageData = new BYTE[header.width * header.height * 3];
	fwrite (pImageData, 1, header.width * header.height * 3, pFile);

	fclose (pFile);
}
*/
