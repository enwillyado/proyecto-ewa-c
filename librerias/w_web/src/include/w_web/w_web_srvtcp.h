/*********************************************************************************************
 *	Name		: _w_web_srvtcp.h
 *	Description	: Servidor Web en TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_SRVTCP_H_
#define _W_WEB_SRVTCP_H_

#include "w_web/w_web_mensaje.h"
#include "w_red/w_red_srvtcp.h"

class ServidorWebTCP : public ServidorTCP
{
public:
	// ASK: �M�s constructores?
	ServidorWebTCP();

public:
	// L�gica del servidor:
	bool send (const EventoServidorTCP &, const Mensaje &);

protected:
	// Cola de eventos incompletos
	MapaEventosServidorTCP mapaEventosServidorTCP;

	// Recepci�n de la petici�n
	virtual bool analizaEventoMsgRecv (const EventoServidorTCP &);
	virtual void procesaMsgNoWebRecv (const EventoServidorTCP &);
	virtual void procesaMsgWebRecv (const EventoServidorTCP &, const Mensaje &);
	virtual Mensaje responderMsgWebRecv (const EventoServidorTCP &, const Mensaje &);

	// Emisi�n de la respuesta
	virtual void procesaMsgWebEnv (const EventoServidorTCP &, const Mensaje & peticion,
								   const Mensaje & respuesta);

public:
	// Sobrecarga de m�todos de procesamiento de mensajes web por CallBack
	typedef Mensaje (*WebCallBack) (ServidorWebTCP &, const EventoServidorTCP &, const Mensaje &);
	bool setWebCallBack (WebCallBack, void* callBackObj = NULL);
	void* getWebCallBackObjPtr() const;

	template<class T>
	T & getWebCallBackObj() const
	{
		return *ASSERT_IN_DEBUG (static_cast<T*> (getWebCallBackObjPtr()));
	}

protected:
	WebCallBack webCallBack;
	void* webCallBackObj;
};

#endif	//_W_WEB_SRVTCP_H_
