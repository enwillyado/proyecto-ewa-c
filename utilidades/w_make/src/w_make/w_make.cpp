/*********************************************************************************************
 *	Name		: w_make.cpp
 *	Description	: Implementanci�n de las funciones para preparar soluciones
 *	Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_make/w_make.h"

#include "w_base/filesStr.hpp"

WMake::WMake()
	: tipoOrden (WMake::DESCONOCIDO)
{
}

void WMake::setFicheroOrden (const std::string & ficheroOrden)
{
	this->ficheroOrden = ficheroOrden;

	const std::string & dir = WFilesStr::getAbsDir (ficheroOrden);
	this->parametros.dirOrigen = dir;

	const std::string & ext = WFilesStr::getExt (ficheroOrden);
	if (ext == "wmake")
	{
		this->tipoOrden = WMake::WMAKE;
	}
	else if (ext == "vcxproj")
	{
		this->tipoOrden = WMake::PROYECTO;
	}
	else
	{
		// TODO: detectar todos los dem�s tipos
		this->tipoOrden = WMake::DESCONOCIDO;
	}
}

WMake::TipoOrden WMake::getTipoOrden() const
{
	return this->tipoOrden;
}

WMake::Estado WMake::preparar()
{
	switch (this->tipoOrden)
	{
	case WMake::SOLUCION:
		return this->prepararSln();
		break;

	case WMake::PROYECTO:
		return this->prepararPro();
		break;

	case WMake::WMAKE:
		return this->prepararWMK();
		break;

	case WMake::ORDEN:
		switch (this->parametros.tipo)
		{
		case WMakeParametros::EJECUTABLE:
			return this->prepararExe();
			break;

		case WMakeParametros::LIBRERIA_DLL:
			return this->prepararDll();
			break;

		case WMakeParametros::LIBRERIA_LIB:
			return this->prepararLib();
			break;

		case WMakeParametros::OBJETO:
			return this->prepararObj();
			break;

		case WMakeParametros::FUENTE_C:
		case WMakeParametros::FUENTE_CPP:
			// TODO: �estos se preparan?
			break;
		}
		break;

	}
	return WMake::OMITIDO;
}
