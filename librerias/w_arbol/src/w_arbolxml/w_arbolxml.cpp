/*********************************************************************************************
 *	Name		: w_arbolxml.cpp
 *	Description	: Arbol para trabajar m�s c�modamente con un xml en memoria, usando std
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_arbol/w_arbolxml.h"

#include <fstream> // para poder trabajar con los ficheros

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Constructores <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

ArbolXML::ArbolXML (const Arbol & nuevo)
	: Arbol (nuevo)
{
}

ArbolXML::ArbolXML (const std::string & nuevaClave, const std::string & nuevoValor)
	: Arbol (nuevaClave, nuevoValor)
{
}


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Utilidades <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

const std::string & ArbolXML::getPathDir() const
{
	return this->pathDir;
}
void ArbolXML::setPathDir (const std::string & value)
{
	this->pathDir = value;
}
void ArbolXML::setPathDirFromFile (const std::string & value)
{
	size_t i = value.find_last_of ("/\\");
	if (i != std::string::npos)
	{
		this->pathDir = value.substr (0, i);
	}
	else
	{
		this->pathDir = value;
	}
#ifdef WIN32
	this->pathDir.push_back ('\\');
#else
	this->pathDir.push_back ('/');
#endif
}
