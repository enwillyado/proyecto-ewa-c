/*********************************************************************************************
 * Name			: w_web_services_secure_prueba.cpp
 * Description	: Implementación del mecanismo para responder a un determinado servicio
 *					que ejecutará el servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_web/w_web_services_secure_prueba.h"

// Servicio a implementar
Mensaje ServicioPruebaSecure::ejecutarServicio (const Evento & evento, const Mensaje & peticion,
		const ServicioSecure::ResultadoAutenticacion & resultadoAutenticacion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::HTTP1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("<div>SRVSERVICE_SECURE_PRUEBA_ID('" + peticion.getURI() + "');</div>"\
					   "<a href='" SRVSERVICE_SECURE_PRUEBA_ID_ALTERNATIVA "'>Ir a la alternativa</a>");
	return respuesta;
}

// Servicio a implementar
Mensaje ServicioPruebaSecureAlternativa::ejecutarServicio (const Evento & evento, const Mensaje & peticion,
		const ServicioSecure::ResultadoAutenticacion & resultadoAutenticacion) const
{
	Mensaje respuesta;
	respuesta.setError (Mensaje::FOUND);
	respuesta.setVersionProtocolo (Mensaje::HTTP1_0);
	respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);
	respuesta.setBody ("SRVSERVICE_SECURE_PRUEBA_ID('" + peticion.getURI() + "');");
	respuesta.setBody ("<div>SRVSERVICE_SECURE_PRUEBA_ID_ALTERNATIVA('" + peticion.getURI() + "');</div>"\
					   "<a href='" SRVSERVICE_SECURE_PRUEBA_ID "'>Ir al anterior</a>");
	return respuesta;
}
