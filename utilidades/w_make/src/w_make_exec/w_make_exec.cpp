/*********************************************************************************************
 *	Name		: w_make_exec.cpp
 *	Description	: Ejecutable para preparar soluciones
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_make_exec.pragmalib.h"

#include "w_make/w_make.h"

#include <iostream>

int main (int argc, char* argv[])
{
	if (argc <= 1)
	{
		std::cout << "Preparador de soluciones del Proyecto eWa (por consola)" << std::endl;
		std::cout << "" << std::endl;
		return 1;
	}

	// Cargar el origen
	const std::string & ficheroOrden = argv[1];

	// Ajustar valores
	WMake wmake;
	wmake.setFicheroOrden (ficheroOrden);

	const WMake::TipoOrden tipoOrden = wmake.getTipoOrden();
	switch (tipoOrden)
	{
	case WMake::DESCONOCIDO:
		std::cerr << "Tipo de orden desconocida" << std::endl;
		return -1;
		break;
	}

	// Y preparar
	const WMake::Estado estado = wmake.preparar();
	switch (estado)
	{
	case WMake::CORRECTO:
		std::cout << "[OK]" << std::endl;
		break;
	default:
		std::cout << "[ERROR] (" << estado << ")" << std::endl;
		break;
	}

	// Retornar el c�digo de error num�rico
	return (int) (estado);
}
