/*********************************************************************************************
 * Name			: w_web_services_base.h
 * Description	: Mecanismo base para responder a un determinado servicio que ejecutar�
 *					el Servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SERVICES_BASE_H_
#define _W_RED_SERVICES_BASE_H_

#include "w_red/w_red_srvevent.h"			//< para tener "EventoServidorTCP"
#include "w_web/w_web_mensaje.h"			//< para tener "Protocolo"

// ----------------------------------------------------------------------------
// Clase padre de la que hereada cada Servicio a registrar
class BasicSrvService
{
public:
	BasicSrvService() {}
	virtual ~BasicSrvService() {}
	enum Errores
	{
		SIN_ERROR = 0,
	};
	// M�todos p�blicos
	virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const = 0;
	virtual std::string getServicioName() const = 0;
	virtual BasicSrvService* newInstance() const = 0;
};

#endif	//_W_RED_SERVICES_BASE_H_
