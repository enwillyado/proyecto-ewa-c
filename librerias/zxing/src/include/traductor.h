/*
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <zxing/LuminanceSource.h>
#include <stdio.h>
#include <stdlib.h>
using namespace zxing;

namespace qrviddec
{
	class BufferBitmapSource : public LuminanceSource
	{
	private:
		int width, height;
		unsigned char* buffer;

	public:
		BufferBitmapSource (int inWidth, int inHeight, unsigned char* inBuffer);
		~BufferBitmapSource();

		int getWidth() const;
		int getHeight() const;
		unsigned char* getRow (int y, unsigned char* row);
		unsigned char* getMatrix();
	};
}
