/*********************************************************************************************
*	Name		: w_red_proxy_tcp.h
*	Description	: Clase para implementar un proxy de red basado en TCP
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_PROXY_TCP_H_
#define _W_RED_PROXY_TCP_H_

#include "w_red/w_red_proxy.h"

#include "w_red/w_red_clitcp.h"
#include "w_red/w_red_srvtcp.h"

#include "w_sistema/w_sistema_thread.h"

#include <string>
#include <map>

class ProxyTCP : public Proxy
{
public:
	ProxyTCP (const std::string & remoteHost, const int remotePort);
	virtual bool open (const int localPort);
	virtual bool start();

protected:
	// CallBack manejador
	static void staticCallBack (ServidorTCP & servidor);
	virtual void callBack();
	virtual void aceptarConexion (const EventoServidorTCP &);

	// Manejadores de datos del proxy
	virtual void onClienteGetData (const SocketID, const std::string & mensaje);
	virtual void onProxyGetData (const SocketID, const std::string & mensaje);

	class DatosCliente;
	typedef std::map<SocketID, DatosCliente*> DatosClientesTCP;
	DatosClientesTCP clientesTCP;
	ServidorTCP servidorTCP;
};


#endif //_W_RED_PROXY_TCP_H_
