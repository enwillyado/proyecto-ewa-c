/*********************************************************************************************
*	Name		: filesStr.hpp
*	Description	: M�todos para tratar nombres de ficheros
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _FILES_STR_HPP_
#define _FILES_STR_HPP_

#include <string>

namespace WFilesStr
{
	static std::string getExt (const std::string & fichero_path)
	{
		size_t i = fichero_path.find_last_of ('.');
		if (i != std::string::npos)
		{
			return fichero_path.substr (i + 1);
		}
		return "";
	}
	static std::string getNombre (const std::string & fichero_path)
	{
		size_t i = fichero_path.find_last_of ("/\\");
		if (i != std::string::npos)
		{
			return fichero_path.substr (i + 1);
		}
		return fichero_path;
	}
	static std::string getNombreOnly (const std::string & fichero_path)
	{
		const std::string & nombre = getNombre (fichero_path);
		size_t i = nombre.find_last_of ('.');
		if (i != std::string::npos)
		{
			return nombre.substr (0, i);
		}
		return nombre;
	}

	static std::string getDir (const std::string & fichero_path)
	{
		size_t i = fichero_path.find_last_of ("/\\");
		if (i != std::string::npos)
		{
			return fichero_path.substr (0, i + 1);
		}
		return "./";
	}
	static std::string getAbsDir (const std::string & fichero_path)
	{
		// TODO: hacer que retorne la ruta absoluta
		return WFilesStr::getDir (fichero_path);
	}
}

#endif	//_FILES_STR_HPP_
