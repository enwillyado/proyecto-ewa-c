/*********************************************************************************************
 *	Name		: _w_red_proxy_tcp_cmd_ejemplo.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar el ejecutable para el PROXY TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_PROXY_TCP_CMD_EJEMPLO_PRAGMALIB_H_
#define _W_RED_PROXY_TCP_CMD_EJEMPLO_PRAGMALIB_H_

#ifdef _MSC_VER

// Parte privada
#include "w_red/_w_red_proxy.pragmalib.h"

#endif

#undef _W_RED_PROXY_TCP_CMD_EJEMPLO_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_RED_PROXY_TCP_CMD_EJEMPLO_PRAGMALIB_H_
