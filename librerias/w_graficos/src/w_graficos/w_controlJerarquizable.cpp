/*********************************************************************************************
 * Name			: w_controlJerarquizable.cpp
 * Description	: Implementaci�n de los desencadenadores de rat�n para controles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_controlJerarquizable.h"

#include <cstring>		//< NULL

// Constructor
WControlJerarquizable::WControlJerarquizable()
	: wappRoot (NULL), controlParent (NULL)
{
}

// ---------------------------------------------------------------------------
// Colocar la ra�z
WApp* WControlJerarquizable::getRootPtr()
{
	return this->wappRoot;
}
bool WControlJerarquizable::haveRootPtr()
{
	return this->getRootPtr() != NULL;
}

WApp & WControlJerarquizable::getRoot()
{
	return * (ASSERT_IN_DEBUG (this->getRootPtr()));
}

void WControlJerarquizable::setRoot (WApp & iWapp)
{
	this->wappRoot = &iWapp;
}

// ---------------------------------------------------------------------------
// Colocar la jerarqu�a
WControl* WControlJerarquizable::getParentPtr()
{
	return this->controlParent;
}

WControl & WControlJerarquizable::getParent()
{
	return * (ASSERT_IN_DEBUG (this->getParentPtr()));
}

void WControlJerarquizable::setParent (WControl & iWapp)
{
	this->controlParent = &iWapp;
}
