#include <boost/test/unit_test.hpp>

#include "w_web/w_web_catalogo.h"		//< para poder tener el cat�logo de servicios del servidor
#include "w_web/w_web_services.h"		//< para poder tener los servicios del servidor catalogados

#define SRVSERVICE_INEXISTENTE_ID "/NoExiste"

BOOST_AUTO_TEST_CASE (Test_SrvServices)
{
	EventoServidorTCP evento;
	Mensaje parser;
#ifdef SRVSERVICE_PING_ID
	{
		// Se a�ade este servicio
		BasicSrvService_Ping* servicio = NULL;
		BasicSrvServicesCatalogo::addService (servicio = new BasicSrvService_Ping());
		BOOST_CHECK (BasicSrvServicesCatalogo::getService (SRVSERVICE_PING_ID) != NULL);
		const Mensaje respuesta = BasicSrvServicesCatalogo::getService (SRVSERVICE_PING_ID)->ejecutarServicio (evento, parser);
		BOOST_CHECK (respuesta % ENCABEZADOID_CONTENT_LENGTH == "29");
		BOOST_CHECK (respuesta.getBody() == "BasicSrvService_Ping('pong');");
		BOOST_CHECK (BasicSrvServicesCatalogo::delService (SRVSERVICE_PING_ID) == true);
		if (servicio != NULL)
		{
			delete servicio;
			servicio = NULL;
		}
	}
#endif
#ifdef SRVSERVICE_OTRACOSA_ID
	{
		// Se a�ade este servicio
		BasicSrvService_OtraCosa* servicio = NULL;
		BasicSrvServicesCatalogo::addService (servicio = new BasicSrvService_OtraCosa());
		BOOST_CHECK (BasicSrvServicesCatalogo::getService (SRVSERVICE_OTRACOSA_ID) != NULL);
		parser.setBody (SRVSERVICE_OTRACOSA_ID SRVSERVICE_OTRACOSA_ID);		// por poner algo...
		const Mensaje  = BasicSrvServicesCatalogo::getService (SRVSERVICE_OTRACOSA_ID)->ejecutarServicio (evento, parser);
		BOOST_CHECK (respuesta.getBody() == "<code>" SRVSERVICE_OTRACOSA_ID ": " SRVSERVICE_OTRACOSA_ID SRVSERVICE_OTRACOSA_ID "</code>");
		BOOST_CHECK (respuesta % ENCABEZADOID_CONTENT_LENGTH == "42");
		BOOST_CHECK (BasicSrvServicesCatalogo::delService (SRVSERVICE_OTRACOSA_ID) == true);
		if (servicio != NULL)
		{
			delete servicio;
			servicio = NULL;
		}
	}
#endif
#ifdef SRVSERVICE_INEXISTENTE_ID
	{
		// Se supone que no existe este servico:
		// TODO: implementar un m�todo del cat�logo para borrar un servicio
		BOOST_CHECK (BasicSrvServicesCatalogo::getService (SRVSERVICE_INEXISTENTE_ID) == NULL);
	}
#endif
}

#ifdef PRUEBA_DEL_LENGUAJE
#include <string>
#include <map>

// Clase abstracta (con todos los m�todos virtuales puros)
class ClaseAbuelo
{
public:
	virtual std::string ejecutarServicio() = 0;
};

// Clase que implementa la clase abstracta de la que hereda
class ClasePadre : public ClaseAbuelo
{
public:
	virtual std::string ejecutarServicio()
	{
		return "hola";
	}
};

// Clase que especializa la clase padre de la que hereda
class Hijo : public ClasePadre
{
public:
	std::string ejecutarServicio()
	{
		return "soy un hijo";
	}
};

// Clase que reutiliza la clase padre de la que hereda
class OtroHijo : public ClasePadre
{
};

// Clase que especializa y reutiliza la clase padre de la que hereda
class MedioHijo : public ClasePadre
{
public:
	std::string ejecutarServicio()
	{
		return ClasePadre::ejecutarServicio() + ", soy medio";
	}
};

BOOST_AUTO_TEST_CASE (Test_Herencia_Polimorfica)
{
	Hijo x;
	BOOST_CHECK (x.ejecutarServicio() == "soy un hijo");
	ClasePadre  x2 = x;
	BOOST_CHECK (x2.ejecutarServicio() == "hola");
	ClasePadre*  y = new Hijo();
	BOOST_CHECK (y->ejecutarServicio() == "soy un hijo");
	ClasePadre*  y1 = &x;
	BOOST_CHECK (y1->ejecutarServicio() == "soy un hijo");
	ClasePadre*  y2 = new ClasePadre();
	BOOST_CHECK (y2->ejecutarServicio() == "hola");

	{
		// Forzar a cast a distintos tipos de clases <hermanas>:
		{
			OtroHijo* z1 = (OtroHijo*)y1;	// forzar a que sea del tipo "OtroHijo" una instancia "Hijo"
			BOOST_CHECK (z1->ejecutarServicio() == "soy un hijo");	//< pues devuelve lo de un "Hijo"
			MedioHijo* z2 = (MedioHijo*)y1;	// forzar a que sea del tipo "MedioHijo"
			BOOST_CHECK (z2->ejecutarServicio() == "soy un hijo");	//< y tambi�n devuelve lo de un "Hijo"
		}
		{
			OtroHijo z;							// tenemos una instancia de "OtroHijo"
			BOOST_CHECK (z.ejecutarServicio() == "hola");	//< devuelve la del padre porque no la implementa
			ClasePadre*  zy = &z;				// forzar a que sea de tipo "ClasePadre*"
			BOOST_CHECK (zy->ejecutarServicio() == "hola");	//< devuelve tambi�n lo suyo (que es lo del padre)
			MedioHijo*   zz = (MedioHijo*)zy;	// forzar a que sea de tipo "MedioHijo*"
			BOOST_CHECK (zz->ejecutarServicio() == "hola");	//< y tambi�n devuelve lo suyo (lo del padre)
		}
	}

	typedef std::map <int, ClaseAbuelo*> Mapa;
	Mapa mapa;
	mapa[0] = new ClasePadre();
	mapa[1] = new Hijo();
	mapa[2] = new OtroHijo();
	mapa[3] = new MedioHijo();

	BOOST_CHECK (mapa[0]->ejecutarServicio() == "hola");
	BOOST_CHECK (mapa[1]->ejecutarServicio() == "soy un hijo");
	BOOST_CHECK (mapa[2]->ejecutarServicio() == "hola");
	BOOST_CHECK (mapa[3]->ejecutarServicio() == "hola, soy medio");

	// Liberar la memoria que ocupa
	while (!mapa.empty())
	{
		delete mapa.begin()->second;
		mapa.erase (mapa.begin());
	}
}
#endif
