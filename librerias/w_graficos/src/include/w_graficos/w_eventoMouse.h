/*********************************************************************************************
 * Name			: w_eventoMouse.h
 * Description	: Evento para los desencadenadores de rat�n
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_EVENTO_MOUSE_H_
#define _W_EVENTO_MOUSE_H_

#include "w_graficos/w_evento.h"

class WEventoMouse : public WEvento
{
public:
	typedef void (*CbEventoMouse) (const WEventoMouse &);

public:
	// Constructor
	WEventoMouse (WControl & iControl)
		: WEvento (iControl)
	{
	}
};

#endif // _W_EVENTO_MOUSE_H_
