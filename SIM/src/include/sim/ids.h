/**************************
 * Defines
 *
 **************************/

// CPU
#define TMP_CUANTO 100
#define TMP_CICLOS 60000

// MMU
#define PAGINAS			(3)
#define PUNTROS_ORG		(4)							//< valor s�per-constante
#define PUNTROS_L		(3)
#define PUNTROS			(PUNTROS_ORG + PUNTROS_L)
#define COMPONT			(8)							//< valor s�per-constante
#define COMPONENTES		(PAGINAS * COMPONT)

#define TAMANIO_RAM		100	* 1024 * 1024			//< 100 MB de memoria

// Pantalla
#define alto 700
#define anxo 525