/*********************************************************************************************
 *	Name		: _w_shell_online.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SHELL_ONLINE_PRAGMALIB_H_
#define _W_SHELL_ONLINE_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_base/_pragmalib_base.h"

// Propia librer�a: (para no autoengancharla si es una dll)
#pragma comment(lib, "w_shell_online" FIN_LIB)

// Parte privada
#include "w_sistema/_w_sistema_execs.pragmalib.h"
#include "w_web/_w_web_protocol.pragmalib.h"
#include "w_web/_w_web_services.pragmalib.h"
#include "w_web/_w_web_server.pragmalib.h"

#endif

#undef _W_SHELL_ONLINE_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_SHELL_ONLINE_PRAGMALIB_H_
