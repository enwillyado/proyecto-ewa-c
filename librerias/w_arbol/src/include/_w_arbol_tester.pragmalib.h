/*********************************************************************************************
 *	Name		: _w_arbol_tester.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar el tester
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ARBOL_TESTER_PRAGMALIB_H_
#define _W_ARBOL_TESTER_PRAGMALIB_H_

#ifdef _MSC_VER

// Parte privada
#include "w_arbol/_w_arbolxml.pragmalib.h"

#endif

#undef _W_ARBOL_TESTER_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_ARBOL_TESTER_PRAGMALIB_H_
