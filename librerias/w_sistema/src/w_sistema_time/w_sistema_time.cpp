/*********************************************************************************************
 *	Name		: w_sistema_time.cpp
 *	Description	: Control del tiempo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema_time.h"

#ifdef WIN32
#include <windows.h>	// para el Sleep
void WTime::esperarMilisegundos (size_t milisegundos)
{
	::Sleep (milisegundos);
}
#else
#include <unistd.h> // para el usleep
void WTime::esperarMilisegundos (size_t milisegundos)
{
	::usleep (milisegundos * 1000); // 1000 microseconds en un millisecond
}
#endif
