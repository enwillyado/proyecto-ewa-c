/*********************************************************************************************
 * Name			: w_textControl.h
 * Description	: Clase para gestionar un control de texto nativo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_TEXT_CONTROL_H_
#define _W_TEXT_CONTROL_H_

#include "w_graficos/w_control.h"

#include <vector>
#include <string>

class WTextControl : public WControl
{
public:
	// Constructores
	WTextControl (const std::string & iName = "");

	// Modificadores
	void resize (const size_t ancho, const size_t alto);
	void setText (const std::string & str);
	void addChar (const char c);
	void remChar();

	virtual void pintar();

	// Manejadores
	virtual void onKeyDown (const unsigned int key);
	virtual void onKeyUp (const unsigned int key);

	void setManejarEventos (const bool);

private:
	// M�todos privados
	static WLienzo getLetra (const unsigned char c);
	static WLienzo getLetra (const unsigned char c, const size_t fontSize);
	WLienzo pintar (const std::string & str);

private:
	// �rea de datos privada
	struct Fila
	{
		std::string texto;
		WLienzo lienzo;
	};
	typedef std::vector<Fila> Filas;
	Filas filas;
	size_t filaCursor;
	size_t columnaCursor;
	bool shift_key;

	bool textoEditable;
	bool manejarEventos;
};

#endif // _W_TEXT_CONTROL_H_
