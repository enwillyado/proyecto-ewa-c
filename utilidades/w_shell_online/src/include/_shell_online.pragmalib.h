/*********************************************************************************************
 *	Name		: _shell_online.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _SHELL_ONLINE_PRAGMALIB_H_
#define _SHELL_ONLINE_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

// Parte privada
#include "w_shell_online/_w_shell_online.pragmalib.h"

#endif

#undef _SHELL_ONLINE_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _SHELL_ONLINE_PRAGMALIB_H_
