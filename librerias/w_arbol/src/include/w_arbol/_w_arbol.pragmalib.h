/*********************************************************************************************
 *	Name		: _w_arbol.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar la librer�a de lectura de XML
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ARBOL_PRAGMALIB_H_
#define _W_ARBOL_PRAGMALIB_H_

#ifdef _MSC_VER

#include "w_base/_pragmalib_base.h"

// Parte p�blica
#pragma comment(lib, "w_arbol" FIN_LIB)

#endif

#undef _W_ARBOL_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_ARBOL_PRAGMALIB_H_
