/*********************************************************************************************
* Name			: w_red_tester_files.cpp
* Description	: Pruebas unitarias que se realizarán sobre files
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema.h"

#include <assert.h>

#include <iostream>

// ----------------------------------------------------------------------------
// Execute Files
int executeFiles()
{
	assert (false == WFiles::fileExists (""));
	return 0;
}
