/*********************************************************************************************
 * Name			: w_ar_drone_controlador.cpp
 * Description	: Implementación de la libería de controlador del AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones/w_ar_drone_controlador.h"

#include "ewadrones/ewadrones.h"
#include "ewadrones/ar_drone_puertos.hpp"

#include "w_sistema/w_sistema_time.h"

ArDroneControlador::ArDroneControlador (EWadrones* ptrArDrone)
	: clienteComandos(), arDrone (ptrArDrone), salidaComandosHabilitada (false), salidaComandos()
{
}

void ArDroneControlador::setPtrArDrone (EWadrones* ptrArDrone)
{
	this->arDrone = ptrArDrone;
}

void ArDroneControlador::init()
{
	if (this->arDrone != NULL)
	{
		const std::string & host = arDrone->getHost();
		const bool abierto = this->clienteComandos.openSocket (host, PUERTO_COMANDOS_EN_PROXY);
		if (true == abierto)
		{
#ifdef INICIO_AUTOMATICO
			this->start();
#endif
		}
	}
}

// ----------------------------------------------------------------------------
void ArDroneControlador::start()
{
	this->hiloComandos.setData (TWThreadData<ArDroneControlador*> (this));
	this->hiloComandos.setFunction (&ArDroneControlador::initControlUDP);
	this->hiloComandos.start();
}
void ArDroneControlador::stop()
{
	this->hiloComandos.stop();
}

void ArDroneControlador::initControlUDP (const WThread & hilo)
{
	ArDroneControlador* data = hilo.getData<TWThreadData<ArDroneControlador*>>().data;
	while (false == hilo.isStoped())
	{
		WTime::esperarMilisegundos (100);
		// Se envía directamente el mensaje de sustenciación
		data->clienteComandos.enviar ("AT*COMWDG=0\r");
	}
}

// ----------------------------------------------------------------------------
void ArDroneControlador::enviarMensaje (const std::string & mensaje)
{
	this->clienteComandos.enviar (mensaje);
	if (true == salidaComandosHabilitada)
	{
		this->salidaComandos += mensaje;
	}
}

// ----------------------------------------------------------------------------
const std::string & ArDroneControlador::getSalidaComandos() const
{
	return this->salidaComandos;
}
void ArDroneControlador::clearSalidaComandos()
{
	this->salidaComandos.clear();
}
void ArDroneControlador::habilitarSalidaComandos (const bool salidaComandosHabilitada)
{
	this->salidaComandosHabilitada = salidaComandosHabilitada;
}

// ----------------------------------------------------------------------------
void ArDroneControlador::despegar()
{
	this->ajustarHorizontal();

	// Takeoff:AT*REF=101,290718208
	this->enviarMensaje ("AT*REF=1,290718208\r");
}

void ArDroneControlador::aterrizar()
{
	this->estabilizar();

	// Landing:AT*REF=102,290717696
	this->enviarMensaje ("AT*REF=1,290717696\r");
}

void ArDroneControlador::emergencia()
{
	// emergency reset: AT*REF=[Sequence number],290717952
	this->enviarMensaje ("AT*REF=1,290717952\r");
}

// ----------------------------------------------------------------------------
void ArDroneControlador::ajustarHorizontal()
{
	// AT*FTRIM=[Sequence number]<LF>
	this->enviarMensaje ("AT*FTRIM=1\r");
}

// ----------------------------------------------------------------------------
void ArDroneControlador::estabilizar()
{
	// Hovering: AT*PCMD=[Sequence number],1,0,0,0,0
	this->enviarMensaje ("AT*PCMD=1,1,0,0,0,0\r");
}

// ----------------------------------------------------------------------------
void ArDroneControlador::arriba()
{
	// gaz 0.1: AT*PCMD=[Sequence number],1,0,0,1036831949,0
	// gaz 0.5: AT*PCMD=[Sequence number],1,0,0,1056964608,0
	this->enviarMensaje ("AT*PCMD=1,1,0,0,1056964608,0\r");
}

void ArDroneControlador::abajo()
{
	// gaz -0.1: AT*PCMD=[Sequence number],1,0,0,-1110651699,0
	// gaz -0.5: AT*PCMD=[Sequence number],1,0,0,-1090519040,0
	this->enviarMensaje ("AT*PCMD=1,1,0,0,-1090519040,0\r");
}

// ----------------------------------------------------------------------------
void ArDroneControlador::defrente()
{
	// roll -0.1: AT*PCMD=[Sequence number],1,0,-1110651699,0,0
	// roll -0.5: AT*PCMD=[Sequence number],1,0,-1090519040,0,0
	this->enviarMensaje ("AT*PCMD=1,1,0,-1090519040,0,0\r");
}

void ArDroneControlador::atras()
{
	// roll 0.1: AT*PCMD=[Sequence number],1,0,1036831949,0,0
	// roll 0.5: AT*PCMD=[Sequence number],1,0,1056964608,0,0
	this->enviarMensaje ("AT*PCMD=1,1,0,1056964608,0,0\r");
}

// ----------------------------------------------------------------------------
void ArDroneControlador::izquierda()
{
	// pitch -0.1: AT*PCMD=[Sequence number],1,-1110651699,0,0,0
	// pitch -0.5: AT*PCMD=[Sequence number],1,-1090519040,0,0,0
	this->enviarMensaje ("AT*PCMD=1,1,-1090519040,0,0,0\r");
}

void ArDroneControlador::derecha()
{
	// pitch 0.1: AT*PCMD=[Sequence number],1,1036831949,0,0,0
	// pitch 0.5: AT*PCMD=[Sequence number],1,1056964608,0,0,0
	this->enviarMensaje ("AT*PCMD=1,1,1056964608,0,0,0\r");
}

// ----------------------------------------------------------------------------
void ArDroneControlador::rotarIzquierda()
{
	// yaw -0.1: AT*PCMD=[Sequence number],1,0,0,0,-1110651699
	// yaw -0.5: AT*PCMD=[Sequence number],1,0,0,0,-1090519040
	this->enviarMensaje ("AT*PCMD=1,1,0,0,0,-1090519040\r");
}

void ArDroneControlador::rotarDerecha()
{
	// yaw 0.1: AT*PCMD=[Sequence number],1,0,0,0,1036831949
	// yaw 0.5: AT*PCMD=[Sequence number],1,0,0,0,1056964608
	this->enviarMensaje ("AT*PCMD=1,1,0,0,0,1056964608\r");
}

// ----------------------------------------------------------------------------
/*
	Hovering:AT*PCMD=201,1,0,0,0,0

	(float)0.05 = (int)1028443341       (float)-0.05 = (int)-1119040307
	(float)0.1  = (int)1036831949       (float)-0.1  = (int)-1110651699
	(float)0.2  = (int)1045220557       (float)-0.2  = (int)-1102263091
	(float)0.5  = (int)1056964608       (float)-0.5  = (int)-1090519040

*/
