/*********************************************************************************************
 *	Name		: ewadrones_control_vision.h
 *	Description	: Clase auxiliar para ejecutar el reconocimiento de im�genes
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_CONTROL_VISION_H_
#define _EWADRONES_CONTROL_VISION_H_

#include "w_graficos/w_lienzo.h"
#include "w_graficos/w_xlienzo.h"
#include "w_graficos/w_rgba.h"

#define ANCHO_VIDEO	(800/2)
#define ALTO_VIDEO	(600/2)

// M�todos para tratar los fotogramas recibidos
class Video
{
public:
	static bool pixelRojo (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y);
	static bool pixelMascara (WLienzoBase &, RGBA & color, const size_t x, const size_t y);
	static bool calcularQR (WLienzoBase & lienzo);
	static void onNewLienzo (const WLienzo & newLienzo);
};

#endif // _EWADRONES_CONTROL_VISION_H_
