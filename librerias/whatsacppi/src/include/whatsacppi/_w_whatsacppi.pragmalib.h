/*********************************************************************************************
*	Name		: _w_zxing.pragmalib.h
*	Description	: Librer�as necesarias para enlazar con la libre�a ZXING del Proyecto eWa
* Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ZXING_PRAGMALIB_H_
#define _W_ZXING_PRAGMALIB_H_

#ifdef _MSC_VER

#include "w_base/_pragmalib_base.h"

// Parte privada
#pragma comment(lib, "w_zxing" FIN_LIB)

#include "zxing/_zxing.pragmalib.h"
#include "w_graficos/_w_imagenes.pragmalib.h"

#endif

#undef _W_ZXING_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_ZXING_PRAGMALIB_H_
