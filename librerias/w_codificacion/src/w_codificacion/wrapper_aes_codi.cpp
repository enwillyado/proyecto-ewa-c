/*********************************************************************************************
 *	Name		: wrapper_aes_codi.cpp
 *	Description	: Wrapper de C++ para algoritmo de AES soportando los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion.h"

//____________________________________________________________________________________________
//
// Encriptar
//____________________________________________________________________________________________

EEncryptResult aesEncryptBuffer (const std::string & buffeIn, std::string & pBufferEncryptOut, const std::string & contr)
{
	AES_KEY key;
	const EEncryptResult result = contrToAESKeyEncrypt (key, contr);
	if (result != EncryptOk)
	{
		return result;
	}
	return aesDesencryptBuffer (buffeIn, pBufferEncryptOut, key);
}

EEncryptResult aesEncryptBuffer (const std::string & buffeIn, std::string & pBufferEncryptOut, const AES_KEY key)
{
	size_t encriptadoSize = AesCalculateSizeEncryptBuffer (buffeIn.size());
	unsigned char* encriptado = new unsigned char [encriptadoSize];
	EEncryptResult result = aesEncryptBuffer (buffeIn.c_str(), buffeIn.size(), encriptado, encriptadoSize, key);
	if (result != EncryptOk)
	{
		delete[] encriptado; // borrar buffer de la cadena encriptada si falla
		pBufferEncryptOut = "";
		return result;
	}

	// Pasar el buffer encriptado a la variable de salida
	pBufferEncryptOut.resize (encriptadoSize);
	for (size_t i = 0; i < pBufferEncryptOut.size(); i++)
	{
		pBufferEncryptOut[i] = encriptado[i];
	}

	delete[] encriptado; // borrar buffer de la cadena encriptada si falla
	return EncryptOk;
}
