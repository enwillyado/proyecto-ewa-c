/*********************************************************************************************
 * Name			: w_eventoKey.h
 * Description	: Evento para los desencadenadores de teclado
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_EVENTO_KEY_H_
#define _W_EVENTO_KEY_H_

#include "w_graficos/w_evento.h"

class WEventoKey : public WEvento
{
public:
	typedef void (*CbEventoKey) (const WEventoKey &);

public:
	// Constructor
	WEventoKey (WControl & iControl, const unsigned int & iKey)
		: WEvento (iControl), key (iKey)
	{
	}

	const unsigned int & key;
};

#endif // _W_EVENTO_KEY_H_
