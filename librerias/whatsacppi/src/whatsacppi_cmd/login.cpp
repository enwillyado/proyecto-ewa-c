#include <iostream>
#include <string>
#include "whatsacppi/phone.h"
#include "whatsacppi/register.h"
#include "whatsacppi/util/log.h"
#include "whatsacppi/util/util.h"
#include "whatsacppi/util/sha1.h"
#include "whatsacppi/protocol/wa.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

using WhatsAcppi::Phone;
using WhatsAcppi::Util::Log;
using WhatsAcppi::Util::sha1Str;
using WhatsAcppi::Protocol::WA;

int mainLogin (int argc, char* argv[])
{
	if (argc < 4)
	{
		cout << "Ussage:" << endl;
		cout << "\t" << argv[0] << " PHONE_NUMBER USERNAME PASSWORD [CARRIER NAME]" << endl;
		return 0;
	}

	WhatsAcppi::Util::Init init;
	Log::setLogLevel (Log::DebugMsg);

	Phone phone (argv[1]);
	if (argc > 4)
	{
		phone.guessPhoneInformation (argv[3]);
	}
	else
	{
		phone.guessPhoneInformation();
	}


#define PRINT(X) cout << #X << ": " << phone.get##X() << endl
	PRINT (PhoneNumber);
	PRINT (Phone);
	PRINT (Country);
	PRINT (Cc);
	PRINT (Iso3166);
	PRINT (Iso639);
#undef PRINT

	string identity = sha1Str (argv[2]);
	cout << "identity: " << identity << endl;

	cout << "** Trying to login" << endl;

	WA wa (phone, identity, argv[2]);
	wa.connect();
	wa.login (argv[3]);

	return 0;
}
