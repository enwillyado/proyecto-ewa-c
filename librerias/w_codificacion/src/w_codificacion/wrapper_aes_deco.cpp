/*********************************************************************************************
 *	Name		: wrapper_aes_deco.cpp
 *	Description	: Wrapper de C++ para algoritmo de AES soportando los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion.h"

//____________________________________________________________________________________________
//
// Desencriptar
//____________________________________________________________________________________________

EEncryptResult aesDesencryptBuffer (const std::string & pBufferEncryptIn, std::string & buffeOut, const std::string & contr)
{
	AES_KEY key;
	const EEncryptResult result = contrToAESKeyEncrypt (key, contr);
	if (result != EncryptOk)
	{
		return result;
	}
	return aesDesencryptBuffer (pBufferEncryptIn, buffeOut, key);
}

EEncryptResult aesDesencryptBuffer (const std::string & pBufferEncryptIn, std::string & buffeOut, const AES_KEY key)
{
	unsigned char* serializadoBuffer = new unsigned char [pBufferEncryptIn.size()];
	const EEncryptResult result = aesDesencryptBuffer (pBufferEncryptIn.c_str(), pBufferEncryptIn.size(), serializadoBuffer, pBufferEncryptIn.size(), key);
	if (result != EncryptOk)
	{
		delete[] serializadoBuffer; // borrar buffer de la cadena serializada si falla
		buffeOut = "";
		return result;
	}

	buffeOut.resize (pBufferEncryptIn.size());
	for (size_t i = 0; i < pBufferEncryptIn.size(); i++)
	{
		buffeOut[i] = serializadoBuffer[i];
	}
	delete[] serializadoBuffer; // borrar buffer de la cadena serializada si falla

	// NOTA: hay que reconstruirlo para eliminar "\0" al final del base64Encode
	buffeOut = std::string (buffeOut.c_str());

	return result;
}
