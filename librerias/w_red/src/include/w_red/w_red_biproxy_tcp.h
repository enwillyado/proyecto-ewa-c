/*********************************************************************************************
*	Name		: w_red_biproxy_tcp.h
*	Description	: Clase para implementar un biproxy de red basado en TCP
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_BIPROXY_TCP_H_
#define _W_RED_BIPROXY_TCP_H_

#include "w_red/w_red_proxy_tcp.h"

// Clase bi-proxy
class BiProxyTCP : public ProxyTCP
{
public:
	BiProxyTCP (const std::string & remoteHost, const int remotePort, const int biLocalPort);
	virtual void onClienteGetData (const SocketID socketID, const std::string & mensaje);

public:
	static void funcionHilo (const WThread & hilo);

protected:
	static void biServidor (ServidorTCP & servidor);

private:
	SocketID biSocketID;
	std::string mensaje0;
	ServidorTCP biServidorTCP;
};

#endif //_W_RED_BIPROXY_TCP_H_
