/*********************************************************************************************
 *	Name		: w_make_wmake.cpp
 *	Description	: Implementanción de las funciones para preparar una orden VMAKE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_make/w_make.h"

#include "w_arbol/w_arbolxml.h"

WMake::Estado WMake::prepararWMK()
{
	const Arbol & wmake = ArbolXML::getArbolFromFile (this->ficheroOrden);
	if (wmake.getLastError() != Arbol::SIN_ERROR)
	{
		return WMake::NO_EXISTE_FICHERO;
	}
	if (wmake.getClave() != "WMake")
	{
		return WMake::ORDEN_MAL_FORMADA;
	}

	// Leer datos
	const Arbol & datos = wmake["Datos"];
	for (size_t i = 0; i < datos.hijosize(); i++)
	{
		const Arbol & dato = datos[i];
		const std::string & valorItem = dato % "valor";
		const std::string & tipoDato = dato.getClave();
		if (tipoDato == "OutBinDir")
		{
			// TODO: hacer absoluto si NO lo es
			this->parametros.dirBinDestino = valorItem;
		}
		else if (tipoDato == "OutObjDir")
		{
			// TODO: hacer absoluto si NO lo es
			this->parametros.dirObjDestino = valorItem;
		}
		else if (tipoDato == "IncludeDir")
		{
			// TODO: hacer absoluto si NO lo es
			this->parametros.carpetasInclusion.push_back (valorItem);
		}
		else if (tipoDato == "IncludeLibDir")
		{
			// TODO: hacer absoluto si NO lo es
			this->parametros.carpetasInclusionLib.push_back (valorItem);
		}
		else
		{
			return WMake::ORDEN_MAL_FORMADA;
		}
	}

	// Leer ordenes
	const Arbol & ordenes = wmake["Ordenes"];
	for (size_t iOrden = 0; iOrden < ordenes.hijosize(); iOrden++)
	{
		const Arbol & orden = ordenes[iOrden];
		if (orden.getClave() != "Orden")
		{
			return WMake::ORDEN_MAL_FORMADA;
		}

		WMake makeOrden;
		makeOrden.tipoOrden = WMake::ORDEN;
		makeOrden.parametros = this->parametros;
		for (size_t i = 0; i < orden.hijosize(); i++)
		{
			const Arbol & item = orden[i];
			const std::string & valorItem = item % "valor";
			const std::string & tipoItem = item.getClave();
			if (tipoItem == "source")
			{
				makeOrden.parametros.pushSrcOrg (valorItem);
			}
			else if (tipoItem == "lib")
			{
				makeOrden.parametros.pushLibOrg (valorItem);
			}
			else
			{
				return WMake::ORDEN_MAL_FORMADA;
			}
		}

		const std::string & nombreOrden = orden % "nombre";
		makeOrden.parametros.nombre = nombreOrden;

		const std::string & tipoOrden = orden % "tipo";
		if (tipoOrden == "lib")
		{
			makeOrden.parametros.tipo = WMakeParametros::LIBRERIA_LIB;
			makeOrden.prepararLib();
		}
		else if (tipoOrden == "exe")
		{
			makeOrden.parametros.tipo = WMakeParametros::EJECUTABLE;
			makeOrden.prepararExe();
		}
		else
		{
			return WMake::ORDEN_MAL_FORMADA;
		}
	}

	return WMake::CORRECTO;
}
