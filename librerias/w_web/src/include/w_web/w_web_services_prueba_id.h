/*********************************************************************************************
 * Name			: w_web_services_prueba_id.h
 * Description	: Identificadores (como string) de los servicios de prueba que se ejecutar�n
 *					en el Servidor usando polimorfismo (al menos los p�blicos)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_SERVICES_PRUEBA_ID_H_
#define _W_WEB_SERVICES_PRUEBA_ID_H_

// Servicios para cuando se hagan pruebas
#define SRVSERVICE_PRUEBA_A_ID	"/prueba"
#define SRVSERVICE_PRUEBA_B_ID	"/prueba/*"
#define SRVSERVICE_PRUEBA_C_ID	"/prueba/*/uno"
#define SRVSERVICE_PRUEBA_D_ID	"/prueba/*/dos"
#define SRVSERVICE_PRUEBA_E_ID	"/prueba/*/dos/*"
#define SRVSERVICE_PRUEBA_F_ID	"/prueba/*/tres/*/rojo"
#define SRVSERVICE_PRUEBA_G_ID	"/prueba/*/tres/*/azul"
#define SRVSERVICE_PRUEBA_H_ID	"/prueba/*/cuatro/*"
#define SRVSERVICE_IPANDPORT_ID "/ipandport"

#endif	//_W_WEB_SERVICES_PRUEBA_ID_H_
