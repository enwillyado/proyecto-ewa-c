/*********************************************************************************************
 * Name			: w_vScrollControl.cpp
 * Description	: Implementaci�n de la clase para gestionar un control de scroll vertical (nativo)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_vScrollControl.h"

#include "w_graficos/w_rgba.h"
#include "w_graficos/w_app.h"

#include "w_sistema/w_sistema_timer.h"

// ----------------------------------------------------------------------------
// Base para los controles internos
// Constructores
WBotonScrollBaseControl::WBotonScrollBaseControl (const std::string & iName)
	: WControl (iName)
{
}

// Modificador
void WBotonScrollBaseControl::setVScrolControl (WVScrollControl* const iVScroll)
{
	vScroll = iVScroll;
}

// ----------------------------------------------------------------------------
// Controles internos: bot�n
// Constructores
WBotonScrollControl::WBotonScrollControl (const std::string & iName)
	: WBotonScrollBaseControl (iName), timer (NULL)
{
	this->setLienzo (WLienzo::createWLienzo (10, 10, RGBA (0, 0, 0xFF)));
}

void WBotonScrollControl::onLDownMouseOver()
{
	this->onXDownMouseOver();
}
void WBotonScrollControl::onMDownMouseOver()
{
	this->onXDownMouseOver();
}
void WBotonScrollControl::onRDownMouseOver()
{
	this->onXDownMouseOver();
}

void WBotonScrollControl::onXDownMouseOver()
{
	this->setTimerScroll (true);
}

void WBotonScrollControl::setTimerScroll (const bool encendido)
{
	if (encendido == true)
	{
		class TimerScroll
		{
		public:
			static void on (WTimer & timer)
			{
				WBotonScrollControl & self = timer.getData<WBotonScrollControl>();
				self.onScroll();
			}
		};
		if (this->timer == NULL)
		{
			this->onScroll();
			this->timer = new WTimer (&TimerScroll::on, 100);
			this->timer->setDataPtr (this);
			this->timer->start();
		}
	}
	else
	{
		if (this->timer != NULL)
		{
			this->timer->end();
			delete this->timer;
			this->timer = NULL;
		}
	}
}

void WBotonScrollControl::onLUpMouseOver()
{
	this->onXUpMouse();
}
void WBotonScrollControl::onMUpMouseOver()
{
	this->onXUpMouse();
}
void WBotonScrollControl::onRUpMouseOver()
{
	this->onXUpMouse();
}

void WBotonScrollControl::onLUpMouseOut()
{
	this->onXUpMouse();
}
void WBotonScrollControl::onMUpMouseOut()
{
	this->onXUpMouse();
}
void WBotonScrollControl::onRUpMouseOut()
{
	this->onXUpMouse();
}

void WBotonScrollControl::onXUpMouse()
{
	this->setTimerScroll (false);
}

void WBotonScrollControl::onMoveMouseEnter()
{
	if (this->timer != NULL)
	{
		this->timer->start();
	}
	WBotonScrollBaseControl::onMoveMouseEnter();
}
void WBotonScrollControl::onMoveMouseOut()
{
	if (this->timer != NULL)
	{
		this->timer->stop();
	}
	WBotonScrollBaseControl::onMoveMouseOut();
}

void WBotonScrollControl::onScroll()
{
	if (this->WBotonScrollBaseControl::vScroll != NULL)
	{
		if (this->getName() == "botonArriba")
		{
			this->WBotonScrollBaseControl::vScroll->onScrollUp();
		}
		else if (this->getName() == "botonAbajo")
		{
			this->WBotonScrollBaseControl::vScroll->onScrollDown();
		}
	}
}
// ----------------------------------------------------------------------------
// Controles internos: bot�n
// Constructores
WZonaScrollControl::WZonaScrollControl (const std::string & iName)
	: WBotonScrollBaseControl (iName)
{
}

void WZonaScrollControl::resize (const size_t ancho, const size_t alto)
{
	this->WControl::resize (ancho, alto);
	this->setLienzo (WLienzo::createWLienzo (this->getAncho(), this->getAlto(), RGBA (0xFF, 0, 0xFF)));
}

void WZonaScrollControl::onLClicMouseOver()
{
	this->onXClicMouseOver();
}
void WZonaScrollControl::onMClicMouseOver()
{
	this->onXClicMouseOver();
}
void WZonaScrollControl::onRClicMouseOver()
{
	this->onXClicMouseOver();
}

void WZonaScrollControl::onXClicMouseOver()
{
	if (this->WBotonScrollBaseControl::vScroll != NULL)
	{
		const WCoordenada & coordenada = this->getPunteroPos();
		const size_t semiAlto = this->getAlto() / 2;
		if (coordenada.posY <= semiAlto)
		{
			this->WBotonScrollBaseControl::vScroll->onScrollUp();
		}
		else
		{
			this->WBotonScrollBaseControl::vScroll->onScrollDown();
		}
	}
}

// ----------------------------------------------------------------------------
// Controles externo (final)

// Constructores
WVScrollControl::WVScrollControl (const std::string & iName)
	: WMultiControl (iName),
	  areaScroll ("areaScroll"), botonArriba ("botonArriba"), botonAbajo ("botonAbajo")
{
	this->init();

	// Suscribirse eventos
	class Auxiliar
	{
	public:
		static void invert (const WEventoMouse & e)
		{
			e.getSelf().getLienzo().invertColor();
			e.getSelf().getParent().recomponerLienzo();
		}
	};

	this->botonArriba.setOnMoveMouseEnter (&Auxiliar::invert);
	this->botonArriba.setOnMoveMouseOut (&Auxiliar::invert);
	this->botonAbajo.setOnMoveMouseEnter (&Auxiliar::invert);
	this->botonAbajo.setOnMoveMouseOut (&Auxiliar::invert);
}
WVScrollControl::WVScrollControl (const WVScrollControl & org)
	: WMultiControl (org), WControlScrollable (org),
	  areaScroll (org.areaScroll), botonArriba (org.botonArriba), botonAbajo (org.botonAbajo)
{
	this->init();
}

// Inicializador
void WVScrollControl::init()
{
	this->areaScroll.setVScrolControl (this);
	this->botonArriba.setVScrolControl (this);
	this->botonAbajo.setVScrolControl (this);

	this->WMultiControl::insertarPtr (&this->areaScroll, false);
	this->WMultiControl::insertarPtr (&this->botonArriba, false);
	this->WMultiControl::insertarPtr (&this->botonAbajo, false);
}

// Modificadores
void WVScrollControl::resize (const size_t ancho, const size_t alto)
{
	this->WControl::resize (ancho, alto);
	this->areaScroll.resize (ancho, alto);

	// Reposicionar
	this->botonAbajo.setPosY (this->getAlto() - 10);
	this->WMultiControl::recomponerLienzo();
}

// Observadores virtuales
WControl & WVScrollControl::getSelf()
{
	return WMultiControl::getSelf();
}
const WControl & WVScrollControl::getSelf() const
{
	return WMultiControl::getSelf();
}

// Manejadores
void WVScrollControl::onWheelMouseOver (const WEventoMouseWheel::Direccion direccion)
{
	if (direccion == WEventoMouseWheel::ARRIBA)
	{
		this->onScrollUp();
	}
	else if (direccion == WEventoMouseWheel::ABAJO)
	{
		this->onScrollDown();
	}
}
