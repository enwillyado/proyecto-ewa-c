/*********************************************************************************************
 *	Name		: zxing_cmd.cpp
 *	Description	: Ejecutable donde se implementa un ejemplo para leer c�digos en BMP
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "zxing/w_zxing_result.h"

WZxingResult::WZxingResult()
	: formato (WZxingResult::FORMATO_DESCONOCIDO)
{
}

const WZxingResult::Formato & WZxingResult::getFormato() const
{
	return this->formato;
}
void WZxingResult::setFormato (const Formato & formato)
{
	this->formato = formato;
}

// Coordenadas
const WZxingResult::WCoordenadas & WZxingResult::getCoordenadas() const
{
	return this->coordenadas;
}
void WZxingResult::clearCoordeandas()
{
	this->coordenadas.clear();
}
void WZxingResult::pushCoordenada (const WCoordenada & coordenada)
{
	this->coordenadas.push_back (coordenada);
}

// Texto
const std::string & WZxingResult::getTexto() const
{
	return this->texto;
}
void WZxingResult::setTexto (const std::string & texto)
{
	this->texto = texto;
}
