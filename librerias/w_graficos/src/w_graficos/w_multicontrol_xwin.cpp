/*********************************************************************************************
 * Name			: w_multicontrol.cpp
 * Description	: Implementaci�n de la clase para gestionar un control m�ltiple
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_multicontrol.h"

#include "w_graficos/w_rgba.h"

// Controles particulares
#include "w_graficos/w_textControl.h"
#include "w_graficos/w_lienzo_bmp.h"
#include "w_graficos/w_xlienzo.h"

#include "w_arbol/w_arbol.h"		//< Arbol

#include "w_base/fromStr.hpp"		//< ::fromStr


// Carga una maquetaci�n
bool WMultiControl::loadXW (const Arbol & arbol, const AppFunctores & functores)
{
	const std::string & raiz = arbol.getClave();

	// Primero, los casos particulares
	if (raiz == "texto")
	{
		const std::string & nombre = arbol.getValorAtributo ("nombre");
		WTextControl & textControl = this->insertarCopia (WTextControl (nombre));

		const std::string & texto = arbol.getValor();
		textControl.setText (texto);
		textControl.setManejarEventos (false);
		textControl.pintar();
		return true;
	}
	else if (raiz == "bmp")
	{
		const std::string & nombre = arbol.getValorAtributo ("nombre");
		WControl & control = this->insertarCopia (WControl (nombre));

		const std::string & fichero = arbol.getValorAtributo ("fichero");
		control.setLienzo (WLienzoBMP::getWLienzoBMPFromFile (fichero));
		return true;
	}
	else if (raiz == "xlz")
	{
		const std::string & nombre = arbol.getValorAtributo ("nombre");
		WControl & control = this->insertarCopia (WControl (nombre));

		const std::string & fichero = arbol.getValorAtributo ("fichero");
		control.setLienzo (WXLienzo::getWXLienzoFromFile (fichero));
		return true;
	}

	// Por �ltimo, el caso general (el resto)
	if (raiz != "div")
	{
		return false;
	}

	// Lo creamos y metemos en el multicontrol actual
	const std::string & nombre = arbol.getValorAtributo ("nombre");
	WMultiControl & div = this->insertarCopia (WMultiControl (nombre));

	// Procesar atributos:
	{
		{
			// Dimensiones
			size_t anchoNum = 0;
			size_t altoNum = 0;
			bool cambiaValores = false;

			// Ver si cambia el ancho
			const std::string & anchoNuevo = arbol.getValorAtributo ("ancho");
			if (anchoNuevo != "")
			{
				const size_t anchoNumNuevo = ::fromStr<size_t> (anchoNuevo);
				cambiaValores = true;
				anchoNum = anchoNumNuevo;
			}

			// Ver si cambia el alto
			const std::string & altoNuevo = arbol.getValorAtributo ("alto");
			if (altoNuevo != "")
			{
				const size_t altoNumNuevo = ::fromStr<size_t> (altoNuevo);
				cambiaValores = true;
				altoNum = altoNumNuevo;
			}

			// Cambiarlos si al menos uno cambi�
			if (true == cambiaValores)
			{
				div.setSize (anchoNum, altoNum);
			}
		}
		{
			// Posiciones
			size_t xNum = 0;
			size_t yNum = 0;
			bool cambiaValores = false;

			// Ver si cambia el ancho
			const std::string & xNuevo = arbol.getValorAtributo ("x");
			if (xNuevo != "")
			{
				const size_t xNumNuevo = ::fromStr<size_t> (xNuevo);
				cambiaValores = true;
				xNum = xNumNuevo;
			}

			// Ver si cambia el alto
			const std::string & yNuevo = arbol.getValorAtributo ("y");
			if (yNuevo != "")
			{
				const size_t yNumNuevo = ::fromStr<size_t> (yNuevo);
				cambiaValores = true;
				yNum = yNumNuevo;
			}

			// Cambiarlos si al menos uno cambi�
			if (true == cambiaValores)
			{
				div.setPos (xNum, yNum);
			}
		}
		{
			// Ver si cambia el fondo
			const std::string & fondo = arbol.getValorAtributo ("fondo");
			if (fondo != "")
			{
				div.setLienzo (WLienzo::createWLienzo (div.getAncho(), div.getAlto(), RGBA (fondo)));
			}

			// Ver si cambia el borde
			const std::string & borde = arbol.getValorAtributo ("borde");
			if (fondo != "")
			{
				div.getLienzo().setBorder (RGBA (borde));
			}
		}

		// Ajustar acciones
		{
			// onKeyUp
			const std::string & onKeyUp = arbol.getValorAtributo ("onKeyUp");
			if (onKeyUp != "")
			{
				AppFunctores::FunctorOnKey functor = functores.getFunctorOnKey (onKeyUp);
				if (functor != NULL)
				{
					div.setOnKeyUp (functor);
				}
			}

			// onKeyDown
			const std::string & onKeyDown = arbol.getValorAtributo ("onKeyDown");
			if (onKeyDown != "")
			{
				AppFunctores::FunctorOnKey functor = functores.getFunctorOnKey (onKeyDown);
				if (functor != NULL)
				{
					div.setOnKeyDown (functor);
				}
			}
		}
		{
			// onLClicMouseOver
			const std::string & onLClicMouseOver = arbol.getValorAtributo ("onLClicMouseOver");
			if (onLClicMouseOver != "")
			{
				AppFunctores::FunctorWEventoMouse functor = functores.getFunctorWEventoMouse (onLClicMouseOver);
				if (functor != NULL)
				{
					div.setOnLClicMouseOver (functor);
				}
			}

			// onMClicMouseOver
			const std::string & onMClicMouseOver = arbol.getValorAtributo ("onLClicMouseOver");
			if (onMClicMouseOver != "")
			{
				AppFunctores::FunctorWEventoMouse functor = functores.getFunctorWEventoMouse (onMClicMouseOver);
				if (functor != NULL)
				{
					div.setOnMClicMouseOver (functor);
				}
			}

			// onRClicMouseOver
			const std::string & onRClicMouseOver = arbol.getValorAtributo ("onRClicMouseOver");
			if (onRClicMouseOver != "")
			{
				AppFunctores::FunctorWEventoMouse functor = functores.getFunctorWEventoMouse (onRClicMouseOver);
				if (functor != NULL)
				{
					div.setOnRClicMouseOver (functor);
				}
			}

			// onMoveMouseOver
			const std::string & onMoveMouseOver = arbol.getValorAtributo ("onMoveMouseOver");
			if (onMoveMouseOver != "")
			{
				AppFunctores::FunctorWEventoMouse functor = functores.getFunctorWEventoMouse (onMoveMouseOver);
				if (functor != NULL)
				{
					div.setOnMoveMouseOver (functor);
				}
			}
		}
		{
			// setOnLMouseDrag
			const std::string & onLMouseDrag = arbol.getValorAtributo ("onLMouseDrag");
			if (onLMouseDrag != "")
			{
				AppFunctores::FunctorWEventoMouseDrag functor = functores.getFunctorWEventoMouseDrag (onLMouseDrag);
				if (functor != NULL)
				{
					div.setOnLMouseDrag (functor);
				}
			}

			// setOnMMouseDrag
			const std::string & onMMouseDrag = arbol.getValorAtributo ("onMMouseDrag");
			if (onMMouseDrag != "")
			{
				AppFunctores::FunctorWEventoMouseDrag functor = functores.getFunctorWEventoMouseDrag (onMMouseDrag);
				if (functor != NULL)
				{
					div.setOnMMouseDrag (functor);
				}
			}

			// setOnRMouseDrag
			const std::string & onRMouseDrag = arbol.getValorAtributo ("onRMouseDrag");
			if (onRMouseDrag != "")
			{
				AppFunctores::FunctorWEventoMouseDrag functor = functores.getFunctorWEventoMouseDrag (onRMouseDrag);
				if (functor != NULL)
				{
					div.setOnRMouseDrag (functor);
				}
			}
		}
	}

	bool ret = true;	//< asumimos que va a ir bien
	for (size_t i = 0; i < arbol.hijosize(); i++)
	{
		const Arbol & arbolHijo = arbol.getHijos()[i];
		ret &= div.loadXW (arbolHijo, functores);
		if (false == ret)
		{
			// si fall�, salir
			break;
		}
	}

	return ret;
}
