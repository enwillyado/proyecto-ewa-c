/*********************************************************************************************
 * Name			: w_web_mensaje_encoding.cpp
 * Description	: Funciones para trabajar con los parámetros enviados en el mensaje
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_web/w_web_mensaje.h"

#include <stdio.h>	// para el "sscanf(...)"
#include <stdlib.h> // para el "atoi"

// ----------------------------------------------------------------------------
// Funciones auxiliares
static std::string replaceString (std::string subject, const std::string & search, const std::string & replace)
{
	size_t pos = 0;
	while ((pos = subject.find (search, pos)) != std::string::npos)
	{
		subject.replace (pos, search.length(), replace);
		pos += replace.length();
	}
	return subject;
}

// ----------------------------------------------------------------------------
/**
 *	Traduce un string codificado en formato URL a formato string
 *	\param	org [in]	el string a descodificar (desde URL-decode a string)
 *  \retval	el string sin codificar
**/
std::string Mensaje::urlDecode (const std::string & org)
{
	std::string ret;
	char ch;
	int ii;
	for (size_t i = 0; i < org.size(); i++)
	{
		if (int (org[i]) == '%')
		{
			sscanf (org.substr (i + 1, 2).c_str(), "%x", &ii);
			ch = static_cast<char> (ii);
			ret += ch;
			i += 2;
		}
		else if (org[i] == '+')
		{
			ret += " ";
		}
		else
		{
			ret += org[i];
		}
	}
	return ret;
}

/**
 *	Traduce un string codificado en formato string a formato URL
 *	\param	org [in]	el string a codificar (desde string a URL-encode)
 *  \retval	el string codificado
 *	//TODO: ver si se puede generalizar de forma similar al ::urlDecode
**/
std::string Mensaje::urlEncode (const std::string & str)
{
	std::string ret = str;
	ret = ::replaceString (ret, "%", "%25");
	ret = ::replaceString (ret, "<", "%3C");
	ret = ::replaceString (ret, "=", "%3D");
	ret = ::replaceString (ret, " ", "%20");
	ret = ::replaceString (ret, "+", "%2B");
	ret = ::replaceString (ret, "&", "%26");
	ret = ::replaceString (ret, "/", "%2F");
	ret = ::replaceString (ret, ">", "%3E");
	ret = ::replaceString (ret, "'", "%27");
	ret = ::replaceString (ret, "\"", "%22");
	return ret;
}
