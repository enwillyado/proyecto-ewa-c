/*********************************************************************************************
 *	Name		: _ewadrones_server.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar el ejecutable para el SERVIDOR WEB
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_SERVER_PRAGMALIB_H_
#define _EWADRONES_SERVER_PRAGMALIB_H_

#ifdef _MSC_VER

// Parte privada
#include "ewadrones/_ewadrones.pragmalib.h"

// Servidor Web
#include "w_web/_w_web_server.pragmalib.h"
#include "w_web/_w_web_protocol.pragmalib.h"
#include "w_web/_w_web_services.pragmalib.h"

#endif

#undef _EWADRONES_SERVER_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _EWADRONES_SERVER_PRAGMALIB_H_
