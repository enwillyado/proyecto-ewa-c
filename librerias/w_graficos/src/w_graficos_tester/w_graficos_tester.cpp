/*********************************************************************************************
 *	Name		: w_graficos_tester.cpp
 *	Description	: Este fichero sirve para configurar las pruebas unitarias que se realizarán
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_graficos_tester.pragmalib.h"

#include "w_graficos/w_rgba.h"
#include "w_graficos/w_lienzo.h"
#include "w_graficos/w_xlienzo.h"
#include "w_graficos/w_lienzo_bmp.h"
#include "w_graficos/w_lienzo_jpeg.h"

#include "w_base/fromFile.hpp"

#include <assert.h>

int main()
{
	// ---------------------------------------------------------------------------
	// RGBA
	{
		const RGBA rgb ("0xFF8844");
		assert (0xFF == rgb.r);
		assert (0x88 == rgb.g);
		assert (0x44 == rgb.b);
		assert (0xFF == rgb.a);
	}

	// ---------------------------------------------------------------------------
	// Lienzos
	{
		WLienzo lienzo;
		// TODO
	}

	// ---------------------------------------------------------------------------
	// Cursor: intentar cargarlo de la carpeta de recursos
	{
		WXLienzo cursor;
		const bool cargado = cursor.leerContenidoFile ("../testdata/cursor.xlz");
		assert (cargado == true);
	}

	// ---------------------------------------------------------------------------
	// LienzoBMP: intentar cargarlo de la carpeta de recursos
	{
		WLienzoBMP lienzo;
		const bool cargado = lienzo.leerContenidoFile ("../testdata/mosca.bmp");
		assert (cargado == true);
		assert (lienzo.getAncho() == 300);
		assert (lienzo.getAlto() == 170);
		assert (lienzo.getPixel (0, 0).r == 0x00);
		assert (lienzo.getPixel (0, 0).g == 0x00);
		assert (lienzo.getPixel (0, 0).b == 0x00);
		assert (lienzo.getPixel (0, 0).a == 0xFF);
	}
	{
		{
			WLienzoJPEG lienzo;
			const bool cargado = lienzo.leerContenidoFile ("../testdata/imagen.jpeg");
			assert (cargado == true);
			assert (lienzo.getAncho() == 640);
			assert (lienzo.getAlto() == 480);
			assert (lienzo.getPixel (0, 0).r == 0x30);
			assert (lienzo.getPixel (0, 0).g == 0x32);
			assert (lienzo.getPixel (0, 0).b == 0x24);
			assert (lienzo.getPixel (0, 0).a == 0xFF);
		}
		{
			const std::string & buffer = ::leerContenidoFile ("../testdata/imagen.jpeg");
			WLienzoJPEG lienzo;
			const bool cargado = lienzo.leerContenidoStr (buffer);
			assert (cargado == true);
			assert (lienzo.getAncho() == 640);
			assert (lienzo.getAlto() == 480);
			assert (lienzo.getPixel (0, 0).r == 0x30);
			assert (lienzo.getPixel (0, 0).g == 0x32);
			assert (lienzo.getPixel (0, 0).b == 0x24);
			assert (lienzo.getPixel (0, 0).a == 0xFF);
		}
	}

	// Finalización correcta
	return 0;
}
