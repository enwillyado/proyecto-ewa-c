/*********************************************************************************************
 *	Name		: w_ar_drone_proxy.cpp
 *	Description	: Ejecutable que implementa el proxy de puertos para AR.DRONE 2
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_ewadrones_proxy_red.pragmalib.h"

#include "w_red/w_red_proxy_tcp.h"
#include "w_red/w_red_proxy_udp.h"

#include "w_red/w_red_biproxy_tcp.h"
#include "w_red/w_red_biproxy_udp.h"

#include "w_red/w_red_bibypass_udp.h"

#include "w_base/interfaces/iStartable.h"

#include "ewadrones/ar_drone_puertos.hpp"

#include <iostream>

// Funci�n manejadora para cada cliente
typedef TWThreadData<IStartable*> TWThreadDataProxy;
static void funcionHilo (const WThread & hilo)
{
	const TWThreadDataProxy & c = hilo.getData<TWThreadDataProxy>();
	c.data->start();
	delete c.data;
}

#include <fstream>
class BiByPassUDP_debug : public BiByPassUDP
{
public:
	BiByPassUDP_debug (const std::string & remoteHost, const int remotePort, const int localPort, const std::string & filename = "log")
		: BiByPassUDP (remoteHost, remotePort, localPort),
		  file (filename, std::ios_base::binary | std::ios_base::out)
	{
	}
	virtual void onRemoteGetData (const EventoServidorUDP & evento, const std::string & mensaje)
	{
		this->file << ">GET>" << std::endl << mensaje << std::endl;
		this->file.flush();
		this->BiByPassUDP::onRemoteGetData (evento, mensaje);
	}
	virtual void onByPassPutData (const EventoServidorUDP & evento, const std::string & mensaje)
	{
		this->file << "<PUT<" << std::endl << mensaje << std::endl;
		this->file.flush();
		this->BiByPassUDP::onByPassPutData (evento, mensaje);
	}
private:
	std::ofstream file;
};

// Funci�n principal
int main()
{
	// ------------------------------------------------------------------------
	// Parrot AR.DRONE 2.0
	{
		BiProxyTCP* ptr = new BiProxyTCP (NOMBRE_DE_SERVIDOR, PUERTO_VIDEO_EN_SERVIDOR, PUERTO_2_VIDEO_EN_PROXY);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_VIDEO_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_VIDEO_EN_PROXY << std::endl;
		}
	}
	{
		BiProxyTCP* ptr = new BiProxyTCP (NOMBRE_DE_SERVIDOR, PUERTO_VIDEO2_EN_SERVIDOR, PUERTO_2_VIDEO2_EN_PROXY);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_VIDEO2_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_VIDEO2_EN_PROXY << std::endl;
		}
	}
	{
		ProxyUDP* ptr = new ProxyUDP (NOMBRE_DE_SERVIDOR, PUERTO_COMANDOS_EN_SERVIDOR);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_COMANDOS_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_COMANDOS_EN_PROXY << std::endl;
		}
	}
	{
		ProxyUDP* ptr = new ProxyUDP (NOMBRE_DE_SERVIDOR, PUERTO_RESPUESTA_EN_SERVIDOR);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_RESPUESTA_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_RESPUESTA_EN_PROXY << std::endl;
		}
	}
	{
		ProxyUDP* ptr = new ProxyUDP (NOMBRE_DE_SERVIDOR, PUERTO_AUTH_EN_SERVIDOR);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_AUTH_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_AUTH_EN_PROXY << std::endl;
		}
	}
	{
		ProxyTCP* ptr = new ProxyTCP (NOMBRE_DE_SERVIDOR, PUERTO_CRITICAL_EN_SERVIDOR);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_CRITICAL_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_CRITICAL_EN_PROXY << std::endl;
		}
	}
	{
		ProxyTCP* ptr = new ProxyTCP (NOMBRE_DE_SERVIDOR, PUERTO_OTHER_EN_SERVIDOR);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_OTHER_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_OTHER_EN_PROXY << std::endl;
		}
	}

	// ------------------------------------------------------------------------
	// Parrot Jumping Sumo
	{
		ProxyTCP* ptr = new ProxyTCP (NOMBRE_DE_SERVIDOR, 44444);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (44444))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_CRITICAL_EN_PROXY << std::endl;
		}
	}
	{
		BiByPassUDP* ptr = new BiByPassUDP (NOMBRE_DE_SERVIDOR, 54321, 54321);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (55555))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_VIDEO_EN_PROXY << std::endl;
		}
	}

	// ------------------------------------------------------------------------
	// TELENT & FTP
	{
		ProxyTCP* ptr = new ProxyTCP (NOMBRE_DE_SERVIDOR, PUERTO_TELNET_EN_SERVIDOR);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_TELNET_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_TELNET_EN_PROXY << std::endl;
		}
	}
	{
		ProxyTCP* ptr = new ProxyTCP (NOMBRE_DE_SERVIDOR, PUERTO_FTP_EN_SERVIDOR);
		TWThreadDataProxy hiloData (ptr);
		if (true == ptr->open (PUERTO_FTP_EN_PROXY))
		{
			WThread hilo (&::funcionHilo, hiloData);
			hilo.start();
		}
		else
		{
			std::cerr << "No se pudo abrir el puerto " << PUERTO_FTP_EN_PROXY << std::endl;
		}
	}

	std::cin.get();
	return 0;
}
