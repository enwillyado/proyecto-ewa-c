/*********************************************************************************************
*	Name		: strTrim.hpp
*	Description	: M�todo para quitar espacios a una cadena
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _STR_TRIM_HPP_
#define _STR_TRIM_HPP_

#define STR_TRIM_DELIMITADORES	" \r\t"

static std::string strTrim (const std::string & s, const std::string & delimitadores = STR_TRIM_DELIMITADORES)
{
	const size_t ini = s.find_first_not_of (delimitadores);
	if (std::string::npos == ini)
	{
		return "";
	}
	const size_t fin = s.find_last_not_of (delimitadores) + 1;
	return s.substr (ini, fin - ini);
}

static std::string strTrimL (const std::string & s, const std::string & delimitadores = STR_TRIM_DELIMITADORES)
{
	const size_t ini = s.find_first_not_of (delimitadores);
	if (std::string::npos == ini)
	{
		return "";
	}
	return s.substr (ini);
}

static std::string strTrimR (const std::string & s, const std::string & delimitadores = STR_TRIM_DELIMITADORES)
{
	const size_t fin = s.find_last_not_of (delimitadores) + 1;
	return s.substr (0, fin);
}

#endif	//_STR_TRIM_HPP_
