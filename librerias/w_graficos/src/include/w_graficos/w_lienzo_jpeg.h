/*********************************************************************************************
 * Name			: w_lienzo_jpeg
 * Description	: Clase para cargar lienzos en JPEG
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_LIENZO_JPEG_H_
#define _W_LIENZO_JPEG_H_

#include "w_graficos/w_lienzo.h"

#include <string>

class WLienzoJPEG: public WLienzo
{
public:
	WLienzoJPEG (const std::string & filename = "");
	static WLienzoJPEG getWLienzoJPEGFromFile (const std::string & filename);
	static WLienzoJPEG getWLienzoJPEGFromStr (const std::string & buffer);

	bool leerContenidoFile (const std::string & filename);
	bool leerContenidoStr (const std::string & buffer);

	/*
	void save (const std::string & filename);
	*/
};

#endif // _W_LIENZO_JPEG_H_
