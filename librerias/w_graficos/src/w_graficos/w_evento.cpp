/*********************************************************************************************
 * Name			: w_evento.cpp
 * Description	: Evento base para los desencadenadores
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_evento.h"

#include "w_graficos/w_control.h"
#include "w_graficos/w_app.h"

WEvento::WEvento (WControl & iControl)
	: control (iControl)
{
}

WControl & WEvento::getSelf() const
{
	return this->control;
}

WApp & WEvento::getRoot() const
{
	return this->control.getRoot();
}
