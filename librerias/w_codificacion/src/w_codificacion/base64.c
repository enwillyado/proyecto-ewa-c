// Funciones para trabajar con cadenas codificadas con Base64

#include <stdlib.h>
#include <string.h>
#include "w_codificacion/w_codificacion_c.h"

#define ERR	-1
#define b64val(c) index64[(unsigned int)(c)]
static const char b64chars[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const int index64[128] =
{
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
	-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
	-1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1
};



/******************************************************************************
 * \brief		Codifica una cadena a otra en base 64
 * \param		[in]	src		Cadena a codificar
 * \param		[in]	srclen	Tamaño de la cadena a codificar
 * \param		[out]	dest	Donde se almacenará
 * \param		[out]	destlen	Espacio existente peara almacenarla
 * \return 0 si ok
 * -1 Error de parametros
 * \see			http://en.wikipedia.org/wiki/Base64
 *****************************************************************************/
int base64_encode (const unsigned char* src, size_t srclen,
				   char* dest, size_t destlen)
{
	// Comprobaciones de parametros
	if (!dest || !src)
	{
		return -1;
	}

	if (destlen < Base64CalculateSizeEncodedBuffer (srclen))
	{
		return -1;
	}

	// De cada vuelta transformamos 3 bytes o 4 bytes, hasta que sea suficientemente
	// pequeño el buffer como para tratar los últimos 0, 1 o 2 bytes de manera individual
	while (srclen >= 3 && destlen > 4)
	{
		*dest++ = b64chars[src[0] >> 2];
		*dest++ = b64chars[ ((src[0] << 4) & 0x30) | (src[1] >> 4)];
		*dest++ = b64chars[ ((src[1] << 2) & 0x3c) | (src[2] >> 6)];
		*dest++ = b64chars[src[2] & 0x3f];
		destlen -= 4;
		srclen -= 3;
		src += 3;
	}

	// Pueden quedar 0, 1 o 2 bytes sin tratar, los tratamos aqui
	if (srclen > 0 && destlen > 4)
	{
		unsigned char fragment = 0;
		*dest++ = b64chars[src[0] >> 2];
		fragment = (src[0] << 4) & 0x30;
		if (srclen > 1)
		{
			fragment |= src[1] >> 4;
		}
		*dest++ = b64chars[fragment];
		*dest++ = (srclen < 2) ? '=' : b64chars[ (src[1] << 2) & 0x3c];
		*dest++ = '=';
	}
	*dest = '\0';
	return 1;
}



/******************************************************************************
 * Transforma una cadena en base 64 a otra normal
 * \param		[out]	dest	Cadena en la que se almacenará el decodificado
 * \param		[in]	src		Cadena en Base64
 * \param		[in]	destlen	Longitud maxima a copiar en dest
 * \return 0 si ok
 * -1 Error de parametros
 * \see			http://en.wikipedia.org/wiki/Base64
 *****************************************************************************/
int base64_decode (unsigned char* dest, const char* src, size_t destlen)
{
	int len = 0;
	char digit1, digit2, digit3, digit4;
	size_t srclen;

	// Comprobacion de parametros
	if (!dest || !src)
	{
		return -1;
	}

	srclen = strlen (src);
	if (destlen < Base64CalculateSizeDecodedBuffer (srclen))
	{
		return -1;
	}

	if (srclen % 4 != 0) // Garantizamos que es una cadena multiplo de 4 bytes
	{
		return -1;
	}

	// Cogemos grupos de 4 en 4 bytes.
	digit4 = 0;
	while ((*src) && (digit4 != '=') && (destlen))
	{
		digit1 = src[0];
		if ((digit1 > 127) || (b64val (digit1) == ERR))
		{
			return -1;
		}
		digit2 = src[1];
		if ((digit2 > 127) || (b64val (digit2) == ERR))
		{
			return -1;
		}
		digit3 = src[2];
		if ((digit3 > 127) || ((digit3 != '=') && (b64val (digit3) == ERR)))
		{
			return -1;
		}
		digit4 = src[3];
		if ((digit4 > 127) || ((digit4 != '=') && (b64val (digit4) == ERR)))
		{
			return -1;
		}
		src += 4;

		*dest++ = (b64val (digit1) << 2) | (b64val (digit2) >> 4);
		len++;
		destlen--;
		if (digit3 != '=')
		{
			*dest++ = ((b64val (digit2) << 4) & 0xf0) | (b64val (digit3) >> 2);
			len++;
			if (digit4 != '=')
			{
				*dest++ = ((b64val (digit3) << 6) & 0xc0) | b64val (digit4);
				len++;
			}
		}
	}

	return len;
}

/******************************************************************************
* \brief		Transforma a base64 y devuelve la cadena transformada
* \param	[in]	data	Cadena a codificar
* \param	[in]	size	Tamaño de la cadena a codificar
* \return		Nueva cadena con la cadena codificada. Null en caso de error
* \see			http://en.wikipedia.org/wiki/Base64
*****************************************************************************/
char* base64_str_encode (const char* data, size_t size)
{
	char* s, *p;
	size_t i;
	size_t c;
	const unsigned char* q;

	//3 bytes becomes 4 chars, but round up and allow for == and trailing NULL
	p = s = (char*) malloc (size * 4 / 3 + 4);
	if (p == NULL)
	{
		return NULL;
	}
	q = (const unsigned char*) data;
	i = 0;
	for (i = 0; i < size;)
	{
		c = q[i++];
		c *= 256;
		if (i < size)
		{
			c += q[i];
		}
		i++;
		c *= 256;
		if (i < size)
		{
			c += q[i];
		}
		i++;
		p[0] = b64chars[ (c & 0x00fc0000) >> 18];
		p[1] = b64chars[ (c & 0x0003f000) >> 12];
		p[2] = b64chars[ (c & 0x00000fc0) >> 6];
		p[3] = b64chars[ (c & 0x0000003f) >> 0];
		if (i > size)
		{
			p[3] = '=';
		}
		if (i > size + 1)
		{
			p[2] = '=';
		}
		p += 4;
	}
	*p = 0;
	return s;
}

