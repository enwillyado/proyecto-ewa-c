/*********************************************************************************************
 * Name			: w_app_x11.cpp
 * Description	: Implementación de los métodos dependientes de la plataforma X11
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef WIN32
#include "w_graficos/w_app.h"

#include "w_graficos/w_rgba.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <stdlib.h>

#include <iostream>

// ----------------------------------------------------------------------------
// Clase interna
class WApp::WAppIntern
{
public:
	WAppIntern()
	{
		this->init();
	};

	// ---------------------------------------------------------------------------
	// Iniciador
	void init()
	{
	}

public:
	Display* dsp;
	Visual* visual;
	Window win;
	GC gc;
	Atom wmDelete;
	Colormap colormap;
};

// ----------------------------------------------------------------------------
// Constructores
WApp::WApp (const std::string & iName, const size_t ancho, const size_t alto)
	: WMultiControl (iName),
	  data (*new WApp::WAppIntern), fondo (ancho, alto), mouse ("Mouse")
{
	this->init (ancho, alto);
}
WApp::~WApp()
{
	delete &data;
}

// ----------------------------------------------------------------------------
// Métodos propios
int WApp::initInternal()
{
	// Iniciar ventana
	this->data.dsp = XOpenDisplay (NULL);
	if (NULL == this->data.dsp)
	{
		return 1;
	}

	int screen = DefaultScreen (this->data.dsp);
	unsigned int white = WhitePixel (this->data.dsp, screen);
	unsigned int black = BlackPixel (this->data.dsp, screen);

	this->data.win = XCreateSimpleWindow (this->data.dsp,
										  DefaultRootWindow (this->data.dsp),
										  0, 0,   // origin
										  this->getAncho(), this->getAlto(), // size
										  0, black, // border width/clr
										  white);   // backgrd clr

	Atom hintAtom = XInternAtom (this->data.dsp, "_MOTIF_WM_HINTS", True);
	if (hintAtom != None)
	{
		const unsigned long MWM_HINTS_DECORATIONS = (1L << 1);
		struct
		{
			unsigned long flags;
			unsigned long functions;
			unsigned long decorations;
			long input_mode;
			unsigned long status;
		} MWMHints = { MWM_HINTS_DECORATIONS, 0, 0, 0, 0 };
		XChangeProperty (this->data.dsp, this->data.win, hintAtom, hintAtom,
						 32, PropModeReplace, (unsigned char*)&MWMHints,
						 sizeof (MWMHints) / 4);
	}
	else
	{
		return 2;
	}

	this->data.wmDelete = XInternAtom (this->data.dsp, "WM_DELETE_WINDOW", True);
	XSetWMProtocols (this->data.dsp, this->data.win, &this->data.wmDelete, 1);

	this->data.gc = XCreateGC (this->data.dsp, this->data.win,
							   0,       // mask of values
							   NULL);   // array of values

	XSetForeground (this->data.dsp, this->data.gc, black);

	this->data.colormap = DefaultColormap (this->data.dsp, DefaultScreen (this->data.dsp));

	this->data.visual = DefaultVisual (this->data.dsp, 0);

	XEvent evt;
	long eventMask = StructureNotifyMask;
	eventMask |= ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | PointerMotionMask | ExposureMask;
	XSelectInput (this->data.dsp, this->data.win, eventMask);

	XMapWindow (this->data.dsp, this->data.win);

	// wait until window appears
	do
	{
		XNextEvent (this->data.dsp, &evt);
	}
	while (evt.type != MapNotify);

	XResizeWindow (this->data.dsp, this->data.win, this->getAncho(), this->getAlto());
	this->pintar();
	XFlush (this->data.dsp);

	return 0;
}

bool WApp::start()
{
	KeyCode keyQ;
	keyQ = XKeysymToKeycode (this->data.dsp, XStringToKeysym ("Q"));

	XEvent evt;

	bool loop = true;
	while (loop)
	{
		XNextEvent (this->data.dsp, &evt);

		switch (evt.type)
		{
		case ButtonRelease:
		{
			this->pintar();
		}
		break;
		case KeyRelease:
		{
			std::cout << evt.xkey.keycode;
			std::cout.flush();
			if (evt.xkey.keycode == keyQ)
			{
				loop = false;
			}
			else
			{
				this->pintar();
			}
		}
		break;
		case MotionNotify:
		{
			XMotionEvent* motionEvent = NULL;
			motionEvent = (XMotionEvent*) (&evt);
			const int xPos = motionEvent->x;
			const int yPos = motionEvent->y;
			if (true == this->setMouse (xPos, yPos))
			{
				this->pintar();
			}
		}
		break;
		case ConfigureNotify:
		{
			// Check if window has been resized
			/*
			if (evt.xconfigure.width != XRES || evt.xconfigure.height != YRES)
			{
			XRES = evt.xconfigure.width;
			YRES = evt.xconfigure.height;
			this->pintar();
			}
			*/
		}
		break;
		case ClientMessage:
		{
			if (evt.xclient.data.l[0] == this->data.wmDelete)
			{
				loop = false;
			}
		}
		break;
		default:
		{
		}
		break;
		}
	}

	return true;
}

bool WApp::stop()
{
	XDestroyWindow (this->data.dsp, this->data.win);
	XCloseDisplay (this->data.dsp);
	return true;
}

bool WApp::setCursorInternal()
{
	return true;
}

bool WApp::rePosAndDimInternal()
{
	/*
	HWND & hWnd = this->data.hWnd;
	::MoveWindow (hWnd, this->getPosX(), this->getPosY(), this->getAncho(), this->getAlto(), true);
	*/
	return true;
}

void WApp::pintar()
{
	this->solicitarPintar();
}
void WApp::solicitarPintar()
{
	this->pintarInternal();
}

bool WApp::pintarInternal()
{
	const WLienzo & lienzoFinal = this->recomponerLienzo();
	WLienzo::Pixeles pBits = lienzoFinal.getPixeles();

	const int width = lienzoFinal.getAncho(), height = this->getAlto();
#ifdef CREAR_IMAGEN
	const size_t numBytes = width * height * 4;
	char* image32 = new char[numBytes];
#ifdef PINTAR_LENTO
	{
		size_t pos = 0;
		for (size_t j = 0; j < height; j++)
		{
			for (size_t i = 0; i < width; i++)
			{
				const RGBA & rgba = lienzoFinal.getPixel (i, j);
				image32 [pos++] = rgba.b;	//b
				image32 [pos++] = rgba.g;	//g
				image32 [pos++] = rgba.r;	//r
				image32 [pos++] = 0;
			}
		}
	}
#else
	memcpy (image32, pBits, numBytes);
#endif
	XImage* ximage = XCreateImage (this->data.dsp, this->data.visual, 24, ZPixmap, 0, image32, width, height, 32, 0);
#else
	char* image32 = (char*)pBits;
	static XImage* ximage = XCreateImage (this->data.dsp, this->data.visual, 24, ZPixmap, 0, image32, width, height, 32, 0);
#endif

	XPutImage (this->data.dsp, this->data.win, this->data.gc, ximage, 0, 0, 0, 0, width, height);

#ifdef CREAR_IMAGEN
	XDestroyImage (ximage);
#endif

	//XFlush (this->data.dsp);

	return true;
}

#endif
