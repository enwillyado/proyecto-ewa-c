/*********************************************************************************************
 * Name			: w_evento.cpp
 * Description	: Evento base para los desencadenadores
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_eventoMouseDrag.h"

#include "w_graficos/w_control.h"
#include "w_graficos/w_app.h"

// Constructor
WEventoMouseDrag::WEventoMouseDrag (WControl & iControl, const size_t & iPrevPosX, const size_t & iPrevPosY)
	: WEventoMouse (iControl),
	  prevPosX (iPrevPosX), prevPosY (iPrevPosY)
{
}

// Observadores r�pidos
size_t WEventoMouseDrag::getPosX() const
{
	return this->WEvento::control.getRoot().getCursor().getPosX() + this->WEvento::control.getRoot().getPosX() ;
}
size_t WEventoMouseDrag::getPosY() const
{
	return this->WEvento::control.getRoot().getCursor().getPosY() + this->WEvento::control.getRoot().getPosY();
}

size_t WEventoMouseDrag::getIncrementoPosX() const
{
	return this->getPosX() - this->prevPosX;
}
size_t WEventoMouseDrag::getIncrementoPosY() const
{
	return this->getPosY() - this->prevPosY;
}
