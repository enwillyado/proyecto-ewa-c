/*********************************************************************************************
 *	Name		: fiewa_services.h
 *	Description	: Declaración de los diferentes servicios del Servidor de Pares
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _FIEWA_SERVICES_H_
#define _FIEWA_SERVICES_H_

#include "fiewa/fiewa_services_id.h"	//< para tener los id de los servicios
#include "fiewa/fiewa_datos_stun.h"		//< para tener la instancia que guarda Datos que ofrece STUN

#include "w_web/w_web_services_base.h"	//< para tener la base de los servicios

#include <string>
#include <vector>

namespace FIEWA
{
	// ----------------------------------------------------------------------------
	class BasicSrvService_IPandPORT : public BasicSrvService
	{
	public:
		BasicSrvService_IPandPORT() : BasicSrvService() {};
		virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
		virtual BasicSrvService* newInstance() const
		{
			return new BasicSrvService_IPandPORT();
		}
		virtual std::string getServicioName() const
		{
			return SRVSERVICE_IPANDPORT_ID;
		}
	};

	// ----------------------------------------------------------------------------
	class BasicSrvService_IPofPEER : public BasicSrvService
	{
	public:
		BasicSrvService_IPofPEER() : BasicSrvService() {};
		virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
		virtual BasicSrvService* newInstance() const
		{
			return new BasicSrvService_IPofPEER();
		}
		virtual std::string getServicioName() const
		{
			return SRVSERVICE_IPOFPEER_ID;
		}

	public:
		typedef std::map<FIEWA::DatosSTUN::PublicID, FIEWA::DatosSTUN> Peers;
		static Peers peers;
	};

	// ----------------------------------------------------------------------------
	class BasicSrvService_LoginPEER : public BasicSrvService
	{
	public:
		BasicSrvService_LoginPEER() : BasicSrvService() {};
		virtual Mensaje ejecutarServicio (const Evento &, const Mensaje &) const;
		virtual BasicSrvService* newInstance() const
		{
			return new BasicSrvService_LoginPEER();
		}
		virtual std::string getServicioName() const
		{
			return SRVSERVICE_LOGIN_ID;
		}

	public:
		typedef std::map<std::string, FIEWA::DatosSTUN> Cuentas;
		static Cuentas cuentas;
	};
}

#endif	//_FIEWA_SERVICES_H_
