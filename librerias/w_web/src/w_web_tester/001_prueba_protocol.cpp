#include <boost/test/unit_test.hpp>

#include "w_web/w_web_mensaje.h"		// para tener el Mensaje

// ----------------------------------------------------------------------------
// Test para probar la clase "HTTP"
#ifdef HTTP
#define HTTP_PETCION_AL_SERVIDOR	\
	"GET /recurso/de.prueba HTTP/1.0\
User-Agent: Mozilla/5.0 (Windows NT 6.1)\
Referer: https://www.proyectoewa.com/\
"

#define HTTP_CUERPO_DE_PRUEBA			"Respuesta de prueba"
#define HTTP_CUERPO_DE_PRUEBA_STRSIZE	"21"
#define HTTP_RESPUESTA_DEL_SERVIDOR	\
	"HTTP/1.0 200 OK\
Date: Fri, 22 Feb 2013 10:10:18 GMT\
Content-Encoding: gzip\
Content-Disposition: attachment\
Content-Length: " HTTP_CUERPO_DE_PRUEBA_STRSIZE "\
X-XSS-Protection: 1; mode=block\
Server: gws\
X-Frame-Options: SAMEORIGIN\
Content-Type: application/json; charset=UTF-8\
Cache-Control: private, max-age=0\
Expires: -1\
\
" HTTP_CUERPO_DE_PRUEBA "\r\n"

BOOST_AUTO_TEST_CASE (Test_HTTP_Parser_Procesar)
{
	HTTP parseador;
	{
		parseador.parseFromMsg (HTTP_PETCION_AL_SERVIDOR);
	}
	{
		parseador.parseFromMsg (HTTP_RESPUESTA_DEL_SERVIDOR);
	}
}
BOOST_AUTO_TEST_CASE (Test_HTTP_Parser_Generar)
{
	{
		// Generar directamente una respuesta HTTP
		HTTP generadorDirecto (HTTP_RESPUESTA_DEL_SERVIDOR);
		// Y ver que es correcta:
		// TODO:
		//		BOOST_CHECK( generadorDirecto.getBody() == HTTP_CUERPO_DE_PRUEBA);
	}
	{
		// Generar la respuesta http poco a poco con la clase heredada:
		HTTP generador;
		generador.setRespuestaPorDefecto (HTTP_CUERPO_DE_PRUEBA);
		// Y ver que es correcta:
		BOOST_CHECK (generador.getBody() == HTTP_CUERPO_DE_PRUEBA);
		BOOST_CHECK (generador.getTipoMensaje() == HTTP::RESPONSE);
		BOOST_CHECK (generador.getVersionProtocolo() == HTTP::HTTP1_0);
		BOOST_CHECK (generador.getUri() == "");		//< normal, �no?
	}
}
#endif

// ----------------------------------------------------------------------------
// Test para probar la clase Mensaje
#define DPROT_SERVICIO_DE_PRUEBA		"servicioDePrueba"
#define DPROT_PETICION_DE_PRUEBA		\
	W_RED_TIPOMENSAJE_GET " " DPROT_SERVICIO_DE_PRUEBA " HTTP/1.0" FIN_DE_LINEA "\
User-Agent: Mozilla/5.0 (Windows NT 6.1)" FIN_DE_LINEA "\
Referer: https://www.proyectoewa.com/" FIN_DE_LINEA "\
Content-Length: 0" FIN_DE_LINEA "\
" FIN_DE_LINEA

#define DPROT_PETICION_DE_PRUEBA_SIN_CONTENT_LENGTH		\
	W_RED_TIPOMENSAJE_GET " " DPROT_SERVICIO_DE_PRUEBA " HTTP/1.0" FIN_DE_LINEA "\
User-Agent: Mozilla/5.0 (Windows NT 6.1)" FIN_DE_LINEA "\
Referer: https://www.proyectoewa.com/" FIN_DE_LINEA "\
" FIN_DE_LINEA

#define DPROT_PETICION_DE_PRUEBA_TROCEADA_1 \
	{											\
		W_RED_TIPOMENSAJE_GET " ",					\
		DPROT_SERVICIO_DE_PRUEBA,				\
		" HTTP/1.0" FIN_DE_LINEA,				\
		NULL									\
	}
#define DPROT_PETICION_DE_PRUEBA_TROCEADA_2 \
	{											\
		W_RED_TIPOMENSAJE_GET,						\
		" ",									\
		DPROT_SERVICIO_DE_PRUEBA,				\
		" HTTP/1.0" FIN_DE_LINEA,				\
		NULL									\
	}
#define DPROT_PETICION_DE_PRUEBA_TROCEADA_3 \
	{											\
		"DP",									\
		"ROT ",									\
		DPROT_SERVICIO_DE_PRUEBA,				\
		" HTTP/1.0",							\
		FIN_DE_LINEA,							\
		NULL									\
	}

void Test_ProtectionProtocol_ProcesarPorPartes (Mensaje & parseador, const char* tokens[])
{
	for (size_t i = 0; tokens[i] != NULL; i++)
	{
		if (parseador.parseFromMsg (tokens[i]) == true)	// si no "admite" m�s datos...
		{
			break;
		}
	}
	// ahora que estamos fuera del bucle: ver si es bien o mal
	BOOST_CHECK (parseador.getLastError() == Mensaje::MENSAJE_CORRECTO);
	// y ver si el buffer est� vac�o o no
	BOOST_CHECK (parseador.getParserStatus() == Mensaje::BUFFER_VACIO);
	// Y si est� bien...
	if (parseador.getParserStatus())
	{
		// Mirar los mensajes evaluados (tipoMensaje)
		BOOST_CHECK (parseador.getTipoMensaje() == Mensaje::DPROT);
		BOOST_CHECK (parseador.getURI() == DPROT_SERVICIO_DE_PRUEBA);
	}
	else
	{
		BOOST_MESSAGE ("El evaluador del Mensaje Web ha fallado y no se pueden comprobar los datos le�dos!");
		// En realidad s� se puede, pero fallar�an siempre salvo milagro :P
	}
	// Este "clearParser()" no es necesario
	parseador.clearParser();
}

BOOST_AUTO_TEST_CASE (Test_ProtectionProtocol_Procesar)
{
	{
		Mensaje parseador;
		// procesar desde el valor de prueba y ver que fue bien
		BOOST_CHECK (parseador.parseFromMsg (DPROT_PETICION_DE_PRUEBA) == true);
		// comprobar diferentes campos
		BOOST_CHECK (parseador.getTipoMensaje() == Mensaje::DPROT);
		BOOST_CHECK (parseador.getURI() == DPROT_SERVICIO_DE_PRUEBA);
		BOOST_CHECK (true == parseador.existeEncabezadoValor (ENCABEZADOID_CONTENT_LENGTH));
	}
	{
		Mensaje parseador;
		// procesar desde el valor de prueba y ver que fue bien
		BOOST_CHECK (parseador.parseFromMsg (DPROT_PETICION_DE_PRUEBA_SIN_CONTENT_LENGTH) == true);
		// comprobar diferentes campos
		BOOST_CHECK (parseador.getTipoMensaje() == Mensaje::DPROT);
		BOOST_CHECK (parseador.getURI() == DPROT_SERVICIO_DE_PRUEBA);
		BOOST_CHECK (false == parseador.existeEncabezadoValor (ENCABEZADOID_CONTENT_LENGTH));
	}
	{
		Mensaje parseador;
		{
			// Probar con otra prueba: los campos separados separados enviados en bloques sin romper palabras (I)
			const char* tokens[] = DPROT_PETICION_DE_PRUEBA_TROCEADA_1;
			BOOST_MESSAGE ("Prueba sin romper palabras (I):");
			Test_ProtectionProtocol_ProcesarPorPartes (parseador, tokens);
		}
		{
			// Probar con otra prueba: los campos separados separados enviados en bloques rompiendo palabras (II)
			const char* tokens[] = DPROT_PETICION_DE_PRUEBA_TROCEADA_2;
			BOOST_MESSAGE ("Prueba sin romper palabras (II):");
			Test_ProtectionProtocol_ProcesarPorPartes (parseador, tokens);
		}
		{
			// Probar con otra prueba: los campos separados separados enviados en bloques rompiendo palabras
			const char* tokens[] = DPROT_PETICION_DE_PRUEBA_TROCEADA_3;
			BOOST_MESSAGE ("Prueba rompiendo palabras:");
			Test_ProtectionProtocol_ProcesarPorPartes (parseador, tokens);
		}
		BOOST_CHECK (parseador.getParserStatus() == Mensaje::BUFFER_VACIO);
	}
	{
		Mensaje parseador;
		BOOST_CHECK (parseador.parseFromMsg ("    ") == true);
		BOOST_CHECK (parseador.getLastError() == Mensaje::MENSAJE_MAL_FORMADO);
		BOOST_CHECK (parseador.getParserStatus() == Mensaje::TIPO_MENSAJE_DESCONOCIDO);
	}
	{
		// Prueba con los encabezados
		Mensaje parseador;
		BOOST_CHECK (parseador.getEncabezadoSize() == 0);
		BOOST_CHECK (parseador.getEncabezadoValor ("Reference") == "");
		BOOST_CHECK (parseador.getEncabezadoSize() == 0);
		parseador % "Reference" = "None";
		BOOST_CHECK (parseador.getEncabezadoSize() == 1);
		BOOST_CHECK (parseador.getEncabezadoValor ("Reference") == "None");
		BOOST_CHECK (parseador % "Reference" == "None");
		parseador % "Reference" = "";
		BOOST_CHECK (parseador.getEncabezadoSize() == 1);
		BOOST_CHECK (parseador.getEncabezadoValor ("Reference") == "");
		BOOST_CHECK (parseador % "Reference" == "");
		BOOST_CHECK (parseador % "Other" == "");
		BOOST_CHECK (parseador.getEncabezadoSize() == 2);
		parseador % "Other" = "hola";
		BOOST_CHECK (parseador.getEncabezadoSize() == 2);
		BOOST_CHECK (parseador.getEncabezadoValor ("Other") == "hola");
		BOOST_CHECK (parseador % "Other" == "hola");
		{
			// Prueba con los encabezados constantes
			const Mensaje parseadorConst = parseador;
			BOOST_CHECK (parseadorConst % "Otro m�s" == "");
			BOOST_CHECK (parseadorConst.getEncabezadoValor ("Otro m�s") == "");
			BOOST_CHECK (parseadorConst % "Other" == "hola");
			BOOST_CHECK (parseadorConst % "Reference" == "");
			BOOST_CHECK (parseadorConst.getEncabezadoSize() == 2);
		}
		BOOST_CHECK (parseador.getEncabezadoSize() == 2);
	}
}

#define DPROT_CUERPO_DE_PRUEBA		"nada"
#define DPROT_RESPUESTA_DE_PRUEBA	\
	W_RED_TIPOMENSAJE_GET " " DPROT_SERVICIO_DE_PRUEBA " HTTP/1.0" FIN_DE_LINEA "\
User-Agent: Mozilla/5.0 (Windows NT 6.1)" FIN_DE_LINEA "\
Referer: https://www.proyectoewa.com/" FIN_DE_LINEA "\
" FIN_DE_LINEA "\
" DPROT_CUERPO_DE_PRUEBA

BOOST_AUTO_TEST_CASE (Test_PortectionProtocol_Generar)
{
	{
		// Generar directamente una respuesta http
		Mensaje generadorDirecto (DPROT_RESPUESTA_DE_PRUEBA);
		BOOST_CHECK (generadorDirecto.getBody() == DPROT_CUERPO_DE_PRUEBA);
	}
	{
		// Generar la respuesta del Protocolo de la Protecci�n poco a poco con la clase heredada:
		Mensaje generador;
		generador.setRespuestaPorDefecto (DPROT_CUERPO_DE_PRUEBA);
		BOOST_CHECK (generador.getBody() == "<code>" DPROT_CUERPO_DE_PRUEBA "</code>");
		BOOST_CHECK (generador.getTipoMensaje() == Mensaje::RESPONSE);
#ifdef HTTP
		BOOST_CHECK (generador.HTTP::getTipoMensaje() == HTTP::UNKNOWN);
#endif
	}
}

#define DPROT_CONSULTA_GETPOST_DATA_URI1 "ruta"
#define DPROT_CONSULTA_GETPOST_DATA_URI2 "file.exe"

#define DPROT_CONSULTA_GETPOST_DATA_VAR1	"variable1"
#define DPROT_CONSULTA_GETPOST_DATA_VAL1	"valor1"
#define DPROT_CONSULTA_GETPOST_DATA1		DPROT_CONSULTA_GETPOST_DATA_VAR1 W_RED_URI_GETPOST_ASIGNATOR DPROT_CONSULTA_GETPOST_DATA_VAL1
#define DPROT_CONSULTA_GETPOST_DATA_VAR2	"variable2"
#define DPROT_CONSULTA_GETPOST_DATA_VAL2	"valor2"
#define DPROT_CONSULTA_GETPOST_DATA2		DPROT_CONSULTA_GETPOST_DATA_VAR2 W_RED_URI_GETPOST_ASIGNATOR DPROT_CONSULTA_GETPOST_DATA_VAL2

#define DPROT_CONSULTA_GETPOST_DATA			DPROT_CONSULTA_GETPOST_DATA1 W_RED_URI_GETPOST_SEPARATOR DPROT_CONSULTA_GETPOST_DATA2

#define DPROT_CONSULTA_GETPOST_SERVICIO		"/" DPROT_CONSULTA_GETPOST_DATA_URI1 "/" DPROT_CONSULTA_GETPOST_DATA_URI2
#define DPROT_CONSULTA_GETPOST_URI			DPROT_CONSULTA_GETPOST_SERVICIO W_RED_URI_GET_INICIATOR DPROT_CONSULTA_GETPOST_DATA

BOOST_AUTO_TEST_CASE (Test_PortectionProtocol_GetPost)
{
	std::string consulta;
	consulta += W_RED_TIPOMENSAJE_GET " " DPROT_CONSULTA_GETPOST_URI " HTTP/1.0" FIN_DE_LINEA;
	consulta += ENCABEZADOID_CONTENT_LENGTH ENCABEZADO_SEPARATOR;
	consulta += Mensaje::toStr (sizeof (DPROT_CONSULTA_GETPOST_DATA) - 1);
	consulta += FIN_DE_LINEA;
	consulta += FIN_DE_LINEA;
	consulta += DPROT_CONSULTA_GETPOST_DATA;
	{
		// Generar directamente una respuesta http
		Mensaje mensaje (consulta);
		BOOST_CHECK (mensaje.getTipoMensaje() == Mensaje::DPROT);
		BOOST_CHECK (mensaje.getBody() == DPROT_CONSULTA_GETPOST_DATA);
		{
			BOOST_CHECK (mensaje.getEncabezadoSize() == 1);
			BOOST_CHECK (true == mensaje.existeEncabezadoValor (ENCABEZADOID_CONTENT_LENGTH));
			BOOST_CHECK (mensaje.getEncabezadoValor (ENCABEZADOID_CONTENT_LENGTH) == Mensaje::toStr (sizeof DPROT_CONSULTA_GETPOST_DATA - 1));
			BOOST_CHECK (mensaje % ENCABEZADOID_CONTENT_LENGTH == Mensaje::toStr (std::string (DPROT_CONSULTA_GETPOST_DATA).size()));
		}
		{
			const Mensaje::VectorVars & uriVars = mensaje.getUriVars();
			BOOST_CHECK (uriVars.size() == 3);
			BOOST_CHECK ("" == uriVars[0]);
			BOOST_CHECK (DPROT_CONSULTA_GETPOST_DATA_URI1 == uriVars[1]);
			BOOST_CHECK (DPROT_CONSULTA_GETPOST_DATA_URI2 == uriVars[2]);
		}
		{
			const Mensaje::ArrayVars & getVars = mensaje.getGetVars();
			BOOST_CHECK (getVars.size() == 2);
			BOOST_CHECK (DPROT_CONSULTA_GETPOST_DATA_VAL1 == getVars.at (DPROT_CONSULTA_GETPOST_DATA_VAR1));
			BOOST_CHECK (DPROT_CONSULTA_GETPOST_DATA_VAL2 == getVars.at (DPROT_CONSULTA_GETPOST_DATA_VAR2));
		}
		{
			const Mensaje::ArrayVars & postVars = mensaje.getPostVars();
			BOOST_CHECK (postVars.size() == 2);
			BOOST_CHECK (DPROT_CONSULTA_GETPOST_DATA_VAL1 == postVars.at (DPROT_CONSULTA_GETPOST_DATA_VAR1));
			BOOST_CHECK (DPROT_CONSULTA_GETPOST_DATA_VAL2 == postVars.at (DPROT_CONSULTA_GETPOST_DATA_VAR2));
		}
	}
}

BOOST_AUTO_TEST_CASE (Test_PortectionProtocol_Inversible)
{
	{
		Mensaje mensaje;
		mensaje.setURI ("/carpeta/index.html");
		mensaje.setTipoMensaje (Mensaje::GET);
		mensaje.setVersionProtocolo (Mensaje::HTTP1_1);

		// Crear una copia a trav�s del mensaje generado
		Mensaje mensajeCopia (mensaje.genMsg());

		// Ver que son iguales campo a campo
		BOOST_CHECK (mensaje.getURI() == mensajeCopia.getURI());
		BOOST_CHECK (mensaje.getBody() == mensajeCopia.getBody());
		BOOST_CHECK (mensaje.getTipoMensaje() == mensajeCopia.getTipoMensaje());
		BOOST_CHECK (mensaje.getVersionProtocolo() == mensajeCopia.getVersionProtocolo());
	}
	{
		Mensaje mensaje;
		mensaje.setURI ("/carpeta/index.html");
		mensaje.setBody ("hola");
		mensaje.setTipoMensaje (Mensaje::POST);
		mensaje.setVersionProtocolo (Mensaje::HTTP1_1);

		// Crear una copia a trav�s del mensaje generado
		Mensaje mensajeCopia (mensaje.genMsg());

		// Ver que son iguales campo a campo
		BOOST_CHECK (mensaje.getURI() == mensajeCopia.getURI());
		BOOST_CHECK (mensaje.getBody() == mensajeCopia.getBody());
		BOOST_CHECK (mensaje.getTipoMensaje() == mensajeCopia.getTipoMensaje());
		BOOST_CHECK (mensaje.getVersionProtocolo() == mensajeCopia.getVersionProtocolo());
	}
	{
		Mensaje mensaje;
		mensaje.setURI ("/carpeta/index.html");
		mensaje.setBody ("hola");
		mensaje.setTipoMensaje (Mensaje::POST);
		mensaje.setVersionProtocolo (Mensaje::HTTP1_0);

		// Crear una copia a trav�s del mensaje generado
		Mensaje mensajeCopia (mensaje.genMsg());

		// Ver que son iguales campo a campo
		BOOST_CHECK (mensaje.getURI() == mensajeCopia.getURI());
		BOOST_CHECK (mensaje.getBody() == mensajeCopia.getBody());
		BOOST_CHECK (mensaje.getTipoMensaje() == mensajeCopia.getTipoMensaje());
		BOOST_CHECK (mensaje.getVersionProtocolo() == mensajeCopia.getVersionProtocolo());
	}
	{
		Mensaje mensaje;
		mensaje.setBody ("hola");
		mensaje.setTipoMensaje (Mensaje::RESPONSE);		// NOTA: con RESPONSE_AND_EXIT no funcionar�a, ya que se hace una conversi�n para enviarse ambas como RESPONSE
		mensaje.setVersionProtocolo (Mensaje::HTTP1_0);

		// Crear una copia a trav�s del mensaje generado
		Mensaje mensajeCopia (mensaje.genMsg());

		// Ver que son iguales campo a campo
		BOOST_CHECK (mensaje.getURI() == mensajeCopia.getURI());
		BOOST_CHECK (mensaje.getBody() == mensajeCopia.getBody());
		BOOST_CHECK (mensaje.getTipoMensaje() == mensajeCopia.getTipoMensaje());
		BOOST_CHECK (mensaje.getVersionProtocolo() == mensajeCopia.getVersionProtocolo());
	}

}



BOOST_AUTO_TEST_CASE (Test_PortectionProtocol_GranBodyTroceado)
{
	// Crear un body gigante troceado
	const std::string & bodyPart1 = std::string (13 * 1024 * 1024, 'd');	// 13 /*MB*/
	const std::string & bodyPart2 = std::string (13 * 1024 * 1024, 'a');	// 13 /*MB*/
	const std::string & bodyPart3 = " basura y mas cosas";

	// ---------------------------------------------------------------------------
	// Cuando no se env�a ENCABEZADOID_CONTENT_LENGTH (ni otros) el funcionamiento no permite la concatenaci�n de mensajes con el body troceado
	{
		std::string cabeceraSimple = W_RED_TIPOMENSAJE_GET " " DPROT_SERVICIO_DE_PRUEBA " HTTP/1.0" FIN_DE_LINEA;
		cabeceraSimple += FIN_DE_LINEA;

		// Parsear un Mensaje Web con el protocolo HTTP
		Mensaje mensaje (cabeceraSimple);
		BOOST_CHECK (mensaje.getLastError() ==
					 Mensaje::MENSAJE_CORRECTO);		// OJO: Al no tener el ENCABEZADOID_CONTENT_LENGTH, finaliza "correctamente" al llegar a este punto
		mensaje.parseFromMsg (bodyPart1);
		BOOST_CHECK (mensaje.getLastError() ==
					 Mensaje::MENSAJE_INCOMPLETO);		// OJO: en este caso (porque el body no es un Mensaje Web, porque no hay espacios) quedar� incompleto
		mensaje.parseFromMsg (bodyPart2);
		BOOST_CHECK (mensaje.getLastError() == Mensaje::MENSAJE_INCOMPLETO);		// OJO: aqu� tambi�n sigue acumulando (porque no hay espacios) quedando incompleto
		mensaje.parseFromMsg (bodyPart3);
		BOOST_CHECK (mensaje.getLastError() ==
					 Mensaje::MENSAJE_MAL_FORMADO);		// OJO: aqu� al ser un trozo que incluye espacios, el parser piensa que est� evaluando un Mensaje Web y detectar� un error
	}

	// ---------------------------------------------------------------------------
	// Cuando no se env�a ENCABEZADOID_CONTENT_LENGTH (pero s� otros) el funcionamiento tampoco es mucho mejor
	{
		std::string cabeceraConEncabezado = W_RED_TIPOMENSAJE_GET " " DPROT_SERVICIO_DE_PRUEBA " HTTP/1.0" FIN_DE_LINEA;
		cabeceraConEncabezado += ENCABEZADOID_CONTENT_TYPE ENCABEZADO_SEPARATOR ENCABEZADOID_CONTENT_TYPE_TEXT FIN_DE_LINEA;
		cabeceraConEncabezado += FIN_DE_LINEA;

		// Parsear el Mensaje Web con el Protocolo HTTP
		Mensaje mensaje (cabeceraConEncabezado);
		BOOST_CHECK (mensaje.getLastError() ==
					 Mensaje::MENSAJE_CORRECTO);		// OJO: Al no tener el ENCABEZADOID_CONTENT_LENGTH, finaliza "correctamente" al llegar a este punto
		mensaje.parseFromMsg (bodyPart1);
		BOOST_CHECK (mensaje.getLastError() ==
					 Mensaje::MENSAJE_INCOMPLETO);		// OJO: en este caso (porque el body no es un Mensaje Web, porque no hay espacios) quedar� incompleto
		mensaje.parseFromMsg (bodyPart2);
		BOOST_CHECK (mensaje.getLastError() == Mensaje::MENSAJE_INCOMPLETO);		// OJO: aqu� tambi�n sigue acumulando (porque no hay espacios) quedando incompleto
		mensaje.parseFromMsg (bodyPart3);
		BOOST_CHECK (mensaje.getLastError() ==
					 Mensaje::MENSAJE_MAL_FORMADO);		// OJO: aqu� al ser un trozo que incluye espacios, el parser piensa que est� evaluando un Mensaje Web y detectar� un error
	}

	// ---------------------------------------------------------------------------
	// Este es el funcionamiento esperado: enviando el ENCABEZADOID_CONTENT_LENGTH
	{
		std::string cabeceraConLength = W_RED_TIPOMENSAJE_GET " " DPROT_SERVICIO_DE_PRUEBA " HTTP/1.0" FIN_DE_LINEA;
		cabeceraConLength += ENCABEZADOID_CONTENT_LENGTH ENCABEZADO_SEPARATOR + Mensaje::toStr (bodyPart1.size() + bodyPart2.size() + bodyPart3.size()) + FIN_DE_LINEA;
		cabeceraConLength += FIN_DE_LINEA;

		// Parsear el Mensaje Web con el Protocolo HTTP
		Mensaje mensaje (cabeceraConLength);
		BOOST_CHECK (mensaje.getLastError() == Mensaje::MENSAJE_INCOMPLETO);
		mensaje.parseFromMsg (bodyPart1);
		BOOST_CHECK (mensaje.getLastError() == Mensaje::MENSAJE_INCOMPLETO);
		mensaje.parseFromMsg (bodyPart2);
		BOOST_CHECK (mensaje.getLastError() == Mensaje::MENSAJE_INCOMPLETO);
		mensaje.parseFromMsg (bodyPart3);
		BOOST_CHECK (mensaje.getLastError() == Mensaje::MENSAJE_CORRECTO);
	}
}
