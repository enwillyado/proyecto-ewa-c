/*********************************************************************************************
 *	Name		: w_sistema_timer.h
 *	Description	: Declaraciones para el trabajo con temporizadores
 *	Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_TIMER_H_
#define _W_SISTEMA_TIMER_H_

#include "w_sistema/w_sistema_thread.h"

#include <cstring>	//< NULL

// Declaraci�n de la clase agrupadora de hilos
class WTimer
{
public:
	typedef TWThreadData<WTimer*> TWThreadDataTimer;
	typedef void (*FunctionTimer) (WTimer &);
	static void functionTimerNull (WTimer &);

public:
	// Constructor
	WTimer (FunctionTimer = functionTimerNull, const size_t milisegundos = 0, void* dataPtr = NULL);

	// Destructor
	virtual ~WTimer();

	// Ejecutores
	void executeFunction();			//< ejecuta la funci�n peri�dica en el hilo actual

	// Control de la ejecuci�n
	WThread::Estado start();		//< ejecuta la funci�n peri�dica en un nuevo hilo
	WThread::Estado restart();		//< solicita el reiniciado de la cuenta
	WThread::Estado pause();		//< solicita la detenci�n programada al hilo
	WThread::Estado resume();		//< solicita la continuaci�n al hilo
	WThread::Estado stop();			//< solicita la finalizaci�n programada al hilo
	WThread::Estado end();			//< detiene de forma abrupta el hilo

	// Observadores
	bool isStarted() const;			//< determina si se ha indicado que este hilo (el principal o su hijo) se ha iniciado
	bool isStoped() const;			//< determina si se ha solicitado que este hilo (el principal o su hijo) se detenga

	// Datos
	size_t getMilisengundos() const;

	void* getDataPtr();
	void setDataPtr (void* dataPtr);

	template<class T>
	T & getData()
	{
		return * (static_cast<T*> (this->getDataPtr()));
	}

private:
	WThread			thread;			//< este es el hilo que se ejecutar� por debajo
	FunctionTimer	functionTimer;	//< funci�n que se ejecutar� peri�dicamente
	size_t			milisegundos;	//< periodo de tiempo de espera entre cada ciclo
	void*			dataPtr;

private:
	class Data;
	Data & data;
};

#endif // _W_SISTEMA_TIMER_H_
