/*********************************************************************************************
 *	Name		: _ewadrones_video_streaming.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar la app de v�deo streaming de EWADRONES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_VIDEO_STREAMING_PRAGMALIB_H_
#define _EWADRONES_VIDEO_STREAMING_PRAGMALIB_H_

#ifdef _MSC_VER

// Parte privada
#include "w_red/_w_red_proxy.pragmalib.h"

#endif

#undef _EWADRONES_VIDEO_STREAMING_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _EWADRONES_VIDEO_STREAMING_PRAGMALIB_H_
