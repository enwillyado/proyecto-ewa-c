/*********************************************************************************************
 * Name			: w_web_mensaje.cpp
 * Description	: Implementaci�n de la l�gica para procesar Mensajes Web
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_web/w_web_mensaje.h"

#include "w_base/fromStr.hpp"
#include "w_base/toStr.hpp"

// ----------------------------------------------------------------------------
// Constructores:
Mensaje::Mensaje()
{
	this->clearParser();
	this->tipoMensaje = Mensaje::UNKNOWN;
	this->versionProtocolo = Mensaje::HTTP1_0;
	this->lastError = SIN_ERROR;
}

/**
 *	Constructor que eval�a directamente un mensaje (de petici�n como de respuesta)
 *	\param	[in]	value		mensaje a evaluar
 *		En "getLastError()" se puede encontrar el error concreto si hubiera
**/
Mensaje::Mensaje (const std::string & value)
{
	this->clearParser();
	this->parseFromMsg (value);
}

// ----------------------------------------------------------------------------
// Procesadores:
/**
 *	Intenta leer el contenido de un fichero dado y aval�a su contenido como si
 *		fuera una mensaje HTTP
 *	\param	[in]	filename	nombre del fichero a procesar
 *	\retval	TRUE si se consigui� evaluar correctamente
 *		o   FALSE en caso contrario: en "getLastError()" se puede encontrar el error concreto
**/
bool Mensaje::parseFromFile (const std::string & filename)
{
	std::string strvalue = ""; // TODO: leer el contenido del archivo
	return this->parseFromMsg (strvalue);
}
/**
 *	Intenta evaluar una cadena dada como si fuera un mensaje (de petici�n o de respuesta)
 *	\param	[in]	value		mensaje a evaluar (o parte de un mensaje)
 *	\retval	TRUE si ya no "admite" m�s mensajes (o termin�, o est� mal y no merece la pena seguir)
 *	ES DECIR,    devuelve TRUE cuando se manda un mensaje (o se env�a el �ltimo de varios)
 *					que generan un mensaje v�lido en el protocolo.
 *	PERO TAMBI�N devuelve TRUE cuando se manda un mensaje (o una parte intermedia) y el evaluador
 *					detecta que, "pase lo que pase", no va a dar por buena la informaci�n.
 *		o   FALSE en el caso contrario
 *	ES DECIR;	devuelve FALSE cuando se manda un mensaje que "tiene buena pinta" pero
 *					que todav�a no est� completamente le�do.
 *			En cualquier caso, con "getParserStatus()" se puede saber el estado concreto
**/
bool Mensaje::parseFromMsg (const std::string & value)
{
	std::string mensaje = this->bufferIncompleto + value;
	size_t prevpos = 0, pos = 0;
	if (this->parserStatus == BUFFER_VACIO)
	{
		// leer el TIPO-MENSAJE
		pos = mensaje.find_first_of (CONJUNTO_SEPARADORES, prevpos);
		if (pos == std::string::npos)
		{
			this->bufferIncompleto = mensaje.substr (prevpos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		{
			// en [prevpos, pos) est� el tipo (como string)
			std::string newTipoMensaje = mensaje.substr (prevpos, pos - prevpos);
			this->setTipoMensaje (Mensaje::convertToTiposMensaje (newTipoMensaje));
			if (this->getTipoMensaje() == UNKNOWN)
			{
				// Antes de tirar la toalla, ver si es una respuesta:
				this->setVersionProtocolo (Mensaje::convertToVersion (newTipoMensaje));
				if (this->getVersionProtocolo() == VERSION_DESCONOCIDA)
				{
					// no nos queda otra que rechazar el formato
					this->lastError = MENSAJE_MAL_FORMADO;
					this->parserStatus = TIPO_MENSAJE_DESCONOCIDO;
					return true;	// return true => termin� (mal paquete)
				}
				// si es una respuesta, se indica:
				this->setTipoMensaje (RESPONSE);
				this->parserStatus = LEIDO_VERSION_PROTOCOLO_RESPONSE;
			}
			else
			{
				this->parserStatus = LEIDO_TIPO_MENSAJE;
			}
		}
	}
	if (this->parserStatus == LEIDO_TIPO_MENSAJE)
	{
		// leer los separadores entre TIPO-MENSAJE y URI
		prevpos = mensaje.find_first_not_of (CONJUNTO_SEPARADORES, pos);
		// en [prevpos, pos) hay espacios
		if (prevpos == std::string::npos)
		{
			this->bufferIncompleto = mensaje.substr (pos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		this->parserStatus = TOCA_LEER_URI;
	}
	if (this->parserStatus == TOCA_LEER_URI)
	{
		// leer la URI
		pos = mensaje.find_first_of (CONJUNTO_SEPARADORES, prevpos);
		if (pos == std::string::npos)
		{
			this->bufferIncompleto = mensaje.substr (prevpos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		{
			// en [prevpos, pos) est� la URI
			std::string newURI = mensaje.substr (prevpos, pos - prevpos);
			this->setURI (newURI);
		}
		this->parserStatus = LEIDO_URI;
	}
	if (this->parserStatus == LEIDO_URI)
	{
		// leer los separadores entre URI y VERSION-PROTOCOLO
		prevpos = mensaje.find_first_not_of (CONJUNTO_SEPARADORES, pos);
		// en [prevpos, pos) hay espacios
		if (prevpos == std::string::npos)
		{
			this->bufferIncompleto = mensaje.substr (pos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		this->parserStatus = TOCA_LEER_VERSION;
	}
	if (this->parserStatus == TOCA_LEER_VERSION)
	{
		// leer la VERSION-PROTOCOLO
		pos = mensaje.find_first_of (CONJUNTO_SEPARADORES, prevpos);
		if (pos == std::string::npos)
		{
			this->bufferIncompleto = mensaje.substr (prevpos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		{
			// en [prevpos, pos) est� la VERSION-PROTOCOLO
			std::string newVersion = mensaje.substr (prevpos, pos - prevpos);
			this->setVersionProtocolo (Mensaje::convertToVersion (newVersion));
			if (this->getVersionProtocolo() == VERSION_DESCONOCIDA)
			{
				this->lastError = MENSAJE_MAL_FORMADO;
				this->parserStatus = VERSION_PROTOCOLO_DESCONOCIDO;
				return true;	// return true => termin� (mal paquete)
			}
		}
		this->parserStatus = LEIDO_VERSION_PROTOCOLO;
	}
	if (this->parserStatus == LEIDO_VERSION_PROTOCOLO)
	{
		// leer los separadores entre VERSION-PROTOCOLO y las cabeceras (separado por SALTO DE L�NEA)
		prevpos = mensaje.find_first_not_of (FIN_DE_LINEA, pos);
		// en [prevpos, pos) hay espacios
		if (prevpos == std::string::npos)
		{
			// se termin� el mensaje y tenemos la primera (y �nica) l�nea correctamente evaluada
			this->parserStatus = BUFFER_VACIO;
		}
		else
		{
			// hay m�s informaci�n en la siguiente l�nea
			this->parserStatus = TOCA_LEER_CABECERA;
		}
	}
	if (this->parserStatus == LEIDO_VERSION_PROTOCOLO_RESPONSE)
	{
		// leer los separadores entre VERSION-PROTOCOLO (de una respuesta) y el C�DIGO DE ERROR
		prevpos = mensaje.find_first_not_of (CONJUNTO_SEPARADORES, pos);
		// en [prevpos, pos) hay espacios
		if (prevpos == std::string::npos)
		{
			this->bufferIncompleto = mensaje.substr (pos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		this->parserStatus = TOCA_LEER_CODIGO_ERROR_RESPONSE;
	}
	if (this->parserStatus == TOCA_LEER_CODIGO_ERROR_RESPONSE)
	{
		// leer el C�DIGO DE ERROR
		pos = mensaje.find_first_of (CONJUNTO_SEPARADORES, prevpos);
		if (pos == std::string::npos)
		{
			this->bufferIncompleto = mensaje.substr (prevpos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		{
			// en [prevpos, pos) est� la VERSION-PROTOCOLO
			std::string newError = mensaje.substr (prevpos, pos - prevpos);
			this->error = (ErroresProtocolo)::fromStr<int> (newError.c_str());
		}
		this->parserStatus = LEIDO_CODIGO_ERROR_RESPONSE;
	}
	if (this->parserStatus == LEIDO_CODIGO_ERROR_RESPONSE)
	{
		// leer los separadores entre C�DIGO-DE-ERROR (de una respuesta) y las cabeceras
		// TODO: quiz�s ahora toque leer el "c�digo de error como string"; por ahora no se env�a,
		//		por lo que no hace falta, y se busca el FIN_DE_LINEA
		prevpos = mensaje.find_first_not_of (FIN_DE_LINEA, pos);
		// en [prevpos, pos) hay espacios
		if (prevpos == std::string::npos)
		{
			this->bufferIncompleto = mensaje.substr (pos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		this->parserStatus = TOCA_LEER_CABECERA;
	}
	while (this->parserStatus == TOCA_LEER_CABECERA)
	{
		// leer las cabeceras
		pos = mensaje.find_first_of (FIN_DE_LINEA, prevpos);
		// en [prevpos, pos) hay espacios
		if (pos == std::string::npos)
		{
			// la cabecera actual no est� completa
			this->bufferIncompleto = mensaje.substr (prevpos);
			this->lastError = MENSAJE_INCOMPLETO;
			return false;
		}
		// en [prevpos, pos) est� la cabecera
		if (prevpos < pos)
		{
			// existe una cabecera, as� que se se procesa y se sigue en el bucle
			std::string newHeader = mensaje.substr (prevpos, pos - prevpos);
			const Encabezado encabezado = Mensaje::convertToEncabezado (newHeader);
			this->setEncabezadoValor (encabezado);
			prevpos = pos + sizeof FIN_DE_LINEA -
					  1;	// TODO: dejarlo as� de "inestable"??? :: solo admite finales de l�nea "\r\n"
		}
		else
		{
			// ya no hay �s cabeceras, se saldr� del bucle while
			prevpos = pos + sizeof FIN_DE_LINEA -
					  1;	// TODO: dejarlo as� de "inestable"??? :: solo admite finales de l�nea "\r\n"
			this->parserStatus = TOCA_LEER_CUERPO;
			this->cuerpo.clear(); // vaciarlo para que no d� problemas
		}
	}
	if (this->parserStatus == TOCA_LEER_CUERPO)
	{
		this->cuerpo += mensaje.substr (
							prevpos); //a�adir a lo que ya hay en el cuerpo (en la primera vuelta deber� est�r vac�o)
		if (true == this->existeEncabezadoValor (ENCABEZADOID_CONTENT_LENGTH))
		{
			// Si est� definido el encabezado "Content-Length", este marcar� el final del cuerpo
			const size_t contentLength = ::fromStr<size_t> (this->getEncabezadoValor (ENCABEZADOID_CONTENT_LENGTH));
			if (contentLength > this->cuerpo.size())
			{
				// si a�n no est� todo el cuerpo:
				this->lastError = MENSAJE_INCOMPLETO;
				return false;
			}
			else if (contentLength < this->cuerpo.size())
			{
				// si el cuerpo es m�s grande que lo esperado:
				this->cuerpo.resize (contentLength); // recortarlo
			}
		}
		else
		{
			// si no est� definido el encabezado "Content-Length":
			// se supone que estar� todo
		}
	}
	this->clearParser();
	this->lastError = MENSAJE_CORRECTO;
	return true;	// return true => termin� (bien)
}

/**
*	Obtiene el estado de la instancia
*	\retval	el estado de la instancia
**/
Mensaje::Errores Mensaje::getLastError() const
{
	return this->lastError;
}

/**
*	Obtiene el estado del evaluador
*	\retval	el estado del evaluador
**/
Mensaje::ParserStates Mensaje::getParserStatus() const
{
	return this->parserStatus;
}

/**
 *	Limpia el evaluador: su estado, el buffer, y tambi�n el error general de la instancia
**/
void Mensaje::clearParser()
{
	this->parserStatus = Mensaje::BUFFER_VACIO;
	this->bufferIncompleto = "";
	this->lastError = SIN_ERROR;
}

// ----------------------------------------------------------------------------
// Accesores (constantes):
/**
 *	Obtiene el tipo de mensaje actual como el enumerado propio de esta clase
 *	\retval	el tipo de mensaje esperado
**/
const Mensaje::TiposMensaje & Mensaje::getTipoMensaje() const
{
	return this->tipoMensaje;
}
const Mensaje::VersionesProtocolo & Mensaje::getVersionProtocolo() const
{
	return this->versionProtocolo;
}
const std::string & Mensaje::getBody() const
{
	return this->cuerpo;
}
const Mensaje::ErroresProtocolo & Mensaje::getError() const
{
	return this->error;
}
const std::string & Mensaje::getURI() const
{
	return this->uri;
}
/**
 *	Obtiene el n�mero de encabezados que hay
 *  \retval	 el n�mero de encabezados que hay
 **/
size_t Mensaje::getEncabezadoSize() const
{
	return this->encabezados.size();
}

/**
 *	Determina si existe un encabezado con el identificador dado
 *	\param	[in]	id	clave del encabezado buscado
 *  \retval	 TRUE si ese idenfiticador del encabezado existe (sea vac�o o no); FALSE en caso contrario
 **/
bool Mensaje::existeEncabezadoValor (const Mensaje::EncabezadoId & id) const
{
	Mensaje::Encabezados::const_iterator itr = encabezados.find (id);
	if (encabezados.find (id) != encabezados.end())
	{
		return true;
	}
	return false;
}

// ----------------------------------------------------------------------------
// Funciones para trabajar con los par�metros enviados por el protocolo
/**
 *	Obtiene el par nombre-valor dando la cadena que lo contiene
 *	\param	[in]	value	strign con el par a procesar
 *  \retval	 el par nombre-valor esperado
 **/
Mensaje::ParVars Mensaje::getParVars (const std::string & value)
{
	Mensaje::ParVars ret;
	size_t i = value.find (W_RED_URI_GETPOST_ASIGNATOR);
	if (i != std::string::npos)
	{
		ret.first = value.substr (0, i - 0);
		ret.second = Mensaje::urlDecode (value.substr (i + 1));
	}
	else
	{
		ret.first = value;
	}
	return ret;
}

/**
 *	Obtiene el array de variables GET que hay en la petici�n actual
 *  \retval	 el array de variables esperado
**/
Mensaje::ArrayVars Mensaje::getVarsFromStr (const std::string & value)
{
	Mensaje::ArrayVars ret;
	if (value != "")
	{
		size_t i = 0;
		while (true)
		{
			size_t j = value.find (W_RED_URI_GETPOST_SEPARATOR, i);
			if (j == std::string::npos)
			{
				break;
			}
			const ParVars & par = Mensaje::getParVars (value.substr (i, j - i));
			ret.insert (par);
			i = j + 1;
		}
		const ParVars & par = Mensaje::getParVars (value.substr (i));
		ret.insert (par);
	}
	return ret;
}

/**
 *	Obtiene la parte URI (path) de la URL recibida (sin escapar)
 *  \retval	 el string correspondiente a la URI (sin parte GET ni indicador de inicio del GET)
 **/
std::string Mensaje::getUriPathRecibed() const
{
	Mensaje::ArrayVars ret;
	size_t i = this->uri.find (W_RED_URI_GET_INICIATOR);
	if (i == std::string::npos)
	{
		// no hay iniciador del "GET" en la URL
		return this->uri;	// todo es URI
	}
	return this->uri.substr (0, i);
}

/**
 *	Obtiene la parte URI (path) de la URL legible
 *  \retval	 el string correspondiente a la URI (sin parte GET ni indicador de inicio del GET)
 **/
std::string Mensaje::getUriPath() const
{
	return Mensaje::urlDecode (this->getUriPathRecibed());
}

/**
 *	Obtiene la parte "GET" (Query) de la URL
 *  \retval	 el string correspondiente al GET de la URL (sin parte UIR ni indicador de inicio del GET)
**/
std::string Mensaje::getUriQuery() const
{
	size_t i = this->uri.find (W_RED_URI_GET_INICIATOR);
	if (i == std::string::npos)
	{
		// no hay iniciador del "GET" en la uri
		return ""; // no hay parte "GET"
	}
	i += std::string (W_RED_URI_GET_INICIATOR).size();
	return this->uri.substr (i);
}


/**
 *	Obtiene el vector de variables URI que hay en la petici�n actual, troceando la URI por "W_RED_URI_SUBSEPARATORS"
 *  \retval	 el array de variables esperado
 **/
Mensaje::VectorVars Mensaje::getUriVars() const
{
	Mensaje::VectorVars ret;
	{
		const std::string & uri = this->getUriPath();
		std::string temporal = "";
		for (size_t i = 0; i < uri.size(); i++)
		{
			switch (uri[i])
			{
			case W_RED_URI_SUBSEPARATORS:
				ret.push_back (Mensaje::urlDecode (temporal));
				temporal = "";
				break;

			default:
				temporal.push_back (uri[i]);
				break;
			}
		}
		ret.push_back (Mensaje::urlDecode (temporal)); // NOTA: siempre habr� [0]; con "" si la uri es vac�a
	}
	return ret;
}

/**
 *	Obtiene el array de variables GET que hay en la petici�n actual
 *  \retval	 el array de variables esperado
 **/
Mensaje::ArrayVars Mensaje::getGetVars() const
{
	return Mensaje::getVarsFromStr (this->getUriQuery());
}

/**
 *	Obtiene el array de variables POST que hay en la petici�n actual
 *  \retval	 el array de variables esperado
**/
Mensaje::ArrayVars Mensaje::getPostVars() const
{
	return Mensaje::getVarsFromStr (this->cuerpo);
}

/**
 *	Obtiene el array de variables POST que hay en la petici�n actual
 *  \retval	 el array de variables esperado
**/
Mensaje::ArrayVars Mensaje::getCookVars() const
{
	return Mensaje::getVarsFromStr (this->getEncabezadoValor (ENCABEZADOID_COOKIE));
}

/**
 *	Obtiene el array de variables POST que hay en la petici�n actual
 *  \retval	 el array de variables esperado
**/
Mensaje::ArrayVars Mensaje::getRequestVars() const
{
	Mensaje::ArrayVars ret;
	{
		const Mensaje::ArrayVars & getVars = this->getGetVars();
		ret.insert (getVars.begin(), getVars.end());
	}
	{
		const Mensaje::ArrayVars & cookVars = this->getCookVars();
		ret.insert (cookVars.begin(), cookVars.end());
	}
	{
		const Mensaje::ArrayVars & postVars = this->getPostVars();
		ret.insert (postVars.begin(), postVars.end());
	}
	return ret;
}

/**
 *	Obtiene el valor de la variable dad
 *	\param	[in]	id	clave de la variable buscada
 *  \retval	 la clave de la variable dada, o "" si no existe
**/
std::string Mensaje::getRequestVar (const std::string & id) const
{
	const Mensaje::ArrayVars reqVars = this->getRequestVars();
	const Mensaje::ArrayVars::const_iterator itr = reqVars.find (id);
	if (itr == reqVars.end())
	{
		// si no existe
		return "";
	}
	return itr->second;

}

/**
 *	Determina si la variable dada existe
 *	\param	[in]	id	clave de la variable buscada
 *  \retval	 TRUE si la variable dada existe; o FALSE en caso contrario
**/
bool Mensaje::existeRequestVar (const std::string & id) const
{
	const Mensaje::ArrayVars reqVars = this->getRequestVars();
	if (reqVars.find (id) == reqVars.end())
	{
		// si no existe
		return false;
	}
	return true;
}


/**
 *	Obtiene la referencia constante al encabezado dado
 *	\param	[in]	id	clave del encabezado buscado
 *  \retval	 el valor del encabezado dado, o "encabezadoPorDefecto" si no existe (es const)
 **/
const Mensaje::EncabezadoValor & Mensaje::getEncabezadoValor (const Mensaje::EncabezadoId & id) const
{
	Mensaje::Encabezados::const_iterator itr = encabezados.find (id);
	if (encabezados.find (id) != encabezados.end())
	{
		return itr->second;
	}
	return encabezadoPorDefecto;	// encabezado por defecto
}

/**
 *	Devuelve una referencia (constante o no) al atributo solicitado
 *	\param	[in]	id	clave del atributo buscado
 *  \retval	 la referencia esperada.
 *				Si no existe, da error si se pide constante;
 *					y crea el atributo, si se pide no-const
 *					devolviendo posteriormente la referencia esperada.
 **/
const Mensaje::EncabezadoValor & Mensaje::operator% (const Mensaje::EncabezadoId & id) const
{
	Encabezados::const_iterator itr = encabezados.find (id);
	if (itr != encabezados.end())
	{
		return itr->second;
	}
	return encabezadoPorDefecto;	// encabezado por defecto
}
Mensaje::EncabezadoValor & Mensaje::operator% (const Mensaje::EncabezadoId & id)
{
	Mensaje::Encabezados::iterator itr = encabezados.find (id);
	if (itr == encabezados.end())
	{
		//< crear un atributo nuevo
		std::pair<Mensaje::EncabezadoId, Mensaje::EncabezadoValor> tmp;
		tmp.first = id;
		tmp.second = "";
		itr = encabezados.insert (tmp).first;
	}
	return itr->second;	//< devolver la referencia al atributo (existente o nuevo)
}

// ----------------------------------------------------------------------------
// Modificadores:
/**
 *	Asigna el tipo de mensaje dado como el enumerado propio de esta clase
 *	\param	[in]	value		tipo de mensaje dado
**/
void Mensaje::setTipoMensaje (const Mensaje::TiposMensaje & value)
{
	this->tipoMensaje = value;
}
void Mensaje::setVersionProtocolo (const Mensaje::VersionesProtocolo & value)
{
	this->versionProtocolo = value;
}
void Mensaje::setEncabezadoValor (const Mensaje::EncabezadoId & id, const Mensaje::EncabezadoValor & value)
{
	this->encabezados [id] = value;
}
void Mensaje::setEncabezadoValor (const Mensaje::Encabezado & value)
{
	this->encabezados [value.first] = value.second;
}
void Mensaje::setBody (const std::string & value)
{
	this->cuerpo = value;
	this->encabezados[ENCABEZADOID_CONTENT_LENGTH] = ::toStr (this->getBody().size());
	this->encabezados[ENCABEZADOID_CONTENT_TYPE]   = ENCABEZADOID_CONTENT_TYPE_HTML_UTF8;
}
void Mensaje::setError (const Mensaje::ErroresProtocolo & value)
{
	this->error = value;
}
void Mensaje::setURI (const std::string & value)
{
	this->uri = value;
}

// ----------------------------------------------------------------------------
// Utilidades est�ticas propias de la clase:
/**
 *	Convierte un string en valor del enumerado local Mensaje::TiposMensaje
 *	\param	[in]	value		tipo de mensaje como string
 *	\retval	el Tipo de Mensaje del enumerado de la clase local correspondiente
 *		o	Mensaje::UNKNOWN si no se conoce
**/
Mensaje::TiposMensaje Mensaje::convertToTiposMensaje (const std::string & value)
{
	// TODO: mejor tener un �rbol est�tico privado donde hacer esta traducci�n en log(n)
	if (value == W_RED_TIPOMENSAJE_GET)
	{
		return Mensaje::GET;
	}
	else if (value == W_RED_TIPOMENSAJE_HEAD)
	{
		return Mensaje::HEAD;
	}
	else if (value == W_RED_TIPOMENSAJE_POST)
	{
		return Mensaje::POST;
	}
	else if (value == W_RED_TIPOMENSAJE_PUT)
	{
		return Mensaje::TIPO_PUT;
	}
	else if (value == W_RED_TIPOMENSAJE_DELETE)
	{
		return Mensaje::TIPO_DELETE;
	}
	else if (value == W_RED_TIPOMENSAJE_TRACE)
	{
		return Mensaje::TIPO_TRACE;
	}
	else if (value == W_RED_TIPOMENSAJE_OPTIONS)
	{
		return Mensaje::TIPO_OPTIONS;
	}
	else if (value == W_RED_TIPOMENSAJE_CONNECT)
	{
		return Mensaje::TIPO_CONNECT;
	}
	return Mensaje::UNKNOWN;
}

/**
 *	Convierte a string el valor del enumerado local Mensaje::UNKNOWN
 *	\param	[in]	value		tipo de mensaje como valor del enumerado
 *	\retval	el Tipo de Mensaje del enumerado de la clase local correspondiente
 *		o	"" si no se conoce
**/
std::string Mensaje::convertFromTiposMensaje (const Mensaje::TiposMensaje & value)
{
	// TODO: mejor tener un �rbol est�tico privado donde hacer esta traducci�n en log(n)
	if (value == Mensaje::GET)
	{
		return W_RED_TIPOMENSAJE_GET;
	}
	else if (value == Mensaje::HEAD)
	{
		return W_RED_TIPOMENSAJE_HEAD;
	}
	else if (value == Mensaje::POST)
	{
		return W_RED_TIPOMENSAJE_POST;
	}
	else if (value == Mensaje::TIPO_PUT)
	{
		return W_RED_TIPOMENSAJE_PUT;
	}
	else if (value == Mensaje::TIPO_DELETE)
	{
		return W_RED_TIPOMENSAJE_DELETE;
	}
	else if (value == Mensaje::TIPO_TRACE)
	{
		return W_RED_TIPOMENSAJE_TRACE;
	}
	else if (value == Mensaje::TIPO_OPTIONS)
	{
		return W_RED_TIPOMENSAJE_OPTIONS;
	}
	else if (value == Mensaje::TIPO_CONNECT)
	{
		return W_RED_TIPOMENSAJE_CONNECT;
	}
	else if (value == Mensaje::UNKNOWN)
	{
		return W_RED_TIPOMENSAJE_UNKNOWN;
	}
	return "";
}

/**
 *	Convierte un string en valor del enumerado local Mensaje::VersionesProtocolo
 *	\param	[in]	value		versi�n del protocolo como string
 *	\retval	la Versi�n del Protocolo del enumerado de la clase local correspondiente
 *		o	Mensaje::VERSION_DESCONOCIDA si no se conoce
**/
Mensaje::VersionesProtocolo Mensaje::convertToVersion (const std::string & value)
{
	// Primero el nuestro, que es m�s importante que nada
	if (value == W_RED_VERSIONPROTOCOLO_EWA)
	{
		return Mensaje::EWA1_0;
	}
	// Despu�s, el resto, los est�ndares de HTTP
	if (value == W_RED_VERSIONPROTOCOLO_10)
	{
		return Mensaje::HTTP1_0;
	}
	else if (value == W_RED_VERSIONPROTOCOLO_11)
	{
		return Mensaje::HTTP1_1;
	}
	return Mensaje::VERSION_DESCONOCIDA;
}
/**
 *	Convierte a string el valor del enumerado local Mensaje::VersionesProtocolo dado
 *	\param	[in]	value		versi�n del protocolo como enumerado
 *	\retval	la Versi�n del Protocolo del enumerado de la clase local correspondiente
 *		o	"" si no se conoce
**/
std::string Mensaje::convertFromVersion (const Mensaje::VersionesProtocolo & value)
{
	// Primero el nuestro, que es m�s importante que nada
	if (value == Mensaje::EWA1_0)
	{
		return W_RED_VERSIONPROTOCOLO_EWA;
	}
	// Despu�s, el resto, los est�ndares de HTTP
	if (value == Mensaje::HTTP1_0)
	{
		return W_RED_VERSIONPROTOCOLO_10;
	}
	else if (value == Mensaje::HTTP1_1)
	{
		return W_RED_VERSIONPROTOCOLO_11;
	}
	return "";
}
/**
 *	Convierte un string en valor del Encabezado local Mensaje::Encabezado
 *	\param	[in]	value		l�nea de un encabezado como string
 *	\retval	el Encabezado de la clase local correspondiente
 *		o	el Encabezado por defecto si no se puede convertir
**/
Mensaje::Encabezado Mensaje::convertToEncabezado (const std::string & value)
{
	Mensaje::Encabezado encabezado;
	size_t inicioID = 0;
	size_t finId = value.find (ENCABEZADO_SEPARATOR);
	if (finId != std::string::npos)
	{
		encabezado.first = value.substr (inicioID, finId - inicioID);
		size_t inicioValue = finId + sizeof (ENCABEZADO_SEPARATOR) - 1;
		size_t finValue = value.size();
		encabezado.second = value.substr (inicioValue, finValue - inicioValue);
	}
	return encabezado;
}

// ----------------------------------------------------------------------------
// Generadores automatizados:
/**
 *	Ajusta los datos locales correspondientes a un Mensaje de Respuesta Web Por Defecto en HTTP
 *		con un cuerpo dado
 *	\param	[in]	body				cuerpo que se usar� en al respuesta que se genera
 *	\param	[in]	error	opt.=200	c�digo de error que incluir� la respuesta
 *	\retval	void	 // TODO �devolver "genPeticion()"?
**/
void Mensaje::setRespuestaWebPorDefecto (const std::string & body,
		const Mensaje::ErroresProtocolo err /*= FOUND(200)*/)
{
	this->encabezados.clear();
	this->tipoMensaje = Mensaje::RESPONSE_AND_EXIT;
	this->versionProtocolo = Mensaje::HTTP1_0;
	this->uri = "";
	this->setBody (std::string ("<code>") + body + std::string ("</code>"));
	this->error = err;
}

/**
 *	Devuelve una cadena con el Mensaje correspondiente a los datos locales actuales
 *	\retval	la cadena esperada
 *	\see Status Code Definitions: http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
**/
// OJO: Como el protocolo HTTP no manda el tipo de mensaje en las respuestas,
//			por lo tanto, ambos generan la misma cabecera
std::string Mensaje::genMsg() const
{
	std::string ret = "";
	{
		// A�adir primera l�nea
		switch (this->tipoMensaje)
		{
		case Mensaje::RESPONSE :
		case Mensaje::RESPONSE_AND_EXIT :
			// se trata de una respuesta:
			ret += Mensaje::convertFromVersion (this->versionProtocolo);
			ret += " ";
			ret += ::toStr (this->error);
			//? TODO: �a�adir el c�digo STRING del error HTTP (Status Code Definition)? Ver see.
			break;
		default:
			// se trata de una consulta:
			ret += Mensaje::convertFromTiposMensaje (this->tipoMensaje);
			ret += " ";
			ret += this->uri;
			ret += " ";
			ret += Mensaje::convertFromVersion (this->versionProtocolo);
			break;
		}
	}
	ret += FIN_DE_LINEA;
	{
		// A�adir cabeceras
		Mensaje::Encabezados::const_iterator itr;
		for (itr = this->encabezados.begin(); itr != this->encabezados.end(); itr++)
		{
			// TODO: hacer un foreach de los encabezados
			ret += itr->first + ENCABEZADO_SEPARATOR + itr->second;
			ret += FIN_DE_LINEA;
		}
		// A�adir el "Content-Length" si no existe
		if (this->existeEncabezadoValor (ENCABEZADOID_CONTENT_LENGTH) == false)
		{
			ret += ENCABEZADOID_CONTENT_LENGTH ENCABEZADO_SEPARATOR + ::toStr (this->getBody().size());
			ret += FIN_DE_LINEA;
		}
	}
	ret += FIN_DE_LINEA;	//< separador de cabecera-cuerpo
	{
		// A�adir cuerpo
		ret += this->getBody();
	}
	return ret;
}
