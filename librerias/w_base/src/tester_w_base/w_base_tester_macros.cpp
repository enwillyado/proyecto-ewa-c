/*********************************************************************************************
 *	Name		: w_base_tester_macros.cpp
 *	Description	: Pruebas unitarias para probar los mecanismos de macros
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_base/macros_variadic.hpp"

#include <string>

#include <assert.h>

#define VALUES a1, a2, a3, a4, a5, a6, a7, a8, a9, a0,\
	b1, b2, b3, b4, b5, b6, b7, b8, b9, b0,\
	c1, c2, c3, c4, c5, c6, c7, c8, c9, c0,\
	d1, d2

int baseTesterMacros()
{
	{
		// Tama�o de tuplas
		assert (PP_TUPLE_SIZE (A) == 1);
		assert (PP_TUPLE_SIZE (A, B) == 2);
		assert (PP_TUPLE_SIZE (A, B, C) == 3);
		assert (PP_TUPLE_SIZE (A, B, C, D) == 4);
		assert (PP_TUPLE_SIZE (A, B, C, D, E) == 5);
		assert (PP_TUPLE_SIZE (VALUES) == 32);
	}
	{
		// Convertir en cadenas tokens
		assert (std::string (PP_STRINGIZE (A)) == std::string ("A"));
		assert (std::string (PP_STRINGIZE (PP_TUPLE_SIZE (A))) == std::string ("1"));
		assert (std::string (PP_STRINGIZE (PP_TUPLE_SIZE (A) - 1)) == std::string ("1 - 1"));
		assert (std::string (PP_STRINGIZE (PP_TUPLE_SIZE (A) y 1)) == std::string ("1 y 1"));
	}
	{
		// Definir acci�n secuencial
#define ACTION_1(x) "("PP_STRINGIZE(x)")"

		// Ejecutar secuencialmente la acci�n sobre los elementos
		const std::string resultado (PP_FOR_EACH (ACTION_1, VALUES));
		assert ("(a1)(a2)(a3)(a4)(a5)(a6)(a7)(a8)(a9)(a0)"\
				"(b1)(b2)(b3)(b4)(b5)(b6)(b7)(b8)(b9)(b0)"\
				"(c1)(c2)(c3)(c4)(c5)(c6)(c7)(c8)(c9)(c0)"\
				"(d1)(d2)" == resultado);
	}
	{
		// Definir acci�n iterativa
#define ACTION_2(x, i) "("PP_STRINGIZE(i)":"PP_STRINGIZE(x)")"

		// Ejecutar iterativamente la acci�n sobre los elementos
		const std::string resultado (PP_ITERATE (ACTION_2, VALUES));
		assert ("(32:a1)(31:a2)(30:a3)(29:a4)(28:a5)(27:a6)(26:a7)(25:a8)(24:a9)(23:a0)"\
				"(22:b1)(21:b2)(20:b3)(19:b4)(18:b5)(17:b6)(16:b7)(15:b8)(14:b9)(13:b0)"\
				"(12:c1)(11:c2)(10:c3)(9:c4)(8:c5)(7:c6)(6:c7)(5:c8)(4:c9)(3:c0)"\
				"(2:d1)(1:d2)" == resultado);
	}
	return 0;
}
