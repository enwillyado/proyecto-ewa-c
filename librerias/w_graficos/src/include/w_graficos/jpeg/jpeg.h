
/*             _      | |        |  _      | |      |  | |
 *            / \     | |          / \     | |  /|  |  | |             \
 *      Q___|____\  __| | | . |__Q____\  __| | (_|__|__| |  Q_|__|__|___)
 *  ___/    :      /      |___|         /               ___/          .
 *               _/                   _/
 *
 */

//  Written by Hamed Ahmadi Nejad
//    ahmadinejad@ce.sharif.edu
//    comphamed@yahoo.com

#ifndef jpeg_h
#define jpeg_h

#include "w_graficos/jpeg/pixel.h"
#include "w_graficos/w_rgba.h"

class Picture
{
	int w, h;
	Pixel** p;
	bool isColor();  //See if there is color in the picture
	bool process();

public:
	Picture();
	Picture (int width, int height);
	~Picture();
	int getwidth()
	{
		return w;
	}
	int getheight()
	{
		return h;
	}
	void setsize (int width, int height); //kills all image data
	void setpixel (int x, int y, int r, int g, int b);
	void setpixelInt (int x, int y, int rgb);
	void getpixel (int x, int y, int & r, int & g, int & b);
	RGBA getpixel (int x, int y);
	int getpixelInt (int x, int y);

	bool processStr (const std::string &);	// Use JPEG string
	bool open (const char* filename);		// Open a JPEG file
	int save (const char* filename, int quality = 75, int h1 = 2, int v1 = 2); //Save to JPEG file
};

#endif
