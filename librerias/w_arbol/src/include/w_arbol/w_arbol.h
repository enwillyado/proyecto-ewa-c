/*********************************************************************************************
 *	Name		: darbol.h
 *	Description	: Arbol para trabajar m�s c�modamente con una estructura Atributos+Hijo
 * 					en memoria, usando la std
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ARBOL_H_
#define _W_ARBOL_H_

#include <string>
#include <vector>
#include <map>

class Arbol;

class Arbol
{
public:
	typedef size_t IndexHijo;
	typedef std::vector<IndexHijo> IndexHijos;
	typedef std::vector<Arbol> Arboles;
	typedef std::vector<std::string> strvector;
	typedef std::pair<std::string, std::string> Atributo;
	typedef std::map<std::string, std::string> Atributos;

	enum Errores
	{
		SIN_ERROR = 0,
		PARSER_ERROR = 1,
		NO_EXISTE_FICHERO,
		DESCONOCIDO,
		NO_EXISTE_ATRIBUTO,
		NO_EXISTE_HIJO
	};
	enum Parametros
	{
		NO_INDEX = (IndexHijo) - 1,
	};

public:
	// Constructor (crea un nodo con la clave dada y el valor dado)
	Arbol (const std::string & nuevaClave = "", const std::string & nuevoValor = "");

	// ------------------------------------------------------------------------
	// Accesores
	size_t hijosize() const;
	size_t atributosize() const;

	const std::string & getClave() const;
	const std::string & getValor() const;
	const Arboles & getHijos() const;
	const Atributos & getAtributos() const;

	// ------------------------------------------------------------------------
	// Modificadores
	void setClave (const std::string &);
	void setValor (const std::string &);

	// ------------------------------------------------------------------------
	// Operadores
	// a) Este para acceder a los hijos (elementos) (solo lectura y lectura-escritura)
	Arbol & operator[] (const IndexHijo index);			//< indicando el �ndice
	Arbol & operator[] (const std::string & indexstr);	//< indicando el nombre del hijo (puede haber varios con el mismo nombre)
	const Arbol & operator[] (const IndexHijo index) const;
	const Arbol & operator[] (const std::string & indexstr) const;

	// b) Y este para acceder a los atributos (solo lectura y lectura-escritura)
	const std::string &	operator% (const std::string & indexstr) const;
	std::string 	&	operator% (const std::string & indexstr);

	Arbol		operator% (const Arbol & indexstr) const;
	Arbol 	&	operator%= (const Arbol & indexstr);

	// c) Operador de comparaci�n
	bool			operator== (const Arbol & otro) const;
	bool			operator!= (const Arbol & otro) const;

	// Comparador de igualdad expl�cito
	bool equals (const Arbol & otro) const;

	// ------------------------------------------------------------------------
	// Operaciones con hijos
	bool existeHijo (const std::string & indexstr) const;
	IndexHijo  getNextIndexHijo (const std::string & indexstr, IndexHijo initIndex = 0) const;
	IndexHijo  getIndexHijoAt (const std::string & indexstr, size_t posicion = 0) const;
	IndexHijos getIndexHijos (const std::string & indexstr) const;

	void addHijo (const Arbol &);		//< forzar que entre ese hijo
	void addHijo (const std::string &);		//< forzar que entre un hijo con esa clave si no existe

	bool eraseHijo (const std::string &);		//< elimina la primera (si existe) instancia de un hijo con esta clave

	// ------------------------------------------------------------------------
	// Operaciones con atributos
	bool existeAtributo (const std::string & indexstr) const;
	Atributo getAtributo (const IndexHijo indexstr) const;
	std::string getValorAtributo (const std::string & indexstr) const;

	void addAtributo (const Arbol &);		//< forzar a a�adir los atributos del arbol dado (sin recursividad)
	void addAtributo (const Atributos &);	//< forzar a a�adir los atributos dados
	void addAtributo (const std::string & indexstr, const std::string & valor);
	bool setValorAtributo (const std::string & indexstr, const std::string & valor);

	bool eraseAtributo (const std::string & indexstr);		//< elimina el atributo con esta clave, si existe

	// ------------------------------------------------------------------------
	// Control de errores
	void clearError();
	void setError (const Errores error);
	const Errores & getLastError() const;
	void setError (const std::string & error);
	const std::string & getLastErrorStr() const;

	// Instancia nula
	static const Arbol npos;

protected:
	// �rea de datos protegida
	std::string clave;
	std::string valor;
	Atributos atributos;
	Arboles hijos;

private:
	Arbol (const Arbol::Errores);
	mutable Errores lastError;
	std::string lastErrorStr;

	static const std::string nocampo;
};

#endif	//_W_ARBOL_H_
