/*********************************************************************************************
 *	Name		: w_make_fuente.cpp
 *	Description	: Implementanción de las funciones para preparar fuente
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_make/w_make.h"

#include "w_sistema/w_sistema_execs.h"
#include "w_sistema/w_sistema_files.h"
#include "w_base/filesStr.hpp"

#include <iostream>

WMake::Estado WMake::prepararObj()
{
	SistemaExecsSimple exec;

	const std::string & dirOrigen = this->parametros.dirOrigen;
	const std::string & dirObjDestino = this->parametros.dirObjDestino;

	const strvector & srcOrigen = this->parametros.srcOrigen;
	if (0 < srcOrigen.size())
	{
		// Crear el directorio de destino de los ficheros objeto
		const std::string & dirObjDestinoFinal = dirOrigen + dirObjDestino;
		std::cout << "+" << dirObjDestinoFinal << std::endl;
		WFiles::createDirPath (dirObjDestinoFinal);
	}

	// Crear los ficheros objeto a partir de los de origen
	for (size_t i = 0; i < srcOrigen.size(); i++)
	{
		const std::string & itemIn = srcOrigen[i];
		const std::string & itemInName = WFilesStr::getNombreOnly (itemIn);
		const std::string & itemOut = dirObjDestino + itemInName + ".o";
		this->parametros.objOrigen.push_back (itemOut);

		std::string comando;
		comando += "g++ -c ";
		comando += (dirOrigen + itemIn);
		comando += " -o ";
		comando += (dirOrigen + itemOut);
		for (size_t iInclude = 0; iInclude < this->parametros.carpetasInclusion.size(); iInclude++)
		{
			comando += " -I" + dirOrigen + this->parametros.carpetasInclusion[iInclude];
		}

		exec.setComando (comando + STDERR_REDIRECT);
		std::cout << "[]" << exec.getComando() << std::endl;
		exec.exeComando();

		const std::string & salida = exec.getSalidaComando();
		std::cout << "[]>" << salida << std::endl;
	}

	return WMake::CORRECTO;
}
