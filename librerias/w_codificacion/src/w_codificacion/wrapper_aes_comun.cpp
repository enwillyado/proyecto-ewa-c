/*********************************************************************************************
 *	Name		: wrapper_aes_deco.cpp
 *	Description	: Wrapper de C++ para algoritmo de AES soportando los tipos mas comunes de C++
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion.h"

EEncryptResult contrToAESKeyEncrypt (AES_KEY & key, const std::string & contr)
{
	if (contr.size() > sizeof key)
	{
		return EncryptErrorBadParam;
	}
	for (size_t i = 0; i < sizeof key; i++)
	{
		key[i] = '0';
	}

	const std::string & sub = contr.substr (0, sizeof key).c_str();
	for (size_t i = 0; i < sub.size() && i < sizeof key; i++)
	{
		key[i] = sub[i];
	}

	return EncryptOk;
}
