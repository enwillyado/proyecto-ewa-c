/*********************************************************************************************
 * Name			: w_sistema_execs_base.cpp
 * Description	: Clase base para llamar a un comando simple
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema_execs_base.h"

SistemaExecsBase::SistemaExecsBase()
	: lastError (SistemaExecsBase::SIN_SELECCIONAR),
	  funcionChild (NULL),
	  lastExecStatus (0), salidaGenerada ("")
{
}

SistemaExecsBase::Errores SistemaExecsBase::getLastError() const
{
	return this->lastError;
}

void SistemaExecsBase::setComando (const std::string & valor)
{
	this->comando = valor;
}
const std::string & SistemaExecsBase::getComando() const
{
	return this->comando;
}

const std::string & SistemaExecsBase::getSalidaComando() const
{
	return this->salidaGenerada;
}
int SistemaExecsBase::getLastExecStatus() const
{
	return this->lastExecStatus;
}
