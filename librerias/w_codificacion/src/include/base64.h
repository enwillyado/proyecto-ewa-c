/*********************************************************************************************
 *	Name		: base64.h
 *	Description	: Funciones para trabajar con Base64
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _BASE64_H_
#define _BASE64_H_

#ifdef __cplusplus
extern "C" {
#endif

int base64_encode (const unsigned char* src, size_t srclen, char* dest, size_t destlen);
int base64_decode (unsigned char* dest, const char* src, size_t destlen);

char* base64_str_encode (const char* s, size_t len);

#ifdef __cplusplus
}
#endif

#endif //_BASE64_H_
