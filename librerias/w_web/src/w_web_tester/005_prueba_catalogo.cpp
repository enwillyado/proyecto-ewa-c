#include <boost/test/unit_test.hpp>

#include "w_web/w_web_catalogo.h"		//< para poder tener el cat�logo de servicios del servidor
#include "w_web/w_web_services.h"		//< para poder tener los servicios del servidor catalogados

BOOST_AUTO_TEST_CASE (Test_CatalogoServicios)
{
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);

#ifdef SRVSERVICE_PING_ID
	{
		// Se a�ade este servicio (m�todo c, con puntero: DESACONSEJABLE HACERLO AS� -porque hay que liberar memoria-)
		BasicSrvService_Ping* servicio = new BasicSrvService_Ping();
		if (servicio == NULL)
		{
			BOOST_MESSAGE ("[ERROR] new BasicSrvService_Ping(): No se puede crear el servicio '" SRVSERVICE_PING_ID "'");
			BOOST_CHECK (false);
		}
		BOOST_MESSAGE ("+ A�adiendo al cat�logo el servicio '" SRVSERVICE_PING_ID "'...");
		BasicSrvServicesCatalogo::addService (servicio);

		BOOST_MESSAGE ("+ Buscando el servicio '" SRVSERVICE_PING_ID "' en el catalogo...");
		BOOST_CHECK (BasicSrvServicesCatalogo::getService (SRVSERVICE_PING_ID) != NULL);
		const std::string & serviceName = BasicSrvServicesCatalogo::getService (SRVSERVICE_PING_ID)->getServicioName();
		BOOST_CHECK (serviceName == SRVSERVICE_PING_ID);

		// Borrarlo cuando ya existe
		BOOST_CHECK (true == BasicSrvServicesCatalogo::delService (SRVSERVICE_PING_ID));
		// Volverlo a borrar
		BOOST_CHECK (false == BasicSrvServicesCatalogo::delService (SRVSERVICE_PING_ID));
		BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);

		// Se libera la memoria de este servicio
		// NOTA: con la nueva implementaci�n de catalogar por copia, se puede liberar tras a�adirlo al cat�logo
		if (servicio != NULL)
		{
			delete servicio;
			servicio = NULL;
		}
	}
	{
		// Se a�ade este servicio (m�todo c++, con referencia: NO SE TIENE QUE BORRAR =D )
		BOOST_MESSAGE ("+ A�adiendo al cat�logo el servicio '" SRVSERVICE_PING_ID "'...");
		BasicSrvServicesCatalogo::addService (BasicSrvService_Ping());

		BOOST_MESSAGE ("+ Buscando el servicio '" SRVSERVICE_PING_ID "' en el catalogo...");
		BOOST_CHECK (BasicSrvServicesCatalogo::getService (SRVSERVICE_PING_ID) != NULL);
		const std::string & serviceName = BasicSrvServicesCatalogo::getService (SRVSERVICE_PING_ID)->getServicioName();
		BOOST_CHECK (serviceName == SRVSERVICE_PING_ID);

		// Borrarlo cuando ya existe
		BOOST_CHECK (BasicSrvServicesCatalogo::size() == 1);
		BOOST_CHECK (true == BasicSrvServicesCatalogo::delService (SRVSERVICE_PING_ID));
		BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);
		// Volverlo a borrar
		BOOST_CHECK (false == BasicSrvServicesCatalogo::delService (SRVSERVICE_PING_ID));
		BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);
	}
#else
	BOOST_MESSAGE ("NO SE PUEDE REALIZAR LA PRUEBA PORQUE NO EXISTE EL SERVICIO '" SRVSERVICE_PING_ID "'");
	BOOST_CHECK (false);
#endif

	BOOST_MESSAGE ("+ Se mete el servicio '" SRVSERVICE_PING_ID "'...");
	BasicSrvServicesCatalogo::addService (BasicSrvService_Ping());
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 1);
	BOOST_MESSAGE ("+ Borrar todos los servicios del cat�logo...");
	BOOST_CHECK (BasicSrvServicesCatalogo::delAllServices());
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);

	BOOST_MESSAGE ("+ Se mete el servicio '" SRVSERVICE_PING_ID "'...");
	BasicSrvServicesCatalogo::addService (BasicSrvService_Ping());
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 1);
	BOOST_MESSAGE ("+ Se saca el servicio '" SRVSERVICE_PING_ID "'...");
	BasicSrvServicesCatalogo::delService (BasicSrvService_Ping());
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);

	BOOST_MESSAGE ("+ Se mete otra vez el servicio '" SRVSERVICE_PING_ID "'...");
	BasicSrvServicesCatalogo::addService (BasicSrvService_Ping());
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 1);
	BOOST_MESSAGE ("+ Se saca otra vez el servicio '" SRVSERVICE_PING_ID "'...");
	BasicSrvServicesCatalogo::delService (BasicSrvService_Ping().getServicioName());
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);
}
