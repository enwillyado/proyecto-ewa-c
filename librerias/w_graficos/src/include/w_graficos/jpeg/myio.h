

/*             _      | |        |  _      | |      |  | |
 *            / \     | |          / \     | |  /|  |  | |             \
 *      Q___|____\  __| | | . |__Q____\  __| | (_|__|__| |  Q_|__|__|___)
 *  ___/    :      /      |___|         /               ___/          .
 *               _/                   _/
 *
 */

//  Written by Hamed Ahmadi Nejad
//    ahmadinejad@ce.sharif.edu
//    comphamed@yahoo.com

#ifndef myio_h
#define myio_h

#include <cstddef>

extern int nError;
namespace MyIO
{
	void openfile (const char* s);
	void copyBuffer (const unsigned char* s, const size_t size);
	void closefile();

	int read2bytes();
	int readbyte();

	void resetwritebuffer();
	void writebyte (int x);
	void write2bytes (int x);
	void writestring (const char* s);
	void writetofile (const char* const filename);

	void Error (const char* const s);
}

#endif
