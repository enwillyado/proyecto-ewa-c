#include <boost/test/unit_test.hpp>

#include "w_web/w_web_catalogo.h"			//< para poder tener el cat�logo de servicios del servidor
#include "w_web/w_web_services_prueba.h"	//< para poder tener los servicios de prueba del servidor catalogados

#define RUTAS_CORRECTAS {\
		{BasicSrvService_PruebaA().getServicioName(), "/prueba"},\
		{BasicSrvService_PruebaA().getServicioName(), "/prueba?algo_que_no&va_\\a/_mo//les&24=$%?//$=$tar"},\
		{BasicSrvService_PruebaA().getServicioName(), "/prueba/"},\
		{BasicSrvService_PruebaB().getServicioName(), "/prueba/loquesea"},\
		{BasicSrvService_PruebaB().getServicioName(), "/prueba/loquesea/"},\
		\
		{BasicSrvService_PruebaC().getServicioName(), "/prueba/loquesea/uno"},\
		{BasicSrvService_PruebaC().getServicioName(), "/prueba/loquesea/uno/"},\
		{BasicSrvService_PruebaD().getServicioName(), "/prueba/loquesea/dos"},\
		{BasicSrvService_PruebaD().getServicioName(), "/prueba/loquesea/dos/"},\
		{BasicSrvService_PruebaE().getServicioName(), "/prueba/loquesea/dos/algo"},\
		{BasicSrvService_PruebaE().getServicioName(), "/prueba/loquesea/dos/ALGO/"},\
		\
		{BasicSrvService_PruebaF().getServicioName(), "/prueba/loquesea/tres/algo/rojo"},\
		{BasicSrvService_PruebaF().getServicioName(), "/prueba/loquesea/tres/algo/rojo/"},\
		{BasicSrvService_PruebaG().getServicioName(), "/prueba/loquesea/TRES/algo/azul"},\
		{BasicSrvService_PruebaG().getServicioName(), "/prueba/loquesea/tres/algo/azul/"},\
		\
		{BasicSrvService_PruebaH().getServicioName(), "/prueba/*/cuatro/algo/"},\
		{BasicSrvService_PruebaH().getServicioName(), "/prueba/*/cuatro/algo/"},\
		{"",""}\
	}

#define RUTAS_INCORRECTAS {\
		"",\
		"/",\
		"/ping",		/*< este fallar� porque en estas pruebas se vac�a el cat�logo y solo se meten*/\
		"/pong",		/*		las pruebas "PruebaX"*/\
		"/no_existe",\
		"/prueba/loquesea/uno/mas",\
		"/prueba/loquesea/dos/algo/mas",\
		"/prueba/loquesea/tres",\
		"/prueba/loquesea/tres/",\
		"/prueba/loquesea/tres////",\
		"/prueba/loquesea/tres/algo",\
		"/prueba/loquesea/tres/algo/",\
		"/prueba/loquesea/tres/algo/otracosa/",\
		"/prueba/loquesea/tres/algo/otracosa/mas",\
		"//prueba/loquesea/cuatro/",\
		"/prueba/loquesea/cuatro/",\
		NULL}

BOOST_AUTO_TEST_CASE (Test_CatalogoServiciosExtendidos)
{
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);
	{
		// Se a�ade este servicio (m�todo c++, con referencia: NO SE TIENE QUE BORRAR =D )
		BOOST_MESSAGE ("+ A�adiendo al cat�logo el servicio '" + BasicSrvService_PruebaA().getServicioName() + "'...");
		BasicSrvServicesCatalogo::addService (BasicSrvService_PruebaA());

		BOOST_MESSAGE ("+ Buscando el servicio '" + BasicSrvService_PruebaA().getServicioName() + "' en el catalogo...");
		BOOST_CHECK (BasicSrvServicesCatalogo::getService (BasicSrvService_PruebaA().getServicioName()) != NULL);
		const std::string & serviceName = BasicSrvServicesCatalogo::getService (BasicSrvService_PruebaA().getServicioName())->getServicioName();
		BOOST_CHECK (serviceName == BasicSrvService_PruebaA().getServicioName());
	}

	BasicSrvServicesCatalogo::addService (BasicSrvService_PruebaB());
	BasicSrvServicesCatalogo::addService (BasicSrvService_PruebaC());
	BasicSrvServicesCatalogo::addService (BasicSrvService_PruebaD());
	BasicSrvServicesCatalogo::addService (BasicSrvService_PruebaE());
	BasicSrvServicesCatalogo::addService (BasicSrvService_PruebaF());
	BasicSrvServicesCatalogo::addService (BasicSrvService_PruebaG());
	BasicSrvServicesCatalogo::addService (BasicSrvService_PruebaH());
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 8);

	BOOST_MESSAGE ("+ Haciendo prueba con las rutas validas:");
	const std::string rutasCorrectas[][2] = RUTAS_CORRECTAS;
	for (size_t i = 0; rutasCorrectas[i][0] != ""; i++)
	{
		const std::string & idName = rutasCorrectas[i][0];
		const std::string & prueba = rutasCorrectas[i][1];
		BOOST_MESSAGE ("+ Probando a obtener el servicio '" + prueba + "' y que sea '" + idName + "'...");
		BOOST_CHECK (NULL != BasicSrvServicesCatalogo::getService (prueba));
		BOOST_CHECK (idName == BasicSrvServicesCatalogo::getService (prueba)->getServicioName());
	}

	BOOST_MESSAGE ("+ Haciendo prueba con las rutas invalidas:");
	const char* rutasIncorrectas[] = RUTAS_INCORRECTAS;
	for (size_t i = 0; rutasIncorrectas[i] != NULL; i++)
	{
		const std::string & prueba = rutasIncorrectas[i];
		BOOST_MESSAGE ("+ Probando a NO obtener el servicio '" + prueba + "'...");
		BOOST_CHECK (NULL == BasicSrvServicesCatalogo::getService (prueba));
	}

	BOOST_CHECK (BasicSrvServicesCatalogo::delAllServices());
	BOOST_CHECK (BasicSrvServicesCatalogo::size() == 0);
}
