/*********************************************************************************************
 *	Name		: w_sistema_files.h
 *	Description	: Declaraciones para el trabajo con ficheros
 *	Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_FILES_H_
#define _W_SISTEMA_FILES_H_

#include <string>

// Declaración del espacio de nombres donde agrupar las funciones para el trabajo con ficheros
namespace WFiles
{
	std::string normalicePath (const std::string & path);
	std::string getRealPathOfPath (const std::string & path);
	std::string getDirOfPath (const std::string & s);
	std::string getFileOfPath (const std::string & s);
	std::string getExtensionFile (const std::string & s);

	bool fileExists (const std::string & filename);
	bool fileExists (const char* filename);

	bool createDirPath (const std::string & s);
};

#endif // _W_SISTEMA_FILES_H_
