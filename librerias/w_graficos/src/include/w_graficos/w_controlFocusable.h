/*********************************************************************************************
 * Name			: w_controlFocusable.h
 * Description	: Desencadenadores de rat�n para controles
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_CONTROL_FOCUSABLE_H_
#define _W_CONTROL_FOCUSABLE_H_

#include "w_graficos/w_eventoKey.h"
#include "w_graficos/w_controlBase.h"

#include <vector>
#include <cstring>	//< size_t

class WControlFocusable : public WControlBase
{
public:
	// Constructor
	WControlFocusable();

public:

	// ---------------------------------------------------------------------------
	// onFocusIn
	virtual void onFocusIn();

	typedef WEvento::CbEvent OnFocusIn;
	size_t setOnFocusIn (OnFocusIn cbOnFocusIn);
	bool unsetOnFocusIn (const size_t index);

	// ---------------------------------------------------------------------------
	// onFocusOut
	virtual void onFocusOut();

	typedef WEvento::CbEvent OnFocusOut;
	size_t setOnFocusOut (OnFocusOut cbOnFocusOut);
	bool unsetOnFocusOut (const size_t index);

private:
	// �rea de desencadenadores
	std::vector<OnFocusIn> cbsOnFocusIn;
	std::vector<OnFocusOut> cbsOnFocusOut;
};

#endif // _W_CONTROL_FOCUSABLE_H_
