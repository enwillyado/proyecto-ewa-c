/*********************************************************************************************
 *	Name		: wrapper_aes.c
 *	Description	: Wrapper de C para algoritmo AES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion_c.h"
#include "aes/aes.h"
#include <memory.h>

/**
 * Cifra un buffer binario mediante el algoritmo de AES
 * \param [in] bufferIn		Buffer a encriptar
 * \param [in] szBufferIn	Tama�o del buffer a encriptar
 * \param [out] bufferOut	Buffer de salida encriptado
 * \param [in] szBufferOut	Tama�o del buffer de salida
 * \param [in] key			Contrase�a a emplear para el cifrado
 * \return	EncryptOk		Codificaci�n Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult aesEncryptBuffer (const void* bufferIn, size_t szBufferIn, void* bufferOut, size_t szBufferOut, const AES_KEY key)
{
	aes_ctx context;

	// Verificamos el tama�o de los buffers
	if (szBufferOut < AesCalculateSizeEncryptBuffer (szBufferIn))
	{
		return EncryptError;
	}

	aes_enc_key (key, BLOCK_SIZE, &context);

	// Debemos entregar bloques de 16 bytes
	while (szBufferIn >= BLOCK_SIZE)
	{
		aes_enc_blk ((const unsigned char*)bufferIn, (unsigned char*)bufferOut, &context);
		szBufferIn -= BLOCK_SIZE;
		bufferIn = (const char*)bufferIn + BLOCK_SIZE;
		bufferOut = (char*)bufferOut + BLOCK_SIZE;
	}

	// Ya entregamos todos los bloques enteros. Ahora deberiamos tener cuidado con el ultimo bloque, que puede estar a medias.
	// Si eso hacemos paddin con 0
	if (szBufferIn > 0)
	{
		// Debemos hacer padding con el resto del buffer. Usamos un 0
		unsigned char bufferTmp[BLOCK_SIZE];

		memcpy (bufferTmp, bufferIn, szBufferIn);
		memset (bufferTmp + szBufferIn, 0, BLOCK_SIZE - szBufferIn);
		aes_enc_blk (bufferTmp, (unsigned char*)bufferOut, &context);
	}

	return EncryptOk;
}

/**
 * Descifra un buffer binario mediante el algoritmo de AES
 * \param [in] bufferIn		Buffer a encriptar
 * \param [in] szBufferIn	Tama�o del buffer a encriptar
 * \param [out] bufferOut	Buffer de salida encriptado
 * \param [in] szBufferOut	Tama�o del buffer de salida
 * \param [in] key			Contrase�a a emplear para el cifrado
 * \return	EncryptOk		Codificaci�n Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult aesDesencryptBuffer (const void* bufferIn, size_t szBufferIn, void* bufferOut, size_t szBufferOut, const AES_KEY key)
{
	aes_ctx context;

	// Verificamos el tama�o de los buffers
	if (szBufferOut < AesCalculateSizeEncryptBuffer (szBufferIn))
	{
		return EncryptError;
	}

	aes_dec_key (key, BLOCK_SIZE, &context);

	// Debemos entregar bloques de 16 bytes
	while (szBufferIn >= BLOCK_SIZE)
	{
		aes_dec_blk ((const unsigned char*)bufferIn, (unsigned char*)bufferOut, &context);
		szBufferIn -= BLOCK_SIZE;
		bufferIn = (const char*)bufferIn + BLOCK_SIZE;
		bufferOut = (char*)bufferOut + BLOCK_SIZE;
	}

	// Ya entregamos todos los bloques enteros. Ahora deberiamos tener cuidado con el ultimo bloque, que puede estar a medias.
	// Si eso hacemos paddin con 0
	if (szBufferIn > 0)
	{
		// Debemos hacer padding con el resto del buffer. Usamos un 0
		unsigned char bufferTmp[BLOCK_SIZE];

		memcpy (bufferTmp, bufferIn, szBufferIn);
		memset (bufferTmp + szBufferIn, 0, BLOCK_SIZE - szBufferIn);
		aes_dec_blk (bufferTmp, (unsigned char*)bufferOut, &context);
	}

	return EncryptOk;
}
