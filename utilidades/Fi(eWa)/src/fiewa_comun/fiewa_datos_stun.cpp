/*********************************************************************************************
 *	Name		: fiewa_datos_stun.cpp
 *	Description	: Implementaci�n de la clase que almacena los datos enviados por el Servidor STUN
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "fiewa/fiewa_datos_stun.h"

#include "w_base/toStr.hpp"
#include "w_base/fromStr.hpp"

#include "w_codificacion/w_codificacion.h"		//< para tener ::convertStringToBase64

// ----------------------------------------------------------------------------
// �raa de datos est�tica
FIEWA::DatosSTUN FIEWA::DatosSTUN::npos;

// ----------------------------------------------------------------------------
// Constructores
FIEWA::DatosSTUN::DatosSTUN()
{
}

FIEWA::DatosSTUN::DatosSTUN (const Mensaje & mensajeRecibirDeSTUN)
{
	this->parseFromMsg (mensajeRecibirDeSTUN);
}

FIEWA::DatosSTUN::DatosSTUN (const Evento & evento)
{
	this->publicIP = evento.ip;
	this->publicPort = evento.puerto;
	this->publicID = ::convertStringToBase64 (evento.ip + ::toStr (evento.puerto));
}

FIEWA::DatosSTUN::DatosSTUN (const FIEWA::DatosSTUN & org)
{
	this->operator= (org);
}

// Factor�a
FIEWA::DatosSTUN FIEWA::DatosSTUN::buscandoPeers (const Mensaje & mensajeRecibirDeSTUN)
{
	FIEWA::DatosSTUN datos;
	if (datos.parseFromMsg (mensajeRecibirDeSTUN) == false)
	{
		return FIEWA::DatosSTUN::npos;
	}
	return datos;
}

// Asignador
bool FIEWA::DatosSTUN::parseFromMsg (const Mensaje & mensajeRecibirDeSTUN)
{
	/*
	if (mensajeRecibirDeSTUN == null)
	{
		return false;
	}
	*/
	const std::string & body = mensajeRecibirDeSTUN.getBody();
	const size_t pos = body.find (':');
	if (pos == std::string::npos)
	{
		return false;
	}
	const size_t nextPos = body.find (':', pos + 1);
	if (pos == std::string::npos)
	{
		return false;
	}
	this->publicIP = body.substr (0, pos);
	this->publicPort = ::fromStr<int> (body.substr (pos + 1, nextPos - pos - 1));
	this->publicID = body.substr (nextPos + 1);
	return true;
}

// M�todos auxiliares est�ticos
int FIEWA::DatosSTUN::buscandoNumPeers (const Mensaje & mensajeRecibirDeSTUNbuscandoPeers)
{
	return ::fromStr<int> (mensajeRecibirDeSTUNbuscandoPeers.getBody());
}

// Operadores
bool FIEWA::DatosSTUN::operator== (const FIEWA::DatosSTUN & org) const
{
	return this->publicIP == org.publicIP && this->publicPort == org.publicPort &&
		   this->publicID == org.publicID;
}
bool FIEWA::DatosSTUN::operator!= (const FIEWA::DatosSTUN & org) const
{
	return this->operator== (org) == false;
}

FIEWA::DatosSTUN & FIEWA::DatosSTUN::operator= (const FIEWA::DatosSTUN & org)
{
	this->publicID = org.publicID;
	this->publicIP = org.publicIP;
	this->publicPort = org.publicPort;
	return *this;
}
// Conversores
std::string FIEWA::DatosSTUN::toString() const
{
	return this->publicIP + ":" + ::toStr (this->publicPort) + ":" + this->publicID;
}
