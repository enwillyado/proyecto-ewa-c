/*********************************************************************************************
 * Name			: w_lienzoBase.cpp
 * Description	: Implementación de la clase base para manipular una representación gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_lienzoBase.h"

#include "w_graficos/w_control.h"
#include "w_graficos/w_lienzo.h"
#include "w_graficos/w_rgba.h"

void WLienzoBase::operator= (const WLienzoBase & lienzo)
{
	for (size_t y = 0; y < lienzo.getAlto(); y++)
	{
		for (size_t x = 0; x < lienzo.getAncho(); x++)
		{
			this->setPixel (x, y, lienzo.getPixel (x, y));
		}
	}
}
void WLienzoBase::operator+= (const WLienzoBase & lienzo)
{
	this->unir (lienzo);
}
void WLienzoBase::unir (const WLienzoBase & lienzo, const size_t xPos, const size_t yPos)
{
	for (size_t y = 0; y < lienzo.getAlto(); y++)
	{
		for (size_t x = 0; x < lienzo.getAncho(); x++)
		{
			this->setPixel (x + xPos, y + yPos, lienzo.getPixel (x, y));
		}
	}
}
void WLienzoBase::unir (const IWControl & control)
{
	this->unir (control.getLienzo(), control.getPosX(), control.getPosY());
}
WLienzo WLienzoBase::sub (const size_t xPos, const size_t yPos, const size_t ancho, const size_t alto) const
{
	WLienzo ret (ancho, alto);
	for (size_t y = yPos, yi = 0; y < this->getAlto() && yi < alto; y++, yi++)
	{
		for (size_t x = xPos, xi = 0; x < this->getAncho() && xi < ancho; x++, xi++)
		{
			ret.setPixel (xi, yi, this->getPixel (x, y));
		}
	}
	return ret;
}

WLienzo WLienzoBase::resize (const size_t ancho, const size_t alto) const
{
	WLienzo ret (ancho, alto);
	if (this->getAncho() == 0 || this->getAlto() == 0)
	{
		return ret;
	}
#ifdef TODOS_PUNTOS_ORIGEN
	const float porcionX = ((float)ancho) / this->getAncho();
	const float porcionY = ((float)alto) / this->getAlto();
	float xi, yi = 0;
	for (size_t y = 0; y < this->getAlto() && yi < alto; y++, yi += porcionY)
	{
		xi = 0;
		for (size_t x = 0; x < this->getAncho() && xi < ancho; x++, xi += porcionX)
		{
			ret.setPixel ((const size_t)xi, (const size_t)yi, this->getPixel (x, y));
		}
	}
#else
	const float porcionX = ((float)this->getAncho()) / ancho;
	const float porcionY = ((float)this->getAlto()) / alto;
	float xi, yi = 0;
	for (size_t y = 0; yi < this->getAlto() && y < alto; yi += porcionY, y++)
	{
		xi = 0;
		for (size_t x = 0; xi < this->getAncho() && x < ancho; xi += porcionX, x++)
		{
			ret.setPixel (x, y, this->getPixel ((const size_t)xi, (const size_t)yi));
		}
	}
#endif
	return ret;
}
// ----------------------------------------------------------------------------
// Transformaciones
void WLienzoBase::forEachPixel (WLienzo::ForEachPixel action)
{
	for (size_t y = 0; y < this->getAlto(); y++)
	{
		for (size_t x = 0; x < this->getAncho(); x++)
		{
			action (*this, x, y);
		}
	}
}

void WLienzoBase::forEachPixel (WLienzo::ForEachPixelColor actionColor)
{
	for (size_t y = 0; y < this->getAlto(); y++)
	{
		for (size_t x = 0; x < this->getAncho(); x++)
		{
			RGBA color = this->getPixelFast (x, y);
			if (true == actionColor (*this, color, x, y))
			{
				this->setPixelFast (x, y, color);
			}
		}
	}
}
void WLienzoBase::forEachPixel (WLienzo::ForEachPixelOnlyColor actionColor)
{
	for (size_t y = 0; y < this->getAlto(); y++)
	{
		for (size_t x = 0; x < this->getAncho(); x++)
		{
			RGBA color = this->getPixelFast (x, y);
			(color.*actionColor)();
			this->setPixelFast (x, y, color);
		}
	}
}

void WLienzoBase::setBorder (const RGBA & rgba)
{
	for (size_t y = 0; y < this->getAlto(); y++)
	{
		this->setPixel (0, y, rgba);
		this->setPixel (this->getAncho() - 1, y, rgba);
	}
	for (size_t x = 0; x < this->getAncho(); x++)
	{
		this->setPixel (x, 0, rgba);
		this->setPixel (x, this->getAlto() - 1, rgba);
	}
}

void WLienzoBase::invertColor()
{
	this->forEachPixel (&RGBA::invertColor);
}

void WLienzoBase::brightness (const short u)
{
	for (size_t y = 0; y < this->getAlto(); y++)
	{
		for (size_t x = 0; x < this->getAncho(); x++)
		{
			RGBA color = this->getPixelFast (x, y);
			color.brightness (u);
			this->setPixelFast (x, y, color);
		}
	}
}

bool forEachPixelColorRGBtoGRAY (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
{
	const size_t gris = (color.r + color.g + color.b) / 3;
	color.r = gris;
	color.g = gris;
	color.b = gris;
	return true;
}
void WLienzoBase::fromRGBtoGRAY()
{
	this->forEachPixel (&forEachPixelColorRGBtoGRAY);
}

bool forEachPixelColorNegative (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
{
	color.r = (0xFF - color.r);
	color.g = (0xFF - color.g);
	color.b = (0xFF - color.b);
	return true;
}
void WLienzoBase::negative()
{
	this->forEachPixel (&forEachPixelColorNegative);
}

bool forEachPixelColorRed (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
{
	color.g = color.b = 0;
	return true;
}
void WLienzoBase::redChannel()
{
	this->forEachPixel (&forEachPixelColorNegative);
}

bool forEachPixelColorGreen (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
{
	color.r = color.b = 0;
	return true;
}
void WLienzoBase::greenChannel()
{
	this->forEachPixel (&forEachPixelColorGreen);
}

bool forEachPixelColorBlue (WLienzoBase & lienzo, RGBA & color, const size_t x, const size_t y)
{
	color.g = color.r = 0;
	return true;
}
void WLienzoBase::blueChannel()
{
	this->forEachPixel (&forEachPixelColorBlue);
}
