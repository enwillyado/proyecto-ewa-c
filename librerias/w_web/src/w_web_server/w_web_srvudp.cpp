/*********************************************************************************************
 *	Name		: _w_web_srvudp.cpp
 *	Description	: Implementaci�n del Servidor Web UDP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_web/w_web_srvudp.h"
#include "w_web/w_web_services.h"
#include "w_web/w_web_catalogo.h"

// --------------------------------------------------------------------------
// Implementaci�n del Servidor TCP

ServidorWebUDP::ServidorWebUDP()
	: ServidorUDP()
{
}

// --------------------------------------------------------------------------
// L�gica del Servidor:
/**
 *	Env�a un mensaje de respuesta al evento dado
 *	\param	evento	[in]	evento que se va a procesar
 *	\param	mensaje	[in]	mensaje que se va a (intentar) enviar
 *	\retval	TRUE si se envi� el mensaje correctamente
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorWebUDP::send (const EventoServidorUDP & evento, const Mensaje & mensaje)
{
	return this->ServidorUDP::send (evento, mensaje.genMsg());
}

/**
 *	Analiza un nuevo mensaje recibido para procesarlo si se conoce el protocolo que tiene
 *	\param	evento	[in]	evento (de MENSAJE_RECIBIDO) que se va a analizar
 *	\retval	TRUE si el evento fue analizado; FALSE si no
**/
bool ServidorWebUDP::analizaEventoMsgRecv (const EventoServidorUDP & evento)
{
	// Si no, procesar el mensaje
	if (evento.tipo == EventoServidorTCP::MENSAJE_RECIBIDO)	//< para cerciorarse
	{
		const Mensaje mensajeRecibido (evento.mensaje);

		// Una vez completo, revisar el tipo del mensaje y si no es <UNKNOWN> procesarlo
		const Mensaje::TiposMensaje tipo = mensajeRecibido.getTipoMensaje();
		if (tipo != Mensaje::UNKNOWN)
		{
			// se ha recibido conocido un Mensaje Web
			// por lo que se va a procesar
			this->procesaMsgWebRecv (evento, mensajeRecibido);
		}
		else
		{
			// si no es un protocolo conocido, procesarlo a parte
			this->procesaMsgNoWebRecv (evento);
		}
		return true;
	}
	return false;
}

/**
 *	Procesa un evento nuevo. Es un evento no web (mensaje sin formato conocido)
 *	\param	evento		[in]	evento que se va a procesar
**/
void ServidorWebUDP::procesaMsgNoWebRecv (const EventoServidorUDP & evento)
{
	// si no es un protocolo conocido, se ignorar�
	// TODO: �dar alg�n mensaje de despedida?
}

/**
 *	Procesa un evento nuevo. Se supone que ser� del Protocolo DHTTP (y "tipoMensaje" ser�a DPROT/GET/POST..)
 *	\param	evento			[in]	evento que se va a procesar
 *	\param	mensajeRecibido	[in]	peticion que se va a procesar
**/
void ServidorWebUDP::procesaMsgWebRecv (const EventoServidorUDP & evento, const Mensaje & mensajeRecibido)
{
	// Obtener la respuesta
	const Mensaje & respuesta = this->responderMsgWebRecv (evento, mensajeRecibido);

	// Procesar la respuesta
	this->procesaMsgWebEnv (evento, mensajeRecibido, respuesta);
}

/**
 *	Procesa un evento nuevo. Se supone que ser� del Protocolo DHTTP (y "tipoMensaje" ser�a DPROT/GET/POST..)
 *	\param	evento			[in]	evento que se va a procesar
 *	\param	mensajeRecibido	[in]	peticion que se va a procesar
 *	\return	respuesta al evento recibido
**/
Mensaje ServidorWebUDP::responderMsgWebRecv (const EventoServidorUDP & evento, const Mensaje & mensajeRecibido)
{
	Mensaje respuesta;	//< objeto que almacenar� la respuesta

	// Buscar el servicio
	const std::string & servicioSolicitado = mensajeRecibido.getURI();
	const BasicSrvService* servicio = BasicSrvServicesCatalogo::getService (servicioSolicitado);
	if (servicio == NULL)
	{
		// si no hay el servicio solicitado...
		const BasicSrvService* servicio = BasicSrvServicesCatalogo::getService (SRVSERVICE_SENDINTERN_ID);
		if (servicio == NULL)
		{
			// Si no hay nada, se da una respuesta hardcodeada
			const std::string & mensajeRespondido = "<hr/><pre><code>" + evento.mensaje + "</code></pre><hr/>";
			// TODO: �dar alg�n mensaje de despedida 'personalizado' alegando la falta del servicio solicitado?
			respuesta.setRespuestaWebPorDefecto (mensajeRespondido, Mensaje::NOT_FOUND);
			respuesta.setTipoMensaje (Mensaje::RESPONSE_AND_EXIT);	// por si se desea terminar en otro sitio
		}
		else
		{
			// si lo hay, solicitarle una respuesta
			respuesta = servicio->ejecutarServicio (evento, mensajeRecibido);
		}
	}
	else
	{
		// si lo hay, solicitarle una respuesta
		respuesta = servicio->ejecutarServicio (evento, mensajeRecibido);
	}
	return respuesta;
}

/**
 *	Procesa la respuesta dada sobre el evento indicado.
 *	\param	evento			[in]	evento que se va a procesar
 *	\param	mensajeRecibido	[in]	peticion que se va a procesar
 *	\param	respuesta		[in]	respuesta que se va a procesar
**/
void ServidorWebUDP::procesaMsgWebEnv (const EventoServidorUDP & evento, const Mensaje & mensajeRecibido,
									   const Mensaje & respuesta)
{
	// Procesar la respuesta dada salvo que sea <RESPONSE_THREADED>
	if (respuesta.getTipoMensaje() != Mensaje::RESPONSE_THREADED)
	{
		// Se env�a la respuesta generada
		this->send (evento, respuesta);
	}
}
