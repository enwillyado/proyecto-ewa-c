/*********************************************************************************************
 * Name			: w_web_services_thread_prueba.h
 * Description	: Implementación de los ServiciosThread de prueba
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRV_SERVICES_THREAD_PRUEBA_H_
#define _W_RED_SRV_SERVICES_THREAD_PRUEBA_H_

#include "w_web/w_web_services_thread.h"			//< para tener el "ServicioConHilo"
#include "w_web/w_web_services_thread_prueba_id.h"	//< para tener los ID de los ServiciosThread de prueba

// ---------------------------------------------------------------------------
// Servicio de Prueba
class ServicioPruebaConHilos : public ServicioConHilo
{
public:
	ServicioPruebaConHilos (ServidorTCP* server);
	virtual BasicSrvService* newInstance() const;
	virtual std::string getServicioName() const;

public:
	// Función para ejecutarse por un hilo
	virtual Mensaje ejecutarServicioFinal (const Evento & evento, const Mensaje & peticion) const;
};

// ---------------------------------------------------------------------------
// Servicio de Prueba
class ServicioPruebaInteractivoConHilos : public ServicioConHilo
{
public:
	ServicioPruebaInteractivoConHilos (ServidorTCP* server);
	virtual BasicSrvService* newInstance() const;
	virtual std::string getServicioName() const;

public:
	// Servicio para ejecutarse desde el catálogo
	virtual Mensaje ejecutarServicio (const Evento & evento, const Mensaje & peticion) const;

	// Función para ejecutarse por un hilo o en el programa principal (depende del parámetro "modo")
	virtual Mensaje ejecutarServicioFinal (const Evento & evento, const Mensaje & peticion) const;
};

#endif	//_W_RED_SRV_SERVICES_THREAD_PRUEBA_H_
