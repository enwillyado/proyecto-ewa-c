/*********************************************************************************************
 *	Name		: _sim_nucleo.pragmalib.h
 *	Description	: Header file with unified libraries to link
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _SIM_NUCLEO_PRAGMALIB_H_
#define _SIM_NUCLEO_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#ifndef END_LIB_EWA
#ifdef _DEBUG
#define END_LIB_EWA "d.lib"
#else
#define END_LIB_EWA ".lib"
#endif
#endif

#pragma message("Enlazando con las librer�as del proyecto 'sim_nucleo'...")

#pragma comment(lib, "sim_nucleo" END_LIB_EWA)

#endif

#endif // _SIM_NUCLEO_PRAGMALIB_H_
