/*********************************************************************************************
 *	Name		: wrapper_des_c.c
 *	Description	: Wrapper de C para el algoritmo DES
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion_c.h"
#include "des.h"

/**
 * Encripta un buffer mediante el método DES
 * \param [in] pBufferIn		Buffer a encriptar
 * \param [in] szBufferIn		Tamaño del buffer a encriptar
 * \param [out] pBufferOut		Buffer donde dejaremos el resultado
 * \param [in,out] szBufferOut	Tamaño del buffer donde se dejara el resultado
 * \param [in] key1				Contraseña 1 del algoritmo
 * \param [in] key2				Contraseña 2 del algoritmo
 * \param [in] nPadding
 * \param [in] mode				DesMode1Des - Algoritmo Des simple
 *								DesMode3Des - Algoritmo 3 Des
 * \param [in] config			DesConfigChainBlockCipher - Se emplea Chain Block Cipher (CBC)
 *								DesConfigElectronicCodeBook - Se emplea Electronic Code Book (ECB)
 * \return	EncryptOk		Encryptacion ha sido correcta
 *			EncryptError	El buffer entregado para la salida es demasiado pequeño
 **/
EEncryptResult desEncryptBuffer (const void* pBufferIn, size_t szBufferIn, void* pBufferOut,
								 size_t* szBufferOut, const Des3Key key1, const Des3Key key2,
								 int nPadding, EDesMode mode, EDesConfig config)
{
	if (mcbDoDES2 ((const unsigned char*)pBufferIn, szBufferIn, key1, key2, (unsigned char*)pBufferOut, szBufferOut,
				   DES_ENCRYPT, nPadding, mode, config) == 1)
	{
		return EncryptOk;
	}
	else
	{
		return EncryptError;
	}
}

/**
 * Desencripta un buffer mediante el método DES
 * \param [in] pBufferIn		Buffer a encriptar
 * \param [in] szBufferIn		Tamaño del buffer a encriptar
 * \param [out] pBufferOut		Buffer donde dejaremos el resultado
 * \param [in,out] szBufferOut	Tamaño del buffer donde se dejara el resultado
 * \param [in] key1				Contraseña 1 del algoritmo
 * \param [in] key2 			Contraseña 2 del algoritmo
 * \param [in] nPadding
 * \param [in] mode				DesMode1Des - Algoritmo Des simple
 *								DesMode3Des - Algoritmo 3 Des
 * \param [in] config			DesConfigChainBlockCipher - Se emplea Chain Block Cipher (CBC)
 *								DesConfigElectronicCodeBook - Se emplea Electronic Code Book (ECB)
 * \return	EncryptOk		Encryptacion ha sido correcta
 *			EncryptError	El buffer entregado para la salida es demasiado pequeño
 **/
EEncryptResult desDesencryptBuffer (const void* pBufferIn, size_t szBufferIn, void* pBufferOut,
									size_t* szBufferOut, const Des3Key key1, const Des3Key key2,
									int nPadding, EDesMode mode, EDesConfig config)
{
	if (mcbDoDES2 ((const unsigned char*)pBufferIn, szBufferIn, key1, key2, (unsigned char*)pBufferOut, szBufferOut,
				   DES_DECRYPT, nPadding, mode, config) == 1)
	{
		return EncryptOk;
	}
	else
	{
		return EncryptError;
	}
}
