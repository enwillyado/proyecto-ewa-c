/*********************************************************************************************
* Name			: w_red_tester_time.cpp
* Description	: Pruebas unitarias que se realizar�n sobre time
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema.h"

#include <iostream>

// ----------------------------------------------------------------------------
// Execute Timers
int executeTimers()
{
	class Periodico
	{
	public:
		static void algo (WTimer & timer)
		{
			static int i = 0;
			std::cout << "[HILO] Temporizador... " << i++ << std::endl;
			std::cout.flush();
		}
	};

	// Ejemplos
	{
		// Crear timer y que se ejecute durante 1 segundos y finalizarlo (SIN esperar a que termine)
		WTimer timer (&Periodico::algo, 6000);
		std::cout << "Lanzar timer..." << std::endl;
		timer.start();
		WTime::esperarMilisegundos (1 * 1000);	// esperar 1 segundo con el timer en marcha
		timer.end();
	}

	{
		// Crear timer y que se ejecute durante 1 segundos y finalizarlo (SIN esperar a que termine)
		WTimer timer (&Periodico::algo, 100);
		std::cout << "Lanzar timer..." << std::endl;
		timer.start();
		WTime::esperarMilisegundos (1 * 1000);	// esperar 1 segundo con el timer en marcha
		timer.stop();
		std::cout << "... otro poco..." << std::endl;
		timer.start();
		WTime::esperarMilisegundos (1 * 1000);	// esperar 1 segundo con el timer en marcha
		std::cout << "...fin timer" << std::endl;
		timer.end();
	}

	{
		// Crear timer y que se ejecute durante 3 segundos y destruirlo (esperarando a que termine)
		WTimer timer (&Periodico::algo, 300);
		std::cout << "Lanzar timer..." << std::endl;
		timer.start();
		WTime::esperarMilisegundos (3 * 1000);	// esperar 3 segundos con el timer en marcha
	}

	{
		// Crear timer, que se ejecute durante 3 segundos y luego esperar activamente a que termine
		WTimer timer (&Periodico::algo, 300);
		std::cout << "Lanzar timer..." << std::endl;
		{
			timer.start();
			WTime::esperarMilisegundos (3 * 1000);	// esperar 3 segundos con el timer en marcha
			timer.stop();
		}
		while (true)	//< espera activa
		{
			// Ver que est� en ejecuci�n
			const bool result = timer.isStarted();
			if (result == false)
			{
				std::cout << "Timer finalizo!" << std::endl;
				break; //salir de la espera activa
			}
			else
			{
				std::cout << "Timer sigue en ejecucion; esperando..." << std::endl;
				WTime::esperarMilisegundos (timer.getMilisengundos() + 100); //< esperar un poco
			}
		}
	}
	return 0;
}
