/*********************************************************************************************
 *	Name		: w_sistema_time.h
 *	Description	: Declaraciones para controlar el tiempo
 *	Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_TIME_H_
#define _W_SISTEMA_TIME_H_

#include <cstring> //< size_t

class WTime
{
public:
	static void esperarMilisegundos (size_t milisegundos);
};

#endif // _W_SISTEMA_TIME_H_
