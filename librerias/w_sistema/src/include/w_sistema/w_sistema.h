/*********************************************************************************************
 *	Name		: w_sistema.h
 *	Description	: Declaraciones de las funciones dependientes del Sistema Operativo
 *	Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_H_
#define _W_SISTEMA_H_

#include "w_sistema/w_sistema_execs.h"

#include "w_sistema/w_sistema_thread.h"

#include "w_sistema/w_sistema_files.h"

#include "w_sistema/w_sistema_time.h"
#include "w_sistema/w_sistema_timer.h"

#endif // _W_SISTEMA_H_
