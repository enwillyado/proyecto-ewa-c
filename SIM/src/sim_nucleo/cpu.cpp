#include "sim/cpu.h"
#include <cstring>

CPU::CPU()
{
	this->laMmu = NULL;
	this->laAbu = new ABU();
}
CPU::~CPU()
{
	if (this->laAbu != NULL)
	{
		delete this->laAbu;
	}
}

void CPU::instalaMMU (MMU* m)
{
	this->laMmu = m;
}
MMU* CPU::getMMU()
{
	return this->laMmu;
}

// acceso a los registros
const bool CPU::dameK()
{
	return this->K;
}
const bool CPU::dameZ()
{
	return this->Z;
}
const bool CPU::dameU()
{
	return this->U;
}
const bool CPU::dameV()
{
	return this->V;
}
const bool CPU::dameW()
{
	return this->W;
}
const bool CPU::dameG()
{
	return this->G;
}
const bool CPU::dameH()
{
	return this->H;
}
const bool CPU::dameI()
{
	return this->I;
}
const bool CPU::dameJ()
{
	return this->J;
}
const bool CPU::dame0()
{
	if (existeMMU())
	{
		return this->laMmu->dame0();
	}
	else
	{
		return 0;
	}
}
const bool CPU::dame1()
{
	if (existeMMU())
	{
		return this->laMmu->dame1();
	}
	else
	{
		return 0;
	}
}
const bool CPU::dame2()
{
	if (existeMMU())
	{
		return this->laMmu->dame2();
	}
	else
	{
		return 0;
	}
}
const bool CPU::dame3()
{
	if (existeMMU())
	{
		return this->laMmu->dame3();
	}
	else
	{
		return 0;
	}
}
const bool CPU::dame4()
{
	if (existeMMU())
	{
		return this->laMmu->dame4();
	}
	else
	{
		return 0;
	}
}
const bool CPU::dame5()
{
	if (existeMMU())
	{
		return this->laMmu->dame5();
	}
	else
	{
		return 0;
	}
}
const bool CPU::dame6()
{
	if (existeMMU())
	{
		return this->laMmu->dame6();
	}
	else
	{
		return 0;
	}
}
const bool CPU::dame7()
{
	if (existeMMU())
	{
		return this->laMmu->dame7();
	}
	else
	{
		return 0;
	}
}
const bool CPU::dame8()
{
	return this->r8;
}
const bool CPU::dame9()
{
	return this->r9;
}
const bool CPU::dameA()
{
	return this->rA;
}
const bool CPU::dameB()
{
	return this->rB;
}
const bool CPU::dameC()
{
	return this->rC;
}
const bool CPU::dameD()
{
	return this->rD;
}
const bool CPU::dameE()
{
	return this->rE;
}
const bool CPU::dameF()
{
	return this->rF;
}

// escritura en los registros
void CPU::tomaK (const bool que)
{
	this->K = que;
}
void CPU::tomaZ (const bool que)
{
	this->Z = que;
}
void CPU::tomaU (const bool que)
{
	this->U = que;
}
void CPU::tomaV (const bool que)
{
	this->V = que;
}
void CPU::tomaW (const bool que)
{
	this->W = que;
}
void CPU::tomaG (const bool que)
{
	this->G = que;
}
void CPU::tomaH (const bool que)
{
	this->H = que;
}
void CPU::tomaI (const bool que)
{
	this->I = que;
}
void CPU::tomaJ (const bool que)
{
	this->J = que;
}
void CPU::toma0 (const bool que)
{
	if (existeMMU())
	{
		this->laMmu->toma0 (que);
	}
}
void CPU::toma1 (const bool que)
{
	if (existeMMU())
	{
		this->laMmu->toma1 (que);
	}
}
void CPU::toma2 (const bool que)
{
	if (existeMMU())
	{
		this->laMmu->toma2 (que);
	}
}
void CPU::toma3 (const bool que)
{
	if (existeMMU())
	{
		this->laMmu->toma3 (que);
	}
}
void CPU::toma4 (const bool que)
{
	if (existeMMU())
	{
		this->laMmu->toma4 (que);
	}
}
void CPU::toma5 (const bool que)
{
	if (existeMMU())
	{
		this->laMmu->toma5 (que);
	}
}
void CPU::toma6 (const bool que)
{
	if (existeMMU())
	{
		this->laMmu->toma6 (que);
	}
}
void CPU::toma7 (const bool que)
{
	if (existeMMU())
	{
		this->laMmu->toma7 (que);
	}
}
void CPU::toma8 (const bool que)
{
	this->r8 = que;
}
void CPU::toma9 (const bool que)
{
	this->r9 = que;
}
void CPU::tomaA (const bool que)
{
	this->rA = que;
}
void CPU::tomaB (const bool que)
{
	this->rB = que;
}
void CPU::tomaC (const bool que)
{
	this->rC = que;
}
void CPU::tomaD (const bool que)
{
	this->rD = que;
}
void CPU::tomaE (const bool que)
{
	this->rE = que;
}
void CPU::tomaF (const bool que)
{
	this->rF = que;
}

const bool CPU::dameSemiInstruccion()
{
	if (this->existeMMU() == true)
	{
		return this->laMmu->dameSemiInstruccion();
	}
	else
	{
		return 0;
	}
}

bool CPU::existeMMU()
{
	return this->laMmu != 0;
}
bool CPU::existeABU()
{
	return this->laAbu != 0;
}

void CPU::dameMMU()
{
	if (this->existeMMU())
	{
		this->tomaK (this->laMmu->dame());
	}
}
void CPU::tomaMMU()
{
	if (this->existeMMU())
	{
		this->laMmu->toma (this->dameK());
	}
}
void CPU::ejecutaMMU()
{
	if (this->existeMMU())
	{
		this->laMmu->funcionDeEjecucion (this->dameZ(), this->dameU(),
										 this->dameV(), this->dameW());
	}
}
void CPU::promociona()
{
	this->tomaZ (this->dameA());
	this->tomaU (this->dameB());
	this->tomaV (this->dameC());
	this->tomaW (this->dameD());
}
void CPU::ejecutaABU()
{
	if (this->existeABU())
	{
		const bool value = this->laAbu->accesoRegistros (
							   this->dameK(), this->dameZ(), this->dameU(), this->dameV(), this->dameW()
						   );
		this->tomaK (value);
	}
}

void CPU::leeRegistro()
{
	if (this->dameZ() == 0)
	{
		if (this->dameU() == 0)
		{
			if (this->dameV() == 0)
			{
				if (this->dameW() == 0)
				{
					this->tomaK (this->dame0());
				}
				else
				{
					this->tomaK (this->dame1());
				}
			}
			else
			{
				if (this->dameW() == 0)
				{
					this->tomaK (this->dame2());
				}
				else
				{
					this->tomaK (this->dame3());
				}
			}
		}
		else
		{
			if (this->dameV() == 0)
			{
				if (this->dameW() == 0)
				{
					this->tomaK (this->dame4());
				}
				else
				{
					this->tomaK (this->dame5());
				}
			}
			else
			{
				if (this->dameW() == 0)
				{
					this->tomaK (this->dame6());
				}
				else
				{
					this->tomaK (this->dame7());
				}
			}
		}
	}
	else
	{
		if (this->dameU() == 0)
		{
			if (this->dameV() == 0)
			{
				if (this->dameW() == 0)
				{
					this->tomaK (this->dame8());
				}
				else
				{
					this->tomaK (this->dame9());
				}
			}
			else
			{
				if (this->dameW() == 0)
				{
					this->tomaK (this->dameA());
				}
				else
				{
					this->tomaK (this->dameB());
				}
			}
		}
		else
		{
			if (this->dameV() == 0)
			{
				if (this->dameW() == 0)
				{
					this->tomaK (this->dameC());
				}
				else
				{
					this->tomaK (this->dameD());
				}
			}
			else
			{
				if (this->dameW() == 0)
				{
					this->tomaK (this->dameE());
				}
				else
				{
					this->tomaK (this->dameF());
				}
			}
		}
	}
}
void CPU::escRegistro()
{
	if (this->dameZ() == 0)
	{
		if (this->dameU() == 0)
		{
			if (this->dameV() == 0)
			{
				if (this->dameW() == 0)
				{
					this->toma0 (this->dameK());
				}
				else
				{
					this->toma1 (this->dameK());
				}
			}
			else
			{
				if (this->dameW() == 0)
				{
					this->toma2 (this->dameK());
				}
				else
				{
					this->toma3 (this->dameK());
				}
			}
		}
		else
		{
			if (this->dameV() == 0)
			{
				if (this->dameW() == 0)
				{
					this->toma4 (this->dameK());
				}
				else
				{
					this->toma5 (this->dameK());
				}
			}
			else
			{
				if (this->dameW() == 0)
				{
					this->toma6 (this->dameK());
				}
				else
				{
					this->toma7 (this->dameK());
				}
			}
		}
	}
	else
	{
		if (this->dameU() == 0)
		{
			if (this->dameV() == 0)
			{
				if (this->dameW() == 0)
				{
					this->toma8 (this->dameK());
				}
				else
				{
					this->toma9 (this->dameK());
				}
			}
			else
			{
				if (this->dameW() == 0)
				{
					this->tomaA (this->dameK());
				}
				else
				{
					this->tomaB (this->dameK());
				}
			}
		}
		else
		{
			if (this->dameV() == 0)
			{
				if (this->dameW() == 0)
				{
					this->tomaC (this->dameK());
				}
				else
				{
					this->tomaD (this->dameK());
				}
			}
			else
			{
				if (this->dameW() == 0)
				{
					this->tomaE (this->dameK());
				}
				else
				{
					this->tomaF (this->dameK());
				}
			}
		}
	}
}

void CPU::ciclo()
{
	this->leeInstruccion();
	this->ejecutaInstruccion();
}
void CPU::leeInstruccion()
{
	this->tomaG (this->dameSemiInstruccion());
	this->tomaH (this->dameSemiInstruccion());
	this->tomaI (this->dameSemiInstruccion());
	this->tomaJ (this->dameSemiInstruccion());
}
void CPU::ejecutaInstruccion()
{
	if (this->dameG() == 0)
	{
		if (this->dameI() == 0)
		{
			if (this->dameJ() == 0)
			{
				this->tomaW (this->dameH());  // 0|1 => W = 0|1;
			}
			else
			{
				this->tomaV (this->dameH());  // 2|3 => V = 0|1;
			}
		}
		else
		{
			if (this->dameJ() == 0)
			{
				this->tomaU (this->dameH());  // 4|5 => U = 0|1;
			}
			else
			{
				this->tomaZ (this->dameH());  // 6|7 => Z = 0|1;
			}
		}
	}
	else
	{
		if (this->dameH() == 0)
		{
			if (this->dameI() == 0)
			{
				if (this->dameJ() == 0)
				{
					// 8 => if( K == 0 ){PC += 4;}
					if (this->dameK() == 0)
					{
						this->dameSemiInstruccion();
						this->dameSemiInstruccion();
						this->dameSemiInstruccion();
						this->dameSemiInstruccion();
					}
				}
				else
				{
					this->promociona();		// 9 => cpu_promociona();
				}
			}
			else
			{
				if (this->dameJ() == 0)
				{
					this->ejecutaMMU();		// A => mmu_ejecuta( Z, U, V, W);
				}
				else
				{
					this->ejecutaABU();		// B => K = abu_dameytoma(K, U, V, W);
				}
			}
		}
		else
		{
			if (this->dameI() == 0)
			{
				if (this->dameJ() == 0)
				{
					this->leeRegistro();	// C => k = cpu_leeRegistro( K, U, V, W );
				}
				else
				{
					this->escRegistro();	// D => cpu_escribeRegistro( K, U, V, W );
				}
			}
			else
			{
				if (this->dameJ() == 0)
				{
					this->dameMMU();		// E => K = mmu_dame();
				}
				else
				{
					this->tomaMMU();		// F => mmu_toma( K );
				}
			}
		}
	}
}
