/*********************************************************************************************
 * Name			: w_jumping_sumo_controlador.cpp
 * Description	: Implementación de la libería de controlador del AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "ewadrones/w_jumping_sumo_controlador.h"

#include "w_graficos/w_lienzo_jpeg.h"

#define ACELERACION			10
#define MAX_ACELERACION		127
#define VOLANTE				10
#define MAX_VOLANTE			64

// ----------------------------------------------------------------------------
void JumpingSumoControlador::estabilizar()
{
	//this->setAcelerador(0);
	//this->setVolante (0);
}

void JumpingSumoControlador::setAcelerador (const short nuevoValor)
{
	if (nuevoValor > MAX_ACELERACION)
	{
		this->acelerador = MAX_ACELERACION;
	}
	else if (nuevoValor < -MAX_ACELERACION)
	{
		this->acelerador = -MAX_ACELERACION;
	}
	else
	{
		this->acelerador = nuevoValor;
	}
}
void JumpingSumoControlador::setVolante (const short nuevoValor)
{
	if (nuevoValor > MAX_VOLANTE)
	{
		this->volante = MAX_VOLANTE;
	}
	else if (nuevoValor < -MAX_VOLANTE)
	{
		this->volante = -MAX_VOLANTE;
	}
	else
	{
		this->volante = nuevoValor;
	}
}
short JumpingSumoControlador::getAcelerador() const
{
	return this->acelerador;
}
short JumpingSumoControlador::getVolante() const
{
	return this->volante;
}

// ----------------------------------------------------------------------------
void JumpingSumoControlador::defrente()
{
	const short nuevoValor = this->acelerador + ACELERACION;
	this->setAcelerador (nuevoValor);
}

void JumpingSumoControlador::atras()
{
	const short nuevoValor = this->acelerador - ACELERACION;
	this->setAcelerador (nuevoValor);
}

// ----------------------------------------------------------------------------
void JumpingSumoControlador::izquierda()
{
	const short nuevoValor = this->volante - VOLANTE;
	this->setVolante (nuevoValor);
}

void JumpingSumoControlador::derecha()
{
	const short nuevoValor = this->volante + VOLANTE;
	this->setVolante (nuevoValor);
}

// ---------------------------------------------------------------------------
// Manejadores
void JumpingSumoControlador::setOnNewLienzo (JumpingSumoControlador::OnNewLienzo cbOnNewLienzo)
{
	this->cbOnNewLienzo = cbOnNewLienzo;
}

void JumpingSumoControlador::onNewLienzo()
{
	if (this->cbOnNewLienzo != NULL)
	{
		this->cbOnNewLienzo (this->lienzo);
	}
}

void JumpingSumoControlador::setOnNewImageJPEG (JumpingSumoControlador::OnNewImageJPEG cbOnNewImageJPEG)
{
	this->cbOnNewImageJPEG = cbOnNewImageJPEG;
}

void JumpingSumoControlador::onNewImageJPEG (const std::string & jpegData)
{
	if (this->cbOnNewImageJPEG != NULL)
	{
		this->cbOnNewImageJPEG (jpegData);
	}
}

void JumpingSumoControlador::setOnBateriaCambia (JumpingSumoControlador::OnBateriaCambia cbOnBateriaCambia)
{
	this->cbOnBateriaCambia = cbOnBateriaCambia;
}

void JumpingSumoControlador::onBateriaCambia (const unsigned char nuevoValor)
{
	if (this->cbOnBateriaCambia != NULL)
	{
		this->cbOnBateriaCambia (nuevoValor);
	}
}
