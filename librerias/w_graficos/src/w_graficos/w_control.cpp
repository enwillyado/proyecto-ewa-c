/*********************************************************************************************
 * Name			: w_control.cpp
 * Description	: Implementación de la clase para gestionar un control con parte gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_control.h"

// Constructores
WControl::WControl (const std::string & iName)
	: WControlJerarquizable(),
	  WControlMouseable(), WControlKeyable(),
	  name (iName), lienzo()
{
	this->xPos = 0;
	this->yPos = 0;
}

// Operadores
void WControl::operator= (const WControl & org)
{
	this->xPos = org.xPos;
	this->yPos = org.yPos;
	this->lienzo = org.lienzo;
}

// Accesores
WLienzo & WControl::getLienzo()
{
	return this->lienzo;
}
const WLienzo & WControl::getLienzo() const
{
	return this->lienzo;
}
void WControl::setLienzo (const WLienzo & lienzo)
{
	this->lienzo = lienzo;
}

size_t WControl::getPosX() const
{
	return this->xPos;
}
size_t WControl::getPosY() const
{
	return this->yPos;
}

size_t WControl::getAlto() const
{
	return this->lienzo.getAlto();
}
size_t WControl::getAncho() const
{
	return this->lienzo.getAncho();
}

// Modificadores
void WControl::setPos (const size_t posX, const size_t posY)
{
	this->setPosX (posX);
	this->setPosY (posY);
}
void WControl::setPosX (const size_t posX)
{
	this->xPos = posX;
}
void WControl::setPosY (const size_t posY)
{
	this->yPos = posY;
}

void WControl::resize (const size_t ancho, const size_t alto)
{
	this->lienzo = this->lienzo.resize (ancho, alto);
	this->recomponerLienzo();
}

void WControl::setSize (const size_t ancho, const size_t alto)
{
	this->lienzo.reserve (ancho, alto);
	this->recomponerLienzo();
}

void WControl::pintar()
{
	WControl* parent = this->getParentPtr();
	if (parent != NULL)
	{
		parent->pintar();
	}
}
WLienzo & WControl::recomponerLienzo()
{
	return this->getLienzo();
}

// Observadores
bool WControl::esApuntadoPor (const WControl & control) const
{
	const WCoordenada & coordenada = WCoordenada (control.getPosX(), control.getPosY());
	return this->esApuntadoPor (coordenada);
}
bool WControl::esApuntadoPor (const WCoordenada & control) const
{
	const bool aLoAncho = ((signed)this->xPos <= (signed)control.posX) &&
						  ((signed) (control.posX) <= ((signed)this->xPos + this->lienzo.getAncho()));
	const bool aLoAlto  = ((signed)this->yPos <= (signed)control.posY) &&
						  ((signed) (control.posY) <= ((signed)this->yPos + this->lienzo.getAlto()));
	return aLoAlto && aLoAncho;
}
bool WControl::leSobreVuela (const WControl & control) const
{
	const bool aLoAncho = ((signed)this->xPos <= (signed)control.xPos) &&
						  (((signed)control.xPos + control.lienzo.getAncho()) <= ((signed)this->xPos + this->lienzo.getAncho()));
	const bool aLoAlto  = ((signed)this->yPos <= (signed)control.yPos) &&
						  (((signed)control.yPos + control.lienzo.getAlto()) <= ((signed)this->yPos + this->lienzo.getAlto()));
	return aLoAlto && aLoAncho;
}
bool WControl::leInterseca (const WControl & control) const
{
	const bool aLoAncho = ((signed)this->xPos <= ((signed)control.xPos - control.lienzo.getAncho())) &&
						  (((signed)control.xPos + control.lienzo.getAncho()) <= ((signed)this->xPos + this->lienzo.getAncho()));
	const bool aLoAlto  = ((signed)this->yPos <= ((signed)control.yPos - control.lienzo.getAlto())) &&
						  (((signed)control.yPos + control.lienzo.getAlto()) <= ((signed)this->yPos + this->lienzo.getAlto()));
	return aLoAlto && aLoAncho;
}

WControl & WControl::getSelf()
{
	return *this;
}
const WControl & WControl::getSelf() const
{
	return *this;
}

const std::string & WControl::getName() const
{
	return this->name;
}

void WControl::clear()
{
	this->WControlMouseable::clear();
	this->WControlKeyable::clear();
}
