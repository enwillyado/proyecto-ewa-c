/*********************************************************************************************
 *	Name		: _w_red_srvtcp.cpp
 *	Description	: Implementaci�n del Servidor UDP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_srvudp.h"

#include "w_base/toStr.hpp"				//< para tener el toStr
#include <stdlib.h>						//< para el "atoi"
#include <string.h>						//< para el "memcpy"
#ifndef WIN32
#include <netdb.h>
#endif

#include <errno.h>						//< para poder tener los errores

#define SOCKET_ERROR_SIGNAL		-1		// valor que retornar� "recvtimeout(...)" en caso de error gen�rico
#define SOCKET_TIMEOUT_SIGNAL	-2		// valor que retornar� "recvtimeout(...)" en caso de que termine el tiempo de espera
#define SOCKET_UNKNOWN_SIGNAL	-3		// valor que retornar� "recvtimeout(...)" en caso de error desconocido

#include "w_red/w_red_srvudp_private.h"

// --------------------------------------------------------------------------
// Implementaci�n del Servidor TCP

ServidorUDP::ServidorUDP()
	: serverSocket()
{
	this->eventCallBack = NULL;
	this->callBackObj = NULL;
	this->sockfd = DESCRIPTOR_NULO;
	this->lastError = ServidorUDP::DESCONECTADO;
}

// --------------------------------------------------------------------------
// L�gica del Servidor:
/**
 *	Obitene el �ltimo evento que se recibe
 *	\retval	el evento que se recibe y lo elimina de la cola de eventos pendientes
**/
EventoServidorUDP ServidorUDP::getEvento()
{
	if (this->colaEventos.size())
	{
		EventoServidorUDP evento = this->colaEventos.front();
		this->colaEventos.pop();
		return evento;
	}
	return EventoServidorUDP();
}
/**
 *	Obitene el n�mero de eventos que a�n est�n pendientes de
 *	\retval	el n�mero esperado
**/
size_t ServidorUDP::getNumEventosPendintes()
{
	return this->colaEventos.size();
}

/**
 *	Procesa un evento nuevo. Por defecto, lo encola y luego llama
 *		al callBack si est� definido; si no, intenta procesarlo como buenamente pueda
 *	\param	evento	[in]	evento que se va a procesar
**/
void ServidorUDP::analizaEvento (const EventoServidorUDP & evento)
{
	if (this->eventCallBack != NULL)
	{
		this->colaEventos.push (evento);
		this->eventCallBack (*this);
	}
	else
	{
		this->analizaEventoDef (evento);
	}
}
void ServidorUDP::analizaEventoDef (const EventoServidorUDP & evento)
{
	switch (evento.tipo)
	{
	case EventoServidorTCP::CONEXION_ACEPTADA:
		// NOTA: no tiene mucho sentido en UDP la conexi�n
		break;

	case EventoServidorUDP::MENSAJE_RECIBIDO:
	case EventoServidorUDP::MENSAJE_RECIBIDO_VACIO:
		if (this->analizaEventoMsgRecv (evento))
		{
			return;
		}
		break;
	case EventoServidorTCP::CONEXION_DESCONECTADA :
	case EventoServidorTCP::CONEXION_PERDIDA :
	case EventoServidorTCP::ERROR_AL_RECIBIR:
	{
		// NOTA: no tiene mucho sentido en UDP la desconexi�n
		break;
	}
	default:
		return;
	}
}

/**
 *	Analiza un nuevo mensaje recibido para procesarlo si se conoce el protocolo que tiene
 *	\param	evento	[in]	evento (de MENSAJE_RECIBIDO) que se va a analizar
 *	\retval	TRUE si el evento fue analizado; FALSE si no
**/
bool ServidorUDP::analizaEventoMsgRecv (const EventoServidorUDP & evento)
{
	return false;
}


/**
 *	Ajusta la funci�n a la que se llamar� cuando se reciba un evento en el servidor
 *	\param	evento	[in]	puntero a la funci�n que se llamar�
 *	\retval	TRUE si se ajust� el callback, FALSE si no
**/
bool ServidorUDP::setCallBack (void (*ptr) (ServidorUDP &), void* callBackObj)
{
	this->eventCallBack = ptr;
	this->callBackObj = callBackObj;
	return (ptr != NULL);
}

void* ServidorUDP::getCallBackObjPtr() const
{
	return this->callBackObj;
}

/**
 *	Obtiene el HOST en el que el Servidor TCP est� escuchando
 *	\retval	el HOST en el que se est� escuchando
 *		o "" si no est� escuchando
**/
const std::string & ServidorUDP::getHost()
{
	return this->host;
}

/**
 *	Obtiene el PUERTO en el que el Servidor TCP est� escuchando
 *	\retval	el PUERTO en el que se est� escuchando
 *		o DESCRIPTOR_NULO si no est� escuchando
**/
unsigned int ServidorUDP::getPort()
{
	if (this->port != 0)
	{
		return this->port;
	}
	return this->serverSocket.getRealPort();
}

/**
 *	Empieza la escucha por el puerto dado
 *	\param	port	[in]	puerto a conectar (como string)
 *	\retval	TRUE si se abri� correctamente, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorUDP::open (const std::string & port)
{
	return this->open (::atoi (port.c_str()));
}

/**
 *	Empieza la escucha por el puerto dado
 *	\param	port	[in]	puerto a conectar
 *	\retval	TRUE si se abri� correctamente, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorUDP::open (unsigned int port)
{
	this->lastError = ServidorUDP::CONECTANDO;
	this->port = port;
	// reinicializamos la instancia que manejar� el socket TCP
	this->serverSocket = ServerSocket (this);
	// Abrir el puerto
	if (false == serverSocket.openSocket (port))
	{
		this->lastError = ServidorUDP::ERRONEO;
		return false;
	}
	this->host = NOMBRE_EN_LOCAL;		//< ponerlo a la escucha
	return true;
}

/**
 *	Empieza a procesar las peticiones
 *	\retval	TRUE si se abri� correctamente, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorUDP::start()
{
	// Bloquearse procesando peticiones hasta que falle o se cierre
	const bool ret = this->serverSocket.procesarPeticiones();
	return ret;
}

/**
 *	Env�a un mensaje de respuesta al evento dado
 *	\param	evento	[in]	evento que se va a procesar
 *	\param	mensaje	[in]	mensaje que se va a (intentar) enviar
 *	\retval	TRUE si se envi� el mensaje correctamente
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorUDP::send (const EventoServidorUDP & evento, const std::string & mensaje)	// TODO: AJUSTAR NOMBRE
{
	return serverSocket.sendMessageUDP (evento.socketID, evento.idUDP, mensaje.c_str(), mensaje.size()) > 0;
}

/**
 *	Env�a un mensaje de respuesta al evento dado
 *	\param	evento	[in]	evento que se va a procesar
 *	\param	mensaje	[in]	mensaje que se va a (intentar) enviar
 *	\retval	TRUE si se envi� el mensaje correctamente
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorUDP::enviar (const std::string & mensaje, const std::string & host, const int port)
{
	return this->enviar (mensaje, host, ::toStr (port));
}

/**
 *	Env�a un mensaje de respuesta al evento dado
 *	\param	evento	[in]	evento que se va a procesar
 *	\param	mensaje	[in]	mensaje que se va a (intentar) enviar
 *	\retval	TRUE si se envi� el mensaje correctamente
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorUDP::enviar (const std::string & mensaje, const std::string & host, const std::string & port)
{
	// Crear la estructura
	struct sockaddr_in servaddr;
	memset (&servaddr, 0, sizeof (servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons (::atoi (port.c_str()));

	// Rellenar el campo "host"
#define CON_GETHOSTBYNAME
#ifdef CON_GETHOSTBYNAME
	struct hostent* he = ::gethostbyname (host.c_str());
	if (he == NULL)
	{
		return false;
	}
	memcpy (&servaddr.sin_addr, he->h_addr_list[0], he->h_length);
#else
	struct addrinfo hints, *servinfo;
	memset (&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;			// IPv4
	hints.ai_socktype = SOCK_DGRAM;		// UDP
	int rv = ::getaddrinfo (host.c_str(), port.c_str(), &hints, &servinfo);
	if (rv != 0)
	{
#ifdef WIN32
		int ei = WSAGetLastError();
#endif
		return false;
	}
	// use the first we can
	if (servinfo == NULL)
	{
		return false;
	}
	struct sockaddr_in* addrinfo_in = (struct sockaddr_in*)servinfo;
	servaddr.sin_addr = addrinfo_in->sin_addr;
	::freeaddrinfo (servinfo); // all done with this structure
#endif

	// Pasar el identificador
	IdentificadorUDP idUDP;
	(* ((struct sockaddr_in*)&idUDP.their_addr)) = servaddr;
	idUDP.addr_size = sizeof (servaddr);

	if (serverSocket.sendMessageUDP (this->sockfd, &idUDP, mensaje.c_str(), mensaje.size()) != mensaje.size())
	{
		this->lastError = ServidorUDP::ERRONEO;
		return false;
	}

	return true;
}

static int recvfromtimeout (int sock, char* data, int length, int timeoutinseconds, sockaddr_in* sockfrom,
							socklen_t* addr_len)
{
	fd_set socks;
	struct timeval t;
	FD_ZERO (&socks);
	FD_SET (sock, &socks);
	t.tv_sec = timeoutinseconds;
	t.tv_usec = 0;
	int n = select (-1, &socks, NULL, NULL, &t);
	if (n == 0)
	{
		return SOCKET_TIMEOUT_SIGNAL;  // timeout!
	}
	if (n == -1)
	{
		return SOCKET_ERROR_SIGNAL;  // error
	}
	return recvfrom (sock, data, length, 0, (sockaddr*)sockfrom, addr_len);
}

bool ServidorUDP::recibir (std::string & mensaje, unsigned int segundosEsperados)
{
	struct sockaddr_storage their_addr;
	socklen_t addr_len = sizeof their_addr;
	char msg [RECV_MENSAJE_MAX_SIZE];
	int bytes_rec;
	if (segundosEsperados > 0)
	{
		bytes_rec = recvfromtimeout (this->sockfd, msg, RECV_MENSAJE_MAX_SIZE, segundosEsperados,
									 (struct sockaddr_in*)&their_addr, &addr_len);
	}
	else
	{
		bytes_rec = ::recvfrom (this->sockfd, msg, RECV_MENSAJE_MAX_SIZE, 0, (struct sockaddr*)&their_addr, &addr_len);
	}
	if (bytes_rec > 0)
	{
		// Se ha recibido un paquete...
		this->lastError = ServidorUDP::CONECTADO;
		mensaje = std::string (msg, bytes_rec);	// devolver el mensaje recibido a la longitud que tiene
		return true;
	}
	else if (bytes_rec == 0)
	{
		// it can be "0", and this can mean only one thing: remote has closed connection!
		// si no, es que se cerr� la conexi�n
		this->close();
		mensaje = "";
		return true;
	}
	else if (bytes_rec == SOCKET_TIMEOUT_SIGNAL)
	{
		if (segundosEsperados > 0)
		{
			// simplemente es un timeout!
			this->lastError = ServidorUDP::SOCKET_RECV_TIMEOUT;
			return false;	// TODO: �podr�a ser "true"?
		}
		// Si llegas aqu� eres un maldito HACKER; o un manazas ha tocado lo que devuelve 'recvtimeout(...)'.
		this->lastError = ServidorUDP::SOCKET_RECV_TIMEOUT_ERROR;
		return false;
	}
	else if (bytes_rec == SOCKET_ERROR_SIGNAL)
	{
		// it can be "-1", and this is a error
		this->lastError = ServidorUDP::ERRONEO;
#ifdef WIN32
		const int ei = WSAGetLastError();
#else
		const int ei = errno;
#endif
		return false;
	}
	else	//< SOCKET_UNKNOWN_SIGNAL and others returns
	{
		// fatal error unknown
		this->lastError = ServidorUDP::ERRONEO;
		return false;
	}
	return false;
}
/**
 *	El servidor deja de escuchar por el puerto que tiene abierto
 *	\retval	TRUE si se cerr� correctamente la conexi�n, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorUDP::close()
{
	this->setDesconectando();
	if (serverSocket.closeSrvSocket() == false)
	{
		this->lastError = ServidorUDP::ERRONEO;
		return false;
	}
	this->host = "";
	this->sockfd = DESCRIPTOR_NULO;
	this->setDesconectado(); // HACK: se deber�a confirmar solo al salir de procesandoPeticiones() si es que se invoc�!
	return true;
}


/**
 *	Obtiene el estado actual del servidor
 *	\retval	el estado
**/
ServidorUDP::Errores ServidorUDP::getLastError() const
{
	return this->lastError;
}

/**
 *	Funci�n que obtiene el �ltimo error ocurrido como string
 *	\retval	el �ltimo valor ocurrido, ya convertido a "error" b�sico
**/
std::string ServidorUDP::getLastErrorStr() const
{
	return ServidorUDP::toStr (this->getLastError());
}
/**
 *	Funci�n que obtiene el �ltimo error ocurrido como string
 *	\retval	el �ltimo valor ocurrido, ya convertido a "error" b�sico
**/
std::string ServidorUDP::toStr (ServidorUDP::Errores error)
{
	switch (error)
	{
	// Errores del socket:
	case CONECTANDO:
		return "CONECTANDO";
	case CONECTADO:
		return "CONECTADO";
	case DESCONECTANDO:
		return "DESCONECTANDO";
	case DESCONECTADO:
		return "DESCONECTADO";
	case ERRONEO:
		return "ERRONEO";

		// NOTA: No hay caso 'default' para que, revisando los warns de compilaci�n, se pueda
		//			detectar nuevos casos de error que no est�n puestos humanamente legibles
	}
	return "Error Desconocido";
}

/**
 *	Obtiene el estado actual del socket del servidor
 *	\retval	el estado
**/
ServerSocket::Errores ServidorUDP::getLastErrorSocket() const
{
	return this->serverSocket.getLastError();
}

/**
 *	Obtiene el estado actual del socket del servidor
 *	\retval	el estado
**/
std::string ServidorUDP::getLastErrorSocketStr() const
{
	return this->serverSocket.getLastErrorStr();
}

// ----------------------------------------------------------------------------
// Funciones que llamar� su amigo ServerSocket que ajustan el estado
void ServidorUDP::setConectado()
{
	this->lastError = ServidorUDP::CONECTADO;
}
void ServidorUDP::setEscuchando()
{
	this->lastError = ServidorUDP::ESCUCHANDO;
}
void ServidorUDP::setProcesando()
{
	this->lastError = ServidorUDP::PROCESANDO;
}
void ServidorUDP::setDesconectando()
{
	this->lastError = ServidorUDP::DESCONECTANDO;
}
void ServidorUDP::setDesconectado()
{
	this->lastError = ServidorUDP::DESCONECTADO;
}
