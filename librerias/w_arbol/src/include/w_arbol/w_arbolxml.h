/*********************************************************************************************
 *	Name		: w_arbolxml.h
 *	Description	: Arbol para trabajar m�s c�modamente en memoria, usando la stl y libxml++
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ARBOLXML_H_
#define _W_ARBOLXML_H_

#define DELIMITADORES_TRIM 		" \f\n\r\t\v"
#define ENCABEZADO_XML(cstr)	"<?xml version=\"1.0\" encoding=\"" cstr "\" standalone=\"yes\"?>"

#include "w_arbol/w_arbol.h"

class ArbolXML : public Arbol
{
public:

	ArbolXML (const std::string & nuevaClave = "", const std::string & nuevoValor = "");		//< lo crea con la clave dada, el valor dado
	ArbolXML (const Arbol &);

	// Transformar a XML
	std::string genXML (bool conSaltosDeLinea = true, bool conEncabezado = true) const;
	static std::string genXML (const Arbol &, bool conSaltosDeLinea = true, bool conEncabezado = true);
	static std::string genCabeceraXMLStd (const Arbol &);

	// Salvar en fichero
	bool saveToFile (const std::string &, bool conSaltosDeLinea = true);

	// Obtener/Ajustar la ruta para el "TryLoad" y dem�s...
	const std::string & getPathDir() const;
	void setPathDir (const std::string &);
	void setPathDirFromFile (const std::string &);

	static ArbolXML getArbolFromSTR (const std::string & str);
	static ArbolXML getArbolFromFile (const std::string & filename);

	// Utilidades
	static bool file_exists_and_can_be_read_from (const std::string & filename);
	static std::string leerContenidoFile (const std::string & filename);
	static std::string trim (const std::string & valor);
	static std::string htmlEntitiesDecode (const std::string & str);
	static std::string htmlEntitiesEncode (const std::string & str);

private:
	static std::string genXML (const Arbol &, bool conSaltosDeLinea, bool conEncabezado, size_t nivelTab);

private:
	// �rea de datos
	std::string pathDir;
};

#endif	//_W_ARBOLXML_H_
