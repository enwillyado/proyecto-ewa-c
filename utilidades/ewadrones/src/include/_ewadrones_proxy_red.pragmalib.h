/*********************************************************************************************
 *	Name		: _ewadrones_proxy_red.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar el ejecutable para el PROXY TCP
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _EWADRONES_PROXY_RED_PRAGMALIB_H_
#define _EWADRONES_PROXY_RED_PRAGMALIB_H_

#ifdef _MSC_VER

// Parte privada
#include "w_red/_w_red_proxy.pragmalib.h"

#endif

#undef _EWADRONES_PROXY_RED_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _EWADRONES_PROXY_RED_PRAGMALIB_H_
