/*********************************************************************************************
 * Name			: w_rgba.cpp
 * Description	: Implementación de la clase para gestionar color RGBA
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_rgba.h"
#include <cstdlib>	//< ::strtol

RGBA::RGBA (const RGBA::byte r, const RGBA::byte g,
			const RGBA::byte b, const RGBA::byte a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

RGBA::RGBA (const std::string & str)
{
	this->loadStr (str);
}
RGBA::RGBA (const char* const str)
{
	this->loadStr (str);
}

void RGBA::loadStr (const std::string & str)
{
	if (str.size() == 8)
	{
		this->r = (RGBA::byte) (::strtol (str.substr (2, 2).c_str(), NULL, 16));
		this->g = (RGBA::byte) (::strtol (str.substr (4, 2).c_str(), NULL, 16));
		this->b = (RGBA::byte) (::strtol (str.substr (6, 2).c_str(), NULL, 16));
		this->a = 0xFF;
	}
	else
	{
		this->r = 0x00;
		this->g = 0x00;
		this->b = 0x00;
		this->a = 0x00;
	}
}

void RGBA::invertColor()
{
	this->r = 0xFF - this->r;
	this->g = 0xFF - this->g;
	this->b = 0xFF - this->b;
}

void RGBA::brightness (const short u)
{
	if (u >= 0)
	{
		this->r = (0xFF - this->r > u) ? (this->r + u) : 0xFF;
		this->g = (0xFF - this->g > u) ? (this->g + u) : 0xFF;
		this->b = (0xFF - this->b > u) ? (this->b + u) : 0xFF;
	}
	else
	{
		const short v = -u;
		this->r = (this->r > v) ? (this->r - v) : 0x00;
		this->g = (this->g > v) ? (this->g - v) : 0x00;
		this->b = (this->b > v) ? (this->b - v) : 0x00;
	}
}
