/*********************************************************************************************
 *	Name		: zxing_cmd.cpp
 *	Description	: Ejecutable donde se implementa un ejemplo para leer c�digos en BMP
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_zxing_cmd.pragmalib.h"

#include "w_graficos/w_lienzo_bmp.h"
#include "zxing/w_zxing.h"

#include <iostream>

int main (int argc, char** argv)
{
	WLienzoBMP lienzo ("../testdata/QR.bmp");
	const WZxingResult result = WZxing::procesar (lienzo);

	// Texto
	std::cout << "Formato:" << std::endl;
	std::cout << " " << result.getFormato() << std::endl;

	// Coordenadas
	std::cout << "Coordenadas:" << std::endl;
	const WZxingResult::WCoordenadas & coordenadas = result.getCoordenadas();
	for (WZxingResult::WCoordenadas::const_iterator itr = coordenadas.begin();
			itr != coordenadas.end(); itr++)
	{
		const WCoordenada & coordenada = *itr;
		std::cout << " " << coordenada.posX << " x " << coordenada.posY << std::endl;
	}

	// Texto
	std::cout << "TEXTO:" << std::endl;
	std::cout << " " << result.getTexto() << std::endl;

	std::cout << "------------------------------" << std::endl;
	std::cout << "Presione una tecla para cerrar..." << std::endl;
	std::cin.get();
	return 0;
}
