/*********************************************************************************************
 * Name			: _w_appweb_tester.pragmalib.h
 * Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_APPWEB_TESTER_PRAGMALIB_H_
#define _W_APPWEB_TESTER_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

// Parte privada
#include "w_appweb/_w_appweb.pragmalib.h"

#endif

#undef _W_APPWEB_TESTER_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_APPWEB_TESTER_PRAGMALIB_H_
