/*********************************************************************************************
 *	Name		: w_red_srvudp_private.h
 *	Description	: Servidor UDP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRVUDP_PRIVATE_H_
#define _W_RED_SRVUDP_PRIVATE_H_

#ifdef WIN32
#include <ws2tcpip.h>
#else
#include <sys/socket.h>
#endif

class IdentificadorUDP
{
public:
	struct sockaddr_storage their_addr;
	socklen_t addr_size;
};

#endif	//_W_RED_SRVUDP_PRIVATE_H_
