/*********************************************************************************************
*	Name		: std_types.hpp
*	Description	: Definiciones para usar elementos T de la stl con un typedef
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _STD_TYPES_HPP_
#define _STD_TYPES_HPP_

#include <string>	// std::string
#include <vector>	// std::vector

typedef std::vector<std::string> strvector;
typedef std::vector<strvector> strmatriz;

#endif	//_STD_TYPES_HPP_
