/*********************************************************************************************
 * Name			: w_lienzo_bmp.h
 * Description	: Clase para cargar lienzos en BMP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_LIENZO_BMP_H_
#define _W_LIENZO_BMP_H_

#include "w_graficos/w_lienzo.h"

#include <string>

class WLienzoBMP : public WLienzo
{
public:
	WLienzoBMP (const std::string & filename = "");
	static WLienzoBMP getWLienzoBMPFromFile (const std::string & filename);

	bool leerContenidoFile (const std::string & filename);

	/*
	void save (const std::string & filename);
	void printHeader (void);
	void negative (void);
	void brightness (const int u);
	void fromRGBtoGRAY (void);
	void redChannel (void);
	void greenChannel (void);
	void blueChannel (void);
	*/

public:
	typedef unsigned char	BYTE;	// 8-bits
	typedef unsigned short	WORD;	// 16-bits
	typedef unsigned int	DWORD;	// 32-bits
	typedef struct
	{
		WORD   identifier;			// Magic number: BM
		DWORD  size;				// tama�o del archivo
		DWORD  reserved;			//
		DWORD  bitoffset;			// offset para comenzar a leer la data
		DWORD  headerSize;			// tama�o de la cabecera
		DWORD  width;				// ancho
		DWORD  height;				// alto
		WORD   planes;				// numero de planos del bitmap
		WORD   bitsPerPixel;		// bit por pixel
		DWORD  compression;			// especificacion de la compresion
		DWORD  imageSize;			// tama�o de la seccion de datos
		DWORD  hresolution;			// resolucion horizontal medida en pixeles por metro
		DWORD  vresolution;			// resolucion vertical medida en pixeles por metro
		DWORD  numberOfColours;		// numero de colores ( 2^bitsPerPixel )
		DWORD  importantColours;	// numero de colores importantes( si = 0: todos los colores son importantes )

	} BMPHeader;

private:
	BMPHeader header;		// Cabecera
};

#endif // _W_LIENZO_BMP_H_
