/*********************************************************************************************
 * Name			: w_areaResize.h
 * Description	: Clase para gestionar un control de �rea con scroll vertical
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_AREA_RESIZE_T_H_
#define _W_AREA_RESIZE_T_H_

#include "w_graficos/w_multicontrol.h"
#include "w_graficos/w_app.h"

#include <string>

// ----------------------------------------------------------------------------
// Controles externo (final)
template<class T>
class WAreaResizeT : public T
{
public:
	// Constructores
	WAreaResizeT (const std::string & iName = "")
		: T (iName), invertido (false), abajo (false)
	{
	}
	WAreaResizeT (WAreaResizeT & org)
		: T (org), invertido (false), abajo (false)
	{
	}

	// Manejadores
	virtual void onMoveMouseOver()
	{
		const WCoordenada & punteroPos = this->getPunteroPos();
		bool dentro = false;
		if (punteroPos.posY < 5 || this->getAlto() - punteroPos.posY < 5)
		{
			dentro = true;
		}
		if (punteroPos.posX < 5 || this->getAncho() - punteroPos.posX < 5)
		{
			dentro = true;
		}

		// Actualizar
		this->actualizar (dentro);
	}
	virtual void onMoveMouseOut()
	{
		this->actualizar (false);
	}

	virtual void onLDownMouseOver()
	{
		this->abajo = true;
		T::onLDownMouseOver();
	}
	virtual void onMDownMouseOver()
	{
		this->abajo = true;
		T::onMDownMouseOver();
	}
	virtual void onRDownMouseOver()
	{
		this->abajo = true;
		T::onRDownMouseOver();
	}

	virtual void onLUpMouseOver()
	{
		this->abajo = false;
		this->actualizar (false);
		T::onLUpMouseOver();
	}
	virtual void onMUpMouseOver()
	{
		this->abajo = false;
		this->actualizar (false);
		T::onMUpMouseOver();
	}
	virtual void onRUpMouseOver()
	{
		this->abajo = false;
		this->actualizar (false);
		T::onRUpMouseOver();
	}

	virtual void onLUpMouseOut()
	{
		this->abajo = false;
		this->actualizar (false);
		T::onLUpMouseOut();
	}
	virtual void onMUpMouseOut()
	{
		this->abajo = false;
		this->actualizar (false);
		T::onMUpMouseOut();
	}
	virtual void onRUpMouseOut()
	{
		this->abajo = false;
		this->actualizar (false);
		T::onRUpMouseOut();
	}

protected:
	static void arrastrar (const WEventoMouseDrag & e)
	{
		WControl & c = e.getSelf();
		WLienzo & l = c.getLienzo();
		const size_t nuevoAncho = c.getAncho() + (signed)e.getIncrementoPosX();
		const size_t nuevoAlto = c.getAlto() + (signed)e.getIncrementoPosY();
		if ((signed)nuevoAncho > 10 && (signed)nuevoAlto > 10)
		{
			l = l.resize (nuevoAncho, nuevoAlto);
			e.getRoot().pintar();
		}
	}
	void actualizar (const bool dentro)
	{
		if (dentro == true)
		{
			this->setOnLMouseDrag (&arrastrar);
			this->setOnMMouseDrag (&arrastrar);
			this->setOnRMouseDrag (&arrastrar);
		}
		else
		{
			if (abajo == false)
			{
				this->setOnLMouseDrag (NULL);
				this->setOnMMouseDrag (NULL);
				this->setOnRMouseDrag (NULL);
			}
			else
			{
				return;
			}
		}
		if (dentro == !invertido)
		{
			invertido = !invertido;
			this->getRoot().getCursor().getLienzo().invertColor();
			this->getRoot().pintar();
		}
	}
	bool invertido;
	bool abajo;
};

#endif // _W_AREA_RESIZE_T_H_
