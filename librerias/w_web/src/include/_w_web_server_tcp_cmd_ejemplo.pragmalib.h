/*********************************************************************************************
 *	Name		: _w_web_server_cmd_ejemplo.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_SERVER_CMD_EJEMPLO_PRAGMALIB_H_
#define _W_WEB_SERVER_CMD_EJEMPLO_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_web/_w_web_server.pragmalib.h"
#include "w_web/_w_web_protocol.pragmalib.h"
#include "w_web/_w_web_services.pragmalib.h"

#endif

#undef _W_WEB_SERVER_CMD_EJEMPLO_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_WEB_SERVER_CMD_EJEMPLO_PRAGMALIB_H_
