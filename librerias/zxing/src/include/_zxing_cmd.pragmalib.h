/*********************************************************************************************
 *	Name		: _zxing_cmd.pragmalib.h
 *	Description	: Librer�as necesarias para enlazar el ejecutable de prueba
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _ZXING_CMD_PRAGMALIB_H_
#define _ZXING_CMD_PRAGMALIB_H_

#ifdef _MSC_VER

// Parte privada
#include "zxing/_w_zxing.pragmalib.h"

#endif

#undef _ZXING_CMD_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _ZXING_CMD_PRAGMALIB_H_
