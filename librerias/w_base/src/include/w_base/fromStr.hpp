/*********************************************************************************************
*	Name		: fromStr.hpp
*	Description	: M�todo de conversi�n T, basado en el operador de flujo, para pasar de string
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _FROM_STR_HPP_
#define _FROM_STR_HPP_

#include <string>       // std::string
#include <sstream>      // std::stringstream, std::stringbuf

template <class T>
static T fromStr (const std::string & s)
{
	// Push
	std::stringstream ss;
	ss << s;

	// Pop
	T ret;
	ss >> ret;
	return ret;
}

#endif	//_FROM_STR_HPP_
