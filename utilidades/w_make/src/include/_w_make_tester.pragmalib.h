/*********************************************************************************************
 *	Name		: _w_make_tester.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_MAKE_TESTER_PRAGMALIB_H_
#define _W_MAKE_TESTER_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

// Parte privada
#include "w_make/_w_make.pragmalib.h"

#endif

#undef _W_MAKE_TESTER_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_MAKE_TESTER_PRAGMALIB_H_
