/*********************************************************************************************
 *	Name		: w_base_tester_strings.cpp
 *	Description	: Pruebas unitarias para probar los mecanismos de cadenas de caracteres
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_base/strToVector.hpp"
#include "w_base/strToMatriz.hpp"

#include "w_base/strTrim.hpp"
#include "w_base/strPurge.hpp"
#include "w_base/strTransform.hpp"

#include <assert.h>

int baseTesterStrings()
{
	// ---------------------------------------------------------------------------
	// strToMatriz
	{
		const std::string & texto = "hola,mundo;saludos al mundo entero,un poco,";
		const strmatriz & trozos = ::strToMatriz (texto, ';', ',');
		assert (2 == trozos.size());
		assert (2 == trozos[0].size());
		assert ("hola" == trozos[0][0]);
		assert ("mundo" == trozos[0][1]);

		assert (3 == trozos[1].size());
		assert ("saludos al mundo entero" == trozos[1][0]);
		assert ("un poco" == trozos[1][1]);
		assert ("" == trozos[1][2]);
	}

	// ---------------------------------------------------------------------------
	// strPurge
	{
		{
			const std::string & texto = " h o l a ";
			const std::string & textoPurge = ::strPurge (texto, ' ');
			assert ("hola" == textoPurge);
		}
		{
			const std::string & texto = " h o l a ";
			const std::string & textoPurge = ::strPurge (texto, " ");
			assert ("hola" == textoPurge);
		}
		{

			const std::string & texto = " h o l a ";
			const std::string & textoPurge = ::strPurge (texto, "oa ");
			assert ("hl" == textoPurge);
		}
	}
	// ---------------------------------------------------------------------------
	// strTrim
	{
		// --------------------------------------------------------------------------
		// Izquierda y derecha
		{
			const std::string & texto = "    h";
			const std::string & textoTrim = ::strTrim (texto);
			assert ("h" == textoTrim);
		}
		{
			const std::string & texto = "    h   ";
			const std::string & textoTrim = ::strTrim (texto);
			assert ("h" == textoTrim);
		}
		{
			const std::string & texto = "h    ";
			const std::string & textoTrim = ::strTrim (texto);
			assert ("h" == textoTrim);
		}
		{
			const std::string & texto = "    ";
			const std::string & textoTrim = ::strTrim (texto);
			assert ("" == textoTrim);
		}

		// --------------------------------------------------------------------------
		// Solo izquierda
		{
			const std::string & texto = "    h";
			const std::string & textoTrim = ::strTrimL (texto);
			assert ("h" == textoTrim);
		}
		{
			const std::string & texto = "    h   ";
			const std::string & textoTrim = ::strTrimL (texto);
			assert ("h   " == textoTrim);
		}
		{
			const std::string & texto = "h    ";
			const std::string & textoTrim = ::strTrimL (texto);
			assert ("h    " == textoTrim);
		}
		{
			const std::string & texto = "    ";
			const std::string & textoTrim = ::strTrimL (texto);
			assert ("" == textoTrim);
		}

		// --------------------------------------------------------------------------
		// Derecha e izquierda
		{
			const std::string & texto = "    h";
			const std::string & textoTrim = ::strTrimR (texto);
			assert ("    h" == textoTrim);
		}
		{
			const std::string & texto = "    h   ";
			const std::string & textoTrim = ::strTrimR (texto);
			assert ("    h" == textoTrim);
		}
		{
			const std::string & texto = "h    ";
			const std::string & textoTrim = ::strTrimR (texto);
			assert ("h" == textoTrim);
		}
		{
			const std::string & texto = "    ";
			const std::string & textoTrim = ::strTrimR (texto);
			assert ("" == textoTrim);
		}
	}
	{
		{
			const std::string & textoOrg = "HoLa";
			const std::string & textoMayusculas = ::strToUpper (textoOrg);
			assert ("HOLA" == textoMayusculas);
		}
		{
			const std::string & textoOrg = "HoLa";
			const std::string & textoMinusculas = ::strToLower (textoOrg);
			assert ("hola" == textoMinusculas);
		}

		{
			{
				assert ("hola mundo maravilloso" == ::remplazar ("hola mundo cruel", "cruel", "maravilloso"));
				assert ("esa asa" == ::remplazar ("eso oso", "o", "a"));
				assert ("aaaaaaa" == ::remplazar ("aaaaaaa", "a", "a"));
				assert ("hola" == ::remplazar ("hola mundo cruel", " mundo cruel", ""));
				assert ("" == ::remplazar ("", "", "o"));
				assert ("hola" == ::remplazar ("hola", "", "o"));
				assert ("ola" == ::remplazar ("hola", "h", ""));
			}
			{
				assert ("el lolo" == ::remplazar ("es solo", 's', 'l'));
				assert ("esa asa" == ::remplazar ("eso oso", 'o', 'a'));
				assert ("aaaaaaa" == ::remplazar ("aaaaaaa", 'a', 'a'));
			}
		}
	}
	return 0;
}
