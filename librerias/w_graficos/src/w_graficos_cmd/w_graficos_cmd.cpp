/*********************************************************************************************
 *	Name		: w_graficos_cmd.cpp
 *	Description	: Este fichero sirve para ver en funcionamiento
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_graficos_cmd.pragmalib.h"

#include "w_graficos/w_app.h"
#include "w_graficos/w_xlienzo.h"
#include "w_graficos/w_lienzo_bmp.h"
#include "w_graficos/w_lienzo_jpeg.h"
#include "w_graficos/w_rgba.h"

#include "w_graficos/w_textControl.h"
#include "w_graficos/w_vScrollControl.h"
#include "w_graficos/w_areaScrollControl.h"
#include "w_graficos/w_areaResize.h"
#include "w_graficos/w_areaResizeControl.h"

#include "w_base/toStr.hpp"
#include "w_base/files.hpp"

#define ANCHO_APP	800
#define ALTO_APP	600

#define ALTO_BARRA	30

// Evitar que salga la consola
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

int main()
{
	// ---------------------------------------------------------------------------
	// Iniciar lienzo del fondo
	WLienzoJPEG fondo;
	{
		const bool cargado = fondo.leerContenidoFile ("../testdata/fondo.jpeg");
		if (false == cargado)
		{
			(WLienzo &)fondo = WLienzo::createWLienzo (ANCHO_APP, ALTO_APP, RGBA (0, 0, 0xFF));
		}

		// Y ponerle borde
		fondo.setBorder (RGBA (0xFF, 0xFF, 0xFF));
	}

	// ---------------------------------------------------------------------------
	// Cursor: intentar cargarlo de la carpeta de recursos
	WXLienzo cursor;
	const bool cargado = cursor.leerContenidoFile ("../testdata/cursor.xlz");
	if (false == cargado)
	{
		// Si no se consigue cargar, se pone por defecto este

		//
		// yx012345678
		// 0 #
		// 1 ##
		// 2 #o#
		// 3 #oo#
		// 4 #ooo#
		// 5 #oooo#
		// 6 #ooooo#
		// 7 #oooooo#
		// 8 #ooooooo#
		// 9 #o88o8###
		// 0 ## #o#
		// 1     #o#
		// 2      ##

		cursor.reserve (20, 20);
		cursor.setPixel (0, 0, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 1, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 1, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 2, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 2, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (2, 2, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 3, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 3, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (2, 3, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (3, 3, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 4, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 4, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (2, 4, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (3, 4, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (4, 4, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 5, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 5, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (2, 5, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (3, 5, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (4, 5, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (5, 5, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 6, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 6, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (2, 6, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (3, 6, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (4, 6, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (5, 6, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (6, 6, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 7, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 7, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (2, 7, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (3, 7, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (4, 7, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (5, 7, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (6, 7, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (7, 7, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 8, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 8, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (2, 8, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (3, 8, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (4, 8, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (5, 8, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (6, 8, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (7, 8, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (8, 8, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 9, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 9, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (2, 9, RGBA (0x88, 0x88, 0x88));
		cursor.setPixel (3, 9, RGBA (0x88, 0x88, 0x88));
		cursor.setPixel (4, 9, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (5, 9, RGBA (0x88, 0x88, 0x88));
		cursor.setPixel (6, 9, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (7, 9, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (8, 9, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (0, 10, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (1, 10, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (3, 10, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (4, 10, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (5, 10, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (6, 10, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (4, 11, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (5, 11, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (6, 11, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (7, 11, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (5, 12, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (6, 12, RGBA (0xFF, 0xFF, 0xFF));
		cursor.setPixel (7, 12, RGBA (0x00, 0x00, 0x00));

		cursor.setPixel (5, 13, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (6, 13, RGBA (0x00, 0x00, 0x00));
		cursor.setPixel (7, 13, RGBA (0x00, 0x00, 0x00));
	}

	// ---------------------------------------------------------------------------
	// Crear la aplicaci�n
	WApp wApp ("app", ANCHO_APP, ALTO_APP);
	wApp.setFondo (fondo);
	wApp.setCursor (cursor);

	{
		// --------------------------------------------------------------------------
		// Barra superior
		WControl controlBarra ("barra");
		controlBarra.setLienzo (WLienzo::createWLienzo (ANCHO_APP, ALTO_BARRA, RGBA ("0x63a889")));
		controlBarra.getLienzo().setBorder (RGBA (0xFF, 0xFF, 0xFF));
		controlBarra.setPosX (0);
		controlBarra.setPosY (0);

		// A�adir eventos
		class Barra
		{
		public:
			static void onLMouseDrag (const WEventoMouseDrag & evento)
			{
				evento.getRoot().setPosX (evento.getRoot().getPosX() + evento.getIncrementoPosX());
				evento.getRoot().setPosY (evento.getRoot().getPosY() + evento.getIncrementoPosY());
				evento.getRoot().rePosAndDim();
			}
		};
		controlBarra.setOnLMouseDrag (Barra::onLMouseDrag);

		// Insertar el control
		wApp.insertar (controlBarra);
	}

	{
		// --------------------------------------------------------------------------
		// Campo de texto nativo
		WTextControl textControlCoords ("text_area_coords");
		textControlCoords.setText ("Coordenadas del raton");
		textControlCoords.setPosX (15);
		textControlCoords.setPosY (45);

		// Insertar el control
		wApp.insertar (textControlCoords);
	}

	{
		// --------------------------------------------------------------------------
		// Campo de texto nativo
		WTextControl textControl ("text_area");
		textControl.setText ("Esto es un texto de prueba");
		textControl.setPosX (45);
		textControl.setPosY (75);

		textControl.resize (300, 50);

		// A�adir eventos
		class TextControl
		{
		public:
			static WTextControl & getWTextControl()
			{
				WTextControl* textControlOriginal = NULL;
				return TextControl::getWTextControl (*textControlOriginal);
			}
			static WTextControl & getWTextControl (WTextControl & iTextControlOriginal)
			{
				static WTextControl & textControlOriginal = iTextControlOriginal;
				return textControlOriginal;
			}
			static void onKeyDown (const WEventoKey & evento)
			{
				if (evento.key == 116)
				{
					const std::string & texto = ::leerContenidoFile ("../testdata/texto.txt");
					getWTextControl().setText (texto);
					getWTextControl().pintar();
				}
				else if (evento.key == 115)
				{
					getWTextControl().setText ("");
					getWTextControl().pintar();
				}
				else
				{
					getWTextControl().onKeyDown (evento.key);
				}
			}
			static void onKeyUp (const WEventoKey & evento)
			{
				getWTextControl().onKeyUp (evento.key);
			}
		};

		// Insertar el control
		const size_t iPos = wApp.insertar (textControl);
		TextControl::getWTextControl (wApp.obtenerAs<WTextControl> (iPos));

		// Tambi�n a�adirlos a la app
		wApp.setOnKeyDown (TextControl::onKeyDown);
		wApp.setOnKeyUp (TextControl::onKeyUp);
	}
	{
		// --------------------------------------------------------------------------
		// Mosca desplazable
		WControl controlMosca ("mosca");
		controlMosca.setLienzo (WLienzoBMP::getWLienzoBMPFromFile ("../testdata/mosca.bmp"));
		controlMosca.setPosX (100);
		controlMosca.setPosY (100);

		// A�adir eventos
		class Mosca
		{
		public:
			static void onXMouseDrag (const WEventoMouseDrag & evento)
			{
				const size_t newX = evento.getSelf().getPosX() + evento.getIncrementoPosX();
				if (newX >= 0 && newX <= evento.getRoot().getAncho() - evento.getSelf().getAncho())
				{
					evento.getSelf().setPosX (newX);
				}
				const size_t newY = evento.getSelf().getPosY() + evento.getIncrementoPosY();
				if (newY >= ALTO_BARRA && newY <= evento.getRoot().getAlto() - evento.getSelf().getAlto())
				{
					evento.getSelf().setPosY (newY);
				}
			}
			static void onLDownMouseOver (const WEventoMouse & evento)
			{
				evento.getSelf().setOnLMouseDrag (Mosca::onXMouseDrag);
				evento.getSelf().setOnMMouseDrag (NULL);
				evento.getSelf().setOnRMouseDrag (NULL);

				evento.getSelf().setOnLUpMouseOver (Mosca::onXUpMouseOver);
				evento.getSelf().setOnMUpMouseOver (NULL);
				evento.getSelf().setOnRUpMouseOver (NULL);

				evento.getSelf().setOnLDownMouseOver (NULL);
				evento.getSelf().setOnMDownMouseOver (NULL);
				evento.getSelf().setOnRDownMouseOver (NULL);
			}
			static void onMDownMouseOver (const WEventoMouse & evento)
			{
				evento.getSelf().setOnLMouseDrag (NULL);
				evento.getSelf().setOnMMouseDrag (Mosca::onXMouseDrag);
				evento.getSelf().setOnRMouseDrag (NULL);

				evento.getSelf().setOnLUpMouseOver (NULL);
				evento.getSelf().setOnMUpMouseOver (Mosca::onXUpMouseOver);
				evento.getSelf().setOnRUpMouseOver (NULL);

				evento.getSelf().setOnLDownMouseOver (NULL);
				evento.getSelf().setOnMDownMouseOver (NULL);
				evento.getSelf().setOnRDownMouseOver (NULL);
			}
			static void onRDownMouseOver (const WEventoMouse & evento)
			{
				evento.getSelf().setOnLMouseDrag (NULL);
				evento.getSelf().setOnMMouseDrag (NULL);
				evento.getSelf().setOnRMouseDrag (Mosca::onXMouseDrag);

				evento.getSelf().setOnLUpMouseOver (NULL);
				evento.getSelf().setOnMUpMouseOver (NULL);
				evento.getSelf().setOnRUpMouseOver (Mosca::onXUpMouseOver);

				evento.getSelf().setOnLDownMouseOver (NULL);
				evento.getSelf().setOnMDownMouseOver (NULL);
				evento.getSelf().setOnRDownMouseOver (NULL);
			}
			static void onXUpMouseOver (const WEventoMouse & evento)
			{
				evento.getSelf().setOnLDownMouseOver (Mosca::onLDownMouseOver);
				evento.getSelf().setOnMDownMouseOver (Mosca::onMDownMouseOver);
				evento.getSelf().setOnRDownMouseOver (Mosca::onRDownMouseOver);
			}

			static void onMoveMouseEnter (const WEventoMouse & evento)
			{
				WXLienzo cursor;
				const bool cargado = cursor.leerContenidoFile ("../testdata/puntero.xlz");
				evento.getRoot().setCursor (cursor);
			}
			static void onMoveMouseOut (const WEventoMouse & evento)
			{
				const WLienzo & cursorOriginal = getCursorOriginal (evento.getRoot());
				evento.getRoot().setCursor (cursorOriginal);
			}
			static WLienzo & getCursorOriginal (const WApp & app)
			{
				static WLienzo cursorOriginal = app.getCursor().getLienzo();
				return cursorOriginal;
			}
		};

		controlMosca.setOnMoveMouseEnter (Mosca::onMoveMouseEnter);
		controlMosca.setOnMoveMouseOut (Mosca::onMoveMouseOut);

		controlMosca.setOnLDownMouseOver (Mosca::onLDownMouseOver);
		controlMosca.setOnMDownMouseOver (Mosca::onMDownMouseOver);
		controlMosca.setOnRDownMouseOver (Mosca::onRDownMouseOver);

		Mosca::getCursorOriginal (wApp);

		// Insertar el control
		wApp.insertar (controlMosca);
	}
	{
		// --------------------------------------------------------------------------
		// Mosca desplazable
		WAreaResizeT<WControl> controlMoscaFija ("moscafija");
		controlMoscaFija.setLienzo (WLienzoBMP::getWLienzoBMPFromFile ("../testdata/mosca.bmp"));
		controlMoscaFija.setPosX (400);
		controlMoscaFija.setPosY (300);

		// Insertar el control
		wApp.insertar (controlMoscaFija);
	}
	{
		// --------------------------------------------------------------------------
		// Mosca desplazable
		WControl controlMoscaFijaAuto ("moscaAuto");
		controlMoscaFijaAuto.setLienzo (WLienzoBMP::getWLienzoBMPFromFile ("../testdata/mosca.bmp"));

		WAreaResizeControl areaResize ("moscaArea");
		areaResize.setPosX (450);
		areaResize.setPosY (50);

		// Insertar el controlMoscaFijaAuto en areaResize
		areaResize.insertar (controlMoscaFijaAuto);

		// A�adir eventos
		class Mosca
		{
		public:
			static void onXMouseDrag (const WEventoMouseDrag & evento)
			{
				const size_t newX = evento.getSelf().getPosX() + evento.getIncrementoPosX();
				if (newX >= 0 && newX <= evento.getRoot().getAncho() - evento.getSelf().getAncho())
				{
					evento.getSelf().setPosX (newX);
				}
				const size_t newY = evento.getSelf().getPosY() + evento.getIncrementoPosY();
				if (newY >= ALTO_BARRA && newY <= evento.getRoot().getAlto() - evento.getSelf().getAlto())
				{
					evento.getSelf().setPosY (newY);
				}
			}
			static void onLDownMouseOver (const WEventoMouse & evento)
			{
				evento.getSelf().setOnLMouseDrag (Mosca::onXMouseDrag);
				evento.getSelf().setOnMMouseDrag (NULL);
				evento.getSelf().setOnRMouseDrag (NULL);

				evento.getSelf().setOnLUpMouseOver (Mosca::onXUpMouseOver);
				evento.getSelf().setOnMUpMouseOver (NULL);
				evento.getSelf().setOnRUpMouseOver (NULL);

				evento.getSelf().setOnLDownMouseOver (NULL);
				evento.getSelf().setOnMDownMouseOver (NULL);
				evento.getSelf().setOnRDownMouseOver (NULL);
			}
			static void onMDownMouseOver (const WEventoMouse & evento)
			{
				evento.getSelf().setOnLMouseDrag (NULL);
				evento.getSelf().setOnMMouseDrag (Mosca::onXMouseDrag);
				evento.getSelf().setOnRMouseDrag (NULL);

				evento.getSelf().setOnLUpMouseOver (NULL);
				evento.getSelf().setOnMUpMouseOver (Mosca::onXUpMouseOver);
				evento.getSelf().setOnRUpMouseOver (NULL);

				evento.getSelf().setOnLDownMouseOver (NULL);
				evento.getSelf().setOnMDownMouseOver (NULL);
				evento.getSelf().setOnRDownMouseOver (NULL);
			}
			static void onRDownMouseOver (const WEventoMouse & evento)
			{
				evento.getSelf().setOnLMouseDrag (NULL);
				evento.getSelf().setOnMMouseDrag (NULL);
				evento.getSelf().setOnRMouseDrag (Mosca::onXMouseDrag);

				evento.getSelf().setOnLUpMouseOver (NULL);
				evento.getSelf().setOnMUpMouseOver (NULL);
				evento.getSelf().setOnRUpMouseOver (Mosca::onXUpMouseOver);

				evento.getSelf().setOnLDownMouseOver (NULL);
				evento.getSelf().setOnMDownMouseOver (NULL);
				evento.getSelf().setOnRDownMouseOver (NULL);
			}
			static void onXUpMouseOver (const WEventoMouse & evento)
			{
				evento.getSelf().setOnLDownMouseOver (Mosca::onLDownMouseOver);
				evento.getSelf().setOnMDownMouseOver (Mosca::onMDownMouseOver);
				evento.getSelf().setOnRDownMouseOver (Mosca::onRDownMouseOver);
			}

			static void onMoveMouseEnter (const WEventoMouse & evento)
			{
				WXLienzo cursor;
				const bool cargado = cursor.leerContenidoFile ("../testdata/puntero.xlz");
				evento.getRoot().setCursor (cursor);
			}
			static void onMoveMouseOut (const WEventoMouse & evento)
			{
				const WLienzo & cursorOriginal = getCursorOriginal (evento.getRoot());
				evento.getRoot().setCursor (cursorOriginal);
			}
			static WLienzo & getCursorOriginal (const WApp & app)
			{
				static WLienzo cursorOriginal = app.getCursor().getLienzo();
				return cursorOriginal;
			}
		};

		areaResize.setOnMoveMouseEnter (Mosca::onMoveMouseEnter);
		areaResize.setOnMoveMouseOut (Mosca::onMoveMouseOut);

		areaResize.setOnLDownMouseOver (Mosca::onLDownMouseOver);
		areaResize.setOnMDownMouseOver (Mosca::onMDownMouseOver);
		areaResize.setOnRDownMouseOver (Mosca::onRDownMouseOver);

		Mosca::getCursorOriginal (wApp);

		// Insertar el control en la app
		wApp.insertar (areaResize);
	}
	{
		// --------------------------------------------------------------------------
		// Vertical Scroll Control independiente
		WVScrollControl vScroll ("VScroll");
		vScroll.setPosX (ANCHO_APP - 10);
		vScroll.setPosY (ALTO_BARRA);
		vScroll.resize (10, ALTO_APP - ALTO_BARRA);

		class Scroll
		{
		public:
			static void onScrollUp (const WEventoScroll & evento)
			{
				static WControl & mosca = evento.getRoot().obtener ("mosca");
				if (mosca.getPosY() > 10 + ALTO_BARRA)
				{
					mosca.setPosY (mosca.getPosY() - 10);
					mosca.pintar();
				}
				else if (mosca.getPosY() > ALTO_BARRA)
				{
					mosca.setPosY (ALTO_BARRA);
					mosca.pintar();
				}
			}
			static void onScrollDown (const WEventoScroll & evento)
			{
				static WControl & mosca = evento.getRoot().obtener ("mosca");
				if (mosca.getPosY() + 10 < ALTO_APP - mosca.getAlto())
				{
					mosca.setPosY (mosca.getPosY() + 10);
					mosca.pintar();
				}
				else if (mosca.getPosY() < ALTO_APP - mosca.getAlto())
				{
					mosca.setPosY (ALTO_APP - mosca.getAlto());
					mosca.pintar();
				}
			}
		};

		vScroll.setOnScrollUp (Scroll::onScrollUp);
		vScroll.setOnScrollDown (Scroll::onScrollDown);

		// Insertar el control
		wApp.insertar (vScroll);
	}
	{
		// --------------------------------------------------------------------------
		// Bot�n de cerrar sobre la barra superior
		WControl controlCerrar ("cerrar");
		controlCerrar.setLienzo (WXLienzo::getWXLienzoFromFile ("../testdata/cerrar.xlz"));
		controlCerrar.setPosX (ANCHO_APP - controlCerrar.getLienzo().getAncho() - 10);
		controlCerrar.setPosY (10);

		// A�adir eventos
		class Cerrar
		{
		public:
			static void invertCursorRoot (const WEventoMouse & evento)
			{
				WApp & app = evento.getRoot();
				WControl & cursor = app.getCursor();
				cursor.getLienzo().invertColor();
				app.setCursor (cursor);
			}

			static void onMoveMouseOver (const WEventoMouse & evento)
			{
				invertCursorRoot (evento);
				evento.control.setOnMoveMouseOver (NULL);
			}
			static void onMoveMouseOut (const WEventoMouse & evento)
			{
				invertCursorRoot (evento);
				evento.control.setOnMoveMouseOver (Cerrar::onMoveMouseOver);
			}
			static void onRClicMouseOver (const WEventoMouse & evento)
			{
				evento.control.getLienzo().invertColor();
			}
			static void onLClicMouseOver (const WEventoMouse & evento)
			{
				evento.getRoot().stop();
			}
		};
		controlCerrar.setOnMoveMouseOver (Cerrar::onMoveMouseOver);
		controlCerrar.setOnMoveMouseOut (Cerrar::onMoveMouseOut);
		controlCerrar.setOnLClicMouseOver (Cerrar::onLClicMouseOver);
		controlCerrar.setOnRClicMouseOver (Cerrar::onRClicMouseOver);

		// Insertar el control
		wApp.insertar (controlCerrar);
	}

	{
		WAreaScrollControl areaScroll ("areaScroll");
		{
			// --------------------------------------------------------------------------
			// Bot�n de cerrar sobre la barra superior
			WControl imagen ("imagen");
			imagen.setLienzo (WLienzoBMP::getWLienzoBMPFromFile ("../testdata/imagen.bmp"));

			// A�adirlo al �rea
			areaScroll.insertar (imagen);
		}

		// Posicionar el control
		areaScroll.setPosX (100);
		areaScroll.setPosY (400);

		// Dimensionar el control (la parte visible)
		areaScroll.resize (210, 100);

		// Insertar el control
		wApp.insertar (areaScroll);
	}
	{
		// --------------------------------------------------------------------------
		// Eventos propios de la aplicaci�n
		class CosasAPP
		{
			class Cerrar
			{
			public:
				static void onLClicMouseOver (const WEventoMouse & evento)
				{
					evento.getRoot().stop();
				}
			};

		public:
			static AppFunctores crearVector()
			{
				AppFunctores ret;
				ret.add ("onKeyUp", &CosasAPP::onKeyUp);
				ret.add ("onMoveMouseOver", &CosasAPP::onMoveMouseOver);
				ret.add ("Cerrar::onLClicMouseOver", &CosasAPP::Cerrar::onLClicMouseOver);
				return ret;
			}
			static AppFunctores & GetFunctores()
			{
				static AppFunctores functores = CosasAPP::crearVector();
				return functores;
			}
			static void onWheelMouseOver (const WEventoMouseWheel & evento)
			{
				const int incr = (evento.direccion == WEventoMouseWheel::ABAJO) ? -10 : 10;
				evento.getRoot().getFondo().brightness (incr);
			}
			static void onKeyUp (const WEventoKey & evento)
			{
				switch (evento.key)
				{
				case 27:
				{
					evento.getRoot().stop();
				}
				break;
				case 49:	//< Presionar la tecla '1'
				{
					WApp & app = evento.getRoot();
					app.clear();
					app.loadXW ("../testdata/maquetacion.xw", CosasAPP::GetFunctores());
					CosasAPP::TextAreaCoordsActualizado (WEvento (app));
				}
				break;
				}
			}
			static WTextControl* actualizarTextAreaCoords (const WEvento & evento)
			{
				return & (evento.getRoot().obtenerAs<WTextControl> ("text_area_coords", true));
			}
			static WTextControl* & TextAreaCoordsActualizado (const WEvento & evento)
			{
				static WTextControl* c = NULL;
				c = CosasAPP::actualizarTextAreaCoords (evento);
				return c;
			}
			static void onMoveMouseOver (const WEventoMouse & evento)
			{
				static WTextControl* & textControl = CosasAPP::TextAreaCoordsActualizado (evento);
				textControl->setText (::toStr (evento.getSelf().getPunteroPos().posX) + " | " +
									  ::toStr (evento.getSelf().getPunteroPos().posY));
				textControl->pintar();
			}
		};
		// A�adirlos a la app
		wApp.setOnKeyUp (CosasAPP::onKeyUp);
		wApp.setOnWheelMouseOver (CosasAPP::onWheelMouseOver);
		wApp.setOnMoveMouseOver (CosasAPP::onMoveMouseOver);
	}

	// Iniciar la aplicaci�n
	wApp.start();

	// Finalizaci�n correcta
	return 0;
}
