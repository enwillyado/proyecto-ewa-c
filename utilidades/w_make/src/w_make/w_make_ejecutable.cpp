/*********************************************************************************************
 *	Name		: w_make_ejecutable.cpp
 *	Description	: Implementanci�n de las funciones para preparar ejecutable
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_make/w_make.h"

#include "w_sistema/w_sistema_execs.h"
#include "w_sistema/w_sistema_files.h"

#include <iostream>

WMake::Estado WMake::prepararExe()
{
	if (this->parametros.srcOrigen.size() > 0)
	{
		// Pasar de src a obj
		this->prepararObj();
	}

	const std::string & dirOrigen = this->parametros.dirOrigen;
	const std::string & dirBinDestino = this->parametros.dirBinDestino;

	{
		// Crear el directorio de destino de la librer�a
		const std::string & dirBinDestinoFinal = dirOrigen + dirBinDestino;
		std::cout << "+" << dirBinDestinoFinal << std::endl;
		WFiles::createDirPath (dirBinDestinoFinal);
	}

	std::string comando;
	comando += "g++ -s -o " + (dirOrigen + dirBinDestino + this->parametros.nombre + ".exe");

	// Inclusi�n de c�digo objeto propio
	const strvector & objOrigen = this->parametros.objOrigen;
	for (size_t i = 0; i < objOrigen.size(); i++)
	{
		const std::string & itemIn = objOrigen[i];
		comando += " " + (dirOrigen + itemIn);
	}

	// Carpetas de inclusi�n de librer�as
	comando += " -L " + (dirOrigen);
	const strvector & carpetasInclusionLib = this->parametros.carpetasInclusionLib;
	for (size_t i = 0; i < carpetasInclusionLib.size(); i++)
	{
		const std::string & carpetaInclusionLib = carpetasInclusionLib[i];
		comando += " -L " + (dirOrigen + carpetaInclusionLib);
	}

	// Inclusi�n de librer�as
	const strvector & libOrigen = this->parametros.libOrigen;
	for (size_t i = 0; i < libOrigen.size(); i++)
	{
		const std::string & itemIn = libOrigen[i];
		comando += " -l" + (itemIn);
	}

	SistemaExecsSimple exec;
	exec.setComando (comando + STDERR_REDIRECT);
	std::cout << "[]" << exec.getComando() << std::endl;
	exec.exeComando();

	const std::string & salida = exec.getSalidaComando();
	std::cout << "[]>" << salida << std::endl;

	return WMake::CORRECTO;
}
