/*********************************************************************************************
 * Name			: w_web_catalogo.cpp
 * Description	: Implementaci�n del mecanismo para responder a un determinado
 *					servicio que ejecutar� el servidor usando polimorfismo
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/

#include "w_web/w_web_catalogo.h"

// Mapa de dispachers est�ticos (donde se van guardando ellos mismos)
BasicSrvServicesCatalogo::MapaServicios BasicSrvServicesCatalogo::mapaServicios;
IndiceServiciosExtendidos BasicSrvServicesCatalogo::indiceServiciosExtendidos;

// ----------------------------------------------------------------------------
// Gesti�n individual de los servicios
/**
 *	Obtiene e puntero al servicio que tenga el identificador dado
 *	\param	evento	[in]	identificador del servicio que se desea obtener
 *	\retval	el puntero al servicio con el identificador dado
 *		o	NULL si no existe un servicio catalogado con ese ID intentando resolverlo en el �ndice
**/
const BasicSrvService* BasicSrvServicesCatalogo::getService (const std::string & id)
{
	const std::string & idAbs = BasicSrvServicesCatalogo::getIdAbs (id);
	return BasicSrvServicesCatalogo::getServiceAbs (idAbs);
}

/**
 *	Obtiene el puntero al servicio que tenga el identificador dado
 *	\param	evento	[in]	identificador del servicio que se desea obtener
 *	\retval	el puntero al servicio con el identificador dado
 *		o	NULL si no existe un servicio catalogado con ese ID intentando resolverlo en el �ndice
**/
const BasicSrvService* BasicSrvServicesCatalogo::getServiceAbs (const std::string & id)
{
	if (id != "")
	{
		BasicSrvServicesCatalogo::MapaServicios::iterator itr = BasicSrvServicesCatalogo::mapaServicios.find (id);
		if (itr != BasicSrvServicesCatalogo::mapaServicios.end())
		{
			return itr->second;
		}
		const std::string & idBasic = BasicSrvServicesCatalogo::indiceServiciosExtendidos.resolve (id);
		if (idBasic != "")	// NOTA: no se soporta �ndices de servicios vac�os
		{
			itr = BasicSrvServicesCatalogo::mapaServicios.find (idBasic);
			if (itr != BasicSrvServicesCatalogo::mapaServicios.end())
			{
				return itr->second;
			}
		}
		else
		{
		}
	}
	return NULL;
}

/**
 *	Inserta un servicio en el cat�logo (internamente hace una copia que autogestionar� el cat�logo)
 *	\param	service	[in]	referencia constante al servicio que se va a a�adir
 *	\retval	TRUE si se ha incluido, o FALSE si ya exist�a y se deja como estaba
**/
bool BasicSrvServicesCatalogo::addService (const BasicSrvService & service)
{
	return BasicSrvServicesCatalogo::addService (&service);
}

/**
 *	Inserta un servicio en el cat�logo (internamente hace una copia que autogestionar� el cat�logo)
 *	\param	ptr	[in]	puntero a un servicio b�sico que se quiera a�adir (ojo, como copia), o NULL para que devuelva siempre FALSE
 *	\retval	TRUE si se ha incluido, o FALSE si ya exist�a y se deja como estaba
**/
bool BasicSrvServicesCatalogo::addService (const BasicSrvService* ptr)
{
	if (ptr != NULL)
	{
		BasicSrvService* otro = ptr->newInstance();
		const std::string & id = otro->getServicioName();
		const std::string & idAbs = BasicSrvServicesCatalogo::getIdAbs (id);
		MapaServicios::const_iterator itr = BasicSrvServicesCatalogo::mapaServicios.find (idAbs);
		if (itr == BasicSrvServicesCatalogo::mapaServicios.end())
		{
			BasicSrvServicesCatalogo::mapaServicios [idAbs] = otro;
		}
		if (IndiceServiciosExtendidos::esExtendido (idAbs) == true)
		{
			BasicSrvServicesCatalogo::insertarExtendido (idAbs);
		}
		return true;
	}
	return false;
}

/**
 *	Borra del cat�logo el servicio dado
 *	\param	ptr	[in]	puntero a un servicio b�sico que se quiera borrar (ojo, la copia), o NULL para que devuelva siempre FALSE
 *	\retval	TRUE si se ha incluido, o FALSE si ya exist�a y se deja como estaba
**/
bool BasicSrvServicesCatalogo::delService (const BasicSrvService & service)
{
	return BasicSrvServicesCatalogo::delService (&service);
}
/**
 *	Borra del cat�logo el servicio dado
 *	\param	ptr	[in]	puntero a un servicio b�sico que se quiera borrar (ojo, la copia), o NULL para que devuelva siempre FALSE
 *	\retval	TRUE si se ha incluido, o FALSE si ya exist�a y se deja como estaba
**/
bool BasicSrvServicesCatalogo::delService (const BasicSrvService* ptr)
{
	if (ptr != NULL)
	{
		const std::string & id = ptr->getServicioName();
		return BasicSrvServicesCatalogo::delService (id);
	}
	return false;
}

/**
 *	Borra del cat�logo el servicio con el identificador dado
 *	\param	idservice	[in]	identificador �nico del servicio a borrar
 *	\retval	TRUE si se ha borrado, o FALSE si no exist�a
**/
bool BasicSrvServicesCatalogo::delService (const std::string & id)
{
	const std::string & idAbs = BasicSrvServicesCatalogo::getIdAbs (id);
	MapaServicios::iterator itr = BasicSrvServicesCatalogo::mapaServicios.find (idAbs);
	if (itr != BasicSrvServicesCatalogo::mapaServicios.end())
	{
		delete itr->second;
		BasicSrvServicesCatalogo::mapaServicios.erase (itr);
		if (IndiceServiciosExtendidos::esExtendido (idAbs) == true)
		{
			BasicSrvServicesCatalogo::borrarExtendido (idAbs);
		}
		return true;
	}
	return false;
}

/**
 *	Obtiene el identificador �nico del servicio dado
 *		B�sicamente se trata de la URI del servicio terminando siempre en "/", en min�sculas y sin los par�metros GET
 *	\param	realid	[in]	identificador del servicio (puede contener parte de variables GET que se ignorar�
 *	\retval	el identificador �nico del servicio dado
**/
std::string BasicSrvServicesCatalogo::getIdAbs (const std::string & realid)
{
	const std::string & id = realid.substr (0, realid.find_first_of ('?'));
	if (id == "")
	{
		return "/";
	}
	if (id[id.size() - 1] != '/')
	{
		return BasicSrvServicesCatalogo::tolower (id + "/");
	}
	return BasicSrvServicesCatalogo::tolower (id);
}
/**
*	Devuelve una cadena con la cadena dada en min�sculas
 *	\param	str	[in]	cadena dada
 *	\retval	copia de la frase, toda en min�sculas
**/
std::string BasicSrvServicesCatalogo::tolower (std::string str)
{
	for (size_t i = 0; i < str.size(); i++)
	{
		str[i] = ::tolower (str[i]);
	}
	return str;
}



// ----------------------------------------------------------------------------
// Gesti�n del grupo de servicios
/**
 *	Obtiene el mapa con todos los servicios que contenga el cat�logo
 *	\retval	TRUE si se ha borrado, o FALSE si no exist�a
**/
const BasicSrvServicesCatalogo::MapaServicios & BasicSrvServicesCatalogo::getAllServices()
{
	return BasicSrvServicesCatalogo::mapaServicios;
}
/**
 *	Borra todos los servicios que contenga el cat�logo
 *	\retval	TRUE si se han borrado correctamente, o FALSE en caso contrario
**/

bool BasicSrvServicesCatalogo::delAllServices()
{
	MapaServicios::const_iterator itr = BasicSrvServicesCatalogo::mapaServicios.begin();
	while (itr != BasicSrvServicesCatalogo::mapaServicios.end())
	{
		const std::string & idAbs = BasicSrvServicesCatalogo::getIdAbs (itr->first);
		if (IndiceServiciosExtendidos::esExtendido (idAbs) == true)
		{
			BasicSrvServicesCatalogo::borrarExtendido (idAbs);
		}
		delete itr->second;
		itr++;
	}
	BasicSrvServicesCatalogo::mapaServicios.clear();
	return true;
}

/**
 *	Obtiene el n�mero de elementos dentro del cat�logo
 *	\retval el n�mero de elementos dentro del cat�logo
**/
size_t BasicSrvServicesCatalogo::size()
{
	return BasicSrvServicesCatalogo::mapaServicios.size();
}

// ----------------------------------------------------------------------------
// Gesti�n de los servicios extendidos
/**
 *	Inserta en el �ndice de Servicios Extendidos el Servicio con identificador dado
 *	\param	idservice	[in]	identificador del Servicio (absoluto)
**/
void BasicSrvServicesCatalogo::insertarExtendido (const std::string & id)
{
	indiceServiciosExtendidos.insert (id);
}
/**
 *	Borra del �ndice de Servicios Extendidos el servicio con identificador dado
 *	\param	idservice	[in]	identificador del Servicio (absoluto)
**/
void BasicSrvServicesCatalogo::borrarExtendido (const std::string & id)
{
	indiceServiciosExtendidos.erase (id);
}
