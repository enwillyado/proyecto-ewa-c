/*********************************************************************************************
 *	Name		: w_red_proxy.h
 *	Description	: Clase base para implementar un proxy de red
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_PORXY_H_
#define _W_RED_PORXY_H_

#include "w_base/interfaces/iStartable.h"

#include <string>

class Proxy : public IStartable
{
public:
	Proxy (const std::string & remoteHost, const int remotePort);
	virtual bool open (const int localPort);

protected:
	std::string host;
	int port, localPort;
};


#endif //_W_RED_PORXY_H_
