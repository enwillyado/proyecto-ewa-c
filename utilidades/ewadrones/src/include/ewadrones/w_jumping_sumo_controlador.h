/*********************************************************************************************
 * Name			: w_jumping_sumo_controlador.h
 * Description	: Clase para el controloador del JUMPING SUMO
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_JUMPING_SUMO_CONTROLADOR_H_
#define _W_JUMPING_SUMO_CONTROLADOR_H_

#include "w_red/w_red_cliudp.h"
#include "w_sistema/w_sistema_thread.h"

#include "w_graficos/w_lienzo.h"

#include <cstring>				//< NULL
#include <string>

// FWD
class EWadrones;

class JumpingSumoControlador
{
public:
	JumpingSumoControlador (EWadrones* = NULL);
	void setPtrArDrone (EWadrones*);
	void init();

	void start();
	void stop();

	void setAcelerador (const short);
	void setVolante (const short);
	short getAcelerador() const;
	short getVolante() const;

	void defrente();
	void atras();
	void derecha();
	void izquierda();

	void estabilizar();

	// Manejadores (suscribirse o reimplementar)
	virtual void onNewLienzo();
	virtual void onNewImageJPEG (const std::string & jpegData);
	virtual void onBateriaCambia (const unsigned char nuevoValor);

	typedef void (*OnNewLienzo) (const WLienzo &);
	void setOnNewLienzo (OnNewLienzo cbOnNewLienzo);

	typedef void (*OnNewImageJPEG) (const std::string &);
	void setOnNewImageJPEG (OnNewImageJPEG cbOnNewImageJPEG);

	typedef void (*OnBateriaCambia) (const unsigned char);
	void setOnBateriaCambia (OnBateriaCambia cbOnBateriaCambia);

protected:
	void enviarMensaje (const std::string &);
	static void initControlUDP (const WThread &);
	static void initEmisor (const WThread &);

	void procesarMensaje (const std::string & mensaje);

	// Lienzo donde va a dejar los fotogramas
	WLienzo lienzo;
	OnNewLienzo cbOnNewLienzo;
	OnNewImageJPEG cbOnNewImageJPEG;
	OnBateriaCambia cbOnBateriaCambia;

protected:
	// �rea de datos
	short acelerador;
	short volante;

private:
	ClienteUDP clienteComandos;
	EWadrones* arDrone;
	unsigned int numeroSecuenciaSyn, numeroSecuenciaAck;
	WThread hiloComandos;
	WThread hiloEmisor;
};

#endif // _W_JUMPING_SUMO_CONTROLADOR_H_
