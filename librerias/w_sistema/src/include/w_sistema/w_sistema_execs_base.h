/*********************************************************************************************
 *	Name		: w_sistema_execs_base.h
 *	Description	: Clase base para llamar a un comando
 *	Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_SISTEMA_EXECS_BASE_H_
#define _W_SISTEMA_EXECS_BASE_H_

#include <string>

#define STDERR_REDIRECT " 2>&1"			// to redirect stderr to a symbolic file

#ifdef WIN32
#define COMADO_CMD	"cmd"
#else
#define COMADO_CMD	"sh"
#endif

class SistemaExecsBase
{
public:
	enum Errores
	{
		SIN_ERROR,
		SIN_SELECCIONAR,
		FINALIZADO,

		ERROR_PIPES,
		ERROR_CREAR_PROCESO,

		ERROR_ENVIAR_AL_HIJO,
		ERROR_RECIBIR_DEL_HIJO,
	};

	static const int BUFFER_CMD_SIZE = 9510;

public:
	// Constructor
	SistemaExecsBase();

	// Ajustar
	virtual void setComando (const std::string & valor);
	virtual const std::string & getComando() const;

	// Inicio
	virtual Errores exeComando() = 0;
	virtual Errores getLastError() const;

	// Postmortem
	virtual const std::string & getSalidaComando() const;
	virtual int getLastExecStatus() const;

public:
	// Funci�n para mostrar la salida del proceso en directo
	typedef void (*FuncionChild) (const std::string &);
	FuncionChild funcionChild;

protected:
	std::string comando;
	mutable Errores lastError;
	mutable std::string salidaGenerada;
	int lastExecStatus;
};

#endif // _W_SISTEMA_EXECS_BASE_H_
