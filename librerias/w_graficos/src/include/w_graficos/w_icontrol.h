/*********************************************************************************************
 * Name			: w_icontrol.h
 * Description	: Interfaz para los controles con parte gr�fica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_ICONTROL_H_
#define _W_ICONTROL_H_

// FWD
class WLienzo;

class IWControl
{
public:
	virtual size_t getAlto() const = 0;
	virtual size_t getAncho() const = 0;

	virtual size_t getPosX() const = 0;
	virtual size_t getPosY() const = 0;

	// Accesores
	virtual WLienzo & getLienzo() = 0;
	virtual const WLienzo & getLienzo() const = 0;
	virtual void setLienzo (const WLienzo & lienzo) = 0;

	// Modificadores
	virtual void setPos (const size_t posX, const size_t posY) = 0;
	virtual void setPosX (const size_t posX) = 0;
	virtual void setPosY (const size_t posY) = 0;

	// Manteniendo contenido (redimensionar)
	virtual void resize (const size_t ancho, const size_t alto) = 0;

	// Cambiar tama�o (recortar/alargar)
	virtual void setSize (const size_t ancho, const size_t alto) = 0;
};

#endif
