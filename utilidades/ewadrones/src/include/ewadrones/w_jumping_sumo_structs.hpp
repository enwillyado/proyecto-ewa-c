/*********************************************************************************************
 * Name			: w_jumping_sumo_structs.hpp
 * Description	: Estructuras internas usadas por el JUMPING SUMO
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_JUMPING_SUMO_STRUCTS_H_
#define _W_JUMPING_SUMO_STRUCTS_H_

#include <inttypes.h>

#define ACK   1
#define SYNC  2
#define IMAGE 3
#define IOCTL 4

#pragma pack(push)
#pragma pack(1)
struct header
{
	uint8_t type;  /* the main protocol type/category: ACK, SYNC, IMAGE, IOCTL */
	uint8_t ext;   /* extension of this type*/
	uint8_t seqno; /* sequence number for this type */
	uint16_t size; /* packet-size including header */
	uint16_t unk;  /* unknown 16bit always 0 in the analyzed data */
};

/* packet base - has a header */
struct packet
{
	struct header head;

	packet (uint8_t t, uint8_t e, uint8_t s, uint16_t size)
	{
		const header c = {t, e, s, size, 0};
		head = c;
	}
};

/* synchronoziation timestamp since device has booted in nanoseconds
 * bi-directional and acknowledged from the other side acknowledge is
 * done with setting head.ext = 1
 *
 * Seems to be send by the device at least before another transaction
 * is done, otherwise with a delay of 500msecs
 */
struct sync : public packet
{
	uint32_t seconds;
	uint32_t nanoseconds;

	sync (uint8_t seq, uint32_t sec, uint32_t nsec) :
		packet (SYNC, 0, seq, sizeof (*this)),
		seconds (sec), nanoseconds (nsec)
	{
	}
};

/* a packet of type sync used to move forward or backward and to turn the sumo */
struct move : public packet
{
	uint8_t b[4];
	const uint8_t active; /* if speed or turn is non-zero, this field goes to one */
	const int8_t  speed; /* -127 - 127 */
	const int8_t  turn;  /* -64 - 64 */

	move (uint8_t seq, uint8_t a, int8_t s, int8_t t) :
		packet (SYNC, 10, seq, sizeof (*this)),
		active (a), speed (s), turn (t)
	{
		b[0] = 0x03;
		b[1] = b[2] = b[3] = 0;
	}
};


/* acknowledge an ioctl packet */
struct ack : public packet
{
	uint8_t seqno; /* ioctl seqno to be acknowledged */

	ack (uint8_t ext, uint8_t seq, uint8_t confirm) :
		packet (ACK, ext, seq, sizeof (*this)),
		seqno (confirm)
	{}
};


struct ioctl_packet : public packet
{
	uint8_t flags; /* I used flags to name this field as I saw bitfields being used for some exchanges */
	uint8_t type;  /* type and func are used to indicate what to tell or instruct with this packet and indicate which data-payload is send */
	uint8_t func;  /* func or index */
	uint8_t unk;   /* unknown */

	ioctl_packet (uint8_t seq, uint16_t size, uint8_t t, uint8_t fu, uint8_t flags = 0) :
		packet (IOCTL, 11, seq, size), /* ext is always set to 11 for outgoing packets */
		flags (flags), type (t), func (fu), unk (0) /* unknown always set to 0 */
	{}
};

struct image : public packet
{
	uint16_t frame_number;                         /* absolute frame-name */
	uint16_t unk0;                                 /* unknown */
	uint8_t unk1;                                  /* unknown */
	const char* const getJpeg() const
	{
		return ((char*)this) + sizeof (*this);
	}
	const std::string getJpegAsString() const
	{
		const std::string ret (this->getJpeg(), this->head.size - sizeof (*this));
		return ret;
	}
};

#pragma pack(pop)

#endif // _W_JUMPING_SUMO_STRUCTS_H_
