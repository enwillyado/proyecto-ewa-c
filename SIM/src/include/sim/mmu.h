#ifndef _MMU_H_
#define _MMU_H_

#include "sim/ids.h"
#include "sim/mapeador.h"

class MMU
{
public:
	// constructor
	MMU();
	~MMU();

	void iniciar();

	void funcionDeEjecucion (const bool Z, const bool U, const bool V, const bool W);

	// acceso a los registros: bit del Bus de Direcciones que apunta el Puntero Actual
	const bool dame();
	void toma (const bool);
	void escribirMemoria (const Direccion, const bool que);
	const bool leer_de_Memoria (const Direccion);

	const bool dameSemiInstruccion(); // la que apunta el PC e incrementa PC

	// acceso a los registros: p�gina actual de puntero actual
	const bool dame0();
	const bool dame1();
	const bool dame2();
	const bool dame3();
	const bool dame4();
	const bool dame5();
	const bool dame6();
	const bool dame7();

	// escritura en los registros: p�gina actual de puntero actual
	void toma0 (const bool);
	void toma1 (const bool);
	void toma2 (const bool);
	void toma3 (const bool);
	void toma4 (const bool);
	void toma5 (const bool);
	void toma6 (const bool);
	void toma7 (const bool);

protected:
	void actualPaginaA (const bool);
	void actualPuntreroA (const bool);

	void PCToActual();
	void actualToPC();
	void seleccionaPagina (int);
	void seleccionaPuntro (int);

	void retrocedePagina();
	void avanza_laPagina();

	void retrocedePuntro();
	void avanza_elPuntro();

	void decrementaPuntero();
	void incrementaPuntero();

	void incrementaPunteroPC();

private:
	MAP* laMap;
	unsigned int pagina_actual;
	unsigned int puntro_actual;
	/**************************
	 * Punteros
	 *
	 **************************/
	bool punteros[PUNTROS][PAGINAS][COMPONT];	// los punteros QRST y Lx van todos juntinos
	bool PC[PAGINAS][COMPONT];					// el puntero PC va por libre
};

#endif	//_MMU_H_
