/*********************************************************************************************
 *	Informacion de la DLL
 *                      
 *  \autor		Marcos Sanchez Iglesias
 *  \version	20081024
 *  \copyright	(C) 2008 DATISA (http://www.datisa.es)
 ********************************************************************************************/

#include "dencryptc.h"

/// Version de la libreria
const char g_EncryptVersion[] = "1.0.0.0";


/// Informacion de algoritmos disponibles
ENCRYPT_API    const EncryptAlgorithmInfo   g_EncryptAlgorithmInfo[] =
{
   { 1, "Sha1"    },
   { 2, "Aes"     },
   { 3, "Base 64" },
   { 4, "Des"     },
   { 5, "Md5"     },   
   { 6, "Datisa"  },   
};


/// Numero algoritmos disponibles
ENCRYPT_API    const int g_EncryptNumAlgorithms = sizeof(g_EncryptAlgorithmInfo) / sizeof(g_EncryptAlgorithmInfo[0]);