/*********************************************************************************************
 *	Name		: _w_web_server.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_WEB_SERVER_PRAGMALIB_H_
#define _W_WEB_SERVER_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_base/_pragmalib_base.h"

// Propia librer�a: (para no autoengancharla si es una dll)
#pragma comment(lib, "w_web_server" FIN_LIB)

// Parte privada
#include "w_red/_w_red_server.pragmalib.h"
#include "w_codificacion/_w_codificacion.pragmalib.h"

#endif

#undef _W_WEB_SERVER_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_WEB_SERVER_PRAGMALIB_H_
