/*********************************************************************************************
 *	Name		: _w_red_client_udpbroadcast_cmd_ejemplo.cpp
 *	Description	: Prueba de ejemplo de un Cliente UDP haciendo broadcast por un puerto dado
 *					 (o uno por defecto)
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_red_client_udpbroadcast_cmd_ejemplo.pragmalib.h"

#ifdef WIN32
#define NOMBRE_DE_SERVIDOR_DEFECTO "255.255.255.255"
#else
#define NOMBRE_DE_SERVIDOR_DEFECTO "0.0.0.0"
#endif

#define PUERTO_EN_SERVIDOR_DEFECTO "52013"

#include <iostream>   // std::ios, std::istream, std::cout

#include "w_red/w_red_cliudp.h"			// para tener el "ClienteUDP"

int mainClient (int argc, char* argv[])
{
	// Arrancar el cliente TCP
	ClienteUDP cliente;
#ifdef _DEBUG
	std::cout << "Generando un 'ClienteUDP'" << std::endl;
#endif
	const std::string & host = NOMBRE_DE_SERVIDOR_DEFECTO;
	const std::string & port = (argc > 1) ? argv[1] : PUERTO_EN_SERVIDOR_DEFECTO;
	bool opened = cliente.openSocket (host, port);
	if (opened && true == cliente.isOpenSocket())
	{
		const std::string & mensajeEnviar = "hola";
		std::string respuesta;
		bool correctoEnviar = cliente.enviar (mensajeEnviar);
		if (correctoEnviar)
		{
#ifdef _DEBUG
			std::cout << "OK: enviar()" << std::endl;
#endif
			std::cout << "Mensaje enviado: " << mensajeEnviar << std::endl;
		}
		else
		{
			std::cerr << "Error: enviar() " << cliente.getLastError() << " = " << cliente.getLastErrorStr() << std::endl;
		}

		// Por desgracia, actualmente los clientes broadcast no pueden recibir una respuesta
		/*
		Protocolo mensajeRecibir;
		bool correctoRecibir = cliente.recibir (mensajeRecibir);
		if (correctoRecibir)
		{
		#ifdef _DEBUG
			std::cout << "OK: enviar()" << std::endl;
		#endif
			std::cout << "Mensaje recibido: " << mensajeRecibir.genMsg() << std::endl;
		}
		else
		{
			std::cerr << "Error: enviar() " << cliente.getLastError() << " = " << cliente.getLastErrorStr() << std::endl;
		}
		*/

		bool closeCorrecto = cliente.closeSocket();
		if (true == closeCorrecto)
		{
#ifdef _DEBUG
			std::cout << "OK: closeSocket()" << std::endl;
#endif
		}
		else
		{
			std::cerr << "Error: closeSocket() " << cliente.getLastError() << " = " << cliente.getLastErrorStr() << std::endl;
			return 2;
		}
	}
	else
	{
		std::cerr << "Error: isOpenSocket() " << cliente.getLastError() << " = " << cliente.getLastErrorStr() << std::endl;
		return 1;
	}
	return 0;
}

int main (int argc, char* argv[])
{
	int ret;
	while (true)
	{
		ret = ::mainClient (argc, argv);
		std::cout << "Presione una tecla para continuar; CONTROL+C para salir" << std::endl;
		std::cin.get();
	}
	return ret;
}
