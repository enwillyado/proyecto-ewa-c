/*********************************************************************************************
 *	Name		: _w_red_clitcp.h
 *	Description	: Cliente que se conecta por TCP a un ServidorTCP
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_CLITCP_H_
#define _W_RED_CLITCP_H_

#include "w_red/w_red_clievent.h"

class ClienteTCP
{
public:
	enum Errores
	{
		SIN_ERROR = 0,

		// Estados siempre correctos del socket:
		SOCKET_ACTIVO = 0,
		SOCKET_DESCONECTADO = 0,	//< el socket est� desconectado (se cerr� bien con un "closesocket()")
		RESPUESTA_CORRECTA = 0,

		// Estados del socket:
		SOCKET_DISPONIBLE = -1,			//< el socket ya est� siendo usado (cuando se intenta abrir con un socket ya abierto sin cerrarlo antes)
		//								contra una IP y PUERTO iguales
		//								ES UN "ERROR RECUPERABLE", llamar a "basicErrorSocket()" lo convierte en "SOCKET_ACTIVO";
		SOCKET_CONECTANDO = -2,			//< se ha comenzado a conectar;
		//								ES UN "ERROR RECUPERABLE", llamar a "basicErrorSocket()" lo convierte en "SOCKET_ACTIVO";
		SOCKET_DESCONECTANDO = -3,		//< se ha comenzado a desconectar;
		//								ES UN "ERROR RECUPERABLE", llamar a "basicErrorSocket()" lo convierte en "SOCKET_ACTIVO"
		SOCKET_SIN_CAMBIOS = -4,		//< el socket ya estaba desconectado (nunca se abri� o se hab�a cerrado ya bien con "close()")
		//								ES UN "ERROR RECUPERABLE", llamar a "basicErrorSocket()" lo convierte en "SOCKET_ACTIVO"
		SOCKET_ESPERANDO_RESPUESTA = 9,	//< se ha enviado un mensaje, y se est� esperando una respuesta
		SOCKET_RESET_BY_PEER = 10 ,		//< la conexi�n se ha reiniciado
		SOCKET_ERROR_FATAL = 11,		//< el socket est� desconectado debido a un error irrecuperable

		// Errores de conexi�n:
		SOCKET_USADO = 1,				//< el socket ya est� siendo usado (cuando se intenta abrir con un socket ya abierto sin cerrarlo antes)
		//									contra una IP o PUERTO diferentes
		SOCKET_LIBERADO = 2,			//< el socket ya no est� siendo usado (se cerr� desde otro hilo)

		// Errores al enviar por un socket:
		SOCKET_ERROR_ENVIAR_DESC = 12,			//< se ha intentado enviar un mensaje en un socket sin conectar
		SOCKET_ERROR_AL_ENVIAR_MENSAJE = 19,	//< no se ha podido enviar un mensaje en un socket aparentemente conectado

		// Errores al recibir por un socket:
		SOCKET_RECV_TIMEOUT = 13,		//< no se ha recibido un mensaje en el lapso de tiempo indicado
		SOCKET_RECV_TIMEOUT_ERROR = 14,	//< ha fallado la recepci�n del mensaje con el tiempo de espera indicado
		MENSAJE_MAL_FORMADO = 15,

		// Errores al cerrar un socket:
		ERROR_AL_LIBERAR_WINSOCK = 16,	//< no se puede liberar las librer�as WINSOCK (solo en WINDOWS)

		// Errores al abrir un socket:
		ERROR_AL_TRANSFORMAR_PUERTO = 17,	//< no se puede obtener el char* con el puerto dado
		ERROR_AL_TRADUCIR_DNS = 18,			//< no se puede obtener la informaci�n dns del host de destino
		ERROR_AL_CREAR_SCKT = 20,			//< no se puede crear un nuevo socket en el sistema
		ERROR_AL_CONNECT_SCKT = 21,			//< no se puede conectar el socket al destino solicitado con la informaci�n indicada
		ERROR_BLOCKING_SCKT = 22,			//< no se puedo indicar al socket que pase a modo no-bloqueante (para habilitar el timeout)
		ERROR_NONBLOCKING_SCKT = 23,		//< no se puedo indicar al socket que pase a modo bloqueante (para habilitar el timeout)
		TIMEOUT_AL_CONNECT_SCKT = 24,		//< no se puede conectar el socket al destino solicitado dentro del timeout indicado
		ERROR_INTERNO_SCKT = 25,			//< no se puede crear un socket con la informaci�n solicitada
		ERROR_HACKER = 26,					//< se ha dado un caso que no se contempla bajo ning�n concepto conocido
		ERROR_DESCONOCIDO = 27,				//< no se puede concretar m�s el error
	};
	ClienteTCP();
	~ClienteTCP();

	// Env�o a alto nivel de los mensajes:
	bool enviar_recibir (const std::string & mensajeEnviar, std::string & respuesta, unsigned int segundosEsperados = 0);

	// Control de errores (public)
	Errores getLastError() const;
	std::string getLastErrorStr() const;

	// Observadores
	int getPort() const;
	std::string getPortStr() const;
	std::string getHost() const;

	// Static Utils
	static std::string toStr (Errores);

public:
	//---------------------------------------------------------------------------
	// Gesti�n del socket que mantiene la conexi�n TCP:
	bool openSocket (const std::string & host, unsigned int port, unsigned int segundosEsperados = 0);
	bool openSocket (const std::string & host, const std::string & port, unsigned int segundosEsperados = 0);
	bool isOpenSocket() const;
	bool reOpenSocket();
	bool closeSocket();

public:
	//---------------------------------------------------------------------------
	// Env�o a bajo nivel de mensajes
	bool enviar (const std::string & mensaje);
	bool recibir (std::string & mensaje, unsigned int segundosEsperados = 0);

protected:
	//---------------------------------------------------------------------------
	// Env�o a bajo nivel de mensajes (protected/private)
	bool sendMsgSocket (const std::string & mensaje);
	bool recvMsgSocket (std::string & mensaje, unsigned int segundosEsperados = 0);

	//---------------------------------------------------------------------------
	// Gesti�n de eventos recibidos en la capa del socket
public:
	bool setCallBack (void (*) (ClienteTCP &), void* callBackObj = NULL);
	void* getCallBackObj() const;
	EventoClienteTCP getEvento();
	size_t getNumEventosPendintes();

private:
	ColaEventosClienteTCP colaEventos;
	void analizaEvento (const EventoClienteTCP &);
	void (*eventCallBack) (ClienteTCP &);
	void* callBackObj;					//< objeto axiliar para el callback.

protected:
	// Control de errores (protected):
	void obtainSysLastErrorSocket();
	Errores basicErrorSocket();
	Errores lastError;

private:
	// �rea de datos (private)
	std::string hostActive;
	std::string portActive;
	int sockfd;					//< descriptor del sockets
};


#endif //_W_RED_CLITCP_H_
