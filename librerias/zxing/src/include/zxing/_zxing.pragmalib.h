/*********************************************************************************************
*	Name		: _zxing.pragmalib.h
*	Description	: Librer�as necesarias para enlazar con la libre�a ZXING
* Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _ZXING_PRAGMALIB_H_
#define _ZXING_PRAGMALIB_H_

#ifdef _MSC_VER

#include "w_base/_pragmalib_base.h"

// Parte privada
#pragma comment(lib, "zxing" FIN_LIB)

//#pragma comment(lib, "iconv_a" SIMPLE_END_LIB)	//< �?�?�?
#pragma comment(lib, "iconv" SIMPLE_END_LIB)

#endif

#undef _ZXING_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _ZXING_PRAGMALIB_H_
