/*********************************************************************************************
 *	Name		: _w_red_clievent.cpp
 *	Description	: Implementación de un evento recibido por un Cliente TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/

#include "w_red/w_red_clievent.h"
#include "w_red/w_red_srvudp_private.h"

EventoUDP::EventoUDP()
{
	tipo = EventoUDP::EVENTO_NULO;
	mensaje = "";
	fechallegada = 0;
	idUDP = NULL;
}
EventoUDP::~EventoUDP()
{
	this->destructor();
}

void EventoUDP::destructor()
{
	if (NULL != idUDP)
	{
		delete idUDP;
		idUDP = NULL;
	}
}

EventoUDP::EventoUDP (const EventoUDP & org)
	: idUDP (NULL)
{
	this->operator= (org);
}
void EventoUDP::operator= (const EventoUDP & org)
{
	this->destructor();

	// Y copiar hijo
	this->Evento::operator= (org);

	// Y campo propio
	if (org.idUDP != NULL)
	{
		this->idUDP = new IdentificadorUDP();
		*this->idUDP = *org.idUDP;
	}
}
