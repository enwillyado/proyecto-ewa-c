/*********************************************************************************************
 * Name			: w_red_tester.cpp
 * Description	: Este fichero sirve para configurar las pruebas unitarias que se realizarán
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_w_sistema_tester.pragmalib.h"

int executeCommands();
int executeCommandSimple();

int executeFiles();

int executeTimers();
int executeThreads();

// ----------------------------------------------------------------------------
// Maintester
int main()
{
	executeTimers();
	executeThreads();

	executeFiles();

	executeCommandSimple();
	return executeCommands();
}
