/*********************************************************************************************
 *	Name		: wrapper_base64_c.c
 *	Description	: Wrapper de C para algoritmo de Base64
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_codificacion/w_codificacion_c.h"
#include "base64.h"
#include <string.h>

/**
 * Codifica un buffer binario en un string de base 64
 * \param [in] buffer64In		Buffer a codificar
 * \param [in] szBufferIn		Tama�o del buffer a codificar
 * \param [out] bufferB64Out	Buffer de salida codificado en base64
 * \param [in] szBufferB64Out	Tama�o del buffer de salida
 * \return	EncryptOk			Codificaci�n Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64EncodeFromBuffer (const void* bufferIn, size_t szBufferIn,
									   char* bufferB64Out, size_t szBuffer64Out)
{
	if (base64_encode ((const unsigned char*)bufferIn, szBufferIn, bufferB64Out, szBuffer64Out) == -1)
	{
		return EncryptError;
	}
	return EncryptOk;
}

/**
 * Codifica un string en un buffer de base 64
 * \param [in] pString			Cadena de caracteres a codificar
 * \param [in] szBufferIn		Tama�o del buffer a codificar
 * \param [out] bufferB64Out	Buffer de salida codificado en base64
 * \param [in] szBufferB64Out	Tama�o del buffer de salida
 * \return	EncryptOk			Codificaci�n Ok
 *			EncryptError		Error en parametros
 **/
EEncryptResult base64EncodeFromString (const char* pString,
									   char* bufferB64Out, size_t szBufferB64Out)
{
	if (base64_encode ((const unsigned char*)pString, strlen (pString), bufferB64Out, szBufferB64Out) == 0)
	{
		return EncryptError;
	}
	return EncryptOk;
}

/**
 * Decodifica una cadena en base 64 en su buffer original
 * \param [in] buffer64In	Buffer a codificar
 * \param [in] szBufferIn	Tama�o del buffer a codificar
 * \param [out] bufferOut	Buffer de salida codificado en base64
 * \param [in] szBufferOut	Tama�o del buffer de salida
 * \return	EncryptOk		Codificaci�n Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult base64DecodeToBuffer (const char* stringB64In,
									 void* bufferOut, size_t* szBufferOut)
{
	int result;
	result = base64_decode ((unsigned char*)bufferOut, stringB64In, *szBufferOut);
	if (result == -1)
	{
		return EncryptError;
	}

	*szBufferOut = result;
	return EncryptOk;
}

/**
 * Decodifica una cadena en base 64 en su string original
 * \param [in] buffer64In	Buffer a codificar
 * \param [in] szBufferIn	Tama�o del buffer a codificar
 * \param [out] bufferOut	Buffer de salida codificado en base64
 * \param [in] szBufferOut	Tama�o del buffer de salida
 * \return	EncryptOk		Codificaci�n Ok
 *			EncryptError	Error en parametros
 **/
EEncryptResult base64DecodeToString (const void* buffer64In,
									 char* bufferOut, size_t* szBufferOut)
{
	size_t result;
	result = base64_decode ((unsigned char*)bufferOut, (const char*)buffer64In, *szBufferOut);
	if (result == -1)
	{
		return EncryptError;
	}

	if (*szBufferOut < result + 1)
	{
		return EncryptError;
	}

	bufferOut[result] = 0;
	*szBufferOut = result;

	return EncryptOk;
}
