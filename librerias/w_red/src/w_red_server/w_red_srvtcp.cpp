/*********************************************************************************************
 *	Name		: _w_red_srvtcp.cpp
 *	Description	: Implementaci�n del Servidor TCP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_srvtcp.h"

#include <stdlib.h>						//< para el "atoi"

// --------------------------------------------------------------------------
// Implementaci�n del Servidor TCP

ServidorTCP::ServidorTCP()
	: serverSocket()
{
	this->eventCallBack = NULL;
	this->callBackObj = NULL;
	this->sockfd = DESCRIPTOR_NULO;
	this->lastError = ServidorTCP::DESCONECTADO;
}

// --------------------------------------------------------------------------
// L�gica del Servidor:
/**
 *	Obitene el �ltimo evento que se recibe
 *	\retval	el evento que se recibe y lo elimina de la cola de eventos pendientes
**/
EventoServidorTCP ServidorTCP::getEvento()
{
	if (this->colaEventos.size())
	{
		EventoServidorTCP evento = this->colaEventos.front();
		this->colaEventos.pop();
		return evento;
	}
	return EventoServidorTCP();
}
/**
 *	Obitene el n�mero de eventos que a�n est�n pendientes de
 *	\retval	el n�mero esperado
**/
size_t ServidorTCP::getNumEventosPendintes()
{
	return this->colaEventos.size();
}

/**
 *	Procesa un evento nuevo. Por defecto, lo encola y luego llama
 *		al callBack si est� definido; si no, intenta procesarlo como buenamente pueda
 *	\param	evento	[in]	evento que se va a procesar
**/
void ServidorTCP::analizaEvento (const EventoServidorTCP & evento)
{
	if (this->eventCallBack != NULL)
	{
		this->colaEventos.push (evento);
		this->eventCallBack (*this);
	}
	else
	{
		this->analizaEventoDef (evento);
	}
}
void ServidorTCP::analizaEventoDef (const EventoServidorTCP & evento)
{
	switch (evento.tipo)
	{
	case EventoServidorTCP::CONEXION_ACEPTADA:
		if (this->analizaEventoConexion (evento))
		{
			return;
		}
	case EventoServidorTCP::MENSAJE_RECIBIDO:
	case EventoServidorTCP::MENSAJE_RECIBIDO_VACIO:
		if (this->analizaEventoMsgRecv (evento))
		{
			return;
		}
	case EventoServidorTCP::CONEXION_DESCONECTADA:
	case EventoServidorTCP::CONEXION_PERDIDA:
	case EventoServidorTCP::ERROR_AL_RECIBIR:
		if (this->analizaEventoDesconexion (evento))
		{
			return;
		}
	default:
		return;
	}
}

/**
 *	Analiza y procesa un evento recibido que sea CONEXION_ACEPTADA
 *	\param	evento	[in]	evento (de CONEXION_ACEPTADA) que se va a analizar y procesar
 *	\retval	TRUE si el evento fue analizado; FALSE si no
 *		ATENCI�N: este evento no controla (ni necesita controlar) que se abra la conexi�n a nivel de socket,
 *					ya que en esta capa se hace el control a nivel de protocolo; y lo otro se hace en el "ServerSocket".
 *					Es m�s, cuando se procesa este m�todo, la conexi�n f�sica ya se ha abierto completamente.
**/
bool ServidorTCP::analizaEventoConexion (const EventoServidorTCP & evento)
{
	if (evento.tipo == EventoServidorTCP::CONEXION_ACEPTADA)	//< para cerciorarse
	{
		return true;
	}
	return false;
}

/**
 *	Analiza y procesa un evento recibido que sea CONEXION_DESCONECTADA o CONEXION_PERDIDA
 *	\param	evento	[in]	evento (de CONEXION_DESCONECTADA o CONEXION_PERDIDA) que se va a analizar y procesar
 *	\retval	TRUE si el evento fue analizado; FALSE si no
 *		ATENCI�N: este evento no controla (ni necesita controlar) que se cierre la conexi�n a nivel de socket,
 *					ya que en esta capa se hace el control a nivel de protocolo; y lo otro se hace en el "ServerSocket".
 *					Es m�s, cuando se procesa este m�todo, la conexi�n f�sica ya se ha cerrado completamente.
**/
bool ServidorTCP::analizaEventoDesconexion (const EventoServidorTCP & evento)
{
	if (evento.tipo == EventoServidorTCP::CONEXION_DESCONECTADA ||
			evento.tipo == EventoServidorTCP::CONEXION_PERDIDA ||
			evento.tipo == EventoServidorTCP::ERROR_AL_RECIBIR)	//< son los casos en los que una conexi�n se cierra
	{
		// Cerrar el socket
		return this->close (evento); // bye!
	}
	else
	{
		// No tiene sentido desconectarse con un tipo diferente, �por qu� iba a desconectarse?
	}
	return false;
}

/**
 *	Analiza un nuevo mensaje recibido para procesarlo si se conoce el protocolo que tiene
 *	\param	evento	[in]	evento (de MENSAJE_RECIBIDO) que se va a analizar
 *	\retval	TRUE si el evento fue analizado; FALSE si no
**/
bool ServidorTCP::analizaEventoMsgRecv (const EventoServidorTCP & evento)
{
	// Si no, procesar el mensaje
	if (evento.tipo == EventoServidorTCP::MENSAJE_RECIBIDO)	//< para cerciorarse
	{
		return true;
	}
	return false;

}

/**
 *	Ajusta la funci�n a la que se llamar� cuando se reciba un evento en el servidor
 *	\param	evento	[in]	puntero a la funci�n que se llamar�
 *	\retval	TRUE si se ajust� el callback, FALSE si no
**/
bool ServidorTCP::setCallBack (void (*ptr) (ServidorTCP &), void* callBackObj)
{
	this->eventCallBack = ptr;
	this->callBackObj = callBackObj;
	return (ptr != NULL);
}

void* ServidorTCP::getCallBackObjPtr() const
{
	return this->callBackObj;
}

/**
 *	Obtiene el HOST en el que el Servidor TCP est� escuchando
 *	\retval	el HOST en el que se est� escuchando
 *		o "" si no est� escuchando
**/
const std::string & ServidorTCP::getHost()
{
	return this->host;
}

/**
 *	Obtiene el PUERTO en el que el Servidor TCP est� escuchando
 *	\retval	el PUERTO en el que se est� escuchando
 *		o DESCRIPTOR_NULO si no est� escuchando
**/
unsigned int ServidorTCP::getPort()
{
	return this->port;
}

/**
 *	Empieza la escucha por el puerto dado
 *	\param	port	[in]	puerto a conectar (como string)
 *	\retval	TRUE si se abri� correctamente, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorTCP::open (const std::string & port)
{
	return this->open (::atoi (port.c_str()));
}

/**
 *	Empieza la escucha por el puerto dado
 *	\param	port	[in]	puerto a conectar
 *	\retval	TRUE si se abri� correctamente, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorTCP::open (unsigned int port)
{
	this->lastError = ServidorTCP::CONECTANDO;
	this->port = port;
	// reinicializamos la instancia que manejar� el socket TCP
	serverSocket = ServerSocket (this);
	// Abrir el puerto
	if (false == serverSocket.openSocket (port))
	{
		this->lastError = ServidorTCP::ERRONEO;
		return false;
	}
	return true;
}

/**
 *	Empieza a procesar las peticiones
 *	\retval	TRUE si se abri� correctamente, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorTCP::start()
{
	// Bloquearse procesando peticiones hasta que falle o se cierre
	const bool ret = this->serverSocket.procesarPeticiones();
	return ret;
}

/**
 *	Env�a un mensaje de respuesta al evento dado
 *	\param	evento	[in]	evento que se va a procesar
 *	\param	mensaje	[in]	mensaje que se va a (intentar) enviar
 *	\retval	TRUE si se envi� el mensaje correctamente
 *				en ".getEstado()" est� el estado actual
 **/
bool ServidorTCP::send (const EventoServidorTCP & evento, const std::string & mensaje)
{
	return this->send (evento.socketID, mensaje);
}
bool ServidorTCP::send (const SocketID socketID, const std::string & mensaje)
{
	return serverSocket.sendMessageTCP (socketID, mensaje.c_str(), mensaje.size()) > 0;
}

/**
 *	Hace que la conexi�n con el emisor del evento se cierre
 *	\param	evento	[in]	evento que se va a procesar
 *	\retval	TRUE si se cerr� correctamente la conexi�n, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorTCP::close (const EventoServidorTCP & evento)	// TODO: AJUSTAR NOMBRE
{
	return serverSocket.closeSocket (evento.socketID);
}

/**
 *	El servidor deja de escuchar por el puerto que tiene abierto
 *	\retval	TRUE si se cerr� correctamente la conexi�n, FALSE si no
 *				en ".getEstado()" est� el estado actual
**/
bool ServidorTCP::close()
{
	this->setDesconectando();
	if (serverSocket.closeSrvSocket() == false)
	{
		this->lastError = ServidorTCP::ERRONEO;
		return false;
	}
	this->host = "";
	this->sockfd = DESCRIPTOR_NULO;
	this->setDesconectado(); // HACK: se deber�a confirmar solo al salir de procesandoPeticiones() si es que se invoc�!
	return true;
}


/**
 *	Obtiene el estado actual del servidor
 *	\retval	el estado
**/
ServidorTCP::Errores ServidorTCP::getLastError() const
{
	return this->lastError;
}

/**
 *	Funci�n que obtiene el �ltimo error ocurrido como string
 *	\retval	el �ltimo valor ocurrido, ya convertido a "error" b�sico
**/
std::string ServidorTCP::getLastErrorStr() const
{
	return ServidorTCP::toStr (this->getLastError());
}
/**
 *	Funci�n que obtiene el �ltimo error ocurrido como string
 *	\retval	el �ltimo valor ocurrido, ya convertido a "error" b�sico
**/
std::string ServidorTCP::toStr (ServidorTCP::Errores error)
{
	switch (error)
	{
	// Errores del socket:
	case CONECTANDO:
		return "CONECTANDO";
	case CONECTADO:
		return "CONECTADO";
	case DESCONECTANDO:
		return "DESCONECTANDO";
	case DESCONECTADO:
		return "DESCONECTADO";
	case ERRONEO:
		return "ERRONEO";

		// NOTA: No hay caso 'default' para que, revisando los warns de compilaci�n, se pueda
		//			detectar nuevos casos de error que no est�n puestos humanamente legibles
	}
	return "Error Desconocido";
}

/**
 *	Obtiene el estado actual del socket del servidor
 *	\retval	el estado
**/
ServerSocket::Errores ServidorTCP::getLastErrorSocket() const
{
	return this->serverSocket.getLastError();
}

/**
 *	Obtiene el estado actual del socket del servidor
 *	\retval	el estado
**/
std::string ServidorTCP::getLastErrorSocketStr() const
{
	return this->serverSocket.getLastErrorStr();
}

// ----------------------------------------------------------------------------
// Funciones que llamar� su amigo ServerSocket que ajustan el estado
void ServidorTCP::setConectado()
{
	this->lastError = ServidorTCP::CONECTADO;
}
void ServidorTCP::setEscuchando()
{
	this->lastError = ServidorTCP::ESCUCHANDO;
}
void ServidorTCP::setProcesando()
{
	this->lastError = ServidorTCP::PROCESANDO;
}
void ServidorTCP::setDesconectando()
{
	this->lastError = ServidorTCP::DESCONECTANDO;
}
void ServidorTCP::setDesconectado()
{
	this->lastError = ServidorTCP::DESCONECTADO;
}
