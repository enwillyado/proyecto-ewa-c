/*********************************************************************************************
 * Name			: w_sistema_execs_simple.cpp
 * Description	: Clase para llamar a un comando simple
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_sistema/w_sistema_execs_simple.h"

#ifndef WIN32
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
FILE* _popen (const char* command, const char* type)
{
	return popen (command, type);
}
int _pclose (FILE* stream)
{
	return pclose (stream);
}
#endif

SistemaExecsSimple::SistemaExecsSimple()
	: SistemaExecsBase()
{
}

SistemaExecsSimple::Errores SistemaExecsSimple::exeComando()
{
	this->salidaGenerada = "";
	FILE* p = _popen ((this->getComando()).c_str(), "r");
	if (p != NULL)
	{
		// El proceso est� en ejecuci�n
		this->lastError = SistemaExecsBase::SIN_ERROR;

		// Mientras se est� ejecutando, obtener la salida
		char output[SistemaExecsBase::BUFFER_CMD_SIZE];
		while (fgets (output, SistemaExecsBase::BUFFER_CMD_SIZE, p) != NULL)
		{
			if (funcionChild != NULL)
			{
				funcionChild (output);
			}
			this->salidaGenerada += std::string (output);
		}

#ifdef WIN32
		// Cerrar descriptor y obtener as� el c�digo de salida del programa.
		// \see http://msdn.microsoft.com/en-us/library/aa298534(v=vs.60).aspx
		this->lastExecStatus = _pclose (p);
#else
		// Esperar a que el proceso finalice
		const pid_t pid = wait (&this->lastExecStatus);
		// TODO: �obtener el c�digo de retorno?
		_pclose (p);
#endif

		// El proceso ha finalizado
		this->lastError = SistemaExecsBase::FINALIZADO;

		// Procesar valor de retorno del programa
		if (this->lastExecStatus == 0)
		{
			// TODO: correcto: �hay que hacer algo?
		}
		else
		{
			// TODO: incorrecto: �hay que hacer algo?
		}
	}
	else
	{
		// El proceso no se ha podido crear
		this->lastError = SistemaExecsBase::ERROR_CREAR_PROCESO;
		this->lastExecStatus = -1;
	}

	// Retornar estado
	return this->getLastError();
}
