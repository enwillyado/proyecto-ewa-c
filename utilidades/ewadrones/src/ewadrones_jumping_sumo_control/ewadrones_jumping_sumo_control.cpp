/*********************************************************************************************
 *	Name		: ewadrones_jumping_sumo_control.cpp
 *	Description	: Ejecutable donde se implementa una interfaz de control para JUMPING SUMO
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_ewadrones_jumping_sumo.pragmalib.h"

#include "ewadrones_control_globales.h"
#include "ewadrones_control_app.hpp"
#include "ewadrones_server.hpp"

#include "w_graficos/w_app.h"
#include "w_graficos/w_xlienzo.h"
#include "w_graficos/w_rgba.h"

#define ANCHO_APP	680
#define ALTO_APP	530

#define ANCHO_VIDEO	640
#define ALTO_VIDEO	480

// Evitar que salga la consola
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

class Video
{
public:
	template<const int N>
	static WControl* & ObtenerAppControl (const std::string & nombre)
	{
		static WControl* control = NULL;
		control = ::GetAPP()->obtenerPtr (nombre, true);
		return control;
	}

	static void Video::onNewLienzo (const WLienzo & newLienzo)
	{
		static WControl* & controlVideoOriginal = Video::ObtenerAppControl<0> ("video");
		controlVideoOriginal->setLienzo (newLienzo);
		controlVideoOriginal->pintar();
	}
};

// Programa principal
int main (int argc, char* argv[])
{
	// Crear la instancia de la APP
	WApp app ("JumpingSumoControl", ANCHO_APP, ALTO_APP);
	::GetAPP() = &app;

	// Instanciar un controlador
	::GetJumpingSumo().setOnNewLienzo (&Video::onNewLienzo);
	::GetJumpingSumo().setOnNewImageJPEG (&ServidorWeb::onNewJPEG);
	::GetJumpingSumo().setOnBateriaCambia (CosasAPP::MostrarBateria);

	// Ajustar a APP
	{
		// Ajustar atributos
		app.setFondo (WLienzo::createWLienzo (ANCHO_APP, ALTO_APP, RGBA ("0xA8A8A8")));
		app.setCursor (WXLienzo::getWXLienzoFromFile ("../testdata/cursor.xlz"));

		// Ajustar manejadores de la APP
		app.setOnKeyDown (CosasAPP::onKeyDown);
		app.setOnKeyUp (CosasAPP::onKeyUp);
		app.setOnFocusOut (CosasAPP::onFousOut);
	}
	{
		// A�adir V�deo
		WControl control ("video");
		control.setLienzo (WLienzo::createWLienzo (ANCHO_APP, ALTO_APP, RGBA ("0xA8A8A8")));
		control.resize (ANCHO_VIDEO, ALTO_VIDEO);
		control.setPos (20, 20);
		app.insertar (control);

		WControl* controlPtr = app.obtenerPtr ("video");
		controlPtr->setOnLMouseDrag (CosasAPP::onXMouseDrag);
		controlPtr->setOnMMouseDrag (CosasAPP::onXMouseDrag);
		controlPtr->setOnRMouseDrag (CosasAPP::onXMouseDrag);
	}
	{
		// A�adir Controles de la ventana
		{
			// --------------------------------------------------------------------------
			// Bot�n de cerrar sobre la barra superior
			WControl controlCerrar ("cerrar");
			controlCerrar.setLienzo (WXLienzo::getWXLienzoFromFile ("../testdata/cerrar.xlz"));
			controlCerrar.setPosX (ANCHO_APP - controlCerrar.getLienzo().getAncho() - 5);
			controlCerrar.setPosY (5);

			// A�adir eventos
			class Cerrar
			{
			public:
				static void onLClicMouseOver (const WEventoMouse & evento)
				{
					evento.getRoot().stop();
				}
			};
			controlCerrar.setOnMoveMouseEnter (CosasAPP::invertCursorRoot);
			controlCerrar.setOnMoveMouseOut (CosasAPP::invertCursorRoot);
			controlCerrar.setOnLClicMouseOver (Cerrar::onLClicMouseOver);

			// Insertar el control
			app.insertar (controlCerrar);

			// --------------------------------------------------------------------------
			// Texto para mensajes de depuraci�n sobre la barra superior
			WTextControl controlTexto ("texto");
			controlTexto.setText ("");
			controlTexto.setPosX (5);
			controlTexto.setPosY (5);
			controlTexto.setManejarEventos (false);

			// Insertar el control
			app.insertar (controlTexto);

			// --------------------------------------------------------------------------
			// Texto para mensajes de depuraci�n sobre la barra superior
			WTextControl controlTextoBateria ("bateria");
			controlTextoBateria.setText ("");
			controlTextoBateria.setPosX (50);
			controlTextoBateria.setPosY (5);
			controlTextoBateria.setManejarEventos (false);

			// Insertar el control
			app.insertar (controlTextoBateria);
		}

		// A�adir Controles de vuelo
		{
			// --------------------------------------------------------------------------
			// Bot�n de cerrar sobre la barra superior
			WControl controlDerecha ("derecha");
			controlDerecha.setLienzo (WXLienzo::getWXLienzoFromFile ("../testdata/despegar.xlz"));
			controlDerecha.setPosX (ANCHO_APP - controlDerecha.getLienzo().getAncho() - 20);
			controlDerecha.setPosY (ALTO_VIDEO + 20);

			// A�adir eventos
			class Derecha
			{
			public:
				static void onXClicMouseOver (const WEventoMouse & evento)
				{
					::GetJumpingSumo().derecha();
				}
			};
			controlDerecha.setOnMoveMouseEnter (CosasAPP::invertCursorRoot);
			controlDerecha.setOnMoveMouseOut (CosasAPP::invertCursorRoot);
			controlDerecha.setOnLClicMouseOver (Derecha::onXClicMouseOver);
			controlDerecha.setOnMClicMouseOver (Derecha::onXClicMouseOver);
			controlDerecha.setOnRClicMouseOver (Derecha::onXClicMouseOver);

			// Insertar el control
			app.insertar (controlDerecha);

			// --------------------------------------------------------------------------
			// Bot�n de aterrizar
			WControl controlAtras ("atras");
			controlAtras.setLienzo (WXLienzo::getWXLienzoFromFile ("../testdata/aterrizar.xlz"));
			controlAtras.setPosX (controlDerecha.getPosX() - controlAtras.getLienzo().getAncho() - 20);
			controlAtras.setPosY (ALTO_VIDEO + 20);

			// A�adir eventos
			class Atras
			{
			public:
				static void onXClicMouseOver (const WEventoMouse & evento)
				{
					::GetJumpingSumo().atras();
				}
			};
			controlAtras.setOnMoveMouseEnter (CosasAPP::invertCursorRoot);
			controlAtras.setOnMoveMouseOut (CosasAPP::invertCursorRoot);
			controlAtras.setOnLClicMouseOver (Atras::onXClicMouseOver);
			controlAtras.setOnMClicMouseOver (Atras::onXClicMouseOver);
			controlAtras.setOnRClicMouseOver (Atras::onXClicMouseOver);

			// Insertar el control
			app.insertar (controlAtras);

			// --------------------------------------------------------------------------
			// Bot�n de parada de emergencia
			WControl controlDefrente ("defrente");
			controlDefrente.setLienzo (WXLienzo::getWXLienzoFromFile ("../testdata/emergencia.xlz"));
			controlDefrente.setPosX (controlAtras.getPosX() - controlDefrente.getLienzo().getAncho() - 20);
			controlDefrente.setPosY (ALTO_VIDEO + 20);

			// A�adir eventos
			class Defrente
			{
			public:
				static void onXClicMouseOver (const WEventoMouse & evento)
				{
					::GetJumpingSumo().defrente();
				}
			};
			controlDefrente.setOnMoveMouseEnter (CosasAPP::invertCursorRoot);
			controlDefrente.setOnMoveMouseOut (CosasAPP::invertCursorRoot);
			controlDefrente.setOnLClicMouseOver (Defrente::onXClicMouseOver);
			controlDefrente.setOnMClicMouseOver (Defrente::onXClicMouseOver);
			controlDefrente.setOnRClicMouseOver (Defrente::onXClicMouseOver);

			// Insertar el control
			app.insertar (controlDefrente);

			// --------------------------------------------------------------------------
			// Bot�n de cerrar sobre la barra superior
			WControl controlIzquierda ("izquierda");
			controlIzquierda.setLienzo (WXLienzo::getWXLienzoFromFile ("../testdata/despegar.xlz"));
			controlIzquierda.setPosX (controlDefrente.getPosX() - controlDefrente.getLienzo().getAncho() - 20);
			controlIzquierda.setPosY (ALTO_VIDEO + 20);

			// A�adir eventos
			class Izquierda
			{
			public:
				static void onXClicMouseOver (const WEventoMouse & evento)
				{
					::GetJumpingSumo().izquierda();
				}
			};
			controlIzquierda.setOnMoveMouseEnter (CosasAPP::invertCursorRoot);
			controlIzquierda.setOnMoveMouseOut (CosasAPP::invertCursorRoot);
			controlIzquierda.setOnLClicMouseOver (Izquierda::onXClicMouseOver);
			controlIzquierda.setOnMClicMouseOver (Izquierda::onXClicMouseOver);
			controlIzquierda.setOnRClicMouseOver (Izquierda::onXClicMouseOver);

			// Insertar el control
			app.insertar (controlIzquierda);
		}
	}

	// Iniciar el JUMPING SUMO
	::GetJumpingSumo().init();
	::GetJumpingSumo().start();

	// Iniciar servidor web en hilo a parte
	WThread hiloServidorWeb (&ServidorWeb::mainServidorWeb);
	hiloServidorWeb.start();

	// Iniciar la APP
	app.start();

	// Finalizar correctamente
	return 0;
}

