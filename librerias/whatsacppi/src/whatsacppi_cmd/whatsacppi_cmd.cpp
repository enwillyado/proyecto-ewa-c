/*********************************************************************************************
 *	Name		: whatsacppi_cmd.cpp
 *	Description	: Ejecutable donde se implementa un ejemplo para leer c�digos en BMP
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_whatsacppi_cmd.pragmalib.h"

#include <iostream>

int mainRequest (int argc, char* argv[]);
int mainRegister (int argc, char* argv[]);
int mainLogin (int argc, char* argv[]);

int main (int argc, char** argv)
{
	std::cout << "------------------------------" << std::endl;
	std::cout << "Seleccione una opci�n:" << std::endl;
	std::cout << "[1] mainRequest" << std::endl;
	std::cout << "[2] mainRegister" << std::endl;
	std::cout << "[3] mainLogin" << std::endl;
	const char c = std::cin.get();
	switch (c)
	{
	case 48 + 1:
		mainRequest (argc, argv);
		break;

	case 48 + 2:
		mainRegister (argc, argv);
		break;

	case 48 + 3:
		mainLogin (argc, argv);
		break;

	}

	std::cout << "------------------------------" << std::endl;
	std::cout << "Presiona INTRO para salir" << std::endl;
	std::cin.get();
	return 0;
}
