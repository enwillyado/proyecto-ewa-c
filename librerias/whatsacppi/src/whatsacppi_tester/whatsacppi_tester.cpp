/*********************************************************************************************
 *	Name		: whatsacppi_tester.cpp
 *	Description	: Ejecutable donde se implementa un ejemplo para leer c�digos en BMP
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "_whatsacppi_tester.pragmalib.h"

#include <iostream>

int testerHmacSha1();
int testerPbkdf2();
int testerRc4();
int testerSha1();

int testerBinTreeNodeWriter();
int testerBinTreeNodeReader();
int testerKeyStream();

int testerJson();
int testerPbkdf2Bis();

int main (int argc, char** argv)
{
	testerHmacSha1();
	testerPbkdf2();
	testerRc4();
	testerSha1();

	testerBinTreeNodeWriter();
	testerBinTreeNodeReader();
	testerKeyStream();

	testerJson();
	testerPbkdf2Bis();

	return 0;
}
