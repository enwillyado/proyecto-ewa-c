/*********************************************************************************************
 *	Name		: fiewa_datos_stun.h
 *	Description	: Clase que almacena los datos enviados por el Servidor STUN
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _FIEWA_DATOS_STUN_H_
#define _FIEWA_DATOS_STUN_H_

#include "w_web/w_web_mensaje.h"	//< para tener el Mensaje Web
#include "w_red/w_red_event.h"	//< para tener el Evento

#include <string>

namespace FIEWA
{
	class DatosSTUN
	{
	public:
		typedef std::string PublicID;

	public:
		// Constructores
		DatosSTUN();
		DatosSTUN (const Mensaje & mensajeRecibirDeSTUN);
		DatosSTUN (const Evento &);
		DatosSTUN (const DatosSTUN &);

		// Factor�a
		static DatosSTUN buscandoPeers (const Mensaje & mensajeRecibirDeSTUN);

		// Asignador
		bool parseFromMsg (const Mensaje & mensajeRecibirDeSTUN);

		// M�todos auxiliares est�ticos
		static int buscandoNumPeers (const Mensaje & mensajeRecibirDeSTUNbuscandoPeers);

		// Operadores
		DatosSTUN & operator= (const DatosSTUN &);
		bool operator== (const FIEWA::DatosSTUN &) const;
		bool operator!= (const FIEWA::DatosSTUN &) const;

		// Conversores
		std::string toString() const;

	public:
		// Elemento nulo
		static DatosSTUN npos;

	public:
		// �rea de datos p�blica
		PublicID publicID;
		std::string publicIP;
		int publicPort;
	};
}

#endif //_FIEWA_DATOS_STUN_H_
