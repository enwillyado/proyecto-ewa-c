/*********************************************************************************************
*	Name		: smartAction.hpp
*	Description	: Clase RAII para ajustar un valor temporal sobre un objeto Obj templatizado
*  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
********************************************************************************************/
#ifndef _SMART_ACTION_HPP_
#define _SMART_ACTION_HPP_

#include <cstddef>

class SmartAction
{
public:
	template <class Obj, typename Value>
	SmartAction (Obj & obj,
				 const Value (Obj::*getter)() const,
				 void (Obj::*setter) (const Value &),
				 const Value & tmpValue)
		: voidTemplateData (NULL)
	{
		PrivateData<Obj, Value>* privateData = PrivateData<Obj, Value>::constructor();
		privateData->value = (obj.*getter)();
		privateData->setter = setter;
		privateData->obj = &obj;
		this->voidTemplateData = privateData;

		(obj.*setter) (tmpValue);
		destruct = &SmartAction::destructor<Obj, Value>;
	}
	~SmartAction()
	{
		destruct (voidTemplateData);
	}

	template <typename Obj>
	Obj* const get()
	{
		PrivateData<Obj, void*>* privateData = static_cast<PrivateData<Obj, void*>*> (this->voidTemplateData);
		return static_cast <Obj*> (privateData->obj);
	}

private:
	template <typename Obj, typename Value>
	static void destructor (void* voidUnion)
	{
		PrivateData<Obj, Value>* privateData = static_cast<PrivateData<Obj, Value>*> (voidUnion);
		(static_cast<Obj*> (privateData->obj)->*privateData->setter) (privateData->value);
		PrivateData<Obj, Value>::destructor (privateData);
	}
	typedef void (*PmDestructor) (void*);
	PmDestructor destruct;

protected:
	template<typename Obj, typename Value>
	class PrivateData
	{
	public:
		static PrivateData* constructor()
		{
			return new PrivateData;
		}
		static void destructor (PrivateData* ptr)
		{
			// ASK: �a�adi programaci�n defensiva sobre el puntero?
			delete ptr;
		}
		void (Obj::*setter) (const Value &);
		Obj* obj;
		Value value;
	};
	void* voidTemplateData;
};

template<typename Obj>
class SmartActionObj
{
public:
	template<typename Value>
	SmartActionObj (Obj & obj,
					const Value (Obj::*getter)() const,
					void (Obj::*setter) (const Value &),
					const Value & tmpValue)
		: smartAction (obj, getter, setter, tmpValue)
	{
	}
	operator Obj & ()
	{
		return * (smartAction.get<Obj>());
	}
private:
	SmartAction smartAction;
};

#endif	//_SMART_ACTION_HPP_
