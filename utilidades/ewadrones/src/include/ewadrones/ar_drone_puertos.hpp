/*********************************************************************************************
 * Name			: ar_drone_puertos.hpp
 * Description	: Enumerado de puertos para el AR.DRONE
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _PUERTOS_HPP_
#define _PUERTOS_HPP_

// Mapa de puertos necesarios para comunicarse con el AR.DRONE
#define NOMBRE_DE_SERVIDOR				"192.168.1.2"
#define PUERTO_VIDEO_EN_SERVIDOR		5555		// TCP
#define PUERTO_VIDEO_EN_PROXY			5555
#define PUERTO_2_VIDEO_EN_PROXY			8888

#define PUERTO_VIDEO2_EN_SERVIDOR		5553		// TCP
#define PUERTO_VIDEO2_EN_PROXY			5553
#define PUERTO_2_VIDEO2_EN_PROXY		8800

#define PUERTO_COMANDOS_EN_SERVIDOR		5556		// UDP
#define PUERTO_COMANDOS_EN_PROXY		5556

#define PUERTO_RESPUESTA_EN_SERVIDOR	5554		// UDP
#define PUERTO_RESPUESTA_EN_PROXY		5554

#define PUERTO_CRITICAL_EN_SERVIDOR		5559		// TCP
#define PUERTO_CRITICAL_EN_PROXY		5559

#define PUERTO_AUTH_EN_SERVIDOR			5552		// UDP
#define PUERTO_AUTH_EN_PROXY			5552

#define PUERTO_OTHER_EN_SERVIDOR		5551		// TCP
#define PUERTO_OTHER_EN_PROXY			5551

#define PUERTO_TELNET_EN_SERVIDOR		23			// TCP
#define PUERTO_TELNET_EN_PROXY			23

#define PUERTO_FTP_EN_SERVIDOR			21			// TCP
#define PUERTO_FTP_EN_PROXY				21

#endif // _PUERTOS_HPP_
