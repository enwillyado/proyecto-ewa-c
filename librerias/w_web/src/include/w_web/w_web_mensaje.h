/*********************************************************************************************
 * Name			: w_web_mensaje.h
 * Description	: L�gica para procesar peticiones los mensajes web bajo el Protocolo HTTP
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_MENSAJE_H_
#define _W_RED_MENSAJE_H_

#include "w_web_mensaje_defs.hpp"

#include <string>
#include <vector>
#include <map>

class Mensaje
{
public:
	typedef std::string EncabezadoId;
	typedef std::string EncabezadoValor;
	typedef std::map<EncabezadoId, EncabezadoValor> Encabezados;
	typedef std::pair<EncabezadoId, EncabezadoValor> Encabezado;

	typedef std::vector<std::string> VectorVars;
	typedef std::map<std::string, std::string> ArrayVars;
	typedef std::pair<std::string, std::string> ParVars;

	enum Errores
	{
		SIN_ERROR,				//< la instancia simplemente est� inicializada
		MENSAJE_MAL_FORMADO,	//< el evaluador evalu� mal y debe ser descartado
		MENSAJE_CORRECTO,		//< el evaluador ha evaluado corectamente el mensaje
		MENSAJE_INCOMPLETO,		//< el evaluador est� evaluando bien, por ahora
	};
	enum ParserStates
	{
		BUFFER_VACIO = 0,
		TIPO_MENSAJE_DESCONOCIDO,
		LEIDO_TIPO_MENSAJE,
		TOCA_LEER_URI,
		LEIDO_URI,
		TOCA_LEER_VERSION,
		VERSION_PROTOCOLO_DESCONOCIDO,
		LEIDO_VERSION_PROTOCOLO,
		TOCA_LEER_VERSION_RESPONSE,
		LEIDO_VERSION_PROTOCOLO_RESPONSE,
		TOCA_LEER_CODIGO_ERROR_RESPONSE,
		LEIDO_CODIGO_ERROR_RESPONSE,
		TOCA_LEER_CABECERA,
		TOCA_LEER_CUERPO,
	};
	enum TiposMensaje
	{
		UNKNOWN = 0,		//< tipo desconocido
		// Peticiones:
		HEAD,				//< tipo que identifican una petici�n HTPP est�ndar HEAD
		GET,				//< tipo que identifican una petici�n HTPP est�ndar GET
		POST,				//< tipo que identifican una petici�n HTPP est�ndar POST
		TIPO_PUT,			//< tipo que identifican una petici�n HTPP est�ndar PUT
		TIPO_DELETE,		//< tipo que identifican una petici�n HTPP est�ndar DELETE
		TIPO_TRACE,			//< tipo que identifican una petici�n HTPP est�ndar TRACE
		TIPO_OPTIONS,		//< tipo que identifican una petici�n HTPP est�ndar OPTIONS
		TIPO_CONNECT,		//< tipo que identifican una petici�n HTPP est�ndar CONNECT
		// Respuestas:
		RESPONSE,			//< tipo que identifica una respuesta HTTP est�ndar de respuesta
		RESPONSE_AND_EXIT,	//< tipo que identifica una respuesta HTTP que obliga a cerrar luego el socket
		RESPONSE_THREADED,	//< tipo que identifica una respuesta HTTP que fue delegada en otro hilo
		RESPONSE_VOID,		//< tipo que identifica una respuesta HTTP que no responde
	};
	enum ErroresProtocolo
	{
		SWITCHING_PROTOCOLS = 101,
		FOUND = 200,
		UNAUTHORIZED = 401,
		NOT_FOUND = 404,
	};
	enum VersionesProtocolo
	{
		EWA1_0,
		HTTP1_0,			// \see http://www.w3.org/Protocols/HTTP/1.0/spec.html
		HTTP1_1,			// \see http://www.ietf.org/rfc/rfc2616.txt
		VERSION_DESCONOCIDA,
	};
	Mensaje();
	Mensaje (const std::string &);	// inicializa con un mensaje (petici�n o respuesta) dado

	// Procesadores:
	bool parseFromFile (const std::string & file);
	bool parseFromMsg (const std::string & value);

	ParserStates getParserStatus() const;
	void clearParser();

	// NOTA: �Por qu� no estos dos m�todos, "getTipoMensaje()" y "setTipoMensaje(...)",
	//		no son virtuales? Pues se debe a que el tipo de dato enumerado no se
	//		puede heredar con la herencia m�ltiple, no se pueden a�adir valores
	//		de la enumeraci�n en las clases derivadas. Por lo tanto, se debe
	//		redefinir el enumerado como tipo local de cada clase derivada y cambiar
	//		el tipo devuelto por la funci�n "get" y el tipo recibido de la "set".
	//		El resto de m�todos virtuales, se pueden superponer	o dejar sin implementar.
	//	\see http://c.conclase.net/curso/?cap=037

	// Accesores (constantes):
	const TiposMensaje & getTipoMensaje() const;
	const VersionesProtocolo & getVersionProtocolo() const;
	size_t getEncabezadoSize() const;
	bool existeEncabezadoValor (const EncabezadoId &) const;
	const EncabezadoValor & getEncabezadoValor (const EncabezadoId &) const;
	const std::string & getBody() const;
	const ErroresProtocolo & getError() const;

	// Acceso a las diferentes partes de la petici�n
	// \see http://tools.ietf.org/html/rfc3986#section-3
	const std::string & getURI() const;		// returns : "/recurse/fil%2B.ext?gets=values"
	std::string getUriQuery() const;		// returns : "gets=values"
	std::string getUriPathRecibed() const;	// returns : "/recurse/fil%2B.ext"
	std::string getUriPath() const;			// returns : "/recurse/fil+.ext"

	VectorVars  getUriVars() const;			// returns : {"recurse", "file.ext"}
	ArrayVars   getGetVars() const;			// returns : {"gets"  : "values"}
	ArrayVars   getCookVars() const;		// returns : {"cookies" : "values"}
	ArrayVars   getPostVars() const;		// returns : {"posts" : "values"}
	ArrayVars   getRequestVars() const;		// returns : {"gets/posts" : "values"}
	std::string getRequestVar (const std::string & id) const;	// returns : {"gets/posts"}[id]
	bool     existeRequestVar (const std::string & id) const;	// returns : TRUE if {"gets/posts"}[id] exists

	// Para acceder a los encabezados
	const EncabezadoValor &	operator% (const EncabezadoId & headerstr) const;
	EncabezadoValor 	&	operator% (const EncabezadoId & headerstr);

	// Modificadores:
	virtual void setTipoMensaje (const TiposMensaje &);
	void setVersionProtocolo (const VersionesProtocolo &);
	void setEncabezadoValor (const EncabezadoId &, const EncabezadoValor &);
	void setEncabezadoValor (const Encabezado &);
	void setBody (const std::string & body);
	void setError (const ErroresProtocolo &);
	void setURI (const std::string &);

	// Generadores automatizados:
	virtual void setRespuestaWebPorDefecto (const std::string & body, const ErroresProtocolo err = FOUND);
	virtual std::string genMsg() const;

	// Control de errores:
	Errores getLastError() const;

	// Utilidades est�ticas propias de la clase:
	static Encabezado convertToEncabezado (const std::string &);
	static ParVars getParVars (const std::string &);
	static ArrayVars getVarsFromStr (const std::string &);

	static std::string urlDecode (const std::string & org);
	static std::string urlEncode (const std::string & org);

	static TiposMensaje convertToTiposMensaje (const std::string &);
	static VersionesProtocolo convertToVersion (const std::string &);

	static std::string convertFromTiposMensaje (const TiposMensaje &);
	static std::string convertFromVersion (const VersionesProtocolo &);

protected:
	//Area de datos:
	TiposMensaje		tipoMensaje;
	VersionesProtocolo	versionProtocolo;
	Encabezados			encabezados;
	EncabezadoValor		encabezadoPorDefecto;
	std::string			uri;
	std::string			cuerpo;
	ErroresProtocolo	error;

	// �rea de estados:
	Errores lastError;
	ParserStates parserStatus;
	std::string	 bufferIncompleto;
};

#endif	//_W_RED_MENSAJE_H_
