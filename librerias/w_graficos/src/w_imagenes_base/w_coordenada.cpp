/*********************************************************************************************
* Name			: w_coordenada.h
* Description	: Implementación de la clase para manipular una coordenada gráfica
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_graficos/w_coordenada.h"

// Constructores
WCoordenada::WCoordenada (const size_t posX, const size_t posY)
{
	this->posX = posX;
	this->posY = posY;
}
