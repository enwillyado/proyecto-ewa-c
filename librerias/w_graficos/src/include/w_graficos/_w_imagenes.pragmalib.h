/*********************************************************************************************
 *	Name		: _w_imagenes.pragmalib.h
 *	Description	: Header file with unified libraries to link
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_IMAGENES_PRAGMALIB_H_
#define _W_IMAGENES_PRAGMALIB_H_

#ifdef _MSC_VER //we will need preinclude

#include "w_base/_pragmalib_base.h"

// Propia librer�a: (para no autoengancharla si es una dll)
#pragma comment(lib, "w_imagenes" FIN_LIB)

// Dependencias
#include "w_graficos/_w_imagenes_base.pragmalib.h"
#include "w_graficos/_w_jpeg.pragmalib.h"
#include "w_arbol/_w_arbolxml.pragmalib.h"

#endif

#undef _W_IMAGENES_PRAGMALIB_H_
#else
#error "Referencia circular"
#endif // _W_IMAGENES_PRAGMALIB_H_
