/*********************************************************************************************
 * Name			: w_web_services_secure_prueba.h
 * Description	: Implementación de los ServiciosSecure de prueba
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_RED_SRV_SERVICES_SECURE_PRUEBA_H_
#define _W_RED_SRV_SERVICES_SECURE_PRUEBA_H_

#include "w_web/w_web_services_secure.h"			//< para tener el "ServicioAutentificado"
#include "w_web/w_web_services_secure_prueba_id.h"	//< para tener los ID de los ServiciosSecure de prueba

// ---------------------------------------------------------------------------
// Servicio de Prueba
class ServicioPruebaSecure : public ServicioSecure
{
public:
	ServicioPruebaSecure (const Credenciales & credenciales = Credenciales(), const bool ignorarIncorrectos = true)
		: ServicioSecure (credenciales, ignorarIncorrectos) {};
	virtual BasicSrvService* newInstance() const
	{
		return new ServicioPruebaSecure (this->credenciales, this->ignorarIncorrectos);
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_SECURE_PRUEBA_ID;
	}

public:
	// Servicio a implementar
	virtual Mensaje ejecutarServicio (const Evento & evento, const Mensaje & peticion, const ResultadoAutenticacion & resultadoAutenticacion) const;
};

// ---------------------------------------------------------------------------
// Servicio de Prueba
class ServicioPruebaSecureAlternativa : public ServicioSecure
{
public:
	ServicioPruebaSecureAlternativa (const Credenciales & credenciales = Credenciales(), const bool ignorarIncorrectos = true)
		: ServicioSecure (credenciales, ignorarIncorrectos) {};
	virtual BasicSrvService* newInstance() const
	{
		return new ServicioPruebaSecureAlternativa (this->credenciales, this->ignorarIncorrectos);
	}
	virtual std::string getServicioName() const
	{
		return SRVSERVICE_SECURE_PRUEBA_ID_ALTERNATIVA;
	}

public:
	// Servicio a implementar
	virtual Mensaje ejecutarServicio (const Evento & evento, const Mensaje & peticion, const ResultadoAutenticacion & resultadoAutenticacion) const;
};

#endif	//_W_RED_SRV_SERVICES_THREAD_PRUEBA_H_
