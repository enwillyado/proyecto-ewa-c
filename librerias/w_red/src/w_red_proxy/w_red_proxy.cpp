/*********************************************************************************************
 *	Name		: w_red_proxy.cpp
 *	Description	: Implementación de la clase base para implementar un proxy de red
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_red/w_red_proxy.h"

Proxy::Proxy (const std::string & remoteHost, const int remotePort)
	: host (remoteHost), port (remotePort)
{
}

bool Proxy::open (const int localPort)
{
	// Ajustar los valores comunes
	this->localPort = localPort;
	return true;
}
