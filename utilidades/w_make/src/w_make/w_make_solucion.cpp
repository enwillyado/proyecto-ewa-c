/*********************************************************************************************
 *	Name		: w_make_solucion.cpp
 *	Description	: Implementanción de las funciones para preparar una orden SOLUCIÓN
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#include "w_make/w_make.h"

WMake::Estado WMake::prepararSln()
{
	return WMake::CORRECTO;
}
