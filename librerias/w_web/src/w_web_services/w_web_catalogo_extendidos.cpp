/*********************************************************************************************
 * Name			: w_web_catalogo_extendidos.cpp
 * Description	: Implementaci�n del �ndice que permite traducir Servicios Extendidos
 *					en Servicios supuestamente disponibles en un Mapa de Servicios
 * Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/

#include "w_web/w_web_catalogo_extendidos.h"

IndiceServiciosExtendidos::IndiceServiciosExtendidos()
{
}

/**
 *	Inserta en el �ndice un Servicio Extendido
 *	\param	idservice	[in]	identificador del servicio (se supone que ser� extendido)
**/
void IndiceServiciosExtendidos::insert (const std::string & id)
{
	if (id == "")
	{
		return;
	}
	size_t i = id.find_first_of ('/');
	if (i == std::string::npos)
	{
		return this->serviciosExtendidos[id].insert ("");
	}
	const std::string & parteAnterior = id.substr (0, i);
	const std::string & partePosterior = id.substr (i + 1);
	return this->serviciosExtendidos[parteAnterior].insert (partePosterior);
}

std::string IndiceServiciosExtendidos::resolve (const std::string & id, const std::string & ret) const
{
	if (id == "")
	{
		return ret;
	}
	size_t i = id.find_first_of ('/');
	if (i == std::string::npos)
	{
		MapaIndices::const_iterator itr = serviciosExtendidos.find (id);
		if (itr == serviciosExtendidos.end())
		{
			return ret;
		}
		return "";
	}
	const std::string & parteAnterior = id.substr (0, i);
	const std::string & partePosterior = id.substr (i + 1);
	// Ahora que est� dividido:
	MapaIndices::const_iterator itr = serviciosExtendidos.find (parteAnterior);
	if (itr == serviciosExtendidos.end())
	{
		itr = serviciosExtendidos.find ("*");
		if (itr != serviciosExtendidos.end())
		{
			return itr->second.resolve (partePosterior, ret + "*" "/");
		}
	}
	else
	{
		return itr->second.resolve (partePosterior, ret + parteAnterior + "/");
	}
	return "";
}

void IndiceServiciosExtendidos::erase (const std::string & id)
{
}

/**
 *	Determina si un identificador de servicio es o no extendido
 *	\retval	TRUE si lo es, FALSE en caso contrario
**/
bool IndiceServiciosExtendidos::esExtendido (const std::string & id)
{
	return std::string::npos != id.find_first_of ('*');
}
