/*********************************************************************************************
 * Name			: w_app__functores.h
 * Description	: Estructura para guardar functores
 * Copyright	(LSeWa) 2015 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _W_APP_FUNCTORES_H_
#define _W_APP_FUNCTORES_H_

#include "w_graficos/w_eventoKey.h"
#include "w_graficos/w_eventoMouse.h"
#include "w_graficos/w_eventoMouseDrag.h"

#include <string>
#include <map>

struct AppFunctores
{
public:
	static const AppFunctores & GetNull();

public:
	typedef void (*FunctorOn) (const WEvento &);
	typedef std::map<std::string, FunctorOn> FunctoresWEvento;

	FunctorOn getFunctorOn (const std::string & id) const;
	void add (const std::string & id, const FunctorOn &);

public:
	typedef void (*FunctorOnKey) (const WEventoKey &);
	typedef std::map<std::string, FunctorOnKey> FunctoresWEventoKey;

	FunctorOnKey getFunctorOnKey (const std::string & id) const;
	void add (const std::string & id, const FunctorOnKey &);

public:
	typedef void (*FunctorWEventoMouse) (const WEventoMouse &);
	typedef std::map<std::string, FunctorWEventoMouse> FunctoresWEventoMouse;

	FunctorWEventoMouse getFunctorWEventoMouse (const std::string & id) const;
	void add (const std::string & id, const FunctorWEventoMouse &);

public:
	typedef void (*FunctorWEventoMouseDrag) (const WEventoMouseDrag &);
	typedef std::map<std::string, FunctorWEventoMouseDrag> FunctoresWEventoMouseDrag;

	FunctorWEventoMouseDrag getFunctorWEventoMouseDrag (const std::string & id) const;
	void add (const std::string & id, const FunctorWEventoMouseDrag &);

private:
	// �rea de datos
	FunctoresWEvento functoresWEvento;
	FunctoresWEventoKey functoresWEventoKey;
	FunctoresWEventoMouse functoresWEventoMouse;
	FunctoresWEventoMouseDrag functoresWEventoMouseDrag;
};

#endif // _W_APP_FUNCTORES_H_
