/*********************************************************************************************
 *	Name		: fiewa_services_id.h
 *	Description	: Identificadores (como string) de los servicios propios del Servidor de Pares
 *  Copyright	(LSeWa) 2014 PROYECTO EWA (http://www.proyectoewa.com/)
 ********************************************************************************************/
#ifndef _FIEWA_SERVICES_ID_H_
#define _FIEWA_SERVICES_ID_H_

// Servicio para cuando se haga una prueba
#define SRVSERVICE_IPANDPORT_ID "/ipandport"
#define SRVSERVICE_IPOFPEER_ID	"/ipofpeer"
#define SRVSERVICE_LOGIN_ID		"/login"

#endif	//_FIEWA_SERVICES_ID_H_
